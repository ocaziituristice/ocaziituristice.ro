<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );


$time_start = microtime( true );
$search     = $_GET['search'];
?>

    <!DOCTYPE html>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <head>
		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/header_charset.php" ); ?>
		<?php
		include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/class_sejururi/class_sejur_normal_responsive.php' );
		if ( ! $_REQUEST['from'] ) {
			$from = 1;
		} else {
			$from = $_REQUEST['from'];
		}

		if ( ! $nr_pe_pagina = $_COOKIE['nr_pe_pagina'] ) {
			$nr_pe_pagina = 10;
		}
		if ( $_REQUEST['tari'] )
		{ 
		$link_tara = $_REQUEST['tari'];
		$tara=desfa_link($_REQUEST['tari']);
		$id_tara=get_id_tara($tara);
		$den_tara  = ucwords( $tara );
		}
		$cont_link = '';
		$indice    = 'cazare-';
		$id_tip    = '';
		$iduri     = '';
		$tip       = desfa_link( $_REQUEST['tip'] );
		$denumire_tip=ucwords($tip);
		$id_tip    = get_id_tip_sejur( $tip );
		$tip_fii   = get_id_tip_sejur_fii( $id_tip );
		$iduri     = "'" . $id_tip . "'";
		if ( $tip_fii['id_tipuri'] ) {
			$iduri = $iduri . ',' . $tip_fii['id_tipuri'];
		}

		if ( $id_tip > 0 ) {
			$indice = 'oferte-' . $_REQUEST['tip'] . '/';
		}

		if ( $_REQUEST['zone'] ) {
			$zona    = desfa_link( $_REQUEST['zone'] );
			$id_zona = get_id_zona( $zona, $id_tara );
			verifica_localizarea($tara,$zona,$localitate=NULL);

			$sel = "select denumire, descriere_seo, titlu_seo, cuvinte_cheie_seo, luni_plecari, data_plecare,descriere from zone where id_zona = '" . $id_zona . "' ";
			$que = mysql_query( $sel ) or die( mysql_error() );
			$row = mysql_fetch_array( $que );
			@mysql_fetch_array( $que );

			$luni_plecari  = explode( ',', $row['luni_plecari'] );
			$dates_plecare = $row['data_plecare'];
			$link_zona     = $_REQUEST['zone'];
			$cont_link     = $cont_link . '&zone=' . $link_zona;
			$den_zona      = ucwords( $row['denumire'] );
			$titlu_pag     = $row['denumire'] . ', ' . $den_tara;
			if($_REQUEST['tip']) {$denumire_tip.=' ';}
			$titlu_pag     = 'Oferte Sejur '.$denumire_tip . $nmasa . $den_zona . ' - ' . $den_tara . ' ' . $an_afisat;
			if ( $nmasa == 'all inclusive' ) {
				$titlu_pag = 'Oferte All Inclusive ' . $den_zona;
			}
			$titlu_h2     = $row['denumire'];
			$titlu_h3     = $row['denumire'];
			$cuvant_cheie = $titlu_pag;
		} else {
			$id_zona = '';
		}

		$zona_detalii = get_detalii_by_id_zona( $id_zona );
	
	if($_REQUEST['oras']) {
	$oras=desfa_link($_REQUEST['oras']);
	if(($oras=='lara' || $oras=='kundu') && $tara=='turcia') {
		header("HTTP/1.0 301 Moved Permanently");
		header("Location: ".$sitepath.'oferte-'.$_REQUEST['tip'].'/turcia/antalya/lara_d_kundu/');
		exit();
	}
	$id_localitate=get_id_localitate($oras, $id_zona);
	
	
	$sel="select denumire, descriere_seo, titlu_seo, cuvinte_cheie_seo, luni_plecari, data_plecare from localitati where id_localitate = '".$id_localitate."' ";
	$que=mysql_query($sel) or die(mysql_error());
	$row=mysql_fetch_array($que);
	@mysql_fetch_array($que);
	
	$luni_plecari=explode(',', $row['luni_plecari']);
	$dates_plecare = $row['data_plecare'];
	$link_oras=$_REQUEST['oras'];
	$cont_link=$cont_link.'&oras='.$link_oras;
	$den_loc=$row['denumire'];
	$titlu_pag=ucwords($tip).' '.$den_loc.', '.$den_tara;
	$titlu_h2=ucwords($tip).' '.$row['denumire'];
	$titlu_h3=ucwords($tip).' '.$row['denumire'];
	$cuvant_cheie=$titlu_pag;
} else $id_localitate='';	
		
		

	$link_p=$sitepath.'oferte-'.$_REQUEST['tip'].'/'.$link_tara.'/'.$link_zona.'/';
$link=$link_p;
$err=0;
 if(!$_REQUEST['data-plecare']) $link='?optiuni=da'; else $link='?data-plecare='.$_REQUEST['data-plecare'];
		$err    = 0;
		include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/request.php" );
		$luna = array(
			'01' => 'Ianuarie',
			'02' => 'Februarie',
			'03' => 'Martie',
			'04' => 'Aprilie',
			'05' => 'Mai',
			'06' => 'Iunie',
			'07' => 'Iulie',
			'08' => 'August',
			'09' => 'Septembrie',
			'10' => 'Octombrie',
			'11' => 'Noiembrie',
			'12' => 'Decembrie'
		); ?>
		<?php
		if ( $rowSEO['title_seo'] == "" ) {
			$metas_title = 'Oferte Sejur ' . str_replace( 'Insula', '', $den_zona ) . ' - ' . $den_tara . ' ' . $an_afisat . ' | ' . $denumire_agentie;
		} else {
			$metas_title = $rowSEO['title_seo'];
		}
		if ( $rowSEO['description'] == "" ) {
			$metas_description = 'Oferte sejur' . str_replace( 'Insula', '', $den_zona ) . ' ' . $an_afisat . ' in ' . $den_tara . ' la cele mai avantajoase tarife pe ' . $den_tara . ' pentru vacanta ta in ' . $den_zona . ' oferite de ' . $denumire_agentie;
		} else {
			$metas_description = $rowSEO['description'];
		}
		if ( $rowSEO['keywords'] == "" ) {
			$metas_keywords = $cuvant_cheie . ', sejur ' . $den_zona . ', cazare ' . $den_zona . ', oferte ' . $den_zona;
		} else {
			$metas_keywords = $rowSEO['keywords'];
		}

	
		$link_of_pag = w3c_and( $link );
		if ( $_REQUEST['ordonare'] ) {
			$link = $link . "&ordonare=" . $_REQUEST['ordonare'];
		}
		if ( $link == '?optiuni=da' ) {
			$link = '';
		}
		$time_end_1 = microtime( true );
		?>
		<?php
		$oferte = new AFISARE_SEJUR_NORMAL_RESPONSIVE();
		$oferte->setAfisare($from, $nr_pe_pagina);
$oferte->config_paginare('nou');
$oferte->setukey($ukey_0);
$oferte->setrequest($request_complet);
if($early=='da') $oferte->setEarly('da');
if($link_tara) $oferte->setTari($link_tara);
if($link_zona) $oferte->setZone($link_zona);
if($link_oras) $oferte->setOrase($link_oras);
if($id_tip) $oferte->setTipOferta($_REQUEST['tip']);
		if ( $early == 'da' ) {
			$oferte->setEarly( 'da' );
		}
		if ( $link_tara ) {
			$oferte->setTari( $link_tara );
		}
		if ( $link_zona ) {
			$oferte->setZone( $link_zona );
		}
		if ( $id_tip ) {
			$oferte->setTipOferta( $_REQUEST['tip'] );
		}
		if ( isset( $_GET['id_hotel_selected'] ) ) {
			$oferte->set_hotel_selected( $_REQUEST['id_hotel_selected'] );
		}
		if ( $_REQUEST['ordonare'] ) {
			$tipO = explode( '-', $_REQUEST['ordonare'] );
			if ( $tipO[0] == 'tip_pret' ) {
				$oferte->setOrdonarePret( $tipO[1] );
			} elseif ( $tipO[0] == 'tip_numH' ) {
				$oferte->setOrdonareNumeH( $tipO[1] );
			}
		}

		$watermark_cautare = 'Cauta hoteluri din ' . $den_tara;
		if ( $_REQUEST['nume_hotel'] && $_REQUEST['nume_hotel'] <> $watermark_cautare ) {
			$oferte->setCautaHotel( $_REQUEST['nume_hotel'] );
		}
		if ( $_REQUEST['transport'] && $_REQUEST['transport'] <> 'toate' ) {
			$oferte->setTransport( $_REQUEST['transport'] );
		}
		if ( $_REQUEST['stele'] && $_REQUEST['stele'] <> 'toate' ) {
			$oferte->setStele( $_REQUEST['stele'] );
		}
		if ( $_REQUEST['concept'] && $_REQUEST['concept'] <> 'toate' ) {
			$oferte->setConcept( $_REQUEST['concept'] );
		}
		if ( $_REQUEST['facilitati'] && $_REQUEST['facilitati'] <> 'toate' ) {
			$oferte->setFacilitati( $_REQUEST['facilitati'] );
		}
		if ( $_REQUEST['masa'] && $_REQUEST['masa'] <> 'toate' ) {
			$oferte->setMasa( $_REQUEST['masa'] );
		}
		if ( $id_loc_plecare_av ) {
			$oferte->setPlecareAvion( $id_loc_plecare_av );
		}
		if ( $id_loc_plecare_aut ) {
			$oferte->setPlecareAutocar( $id_loc_plecare_aut );
		}
		if ( $search == 'da' ) {
			$oferte->setCautare( $search );
		}
		if ( sizeof( $ds ) == 3 ) {
			$oferte->setDistanta( $ds );
		} ?>

		<?php
		$search = $_GET['search'];
		if ( $search == 'da' ) {
			include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/adauga_in_cache.php" );
		} ?>

		<?php

		$oferte->initializeaza_pagini( $link_p, 'pag-###/', $link );
		$nr_hoteluri = $oferte->numar_oferte();
		$time_end_2  = microtime( true );

		$meta_pages     = $oferte->meta_pages( $_REQUEST['tari'], $_REQUEST['zone'], null );
		$meta_prev      = $meta_pages['meta_prev'];
		$meta_next      = $meta_pages['meta_next'];
		$meta_canonical = '<link rel="canonical" href="' . $meta_pages['link_canonical'] . '" />';

		$afisare_info = '<strong>Sejur ' . $den_zona . '</strong>';
		//if($checkin) $afisare_info .= ' cu plecare in ziua <strong>'.date("d.m.Y",strtotime($checkin)).'</strong> (&plusmn; 4 zile)';
		if ( $nr_stele ) {
			$info_stele   = 'Hoteluri de categorie ' . $nr_stele . ' stele';
			$afisare_info .= ' de <strong>' . $nr_stele;
			if ( $nr_stele > 1 ) {
				$afisare_info .= ' stele';
			} else {
				$afisare_info .= ' stea';
			}
			$afisare_info .= '</strong>';
		}
		if ( $id_transport ) {
			if ( $id_transport == 1 ) {
				$afisare_info .= ' <strong>' . $trans . '</strong>';
			} else {
				$afisare_info .= ' cu transport <strong>' . $trans . '</strong>';
			}
		}
		if ( $nmasa ) {
			$afisare_info .= ' si masa <strong>' . $nmasa . '</strong> inclusa';
		}
		if ( $plecare_avion <> 'toate' ) {
			$afisare_info .= ', plecare din ' . ucwords( $plecare_avion );
		}
		if ( $plecare_autocar <> 'toate' ) {
			$afisare_info .= ', plecare din ' . ucwords( $plecare_autocar );
		}
		if ( $nfacilitati ) {
			$afisare_info .= ', cu <strong>' . $nfacilitati . '</strong>';
		}
		if ( $nconcept ) {
			$afisare_info .= ', pentru <strong>' . $nconcept . '</strong>';
		}

		if ( $_REQUEST['optiuni'] ) {
			//$metas_title = strip_tags($afisare_info);
		}
		$time_end_3 = microtime( true );
		?>

		<?php if ( $rowSEO['title_seo'] ) {
			$keyword_nice[1] = $rowSEO['title_seo'];
			$keyword_nice[0] = $rowSEO['description'];
			$keyword_nice[2] = $rowSEO['keywords'];
		} else {
			$keyword_nice[1] = "Oferte sejur " . $den_zona . " din " . $den_tara;
			$keyword_nice[0] = "Sejururi " . $den_zona . " - " . $den_tara;
			$keyword_nice[2] = "Destinatii " . $den_zona . ", sejururi " . $den_zona . ", cazare " . $den_zona;
		} ?>
        <title><?php echo $metas_title; ?></title>
        <meta name="description" content="<?php echo $metas_description; ?>"/>
        <meta name="keywords" content="<?php echo $metas_keywords; ?>"/>
		<?php echo $meta_canonical . " " . $meta_prev . " " . $meta_next . "\n"; ?>
        <!--		--><?php //include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/addins_head.php' ); ?>
		<?php require_once( "includes/header/header_responsive.php" ); ?>
    </head>

    <body>
	<?php include( $_SERVER['DOCUMENT_ROOT'] . '/includes/header/admin_bar_responsive.php' ); ?>

	<?php // Header ?>
    <header>
		<?php require( "includes/header/meniu_header_responsive.php" ); ?>
    </header>

	<?php // Breadcrumbs and general search ?>
    <div class="layout">
		<?php require( "includes/header/breadcrumb_responsive.php" ); ?>
		<?php require( "includes/header/search_responsive.php" ); ?>
    </div>
    <script>
        function myFunction(x) {
            x.classList.toggle("change");
        }
    </script>

	<?php // Content ?>
    <div class="layout">
        <script type="text/javascript">
            var ray = {
                ajax: function (st) {
                    this.show('load');
                },
                show: function (el) {
                    this.getID(el).style.display = '';
                },
                getID: function (el) {
                    return document.getElementById(el);
                }
            }
        </script>
        <div id="load" style="display:none;"></div>
        <script>
          //  $("#load").empty().html('<img src="/images/loader.gif" alt="" />')
        //    $("#load").load("/includes/mesaj_reclama.php");
        </script>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/sejururi/localitate_new1.php" );
		$time_end_4 = microtime( true );
		?>
    </div>


	<?php // Newsletter ?>
	<?php require_once( "includes/newsletter_responsive.php" ); ?>

	<?php // Footer ?>
	<?php require_once( "includes/footer/footer_responsive.php" ); ?>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

    </body>
    </html>
<?php //include_once($_SERVER['DOCUMENT_ROOT']."/sfarsit_cache.php");?>
<?php $time_end = microtime( true );
//echo "Pagina a fost incarcata in ". $execution_time = ($time_end - $time_start).";". ($time_end_1 - $time_start).";".($time_end_2 - $time_start).";".($time_end_3 - $time_start).";".($time_end_4 - $time_start)." secunde";
?>
