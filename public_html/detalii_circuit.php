<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );
if ( $_REQUEST['id_oferta'] ) {
	include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_sejur.php' );
	include( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/class_sejururi/class_sejur.php' );

	$id_oferta              = $_REQUEST['id_oferta'];
	$id_circuit             = $id_oferta;
	$GLOBALS['tip_unitate'] = 'Circuit';
	setlocale( LC_TIME, array( 'ro.utf-8', 'ro_RO.UTF-8', 'ro_RO.utf-8', 'ro', 'ro_RO', 'ro_RO.ISO8859-2' ) );
	$det           = new DETALII_SEJUR();
	$detalii       = $det->select_det_sejur( $id_oferta );
	$preturi       = $det->select_preturi_circuit( $id_oferta );
	$id_hotel      = $detalii['id_hotel'];
	$detalii_hotel = $det->select_camp_hotel( $id_hotel );
} elseif ( $_REQUEST['id_hotel'] ) {
	$id_hotel      = $_REQUEST['id_hotel'];
	$det           = new DETALII_HOTEL();
	$detalii_hotel = $det->select_camp_hotel( $id_hotel );
	$id_oferta     = '';
}
$link_oferta = substr( $sitepath, 0, - 1 ) . make_link_circuit( $detalii_hotel['denumire'], $id_oferta );

if ( ! is_bot() and $link_oferta == ( substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI'] ) ) {
	$sel_ofvisit = "SELECT link FROM oferte_vizitate WHERE id_sesiune_vizitator = '" . session_id() . "' AND link = '" . $link_oferta . "' ";
	$que_ofvisit = mysql_query( $sel_ofvisit ) or die( mysql_error() );
	if ( mysql_num_rows( $que_ofvisit ) == 0 ) {
		$que_of_visit = "INSERT INTO oferte_vizitate (id_oferta, link, referrer, data_vizita, ip_vizitator, id_sesiune_vizitator, user_agent) VALUES ('" . $id_oferta . "', '" . substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI'] . "', '" . $_SERVER['HTTP_REFERER'] . "', NOW(), '" . $_SERVER['REMOTE_ADDR'] . "', '" . session_id() . "', '" . $_SERVER['HTTP_USER_AGENT'] . "')";
		$res_of_visit = mysql_query( $que_of_visit ) or die ( mysql_error() );
	}
}

if ( $detalii['denumire'] == '' or $detalii_hotel['denumire'] == '' or $detalii_hotel['tip_unitate'] != 'Circuit' ) {
	header( "HTTP/1.0 404 Not Found" );
	$handle = curl_init( $sitepath . '404.php' );
	curl_exec( $handle );
	exit();
}

$sel_cont = "SELECT
GROUP_CONCAT(DISTINCT tari.denumire Order by traseu_circuit.ordine SEPARATOR ', ') as tari,
GROUP_CONCAT(DISTINCT 
if(traseu_circuit.tara_principala='da',tari.denumire,'') Order by traseu_circuit.ordine SEPARATOR ',') as tari_principale,
GROUP_CONCAT(DISTINCT localitati.denumire Order by traseu_circuit.ordine SEPARATOR ', ') as localitati,
GROUP_CONCAT(DISTINCT traseu_circuit.id_hotel SEPARATOR ',') as hoteluri
FROM
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
Left join localitati on traseu_circuit.id_localitate = localitati.id_localitate
WHERE
hoteluri.tip_unitate = 'Circuit' and
oferte.id_oferta = '" . $id_oferta . "'
Order by traseu_circuit.ordine ";
$que_cont = mysql_query( $sel_cont ) or die( mysql_error() );
$row_of          = mysql_fetch_array( $que_cont );
$tari_principale = $row_of['tari_principale'];
@mysql_free_result( $que_cont );


$metas_title       = $detalii_hotel['denumire'];
$metas_description = $detalii_hotel['denumire'] . ', ' . $detalii['nr_zile'] . ' zile, masa ' . $detalii['masa'] . ', transport ' . $detalii['transport'];
if ( $detalii['taxa_avion']['inclus'] == 'da' ) {
	$metas_description = $metas_description . ' cu toate taxele incluse';
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/header_charset.php" ); ?>
    <title><?php echo $metas_title; ?></title>
    <meta name="description" content="<?php echo $metas_description; ?>"/>
    <link rel="canonical" href="<?php echo $link_oferta; ?>"/>

    <meta property="og:title" content="<?php echo $metas_title; ?>"/>
    <meta property="og:description" content="<?php echo $metas_description; ?>"/>
    <meta property="og:url"
          content="<?php echo $sitepath . 'circuit/'/*.fa_link($detalii_hotel['nume_continent']).'/'*/ . fa_link_oferta( $detalii['denumire'] ) . '-' . $id_oferta . '.html'; ?>"/>
    <meta property="og:image" content="<?php echo $sitepath . 'img_prima_hotel/' . $detalii_hotel['poza1']; ?>"/>
    <meta property="og:site_name" content="<?php echo $denumire_agentie; ?>"/>
    <meta property="fb:admins" content="100000435587231"/>
    <meta property="fb:app_id" content="314711018608962"/>
    <meta property="og:type" content="ocaziituristice:trip"/>

	<?php if ( $_REQUEST['denumire_circuit'] <> fa_link_circuit( $detalii_hotel['denumire'] ) ) { ?>
        <link rel="canonical"
              href="<?php echo $sitepath . 'circuit/'/*.fa_link($detalii_hotel['nume_continent']).'/'*/ . fa_link_circuit( $detalii_hotel['denumire'] ) . '-' . $id_oferta . '.html'; ?>"/>
	<?php } ?>
	<?php require_once( "includes/header/header_responsive.php" ); ?>
</head>

<body>

<?php include( $_SERVER['DOCUMENT_ROOT'] . '/includes/header/admin_bar_responsive.php' ); ?>

<?php // Header ?>
<header>
	<?php require( "includes/header/meniu_header_responsive.php" ); ?>
</header>

<?php // Breadcrumbs and general search ?>
<div class="layout">
	<?php require( "includes/header/breadcrumb_responsive_intreg.php" ); ?>
</div>
<script>
    function myFunction(x) {
        x.classList.toggle("change");
    }
</script>

<div class="layout">
	<?php if ( isset( $_GET['cerere_oferta'] ) ) { ?>
        <div class="NEW-column-full pad20 circuit">
            <h2>Multumim pentru cererea efectuata! <span class="green">Care sunt urmatorii pasi?</span></h2>
            <div class="NEW-orange NEW-round8px" style="border:1px solid #ffd0d0; padding:10px 5px;">
                <img src="<?php echo $imgpath; ?>/cerere_oferta.jpg" class="float-left" style="margin:10px;"/>
                <div class="float-right" style="margin:0 10px 0 50px; width:260px;">
                    <p>Pentru orice nelamuriri sau intrebari nu ezita sa ne contactezi:</p>
                    <strong>Telefon:</strong> <?php echo $contact_telefon; ?><br/>
                    <strong>Mobil:</strong> <?php echo $contact_mobil; ?><br/>
                    <strong>Program:</strong> <?php echo $contact_program; ?>
                </div>
                <p style="padding-top:17px;" class="black">In scurt timp vei fi contactat (telefonic sau prin
                    e-mail) de catre un agent de turism din echipa Ocaziituristice.ro in care ti se va prezenta
                    oferta noastra pentru cererea ta.</p>
                <p class="black">Iti multumim pentru interesul acordat!</p>
                <br class="clear"/>
            </div>
			<?php /*?><div class="fb-like-box" data-href="http://www.facebook.com/OcaziiTuristice.ro" data-width="292" data-show-faces="true" data-stream="false" data-header="false" style="float:right;"></div><?php */ ?>
            <br/><br/>
            <h2 class="red">Spune-le si prietenilor tai despre aceasta oferta</h2>
            <br/>
            <img src="<?php echo $sitepath . 'thumb_hotel/' . $detalii_hotel['poza1']; ?>" class="images"
                 style="margin:10px;"/>
            <h3 style="margin:10px 10px 0 10px;"><?php echo $metas_title; ?></h3>
            <div class="grey italic"><?php echo $sitepath . fa_link( $detalii_hotel['denumire_continent'] ) . '/' . fa_link_oferta( $detalii['denumire'] ) . '-' . $id_oferta . '.html'; ?></div>
            <div style="float:left; margin:30px 0 0 30px;">
                <a name="fb_share"></a>
                <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
            </div>
            <br class="clear"/>
        </div>
	<?php } ?>

	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/circuite/detalii_circuit_responsive.php" ); ?>

    <div class="circuit">
        <div id="alteOferteVizitate"></div>
    </div>
</div>
<?php // Newsletter ?>
<?php require_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/newsletter_responsive.php" ); ?>
<?php // Footer ?>
<?php require_once( $_SERVER['DOCUMENT_ROOT'] ."/includes/footer/footer_responsive.php" ); ?>
<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

<?php
if ( ! is_bot() and $link_oferta == ( substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI'] and isset( $err_logare_admin ) ) ) {
	$sel_ofvisit = "SELECT link FROM oferte_vizitate WHERE id_sesiune_vizitator = '" . session_id() . "' AND link = '" . $link_oferta . "' ";
	$que_ofvisit = mysql_query( $sel_ofvisit ) or die( mysql_error() );
	if ( mysql_num_rows( $que_ofvisit ) == 0 ) {
		$que_of_visit = "INSERT INTO oferte_vizitate (id_oferta, link, referrer, data_vizita, ip_vizitator, id_sesiune_vizitator, user_agent) VALUES ('" . $id_oferta . "', '" . substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI'] . "', '" . $_SERVER['HTTP_REFERER'] . "', NOW(), '" . $_SERVER['REMOTE_ADDR'] . "', '" . session_id() . "', '" . $_SERVER['HTTP_USER_AGENT'] . "')";
		$res_of_visit = mysql_query( $que_of_visit ) or die ( mysql_error() );
	}
}
?>

<script>
    $("#alteOferteVizitate").empty().html('<img src="/images/loader.gif" alt="" />');
    $("#alteOferteVizitate").load("/includes/oferte_vizualizate_altii_responsive.php?idOf=<?php echo $id_oferta; ?>");
</script>
<?php if ( $detalii_hotel['cod_remarketing'] ) {
	echo $detalii_hotel['cod_remarketing'];
} ?>
</body>
</html>
