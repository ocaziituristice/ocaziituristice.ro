<?php

include_once __DIR__ . '/config/functii.php';
include_once __DIR__ . '/config/includes/class/mysql.php';

use Phalcon\Di\FactoryDefault;

$di = FactoryDefault::getDefault();

/** @var \Phalcon\Db\Adapter\Pdo\Mysql $db */
$db = $di->get('db');

$luna = [
    '01' => 'Ianuarie',
    '02' => 'Februarie',
    '03' => 'Martie',
    '04' => 'Aprilie',
    '05' => 'Mai',
    '06' => 'Iunie',
    '07' => 'Iulie',
    '08' => 'August',
    '09' => 'Septembrie',
    '10' => 'Octombrie',
    '11' => 'Noiembrie',
    '12' => 'Decembrie'
];

$linkPag = $sitepath;

if ($_GET['tara']) {
    $tara = desfa_link($_GET['tara']);
    $id_tara = get_id_tara($tara);
    $linkPag = $linkPag . 'sejur-' . $_GET['tara'] . '/';

    if (!$_REQUEST['zone']) {
        $result = $db->fetchAll('select denumire, luni_plecari from tari where id_tara = :id', ['id' => $id_tara]);

        if (empty($result)) {
            die('no data');
        }

        $luni_plecari = explode(',', $row['luni_plecari']);
    }
}
if ($_REQUEST['zone']) {
    $zona = desfa_link($_REQUEST['zone']);
    $id_zona = get_id_zona($zona, $id_tara);
    $linkPag = $linkPag . $_GET['zone'] . '/';
    if (!$_REQUEST['oras']) {
        $sel = "select denumire, luni_plecari from zone where id_zona = '" . $id_zona . "' ";
        $que = mysql_query($sel) or die(mysql_error());
        $row = mysql_fetch_array($que);
        $luni_plecari = explode(',', $row['luni_plecari']);
    }
}
if ($_REQUEST['oras']) {
    $oras = desfa_link($_REQUEST['oras']);
    $id_localitate = get_id_localitate($oras, $id_zona);
    $linkPag = $linkPag . $_GET['oras'] . '/';
    $sel = "select denumire, luni_plecari from localitati where id_localitate = '" . $id_localitate . "' ";
    $que = mysql_query($sel) or die(mysql_error());
    $row = mysql_fetch_array($que);
    $luni_plecari = explode(',', $row['luni_plecari']);
}
if ($_GET['transport']) {
    $transport = desfa_link($_GET['transport']);
    $id_transport = get_id_transport($transport);
    $linkPag = $linkPag . '?optiuni=da&transport=' . $_GET['transport'];
}
if ($_GET['oras_plecare_avion']) {
    $plecare = desfa_link($_GET['oras_plecare_avion']);
    $id_loc_plecare_av = get_id_localitate($plecare, '');
    $linkPag = $linkPag . '&plecare-avion=' . $_GET['oras_plecare_avion'];
}
if ($_GET['stele']) {
    $stele = $_GET['stele'];
    $linkPag = $linkPag . '&stele=' . $_GET['stele'];
}
if ($_GET['masa']) {
    $masa = desfa_link($_GET['masa']);
    $linkPag = $linkPag . '&masa=' . $_GET['masa'];
}
?>

<div class="form-group col-md-3">
    <label class="white">Țara:</label>
    <div id="tara-placeholder" class="form-control input-replace">
        <span class="placeholder">Alege țara</span>
    </div>
    <input id="tara" type="hidden" name="tara"/>
</div>

<div class="form-group col-md-3">
    <label class="white">Destinația:</label>
    <div id="localitate-placeholder" class="form-control input-replace">
        <span class="placeholder">Alege destinația</span>
    </div>
    <input id="localitate" type="hidden" name="localitate"/>
</div>
<?php /*
<select name="localitate" id="localitate" onchange="af_filtru(1);" class="">
    <option value="">- Alegeti destinatia -</option>
	<?php if ( ! $id_tara ) { ?>
        <option value="" disabled="disabled">Selectati tara intai</option><?php } ?>
	<?php if ( $id_tara ) {
		$zone = get_zone( '', '', '', $id_tara, '' );
		if ( sizeof( $zone ) > 0 ) {
			foreach ( $zone as $id_zona1 => $value ) {
				$link_z = fa_link( $value ); ?>
                <option value="<?php echo '&zone=' . $link_z; ?>" <?php if ( $link_z == $_GET['zone'] ) { ?> selected="selected" <?php } ?>><?php echo strtoupper( $value ); ?></option>
				<?php $loc_f = get_localitate( '', '', $id_zona1, $id_tara, '' );
				if ( sizeof( $loc_f ) > 0 ) {
					foreach ( $loc_f as $kloc_tf => $loc_tf ) {
						$link_lo = fa_link( $loc_tf['denumire'] ); ?>
                        <option value="<?php echo '&zone=' . $link_z . '&oras=' . $link_lo; ?>" <?php if ( $link_lo == $_GET['oras'] ) { ?> selected="selected" <?php } ?>>
                            &nbsp;&nbsp;&nbsp;<?php echo $loc_tf['denumire']; ?></option>
					<?php }
				}
			}
		}
	} ?>
</select> */ ?>

<div class="form-group col-md-2">
    <label class="white">Transport:</label>
    <div id="transport-placeholder" class="form-control input-replace">
        <span class="placeholder">Alege transport</span>
    </div>
    <input id="transport" type="hidden" name="transport"/>
</div>
<?php $selTr = "
  select
  transport.denumire
  from
  oferte
  inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
  inner join localitati on hoteluri.locatie_id = localitati.id_localitate
  inner join zone on localitati.id_zona = zone.id_zona
  inner join transport on oferte.id_transport = transport.id_trans ";
/*if($luni_plecariF)  $selTr=$selTr." inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and ((oferte.tip_preturi = 'plecari' and date_format(data_pret_oferta.data_start, '%m-%Y')  = '".$luni_plecariF."') or (oferte.tip_preturi = 'perioade' and data_pret_oferta.data_start <='".$st."' and data_pret_oferta.data_end >= '".$sf."'))) ";*/
$selTr = $selTr . " where
  oferte.valabila = 'da'
  and hoteluri.tip_unitate <> 'Circuit'
  and zone.id_tara = '" . $id_tara . "' ";
if ($id_zona) {
    $selTr = $selTr . " and zone.id_zona = '" . $id_zona . "' ";
}
if ($id_localitate) {
    $selTr = $selTr . " and localitati.id_localitate = '" . $id_localitate . "' ";
}
if ($stele) {
    $selTr = $selTr . " and hoteluri.stele = '" . $stele . "' ";
}
$selTr = $selTr . "Group by transport.denumire
  Order by transport.denumire ";
$queTr = mysql_query($selTr) or die(mysql_error()); ?>
<?php /*
<select name="transport" id="transport" onchange="af_filtru(2);" class="">
    <option value="" <?php if ( ! $id_transport ) { ?> selected="selected" <?php } ?>>- Alegeti tipul transportului -
    </option>
	<?php if ( ! $id_tara ) { ?>
        <option value="" disabled="disabled">Selectati tara intai</option><?php } ?>
	<?php $i = 0;
	while ( $value = mysql_fetch_array( $queTr ) ) {
		$i ++;
		$lingTr = fa_link( $value['denumire'] ); ?>
        <option value="<?php echo '?&transport=' . $lingTr; ?>" <?php if ( $lingTr == $_GET['transport'] ) { ?> selected="selected" <?php } ?>><?php echo strtoupper( $value['denumire'] ); ?></option>
		<?php if ( $value['denumire'] == 'Avion' ) {
			$selFTA = "select loc_plecare.denumire
    from
    oferte
    inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
    inner join localitati on hoteluri.locatie_id = localitati.id_localitate
    inner join zone on localitati.id_zona = zone.id_zona
    inner join oferte_transport_avion on oferte.id_oferta = oferte_transport_avion.id_oferta
    inner join aeroport on oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
    inner join localitati as loc_plecare on aeroport.id_localitate = loc_plecare.id_localitate ";
			/* if($luni_plecariF)  $selFTA=$selFTA." inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and ((oferte.tip_preturi = 'plecari' and date_format(data_pret_oferta.data_start, '%m-%Y')  = '".$luni_plecariF."') or (oferte.tip_preturi = 'perioade' and data_pret_oferta.data_start <='".$st."' and data_pret_oferta.data_end >= '".$sf."'))) ";*/
/*			$selFTA = $selFTA . " where
    oferte.valabila = 'da'
    and hoteluri.tip_unitate <> 'Circuit'
    and zone.id_tara = '" . $id_tara . "'
    and oferte_transport_avion.tip = 'dus' ";
			if ( $id_zona ) {
				$selFTA = $selFTA . " and zone.id_zona = '" . $id_zona . "' ";
			}
			if ( $id_localitate ) {
				$selFTA = $selFTA . " and localitati.id_localitate = '" . $id_localitate . "' ";
			}
			if ( $stele ) {
				$selFTA = $selFTA . " and hoteluri.stele = '" . $stele . "' ";
			}
			$selFTA = $selFTA . " Group by loc_plecare.denumire Order by loc_plecare.denumire ";
			$queFTA = mysql_query( $selFTA ) or die( mysql_error() ); ?>
            <optgroup label="Orasul plecarii cu avionul">
				<?php while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$lingTr = fa_link( $loc_FTA['denumire'] ); ?>
                    <option value="<?php echo '&transport=avion&oras_plecare_avion=' . $lingTr; ?>" <?php if ( $lingTr == $_GET['oras_plecare_avion'] || ( $transport == 'avion' && mysql_num_rows( $queFTA ) == 1 ) ) { ?> selected="selected" <?php } ?>>
                        &nbsp;&nbsp;&nbsp;<?php echo $loc_FTA['denumire']; ?></option>
				<?php }
				@mysql_free_result( $queFTA ); ?></optgroup>
		<?php } ?>
	<?php }
	@mysql_free_result( $queTr ); ?>
</select> */ ?>

<div class="form-group col-md-2">
    <label class="white">Număr stele:</label>
    <div id="stele-placeholder" class="form-control input-replace">
        <span class="placeholder">Alege număr stele</span>
    </div>
    <input id="stele" type="hidden" name="stele"/>
</div>
<?php $selSt = "select
   hoteluri.stele
   from
   oferte
   inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
   inner join localitati on hoteluri.locatie_id = localitati.id_localitate
   inner join zone on localitati.id_zona = zone.id_zona
   inner join transport on oferte.id_transport = transport.id_trans ";
/*  if($luni_plecariF) $selSt=$selSt." inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and ((oferte.tip_preturi = 'plecari' and date_format(data_pret_oferta.data_start, '%m-%Y')  = '".$luni_plecariF."') or (oferte.tip_preturi = 'perioade' and data_pret_oferta.data_start <='".$st."' and data_pret_oferta.data_end >= '".$sf."'))) ";*/
if ($id_loc_plecare_av) {
    $selSt = $selSt . "inner Join oferte_transport_avion on oferte.id_oferta = oferte_transport_avion.id_oferta
inner Join aeroport on (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport and aeroport.id_localitate = '" . $id_loc_plecare_av . "') ";
}
$selSt = $selSt . " where
   oferte.valabila = 'da'
   and hoteluri.tip_unitate <> 'Circuit'
   and hoteluri.stele > '0'
   and zone.id_tara = '" . $id_tara . "' ";
if ($id_zona) {
    $selSt = $selSt . " and zone.id_zona = '" . $id_zona . "' ";
}
if ($id_localitate) {
    $selSt = $selSt . " and localitati.id_localitate = '" . $id_localitate . "' ";
}
$selSt = $selSt . "Group by hoteluri.stele
   Order by hoteluri.stele ";
$queSt = mysql_query($selSt) or die(mysql_error()); ?>
<?php /*
<select name="stele" id="stele" onchange="af_filtru(2);" class="">
    <option value="">- Selectati nr de stele -</option>
	<?php if ( ! $id_tara ) { ?>
        <option value="" disabled="disabled">Selectati tara intai</option><?php } ?>
	<?php while ( $nr_s = mysql_fetch_array( $queSt ) ) {
		$i = $nr_s['stele']; ?>
        <option value="<?php echo $i; ?>" <?php if ( $i == $stele ) { ?> selected="selected" <?php } ?>><?php echo $i;
			if ( $i > 1 ) {
				echo ' stele ';
			} else {
				echo ' stea ';
			} ?></option>
	<?php } ?>
</select> */ ?>
<input type="hidden" id="masa">

<div class="form-group col-md-2">
    <a href="<?php echo $linkPag; ?>" class="btn"
       onclick="ga('send', 'event', 'form cautare', 'motor cautare index', '<?php echo substr($sitepath, 0, -1) . $_SERVER['REQUEST_URI']; ?>');">
        Caută
    </a>
</div>

<div class="clear"></div>

<div class="col-md-12">
    <div class="advanced-search-options">
        <div class="tara-search search-box-wrapper" style="display: none;">
            <?php $tari_f = get_tari('', '', '', '', '', '', '', 'hoteluri'); ?>
            <?php if (sizeof($tari_f) > 0) : ?>
                <?php foreach ($tari_f as $key_tf => $value_tf) : ?>
                    <div class="option-column">
                        <div class="js-option option<?php echo desfa_link($value_tf) == $tara ? ' active' : '' ?>"
                             data-value="<?php echo fa_link($value_tf) ?>" data-type="tara"><?php echo $value_tf; ?></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="clear"></div>
        </div>
        <div class="localitate-search search-box-wrapper" style="display: none;">
            <?php if ($id_tara) : ?>
            <?php else : ?>
                <div class="option-column" style="width: 100%;">
                    <div class="option active" onclick="$('#tara-placeholder').trigger('click');">
                        Te rugam sa selectezi tara inainte
                    </div>
                </div>
                <div class="clear"></div>
            <?php endif; ?>
        </div>
        <div class="transport-search search-box-wrapper" style="display: none;">
            <?php if ($id_tara) : ?>
            <?php else : ?>
                <div class="option-column" style="width: 100%;">
                    <div class="option active" onclick="$('#tara-placeholder').trigger('click');">
                        Te rugam sa selectezi tara inainte
                    </div>
                </div>
                <div class="clear"></div>
            <?php endif; ?>
        </div>
        <div class="stele-search search-box-wrapper" style="display: none;">
            <?php if ($id_tara) : ?>
            <?php else : ?>
                <div class="option-column" style="width: 100%;">
                    <div class="option active" onclick="$('#tara-placeholder').trigger('click');">
                        Te rugam sa selectezi tara inainte
                    </div>
                </div>
                <div class="clear"></div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.input-replace').click(function () {
            if ($(this).hasClass('active')) {
                $('.input-replace').removeClass('active');
                $('.search-box-wrapper').hide();
            } else {
                var type = ($(this).attr('id')).replace('-placeholder', '');
                $('.input-replace').removeClass('active');
                $(this).addClass('active');
                $('.search-box-wrapper').hide();
                $('.' + type + '-search').show();
            }
        });

        $('.js-option').click(function () {
            var type = $(this).attr('data-type');
            var value = $(this).attr('data-value');
            var text = $(this).html();
            $('#' + type + '-placeholder').html(text);
            $('#' + type).val(value);
            af_filtru_responsive(1);
        });
    });
</script>

<div class="clear"></div>
