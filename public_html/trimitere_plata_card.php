<html>
<body>
<?php
	include("PlatiOnlineRo/clspo.php");
	include("PlatiOnlineRo/RSALib.php");
	$my_class = new PO3();

	$my_class->LoginID = $lid;
	$my_class->KeyEnc = $ke;
	$my_class->KeyMod = $km;

	$my_class->amount = "1.00";
	$my_class->currency = "RON";
	$my_class->OrderNumber = "1";
	$my_class->action = "2";
	$ret = $my_class->InsertHash_Auth();

	
	//Pt. Rate RZB
	/*
	$my_class->rate = "6";
	$my_class->action = "10";
	$ret = $my_class->InsertHash_AuthRate_RZB();
	*/

	//Pt. Rate BT
	/*
	$my_class->rate = "6";
	$my_class->action = "16";
	$ret = $my_class->InsertHash_AuthRate_RZB();
	*/
	
	$vOrderString= "<start_string>"; 
	$vOrderString.= "<item>";
	$vOrderString.= "<ProdID>1</ProdID>";
	$vOrderString.= "<qty>1</qty>";
	$vOrderString.= "<itemprice>0.81</itemprice>";
	$vOrderString.= "<name>Produs de test 1</name>";
	$vOrderString.= "<period></period><rec_id>0</rec_id>";
	$vOrderString.= "<description>Descriere produs</description>";
	$vOrderString.= "<pimg></pimg><rec_price>0</rec_price>";
	$vOrderString.= "<vat>0.19</vat>";
	$vOrderString.= "<lang_id></lang_id><stamp>".htmlspecialchars(date("F j, Y, g:i a")). "</stamp><on_stoc>1</on_stoc>";
	$vOrderString.= "<prodtype_id></prodtype_id><categ_id>0</categ_id><merchLoginID>0</merchLoginID>";
	$vOrderString.= "</item>";

	//cupon
	//$vOrderString .= "<coupon><key>cod</key><value>".abs(round(0.05,2))."</value><percent>1</percent><workingname>Nume cupon</workingname><type>0</type><scop>0</scop><vat>0</vat></coupon>";

	//shipping
	$vOrderString .= "<shipping><type>Denumire shipping</type><price>1.00</price><pimg></pimg><vat>0</vat></shipping>";
	$vOrderString .= "</start_string>";

?>

	<form id="registerForm" autocomplete="off" method="post" action="https://secure2.plationline.ro/">
		<?php echo $ret;?>
		<input type="hidden" name="f_login" value="<?php echo $my_class->LoginID;?>">
		<input type="hidden" name="f_show_form" value="0">
		<input type="hidden" name="f_amount" value="<?php echo $my_class->amount;?>">
		<input type="hidden" name="f_currency" value="<?php echo $my_class->currency;?>">
		<input type="hidden" name="f_order_number" value="<?php echo $my_class->OrderNumber;?>">
		<input type="hidden" name="F_Language" value="ro" >
		<input type="hidden" name="F_Lang" value="ro">
		<input type="hidden" name="f_order_string" value="<?php echo $vOrderString ?>">
		<input type="hidden" name="f_first_name" id="f_first_name" value="Prenume">
		<input type="hidden" name="f_last_name" id="f_last_name" value="Nume">
		<input type="hidden" name="f_cnp" value="-">
		<input type="hidden" name="f_address" id="f_address" value="Adresa facturare">
		<input type="hidden" name="f_city" id="f_city" value="Bucuresti">
		<input type="hidden" name="f_state" id="f_state" value="Bucuresti">
		<input type="hidden" name="f_zip" id="f_zip" value="800000">
		<input type="hidden" name="f_country" id="f_country" value="RO">
		<input type="hidden" name="f_phone" id="f_phone" value="0700000000">
		<input type="hidden" name="f_email" id="f_email" value="razvan@ocaziituristice.ro">
		<input type="hidden" name="f_company" value="-">
		<input type="hidden" name="f_reg_com" value="-">
		<input type="hidden" name="f_cui" value="-">
		<input type="hidden" name="f_ship_to_first_name" value="Prenume livrare" />
		<input type="hidden" name="f_ship_to_last_name" value="Nume livrare" />
		<input type="hidden" name="f_ship_to_phone" value="0700000001" />
		<input type="hidden" name="f_ship_to_address" value="Adresa livrare" />
		<input type="hidden" name="f_ship_to_city" value="Bucuresti" />
		<input type="hidden" name="f_ship_to_state" value="Bucuresti" />
		<input type="hidden" name="f_ship_to_zipcode" value="800001" />
		<input type="hidden" name="f_ship_to_country" value="RO" />

<!-- daca e test mode START here -->
		<input type="hidden" name="f_Test_Request" value="1">
<!-- daca e test mode END here -->
		<input type="submit" value="Plateste" />
	</form>
</body>
</html>