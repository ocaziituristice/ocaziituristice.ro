<?php
if ($_POST){
	$a = tez_xml('eximtur','test',$_POST['serviciu'],$_POST['param']);
	header("Content-type: text/xml; charset utf8");
	echo $a;exit;
}


function tez_xml($user,$parola,$serviciu,$parametri=null){

	$post['auth']['user'] = $user;
	$post['auth']['parola'] = $parola;

	$post['cerere']['serviciu'] = $serviciu;
	if (is_array($parametri)){
		foreach ($parametri as $k=>$v){
			$post['cerere']['param'][$k] = $v;
		}
	}



	$url = 'http://www.teztour.ro/xml/';
	$user_agent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30)";

	$ch = curl_init();    // initialize curl handle
	curl_setopt($ch, CURLOPT_URL, $url); // set url to post to

	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_POSTFIELDS,p($post));

	curl_setopt($ch, CURLOPT_FAILONERROR, 1);              // Fail on errors

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);    // allow redirects
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
	curl_setopt($ch, CURLOPT_PORT, 80);            //Set the port number
	curl_setopt($ch, CURLOPT_TIMEOUT, 10); // times out after 30 sec
	curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

	return curl_exec($ch);
}
function p($a,$cale=array()){
	$s='';
	if(is_array($a)){
		foreach ($a as $k=>$v){
			$nc=$cale;
			$nc[]=$k;
			$s.=p($v,$nc);
		}
	}
	else{
		$key=array_shift($cale);
		while($k=array_shift($cale)){
			$key.="[{$k}]";
		}
		$s="{$key}=".urlencode($a).'&';
	}
	return $s;
}
?>

<table cellpadding="3" border="1" cellspacing="0">
	<tr>
		<td width="200">Denumire serviciu</td>
		<td width="100">Serviciu</td>
		<td width="200">Parametrii</td>
		<td width="300">Observatii</td>
		<td width="100">Demo</td>
	</tr>
	<tr>
		<td>Lista tari</td>
		<td>getTari</td>
		<td>-</td>
		<td>Nu avem nevoie de nici un parametru</td>
		<td>
			<form action="" method="post">
			<input type="hidden" value="getTari" name="serviciu"/>
			<input type="submit" value="Demo" name="send">
			</form>
		</td>
	</tr>
	<tr>
		<td>Lista statiuni din tara x</td>
		<td>getStatiuni</td>
		<td><em>id_tara</em></td>
		<td>Necesar<br /> de forma Ex: array('id_tara'=>1)</td>
		<td>
			<form action="" method="post">
			<input type="hidden" value="getStatiuni" name="serviciu"/>
			Tara:<input type="text" name="param[id_tara]" value="" size="3"/><br />
			<input type="submit" value="Demo" name="send">
			</form>
		</td>
	</tr>
	<tr>
		<td>Lista de hoteluri din statiunea x</td>
		<td>getHoteluri</td>
		<td><em>id_statiune</em></td>
		<td>Necesar<br /> de forma Ex: array('id_statiune'=>1)</td>
		<td>
			<form action="" method="post">
			<input type="hidden" value="getHoteluri" name="serviciu"/>
			Statiunea:<input type="text" name="param[id_statiune]" value="" size="3"/><br />
			<input type="submit" value="Demo" name="send">
			</form>
		</td>
	</tr>
	<tr>
		<td>Informatii despre hotelul x</td>
		<td>getInfoHotel</td>
		<td><em>id_hotel</em></td>
		<td>Necesar<br /> de forma Ex: array('id_hotel'=>1)</td>
		<td>
			<form action="" method="post">
			<input type="hidden" value="getInfoHotel" name="serviciu"/>
			Hotel:<input type="text" name="param[id_hotel]" value="" size="3"/><br />
			<input type="submit" value="Demo" name="send">
			</form>
		</td>
	</tr>
	<tr>
		<td>Lista cu preturi din tara x</td>
		<td>getPreturi</td>
		<td><em>id_tara</em></td>
		<td>Necesar<br /> de forma Ex: array('id_tara'=>1)</td>
		<td>
			<form action="" method="post">
			<input type="hidden" value="getPreturi" name="serviciu"/>
			Tara:<input type="text" name="param[id_tara]" value="" size="3"/><br />
			<input type="submit" value="Demo" name="send">
			</form>
		</td>
	</tr>
	<tr>
		<td colspan="5">Apelare functie:<br /><?php highlight_string('<?php tez_xml($user,$parola,$serviciu,$parametri=null);?>');?></td>
	</tr>
</table>
