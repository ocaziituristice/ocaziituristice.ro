<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii_pt_afisare.php");
$partie = desfa_link($_REQUEST['partie']);
$sel_partii = "SELECT
partii.id,
partii.denumire,
partii.id_localitate,
partii.descriere,
partii.alt_min,
partii.alt_max,
partii.transport_pe_cablu,
partii.km_partie_incepatori,
partii.km_partie_avansati,
partii.km_partie_experimentati,
partii.km_partie_total,
partii.sezon,
partii.img_harta,
localitati.denumire AS localitatenume,
tari.denumire AS taranume
FROM
partii
LEFT JOIN localitati ON partii.id_localitate = localitati.id_localitate
LEFT JOIN zone ON localitati.id_zona = zone.id_zona
LEFT JOIN tari ON zone.id_tara = tari.id_tara
WHERE partii.denumire='$partie'";
$rez_partii = mysql_query($sel_partii) or die(mysql_error());
$detalii = @mysql_fetch_array($rez_partii);
@mysql_free_result($rez_partii);
if(!$detalii['id']) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();	
} ?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<?php $taraaa = ucwords(desfa_link($_REQUEST['tara'])); ?>
<title>Partie ski <?php echo $detalii['denumire']; ?> | Partii ski Austria | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Partie ski <?php echo $detalii['denumire']; ?> din Austria, detalii, clasificare si informatii utile despre Partia <?php echo $detalii['denumire']; ?>" />
<meta name="keywords" content="" />
<meta name="Publisher" content="ocaziituristice.ro" />
<meta name="robots" content="index,follow" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="http://www.ocaziituristice.ro/" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
  <table class="mainTableClass" border="0" cellspacing="0" cellpadding="0">
	<tr>
      <td colspan="2" align="left" valign="top">
        <div class="breadcrumb">
         <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
        </div>
      </td>
    </tr>
    <tr>
	  <td class="mainTableColumnLeft" align="left" valign="top">
      <div id="columnLeft">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/descopera/stanga_detalii_partii_ski.php"); ?>
      </div>
      </td>
	  <td class="mainTableColumnRight" align="left" valign="top">
        <div id="columnRight">
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta.php"); ?>
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_main_reclama.php"); ?>
        </div>
      </td>
	</tr>
  </table>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
