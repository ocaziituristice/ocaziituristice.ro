<?php //error_reporting(E_ALL);
//ini_set('display_errors', 1);?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$den_tara = 'Romania';
$id_tara = get_id_tara($den_tara);

$den_zona_litoral = 'Litoral';
$id_zona_litoral = get_id_zona($den_zona_litoral, $id_tara);

$den_zona_balneo = 'Statiuni Balneare';
$id_zona_balneo = get_id_zona($den_zona_balneo, $id_tara);

$link_tara = fa_link_oferta($den_tara);
$link_zona_litoral = fa_link_oferta($den_zona_litoral);

if(str_replace("/sejur-","",$_SERVER['REQUEST_URI'])==$link_tara."/") $img_path = 'images/';
	else $img_path = '/sejur-'.$link_tara.'/images/';
	

$sel_dest = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
count(distinct(oferte.id_hotel)) as numar_hoteluri,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
zone.denumire as denumire_zona,
localitati.info_scurte

FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = 1
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_loc'][] = $row_dest['denumire_localitate'];
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['link'][] = '/cazare-'.fa_link($row_dest['denumire_localitate']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona_litoral.'/'.$row_dest['id_localitate'].'/';
	$destinatie['id_loc'][] = $row_dest['id_localitate'];
	$destinatie['info_scurte'][] = $row_dest['info_scurte'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);

if($row_dest['numar']==1)
	{$destinatie['numar_hoteluri'][]=$row_dest['numar_hoteluri']." Hotel ";}
	else
	{ $destinatie['numar_hoteluri'][] = $row_dest['numar_hoteluri']." Hoteluri ";}
	
} @mysql_free_result($rez_dest);

?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cazare Romania 2018 | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Oferte de cazare in Romania la mare si in statiunile balneare" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/header/header_responsive.php"); ?>
</head>
<body>
 <header>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/breadcrumb_responsive.php"); ?>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/search_responsive.php"); ?>
    </div>
<script src="<?php echo PATH_JS?>/jquery-ui.min.js"></script>

<div class="layout">

    <div id="middle">

        <div id="middleInner">

            <div class="NEW-column-full">

                <div id="NEW-destinatie" class="clearfix">

                    <h1 class="blue" style="float:left;">Cazare Romania 2018 </h1>

                    <div class="float-right">

                        <?php /*?><div class="fb-share-button" data-href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/;src=sdkpreparse">Distribuie</a></div>
<?php */?>
                    </div>

                    <br class="clear" />



                        <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">

                                <div class="NEW-search-wide">

                                    <div class="chapter-title blue">Caută Romania:</div>

                                    <div id="afis_filtru"></div>

                                </div>

                            </div></div>

                        <br class="clear"><br>
                        

   <?php
   
  /* echo '<pre>';
        print_r($destinatie);
        echo  '</pre>';*/
   
    if(sizeof($destinatie['den_loc'])>0) { ?>
 <div id="destinations"  class="clearfix">
 
               <?php foreach($destinatie['den_loc'] as $key1 => $value1) { ?>

                        <div class="destItem NEW-round4px clearfix" >

                            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">

                                <img src="/images/localitati/cazare-<?php echo fa_link($destinatie['den_loc'][$key1]); ?>.jpg" alt="Cazare <?php echo $destinatie['den_loc'][$key1]; ?>">

                                <span class="over"></span>

                                <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>

                                <h2><?php echo $destinatie['den_loc'][$key1]; ?></h2>

                            </div>

                            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><div class="bottomBox clearfix">

                                <div class="price" align="center"><span class="red"><span class="pret"></span> </span> Oferte cazare la <?php echo $destinatie['numar_hoteluri'][$key1]?>  din <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte  <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?>  ">

			 <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?></a></span></span></div>

                            </div>

                        </div>
<?php }?>    

                    </div>

<?php }?>

                    <br class="clear"><br>

                    <div class="pad10 newText">

                         <img src="<?php echo $img_path; ?>icon-sejur-romania.jpg" alt="Sejur Romania" class="float-left" style="margin:3px 10px 0 0;">

                         <h2><strong>Sejur România</strong> - <strong>istorie, cultură, tratament, relaxare</strong></h2>

                       
           
<p class="text-justify">În spaţiul carpato-danubiano-pontic, o atracţie pentru turiştii de orice vârstă o reprezintă <em>sejur România</em>. Relieful caracterizat prin cele patru elemente: varietate, proporţionalitate, complementaritate şi dispunere simetrică cuprinde munţi, dealuri, podişuri şi câmpii, fluvii, râuri, lacuri, ape subterane şi ape marine.</p>
          <br class="clear">
          <p class="text-justify">Vegetaţia României a impresionat din toate timpurile turiştii ce au ajuns pe meleagurile acestei ţări. Plantele ocrotite catalogate drept monumente ale naturii se pot întâlni în cele trei mari zone de vegetaţie din România, zona alpină, zona de pădure şi zona de stepă alături de fauna cu excepţionalele exemplare de cerbi, urşi, capre negre, râşi, mistreţi, păsări şi fluturi.</p>
          <p class="text-justify">Turiştilor care vor să beneficieze de un sejur România li se deschid porţile unei oferte foarte variate atunci când vor să se bucure de relaxare, de linişte, de aer curat şi de răcoare.</p>
          <p class="text-justify">Zonele montane sunt împânzite de o mulţime de trasee turistice prin care se pot explora cele mai spectaculoase cascade, peşteri, vulcani noroioşi, lacuri cu ape cristaline, staţiuni montane perfecte pentru practicarea sporturilor de iarnă cu pârtii de schi dotate cu tunuri de zăpadă şi nocturnă.</p>
          <p class="text-justify">Staţiunile balneo montane, salinele, pădurile de fag şi brad, aerul pur şi bogat în aerosoli ce favorizează tratamentul alergiilor şi al altor afecţiuni ale aparatului respirator, îşi aşteaptă turiştii care vin la tratament.</p>
          <p class="text-justify">Oferta de <strong>cazare România</strong> cuprinde şi oraşele-cetăţi medievale, zonele în care agroturismul este în topul preferinţelor, litoralul sau delta şi numeroase alte puncte de interes, printre care şi podgoriile ce se laudă cu o tradiţie îndelungată.</p>
          <p class="text-justify">Istoria bogată prezentă în multitudinea de mânăstiri şi biserici cunoscute în întreaga lume, poate fi descoperită într-un sejur România.</p>
          <p class="text-justify">Litoralul Mării Negre este cea mai importantă zonă touristică a României, având în vedere numărul de turişti români şi străini care îl vizitează anual, cea mai bună variantă de <em>cazare România</em> pentru familiile cu sau fără copii, alături de una dintre cele mai sălbatice şi misterioase Delte ale lumii care îşi aşteaptă turiştii în fiecare an cu noi şi interesante dotări şi facilităţi de cazare şi distracţie.</p>
          <p class="text-justify">Turiştii pot încerca de la sporturile sau activităţile de agrement - tractări cu colaci sau banane gonflabile, plimbări cu hidrobicicleta sau schi-jet-ul, ridicări cu paraşuta, stand-up paddling, până la cele mai exclusiviste şi costisitoare - yahting, windsurfing, wakeboarding, kitesurfing, flyboarding, scufundări, care necesită o pregătire iniţială sau cursuri, oferite de cele câteva cluburi şi şcoli din staţiunile litorale.</p>
          <p class="text-justify">Cu toate aceste obiective turistice unice din ţara noastră, ne putem mândri în faţa Europei!</p>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    $("#afis_filtru").load("/includes/search/filtru.php?tara=romania");

</script>



<script src="/js/ajaxpage.js"></script>

<script src="/js/jquery.rating.js"></script>

<script>

    $('input.rating').rating({

        callback: function(value) {

            var dataString = 'rating=' + value;

            $.ajax({

                type: "POST",

                url: "/voting/////",

                data:dataString,

                success: function(data) {

                    $('#rating').html(data);

                }

            })

        },required:'hidden'

    });

</script>



<script src="/js/ocazii_main.js"></script>

<script>document.oncopy = addLink;</script>

<script src="/js/jquery.masonry.min.js"></script>

<script>

    $(window).load(function(){

        $('#destinations').masonry({

            itemSelector: '.destItem'

        });

    });

</script>













<script>

    $(window).bind("load", function() {

        $.getScript('/js/ocazii_lateload.js', function() {});

    });

</script>

<!-- Google Code for Remarketing Tag -->

<!--------------------------------------------------

Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup

--------------------------------------------------->









<script type="text/javascript">


    $(document).ready(function() {

// Tooltip only Text

        $('.masterTooltip').hover(function(){

            // Hover over code

            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<p class="tooltip"></p>')

                .text(title)

                .appendTo('body')

                .fadeIn('slow');

        }, function() {

            // Hover out code

            $(this).attr('title', $(this).data('tipText'));

            $('.tooltip').remove();

        }).mousemove(function(e) {

            var mousex = e.pageX + 20; //Get X coordinates

            var mousey = e.pageY + 10; //Get Y coordinates

            $('.tooltip')

                .css({ top: mousey, left: mousex })

        });



    });

    const mq = window.matchMedia( "(max-width: 768px)" );



    if(mq){



        $('.topBox').each(function(){

            // Cache event

            var existing_event = this.onclick;



            // Remove the event from the link

            this.onclick = null;



            // Add a check in for the class disabled

//            $(this).click(function(e){

//                $(this).addClass('active_img');

//

//                if($(this).hasClass('active_img')){

//                    e.stopImmediatePropagation();

//                    e.preventDefault();

//                }

//

//            });



            // Reattach your original onclick, but now in the correct order

            // if it was set in the first place

            if(existing_event) $(this).click(existing_event);

        });

    }





</script>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php"); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php"); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom_responsive.php"); ?>

