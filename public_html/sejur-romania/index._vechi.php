<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$den_tara = 'Romania';
$id_tara = get_id_tara($den_tara);

$den_zona_litoral = 'Litoral';
$id_zona_litoral = get_id_zona($den_zona_litoral, $id_tara);

$den_zona_balneo = 'Statiuni Balneare';
$id_zona_balneo = get_id_zona($den_zona_balneo, $id_tara);

$link_tara = fa_link_oferta($den_tara);
$link_zona_litoral = fa_link_oferta($den_zona_litoral);

if(str_replace("/sejur-","",$_SERVER['REQUEST_URI'])==$link_tara."/") $img_path = 'images/';
	else $img_path = '/sejur-'.$link_tara.'/images/';

/*$sel_loc_litoral = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
localitati.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND localitati.id_zona = '".$id_zona_litoral."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";*/

$sel_loc_litoral = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
count(distinct(oferte.id_hotel)) as numar_hoteluri,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
zone.denumire as denumire_zona,
localitati.info_scurte

FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = 1
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";


$rez_loc_litoral = mysql_query($sel_loc_litoral) or die(mysql_error());
while($row_loc_litoral = mysql_fetch_array($rez_loc_litoral)) {
	$destinatie_litoral['den_loc'][] = $row_loc_litoral['denumire_localitate'];
	$destinatie_litoral['link'][] = '/cazare-'.fa_link($row_loc_litoral['denumire_localitate']).'/';
	$destinatie_litoral['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona_litoral.'/'.$row_loc_litoral['id_localitate'].'/';
	$destinatie_litoral['id_loc'][] = $row_loc_litoral['id_localitate'];
	$destinatie_litoral['info_scurte'][] = $row_loc_litoral['info_scurte'];
	$destinatie_litoral['pret_minim'][] = round($row_loc_litoral['pret_minim']);
if($row_loc['numar_hoteluri']==1)
	{$destinatie_litotal['numar_hoteluri'][]=$row_loc_litoral['numar_hoteluri']." Hotel ".$row_loc_litoral['denumire_localitate'];}
	else
	{ $destinatie_litoral['numar_hoteluri'][] = $row_loc_litoral['numar_hoteluri']." Hoteluri ".$row_loc_litoral['denumire_localitate'];}
} @mysql_free_result($rez_loc_litoral);

$sel_loc_balneo = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
localitati.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND localitati.id_zona = '".$id_zona_balneo."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_loc_balneo = mysql_query($sel_loc_balneo) or die(mysql_error());
while($row_loc_balneo = mysql_fetch_array($rez_loc_balneo)) {
	$destinatie_balneo['den_loc'][] = $row_loc_balneo['denumire_localitate'];
	$destinatie_balneo['link'][] = '/sejur-romania/statiuni-balneare/'.fa_link($row_loc_balneo['denumire_localitate']).'/';
	$destinatie_balneo['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona_balneo.'/'.$row_loc_balneo['id_localitate'].'/';
	$destinatie_balneo['id_loc'][] = $row_loc_balneo['id_localitate'];
	$destinatie_balneo['info_scurte'][] = $row_loc_balneo['info_scurte'];
	$destinatie_balneo['pret_minim'][] = round($row_loc_balneo['pret_minim']);
} @mysql_free_result($rez_loc_balneo);

$sel_zone = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
zone.id_zona,
zone.denumire AS denumire_zona,
zone.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND oferte.last_minute = 'nu'
AND zone.id_tara = '".$id_tara."'
AND zone.id_zona <> '".$id_zona_litoral."' AND zone.id_zona <> '".$id_zona_balneo."'
GROUP BY denumire_zona
ORDER BY numar DESC, denumire_zona ASC ";
$rez_zone = mysql_query($sel_zone) or die(mysql_error());
while($row_zone = mysql_fetch_array($rez_zone)) {
	$destinatie['den_zona'][] = $row_zone['denumire_zona'];
	$destinatie['link'][] = '/sejur-romania/'.fa_link($row_zone['denumire_zona']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$row_zone['id_zona'].'/';
	$destinatie['id_zona'][] = $row_zone['id_zona'];
	$destinatie['info_scurte'][] = $row_zone['info_scurte'];
	$destinatie['pret_minim'][] = round($row_zone['pret_minim']);
//	$destinatie['numar_hoteluri'][] = round($row_zone['pret_minim']);
} @mysql_free_result($rez_zone);
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Sejur Romania | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Sejururi Romania, vacante la munte si la mare, sejururi pe litoral" />
<link rel="canonical" href="<?php echo '/sejur-'.$link_tara.'/'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Sejur Romania</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?>
        
        <br class="clear">
        <?php /*?><div class="Hline"></div>

        <br class="clear">
        
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
        
        <br class="clear"><?php */?>

		<?php $tipuri = get_tiputi_oferte('', '', '', $id_tara, '', '');
		if(sizeof($tipuri)>0) {
			$link_new = $link_tara.'/';
		?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/sejur-'.$link_new; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		foreach($tipuri as $key_t=>$value_t) {
			$i++;
			$valTip = fa_link($value_t['denumire']);
			if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire']; ?></a></li>
		<?php } ?>
        </ul>
		<?php } ?>

        <?php if(sizeof($destinatie_litoral['den_loc'])>0) { ?>
        <div class="ofsp-titlu green newText">Cazare Romania 2016-<?php echo date("Y")+1; ?></div>
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie_litoral['den_loc'] as $key1_litoral => $value1_litoral) { ?>
          <div class="destItem NEW-round4px clearfix">
            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie_litoral['link'][$key1_litoral]; ?>'">
              <img src="<?php echo $img_path; ?>cazare-<?php echo fa_link($destinatie_litoral['den_loc'][$key1_litoral]); ?>.jpg" alt="<?php echo $destinatie_litoral['den_loc'][$key1_litoral]; ?>" >
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie_litoral['den_loc'][$key1_litoral]; ?></h2>
            </div>
            <div class="bottomBox clearfix">
              <div class="price"><span class="pret"><?php echo $destinatie_litoral['numar_hoteluri'][$key1_litoral]; ?></span></div>
              <div class="details"><a href="<?php echo $destinatie_litoral['link'][$key1_litoral]; ?>" title="Cazare <?php echo $destinatie_litoral['den_loc'][$key1_litoral];?>">Cazare <?php echo $destinatie_litoral['den_loc'][$key1_litoral]?></a></div>
            </div>
          </div>
          <?php } ?>
        </div>
        <br>
        <?php } ?>
        
        <?php /*?><?php if(sizeof($destinatie_balneo['den_loc'])>0) { ?>
        <div class="ofsp-titlu green newText"><a href="/sejur-romania/statiuni-balneare/" title="Cazare Statiuni Balneare">Cazare Statiuni Balneare</a></div>
        <div class="pad0-10"><a href="/sejur-romania/statiuni-balneare/" title="Cazare Statiuni Balneare"><img src="<?php echo $img_path; ?>statiuni-balneare.jpg" alt="Statiuni Balneare" class="NEW-round6px"></a></div>
        <br><br>
        <?php } ?><?php */?>
        
        <br>
		
        <div class="pad10">
          <img src="<?php echo $img_path; ?>icon-sejur-romania.jpg" alt="Sejur Romania" class="float-left" style="margin:3px 10px 0 0;">
          <h2><strong>Sejur România</strong> - <strong>istorie, cultură, tratament, relaxare</strong></h2>
          <p class="newText text-justify">În spaţiul carpato-danubiano-pontic, o atracţie pentru turiştii de orice vârstă o reprezintă <em>sejur România</em>. Relieful caracterizat prin cele patru elemente: varietate, proporţionalitate, complementaritate şi dispunere simetrică cuprinde munţi, dealuri, podişuri şi câmpii, fluvii, râuri, lacuri, ape subterane şi ape marine.</p>
          <br class="clear">
          <p class="newText text-justify">Vegetaţia României a impresionat din toate timpurile turiştii ce au ajuns pe meleagurile acestei ţări. Plantele ocrotite catalogate drept monumente ale naturii se pot întâlni în cele trei mari zone de vegetaţie din România, zona alpină, zona de pădure şi zona de stepă alături de fauna cu excepţionalele exemplare de cerbi, urşi, capre negre, râşi, mistreţi, păsări şi fluturi.</p>
          <p class="newText text-justify">Turiştilor care vor să beneficieze de un sejur România li se deschid porţile unei oferte foarte variate atunci când vor să se bucure de relaxare, de linişte, de aer curat şi de răcoare.</p>
          <p class="newText text-justify">Zonele montane sunt împânzite de o mulţime de trasee turistice prin care se pot explora cele mai spectaculoase cascade, peşteri, vulcani noroioşi, lacuri cu ape cristaline, staţiuni montane perfecte pentru practicarea sporturilor de iarnă cu pârtii de schi dotate cu tunuri de zăpadă şi nocturnă.</p>
          <p class="newText text-justify">Staţiunile balneo montane, salinele, pădurile de fag şi brad, aerul pur şi bogat în aerosoli ce favorizează tratamentul alergiilor şi al altor afecţiuni ale aparatului respirator, îşi aşteaptă turiştii care vin la tratament.</p>
          <p class="newText text-justify">Oferta de <strong>cazare România</strong> cuprinde şi oraşele-cetăţi medievale, zonele în care agroturismul este în topul preferinţelor, litoralul sau delta şi numeroase alte puncte de interes, printre care şi podgoriile ce se laudă cu o tradiţie îndelungată.</p>
          <p class="newText text-justify">Istoria bogată prezentă în multitudinea de mânăstiri şi biserici cunoscute în întreaga lume, poate fi descoperită într-un sejur România.</p>
          <p class="newText text-justify">Litoralul Mării Negre este cea mai importantă zonă touristică a României, având în vedere numărul de turişti români şi străini care îl vizitează anual, cea mai bună variantă de <em>cazare România</em> pentru familiile cu sau fără copii, alături de una dintre cele mai sălbatice şi misterioase Delte ale lumii care îşi aşteaptă turiştii în fiecare an cu noi şi interesante dotări şi facilităţi de cazare şi distracţie.</p>
          <p class="newText text-justify">Turiştii pot încerca de la sporturile sau activităţile de agrement - tractări cu colaci sau banane gonflabile, plimbări cu hidrobicicleta sau schi-jet-ul, ridicări cu paraşuta, stand-up paddling, până la cele mai exclusiviste şi costisitoare - yahting, windsurfing, wakeboarding, kitesurfing, flyboarding, scufundări, care necesită o pregătire iniţială sau cursuri, oferite de cele câteva cluburi şi şcoli din staţiunile litorale.</p>
          <p class="newText text-justify">Cu toate aceste obiective turistice unice din ţara noastră, ne putem mândri în faţa Europei!</p>
        </div>

        <br class="clear">

 <?php /*?>       <?php if(sizeof($destinatie_litoral['den_loc'])>0) { ?>
        <h3 class="mar10">Cazare Romania 2014 - <?php echo date("Y")+1; ?></h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie_litoral['den_loc'] as $key2_litoral => $value2_litoral) { ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie_litoral['link'][$key2_litoral]; ?>" title="Cazare <?php echo $destinatie_litoral['den_loc'][$key2_litoral]; ?>" class="link-blue block">Cazare <?php echo $destinatie_litoral['den_loc'][$key2_litoral]; ?></a></li>
          <?php } ?>
        </ul>
        
        <br class="clear"><br>
        <?php } ?><?php */?>
        
        <?php /*?><?php if(sizeof($destinatie_balneo['den_loc'])>0) { ?>
        <h3 class="mar10">Staţiuni Balneare - cazări disponibile</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie_balneo['den_loc'] as $key2_balneo => $value2_balneo) { ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie_balneo['link'][$key2_balneo]; ?>" title="Cazare <?php echo $destinatie_balneo['den_loc'][$key2_balneo]; ?>" class="link-blue block">Cazare <?php echo $destinatie_balneo['den_loc'][$key2_balneo]; ?></a></li>
          <?php } ?>
        </ul>
        
        <br class="clear"><br>
        <?php } ?><?php */?>
        
      <?php /*?>  <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <h3 class="mar10">Alte destinaţii România</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie['den_zona'] as $key2 => $value2) { ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie['link'][$key2]; ?>" title="Cazare <?php echo $destinatie['den_zona'][$key2]; ?>" class="link-blue block">Cazare <?php echo $destinatie['den_zona'][$key2]; ?></a></li>
          <?php } ?>
        </ul>
                
        <br class="clear"><br>
        <?php } ?><?php */?>
        
        <br class="clear"><br><br>
        
        <div class="pad10"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?></div>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>