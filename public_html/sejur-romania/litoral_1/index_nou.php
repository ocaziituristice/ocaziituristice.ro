<?php //error_reporting(E_ALL);
//ini_set('display_errors', 1);?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$den_zona = 'Litoral';
$id_tara = 1;
$id_zona = get_id_zona($den_zona, $id_tara);
$den_tara = get_den_tara($id_tara);

$link_tara = fa_link_oferta($den_tara);
$link_zona = fa_link_oferta($den_zona);

$sel_loc = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
COUNT(distinct(oferte.id_hotel)) as numar_hoteluri,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
localitati.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND localitati.id_zona = '".$id_zona."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_loc = mysql_query($sel_loc) or die(mysql_error());
while($row_loc = mysql_fetch_array($rez_loc)) {
	$destinatie['den_loc'][] = $row_loc['denumire_localitate'];
	$destinatie['link'][] = '/cazare-'.fa_link($row_loc['denumire_localitate']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona.'/'.$row_loc['id_localitate'].'/';
	$destinatie['id_loc'][] = $row_loc['id_localitate'];
	$destinatie['info_scurte'][] = $row_loc['info_scurte'];
	$destinatie['pret_minim'][] = round($row_loc['pret_minim']);
	$destinatie['numar'][] = $row_loc['numar_hoteluri'];
	$destinatie['den_zona'][] = $den_zona;
} @mysql_free_result($rez_dest);

?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cazare Litoral Romania 2018 | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Oferte litoral 2018, oferte vacante pe litoral, cazare la hotelurile din Mamaia, Neptun, Jupiter, Sarturn,Venus, Eforie Nord" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/header/header_responsive.php"); ?>
</head>
<body>
 <header>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/breadcrumb_responsive.php"); ?>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/search_responsive_gol.php"); ?>
    </div>
<script src="<?php echo PATH_JS?>/jquery-ui.min.js"></script>

<div class="layout">

    <div id="middle">

        <div id="middleInner">

            <div class="NEW-column-full">

                <div id="NEW-destinatie" class="clearfix">

                    <h1 class="blue" style="float:left;">Cazare Litoral Romania 2018 </h1>

                    <div class="float-right">

                        <?php /*?><div class="fb-share-button" data-href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/;src=sdkpreparse">Distribuie</a></div>
<?php */?>
                    </div>

                    <br class="clear" />



                        <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">

                                <div class="NEW-search-wide">

                                    <div class="chapter-title blue">Caută Oferte Litoral Romania:</div>

                                    <div id="afis_filtru"></div>

                                </div>

                            </div></div>

                        <br class="clear"><br>
                        

   <?php
   
  /* echo '<pre>';
        print_r($destinatie);
        echo  '</pre>';*/
   
    if(sizeof($destinatie['den_loc'])>0) { ?>
 <div id="destinations"  class="clearfix">
 
               <?php foreach($destinatie['den_loc'] as $key1 => $value1) { ?>

                        <div class="destItem NEW-round4px clearfix" >

                            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">

                                <img src="/images/localitati/cazare-<?php echo fa_link($destinatie['den_loc'][$key1]); ?>.jpg" alt="Cazare <?php echo $destinatie['den_loc'][$key1]; ?>">

                                <span class="over"></span>

                                <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>

                                <h2><?php echo $destinatie['den_loc'][$key1]; ?></h2>

                            </div>

                            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><div class="bottomBox clearfix">

                                <div class="price" align="center"><span class="red"><span class="pret"></span> </span> Oferte cazare la <?php echo $destinatie['numar'][$key1]?> hoteluri din <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte  <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?>  ">

			 <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?></a></span></span></div>

                            </div>

                        </div>
<?php }?>    

                    </div>

<?php }?>

                    <br class="clear"><br>

                    <div class="pad10 newText">

                        <img src="/sejur-romania/litoral/images/icon-litoral-romania.jpg" alt="Cazare Litoral" class="float-left" style="margin:3px 10px 0 0;">

                        <h2><strong>Cazare Litoral</strong> - o vacanţă plăcută la Marea Neagră</h2>

                       
           
<p class="text-justify">Litoralul românesc rămâne în continuare un punct de atracţie important pentru perioada verii, fiind cea mai importantă zonă turistică a României.</p>
          <br class="clear">
          <p class="text-justify">Revenind pe culmile gloriei de altădată, ofertele de <em>cazare litoral</em> asigură îndrăgostiţilor, tinerilor căsătoriţi sau chiar familiilor cu copii, o minunată vacanţă la malul Mării Negre pe plajele cu nisip fin şi auriu. Umbrele cochete, şezlonguri, terase amenajate cu mobilier nou şi modern, oferă o vacanţă plăcută turiştilor care vor să se bucure de apă, soare şi multă distracţie.</p>
          <p class="text-justify">Mâncăruri româneşti şi internaţionale, cazare litoral în camere confortabile şi elegante, restaurante care se impun prin calitatea serviciilor oferite, reprezintă alegerea perfectă pentru relaxare în verile lungi şi călduroase.</p>
          <p class="text-justify">Pornind din nord, de la graniţa cu Ucraina, până în sud, la graniţa cu Bulgaria, litoralul românesc oferă peisaje uimitoare pe o lungime de peste 100 km. Plajele întinse şi însorite, oportunităţile de cazare şi posibilităţile de agrement fac litoralul românesc atractiv. Salba de staţiuni de pe ţărmul Mării Negre se întrece în <strong>oferte litoral</strong> pentru turiştii români şi străini.</p>
          <p class="text-justify">Există o mulţime de oportunităţi de distracţie şi agrement care acoperă toate preferinţele şi exigenţele: baruri, discoteci, cazinouri, dar şi turism balnear în bazele de tratament cu nămoluri terapeutice sau tratamente SPA în staţiunile special destinate.</p>
          <p class="text-justify">De asemenea, litoralul românesc oferă posibilităţi de practicare a tuturor sporturilor acvatice, fiecare staţiune dispunând de baze de agrement moderne. Opţiunile sunt completate de terenuri de tenis, volei, baschet, fotbal şi golf, parcuri de distracţie şi centre de divertisment.</p>
          <p class="text-justify">De mare succes este programul “<a href="/oferte-litoralul-pentru-toti/romania/" class="link-blue" title="Oferte Litoralul pentru Toti">Litoralul pentru toţi</a>” pentru perioada de extrasezon, cu pachete speciale şi oferte litoral ieftine, în condiţiile în care nicio altă destinaţie externă nu vine cu o ofertă atât de avantajoasă. Se asigură un confort sporit, iar tarifele sunt foarte avantajoase în raport cu dotările şi facilităţile oferite. Cazare litoral cu preţuri accesibile pentru orice buzunar, reprezintă magnetul pentru toţi turiştii dornici de un concediu estival.</p>
          <p class="text-justify">Pitorescul litoralului românesc este întregit de muzee, monumente istorice de mare însemnătate, ruine antice şi rezervaţii naturale unice în ţară şi în Europa. Peisaje uimitoare, transformări anuale spectaculoase ale staţiunilor, obiective şi atracţii turistice care merită vizitate, toate reprezintă chemarea litoralului românesc. </p>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    $("#afis_filtru").load("/includes/search/filtru.php?tara=romania&zone=litoral");

</script>



<script src="/js/ajaxpage.js"></script>

<script src="/js/jquery.rating.js"></script>

<script>

    $('input.rating').rating({

        callback: function(value) {

            var dataString = 'rating=' + value;

            $.ajax({

                type: "POST",

                url: "/voting/////",

                data:dataString,

                success: function(data) {

                    $('#rating').html(data);

                }

            })

        },required:'hidden'

    });

</script>



<script src="/js/ocazii_main.js"></script>

<script>document.oncopy = addLink;</script>








<script>

    $().ready(function() {

        $("#data_start").datepicker({

            numberOfMonths: 3,

            dateFormat: "yy-mm-dd",

            showButtonPanel: true

        });

        $("#data_end").datepicker({

            numberOfMonths: 3,

            dateFormat: "yy-mm-dd",

            showButtonPanel: true

        });

    });

</script>



<!--<script src="/js/jquery.masonry.min.js"></script>-->

<script>

    $(window).load(function(){

        $('#destinations').masonry({

            itemSelector: '.destItem'

        });

    });

</script>













<script>

    $(window).bind("load", function() {

        $.getScript('/js/ocazii_lateload.js', function() {});

    });

</script>

<!-- Google Code for Remarketing Tag -->

<!--------------------------------------------------

Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup

--------------------------------------------------->









<script type="text/javascript">


    $(document).ready(function() {

// Tooltip only Text

        $('.masterTooltip').hover(function(){

            // Hover over code

            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<p class="tooltip"></p>')

                .text(title)

                .appendTo('body')

                .fadeIn('slow');

        }, function() {

            // Hover out code

            $(this).attr('title', $(this).data('tipText'));

            $('.tooltip').remove();

        }).mousemove(function(e) {

            var mousex = e.pageX + 20; //Get X coordinates

            var mousey = e.pageY + 10; //Get Y coordinates

            $('.tooltip')

                .css({ top: mousey, left: mousex })

        });



    });

    const mq = window.matchMedia( "(max-width: 768px)" );



    if(mq){



        $('.topBox').each(function(){

            // Cache event

            var existing_event = this.onclick;



            // Remove the event from the link

            this.onclick = null;



            // Add a check in for the class disabled

//            $(this).click(function(e){

//                $(this).addClass('active_img');

//

//                if($(this).hasClass('active_img')){

//                    e.stopImmediatePropagation();

//                    e.preventDefault();

//                }

//

//            });



            // Reattach your original onclick, but now in the correct order

            // if it was set in the first place

            if(existing_event) $(this).click(existing_event);

        });

    }





</script>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php"); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php"); ?>

