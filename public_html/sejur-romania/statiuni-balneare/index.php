<?php //error_reporting(E_ALL);
//ini_set('display_errors', 1);?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$den_zona = 'statiuni balneare';
$id_tara = 1;
$id_zona = get_id_zona($den_zona, $id_tara);
$den_tara = get_den_tara($id_tara);

$link_tara = fa_link_oferta($den_tara);
$link_zona = fa_link_oferta($den_zona);

$sel_loc = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
COUNT(distinct(oferte.id_hotel)) as numar_hoteluri,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
localitati.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND localitati.id_zona = '".$id_zona."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_loc = mysql_query($sel_loc) or die(mysql_error());
while($row_loc = mysql_fetch_array($rez_loc)) {
	$destinatie['den_loc'][] = $row_loc['denumire_localitate'];
	$destinatie['link'][] = '/cazare-'.fa_link($row_loc['denumire_localitate']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona.'/'.$row_loc['id_localitate'].'/';
	$destinatie['id_loc'][] = $row_loc['id_localitate'];
	$destinatie['info_scurte'][] = $row_loc['info_scurte'];
	$destinatie['pret_minim'][] = round($row_loc['pret_minim']);
	$destinatie['numar'][] = $row_loc['numar_hoteluri'];
	$destinatie['den_zona'][] = $den_zona;
} @mysql_free_result($rez_dest);

?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Statiuni balneare, oferte cazare si tratament | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Statiuni balneare din Romania, oferte pentru cazare si tratament in statiunile balneoclimaterice" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/header/header_responsive.php"); ?>
</head>
<body>
 <header>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/breadcrumb_responsive.php"); ?>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/search_responsive.php"); ?>
    </div>
<script src="<?php echo PATH_JS?>/jquery-ui.min.js"></script>

<div class="layout">

    <div id="middle">

        <div id="middleInner">

            <div class="NEW-column-full">

                <div id="NEW-destinatie" class="clearfix">

                    <h1 class="blue" style="float:left;">Staţiuni Balneare, cazare şi tratament </h1>

                    <div class="float-right">

                        <?php /*?><div class="fb-share-button" data-href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/;src=sdkpreparse">Distribuie</a></div>
<?php */?>
                    </div>

                    <br class="clear" />



                        <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">

                                <div class="NEW-search-wide">

                                    <div class="chapter-title blue">Caută Oferte in Staţiuni Balneare:</div>

                                    <div id="afis_filtru"></div>

                                </div>

                            </div></div>

                        <span class="clear"></span>
                        

 
   	<?php $tipuri = get_tiputi_oferte('', '', $id_zona, $id_tara, '', '');
		if(sizeof($tipuri)>0) {
			$link_new = $link_tara.'/'.$link_zona.'/';
		?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/sejur-'.$link_new; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		foreach($tipuri as $key_t=>$value_t) {
			$i++;
			$valTip = fa_link($value_t['denumire']);
			if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire']; ?></a></li>
		<?php } ?>
        </ul>
		<?php } 
   
  /* echo '<pre>';
        print_r($destinatie);
        echo  '</pre>';*/
   
    if(sizeof($destinatie['den_loc'])>0) { ?>
 <div id="destinations"  class="clearfix">
 
               <?php foreach($destinatie['den_loc'] as $key1 => $value1) { ?>

                        <div class="destItem NEW-round4px clearfix" >

                            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">

                                <img src="/images/localitati/cazare-<?php echo fa_link($destinatie['den_loc'][$key1]); ?>.jpg" alt="Cazare <?php echo $destinatie['den_loc'][$key1]; ?>">

                                <span class="over"></span>

                                <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>

                                <h2><?php echo $destinatie['den_loc'][$key1]; ?></h2>

                            </div>

                            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><div class="bottomBox clearfix">

                                <div class="price" align="center"><span class="red"><span class="pret"></span> </span> Oferte cazare la <?php echo $destinatie['numar'][$key1]?> hoteluri din <br /><span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte  Cazare si Tramanet <?php echo $destinatie['den_loc'][$key1]; ?>  ">

			 <?php echo $destinatie['den_loc'][$key1]?></a></span></span></div>

                            </div>

                        </div>
<?php }?>    

                    </div>

<?php }?>

                    <br class="clear"><br>

                    <div class="pad10 newText">

          <img src="/sejur-romania/statiuni-balneare/images/icon-statiuni-balneare.jpg" alt="Cazare Statiuni Balneare" class="float-left" style="margin:3px 10px 0 0;">

                        <h2>Concediul în <strong>staţiuni balneare</strong>, pentru toate vârstele</h2>

                       
           
 <p class="text-justify">Concediul în <em>staţiuni balneare</em> nu trebuie să fie o opţiune doar pentru pensionari. De excepţionalele efecte asupra sănătăţii pe care vacanţele petrecute în astfel de locaţii le au asupra sănătăţii pot profita persoanele de toate vârstele, iar pachetele de oferte moderne se concentrează în mod special pe acest aspect urmărind să atragă tinerii şi familiile către aceste zone cu un potenţial excepţional de care România nu duce deloc lipsă.</p>
          
          <br class="clear">
          
          <p class="text-justify">Un concediu de tratament staţiuni balneare nu este doar o excelentă ocazie de relaxare şi recuperare pentru persoanele care suferă de diverse afecţiuni, dar şi o opţiune de buget pentru familiile acestora care se pot relaxa în zonele pitoreşti în care sunt aşezate cele mai multe dintre aceste staţiuni. Pe timp de vară, acestea pot înlocui cu succes o vacanţă mult mai scumpă pe litoral. Avantajul este acela că staţiunile de acest tip sunt mult mai liniştite, mai puţin aglomerate, iar ofertele de cazare la hotel au preţuri promoţionale prin diverse programe guvernamentale.</p>
          <h3 class="text-justify mar0 pad0">Staţiuni balneare - sănătate şi relaxare</h3>
          <br>
          <p class="text-justify">Staţiunile balneare din România sunt cunoscute pentru marea diversitate de afecţiuni pe care acestea le tratează. În staţiunile ce sunt fie de curând modernizate fie în curs de modernizare, cu echipamente medicale de ultimă generaţie şi oferte de cazare în condiţii cu adevărat excepţionale, efectul terapeutic al apei este combinat.</p>
          <p class="text-justify">În <strong>staţiuni balneoclimaterice</strong> se tratează un număr mare de afecţiuni prin tehnici precum băile în ape minerale cu sulf, calciu sau magneziu, băi termale şi împachetări cu nămol. Tehnicile dezvoltate aici de sute de ani, combinate cu tehnologia modernă pot trata sau amelioara numeroase probleme de sănătate, de la alergii la tratamente regenerative, reumatism, infertilitate sau alte probleme ginecologice, toate acestea într-un peisaj idilic de care se poate bucura tot restul familiei care nu participă la tratament, cu ajutorul unor activităţi precum înotul în piscină, tenisul sau plimbările lungi într-un cadru liniştit şi relaxant.</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    $("#afis_filtru").load("/includes/search/filtru.php?tara=romania&zone=statiuni-balneare");

</script>



<script src="/js/ajaxpage.js"></script>

<script src="/js/jquery.rating.js"></script>

<script>

    $('input.rating').rating({

        callback: function(value) {

            var dataString = 'rating=' + value;

            $.ajax({

                type: "POST",

                url: "/voting/////",

                data:dataString,

                success: function(data) {

                    $('#rating').html(data);

                }

            })

        },required:'hidden'

    });

</script>



<script src="/js/ocazii_main.js"></script>

<script>document.oncopy = addLink;</script>

<!--<script src="/js/jquery.masonry.min.js"></script>-->
<script>

    $(window).bind("load", function() {

        $.getScript('/js/ocazii_lateload.js', function() {});

    });

</script>

<!-- Google Code for Remarketing Tag -->

<!--------------------------------------------------

Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup

--------------------------------------------------->









<script type="text/javascript">


    $(document).ready(function() {

// Tooltip only Text

        $('.masterTooltip').hover(function(){

            // Hover over code

            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<p class="tooltip"></p>')

                .text(title)

                .appendTo('body')

                .fadeIn('slow');

        }, function() {

            // Hover out code

            $(this).attr('title', $(this).data('tipText'));

            $('.tooltip').remove();

        }).mousemove(function(e) {

            var mousex = e.pageX + 20; //Get X coordinates

            var mousey = e.pageY + 10; //Get Y coordinates

            $('.tooltip')

                .css({ top: mousey, left: mousex })

        });



    });

    const mq = window.matchMedia( "(max-width: 768px)" );



    if(mq){



        $('.topBox').each(function(){

            // Cache event

            var existing_event = this.onclick;



            // Remove the event from the link

            this.onclick = null;



            // Add a check in for the class disabled

//            $(this).click(function(e){

//                $(this).addClass('active_img');

//

//                if($(this).hasClass('active_img')){

//                    e.stopImmediatePropagation();

//                    e.preventDefault();

//                }

//

//            });



            // Reattach your original onclick, but now in the correct order

            // if it was set in the first place

            if(existing_event) $(this).click(existing_event);

        });

    }





</script>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php"); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php"); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom_responsive.php"); ?>

