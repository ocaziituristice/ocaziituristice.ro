<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$den_zona = 'Statiuni Balneare';
$id_tara = get_id_tara_by_zona($den_zona);
$id_zona = get_id_zona($den_zona, $id_tara);
$den_tara = get_den_tara($id_tara);

$link_tara = fa_link_oferta($den_tara);
$link_zona = fa_link_oferta($den_zona);

$sel_loc = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
count(distinct(oferte.id_hotel)) as numar_hoteluri,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
localitati.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND localitati.id_zona = '".$id_zona."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_loc = mysql_query($sel_loc) or die(mysql_error());
while($row_loc = mysql_fetch_array($rez_loc)) {
	$destinatie['den_loc'][] = $row_loc['denumire_localitate'];
	$destinatie['link'][] = '/cazare-'.fa_link($row_loc['denumire_localitate']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona.'/'.$row_loc['id_localitate'].'/';
	$destinatie['id_loc'][] = $row_loc['id_localitate'];
	if($row_loc['numar_hoteluri']==1)
	{$destinatie['numar_hoteluri'][]=$row_loc['numar_hoteluri']." Hotel ".$row_loc['denumire_localitate'];}
	else
	{ $destinatie['numar_hoteluri'][] = $row_loc['numar_hoteluri']." Hoteluri ".$row_loc['denumire_localitate'];}
	$destinatie['info_scurte'][] = $row_loc['info_scurte'];
	$destinatie['pret_minim'][] = round($row_loc['pret_minim']);
} @mysql_free_result($rez_loc);
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Statiuni balneare, oferte cazare si tratament | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Statiuni balneare din Romania, oferte pentru cazare si tratament in statiunile balneoclimaterice" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Staţiuni Balneare, cazare şi tratament</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?>

        <br class="clear">
        <?php /*?><div class="Hline"></div>

        <br class="clear">
        
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
        
        <br class="clear"><?php */?>

		<?php $tipuri = get_tiputi_oferte('', '', $id_zona, $id_tara, '', '');
		if(sizeof($tipuri)>0) {
			$link_new = $link_tara.'/'.$link_zona.'/';
		?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/sejur-'.$link_new; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		foreach($tipuri as $key_t=>$value_t) {
			$i++;
			$valTip = fa_link($value_t['denumire']);
			if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire']; ?></a></li>
		<?php } ?>
        </ul>
		<?php } ?>

        <?php if(sizeof($destinatie['den_loc'])>0) { ?>
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie['den_loc'] as $key1 => $value1) { ?>
          <div class="destItem NEW-round4px clearfix" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
            <div class="topBox">
              <img src="/sejur-romania/statiuni-balneare/images/statiuni-balneare-<?php echo fa_link($destinatie['den_loc'][$key1]); ?>.jpg" alt="<?php echo $destinatie['den_loc'][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie['den_loc'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?><div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><?php } ?>
            <div class="bottomBox clearfix">
              <div class="price"><span class="pret"><?php echo $destinatie['numar_hoteluri'][$key1]; ?></div>
              <div class="details"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Cazare <?php echo $destinatie['den_loc'][$key1]; ?>">Cazare <?php echo $destinatie['den_loc'][$key1]; ?></a></div>
            </div>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        
        <br>
		
        <div class="pad10">
          <img src="/sejur-romania/statiuni-balneare/images/icon-statiuni-balneare.jpg" alt="Cazare Statiuni Balneare" class="float-left" style="margin:3px 10px 0 0;">
          <h2>Concediul în <strong>staţiuni balneare</strong>, pentru toate vârstele</h2>
          <p class="newText text-justify">Concediul în <em>staţiuni balneare</em> nu trebuie să fie o opţiune doar pentru pensionari. De excepţionalele efecte asupra sănătăţii pe care vacanţele petrecute în astfel de locaţii le au asupra sănătăţii pot profita persoanele de toate vârstele, iar pachetele de oferte moderne se concentrează în mod special pe acest aspect urmărind să atragă tinerii şi familiile către aceste zone cu un potenţial excepţional de care România nu duce deloc lipsă.</p>
          
          <br class="clear">
          
          <p class="newText text-justify">Un concediu de tratament staţiuni balneare nu este doar o excelentă ocazie de relaxare şi recuperare pentru persoanele care suferă de diverse afecţiuni, dar şi o opţiune de buget pentru familiile acestora care se pot relaxa în zonele pitoreşti în care sunt aşezate cele mai multe dintre aceste staţiuni. Pe timp de vară, acestea pot înlocui cu succes o vacanţă mult mai scumpă pe litoral. Avantajul este acela că staţiunile de acest tip sunt mult mai liniştite, mai puţin aglomerate, iar ofertele de cazare la hotel au preţuri promoţionale prin diverse programe guvernamentale.</p>
          <h3 class="text-justify mar0 pad0">Staţiuni balneare - sănătate şi relaxare</h3>
          <br>
          <p class="newText text-justify">Staţiunile balneare din România sunt cunoscute pentru marea diversitate de afecţiuni pe care acestea le tratează. În staţiunile ce sunt fie de curând modernizate fie în curs de modernizare, cu echipamente medicale de ultimă generaţie şi oferte de cazare în condiţii cu adevărat excepţionale, efectul terapeutic al apei este combinat.</p>
          <p class="newText text-justify">În <strong>staţiuni balneoclimaterice</strong> se tratează un număr mare de afecţiuni prin tehnici precum băile în ape minerale cu sulf, calciu sau magneziu, băi termale şi împachetări cu nămol. Tehnicile dezvoltate aici de sute de ani, combinate cu tehnologia modernă pot trata sau amelioara numeroase probleme de sănătate, de la alergii la tratamente regenerative, reumatism, infertilitate sau alte probleme ginecologice, toate acestea într-un peisaj idilic de care se poate bucura tot restul familiei care nu participă la tratament, cu ajutorul unor activităţi precum înotul în piscină, tenisul sau plimbările lungi într-un cadru liniştit şi relaxant.</p>
        </div>

        <br class="clear">

		<?php /*?><?php
        $oferte_rec=new AFISARE_SEJUR_NORMAL();
        $oferte_rec->setAfisare(1, 4);
        $oferte_rec->setRecomandata('da');
		$oferte_rec->setRandom(1);
        $oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_early_booking.tpl");
		$oferte_rec->setTari($link_tara);
		$oferte_rec->setZone($link_zona);
		$nr_hoteluri=$oferte_rec->numar_oferte();
        if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
        ?>
        <div class="ofRec" style="padding:8px 5px 6px 5px;">
          <h3 class="blue">Oferte recomandate pentru Statiuni Balneare</h3>
		  <?php $oferte_rec->afiseaza();?>
          <br class="clear">
        </div>
        <?php } ?>

        <br class="clear"><br><?php */?>
        
		<?php /*?><?php $sel_loc = "SELECT
		COUNT(oferte.id_oferta) AS numar,
		localitati.denumire AS denumire_localitate
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		WHERE oferte.valabila = 'da'
		AND hoteluri.tip_unitate <> 'Circuit'
		AND localitati.id_zona = '".$id_zona."'
		AND oferte.last_minute = 'nu'
		GROUP BY denumire_localitate
		ORDER BY numar DESC, denumire_localitate ASC ";
		$rez_loc = mysql_query($sel_loc) or die(mysql_error());
		while ($row_loc = mysql_fetch_array($rez_loc)) {
			$den_loc = $row_loc['denumire_localitate'];
			$link = '/sejur-romania/statiuni-balneare/'.fa_link($den_loc).'/';
			//if(strlen($row_loc['denumire_localitate'])>13) {
			//	$den_lunga_loc = 'style="font-size:44px; letter-spacing:-1.5pt;"';
			//} else {
			//	$den_lunga_loc = 'style="font-size:44px;"';
			//}
		?>
        <a href="<?php echo $link; ?>" title="Cazare <?php echo $den_loc; ?>" class="cb-item" style="background:url(images/old_statiuni-balneare-<?php echo fa_link($den_loc); ?>.jpg) left top no-repeat;"><span class="titlu" <?php echo $den_lunga_loc; ?>><?php echo $den_loc; ?></span></a>
		<?php } @mysql_free_result($rez_loc); ?>
        
        <br class="clear"><br><?php */?>
        

        
        <br class="clear">
        
        <div class="pad10"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?></div>
        
        <br class="clear"><br>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>