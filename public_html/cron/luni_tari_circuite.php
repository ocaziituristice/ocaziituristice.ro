<?php include_once('../config/includes/class/mysql.php');
$acest_an=date('Y');
$aceasta_luna=date('m');
$luna=array();
echo $selD="SELECT
	data_pret_oferta.data_start,
	data_pret_oferta.data_end,
	oferte.tip_preturi,
	tari.id_tara
	FROM oferte
	INNER JOIN data_pret_oferta ON oferte.id_oferta = data_pret_oferta.id_oferta
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara
	WHERE
	(((data_pret_oferta.data_end is null OR data_pret_oferta.data_end = '0000-00-00') AND data_pret_oferta.data_start >= now()) OR ((data_pret_oferta.data_end is not null AND data_pret_oferta.data_end <> '0000-00-00') AND data_pret_oferta.data_end>= now()))
	GROUP BY data_pret_oferta.data_start, data_pret_oferta.data_end, oferte.tip_preturi, tari.id_tara
	ORDER BY tari.id_tara, data_pret_oferta.data_start
";
$queD=mysql_query($selD) or die(mysql_error());
while($rowD=mysql_fetch_array($queD)) {
	if($rowD['tip_preturi']=='perioade' && $rowD['data_end']<>'0000-00-00') {
		$st=explode('-', $rowD['data_start']);
		$end=explode('-', $rowD['data_end']);
		$luna[$rowD['id_tara']][$st[0]][$st[1]]=1;
		if($st[1]<$end[1]) for($i=$st[1]+1; $i<=$end[1]; $i++) {
			if($i<10) $j='0'.$i; else $j=$i;
			$luna[$rowD['id_tara']][$st[0]][$j]=1;
		} elseif($st[1]>$end[1]) {
			for($i=$st[1]+1; $i<=12; $i++) {
				if($i<10) $j='0'.$i; else $j=$i;
				$luna[$rowD['id_tara']][$st[0]][$j]=1;
			}
			for($i=1; $i<=$end[1]; $i++) {
				if($i<10) $j='0'.$i; else $j=$i;
				$luna[$rowD['id_tara']][$end[0]][$j]=1;
			}
		}
	} else {
		$st=explode('-', $rowD['data_start']);
		$luna[$rowD['id_tara']][$st[0]][$st[1]]=1;
	}
} @mysql_free_result($queD);

$luni_pe_tara=array();
foreach($luna as $id_tara=>$value) {
	$luni='';
	foreach($value as $an => $value1) {
		foreach($value1 as $luna1 =>$val) {
			if(($an==$acest_an && $luna1>=$aceasta_luna) || $an>$acest_an) $luni=$luni.$luna1.'-'.$an.',';
		}
	}
	$luni=substr($luni,0, -1);
	/*$upd="UPDATE tari SET luni_plecari_circuite = '".$luni."' WHERE id_tara = '".$id_tara."' ";
	$queU=mysql_query($upd) or die(mysql_error());
	@mysql_free_result($queU);*/
} ?>

<p>Lunile de plecare la tari pentru circuite updatate.</p>
