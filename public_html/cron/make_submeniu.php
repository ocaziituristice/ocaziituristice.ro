<?php include_once('../config/includes/class/mysql.php');
include_once('../config/functii.php');

/***** TURISM EXTERN *****/
$cont_ext = '<div id="sub_turism_extern" class="submenu clearfix">'."\n";

$tari=array();
$sel_tari="SELECT
COUNT(oferte.id_oferta) AS numar,
tari.denumire,
tari.id_tara,
tari.steag
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND tari.id_tara <> '1'
AND hoteluri.tip_unitate <> 'Circuit'
AND oferte.last_minute = 'nu'
GROUP BY tari.id_tara
ORDER BY numar DESC ";
$rezultat=mysql_query($sel_tari) or die(mysql_error());
$nt_total=mysql_num_rows($rezultat);

$nr_pe_l=floor($nt_total/3);
if(fmod($nt_total,3)<>0) $nr_pe_l++;
$i=1;
$j=0;
while($row=mysql_fetch_array($rezultat)) {
	$j++;
	$tari[$i][$j]=array('den'=>$row['denumire'], 'steag'=>$row['steag']);
	if($j==$nr_pe_l) {
		$i++;
		$j=0;
	}
}

foreach($tari as $nr => $val) {
	$cont_ext = $cont_ext.'<ul class="ext">'."\n";
	foreach($val as $k=> $value) {
		if($value['den']<>'Emiratele Arabe Unite') { $cont_ext = $cont_ext.'<li class="turism_extern"><a href="/sejur-'.fa_link($value['den']).'/" title="Sejur '.$value['den'].'" class="country" style="background-image:url(/thumb_steag_tara/'.$value['steag'].')">'.$value['den'].'</a></li>'."\n";
		} else { $cont_ext = $cont_ext.'<li class="turism_extern"><a href="/sejur-'.fa_link($value['den']).'/dubai/" title="Sejur Dubai" class="country" style="background-image:url(/thumb_steag_tara/'.$value['steag'].')">Dubai</a></li>'."\n";
		}
	}
	$cont_ext = $cont_ext.'</ul>'."\n";
}

$cont_ext = $cont_ext.'</div>'."\n";
/***** TURISM EXTERN *****/

/****** REVELION EXTERN ***************/

$cont_revelion_ext = '<div id="sub_revelion_extern" class="submenu clearfix">'."\n";

$tari=array();
$sel_tari="SELECT
COUNT(oferte.id_oferta) AS numar,
tari.denumire,
tari.id_tara,
tari.steag
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
INNER JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
WHERE oferte.valabila = 'da'
AND tari.id_tara <> '1'
AND hoteluri.tip_unitate <> 'Circuit'
AND oferte.last_minute = 'nu'
AND (oferta_sejur_tip.id_tip_oferta=33 or oferta_sejur_tip.id_tip_oferta=69)
GROUP BY tari.id_tara
ORDER BY numar DESC";
$rezultat=mysql_query($sel_tari) or die(mysql_error());
$nt_total=mysql_num_rows($rezultat);

$nr_pe_l=floor($nt_total/3);
if(fmod($nt_total,2)<>0) $nr_pe_l++;
$i=1;
$j=0;
while($row=mysql_fetch_array($rezultat)) {
	$j++;
	$tari[$i][$j]=array('den'=>$row['denumire'], 'steag'=>$row['steag']);
	if($j==$nr_pe_l) {
		$i++;
		$j=0;
	}
}

foreach($tari as $nr => $val) {
	$cont_revelion_ext = $cont_revelion_ext.'<ul class="ext">'."\n";
	foreach($val as $k=> $value) {
		if($value['den']<>'Emiratele Arabe Unite') { $cont_revelion_ext = $cont_revelion_ext.'<li class="turism_extern"><a href="/oferte-revelion/'.fa_link($value['den']).'/" title="Revelion '.$value['den'].'" class="country" style="background-image:url(/thumb_steag_tara/'.$value['steag'].')">'."Revelion 2015 ".$value['den'].'</a></li>'."\n";
		} else { $cont_revelion_ext = $cont_revelion_ext.'<li class="turism_extern"><a href="/oferte-revelion/'.fa_link($value['den']).'/dubai/" title="Revelion Dubai" class="country" style="background-image:url(/thumb_steag_tara/'.$value['steag'].')">Revelion 2015 Dubai</a></li>'."\n";
		}
	}
	$cont_revelion_ext = $cont_revelion_ext.'</ul>'."\n";
}

$cont_revelion_ext = $cont_revelion_ext.'</div>'."\n";


/*************** END REVELION EXTERN ***********/








/***** STATIUNI BALNEARE *****/
$cont=$cont.'
<div id="sub_balneare_statiuni" class="submenu">
  <table border="0" cellpadding="0" cellspacing="0">
	<tr>';

$den_zona = 'Statiuni Balneare';
$id_tara = get_id_tara_by_zona($den_zona);
$id_zona = get_id_zona($den_zona, $id_tara);
$balneo = array();

$sel_balneo = "SELECT
COUNT(oferte.id_oferta) AS numar,
localitati.denumire AS denumire_localitate
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND localitati.id_zona = '".$id_zona."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$res_balneo = mysql_query($sel_balneo) or die(mysql_error());
$nr_balneo = mysql_num_rows($res_balneo);

$nr_pe_balneo = floor($nr_balneo/3);
if(fmod($nr_balneo,3)<>0) $nr_pe_balneo++;
$ii=1;
$jj=0;
while($row_balneo=mysql_fetch_array($res_balneo)) {
	$jj++;
	$balneo[$ii][$jj]=array('den'=>$row_balneo['denumire_localitate']);
	if($jj==$nr_pe_balneo) {
		$ii++;
		$jj=0;
	}
}

foreach($balneo as $key => $vall) {
	$cont=$cont.'
	  <td class="sm_column">
		<ul>';
	foreach($vall as $k => $value) {
		$cont=$cont.'
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/'.fa_link($value['den']).'/">'.$value['den'].'</a></li>';
	}
	$cont=$cont.'
		</ul>
	  </td>';
}

$cont=$cont.'
	</tr>
  </table>
</div>
';
/***** STATIUNI BALNEARE *****/


/***** CIRCUITE *****/
/*$cont=$cont.'<div id="sub_circuite" class="submenu">
  <table border="0" cellpadding="0" cellspacing="0">
	<tr>';

$tari_circ=array();
$sel_circ="SELECT
tari.denumire,
continente.nume_continent
FROM oferte
INNER JOIN hoteluri on oferte.id_hotel = hoteluri.id_hotel
INNER JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara
INNER JOIN continente ON continente.id_continent = tari.id_continent
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
AND traseu_circuit.tara_principala = 'da'
AND traseu_circuit.id_tara IN (2,4,6,8,9,16,22,26,29,56,95,97,212,236,229)
GROUP BY tari.denumire
ORDER BY tari.denumire ASC ";
$res_circ=mysql_query($sel_circ) or die(mysql_error());
$nr_total_circ=mysql_num_rows($res_circ);

$nr_pe_circ=floor($nr_total_circ/3);
if(fmod($nr_total_circ,3)<>0) $nr_pe_circ++;
$i_circ=1;
$j_circ=0;
while($row_circ=mysql_fetch_array($res_circ)) {
	$j_circ++;
	$tari_circ[$i_circ][$j_circ]=array('den'=>$row_circ['denumire'], 'continent'=>$row_circ['nume_continent']);
	if($j_circ==$nr_pe_circ) {
		$i_circ++;
		$j_circ=0;
	}
}

foreach($tari_circ as $nr_circ => $val_circ) {
	$cont=$cont.'
	  <td class="sm_column">
		<ul>';
	foreach($val_circ as $k_circ => $value_circ) {
		$cont=$cont.'
		  <li class="item"><a href="/circuite/'.fa_link($value_circ['continent']).'/'.fa_link($value_circ['den']).'/">Circuit '.$value_circ['den'].'</a></li>';
	}
	$cont=$cont.'
		</ul>
	  </td>';
}

$cont=$cont.'
	</tr>
  </table>
</div>
';*/
/***** CIRCUITE *****/


//include('../includes/header/submenus_litoral.php');
include('../includes/header/submenus_romania.php');

$handle1 = fopen($_SERVER['DOCUMENT_ROOT'].'/includes/header/submenus.php', 'w+');
fwrite($handle1, $cont);
fclose($handle1);

$handle_ext = fopen($_SERVER['DOCUMENT_ROOT'].'/includes/header/submenus_turism_extern.php', 'w+');
fwrite($handle_ext, $cont_ext);
fclose($handle_ext);

$handle_revelion_ext = fopen($_SERVER['DOCUMENT_ROOT'].'/includes/header/submenus_revelion_extern.php', 'w+');
fwrite($handle_revelion_ext, $cont_revelion_ext);
fclose($handle_revelion_ext);
?>

<p>Submeniuri - updatat OK.</p>
