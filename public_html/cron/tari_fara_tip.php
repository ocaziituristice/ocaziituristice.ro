<?php include_once('../config/includes/class/mysql.php');
$m=''; 
//coordonate_hoteluri
$sel="SELECT
hoteluri.nume,
hoteluri.id_hotel
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE (hoteluri.latitudine IS NULL
	OR hoteluri.latitudine = '0'
	OR hoteluri.longitudine IS NULL
	OR hoteluri.longitudine = '0')
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY hoteluri.id_hotel
ORDER BY tari.denumire ASC, hoteluri.nume ASC
";
$que=mysql_query($sel) or die(mysql_error());
if(mysql_num_rows($que)>0) {
$m=$m.'<br/><br/><h2>Lista hoteluri fara coordonate</h2>
<br/>
<ul>';	
while($row=mysql_fetch_array($que)) {
$m=$m.'<li><a href="'.$sitepath_adm.'editare_hotel.php?pas=2&hotel='.$row['id_hotel'].'" target="_blank">'.$row['nume'].'</a></li>';	
	
}
$m=$m.'</ul>';
} @mysql_free_result($que);

$sel="SELECT
tari.id_tara,
tari.denumire
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel 
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
LEFT JOIN tari_tip_sejur ON tari.id_tara = tari_tip_sejur.id_tara
WHERE tari_tip_sejur.id_tip_oferta IS NULL
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY tari.id_tara
ORDER BY tari.denumire ASC
";
$que=mysql_query($sel) or die(mysql_error());
if(mysql_num_rows($que)>0) {
$m='<h2>Lista tari cu oferte care nu au setate tipurile</h2>
<br/>
<ul>';
 while($row=mysql_fetch_array($que)) {
  $m=$m.'<li><a href="'.$sitepath_adm.'editare_tara.php?pas=2&tara='.$row['id_tara'].'" target="_blank">'.$row['denumire'].'</a></li>';
 }
$m=$m.'</ul>';
} @mysql_free_result($que);
//coordonate_localitati
$sel="SELECT
localitati.denumire,
localitati.id_localitate
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE (localitati.latitudine IS NULL
	OR localitati.latitudine = '0'
	OR localitati.longitudine IS NULL
	OR localitati.longitudine = '0')
GROUP BY localitati.id_localitate
ORDER BY tari.denumire ASC, localitati.denumire ASC
";
$que=mysql_query($sel) or die(mysql_error());
if(mysql_num_rows($que)>0) {
$m=$m.'<br/><br/><h2>Lista localitati fara coordonate</h2>
<br/>
<ul>';	
while($row=mysql_fetch_array($que)) {
$m=$m.'<li><a href="'.$sitepath_adm.'editare_localitate.php?pas=2&localitate='.$row['id_localitate'].'" target="_blank">'.$row['denumire'].'</a></li>';	
	
}
$m=$m.'</ul>';
} @mysql_free_result($que);

//coordonate_zone
$sel="SELECT
zone.denumire,
zone.id_zona
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE (zone.latitudine IS NULL
	OR zone.latitudine = '0'
	OR zone.longitudine IS NULL
	OR zone.longitudine = '0')
GROUP BY zone.id_zona
ORDER BY tari.denumire ASC, zone.denumire ASC
";
$que=mysql_query($sel) or die(mysql_error());
if(mysql_num_rows($que)>0) {
$m=$m.'<br/><br/><h2>Lista zone fara coordonate</h2>
<br/>
<ul>';	
while($row=mysql_fetch_array($que)) {
$m=$m.'<li><a href="'.$sitepath_adm.'editare_zona.php?pas=2&zona='.$row['id_zona'].'" target="_blank">'.$row['denumire'].'</a></li>';	
	
}
$m=$m.'</ul>';
} @mysql_free_result($que);

//coordonate_tari
$sel="SELECT
tari.denumire,
tari.id_tara
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE (tari.latitudine IS NULL
	OR tari.latitudine = '0'
	OR tari.longitudine IS NULL
	OR tari.longitudine = '0')
GROUP BY tari.id_tara
ORDER BY tari.denumire
";
$que=mysql_query($sel) or die(mysql_error());
if(mysql_num_rows($que)>0) {
$m=$m.'<br/><br/><h2>Lista tari fara coordonate</h2>
<br/>
<ul>';	
while($row=mysql_fetch_array($que)) {
$m=$m.'<li><a href="'.$sitepath_adm.'editare_tara.php?pas=2&tara='.$row['id_tara'].'" target="_blank">'.$row['denumire'].'</a></li>';	
	
}
$m=$m.'</ul>';
} @mysql_free_result($que);


$handle1 = fopen('../adm/tari_fara_tip.php', 'w+');
fwrite($handle1, $m);
fclose($handle1); ?>

<p>Lista tari, localitati, fara coordonate, tipuri updatata.</p>
