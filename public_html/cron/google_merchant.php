<?php
include_once('../config/includes/class/mysql.php');
include_once('../adm/cofig_adm.php');
include_once('../config/functii.php');
include_once('../config/functii_pt_afisare.php');
include_once('../config/includes/class/class_sejururi/class_sejur_normal.php');

mysql_query("TRUNCATE TABLE google_merchant") or die(mysql_error());

$sel_cir = "SELECT
tari.id_tara,
tari.denumire,
continente.nume_continent
FROM oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
INNER JOIN continente ON continente.id_continent = tari.id_continent
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
AND traseu_circuit.tara_principala = 'da'
GROUP BY tari.denumire
ORDER BY tari.denumire ASC
";
$que_cir = mysql_query($sel_cir);
while($row_cir = mysql_fetch_array($que_cir)) {
	$oferte=new AFISARE_SEJUR_NORMAL();
	$oferte->setCircuit('da');
	//$oferte->setContinent(fa_link($row_cir['nume_continent']));
	$oferte->setTariCircuit(array('tari'=>fa_link($row_cir['denumire'])));
	
	$sql_circuite = $oferte->sel_toate();
	$rez_circuite = mysql_query($sql_circuite) or die(mysql_error());
	while($row_circuite = mysql_fetch_array($rez_circuite)) {
		$ins_products = "INSERT INTO google_merchant
		VALUES (
		'C".$row_circuite['id_oferta']."',
		'".$row_circuite['denumire_oferta']."',
		'-',
		'New',
		'".$row_circuite['pret_minim']." ".substr($row_circuite['moneda'],0,3)."',
		'in stock',
		'".substr($sitepath, 0, -1).make_link_circuit($row_circuite['denumire_oferta'], $row_circuite['id_oferta'])."',
		'".$sitepath."img_mediu_hotel/".$row_circuite['poza1']."'
		) ";
		$que_products = mysql_query($ins_products) or die(mysql_error());
		@mysql_free_result($que_products);
	}	
}

$sel_sej = "SELECT
Count(oferte.id_oferta) AS numar,
tari.denumire,
tari.id_tara,
tari.descriere_scurta
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND tari.id_tara <> '1'
AND hoteluri.tip_unitate <> 'Circuit'
AND oferte.last_minute = 'nu'
GROUP BY tari.id_tara
ORDER BY numar DESC
";
$que_sej = mysql_query($sel_sej);
while($row_sej = mysql_fetch_array($que_sej)) {
	$oferte=new AFISARE_SEJUR_NORMAL();
	$oferte->setTari($row_sej['denumire']);
	
	$sql_sejururi = $oferte->sel_toate();
	$rez_sejururi = mysql_query($sql_sejururi) or die(mysql_error());
	while($row_sejururi = mysql_fetch_array($rez_sejururi)) {
		if($row_sejururi['detalii_concept']) {
			$comentariu = strip_tags($row_sejururi['detalii_concept']);
		} else if($row_sejururi['new_descriere']) {
			$comentariu = truncate_str(strip_tags($row_sejururi['new_descriere']), 270).' ...';
		} else if($row_sejururi['descriere_hotel']) {
			$comentariu = truncate_str(strip_tags($row_sejururi['descriere_hotel']), 270).' ...';
		}
		
		$ins_products = "INSERT INTO google_merchant
		VALUES (
		'S".$row_sejururi['id_oferta']."',
		'".$row_sejururi['denumire_oferta']."',
		'".$comentariu."',
		'New',
		'".$row_sejururi['pret_minim']." ".substr($row_sejururi['moneda'],0,3)."',
		'in stock',
		'".substr($sitepath, 0, -1).make_link_oferta($row_sejururi['denumire_localitate'], $row_sejururi['denumire_hotel'], $row_sejururi['denumire_oferta_scurta'], $row_sejururi['id_oferta'])."',
		'".$sitepath."img_mediu_hotel/".$row_sejururi['poza1']."'
		) ";
		$que_products = mysql_query($ins_products) or die(mysql_error());
		@mysql_free_result($que_products);
	}	
}

mysql_query("OPTIMIZE TABLE google_merchant") or die(mysql_error());

echo '<p>Google Merchant updated!</p>';

?>