<?php
include_once('../config/includes/class/mysql.php');
include_once('../adm/cofig_adm.php');
include_once('../config/functii.php');
include_once('../config/functii_pt_afisare.php');
include_once('../config/includes/class/class_sejururi/class_sejur_normal.php');

mysql_query("TRUNCATE TABLE profitshare_products") or die(mysql_error());

$sej = array();

$tari = get_tari('', '', '', '', '', NULL, NULL, 'ASC');
if(sizeof($tari)>0) {
	foreach($tari as $k_tar => $v_tar) {
		if($v_tar=='Romania') $coin = 'Lei'; else $coin = 'Euro';
		if($v_tar=='Romania' or $v_tar=='Bulgaria') {
			
			$zone = get_zone('', '', '', $k_tar, '', NULL, 'ASC');
			if(sizeof($zone)>0) {
				foreach($zone as $k_zon => $v_zon) {
					$code_zone = $k_tar.'_'.$k_zon;
					$sej[$code_zone]['categorie'] = $v_tar.' - '.$v_zon;
					
					$localitati = get_localitate('', '', $k_zon, $k_tar, '', NULL, 'ASC');
					foreach($localitati as $k_loc => $v_loc) {
						$tematici = get_tiputi_oferte('', $k_loc, $k_zon, $k_tar, '', '');
						if(sizeof($tematici)>0) {
							foreach($tematici as $k_tem => $v_tem) {
								$cod_produs = 'L_'.$k_loc.'_'.$k_tem;
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['producator'] = $v_tem['denumire'];
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['cod_producator'] = $k_tem;
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['model'] = $v_tem['denumire'];
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['nume'] = $v_tem['denumire'].' '.$v_loc['denumire'];
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['descriere'] = '';
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['link'] = $sitepath.'oferte-'.fa_link($v_tem['denumire']).'/'.fa_link($v_tar).'/'.fa_link($v_zon).'/'.fa_link($v_loc['denumire']).'/';
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['imagine'] = substr($sitepath, 0, -1).getpic_destinatie('', '', $k_loc);
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['pret'] = get_pricemin($k_tar, $k_zon, $k_loc, $k_tem);
								$sej[$code_zone]['produse'][$k_loc][$cod_produs]['moneda'] = $coin;
							}
						} else {
							$cod_produs = 'L_'.$k_loc;
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['producator'] = 'Sejur';
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['cod_producator'] = 'SEJ';
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['model'] = 'Sejur';
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['nume'] = $v_loc['denumire'];
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['descriere'] = '';
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['link'] = $sitepath.'sejur-'.fa_link($v_tar).'/'.fa_link($v_zon).'/'.fa_link($v_loc['denumire']).'/';
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['imagine'] = substr($sitepath, 0, -1).getpic_destinatie('', '', $k_loc);
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['pret'] = get_pricemin($k_tar, $k_zon, $k_loc);
							$sej[$code_zone]['produse'][$k_loc][$cod_produs]['moneda'] = $coin;
						}
					}
				}
			}
		} else {
			$code_tari = $k_tar;
			$sej[$k_tar]['categorie'] = $v_tar;
			
			$zone = get_zone('', '', '', $k_tar, '', NULL, 'ASC');
			if(sizeof($zone)>0) {
				foreach($zone as $k_zon => $v_zon) {
					$tematici = get_tiputi_oferte('', '', $k_zon, $k_tar, '', '');
					if(sizeof($tematici)>0) {
						foreach($tematici as $k_tem => $v_tem) {
							$cod_produs = 'Z_'.$k_zon.'_'.$k_tem;
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['producator'] = $v_tem['denumire'];
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['cod_producator'] = $k_tem;
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['model'] = $v_tem['denumire'];
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['nume'] = $v_tem['denumire'].' '.$v_zon;
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['descriere'] = '';
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['link'] = $sitepath.'oferte-'.fa_link($v_tem['denumire']).'/'.fa_link($v_tar).'/'.fa_link($v_zon).'/';
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['imagine'] = substr($sitepath, 0, -1).getpic_destinatie('', $k_zon, '');
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['pret'] = get_pricemin($k_tar, $k_zon, '', $k_tem);
							$sej[$code_tari]['produse'][$k_zon][$cod_produs]['moneda'] = $coin;
						}
					} else {
						$cod_produs = 'Z_'.$k_zon;
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['producator'] = 'Sejur';
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['cod_producator'] = 'SEJ';
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['model'] = 'Sejur';
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['nume'] = $v_zon;
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['descriere'] = '';
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['link'] = $sitepath.'sejur-'.fa_link($v_tar).'/'.fa_link($v_zon).'/';
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['imagine'] = substr($sitepath, 0, -1).getpic_destinatie('', $k_zon, '');
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['pret'] = get_pricemin($k_tar, $k_zon, '');
						$sej[$code_tari]['produse'][$k_zon][$cod_produs]['moneda'] = $coin;
					}
				}
			}
		}
	}
}
//echo '<pre>';print_r($sej);echo '<pre>';

foreach($sej as $k_categ => $v_categ) {
	foreach($v_categ['produse'] as $k_products => $v_products) {
		foreach($v_products as $k_produs => $v_produs) {
			$ins_products = "INSERT INTO profitshare_products
			(cod_categorie, categorie, categorie_parinte, producator, cod_producator, model, cod_produs, nume, link, imagine, pret_fara_tva, pret_cu_tva, moneda)
			VALUES
			('".$k_categ."', '".$v_categ['categorie']."', '".$v_categ['categorie']."', '".$v_produs['producator']."', '".$v_produs['cod_producator']."', '".$v_produs['model']."', '".$k_produs."', '".$v_produs['nume']."', '".$v_produs['link']."', '".$v_produs['imagine']."', '".$v_produs['pret']."', '".$v_produs['pret']."', '".$v_produs['moneda']."')
			";
			$que_products = mysql_query($ins_products) or die(mysql_error());
			@mysql_free_result($que_products);
			//echo $ins_products;echo '<br>';
		}
	}
}

$sel_cir = "SELECT
tari.id_tara,
tari.denumire,
continente.nume_continent
FROM oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
INNER JOIN continente ON continente.id_continent = tari.id_continent
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
AND traseu_circuit.tara_principala = 'da'
GROUP BY tari.denumire
ORDER BY tari.denumire ASC
";
$que_cir = mysql_query($sel_cir);
while($row_cir = mysql_fetch_array($que_cir)) {
	$oferte=new AFISARE_SEJUR_NORMAL();
	$oferte->setCircuit('da');
	//$oferte->setContinent(fa_link($row_cir['nume_continent']));
	$oferte->setTariCircuit(array('tari'=>fa_link($row_cir['denumire'])));
	
	$sql_circuite = $oferte->sel_toate();
	$rez_circuite = mysql_query($sql_circuite) or die(mysql_error());
	while($row_circuite = mysql_fetch_array($rez_circuite)) {
		$ins_products = "INSERT INTO profitshare_products
		(cod_categorie, categorie, categorie_parinte, producator, cod_producator, model, cod_produs, nume, link, imagine, pret_fara_tva, pret_cu_tva, moneda)
		VALUES
		('".$row_cir['id_tara']."', '".$row_cir['denumire']."', '".$row_cir['denumire']."', 'Circuit', 'CIR', 'Circuit', 'C".$row_circuite['id_oferta']."', '".$row_circuite['denumire_oferta']."', '".substr($sitepath, 0, -1).make_link_circuit($row_circuite['denumire_oferta'], $row_circuite['id_oferta'])."', '".$sitepath."img_mediu_hotel/".$row_circuite['poza1']."', '".$row_circuite['pret_minim']."', '".$row_circuite['pret_minim']."', '".ucfirst(strtolower($row_circuite['moneda']))."')
		";
		$que_products = mysql_query($ins_products) or die(mysql_error());
		@mysql_free_result($que_products);
	}	
}

mysql_query("OPTIMIZE TABLE profitshare_products") or die(mysql_error());

echo '<p>Profitshare products updated!</p>';

?>