<?php include_once('../config/includes/class/mysql.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);

class cursBnrXML {
	 var $xmlDocument = "";
	 var $date = "";
	 var $currency = array();
	
	function cursBnrXML($url) {
		$this->xmlDocument = file_get_contents($url);
		$this->parseXMLDocument();
	}
	
	function parseXMLDocument() {
		 $xml = new SimpleXMLElement($this->xmlDocument);
		 
		 $this->date=$xml->Header->PublishingDate;
		 
		 foreach($xml->Body->Cube->Rate as $line)    
		 {                      
			 $this->currency[]=array("name"=>$line["currency"], "value"=>$line, "multiplier"=>$line["multiplier"]);
		 }
	}
	
	
	function getCurs($currency) {
		foreach($this->currency as $line)
		{
			if($line["name"]==$currency)
			{
				return $line["value"];
			}
		}
		
		return "Incorrect currency!";
	}
}

$curs = new cursBnrXML("http://www.bnr.ro/nbrfxrates.xml");

$curs_data = $curs->date;
$curs_euro = $curs->getCurs("EUR");
$curs_dolar = $curs->getCurs("USD");

$upd_curs_valutar = "UPDATE curs_valutar SET data='$curs_data', USD='$curs_dolar', EURO='$curs_euro' ";
$que_curs_valutar = mysql_query($upd_curs_valutar) or die(mysql_error());

$sel_curs = "SELECT * FROM curs_valutar ";
$que_curs = mysql_query($sel_curs) or die(mysql_error());
$curs = mysql_fetch_array($que_curs);
@mysql_free_result($que_curs);

$sel_toate = "SELECT
oferte.id_oferta,
preturi.*
FROM oferte
LEFT JOIN (SELECT
	data_pret_oferta.pret,
	data_pret_oferta.moneda,
	data_pret_oferta.id_oferta
	FROM data_pret_oferta
	INNER JOIN tip_camera ON data_pret_oferta.tip_camera = tip_camera.id_camera
	WHERE (((data_pret_oferta.data_end IS NULL OR data_pret_oferta.data_end = '0000-00-00') AND data_pret_oferta.data_start >= NOW()) OR ((data_pret_oferta.data_end IS NOT NULL OR data_pret_oferta.data_end <> '0000-00-00') AND data_pret_oferta.data_end >= NOW()))
	GROUP BY data_pret_oferta.pret, data_pret_oferta.id_oferta
	ORDER BY data_pret_oferta.pret
) AS preturi on oferte.id_oferta = preturi.id_oferta
WHERE oferte.valabila = 'da'
GROUP BY oferte.id_oferta ";
$que_toate = mysql_query($sel_toate) or die(mysql_error());
while($row_toate = mysql_fetch_array($que_toate)) {
	if($row_toate['moneda']<>'RON') $pret_ron = $row_toate['pret'] * $curs[$row_toate['moneda']]; else $pret_ron = $row_toate['pret'];
	
	echo "<br />". $sel_up = "UPDATE oferte SET pret_minim = '".$row_toate['pret']."', moneda = '".$row_toate['moneda']."', pret_minim_lei = '".$pret_ron."' WHERE id_oferta = '".$row_toate['id_oferta']."' ";
	$que_up = mysql_query($sel_up) or die(mysql_error());
	@mysql_free_result($que_up);
} @mysql_free_result($que_toate);

sleep(1);

$sel_mindate = "SELECT
data_pret_oferta.id_oferta,
MIN(data_pret_oferta.data_start) AS start_date_min
FROM data_pret_oferta
INNER JOIN oferte ON data_pret_oferta.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND data_pret_oferta.data_start >= NOW()
GROUP BY data_pret_oferta.id_oferta
";
$que_mindate = mysql_query($sel_mindate) or die(mysql_error());
while($row_mindate = mysql_fetch_array($que_mindate)) {
	$sel_up = "UPDATE oferte SET start_date_pret = '".$row_mindate['start_date_min']."' WHERE id_oferta = '".$row_mindate['id_oferta']."' ";
	$que_up = mysql_query($sel_up) or die(mysql_error());
	@mysql_free_result($que_up);
} @mysql_free_result($que_mindate);

sleep(1);

$sel_hotel_min = "SELECT
hoteluri.id_hotel,
MIN(oferte.pret_minim_lei/oferte.nr_nopti) AS min_price
FROM hoteluri
INNER JOIN oferte ON oferte.id_hotel = hoteluri.id_hotel
WHERE oferte.valabila = 'da'
GROUP BY hoteluri.id_hotel ";
$que_hotel_min = mysql_query($sel_hotel_min) or die(mysql_error());
while($row_hotel_min = mysql_fetch_array($que_hotel_min)) {
	$sel_upd_hot = "UPDATE hoteluri SET min_price = '".$row_hotel_min['min_price']."' WHERE id_hotel = '".$row_hotel_min['id_hotel']."' ";
	$que_upd_hot = mysql_query($sel_upd_hot) or die(mysql_error());
	@mysql_free_result($que_upd_hot);
} @mysql_free_result($que_hotel_min);

?>

<p>Pret minim updatat.</p>
