<?php
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');

$del = "TRUNCATE TABLE cautare ";
$que_del = mysql_query($del) or die(mysql_error());
@mysql_free_result($que_del);

$sel_tara = "SELECT tari.denumire AS den_tara, tari.id_tara
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY tari.id_tara
ORDER BY tari.denumire ";
$que_tara = mysql_query($sel_tara) or die(mysql_error());
while($row_tara = mysql_fetch_array($que_tara)) {
	$id_tara = $row_tara['id_tara'];
	$cautare = $row_tara['den_tara'];
	$afisare = "<strong>".$row_tara['den_tara']."</strong>";
	$link = '/sejur-'.fa_link($row_tara['den_tara']).'/';
	
	$ins_tara = "INSERT INTO cautare (cautare,afisare,link,id_tara) VALUES ('$cautare','$afisare','$link','$id_tara') ";
	$que_ins_tara = mysql_query($ins_tara) or die(mysql_error());

	$sel_zona = "SELECT tari.denumire AS den_tara, tari.id_tara, zone.denumire AS den_zona, zone.id_zona
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND zone.id_tara = '".$row_tara['id_tara']."'
	GROUP BY zone.id_zona
	ORDER BY zone.denumire ";
	$que_zona = mysql_query($sel_zona) or die(mysql_error());
	while($row_zona = mysql_fetch_array($que_zona)) {
		$id_tara = $row_zona['id_tara'];
		$cautare = $row_zona['den_zona'];
		$afisare = "<strong>".$row_zona['den_zona']."</strong>, ".$row_zona['den_tara']."";
		$link = '/sejur-'.fa_link($row_zona['den_tara']).'/'.fa_link($row_zona['den_zona']).'/';
		
		$ins_zona = "INSERT INTO cautare (cautare,afisare,link,id_tara) VALUES ('$cautare','$afisare','$link','$id_tara') ";
		$que_ins_zona = mysql_query($ins_zona) or die(mysql_error());

		$sel_loc = "SELECT tari.denumire AS den_tara, tari.id_tara, zone.denumire AS den_zona, zone.id_zona, localitati.denumire AS den_loc, localitati.id_localitate
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		WHERE oferte.valabila = 'da'
		AND hoteluri.tip_unitate <> 'Circuit'
		AND localitati.id_zona = '".$row_zona['id_zona']."'
		GROUP BY localitati.id_localitate
		ORDER BY localitati.denumire ";
		$que_loc = mysql_query($sel_loc) or die(mysql_error());
		while($row_loc = mysql_fetch_array($que_loc)) {
			$id_tara = $row_loc['id_tara'];
			$cautare = $row_loc['den_loc'];
			$afisare = "<strong>".$row_loc['den_loc']."</strong>, ".$row_loc['den_zona'].", ".$row_loc['den_tara']."";
			$link = '/sejur-'.fa_link($row_loc['den_tara']).'/'.fa_link($row_loc['den_zona']).'/'.fa_link($row_loc['den_loc']).'/';
			
			$ins_loc = "INSERT INTO cautare (cautare,afisare,link,id_tara) VALUES ('$cautare','$afisare','$link','$id_tara') ";
			$que_ins_loc = mysql_query($ins_loc) or die(mysql_error());
		}
		@mysql_free_result($que_ins);
		// inserare zone terminata
	}
	@mysql_free_result($que_ins);
	// inserare zone terminata
}
@mysql_free_result($que_ins);
// inserare tari terminata

$sel_hoteluri = "SELECT
tari.id_tara,
tari.denumire AS denumire_tara,
localitati.denumire AS denumire_localitate,
zone.denumire AS denumire_zone,
hoteluri.nume AS denumire_hotel,
hoteluri.stele,
hoteluri.id_hotel
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY hoteluri.id_hotel
ORDER BY tari.denumire";
$que_hoteluri = mysql_query($sel_hoteluri) or die(mysql_error());
while($row_hoteluri = mysql_fetch_array($que_hoteluri)) {
	$id_tara = $row_hoteluri['id_tara'];
	$cautare = $row_hoteluri['denumire_hotel'];
	$afisare = "<strong>".$row_hoteluri['denumire_hotel']." ".$row_hoteluri['stele']."*</strong><br><em>".$row_hoteluri['denumire_zone']." / ".$row_hoteluri['denumire_localitate']." / ".$row_hoteluri['denumire_tara']."</em>";
	$link = make_link_oferta($row_hoteluri['denumire_localitate'], $row_hoteluri['denumire_hotel'], NULL, NULL);
	
	$ins = "INSERT INTO cautare (cautare,afisare,link,id_tara) VALUES ('$cautare','$afisare','$link','$id_tara') ";
	$que_ins = mysql_query($ins) or die(mysql_error());
}
@mysql_free_result($que_ins);
// terminat inserarea hotelurilor
?>

<p>Tabel 'cautare' updatat.</p>
