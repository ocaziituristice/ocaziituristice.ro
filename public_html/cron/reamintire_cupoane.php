<?php include_once('../config/functii.php');
include_once('../config/includes/class/mysql.php');
include_once('../config/includes/class/send_mail.php');

$i=0;
$zile_reminder = array('7','15');

foreach($zile_reminder as $value) {
	$sel_cupoane = "SELECT
	DATE_ADD(cupoane_campanii.data_inceput, INTERVAL cupoane_campanii.nr_zile DAY) AS data_expirare,
	cupoane.*,
	cupoane_campanii.*,
	useri_fizice.*,
	tari.denumire AS den_tara,
	zone.denumire AS den_zona,
	localitati.denumire AS den_localitate
	FROM cupoane
	INNER JOIN cupoane_campanii ON cupoane_campanii.id_campanie = cupoane.id_campanie
	INNER JOIN useri_fizice ON useri_fizice.id_useri_fizice = cupoane.id_useri_fizice
	LEFT JOIN tari ON tari.id_tara = cupoane_campanii.id_tara
	LEFT JOIN zone ON zone.id_zona = cupoane_campanii.id_zona
	LEFT JOIN localitati ON localitati.id_localitate = cupoane_campanii.id_localitate
	WHERE DATE_ADD(cupoane_campanii.data_inceput, INTERVAL (cupoane_campanii.nr_zile - '".$value."') DAY) = CURDATE()
	AND cupoane.activ = 'da'
	";//AND (useri_fizice.email = 'daniel@ocaziituristice.ro' OR useri_fizice.email = 'draghici_razvan@yahoo.com')
	$que_cupoane = mysql_query($sel_cupoane) or die(mysql_error());
	while($row_cupoane = mysql_fetch_array($que_cupoane)) {
		
		$i++;
		
		$coupon_valoare = new_price($row_cupoane['valoare_campanie']).' '.moneda($row_cupoane['moneda_campanie']);
		$coupon_expirare = date("d.m.Y", strtotime($row_cupoane['data_inceput'].' + '.($row_cupoane['nr_zile']-1).' days'));
		
		if($row_cupoane['id_tara']==0 and $row_cupoane['id_zona']==0 and $row_cupoane['id_localitate']==0) {
			$coupon_destinatie = 'toate destinatiile';
		} else if($row_cupoane['id_localitate']!=0) {
			$coupon_destinatie = $row_cupoane['den_localitate'].' ('.$row_cupoane['den_tara'].')';
		} else if($row_cupoane['id_zona']!=0) {
			$coupon_destinatie = $row_cupoane['den_zona'].' ('.$row_cupoane['den_tara'].')';
		} else if($row_cupoane['id_tara']!=0) {
			$coupon_destinatie = $row_cupoane['den_tara'];
		}
		
		$GLOBALS['nume_expeditor2'] = $row_cupoane['prenume'].' '.$row_cupoane['nume'];
		$GLOBALS['mail_expeditor'] = $row_cupoane['email'];
		$GLOBALS['cod_cupon'] = $row_cupoane['cod_cupon'];
		$GLOBALS['coupon_valoare'] = $coupon_valoare;
		$GLOBALS['coupon_destinatie'] = $coupon_destinatie;
		$GLOBALS['coupon_expirare'] = $coupon_expirare;
		$GLOBALS['coupon_link'] = $row_cupoane['link_redirect'];
		$GLOBALS['nr_zile'] = $value;
		$GLOBALS['sigla'] = $GLOBALS['path_sigla'];
		
		
		$send_m = new SEND_EMAIL;
		//email_client
		$param['templates'] = $_SERVER['DOCUMENT_ROOT'].'/mail/templates/reamintire_cupon.htm';
		$param['subject'] = $nume_expeditor2.', reducerea ta de '.str_replace("&euro;", "EUR", $coupon_valoare).' pentru '.$coupon_destinatie.' expira in curand';
		$param['from_email'] = $GLOBALS['email_rezervare'];
		$param['to_email'] = $mail_expeditor;
		$param['to_nume'] = $nume_expeditor2;
		$param['fr_email'] = $GLOBALS['email_rezervare'];
		$param['fr_nume'] = $GLOBALS['denumire_agentie'];
		$send_m->send_rezervari($param);
		
	} @mysql_free_result($que_cupoane);
}

?>

<p><?php echo $i; ?> persoane notificate.</p>