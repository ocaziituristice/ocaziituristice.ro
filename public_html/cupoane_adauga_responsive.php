<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/peste_tot.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_sejur.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/class_sejururi/class_sejur.php' );

/*** check login admin ***/
$logare_admin     = new LOGIN( 'useri' );
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/cupoane.php' );
$cupoane  = new CUPOANE();
$campaign = $cupoane->decode_link( $_GET['hash'] );


$coupon_valoare = new_price( $campaign['valoare_campanie'] ) . ' ' . moneda( $campaign['moneda_campanie'] );
if ( $campaign['moneda_campanie'] == 'Procent' ) {
	$coupon_valoare = round( $campaign['valoare_campanie'], 0 ) . '%';
}

$coupon_perioada_end = strtotime( $campaign['data_inceput'] . ' + ' . $campaign['nr_zile'] . ' days' );
$coupon_perioada     = date( 'j M Y', strtotime( $campaign['data_inceput'] ) ) . ' - ' . date( 'j M Y', $coupon_perioada_end );

if ( $campaign['id_tara'] == 0 and $campaign['id_zona'] == 0 and $campaign['id_localitate'] == 0 ) {
	$coupon_destinatie = 'toate destinatiile';
} else if ( $campaign['id_localitate'] != 0 ) {
	$coupon_destinatie = $campaign['den_localitate'] . ' (' . $campaign['den_tara'] . ')';
	$link_destinatie   = '/sejur-' . fa_link( $campaign['den_tara'] ) . '/' . fa_link( $campaign['den_zona'] ) . '/' . fa_link( $campaign['den_localitate'] ) . '/';
} else if ( $campaign['id_zona'] != 0 ) {
	$coupon_destinatie = $campaign['den_zona'] . ' (' . $campaign['den_tara'] . ')';
	$link_destinatie   = '/sejur-' . fa_link( $campaign['den_tara'] ) . '/' . fa_link( $campaign['den_zona'] ) . '/';
} else if ( $campaign['id_tara'] != 0 ) {
	$coupon_destinatie = $campaign['den_tara'];
	$link_destinatie   = '/sejur-' . fa_link( $campaign['den_tara'] ) . '/';
}

if ( isset( $_POST['trimite'] ) ) {
	$ins_cupon = $cupoane->insert_coupon( $campaign['id_campanie'], $coupon_perioada_end, $_POST['sex'], $_POST['nume'], $_POST['prenume'], $_POST['email'], $_POST['data_nasterii'], $_POST['telefon'] );
	if ( $ins_cupon['error'] == 1 ) {
		$link_succes = $_SERVER['REQUEST_URI'];
		echo '<script>alert("';
		foreach ( $ins_cupon['mesaje'] as $k_cupon => $v_cupon ) {
			echo $v_cupon . '\n';
		}
		echo '"); document.location.href="' . $link_succes . '"; </script>';
	} else if ( $ins_cupon['error'] == 0 ) {
		if ( $_POST['send_email'] == 'da' ) {
			$sel_user = "SELECT useri_fizice.* FROM useri_fizice WHERE id_useri_fizice = '" . $ins_cupon['cupon']['id_useri_fizice'] . "' ";
			$que_user = mysql_query( $sel_user ) or die( mysql_error() );
			$row_user = mysql_fetch_array( $que_user );

			$GLOBALS['nume_expeditor2']   = $row_user['prenume'] . ' ' . $row_user['nume'];
			$GLOBALS['mail_expeditor']    = $row_user['email'];
			$GLOBALS['cod_cupon']         = $ins_cupon['cupon']['cod_cupon'];
			$GLOBALS['coupon_valoare']    = $coupon_valoare;
			$GLOBALS['coupon_perioada']   = $coupon_perioada;
			$GLOBALS['coupon_destinatie'] = $coupon_destinatie;
			$GLOBALS['coupon_link']       = $campaign['link_redirect'];
			$GLOBALS['sigla']             = $GLOBALS['path_sigla'];

			$send_m = new SEND_EMAIL;
			//email_client
			$param['templates']  = $_SERVER['DOCUMENT_ROOT'] . '/mail/templates/inregistrare_cupon.htm';
			$param['subject']    = 'CUPON REDUCERE - ' . $denumire_agentie;
			$param['from_email'] = $GLOBALS['email_rezervare'];
			$param['to_email']   = $mail_expeditor;
			$param['to_nume']    = $nume_expeditor2;
			$param['fr_email']   = $GLOBALS['email_rezervare'];
			$param['fr_nume']    = $GLOBALS['denumire_agentie'];
			$send_m->send_rezervari( $param );
		}

		$coupon      = $cupoane->decode_cupon( $_COOKIE['cupon'] );
		$link_succes = $coupon['link_redirect'];
		echo '<script>alert("Felicitari! Cuponul a fost generat!\nIn scurt timp veti primi pe adresa de email inregistrata un mesaj cu CODUL CUPONULUI!"); document.location.href="' . $campaign['link_redirect'] . '"; </script>';
	}
}

?>
<!DOCTYPE html>
<html lang="ro">
<head>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/header_charset.php" ); ?>
    <title>Cupoane Reducere | Ocaziituristice.ro</title>
    <meta name="robots" content="noindex, nofollow">
	<?php require_once( "includes/header/header_responsive.php" ); ?>
    <link href="/js/jquery_validate/validationEngine.jquery.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<?php // Header ?>
<header>
	<?php require( "includes/header/meniu_header_responsive.php" ); ?>
</header>

<?php // Breadcrumbs and general search ?>
<div class="layout">
	<?php require( "includes/header/breadcrumb_responsive_intreg.php" ); ?>
</div>
<?php 

$now = new DateTime();
    $startdate = new DateTime($campaign['data_start_ridicare']);
    $enddate = new DateTime($campaign['data_end_ridicare']);

    if($startdate <= $now && $now <= $enddate) {
     


?>
<div class="layout">
    <div class="layout-wrapper">
        <h1 class="blue">Completeaza formularul si profita de reducere in perioada <span
                    class="red"><?php echo $coupon_perioada; ?></span></h1>
    </div>
</div>

<div class="layout">
    <div class="layout-wrapper">
        <div id="NEW-destinatie">
            <div class="pad20">

                <div class="coupon NEW-round4px">
                    <div class="inner NEW-round6px clearfix">
                        <div class="discount text-center">
                            <span class="titlu white">DISCOUNT</span>
                            <span class="value white"><?php echo $coupon_valoare; ?></span>
                        </div>
                        <div class="comments black text-center bigger-11em">
                            Cuponul de reducere cu valoarea de<br>
                            <b><?php echo $coupon_valoare; ?></b>
                            este valabil in perioada<br>
                            <b><?php echo $coupon_perioada; ?></b><br>
                            pentru ofertele din
                            <b><?php echo $coupon_destinatie; ?></b><br>
                            de pe portalul <span class="blue"
                                                 style="padding: 0;"><?php echo $denumire_agentie; ?></span>
                        </div>
                    </div>
                </div>

                <div class="coupon-info">
                    <div class="titlu bigger-12em">* <span class="underline">Informatii cupon de reducere</span></div>
                    <p>Cuponul de reducere este valabil <strong>DOAR dupa completarea campurilor</strong> de mai jos cu
                        datele dumneavoastra. Dupa ce veti completa campurile corect, veti primi un email cu <strong>CODUL
                            CUPONULUI</strong>. Acest cod il puteti folosi <strong>DOAR</strong> la rezervarile on-line,
                        <strong>DOAR</strong> in perioada mentionata.</p>
                    <p><strong>ATENTIE!</strong> Valabilitatea cuponului se refera la perioada in care acesta se
                        foloseste, nu perioada sejurului.</p>
                    <p>Cuponul de reducere <strong>NU este transmisibil</strong>.</p>
                </div>

                <br class="clear"><br>
                <div class="NEW-calculeaza bkg-blue">
                    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="coupon-form">
                        <div class="clearfix">
                            <h2 class="blue underline">Inregistreaza Cupon</h2>
                            <div class="float-right bigger-14em form-content col-md-6 coupon-right-side">
                                <div class="item2 clearfix">
                                    <div class="left2">Discount:</div>
                                    <div class="right2 bold"><?php echo $coupon_valoare; ?></div>
                                </div>
                                <div class="item2 clearfix">
                                    <div class="left2">Perioada valabilitate:</div>
                                    <div class="right2 bold"><?php echo $coupon_perioada; ?></div>
                                </div>
                                <div class="item2 clearfix">
                                    <div class="left2">Destinatie:</div>
                                    <div class="right2 bold">
                                        <a href="<?php echo $link_destinatie; ?>" target="_blank"
                                           class="link-blue"><?php echo $coupon_destinatie; ?>
                                            <img src="/images/icon_external_link.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 float-left">
                                <div class="box_detalii_nume_prenume">
                                    <div class="form-group new_check input_none">
                                        <input id="sex_m" type="radio" name="sex"
                                               value="m"<?php echo $_POST['sex'] == 'm' ? ' checked="checked"' : '' ?>
                                               data-validation-engine="validate[required]"/>
                                        <label for="sex_m">Domnul</label>
                                        <input id="sex_f" type="radio" name="sex"
                                               value="f"<?php echo $_POST['sex'] == 'f' ? ' checked="checked"' : '' ?>
                                               data-validation-engine="validate[required]"/>
                                        <label for="sex_f">Doamna</label>
                                    </div>
                                    <div class="input new_input_text">
                                        <input name="nume" id="nume" type="text" class="necesar"
                                               value="<?php echo $_POST['nume'] ? $_POST['nume'] : '' ?>"
                                               data-validation-engine="validate[required]"
                                               onblur="checkIfEmpty($(this).attr('id'))"/>
                                        <label for="nume" class="transition01">Nume *</label>
                                    </div>
                                    <div class="input new_input_text">
                                        <input name="prenume" id="prenume" type="text" class="necesar"
                                               value="<?php echo $_POST['prenume'] ? $_POST['prenume'] : '' ?>"
                                               data-validation-engine="validate[required]"
                                               onblur="checkIfEmpty($(this).attr('id'))"/>
                                        <label for="prenume" class="transition01">Prenume *</label>
                                    </div>
                                    <div class="input new_input_text">
                                        <input name="email" id="email" type="text" class="necesar"
                                               value="<?php echo $_POST['email'] ? $_POST['email'] : '' ?>"
                                               data-validation-engine="validate[required,custom[email]]"
                                               onblur="checkIfEmpty($(this).attr('id'))"/>
                                        <label for="email" class="transition01">E-mail *</label>
                                    </div>
                                    <div class="input new_input_text">
                                        <input name="data_nasterii" id="data-nasterii" type="text" class="necesar"
                                               value="<?php echo $_POST['data_nasterii'] ? $_POST['data_nasterii'] : ''; ?>"/>
                                        <label for="data_nasterii" class="transition01">Data nasterii *</label>
                                    </div>
                                    <div class="input new_input_text">
                                        <input name="telefon" id="telefon" type="text" class="necesar"
                                               value="<?php echo $_POST['telefon'] ? $_POST['telefon'] : '' ?>"
                                               data-validation-engine="validate[required]"
                                               onblur="checkIfEmpty($(this).attr('id'))"/>
                                        <label for="telefon" class="transition01">Telefon *</label>
                                    </div>
									<?php if ( ! $err_logare_admin ) : ?>
                                        <div class="form-group new_check input_none">
                                            <h6>* Trimite email?</h6>
                                            <input id="send_email_yes" type="radio" name="send_email" value="da"/>
                                            <label for="send_email_yes">Da</label>
                                            <input id="send_email_no" type="radio" name="send_email" value="nu"/>
                                            <label for="send_email_no">Nu</label>
                                        </div>
									<?php else : ?>
                                        <input name="send_email" id="send_email" type="hidden" value="da">
									<?php endif; ?>
                                    <div class="form-group new_check input_none">
                                        <input id="termeni_conditii" type="checkbox" name="termeni_conditii"
                                               value="da"<?php echo $_POST['termeni_conditii'] == 'da' ? 'checked="checked"' : '' ?>
                                               data-validation-engine="validate[required]" style="width: 0;"/>
                                        <label for="termeni_conditii" class="form-terms">
                                            * Sunt de acord cu <a
                                                    href="<?php echo $sitepath; ?>termeni_si_conditii.html"
                                                    target="_blank" class="infos2 link-blue" rel="nofollow">termenii si
                                                conditiile</a> de utilizare a site-ului.
                                        </label>
                                    </div>
                                    <div class="form-group new_check input_none"
                                         style="height: auto;line-height: normal;">
                                        <input type="submit" name="trimite" value="Inregistreaza Cupon"
                                               class="button-red"
                                               onClick="ga('send', 'event', 'pagina cupon', 'inregistreaza', '');"/>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </form>
                    <script type="text/javascript">
                        function checkIfEmpty(id) {
                            if ($('#' + id).val()) {
                                $('#' + id).addClass('used');
                            } else {
                                $('#' + id).removeClass('used');
                            }
                        }
                    </script>
                </div>

            </div>
        </div>
    </div>
</div>
<?php  }else{ // daca nu mai e valabila ridicarea cupoanelor?>
<div class="layout">
    <div class="layout-wrapper">
        <h1 class="blue">Oops!! din pacate inregistrarea cupoanelor de reducere pentru aceasta campanie nu mai e valabila.
        </h1>
    </div>
</div>       
   <?php }?>
<?php // Footer ?>
<?php require_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/footer/footer_responsive.php" ); ?>
<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

<script src="/js/jquery_validate/jquery.validationEngine-ro.js"></script>
<script src="/js/jquery_validate/jquery.validationEngine.js"></script>
<script>
    jQuery(document).ready(function () {
        jQuery("#coupon-form").validationEngine('attach', {promptPosition: "topRight"});
    });

    function checkHELLO(field, rules, i, options) {
        if (field.val() != "HELLO") {
            return options.allrules.validate2fields.alertText;
        }
    }
</script>
<script>
    $("#data-nasterii").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+0',
        dateFormat: "yy-mm-dd",
        defaultDate: "-18y",
        showButtonPanel: true
    });

    $('#data-nasterii').change(function () {
        checkIfEmpty('data-nasterii');
    });
</script>
</body>
</html>
