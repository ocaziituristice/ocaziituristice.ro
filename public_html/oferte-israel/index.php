<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$metas_title = 'Hoteluri '.$row_loc['denumire'].' din '.$row_loc['den_tara'].' | '.$denumire_agentie;
$metas_description = 'Hoteluri '.$row_loc['denumire'].' aflate in zona '.$row_loc['den_zona'].' din '.$row_loc['den_tara'].'. La '.$denumire_agentie.' gasiti cele mai bune oferte la hotelurile din '.$row_loc['denumire'];
$metas_keywords = 'hoteluri '.$row_loc['denumire']. ', hoteluri '.$row_loc['denumire']. ' din '.$row_loc['den_zona'].', hoteluri '.$row_loc['denumire']. ' din '.$row_loc['den_tara'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?></title>
<meta name="description" content="<?php echo $metas_description; ?>" />
<meta name="keywords" content="<?php echo $metas_keywords; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="initialize(); load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">

<div id="NEW-destinatie">

  <h1 class="blue float-left">Oferte Israel</h1>
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  
  <br class="clear"><br>
  
  <img src="images/img-israel.jpg" alt="Oferte Israel">
  
  <br class="clear"><br>
  
  <p class="text-justify text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut eros nisi. Sed laoreet sed leo ut ullamcorper. Nunc ac tortor tempus, dignissim tellus non, pellentesque augue. Proin convallis lectus elit, sed ultricies neque mollis ac. Nam et convallis libero. Vivamus pretium felis ac eros laoreet, quis iaculis magna semper. Morbi consectetur imperdiet mattis. Vestibulum sodales vel eros vel vehicula. Quisque in ullamcorper augue. Pellentesque feugiat justo vitae tincidunt tincidunt. Nullam non erat pharetra quam placerat feugiat. Maecenas id ligula dui.</p>
  <p class="text-justify text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eros eros, vehicula sed magna ac, auctor egestas dui. Duis nisl est, bibendum eu eros quis, posuere ultricies velit. Sed facilisis molestie elit, ac laoreet nisl fermentum a. In id imperdiet nisi. Maecenas sit amet elit convallis, gravida diam eu, lobortis quam. Vivamus nec pellentesque nisl, sit amet consequat nunc. Sed orci risus, gravida sit amet odio ut, lacinia scelerisque arcu. Nulla rutrum consectetur diam ac varius. Nullam rhoncus urna tortor, et eleifend arcu placerat in. Duis mi risus, convallis sed fringilla a, scelerisque sed turpis.</p>
  
  <br class="clear"><br>
  
  <?php //afisare oferte speciale ?>  
  <?php
$oferte_rec=new AFISARE_SEJUR_NORMAL();
$oferte_rec->setAfisare(1, 4);
$oferte_rec->setRecomandata('da');
$oferte_rec->setRandom(1);
$oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_recomandate.tpl");

$oferte_rec->setTari('israel');
$nr_hoteluri=$oferte_rec->numar_oferte();
if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
	$cuvant_cheie = $_REQUEST['keyword'];
?>
<div class="ofRec NEW-round6px">
  <div class="tit blue">Recomandari hoteluri pentru <?php echo $cuvant_cheie; ?></div>
  <?php $oferte_rec->afiseaza();?>
  <br class="clear" />
</div>
<?php } ?>

  <br class="clear" /><br />
   
  <h3 class="blue">Destinatii recomandate pentru Programe Seniori</h3>
  <?php //afisare oferte speciale ?> 
  
  <br class="clear"><br>
  
  <div class="linkuri-diverse clearfix">
    <div class="float-left" style="width:750px;">
      <h3>Hoteluri <?php echo $row_loc['denumire']; ?></h3>
      <ul>
<?php
if(sizeof($filtruOf['stele'])>0) {
	krsort($filtruOf['stele']);
	foreach($filtruOf['stele'] as $i=>$nr_s) {
		echo '<li><a href="'.$link.'?optiuni=da&amp;stele='.fa_link($i).'" class="link-black float-left" style="width:240px;">Hoteluri de '.$i.' stele din '.$row_loc['denumire'].' </a></li>';
	}
}

if(sizeof($filtruOf['masa'])>0) {
	ksort($filtruOf['masa']);
	foreach($filtruOf['masa'] as $keyM=>$valueM) {
		foreach($valueM as $denM=>$nr_m) {
			echo '<li><a href="'.$link.'?optiuni=da&amp;masa='.fa_link($denM).'" class="link-black float-left" style="width:240px;">Hoteluri '.$row_loc['denumire'].' cu '.$denM.'</a></li>';
		}
	}
}

if(sizeof($filtruOf['facilitati'])>0) {
	krsort($filtruOf['facilitati']);
	foreach($filtruOf['facilitati'] as $denF=>$nr_f) {
		echo '<li><a href="'.$link.'?optiuni=da&amp;facilitati='.fa_link($denF).'" class="link-black float-left" style="width:240px;">Hoteluri '.$row_loc['denumire'].' cu '.$denF.'</a></li>';
	}
}
?>
      </ul>
    </div>
    <div class="float-left" style="width:230px;">
      <h3>Linkuri utile <?php echo $row_loc['denumire']; ?></h3>
      <ul>
        <li><a href="" class="link-black"></a></li>
      </ul>
    </div>
</div>

  <br class="clear"><br>
  
</div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
