        <div class="text-justify article">
          
    
        
          <br class="clear">
          
        
     
            <table width="100%" border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td width="50%" align="left" valign="top"><table width="100%" border="1" cellspacing="0" cellpadding="4">
                  <tr>
                    <th align="center" valign="top">STAȚIA</th>
                    <th align="center" valign="top">DUS</th>
                    <th align="center" valign="top">ÎNTORS</th>
                    <th align="center" valign="top">Frecventa</th>
                    <th align="center" valign="top">Tarif Dus-Intors</th>
                  </tr>
                  <tr>
                    <td colspan="5" align="left" valign="top">Transferuri din <strong>zona Moldova</strong> in fiecare zi de luni, joi si sambata</td>
                  </tr>

                  <tr>
                    <td align="left" valign="top"> Iasi - Parcare Billa la Gara


</td>
                    <td align="center" valign="top">00:10</td>
                    <td align="center" valign="top">02:20</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">60 euro</td>
                  </tr>
                          <tr>
                    <td align="left" valign="top">Roman - Gara</td>
                    <td align="center" valign="top">01:00	</td>
                    <td align="center" valign="top">01:30</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">60 euro</td>
                  </tr>
                          <tr>
                    <td align="left" valign="top">Bacau - Parcare Stadionul Municipal</td>
                    <td align="center" valign="top">01:30</td>
                    <td align="center" valign="top">01:00</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">60  euro</td>
                  </tr>
                          <tr>
                    <td align="left" valign="top">Adjud - 
                      Intersectie cu Onesti</td>
                    <td align="center" valign="top">02:10	</td>
                    <td align="center" valign="top">00:20</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">60 euro</td>
                  </tr>
                          <tr>
                    <td align="left" valign="top">Focsani -
                      Benzinaria Mol spre
                      Bucuresti</td>
                    <td align="center" valign="top">03:00	 </td>
                    <td align="center" valign="top">23:30</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">55 euro</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">Rm. Sarat 
                    Centru (inainte de pod)</td>
                    <td align="center" valign="top">03:30	</td>
                    <td align="center" valign="top">23:00</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">55 euro</td>
                  </tr>
                   <tr>
                    <td align="left" valign="top">Buzau - 
                     Parcare vis-a-vis de Vama- Petrom</td>
                    <td align="center" valign="top">04:00	</td>
                    <td align="center" valign="top">22:30</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">50 euro</td>
                  </tr>
                   <tr>
                    <td align="left" valign="top">Ploiesti - Petrom Metro</td>
                    <td align="center" valign="top">04:30</td>
                    <td align="center" valign="top">22:00</td>
                    <td align="center" valign="top">luni, joi,  sambata</td>
                    <td align="center" valign="top">35 euro</td>
                  </tr>
                   <tr>
                    <td colspan="5" align="left" valign="top">&nbsp;</td>
                  </tr>

                    <tr>
                    <td colspan="5" align="left" valign="top">Transferuri din <strong>zona Sibiu-Brasov</strong> in fiecare zi de luni si sambata:</td>
                  </tr>
                    <tr>
                    <td align="left" valign="top">Sibiu - Benzinaria Mol (Kaufland)</td>
                    <td align="center" valign="top">23:00</td>
                    <td align="center" valign="top">03:30</td>
                    <td align="center" valign="top">luni, sambata</td>
                    <td align="center" valign="top">60 euro</td>
                  </tr>
                    <tr>
                    <td align="left" valign="top">Brasov - Benzinaria Mol (Hotel Cubix)
                   </td>
                    <td align="center" valign="top">02:00</td>
                    <td align="center" valign="top">00:30</td>
                    <td align="center" valign="top">luni, sambata</td>
                    <td align="center" valign="top">55 euro</td>
                  </tr>
                    <tr>
                    <td align="left" valign="top">Sinaia- 
                      Gara -
                      Sinaia</td>
                    <td align="center" valign="top">03:30</td>
                    <td align="center" valign="top">23:00</td>
                    <td align="center" valign="top">luni, sambata</td>
                    <td align="center" valign="top">40 euro</td>
                  </tr>
                    <tr>
                    <td align="left" valign="top">Campina - 
                      Banesti 
                      – DN1 avion</td>
                    <td align="center" valign="top">04:00</td>
                    <td align="center" valign="top">22:30</td>
                    <td align="center" valign="top">luni, sambata</td>
                    <td align="center" valign="top">40 euro</td>
                  </tr>
                    <tr>
                    <td align="left" valign="top">                      Ploiesti -
                      Petrom 
                      Metro</td>
                    <td align="center" valign="top">04:30</td>
                    <td align="center" valign="top">22:00</td>
                    <td align="center" valign="top">luni, sambata</td>
                    <td align="center" valign="top">35 euro</td>
                  </tr>
                    <tr>
                    <td colspan="5" align="left" valign="top">&nbsp;</td>
                  </tr>
                   <tr>
                    <td colspan="5" align="left" valign="top">Transferuri din <strong>zona Oltenia</strong> in fiecare zi de luni</td>
                  </tr>
                   <tr>
                    <td align="left" valign="top">Craiova Mc Donalds – Calea Bucuresti</td>
                    <td align="center" valign="top">01:30</td>
                    <td align="center" valign="top">02:00</td>
                    <td align="center" valign="top">luni</td>
                    <td align="center" valign="top">55 euro</td>
                  </tr>
                   <tr>
                    <td align="left" valign="top">Bals - Petrom Intrare</td>
                    <td align="center" valign="top">02:00</td>
                    <td align="center" valign="top">01:30</td>
                    <td align="center" valign="top">luni</td>
                    <td align="center" valign="top">55 euro</td>
                  </tr>
                   <tr>
                    <td align="left" valign="top">Slatina - Rompetrol – Strada Pitesti</td>
                    <td align="center" valign="top">02:30</td>
                    <td align="center" valign="top">01:00</td>
                    <td align="center" valign="top">luni</td>
                    <td align="center" valign="top">50 euro</td>
                  </tr>
                  
                    <tr>
                    <td align="left" valign="top">Pitesti - Mc Donald’s Gara</td>
                    <td align="center" valign="top">04:00</td>
                    <td align="center" valign="top">23:30</td>
                    <td align="center" valign="top">luni</td>
                    <td align="center" valign="top">45 euro</td>
                  </tr>
                    <tr>
                    <td colspan="5" align="left" valign="top">&nbsp;</td>
                  </tr>
                  
                  
                  
                  	

                  
                  <tr>
                    <td align="left" valign="top"><strong>BUCUREȘTI - Vis-a-vis de Coloane, Gara de Nord</strong></td>
                    <td align="center" valign="top">06.00</td>
                    <td align="center" valign="top">20.00</td>
                    <td align="center" valign="top">zilnic</td>
                    <td align="center" valign="top">25 euro</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">GIURGIU - Rompetrol – vis-à-vis de Kaufland</td>
                    <td align="center" valign="top">08.00</td>
                    <td align="center" valign="top">18.30</td>
                    <td align="center" valign="top">zilnic</td>
                    <td align="center" valign="top">25 euro</td>
                  </tr>
                  <tr>
                    <td colspan="5" align="left" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">VARNA - Catedrala
(pentru statiunile din sud se
va face transfer cu midibus/autocar)</td>
                    <td align="center" valign="top">11.15</td>
                    <td align="center" valign="top">15.00</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">C-TIN ȘI ELENA - Hotel Romance langa benzinarie</td>
                    <td align="center" valign="top">11.30</td>
                    <td align="center" valign="top">14.45</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">SUNNY DAY - Poarta de lângă șoseaua principală</td>
                    <td align="center" valign="top">11.40</td>
                    <td align="center" valign="top">14.40</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">NISIPURILE DE AUR - Hotel Melia Grand Hermitage</td>
                    <td align="center" valign="top">11.45</td>
                    <td align="center" valign="top">14.30</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">ALBENA - Centrul de informare turistică</td>
                    <td align="center" valign="top">12.00</td>
                    <td align="center" valign="top">14.15</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">BALCIC - Hotel Bisser</td>
                    <td align="center" valign="top">12.15</td>
                    <td align="center" valign="top">14.00</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="5" align="left" valign="top">&nbsp;</td>
                    </tr>
                  <tr>
                    <td align="left" valign="top">OBZOR - Hotel Miramar</td>
                    <td align="center" valign="top">12.00</td>
                    <td align="center" valign="top">13.45</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">SUNNY BEACH - vis-a-vis de Hotel Kuban</td>
                    <td align="center" valign="top">12.30</td>
                    <td align="center" valign="top">13.00</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">NESSEBAR - Hotel Sol Palace</td>
                    <td align="center" valign="top">12.45</td>
                    <td align="center" valign="top">12.45</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
<p>NOU! OPTIONAL OFERIM TURISTILOR TRANSFER PANA LA HOTELUL ALES – SUPLIMENT 5 EURO/PERSOANA DUS-INTORS! <br />
REDUCERE 5 euro - pentru copii cu varsta intre 2 si 12 ani!</p>
            <?php /*?><div class="float-right black uppercase smaller-09em underline">Transport asigurat de <strong>Tiladelfia Turism</strong></div><?php */?>
          </div>
          
