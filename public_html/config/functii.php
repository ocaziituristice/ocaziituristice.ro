<?php include($_SERVER['DOCUMENT_ROOT'].'/config/seteri.php');
function redirect_php($link) {
	header("Location: ".$link."");
}

function redirect_java($link) {
	echo '<script> document.location.href=\''.$link.'\'; </script>';
}

function encrypt($pure_string,$key) {
    $dirty = array("+", "/", "=");
    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $_SESSION['iv'] = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $_SESSION['iv']);
    $encrypted_string = base64_encode($encrypted_string);
    return str_replace($dirty, $clean, $encrypted_string);
}

function decrypt($encrypted_string,$key) { 
    $dirty = array("+", "/", "=");
    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");

    $string = base64_decode(str_replace($clean, $dirty, $encrypted_string));

    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $key,$string, MCRYPT_MODE_ECB, $_SESSION['iv']);
    return $decrypted_string;
}

function generatePassword($length) {
	// start with a blank password
	$password = "";
	// define possible characters
	$possible = "0123456789abcdefghijklmnpqrstvwxyz";
	// set up a counter
	$i = 0;
	// add random characters to $password until $length is reached
	while ($i < $length) {
		// pick a random character from the possible ones
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
		// we don't want this character if it's already in the password
		if (!strstr($password, $char)) {
			$password .= $char;
			$i++;
		}
	}
	// done!
	return $password;
}

function exista_mail($tabel, $camp, $email) {
	$sel="select $camp from $tabel where $camp = '$email' ";
	$que=mysql_query($sel) or die(mysql_error());
	$nr=mysql_num_rows($que);
	@mysql_free_result($que);
	if($nr==0) return true; else return false;
}

function validate_email($email) {
	if(preg_match("/^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,6}$/i", $email)) return true;
	else return false;
}

function fa_link_poza ($nume) {
	$de_inlocuit=array('-',' ','*','/',',','&','\'','(',')','ü','Ü','ù','Ù','û','Û','ú','Ú','ö','Ö','ò','Ò','ô','Ô','ó','Ó','ÿ','ý','Ý','õ','Õ','ñ','Ñ', '!', '?','%');
	$inlocuit=array('_d_','-','_steluta_','__','_v_','_w_','_sg_','_ps_','_pd_','_up_','_upm_','_ual_','_ualm_','_us_','_usm_','_uar_','_uarm_','_ot_','_otm_','_oal_','_oalm_','_os_','_osm_','_oar_','_oarm_','_ygrecp_','_ygrecar_','_ygrecarm_','_otilda_','_otildam_','_ntilda_','_ntildam_' , '_exclam_', '_q_','_pr_');
	$link_nume=str_replace($de_inlocuit,$inlocuit,strtolower($nume));
	$link_nume=preg_replace("/[^a-zA-Z0-9.\_\d]/i", "", $link_nume);
	return $link_nume;
}

function get_den_tip_oferta($id_tip_oferta) {
	$get_tip_oferta = "SELECT denumire_tip_oferta FROM tip_oferta WHERE id_tip_oferta = '".$id_tip_oferta."' ";
	$rez_tip_oferta = mysql_query($get_tip_oferta) or die(mysql_error());
	$row_tip_oferta = mysql_fetch_array($rez_tip_oferta);
	
	$rand_tip_oferta = $row_tip_oferta["denumire_tip_oferta"];
	mysql_free_result($rez_tip_oferta);
	return $rand_tip_oferta;
}

function get_id_tara($nume_tara) {
	$get_tara='SELECT id_tara FROM tari WHERE TRIM(LOWER(denumire)) = "'.strtolower($nume_tara).'" ';
	$rez_tara=mysql_query($get_tara) or die(mysql_error());
	$row_tara=mysql_fetch_array($rez_tara);
	
	$rand_tara=$row_tara["id_tara"];
	mysql_free_result($rez_tara);
	return $rand_tara;
}

function get_den_tara($id_tara) {
	$get_tara="SELECT denumire FROM tari WHERE id_tara = '".$id_tara."' ";
	$rez_tara=mysql_query($get_tara) or die(mysql_error());
	$row_tara=mysql_fetch_array($rez_tara);
	
	$rand_tara=$row_tara["denumire"];
	mysql_free_result($rez_tara);
	return $rand_tara;
}

function get_id_tara_by_zona($nume_zona) {
	$get_zona='SELECT id_tara FROM zone WHERE TRIM(LOWER(denumire)) = "'.strtolower($nume_zona).'" ';
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["id_tara"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

function get_id_tara_by_id_hotel($id_hotel) {
	$get_tara = "SELECT zone.id_tara FROM hoteluri
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	WHERE hoteluri.id_hotel = '".$id_hotel."' ";
	$rez_tara = mysql_query($get_tara) or die(mysql_error());
	$row_tara = mysql_fetch_array($rez_tara);
	
	$rand_tara = $row_tara['id_tara'];
	mysql_free_result($rez_tara);
	return $rand_tara;
}

//pt_zona____
function get_id_zona($nume_zona, $id_tara) {
	$get_zona='SELECT id_zona FROM zone WHERE TRIM(LOWER(denumire)) = "'.strtolower($nume_zona).'" '; if($id_tara) $get_zona=$get_zona.' and id_tara = "'.$id_tara.'" ';
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["id_zona"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

function get_den_zona($id_zona) {
	$get_zona='SELECT denumire FROM zone WHERE id_zona = "'.$id_zona.'" ';
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["denumire"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

function verif_zona($nume_zona, $id_tara) {
	$get_zona='SELECT id_zona FROM zone WHERE TRIM(LOWER(denumire)) = "'.strtolower($nume_zona).'" '; if($id_tara) $get_zona=$get_zona.' and id_tara = "'.$id_tara.'" ';
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["id_zona"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

//pt_localitate____
function get_id_localitate($nume_localitate, $id_zona=NULL) {
	$get_loc = 'SELECT id_localitate FROM localitati WHERE TRIM(LOWER(denumire)) = "'.strtolower($nume_localitate).'" ';
	if($id_zona) $get_loc = $get_loc.' and id_zona = "'.$id_zona.'" ';
	$rez_loc = mysql_query($get_loc) or die(mysql_error());
	$row_loc = mysql_fetch_array($rez_loc);
	
	$rand_loc = $row_loc["id_localitate"];
	mysql_free_result($rez_loc);
	return $rand_loc;
}
function get_detalii_by_id_localitate($id_localitate) {
	$sel_get_zona = "SELECT zone.denumire as denumire_zona,zone.id_zona as id_zona, zone.specificatie as specificatie, tari.denumire as denumire_tara,localitati.descriere as descriere_localitate, localitati.denumire as denumire_localitate,
	localitati.poza1 as imagine1, localitati.poza2 as imagine2, localitati.poza3 as imagine3,
	tari.id_tara as id_tara from zone
	INNER JOIN localitati ON localitati.id_zona = zone.id_zona 
	INNER JOIN tari on zone.id_tara=tari.id_tara
	WHERE localitati.id_localitate = '".$id_localitate."' ";
	$get_zona = mysql_query($sel_get_zona) or die(mysql_error());
	$row_zona = mysql_fetch_array($get_zona);
	$zona = array();
	$zona['id_zona']=$row_zona['id_zona'];
	$zona['denumire_zona']=$row_zona['denumire_zona']; 
	$zona['denumire_localitate']=$row_zona['denumire_localitate']; 
	$zona['specificatie']=$row_zona['specificatie']; 
	$zona['id_tara']=$row_zona['id_tara'];
	$zona['denumire_tara']=$row_zona['denumire_tara']; 
	$zona['descriere_localitate']=$row_zona['descriere_localitate'];
	$zona['imagine1']=$row_zona['imagine1'];
	$zona['imagine2']=$row_zona['imagine2'];
	$zona['imagine3']=$row_zona['imagine3'];
	 
	mysql_free_result($get_zona);
	return $zona;
}
function get_detalii_by_id_zona($id_zona) {
	$sel_get_zona = "SELECT zone.denumire as denumire_zona,zone.id_zona as id_zona, zone.specificatie as specificatie, tari.denumire as denumire_tara,zone.descriere as descriere_zona,
	zone.poza1 as imagine1, zone.poza2 as imagine2, zone.poza3 as imagine3,zone.localitati_plecare_avion as localitati_plecare_avion,
	tari.id_tara as id_tara from zone
	INNER JOIN tari on zone.id_tara=tari.id_tara
	WHERE zone.id_zona = '".$id_zona."' ";
	$get_zona = mysql_query($sel_get_zona) or die(mysql_error());
	$row_zona = mysql_fetch_array($get_zona);
	$zona = array();
	$zona['id_zona']=$row_zona['id_zona'];
	$zona['denumire_zona']=$row_zona['denumire_zona']; 
	$zona['specificatie']=$row_zona['specificatie']; 
	$zona['id_tara']=$row_zona['id_tara'];
	$zona['denumire_tara']=$row_zona['denumire_tara']; 
	$zona['descriere_zona']=$row_zona['descriere_zona']; 
	$zona['imagine1']=$row_zona['imagine1'];
	$zona['imagine2']=$row_zona['imagine2'];
	$zona['imagine3']=$row_zona['imagine3'];
	$zona['localitati_plecare_avion']=$row_zona['localitati_plecare_avion'];
	 
	mysql_free_result($get_zona);
	return $zona;
}

function get_id_localitate_by_id_hotel($id_hotel) {
	$get_loc = "SELECT hoteluri.locatie_id FROM hoteluri
	WHERE hoteluri.id_hotel = '".$id_hotel."' ";
	$rez_loc = mysql_query($get_loc) or die(mysql_error());
	$row_loc = mysql_fetch_array($rez_loc);
	
	$rand_loc = $row_loc['id_tara'];
	mysql_free_result($rez_loc);
	return $rand_loc;
}

function get_den_localitate($id_localitate) {
	$get_zona="SELECT denumire FROM localitati WHERE id_localitate = '".$id_localitate."' ";
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["denumire"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

function verif_localitate($nume_localitate, $id_zona) {
	$get_zona='SELECT id_localitate FROM localitati WHERE TRIM(LOWER(denumire)) = "'.strtolower($nume_localitate).'" '; if($id_zona) $get_zona=$get_zona.' and id_zona = "'.$id_zona.'" ';
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["id_localitate"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

//hotel____
function get_id_hotel($nume_hotel, $id_localitate) {
	$get_zona='SELECT id_hotel FROM hoteluri WHERE TRIM(LOWER(nume)) = "'.strtolower($nume_hotel).'" '; if($id_localitate) $get_zona=$get_zona.' and locatie_id = "'.$id_localitate.'" ';
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["id_hotel"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

function get_den_hotel($id_hotel) {
	$get_zona="SELECT nume FROM hoteluri WHERE id_hotel = '".$id_hotel."' ";
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["nume"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

function get_idhotel($nume_hotel, $nume_localitate) {
	$get_idhotel='SELECT
	hoteluri.id_hotel
	FROM hoteluri
	INNER JOIN localitati ON localitati.id_localitate=hoteluri.locatie_id
	WHERE TRIM(LOWER(hoteluri.nume))="'.strtolower($nume_hotel).'"
	AND TRIM(LOWER(localitati.denumire))="'.strtolower($nume_localitate).'"	';
	$rez_idhotel=mysql_query($get_idhotel) or die(mysql_error());
	$row_idhotel=mysql_fetch_array($rez_idhotel);
 $rand_idhotel=$row_idhotel["id_hotel"];
	mysql_free_result($rez_idhotel);
	return $rand_idhotel;
}

function get_nr_vizite($id_hotel) {
	$sel_nr ="SELECT COUNT(oferte_vizitate.id) AS number FROM oferte_vizitate 
INNER JOIN oferte ON oferte.id_oferta = oferte_vizitate.id_oferta
 WHERE oferte.id_hotel = '".$id_hotel."'";
	$que_nr = mysql_query($sel_nr) or die(mysql_error());
	$row_nr = mysql_fetch_array($que_nr);
	$nr_vizite = $row_nr['number'];
	
	$sel_nr = "SELECT COUNT(oferte_vizitate.id) AS number FROM oferte_vizitate WHERE oferte_vizitate.id_hotel = '".$id_hotel."'";
	$que_nr = mysql_query($sel_nr) or die(mysql_error());
	$row_nr = mysql_fetch_array($que_nr);
	
	$nr_vizite = $nr_vizite+$row_nr['number'];
	mysql_free_result($que_nr);
	
	return $nr_vizite;
}

function get_nr_rezervari($id_hotel) {
	$sel_nr = "SELECT COUNT(cerere_rezervare.id_cerere) AS number FROM cerere_rezervare INNER JOIN oferte ON cerere_rezervare.id_oferta = oferte.id_oferta WHERE oferte.id_hotel = '".$id_hotel."' GROUP BY oferte.id_hotel ";
	$que_nr = mysql_query($sel_nr) or die(mysql_error());
	$row_nr = mysql_fetch_array($que_nr);
	
	$nr_rezervari = $row_nr['number'];
	mysql_free_result($que_nr);
	return $nr_rezervari;
}

function verif_hotel($nume_hotel, $id_localitate) {
	$get_zona='SELECT id_hotel FROM hoteluri WHERE TRIM(LOWER(nume)) = "'.strtolower($nume_hotel).'" and locatie_id = "'.$id_localitate.'" ';
	$rez_zona=mysql_query($get_zona) or die(mysql_error());
	$row_zona=mysql_fetch_array($rez_zona);
	
	$rand_zona=$row_zona["id_hotel"];
	mysql_free_result($rez_zona);
	return $rand_zona;
}

function cautare_live($id_hotel) {
	$sel = "SELECT cautare_live FROM hoteluri WHERE id_hotel = '".$id_hotel."' ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	mysql_free_result($que);
	return $row['cautare_live'];
}

function fa_link($nume) {
	$nume=htmlspecialchars_decode($nume);
	$de_inlocuit=array('-','&#039;','/','.',',',' ','&','\'','(',')','ü','Ü','ù','Ù','û','Û','ú','Ú','ö','Ö','ò','Ò','ô','Ô','ó','Ó','ÿ','ý','Ý','õ','Õ','ñ','Ñ', '!', '?','*','%');
	$inlocuit=array('_d_','_apostrof_','__','punct','_v_','-','_w_','_sg_','_ps_','_pd_','_up_','_upm_','_ual_','_ualm_','_us_','_usm_','_uar_','_uarm_','_ot_','_otm_','_oal_','_oalm_','_os_','_osm_','_oar_','_oarm_','_ygrecp_','_ygrecar_','_ygrecarm_','_otilda_','_otildam_','_ntilda_','_ntildam_' , '_exclam_', '_q_','_stele_','_pr_');
	$link_nume=str_replace($de_inlocuit,$inlocuit,strtolower($nume));
	return $link_nume;
}

function fa_link_oferta($nume) {
	$nume=htmlspecialchars_decode($nume);
	$de_inlocuit=array(' - ','&#039;','/','.',',',' ','&','\'','(',')','ü','Ü','ù','Ù','û','Û','ú','Ú','ö','Ö','ò','Ò','ô','Ô','ó','Ó','ÿ','ý','Ý','õ','Õ','ñ','Ñ', '!', '?','*','%');
	$inlocuit=array('-','_apostrof_','__','punct','_v_','-','_w_','_sg_','_ps_','_pd_','_up_','_upm_','_ual_','_ualm_','_us_','_usm_','_uar_','_uarm_','_ot_','_otm_','_oal_','_oalm_','_os_','_osm_','_oar_','_oarm_','_ygrecp_','_ygrecar_','_ygrecarm_','_otilda_','_otildam_','_ntilda_','_ntildam_' , '_exclam_', '_q_','_stele_','_pr_');
	$link_nume=str_replace($de_inlocuit,$inlocuit,strtolower($nume));
	return $link_nume;
}

function fa_link_circuit($nume) {
	$nume=htmlspecialchars_decode($nume);
	$link_nume=preg_replace('![^a-zA-Z0-9]+!i', ' ', $nume);
	$link_nume=str_replace(' ', '-', strtolower($link_nume));
	return $link_nume;
}

function fa_link_hotel($localitate, $denumire_hotel, $id_hotel=NULL)
{	
	$link_hotel=$sitepath.'/cazare-'.fa_link($localitate).'/'.fa_link($denumire_hotel).'/';
	return $link_hotel;
}

function make_link_oferta($localitate, $denumire_hotel, $denumire_scurta_oferta=NULL, $id_oferta=NULL) {
	$value='/cazare-'.fa_link_oferta($localitate).'/'.fa_link_oferta($denumire_hotel).'/';
	if(($denumire_scurta_oferta!=NULL) and ($id_oferta!=NULL)) $value.=fa_link_oferta($denumire_scurta_oferta).'-'.$id_oferta.'.html';
	return $value;
}

function make_link_circuit($denumire_hotel, $id_oferta, $directory=NULL) {
	$value = '/circuit/'.fa_link_circuit($denumire_hotel).'-'.$id_oferta;
	if($directory=='da') {
		$value .= '/';
	} else {
		$value .= '.html';
	}
	return $value;
}

function make_link_filters($link_p, $transport, $stele, $masa, $distanta, $plecare_avion, $plecare_autocar, $concept, $facilitati, $data_plecare=NULL,$search=NULL) {
	$link_nou='';
	if(($transport!='toate') and ($transport!='')) $link_nou=$link_nou.'&amp;transport='.$transport;
	if(($masa!='toate') and ($masa!='')) $link_nou=$link_nou.'&amp;masa='.$masa;
	if(($stele!='toate') and ($stele!='')) $link_nou=$link_nou.'&amp;stele='.$stele;
	if(($distanta!='toate') and ($distanta!='')) $link_nou=$link_nou.'&amp;distanta='.$distanta;
	if($transport!='fara-transport') {
	if(($plecare_avion!='toate' && $transport!='toate') and ($plecare_avion!='' && $transport!='')) $link_nou=$link_nou.'&amp;plecare-avion='.$plecare_avion;
	}
	if(($plecare_autocar!='toate' && $transport!='toate') and ($plecare_autocar!='' && $transport!='')) $link_nou=$link_nou.'&amp;plecare-autocar='.$plecare_autocar;
	if(($concept!='toate') and ($concept!='')) $link_nou=$link_nou.'&amp;concept='.$concept;
	if(($facilitati!='toate') and ($facilitati!='')) $link_nou=$link_nou.'&amp;facilitati='.$facilitati;
	/*if($_GET['search']=='da') {$link_search="&search=da&";}	
	if(isset($_GET['adulti'])) {$link_search.='adulti='.$_GET['adulti']."&";} 
	if(isset($_GET['copii'])) {$link_search.='copii='.$_GET['copii']."&";} 
	if(isset($_GET['age'][0])) {$link_search.='age[0]='.$_GET['age'][0]."&";}
	if(isset($_GET['age[1]'])) {$link_search.='age[0]='.$_GET['age[1]']."&";}
	if(isset($_GET['age[2]'])) {$link_search.='age[0]='.$_GET['age[2]']."&";}
	$link_nou=$link_nou.$link_search;*/
	/*if($data_plecare) {*/
		/*if($data_plecare===TRUE) $link_nou=$link_nou.'&amp;data-plecare=';
		else $link_nou=$link_nou.'&amp;data-plecare='.$data_plecare;*/
		//if($data_plecare===TRUE) $link_nou=$link_nou.'&amp;checkin=';
		//else $link_nou=$link_nou.'&amp;checkin='.$data_plecare;
	/*}*/
	if($link_nou!='') $link_p=$link_p.$link_nou;
	
	return $link_p;
}

function make_link_filters_circuite($link_p, $transport, $nr_zile, $plecare_avion) {
	$link_nou='';
	if(($transport!='toate') and ($transport!='')) $link_nou=$link_nou.'&amp;transport='.$transport;
	if(($nr_zile!='toate') and ($nr_zile!='')) $link_nou=$link_nou.'&amp;nr_zile='.$nr_zile;
	if(($plecare_avion!='toate' && $transport!='toate') and ($plecare_avion!='' && $transport!='')) $link_nou=$link_nou.'&amp;plecare-avion='.$plecare_avion;
	if($link_nou!='') $link_p=$link_p.$link_nou;
	
	return $link_p;
}

function remove_current_filter($string, $option) {
	$arr_string = explode(',',$string);
	
	if(($key_string = array_search($option, $arr_string)) !== false) {
		unset($arr_string[$key_string]);
	}
	
	$value='';
	$i=0;
	foreach($arr_string as $value_string) {
		$i++;
		$value.=$value_string;
		if($i<sizeof($arr_string)) $value.=',';
	}
	
	return $value;
}

function remove_special_characters($string) {
	$find[] = 'â€œ';  // left side double smart quote
	$find[] = 'â€';  // right side double smart quote
	$find[] = 'â€˜';  // left side single smart quote
	$find[] = 'â€™';  // right side single smart quote
	$find[] = 'ï¿½';  // left side single smart quote
	$find[] = 'ï¿½';  // right side single smart quote
	$find[] = 'â€¦';  // elipsis
	$find[] = 'â€"';  // em dash
	$find[] = 'â€"';  // en dash
	$find[] = '–';  // en dash
	$find[] = '™';  // trademark
	$find[] = '©';  // copyright
	$find[] = '…';  // elipsis
	$find[] = '¼';  // one quarter
	$find[] = '½';  // one half
	$find[] = '¾';  // three quarters
	$find[] = ' & ';  // space ampersand space
	$find[] = '?'; // question mark
	$find[]= '€'; // euro
	$find[]= '"'; // ghilimele stanga
	$find[]='•'; //puncte
	$find[]='&#61607;';
	$find[]="'";
	$find[]="*";
	$replace[] = '`';
	$replace[] = '`';
	$replace[] = "`";
	$replace[] = "`";
	$replace[] = "`";
	$replace[] = "`";
	$replace[] = "...";
	$replace[] = "-";
	$replace[] = "-";
	$replace[] = "-";
	$replace[] = "(tm)";
	$replace[] = "(c)";
	$replace[] = "...";
	$replace[] = "1/4";
	$replace[] = "1/2";
	$replace[] = "3/4";
	$replace[] = " and ";
	$replace[] = " ";
	$replace[]= "euro";
	$replace[]= '"';
	$replace[]='-';
	$replace[]='-';
	$replace[]='`';
	$replace[]="";
	$string=trim($string);
	$stringul= str_replace($find, $replace, $string);
	for ($i=1;$i<=31;$i++)
	if ($i<>13) $stringul=str_replace(chr($i),"",$stringul);
	return trim($stringul);
}

function fa_link_vechi($nume) {
	$nume=htmlspecialchars_decode($nume);
	$de_inlocuit=array(' ','&#039;','/','.',',','-','&','\'','(',')','ü','Ü','ù','Ù','û','Û','ú','Ú','ö','Ö','ò','Ò','ô','Ô','ó','Ó','ÿ','ý','Ý','õ','Õ','ñ','Ñ', '!', '?','*');
	   $inlocuit=array('_','_apostrof_','__','punct','_v_','_d_','_w_','_sg_','_ps_','_pd_','_up_','_upm_','_ual_','_ualm_','_us_','_usm_','_uar_','_uarm_','_ot_','_otm_','_oal_','_oalm_','_os_','_osm_','_oar_','_oarm_','_ygrecp_','_ygrecar_','_ygrecarm_','_otilda_','_otildam_','_ntilda_','_ntildam_' , '_exclam_', '_q_','_stele_');
	$link_nume=str_replace($de_inlocuit,$inlocuit,strtolower($nume));
	return $link_nume;
}

function desfa_link($nume) {
	$de_inlocuit=array('&#039;','(',')','\'','and',',',' ','-','/','.','/','ü','Ü','ù','Ù','û','Û','ú','Ú','ö','Ö','ò','Ò','ô','Ô','ó','Ó','ÿ','ý','Ý','õ','Õ','ñ','Ñ', '!' , '?','*');
	$inlocuit=array('_apostrof_','_ps_','_pd_','_sg_','_w_','_v_','-','_d_','__','punct','slash','_up_','_upm_','_ual_','_ualm_','_us_','_usm_','_uar_','_uarm_','_ot_','_otm_','_oal_','_oalm_','_os_','_osm_','_oar_','_oarm_','_ygrecp_','_ygrecar_','_ygrecarm_','_otilda_','_otildam_','_ntilda_','_ntildam_', '_exclam_' , '_q_','_stele_');
	$link_nume=str_replace($inlocuit,$de_inlocuit,strtolower($nume));
	return $link_nume;
}

function inserare_frumos($string) {
	$string=htmlspecialchars($string, ENT_QUOTES);
	return $string;
}

function fa_link_search($string) {
	//$string=htmlspecialchars($string, ENT_QUOTES);
	$de_inlocuit=array(' ');
	$inlocuit=array('_');
	$string=str_replace($de_inlocuit,$inlocuit,strtolower($string));
	return $string;
}

function desfa_link_seach($string) {
	$de_inlocuit=array('_');
	$inlocuit=array('-');
	$string=str_replace($de_inlocuit,$inlocuit,strtolower($string));
	$string=htmlspecialchars($string, ENT_QUOTES);
	return $string;
}

function selecteaza_tip_supliment() {
	$sel_supl='select * from tip_suplimente Group by id_sup ';
	$que_supl=mysql_query($sel_supl) or die(mysql_error());
	$tip_supliment=array();
	while($row_supl=mysql_fetch_array($que_supl)) {
		$tip_supliment[$row_supl['id_sup']]=$row_supl['denumire'];
	}
	@mysql_free_result($que_supl);
	return $tip_supliment;
}

function afisare_frumos($string, $tip=NULL) {
	$string=htmlspecialchars_decode($string);
	if($tip=='nl2p') echo nl2p($string);
	elseif($tip=='nl2li') echo nl2li($string);
	elseif($tip=='NEW_nl2li') echo NEW_nl2li($string);
	else echo nl2br($string);
}


function nl2p($str) {
	$value = str_replace('<p></p>', '', '<p>' . preg_replace('#\n|\r#', '</p>$0<p>', $str) . '</p>');

	return $value;
} 

function nl2li($str) {
	$value = str_replace('<li></li>', '', '<li>' . preg_replace('#\n|\r#', '</li>$0<li>', $str) . '</li>');
	$value = str_replace('<li>- ', '<li>', $value);
	return $value;
}

function NEW_nl2li($str) {
	$value = preg_replace('#\n|\r#', '</span>$0<span>- ', $str);
	$value = str_replace('<span></span>', '', '<span>- '.$value.'</span>');
	$value = str_replace('<span>-  </span>', '<br />', $value);
	$value = change_words($value);
	return $value;
}
function change_words($value) {
	$descNew_search = $GLOBALS['descNew_search'];
	$descNew_replace = $GLOBALS['descNew_replace'];
	$value = str_replace($descNew_search, $descNew_replace, $value);
	for($i=1;$i<=31;$i++)
	if($i<>13) $value=str_replace(chr($i),"",$value);
	return $value;
}

function pret_minim_sejur($id_oferta, $id_hotel, $id_transport, $id_tip_oferta) {
$sel="select
data_pret_oferta.pret,
data_pret_oferta.moneda,
oferte.id_oferta,
oferte.denumire,
oferte.exprimare_pret
from
oferte
left join data_pret_oferta on oferte.id_oferta = data_pret_oferta.id_oferta ";
if($id_tip_oferta) $sel=$sel." inner join oferta_sejur_tip on (oferte.id_oferta = oferta_sejur_tip.id_oferta and oferta_sejur_tip.id_tip_oferta = '".$id_tip_oferta."') ";
$sel=$sel." where
oferte.valabila = 'da'
and (((data_pret_oferta.data_end is null or data_pret_oferta.data_end = '0000-00-00') and data_pret_oferta.data_start >= now()) or ((data_pret_oferta.data_end is not null and data_pret_oferta.data_end <> '0000-00-00') and data_pret_oferta.data_end>= now())) ";
if($id_oferta) $sel=$sel." and oferte.id_oferta = '".$id_oferta."' ";
if($id_hotel) $sel=$sel." and oferte.id_hotel = '".$id_hotel."' ";
if($id_transport) $sel=$sel." and oferte.id_transport = '".$id_transport."' ";
$sel=$sel." Group by data_pret_oferta.data_start, data_pret_oferta.data_end,
data_pret_oferta.pret, 
data_pret_oferta.moneda, 
data_pret_oferta.tip_camera 
Order by data_pret_oferta.pret ";
//echo $sel.'<br/>';
$que=mysql_query($sel) or die(mysql_error());
$row=mysql_fetch_array($que);
@mysql_free_result($que);
return $row;
}

function get_tip_oferta_tari($id_tara) {
	$selT="select
	tip_oferta.id_tip_oferta,
	tip_oferta.denumire_tip_oferta
	from tip_oferta
	inner join tari_tip_sejur on (tari_tip_sejur.id_tara = '".$id_tara."' and tip_oferta.id_tip_oferta = tari_tip_sejur.id_tip_oferta)
	where tip_oferta.activ = 'da'
	and tip_oferta.apare_site = 'da'
	and tip_oferta.tip is not null
	Group by tip_oferta.id_tip_oferta
	Order by tip_oferta.denumire_tip_oferta ASC ";
	$queT=mysql_query($selT) or die(mysql_error());
	while($rowT=mysql_fetch_assoc($queT)) {
		$tip[$rowT['id_tip_oferta']]=$rowT['denumire_tip_oferta'];
	} @mysql_free_result($queT);
	return $tip;
}

function get_tari($id_tipuri, $id_transport, $id_zona, $id_localitate, $early, $id_curent=NULL, $last_minute=NULL, $order=NULL) {
	$selT="SELECT count(hoteluri.id_hotel) as nr_hoteluri, tari.id_tara, tari.denumire
	FROM oferte ";
	if($id_tipuri) $selT .= "INNER JOIN (
		SELECT oferta_sejur_tip.id_oferta, oferta_sejur_tip.id_tip_oferta
		FROM oferta_sejur_tip
		LEFT JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
		WHERE tip_oferta.id_tip_oferta IN (".$id_tipuri.")
		GROUP BY oferta_sejur_tip.id_oferta
		ORDER BY tip_oferta.denumire_tip_oferta
	) AS tipuri_oferta ON tipuri_oferta.id_oferta = oferte.id_oferta ";
	$selT .= "INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara ";
	if($early) $selT .= "INNER JOIN early_booking ON (
		oferte.id_oferta = early_booking.id_oferta
		AND early_booking.end_date >= NOW()
		AND early_booking.discount >'0'
		AND early_booking.tip = 'sejur'
	) ";
	if($id_transport) $selT .= "INNER JOIN transport ON (
		oferte.id_transport = transport.id_trans
		AND oferte.id_transport = '".$id_transport."'
	) ";
	$selT .= "WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit' ";
	if($last_minute) $selT .= "AND oferte.last_minute = 'da' "; else $selT .= "AND oferte.last_minute = 'nu' ";
	if($id_zona) $selT .= "AND zone.id_zona = '".$id_zona."' ";
	if($id_localitate) $selT .= "AND localitati.id_localitate = '".$id_localitate."' ";
	if($id_curent) $selT .= "AND tari.id_tara='".$id_curent."' ";
	$selT .= "GROUP BY tari.id_tara ";
	if($order=='hoteluri') {$selT .= "ORDER BY nr_hoteluri DESC";}
		elseif($order) $selT=$selT." ORDER BY tari.denumire ".$order." "; 
		else $selT=$selT." ORDER BY tari.denumire ASC ";
	//if(!$err_logare_admin)  echo $selT;
	
	$queT = mysql_query($selT) or die(mysql_error());
	while($rowT = mysql_fetch_assoc($queT)) {
		$tip[$rowT['id_tara']] = $rowT['denumire'];
	} @mysql_free_result($queT);
	
	return $tip;
}

function get_zone($id_tipuri, $id_transport, $id_localitate, $id_tara, $early, $id_exclud=NULL, $order=NULL) {
	$selT="SELECT COUNT(DISTINCT(oferte.id_hotel)) AS numar_hot_zona,
	zone.id_zona,
	zone.denumire
	FROM oferte ";
	if(preg_match('/\\d/',$id_tipuri) ) $selT=$selT." INNER JOIN (SELECT
		oferta_sejur_tip.id_oferta,
		oferta_sejur_tip.id_tip_oferta
		FROM oferta_sejur_tip
		LEFT JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
		WHERE tip_oferta.id_tip_oferta IN (".$id_tipuri.")
		GROUP BY oferta_sejur_tip.id_oferta
		ORDER BY tip_oferta.denumire_tip_oferta)
	AS tipuri_oferta ON tipuri_oferta.id_oferta = oferte.id_oferta ";
	$selT=$selT." INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara ";
	if($early) $selT=$selT." INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta
		AND early_booking.end_date >= now()
		AND early_booking.discount >'0'
		AND early_booking.tip = 'sejur') ";
	if($id_transport) $selT=$selT." INNER JOIN transport on (oferte.id_transport = transport.id_trans AND oferte.id_transport = '".$id_transport."') ";
	$selT=$selT." WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND oferte.last_minute = 'nu' ";
	if($id_localitate) $selT=$selT." AND localitati.id_localtiate = '".$id_localitate."' ";
	if($id_tara) $selT=$selT." AND tari.id_tara = '".$id_tara."' ";
	if($id_exclud) $selT=$selT." AND zone.id_zona<>'".$id_exclud."' ";
	$selT=$selT." GROUP BY zone.id_zona ";
	
	if($order) $selT=$selT." ORDER BY zone.denumire ".$order." "; else $selT=$selT." ORDER BY numar_hot_zona DESC ";
	
	
	$queT=mysql_query($selT) or die(mysql_error());
	
				

	while($rowT=mysql_fetch_assoc($queT)) {
		$tip[$rowT['id_zona']]=$rowT['denumire'];
	} @mysql_free_result($queT);
	//if(!$err_logare_admin) { echo $id_tipuri; echo '<pre>';print_r($tip);echo '</pre>'; }
	return $tip;
}

function scris_stele ($nr_stele)
{if ( $nr_stele!= '' ) {
		$stele_afis=explode(',',$nr_stele);
		

		//$stele_afis=sort($stele_afis1);
		//echo '<pre>'; print_r($stele_afis); echo '</pre>';
		if(sizeof($stele_afis)>0) {
		$a1=0;
		foreach($stele_afis as $stele1) {	
			$a1++;
		if($a1==1 )
			{$nr_stele_afis=$stele1;}
		if($a1==sizeof($stele_afis) and $a1>1 )
		{
			$nr_stele_afis.=' și '.$stele1;
			}	
		if($a1!=sizeof($stele_afis) and $a1>1 )
			{
				$nr_stele_afis.=', '.$stele1;
		}	
			
		}
		$scris_stele_frumos= ' de ' . $nr_stele_afis . " stele";
		}
		
		}
	return 	$scris_stele_frumos;
	}


function get_localitate($id_tipuri, $id_transport, $id_zona, $id_tara, $early, $id_exclud=NULL, $order=NULL) {
	
	$selT="SELECT
	localitati.id_localitate,
	localitati.denumire,
	localitati.poza1,
	localitati.poza2,
        localitati.descriere_scurta as descriere_localitate,
	zone.denumire AS denumire_zona,
	zone.specificatie AS specificatie,
	COUNT(distinct hoteluri.id_hotel) AS count_hotel
	FROM oferte ";
	if(preg_match('/\\d/',$id_tipuri) ) $selT=$selT." INNER JOIN (SELECT
		oferta_sejur_tip.id_oferta,
		oferta_sejur_tip.id_tip_oferta
		FROM oferta_sejur_tip
		LEFT JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
		WHERE tip_oferta.id_tip_oferta IN (".$id_tipuri.")
		GROUP BY oferta_sejur_tip.id_oferta
		ORDER BY tip_oferta.denumire_tip_oferta)
	AS tipuri_oferta ON tipuri_oferta.id_oferta = oferte.id_oferta ";
	$selT=$selT." INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara ";
	if($early) $selT=$selT." INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta
		AND early_booking.end_date >= now()
		AND early_booking.discount >'0'
		AND early_booking.tip = 'sejur') ";
	if($id_transport) $selT=$selT." INNER JOIN transport ON (oferte.id_transport = transport.id_trans AND oferte.id_transport = '".$id_transport."') ";
	$selT=$selT." WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND oferte.last_minute = 'nu' ";
	if($id_zona) $selT=$selT." AND zone.id_zona = '".$id_zona."' ";
	if($id_tara) $selT=$selT." AND tari.id_tara = '".$id_tara."' ";
	if($id_exclud) $selT=$selT." AND localitati.id_localitate<>'".$id_exclud."' ";
	$selT=$selT." GROUP BY localitati.id_localitate ";
	if($order) $selT=$selT." ORDER BY count_hotel ".$order." "; else $selT=$selT." ORDER BY localitati.denumire ASC ";
	
	//if(!$err_logare_admin) echo  $selT;
	
	$queT=mysql_query($selT) or die(mysql_error());
	while($rowT=mysql_fetch_assoc($queT)) {
		$tip[$rowT['id_localitate']] = array(
			'denumire'=>$rowT['denumire'],
			'poza1'=>$rowT['poza1'],
			'poza2'=>$rowT['poza2'],
			'denumire_zona'=>$rowT['denumire_zona'],
			'specificatie_zona'=>$rowT['specificatie'],
                        'descriere_localitate'=>$rowT['descriere_localitate'],
			'count_hotel'=>$rowT['count_hotel']
		);
	} @mysql_free_result($queT);
//	if(!$err_logare_admin) { echo $id_tipuri; echo '<pre>';print_r($tip);echo '</pre>'; }
	
	return $tip;
}

function get_hoteluri($id_tipuri, $id_transport, $id_localitate, $id_zona, $id_tara, $early, $limit) {
$selT="select
hoteluri.id_hotel,
hoteluri.nume,
hoteluri.stele,
hoteluri.poza1
from
oferte ";
if($id_tipuri) $selT=$selT." inner join (select oferta_sejur_tip.id_oferta, oferta_sejur_tip.id_tip_oferta from oferta_sejur_tip left join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta where tip_oferta.id_tip_oferta in (".$id_tipuri.") Group by oferta_sejur_tip.id_oferta Order by tip_oferta.denumire_tip_oferta) as tipuri_oferta on tipuri_oferta.id_oferta = oferte.id_oferta ";
$selT=$selT." inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate 
inner join zone on localitati.id_zona = zone.id_zona 
inner join tari on zone.id_tara = tari.id_tara ";
if($early) $selT=$selT." inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur') ";
if($id_transport) $selT=$selT." inner join transport on (oferte.id_transport = transport.id_trans and oferte.id_transport = '".$id_transport."') ";
$selT=$selT." where
oferte.valabila = 'da' and hoteluri.tip_unitate <> 'Circuit' and oferte.last_minute = 'nu' ";
if($id_localitate) $selT=$selT." and localitati.id_localitate = '".$id_localitate."' ";
if($id_zona) $selT=$selT." and zone.id_zona = '".$id_zona."' ";
if($id_tara) $selT=$selT." and tari.id_tara = '".$id_tara."' ";
$selT=$selT." Group by hoteluri.id_hotel
Order by hoteluri.nume ";
if($limit) $selT=$selT." Limit ".$limit;
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_assoc($queT)) {
$tip[$rowT['id_hotel']]=array('nume'=>$rowT['nume'], 'stele'=>$rowT['stele'], 'poza'=>$rowT['poza1']);
} @mysql_free_result($queT);
return $tip;
}

function get_tiputi_oferte($id_transport, $id_localitate, $id_zona, $id_tara, $early, $id_hotel) {
	$selT="SELECT
	tip_oferta.id_tip_oferta,
	tip_oferta.denumire_tip_oferta
	FROM oferte
	INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara ";
	if($early) $selT=$selT." INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta AND early_booking.end_date >= NOW() AND early_booking.discount >'0' AND early_booking.tip = 'sejur') ";
	if($id_transport) $selT=$selT." INNER JOIN transport ON (oferte.id_transport = transport.id_trans AND oferte.id_transport = '".$id_transport."') ";
	$selT=$selT." WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND tip_oferta.activ = 'da'
	AND tip_oferta.apare_site = 'da'
	AND tip_oferta.tip IS NOT NULL ";
	if($id_hotel) $selT=$selT." AND hoteluri.id_hotel = '".$id_hotel."' ";
	if($id_localitate) $selT=$selT." AND localitati.id_localitate = '".$id_localitate."' ";
	if($id_zona) $selT=$selT." AND zone.id_zona = '".$id_zona."' ";
	if($id_tara) $selT=$selT." AND tari.id_tara = '".$id_tara."' ";
	$selT=$selT." GROUP BY tip_oferta.denumire_tip_oferta
	ORDER BY tip_oferta.denumire_tip_oferta ";
	//echo $selT;
	$queT=mysql_query($selT) or die(mysql_error());
	while($rowT=mysql_fetch_assoc($queT)) {
		$tip[$rowT['id_tip_oferta']]=array('denumire'=>$rowT['denumire_tip_oferta']);
	} @mysql_free_result($queT);

	$selT="SELECT
	tpof.id_tip_oferta,
	tpof.denumire_tip_oferta
	FROM oferte
	INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara
	LEFT JOIN tip_oferta AS tpof ON tip_oferta.parinte = tpof.id_tip_oferta ";
	if($early) $selT=$selT." INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta AND early_booking.end_date >= NOW() AND early_booking.discount >'0' AND early_booking.tip = 'sejur') ";
	if($id_transport) $selT=$selT." INNER JOIN transport ON (oferte.id_transport = transport.id_trans AND oferte.id_transport = '".$id_transport."') ";
	$selT=$selT." WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND tip_oferta.parinte <> ''
	AND tpof.activ = 'da'
	AND tpof.apare_site = 'da'
	AND tip_oferta.tip IS NOT NULL ";
	if($id_hotel) $selT=$selT." AND hoteluri.id_hotel = '".$id_hotel."' ";
	if($id_localitate) $selT=$selT." AND localitati.id_localitate = '".$id_localitate."' ";
	if($id_zona) $selT=$selT." AND zone.id_zona = '".$id_zona."' ";
	if($id_tara) $selT=$selT." AND tari.id_tara = '".$id_tara."' ";
	$selT=$selT." GROUP BY tip_oferta.denumire_tip_oferta
	ORDER BY tip_oferta.denumire_tip_oferta ";
	//echo $selT;
	$queT=mysql_query($selT) or die(mysql_error());
	while($rowT=mysql_fetch_assoc($queT)) {
		$tip[$rowT['id_tip_oferta']]=array('denumire'=>$rowT['denumire_tip_oferta']);
	} @mysql_free_result($queT);

	return $tip;
}

function get_tiputi_circuite($id_transport, /*$id_continent,*/ $id_tara, $early, $id_hotel) {
	$selT = "SELECT
	tip_oferta.id_tip_oferta,
	tip_oferta.denumire_tip_oferta
	FROM oferte
	INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN continente ON hoteluri.id_continent = continente.id_continent ";
	if($early) $selT = $selT." INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta AND early_booking.end_date >= NOW() AND early_booking.discount >'0' AND early_booking.tip = 'sejur') ";
	if($id_tara) $selT = $selT." INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara = '".$id_tara."' AND traseu_circuit.tara_principala = 'da') ";
	if($id_transport) $selT = $selT." INNER JOIN transport ON (oferte.id_transport = transport.id_trans AND oferte.id_transport = '".$id_transport."') ";
	$selT = $selT." WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate = 'Circuit'
	AND tip_oferta.activ = 'da'
	AND tip_oferta.apare_site = 'da'
	AND tip_oferta.tip IS NOT NULL ";
	if($id_hotel) $selT = $selT." AND hoteluri.id_hotel = '".$id_hotel."' ";
	/*if($id_continent) $selT=$selT." AND continente.id_continent = '".$id_continent."' ";*/
	$selT = $selT." GROUP BY tip_oferta.id_tip_oferta
	ORDER BY tip_oferta.denumire_tip_oferta ";
	$queT = mysql_query($selT) or die(mysql_error());
	while($rowT = mysql_fetch_assoc($queT)) {
		$tip[$rowT['id_tip_oferta']] = array('denumire'=>$rowT['denumire_tip_oferta']);
	} @mysql_free_result($queT);
	
	return $tip;
}

function pret_minim_circuit($id_circuit) {
$sel_pret="SELECT 
pret, 
moneda
FROM 
plecari_circuit 
WHERE 
id_circuit='".$id_circuit."' 
and (((plecari_circuit.data_end is null or plecari_circuit.data_end = '0000-00-00') and plecari_circuit.data_plecare >= now()) or ((plecari_circuit.data_end is not null and plecari_circuit.data_end <> '0000-00-00') and plecari_circuit.data_end >= now())) ORDER BY pret";
$rez_pret=mysql_query($sel_pret);
$row_pret=mysql_fetch_array($rez_pret);
$pretuu[0]=$row_pret["pret"];
$pretuu[1]=$row_pret["moneda"];
mysql_free_result($rez_pret);
return $pretuu;
}

function getpic_destinatie($id_tara, $id_zona, $id_localitate) {
	$sel_pic = "SELECT poza1 ";
	if($id_tara) $sel_pic .= "FROM tari WHERE id_tara = '".$id_tara."' ";
	else if($id_zona) $sel_pic .= "FROM zone WHERE id_zona = '".$id_zona."' ";
	else if($id_localitate) $sel_pic .= "FROM localitati WHERE id_localitate = '".$id_localitate."' ";
	$que_pic = mysql_query($sel_pic);
	$row_pic = mysql_fetch_array($que_pic);
	$picture = '/thumb_';
	if($id_tara) $picture .= 'tara';
	else if($id_zona) $picture .= 'zona';
	else if($id_localitate) $picture .= 'localitate';
	$picture .= '/'.$row_pic['poza1'];
	mysql_free_result($que_pic);
	return $picture;
}

function get_pricemin($id_tara, $id_zona, $id_localitate, $id_tematica=NULL) {
	$sel_price = "SELECT ";
	if($id_tara=='1') $sel_price .= "MIN(pret_minim_lei) AS pret "; else $sel_price .= "MIN(pret_minim) AS pret ";
	$sel_price .= "FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara
	LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	LEFT JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit' ";
	if($id_localitate) $sel_price .= " AND localitati.id_localitate = '".$id_localitate."' ";
	else if($id_zona) $sel_price .= " AND zone.id_zona = '".$id_zona."' ";
	else if($id_tara) $sel_price .= " AND zone.id_tara = '".$id_tara."' ";
	if($id_tematica) $sel_price .= " AND (tip_oferta.id_tip_oferta = '".$id_tematica."' OR tip_oferta.parinte = '".$id_tematica."') ";
	$que_price = mysql_query($sel_price);
	$row_price = mysql_fetch_array($que_price);
	mysql_free_result($que_price);
	return $row_price['pret'];
}

function check_weather($id_tara, $id_zona=NULL) {
	$sel = "SELECT id_clima FROM clima WHERE id_tara = '".$id_tara."' ";
	if(isset($id_zona)) $sel = $sel." AND id_zona = '".$id_zona."' ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	if(mysql_num_rows($que)>0) $value = 1; else $value = 0;
	
	return $value;
}

function check_articles($id_tara, $id_zona=NULL, $id_localitate=NULL) {
	if(isset($id_zona)) $idzona = $id_zona; else $idzona = 0;
	if(isset($id_localitate)) $idlocalitate = $id_localitate; else $idlocalitate = 0;
	$sel = "SELECT id_article FROM articles WHERE activ = 'da' AND id_tara = '".$id_tara."' AND id_zona = '".$idzona."' AND id_localitate = '".$idlocalitate."' AND articol_principal!='da'";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	if(mysql_num_rows($que)>0) $value = 1; else $value = 0;
	
	return $value;
}

function articles_side_menu($id_tara, $id_zona=NULL, $id_localitate=NULL) {
	if(isset($id_zona)) $idzona = $id_zona; else $idzona = 0;
	if(isset($id_localitate)) $idlocalitate = $id_localitate; else $idlocalitate = 0;
	$sel = "SELECT * FROM articles WHERE activ = 'da' AND articol_principal = 'da' AND id_tara = '".$id_tara."' AND id_zona = '".$idzona."' AND id_localitate = '".$idlocalitate."' ";
	$que = mysql_query($sel) or die(mysql_error());
	while($row = mysql_fetch_array($que)) {
		$value[$row['id_article']]['denumire'] = $row['denumire'];
		$value[$row['id_article']]['link'] = $row['link'];
		$value[$row['id_article']]['link_area'] = $row['link_area'];
		$value[$row['id_article']]['link_localiate'] = $row['link_area'];
	}
	
	return $value;
}

function schimba_caractere($string) {
	$sel_cuv="SELECT * FROM replacements GROUP BY id";
	$que_cuv=mysql_query($sel_cuv) or die(mysql_error());
	while($row_cuv=mysql_fetch_array($que_cuv)) {
		$find[] = $row_cuv['cuvant'];
		$replace[] = $row_cuv['alias'];
	}
	@mysql_free_result($que_cuv);
	
	$string=trim($string);
	$stringul= str_replace($find, $replace, $string);
	for($i=1;$i<=31;$i++)
	if($i<>13) $stringul=str_replace(chr($i),"",$stringul);
	return trim($stringul); 
}

function get_id_transport($transport) {
	$get_transport="SELECT id_trans FROM transport WHERE denumire like '%".$transport."%'";
	$rez_transport=mysql_query($get_transport);
	$row_transport=mysql_fetch_array($rez_transport);
	$rand_transport=$row_transport['id_trans'];
	mysql_free_result($rez_transport);
	
	return $rand_transport;
}

function get_den_transport($id_transport) {
	$get_transport = "SELECT denumire FROM transport WHERE id_trans = '".$id_transport."' ";
	$rez_transport = mysql_query($get_transport);
	$row_transport = mysql_fetch_array($rez_transport);
	$rand_transport = $row_transport['denumire'];
	mysql_free_result($rez_transport);
	
	return $rand_transport;
}

function get_details_transport($id_transport) {
	$get_transport = "SELECT * FROM transport WHERE id_trans = '".$id_transport."' ";
	$rez_transport = mysql_query($get_transport);
	$row_transport = mysql_fetch_array($rez_transport);
	mysql_free_result($rez_transport);
	
	return $row_transport;
}

function denLuniRo($value) {
	$months = array (
	'January' => 'Ianuarie',
	'February' => 'Februarie',
	'March' => 'Martie',
	'April' => 'Aprilie',
	'May' => 'Mai',
	'June' => 'Iunie',
	'July' => 'Iulie',
	'August' => 'August',
	'September' => 'Septembrie',
	'October' => 'Octombrie',
	'November' => 'Noiembrie',
	'December' => 'Decembrie'
	);
	while (list($character, $replacement) = each($months)) {
		$value = str_replace($character, $replacement, $value);
	}
	return $value;
}

function denZileRo($value) {
	$months = array (
	'Monday' => 'Luni',
	'Tuesday' => 'Marți',
	'Wednesday' => 'Miercuri',
	'Thursday' => 'Joi',
	'Friday' => 'Vineri',
	'Saturday' => 'Sâmbătă',
	'Sunday' => 'Duminică'
	);
	while (list($character, $replacement) = each($months)) {
		$value = str_replace($character, $replacement, $value);
	}
	return $value;
}

function denLuniEn($value) {
	$months = array (
	'Ianuarie' => 'January',
	'Februarie' => 'February',
	'Martie' => 'March',
	'Aprilie' => 'April',
	'Mai' => 'May',
	'Iunie' => 'June',
	'Iulie' => 'July',
	'August' => 'August',
	'Septembrie' => 'September',
	'Octombrie' => 'October',
	'Noiembrie' => 'November',
	'Decembrie' => 'December'
	);
	while (list($character, $replacement) = each($months)) {
		$value = str_replace($character, $replacement, $value);
	}
	return $value;
}

/*function soldout($id_oferta, $id_camera, $data_start, $tarif) {
	$sel_soldout = "SELECT data_start AS soldout_start, data_end AS soldout_end FROM sold_out WHERE id_oferta='".$id_oferta."' AND camera='".$id_camera."'";
	$que_soldout = mysql_query($sel_soldout) or die(mysql_error());
	$test=false;
	while($row_soldout = mysql_fetch_array($que_soldout) and !$test) {
		if((strtotime(denLuniEn($data_start))>=strtotime($row_soldout['soldout_start'])) and (strtotime(denLuniEn($data_start))<=strtotime($row_soldout['soldout_end']))) {
			$test=true;
			return true;
			break;
		} else {
			$test=false;
			return false;
		}
	}
}*/

function soldout($id_oferta, $id_camera, $data_start/*, $tarif*/) {
	$arr1 = array();
	$sel_soldout = "SELECT data_start AS soldout_start, data_end AS soldout_end FROM sold_out WHERE id_oferta='".$id_oferta."' AND camera='".$id_camera."'";
	$que_soldout = mysql_query($sel_soldout) or die(mysql_error());
	while($row_soldout = mysql_fetch_array($que_soldout)) {
		$arr1[] = array(strtotime($row_soldout['soldout_start']), strtotime($row_soldout['soldout_end']));
	}
	$value = 0;
	for($i=0; $i<sizeof($arr1); $i++) {
		if((strtotime(denLuniEn($data_start)) >= $arr1[$i][0]) and (strtotime(denLuniEn($data_start)) <= $arr1[$i][1])) {
			//echo ' -- '.date('Y-m-d',strtotime(denLuniEn($data_start))).' -- '.date('Y-m-d',$arr1[$i][0]).' | '.date('Y-m-d',$arr1[$i][1]);
			$value++;
		}
	}
	return $value;
}

function moneda($moneda) {
	if($moneda=='Procent') {$coin='%';}
	if($moneda=='EURO' or $moneda=='EUR') { $coin='&euro;'; }
	elseif($moneda=='USD') { $coin='$'; }
	elseif($moneda=='PROCENT') { $coin='%'; }
	elseif($moneda=='RON') { $coin='Lei'; }
	else $coin=$moneda;
	
	return $coin;
}

function reverse_moneda($moneda) {
	if($moneda=='€') { $coin='EURO'; }
	elseif($moneda=='&euro;') { $coin='EURO'; }
	else $coin=$moneda;
	
	return $coin;
}

function nopti($value) {
	if($value>1) { $value=$value.' nopti'; }
	else { $value='pe noapte'; }
	
	return $value;
}

function transport($value) {
	if($value=='Fara transport') $value = 'fara transport';
	else if($value=='La cerere') $value = 'la cerere';
	else $value = 'cu '.strtolower($value);
	
	return $value;
}

function create_puncte($pret_lei, $moneda=NULL) {
	if($moneda==NULL) {
		//pretul este cel minim si este per persoana
		$puncte = round($pret_lei/10*6);
	} else {
		$sel_curs = "SELECT * FROM curs_valutar ";
		$que_curs = mysql_query($sel_curs);
		$row_curs = mysql_fetch_array($que_curs);
		@mysql_free_result($que_curs);
		
		$moneda = reverse_moneda($moneda);
		$puncte = round($pret_lei * $row_curs[$moneda] / 10 * 6);
	}
	
	return $puncte;
}

function final_price_lei($pret, $moneda, $adaos=NULL) {
	$moneda = reverse_moneda($moneda);
	if($moneda=='USD' or $moneda=='EURO' or $moneda=='EUR') {
		$sel_curs = "SELECT * FROM curs_valutar ";
		$que_curs = mysql_query($sel_curs);
		$row_curs = mysql_fetch_array($que_curs);
		@mysql_free_result($que_curs);
		
		$pret_final_lei = $pret * $row_curs[$moneda]*1.00;
		if(isset($adaos) and $adaos>0) $pret_final_lei = $pret_final_lei * (100 + $adaos) / 100;
	} else {
		$pret_final_lei = $pret;
	}
	return round($pret_final_lei, 2);
}

function final_price_eur($pret, $moneda) {
	$moneda = reverse_moneda($moneda);
	$sel_curs = "SELECT * FROM curs_valutar ";
	$que_curs = mysql_query($sel_curs);
	$row_curs = mysql_fetch_array($que_curs);
	@mysql_free_result($que_curs);
	if($moneda=='RON') {
		$pret_final = $pret / $row_curs['EURO'];
	} else {
		$pret_final = $pret;
	}
	return round($pret_final, 2);
}

function final_price_euro($pret) {
	$sel_curs = "SELECT * FROM curs_valutar ";
	$que_curs = mysql_query($sel_curs);
	$row_curs = mysql_fetch_array($que_curs);
	@mysql_free_result($que_curs);
		
	$pret_final_lei = $pret / $row_curs['EURO'];
	return round($pret_final_lei, 2);
}

function truncate_str($str, $maxlen) {
	if(strlen($str)<=$maxlen) return $str;
	
	$newstr=substr($str, 0, $maxlen);
	if(substr($newstr,-1,1)!=' ') $newstr=substr($newstr, 0, strrpos($newstr, " "));
	
	return $newstr;
}


function truncate($string,$length=100,$append="&hellip;") {
  $string = trim($string);

  if(strlen($string) > $length) {
    $string = wordwrap($string, $length);
    $string = explode("\n", $string, 2);
    $string = $string[0] . $append;
  }

  return $string;
}

function check_array($anArray) {
	if(empty($anArray)) { return false; }
	foreach($anArray as $element) {}
	return true;
}

function w3c_and($value) {
	return str_replace('&','&amp;',$value);
}

function get_id_camera($value) {
	$sel_cam="SELECT * FROM tip_camera_corespondent WHERE den_camera = '$value' ";
	$que_cam=mysql_query($sel_cam);
	$row_cam=mysql_fetch_array($que_cam);
	@mysql_free_result($que_cam);
	
	return $row_cam['id_camera_corespondent'];
}

function get_den_camera($value) {
	$sel_cam="SELECT * FROM tip_camera WHERE id_camera = '$value' ";
	$que_cam=mysql_query($sel_cam);
	$row_cam=mysql_fetch_array($que_cam);
	@mysql_free_result($que_cam);
	
	return $row_cam['denumire'];
}

function make_date_from_zzll($value) {
	$data = explode('.',$value);
	$day = $data[0];
	$month = $data[1];
	//if( $month<=12 and $month>1) {
	//if(($day<=date('d')) and ($month<=(date('m')-5))) {
	if( ($month<=(date('m')-1))) {

		$year = date('Y')+1;
	} else {
		$year = date('Y');
	}
	
	return $year.'-'.$month.'-'.$day;
}

function new_price($value) {
	//$value = round($value, 2);
	$value = round($value);
	$price = explode('.',$value);
	$suma = $price[0];
	if((sizeof($price)>1) and ($price[1]>0)) {
		$zecimale = '<sup>'.$price[1].'</sup>';
	}
	$pret = $suma.$zecimale;
	
	return $pret;
}

function getIntersection($a1,$a2,$b1,$b2) {
	$a1 = strtotime($a1);
	$a2 = strtotime($a2);
	$b1 = strtotime($b1);
	$b2 = strtotime($b2);
	if($a1==$a2){$a2=$a2+(60 * 60 * 24);}
	if($b1 > $a2 || $a1 > $b2 || $a2 < $a1 || $b2 < $b1) {
		return false;
	}
	$start = $a1 < $b1 ? $b1 : $a1;
	$end = $a2 < $b2 ? $a2 : $b2;

	return array('start' => $start, 'end' => $end);
}

function dateDiff_nou($date_1 , $date_2 , $differenceFormat = '%a' )
{
    //$date_1=date('Y-m-d',$date_1);
	//$date_2=date('Y-m-d',$date_2);
	$datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    
    $interval = date_diff($datetime1, $datetime2);
    
    return $interval->format($differenceFormat);
    
}

function dateDiff($d1, $d2) {
	if(strtotime($d1)!='') {
		$d11 = strtotime($d1);
	} else {
		$d11 = $d1;
	}
	if(strtotime($d2)!='') {
		$d22 = strtotime($d2);
	} else {
		$d22 = $d2;
	}
	return round(abs($d11-$d22+86400)/86400);
}

function is_bot() {
    $spiders = array("abot", "dbot", "ebot", "hbot", "kbot", "lbot", "mbot", "nbot", "obot", "pbot", "rbot", "sbot", "tbot", "vbot", "ybot", "zbot", "bot.", "bot/", "_bot", ".bot", "/bot", "-bot", ":bot", "(bot", "crawl", "slurp", "spider", "seek", "accoona", "acoon", "adressendeutschland", "ah-ha.com", "ahoy", "altavista", "ananzi", "anthill", "appie", "arachnophilia", "arale", "araneo", "aranha", "architext", "aretha", "arks", "asterias", "atlocal", "atn", "atomz", "augurfind", "backrub", "bannana_bot", "baypup", "bdfetch", "big brother", "biglotron", "bjaaland", "blackwidow", "blaiz", "blog", "blo.", "bloodhound", "boitho", "booch", "bradley", "butterfly", "calif", "cassandra", "ccubee", "cfetch", "charlotte", "churl", "cienciaficcion", "cmc", "collective", "comagent", "combine", "computingsite", "csci", "curl", "cusco", "daumoa", "deepindex", "delorie", "depspid", "deweb", "die blinde kuh", "digger", "ditto", "dmoz", "docomo", "download express", "dtaagent", "dwcp", "ebiness", "ebingbong", "e-collector", "ejupiter", "emacs-w3 search engine", "esther", "evliya celebi", "ezresult", "falcon", "felix ide", "ferret", "fetchrover", "fido", "findlinks", "fireball", "fish search", "fouineur", "funnelweb", "gazz", "gcreep", "genieknows", "getterroboplus", "geturl", "glx", "goforit", "golem", "grabber", "grapnel", "gralon", "griffon", "gromit", "grub", "gulliver", "hamahakki", "harvest", "havindex", "helix", "heritrix", "hku www octopus", "homerweb", "htdig", "html index", "html_analyzer", "htmlgobble", "hubater", "hyper-decontextualizer", "ia_archiver", "ibm_planetwide", "ichiro", "iconsurf", "iltrovatore", "image.kapsi.net", "imagelock", "incywincy", "indexer", "infobee", "informant", "ingrid", "inktomisearch.com", "inspector web", "intelliagent", "internet shinchakubin", "ip3000", "iron33", "israeli-search", "ivia", "jack", "jakarta", "javabee", "jetbot", "jumpstation", "katipo", "kdd-explorer", "kilroy", "knowledge", "kototoi", "kretrieve", "labelgrabber", "lachesis", "larbin", "legs", "libwww", "linkalarm", "link validator", "linkscan", "lockon", "lwp", "lycos", "magpie", "mantraagent", "mapoftheinternet", "marvin/", "mattie", "mediafox", "mediapartners", "mercator", "merzscope", "microsoft url control", "minirank", "miva", "mj12", "mnogosearch", "moget", "monster", "moose", "motor", "multitext", "muncher", "muscatferret", "mwd.search", "myweb", "najdi", "nameprotect", "nationaldirectory", "nazilla", "ncsa beta", "nec-meshexplorer", "nederland.zoek", "netcarta webmap engine", "netmechanic", "netresearchserver", "netscoop", "newscan-online", "nhse", "nokia6682/", "nomad", "noyona", "nutch", "nzexplorer", "objectssearch", "occam", "omni", "open text", "openfind", "openintelligencedata", "orb search", "osis-project", "pack rat", "pageboy", "pagebull", "page_verifier", "panscient", "parasite", "partnersite", "patric", "pear.", "pegasus", "peregrinator", "pgp key agent", "phantom", "phpdig", "picosearch", "piltdownman", "pimptrain", "pinpoint", "pioneer", "piranha", "plumtreewebaccessor", "pogodak", "poirot", "pompos", "poppelsdorf", "poppi", "popular iconoclast", "psycheclone", "publisher", "python", "rambler", "raven search", "roach", "road runner", "roadhouse", "robbie", "robofox", "robozilla", "rules", "salty", "sbider", "scooter", "scoutjet", "scrubby", "search.", "searchprocess", "semanticdiscovery", "senrigan", "sg-scout", "shai'hulud", "shark", "shopwiki", "sidewinder", "sift", "silk", "simmany", "site searcher", "site valet", "sitetech-rover", "skymob.com", "sleek", "smartwit", "sna-", "snappy", "snooper", "sohu", "speedfind", "sphere", "sphider", "spinner", "spyder", "steeler/", "suke", "suntek", "supersnooper", "surfnomore", "sven", "sygol", "szukacz", "tach black widow", "tarantula", "templeton", "/teoma", "t-h-u-n-d-e-r-s-t-o-n-e", "theophrastus", "titan", "titin", "tkwww", "toutatis", "t-rex", "tutorgig", "twiceler", "twisted", "ucsd", "udmsearch", "url check", "updated", "vagabondo", "valkyrie", "verticrawl", "victoria", "vision-search", "volcano", "voyager/", "voyager-hc", "w3c_validator", "w3m2", "w3mir", "walker", "wallpaper", "wanderer", "wauuu", "wavefire", "web core", "web hopper", "web wombat", "webbandit", "webcatcher", "webcopy", "webfoot", "weblayers", "weblinker", "weblog monitor", "webmirror", "webmonkey", "webquest", "webreaper", "websitepulse", "websnarf", "webstolperer", "webvac", "webwalk", "webwatch", "webwombat", "webzinger", "wget", "whizbang", "whowhere", "wild ferret", "worldlight", "wwwc", "wwwster", "xenu", "xget", "xift", "xirq", "yandex", "yanga", "yeti", "yodao", "zao/", "zippp", "zyborg", "....");

    foreach($spiders as $spider) {
        //If the spider text is found in the current user agent, then return true
        if ( stripos($_SERVER['HTTP_USER_AGENT'], $spider) !== false ) return true;
    }
    //If it gets this far then no bot was found!
    return false;

}

function insert_user($sex, $nume, $prenume, $email, $data_nasterii, $telefon, $tip_adaugare) {
	$sel_users = "SELECT * FROM useri_fizice WHERE lower(email) = '".strtolower($email)."' ";
	$que_users = mysql_query($sel_users) or die(mysql_error());
	$row_users = mysql_fetch_array($que_users);
	if(mysql_num_rows($que_users)==0) {
		$ins_useri_fizice = "INSERT INTO useri_fizice SET
		nume = '".ucwords(strtolower($nume))."',
		prenume = '".ucwords(strtolower($prenume))."',
		email = '".$email."',
		telefon = '".$telefon."',
		sex = '".$sex."',
		data_nasterii = '".$data_nasterii."',
		data_adaugarii = NOW(),
		session_id = '".session_id()."',
		tip_adaugare = '".$tip_adaugare."' ";
		$rez_ins_useri_fizice = mysql_query($ins_useri_fizice) or die(mysql_error());
		@mysql_free_result($rez_ins_useri_fizice);
		
		$sel_useri_fizice = "SELECT id_useri_fizice FROM useri_fizice WHERE session_id='".session_id()."' AND lower(email)='".strtolower($email)."' ORDER BY id_useri_fizice DESC";
		$que_useri_fizice = mysql_query($sel_useri_fizice) or die(mysql_error());
		$row_useri_fizice = mysql_fetch_array($que_useri_fizice);
		$id_usr = $row_useri_fizice['id_useri_fizice'];
		@mysql_free_result($que_useri_fizice);
	} else {
		$id_usr = $row_users['id_useri_fizice'];
		
		if($row_users['telefon']=='') $telefon = $telefon; else $telefon = $row_users['telefon'];
		if($row_users['sex']=='') $sex = $sex; else $sex = $row_users['sex'];
		if($row_users['data_nasterii']==NULL) $data_nasterii = $data_nasterii; else $data_nasterii = $row_users['data_nasterii'];
		
		$upd_useri_fizic = "UPDATE useri_fizice SET
		telefon = '".$telefon."',
		sex = '".$sex."',
		data_nasterii = '".$data_nasterii."'
		WHERE lower(email) = '".strtolower($email)."'
		";
		$rez_upd_useri_fizic = mysql_query($upd_useri_fizic) or die(mysql_error());
		@mysql_free_result($rez_upd_useri_fizic);
	}
	return $id_usr;
}

function top_vanzari($id_hotel, $multiplier) {
	$sel_upd = "UPDATE hoteluri SET counter = counter+'".$multiplier."' WHERE id_hotel = '".$id_hotel."' ";
	$que_upd = mysql_query($sel_upd) or die(mysql_error());
	@mysql_free_result($que_upd);
}

function change_meal($value) {
	$replacements = array(
		'BB' => 'Mic Dejun',
		'B&B' => 'Mic Dejun',
		'BED AND BREAKFAST' => 'Mic Dejun',
		'Bed ANDamp; breakfast'=> 'Mic Dejun',
		'HB' => 'Demipensiune',
		'Half Board' => 'Demipensiune',
		'HALF BOARD' => 'Demipensiune',
		'Cina' => 'Cină',
		'DI' => 'Cină',
		'UA+' =>'Ultra All Inclusive',
		'Pensiune completa' => 'Pensiune completă',
		'DP'=>'Demipensiune',
		'PC' => 'Pensiune completă',
		'FB' => 'Pensiune completă',
		'Full Board' => 'Pensiune completă',
		'FULL BOARD' => 'Pensiune completă',
		'UAI' => 'Ultra all inclusive',
		'PRAI' => 'Premium all inclusive',
		'Premium AI' => 'Premium all inclusive',
		'AI' => 'All inclusive',
		'Supliment all inclusive' => 'All inclusive',
		'ROYAL All inclusive' => 'Royal all inclusive',
		'Fara masa' => 'Fără masă',
		'ROOM ONLY'=>'Fără masă',
		'RO' => 'Fără masă',
		'RR' => 'Fără masă',
		'BO' => 'Fără masă',
		'SC' => 'Fără masă',
		'Room only'=> 'Fără masă',
		'NONE' => 'Fără masă',
		'Fisa cont' => 'Fisa cont',
		'FC' => 'Fisa cont',
		'All Inclusive'=>'All inclusive',
		'Mic    Dejun'=>'Mic Dejun',
		
		'breakfast included'=>'Mic Dejun',
		'Bb'=>'Mic Dejun',
		'Hb'=>'Demipensiune',
		'self catering'=>'Fără masă',
		'self caterin'=>'Fără masă',
		'Accommodation ANDamp; All inclusive'=>'All inclusive',
		'All inclusive, Mic Dejun'=>'All inclusive',
		'Mic Dejun + Pranz sau Cină- Restaurant Melodia'=>'Demipensiune',
		'Mic Dejun + Pranz si Cină- Restaurant Melodia'=>'Pensiune Completa',
		'Mic Dejun, All inclusive'=>'All inclusive',
		'Fără masă + Mic Dejun- Restaurant Vulturul(IT)'=>'Mic Dejun',
		'Fără masă + Mic Dejun- Restaurant Vulturul(ST)'=>'Mic Dejun',
		'Fără masă + Cină- Restaurant Vulturul(IT)'=>'Cina',
		'Fără masă + Cină- Restaurant Vulturul(ST)'=>'Cina',
		'Fără masă + Pranz- Restaurant Vulturul(IT)'=>'Pranz',
		'Fără masă + Pranz- Restaurant Vulturul(ST)'=>'Pranz',
		'Demipensiune (F)'=>'Demipensiune',
		'Demipensiune+'=>'Demipensiune',
		'Mic Dejun, Pranz'=>'Demipensiune',
		'Accommodation ANDamp; halfboard'=>'Demipensiune',
		'Mic Dejun (bufet)'=>'Mic Dejun',
		'Pensiune Completa (F)'=>'Pensiune Completa',
		'Bed And Breakfast'=>'Mic Dejun',	
		'Accommodation ANDamp; All inclusive'=>'All Inclusive',	
		'Pensiune completÄƒ'=>'Pensiune Completa',
		'Mic Dejun, Supliment demipensiune'=>'Demipensiune',
		'Supliment demipensiune, Mic Dejun'=>'Demipensiune',
		'Pranz, Mic Dejun'=>'Demipensiune',
		'Mic Dejun + Pranz- Restaurant Majestic'=>'Demipensiune',
		'Mic Dejun + Pranz- Complex Bran- Brad- Bega'=>'Demipensiune',
		'Demipensiune, Mic Dejun'=>'Demipensiune',
		'Fără masă + Mic Dejun- Restaurant National'=>'Mic Dejun',
		'Supliment All inclusive Premium, All inclusive'=>'All Inclusive',
		'All inclusive, Supliment All inclusive Premium'=>'All Inclusive',
		'Demi exclusiv'=>'Demipensiune',
		'DEMI EXCLUSIV'=>'Demipensiune',
		'Cină, Demipensiune'=>'Demipensiune',
		'Demipensiune 50, Mic Dejun'=>'Demipensiune',
		'Mic Dejun, Demipensiune 50'=>'Demipensiune',
		'Pensiune completă 100, Mic Dejun'=>'Pensiune Completa',
		'Mic Dejun, Pensiune completă 100'=>'Pensiune Completa',
		'Mic Dejun(U)'=>'Mic dejun',
		'Mic Dejun + Pranz- Restaurant Bicaz'=>'Demipensiune',
		'Demipensiune 40, Mic Dejun'=>'Demipensiune',
		'Supliment pensiune completa, Mic Dejun'=>'Pensiune Completa',
		'Pensiune completă-bufet suedez'=>'Pensiune Completa',
		'Pensiune completă 80, Mic Dejun'=>'Pensiune Completa',
		'Accommodation ANDamp; full board'=>'Pensiune Completa',
		'All inclusiveG'=>'All Inclusive Gold',
		'Mic Dejun, Dejun'=>'Demipensiune',
		'Supliment All inclusive'=>'All inclusive',
		'Mic Dejun + Pranz- Restaurant Roxana'=>'Demipensiune',
		'Bonuri Valorice 78lei/pers/zi, Mic Dejun'=>'Pensiune Completa',
		'Pensiune completă 78, Mic Dejun'=>'Pensiune Completa',
		'Mic Dejun, Pensiune completă'=>'Pensiune Completa',
		'Pensiune Completa 80'=>'Pensiune Completa',
		'Mic Dejun, Bonuri valorice 80 lei/pers/zi'=>'Pensiune Completa',
		'Demipensiune 43'=>'Demipensiune',
		'Mic Dejun, Demipensiune 40'=>'Demipensiune',
		'Mic Dejun, Demipensiune 35'=>'Demipensiune',
		'Demipensiune( bufet)'=>'Demipensiune',
		'Supliment demipensiune( bufet), Mic Dejun'=>'Demipensiune',
		'Pranz bufet, Mic Dejun'=>'Demipensiune',
		'Pensiune completă 63'=>'Pensiune Completa',
		'Pensiune completă 70, Mic Dejun'=>'Pensiune Completa',
		'Mic Dejun + Pranz- Restaurant Lidia'=>'Demipensiune',
		'MASA PRINCIPALA 38 Fără masăN/PERSOANA'=>'Demipensiune',
		'Fără masă + Pranz- Restaurant Raluca'=>'Demipensiune',
		'Pensiune completă 115'=>'Pensiune completa',
		'Demipensiune 70'=>'Demipensiune',
		'Mic Dejun, Demipensiune 30'=>'Demipensiune',
		'Pensiune completă 65, Mic Dejun'=>'Pensiune Completa',
		'Mic Dejun-bonuri valorice 25 ron/pers/zi, cina-bonuri valorice 30 ron/pers/zi'=>'Demipensiune',
		'MD 15'=>'Mic Dejun',
		'MD 18'=>'Mic Dejun',
		'Fără masă + Pranz- Restaurant Prahova'=>'Pranz',
		'Fără masă + Pranz- Restaurant Prahova (M)'=>'Pranz',
		'Mic Dejun + Pranz- Restaurant Siret'=>'Demipensiune',
		'Fără masă + Pranz- Restaurant Cupidon'=>'Pranz',
		'Fără masă + Pranz- Restaurant Cerna'=>'Pranz',
		'Mic Dejun + Pranz- Restaurant Hora'=>'Demipensiune',
		'Fără masă + Mic Dejun- Restaurant Meteor'=>'Mic dejun',
		'All inclusive - 3 mese bufet suedez cu bauturi indigene'=>'All Inclusive',
		'Cont turist 110 lei / pers /zi'=>'Pensiune completa',
		'Pensiune completă 110'=>'Pensiune completa',
		'Fără masă + Pensiune completă MENIU FIX - CORP OLTUL'=>'Pensiune completă MENIU FIX',
		'Fără masă + Taxa eliberare card MASA FISA CONT - CORP OLTUL'=>'Masa fisa cont',
		'Demipensiune + Demipensiune Restaurant Dietetic Traian - MENIU FIX'=>'Demipensiune meniu fix, restaurant Dietetic',
		'Demipensiune + Demipensiune Restaurant Intim - FISA CONT'=>'Demipensiune fisa cont, restuarant Intim',
		'Accommodation ANDamp; other services incl + Demipensiune Restaurant Dietetic Traian - MENIU FIX'=>'Demipensiune meniu fix, restaurant Dietetic',
		'Pensiune Completa + Pensiune completă Restaurant Dietetic Traian - MENIU FIX'=>'Pensiune Completa meniu fix, restaurant Dietetic',
		'Accommodation ANDamp; other services incl + Pensiune completă Restaurant Dietetic Traian - MENIU FIX'=>'Pensiune Completa meniu fix, restaurant Dietetic',
		'Pensiune Completa + Pensiune completă Restaurant Intim - FISA CONT'=>'Pensiune Completa fisa cont, restuarant Intim',
		'Accommodation ANDamp; other services incl + Tratament Hotel Traian'=>'Fisa cont',
		'Mic Dejun + Tratament | Mures'=>'Mic Dejun',
		'Mic Dejun + Cină | Mures + Tratament | Mures'=>'Mic dejun + Cina',
		'Mic Dejun + Cină | Mures'=>'Mic dejun + Cina',
		'Demipensiune + CINA HOTEL MURES'=>'Mic dejun + Cina',
		'Accommodation ANDamp; other services incl + CINA HOTEL MURES'=>'Mic dejun + Cina',
		'Accommodation ANDamp; other services incl'=>'Mic dejun',
		'All inclusiveP'=>'All inclusive Premium',
		'All inclusiveL'=>'All inclusive Light',
		
		'Pensiune completă FISA CONT - CORP CACIULATA + Taxa eliberare card MASA FISA CONT - CORP CACIULATA + Tratament Luni - Vineri - CORP CACIULATA'=>'Pensiune completă Fisa Cont + Tratament',
		'Pensiune completă FISA CONT - CORP CACIULATA + Taxa eliberare card MASA FISA CONT - CORP CACIULATA + Tratament Luni - Vineri - CORP CACIULATA'=>'Pensiune completă Fisa Cont + Tratament',
		'Pensiune completă MENIU FIX - CORP CACIULATA + Tratament Luni - Vineri - CORP CACIULATA'=>'Pensiune completă meniu fix + Tratament',
		'Pensiune completă MENIU FIX - CORP CACIULATA'=>'Pensiune completă meniu fix',
		'Pensiune completă FISA CONT - CORP CACIULATA + Taxa eliberare card MASA FISA CONT - CORP CACIULATA'=>'Pensiune completă fisa cont',
		
		
				'Tratament Luni - Vineri - CORP OLTUL + Taxa eliberare card MASA FISA CONT - CORP OLTUL + Pensiune completă FISA CONT - CORP OLTUL'=>'Pensiune completă Fisa Cont + Tratament',
		'Pensiune completă FISA CONT - CORP OLTUL + Taxa eliberare card MASA FISA CONT - CORP OLTUL + Tratament Luni - Vineri - CORP OLTUL'=>'Pensiune completă Fisa Cont + Tratament',
		'Tratament Luni - Vineri - CORP OLTUL + Pensiune completă MENIU FIX - CORP OLTUL'=>'Pensiune completă meniu fix + Tratament',
		'Pensiune completă MENIU FIX - CORP OLTUL'=>'Pensiune completă meniu fix',
		'Taxa eliberare card MASA FISA CONT - CORP OLTUL + Pensiune completă FISA CONT - CORP OLTUL'=>'Pensiune completă fisa cont',
		
		
		'Pensiune completă FISA CONT - HOTEL CENTRAL + Taxa eliberare card MASA FISA CONT - HOTEL CENTRAL + Tratament Luni - Vineri - HOTEL CENTRAL'=>'Pensiune completă Fisa Cont + Tratament',
		'Pensiune completă FISA CONT - HOTEL CENTRAL + Taxa eliberare card MASA FISA CONT - HOTEL CENTRAL + Tratament Luni - Vineri - HOTEL CENTRAL'=>'Pensiune completă Fisa Cont + Tratament',
		'Pensiune completă MENIU FIX - HOTEL CENTRAL + Tratament Luni - Vineri - HOTEL CENTRAL'=>'Pensiune completă meniu fix + Tratament',
		'Pensiune completă MENIU FIX - HOTEL CENTRAL'=>'Pensiune completă meniu fix',
		'Pensiune completă FISA CONT - HOTEL CENTRAL + Taxa eliberare card MASA FISA CONT - HOTEL CENTRAL'=>'Pensiune completă fisa cont',
		
		'NM'=>'Fara masa',
		'Mic Dejun- bonuri valorice 30 ron/ persoana/ zi'=>'Mic dejun (30 lei)',
		'dejun-bonuri valorice 35 ron/pers/zi'=>'Pranz (35 lei)',
		'Все включено плюс'=>'All Inclusive+',
		'Mic Dejun, All inclusive'=>'All Inclusive',
		'Demi-all'=>'Demipensiune',
		'All inclusive, Mic Dejun'=>'All inclusive',
		'Mic Dejun + Tratament - HOTEL TERMAL'=>'Mic Dejun + Tratament',
		'Mic Dejun + Tratament - HOTEL INTERNATIONAL'=>'Mic Dejun + Tratament',
		'Mic Dejun + Tratament - HOTEL POIENITA'=>'Mic Dejun + Tratament',
		
		'Mic Dejun + CINA - HOTEL TERMAL + TRATAMENT - HOTEL TERMAL'=>'Mic Dejun + Cina + Tratament',
		'Mic Dejun + CINA - HOTEL INTERNATIONAL + TRATAMENT - HOTEL INTERNATIONAL'=>'Mic Dejun + Cina + Tratament',
		'Mic Dejun + CINA - H0TEL POIENITA + TRATAMENT - HOTEL POIENITA'=>'Mic Dejun + Cina + Tratament',
		
		'Mic Dejun + TRATAMENT - HOTEL TERMAL'=>'Mic Dejun + Tratament',
		'Mic Dejun + TRATAMENT - HOTEL INTERNATIONAL'=>'Mic Dejun + Tratament',
		'Mic Dejun + TRATAMENT - HOTEL POIENITA'=>'Mic Dejun + Tratament',
		
		'Mic Dejun + CINA - HOTEL TERMAL'=>'Mic Dejun + Cina',
		'Mic Dejun + CINA - HOTEL INTERNATIONAL'=>'Mic Dejun + Cina',
		'Mic Dejun + CINA - H0TEL POIENITA'=>'Mic Dejun + Cina',
		'Mic dejun'=>'Mic Dejun'
	);
	$den_masa = str_replace(array_keys($replacements), array_values($replacements), $value);
	/*if($den_masa!='')*/ $meal = $den_masa; /*else $meal = $value;*/
	if($den_masa!='') $meal = $den_masa; else $meal = $value;
	//echo $value;
	return $meal;
}

function change_meal_new($matches) {
	return "+ Bonuri valorice " . end($matches) . " lei/pers/zi";
}

function change_meal_name($mealName) {
	$mealName = str_replace(array("Fără masă + ", "Fisa cont", "Ron/pers/zi", "Ron/ pers/zi"), array("", "Bonuri valorice", "lei/pers/zi", "lei/pers/zi"), $mealName);
	$mealName = preg_replace("/,.*\+ All inclusive/", ", All inclusive", $mealName);
	$mealName = preg_replace("/,.*\+ Supliment all inclusive/", ", All inclusive", $mealName);
	$mealName = preg_replace_callback( "/([\+])+([ ][a-zA-Z \ă]*)+([\d]+)/", "change_meal_new", $mealName );
	return $mealName;
}


function change_serie($value) {
	
	$replacements = array(
	'eb 31.01.2017 mic dejun'=>'Early Booking 31.01.2017',
	'eb 31.03 .2017 mic dejun'=>'Early Booking 31.03.2017',
	'2017 mic dejun'=>' ',
	'2017 pensiune completa'=>' '
);
$den_serie = str_replace(array_keys($replacements), array_values($replacements), $value);
	/*if($den_masa!='')*/ $serie = $den_serie; /*else $meal = $value;*/
	if($den_serie!='') $serie = $den_serie; else $serie = $value;
	//echo $value;
	return $serie;
}

function change_servicii($value) {
	
	$replacements = array(
	'mic dejun bufet,'=>'',
);
$den_servicii = str_replace(array_keys($replacements), array_values($replacements), $value);
	/*if($den_masa!='')*/ $servicii = $den_servicii; /*else $meal = $value;*/
	if($den_servicii!='') $servicii = $den_servicii; else $servicii = $value;
	//echo $value;
	return $servicii;
}

function extrage_masa ($string_servicii)
{ $tipuri_de_masa=array(
'pensiune completa'=>'Pensiune Completa',
'pensiune completa meniu fisa cont'=> 'Pensiune Completa Fisa cont',
'cazare cu mic dejun'=>'Mic dejun',
'mic dejun ('=>'Mic dejun',
'fisa cont 200 lei,'=>'Fisa cont',
'demipensiune ('=>'Demipensiune',
'masa cont restaurant (50 '=>'Fisa cont',
'pensiune completa meniu fix'=>'Pensiune Completa Meniu Fix');

foreach($tipuri_de_masa as $k_tip_masa => $v_tip_masa) {
if (strpos(strtolower($string_servicii),$k_tip_masa) !== false) {
    $tip_masa=$v_tip_masa;
}
}
return $tip_masa;

	
	}

function objectToArray( $object ) {
	if(!is_object($object) && !is_array($object)) {
		return $object;
	}
	if(is_object($object)) {
		$object = get_object_vars($object);
	}
	return array_map('objectToArray', $object);
}


function SimpleXML2Array($xml){
    $array = (array)$xml;

    //recursive Parser
    foreach ($array as $key => $value){
        if(strpos(get_class($value),"SimpleXML")!==false){
            $array[$key] = SimpleXML2Array($value);
        }
    }

    return $array;
}

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}


function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

function aasort($array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
}

function multid_sort($arr, $index) {
    $b = array();
    $c = array();
    foreach ($arr as $key => $value) {
        $b[$key] = $value[$index];
    }
    asort($b);
    foreach ($b as $key => $value) {
        $c[] = $arr[$key];
    }
    return $c;
}

function erori_disponibilitate($id_oferta=NULL, $id_hotel=NULL, $plecdata=NULL, $pleczile=NULL, $adulti=NULL, $copii=NULL) {
	$plecdata = date("Y-m-d", strtotime($plecdata));
	
	$ins = "INSERT INTO erori_disponibilitate (id_oferta, id_hotel, data, nr_nopti, nr_adulti, nr_copii, ip, data_adaugare) VALUES ('".$id_oferta."', '".$id_hotel."', '".$plecdata."', '".$pleczile."', '".$adulti."', '".$copii."', '".$_SERVER['REMOTE_ADDR']."', NOW()) ";
	$que = mysql_query($ins) or die(mysql_error());
	@mysql_free_result($que);
}

function showHotels($id_localitate) {
	$value = '';
	
	$sel = "SELECT
	hoteluri.id_hotel,
	hoteluri.nume,
	hoteluri.stele,
	hoteluri.poza1,
	hoteluri.latitudine, 
	hoteluri.longitudine,
	localitati.denumire AS den_loc,
	zone.denumire AS den_zona,
	tari.denumire AS den_tara
	FROM oferte
	INNER JOIN hoteluri ON hoteluri.id_hotel = oferte.id_hotel
	INNER JOIN localitati ON localitati.id_localitate = hoteluri.locatie_id
	INNER JOIN zone ON zone.id_zona = localitati.id_zona
	INNER JOIN tari ON tari.id_tara = zone.id_tara
	WHERE valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND hoteluri.latitudine IS NOT NULL
	AND hoteluri.longitudine IS NOT NULL
	AND localitati.id_localitate = '".$id_localitate."'
	GROUP BY hoteluri.id_hotel
	ORDER BY hoteluri.nume
	";
	$que = mysql_query($sel) or die(mysql_error());
	while($row = mysql_fetch_array($que)) {
		$sel_minprice = "SELECT MIN(pret_minim) AS min_price, moneda, exprimare_pret FROM oferte WHERE valabila = 'da' AND id_hotel = '".$row['id_hotel']."' ";
		$que_minprice = mysql_query($sel_minprice) or die(mysql_error());
		$row_minprice = mysql_fetch_array($que_minprice);
		$stars = '';
		for($i=1; $i<=$row['stele']; $i++) { $stars .= '<i class="icon-star yellow"></i>'; }
$value .= 'var point = new google.maps.LatLng('.$row['latitudine'].','.$row['longitudine'].');
var marker = createMarker(point, \''.$row['nume'].' '.$row['stele'].'*\', \'<a href="'.make_link_oferta($row['den_loc'], $row['nume'], NULL, NULL).'" title="'.$row['nume'].'" class="info-window" style="background-image:url(/img_mediu_hotel/'.$row['poza1'].');"><span class="bkg"><span>'.$row['nume'].' '.$stars.'</span><span class="pret">de la <strong>'.new_price($row_minprice['min_price']).' '.moneda($row_minprice['moneda']).'</strong> /'.$row_minprice['exprimare_pret'].'</span></span></a>\')'."\n";
	}
	
	return $value;
}

function get_airline_by_iata($value, $denumire_companie=NULL) {
	if($value=='OB') $value = '0B';
	$sel = "SELECT * FROM companii_aeriene WHERE (cod_iata = '".$value."' OR cod_icao = '".$value."') ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
if($denumire_companie=='da'){$companie=$row['denumire_companie'];}
else {	$companie=$row['id_companie'];}
return $companie;
}

function get_airline_by_tez($value) {
	$sel = "SELECT * FROM companii_aeriene WHERE tez_id = '".$value."' ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	return $row['cod_iata'];
}

function get_airport_code_by_tez($value) {
	$sel = "SELECT * FROM aeroport WHERE tez_id_aeroport = '".$value."' ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	return $row['cod'];
}

function get_airport_by_iata($value) {
	 $sel = "SELECT * FROM aeroport WHERE cod = '".$value."' ";
	
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);	
	
	return $row['denumire'];
}


function get_detalii_furnizor($id) {
	$sel = "SELECT * FROM furnizori WHERE id_furnizor = '".$id."' ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	return $row;
}

function get_id_furnizor_by_id_oferta($id_oferta) {
	$sel = "SELECT furnizor FROM oferte WHERE id_oferta = '".$id_oferta."' ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	return $row['furnizor'];
}

function get_id_localitate_corespondent($id_localitate, $id_furnizor) {
	$sel = "SELECT * FROM localitati_corespondent WHERE id_furnizor = '".$id_furnizor."' AND id_corespondent = '".$id_localitate."' ";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	return $row['code_furnizor'];
}

function calculeaza_comision($pret, $comision_fix, $comision_procent) {
	if($comision_fix>0) {
		$total = $comision_fix;
	} else if($comision_procent>0) {
		$total = $pret * $comision_procent / 100;
	} else {
		$total = 0;
	}
	
	return $total;
}

function hotel_link($value) {
	$value = str_replace("http://", "", strtolower($value));
	$value = str_replace("http:/", "", $value);
	$value = str_replace("http:", "", $value);
	$value = str_replace("http", "", $value);
	$value = "http://".$value;
	
	return $value;
}

function find_items($array,$findwhat,$value,$found=array()) {
		//if(!$err_logare_admin) { echo '<pre>';print_r($value);echo '</pre>'; }	

	foreach($array as $k=>$v) {
		if(is_array($v)) {
			$result = find_items($v,$findwhat,$value,$found);
			if($result === true) {
				$found[] = $v;
			} else {
				$found = $result;
			}
		} else {
			if($k==$findwhat && strtolower($v)==strtolower($value)){
				return TRUE;
			}
		}
	}
	return $found;	
}


function optimize_rooms($rezcam, $pleczile, $plecdata,$tip_afisare=NULL) {
	
	//$rezcam_filtered = find_items($rezcam,'nr_nights',$pleczile);
	$rezcam_filtered=$rezcam;
	
	foreach($rezcam_filtered as $key => $value)
{
	if( strtolower($rezcam_filtered[$key]['denumire_camera'])!='camera standard') $rezcam_filtered[$key]['denumire_camera']=preg_replace("/ Standard$/","",$rezcam_filtered[$key]['denumire_camera']); 


 $rezcam_filtered[$key]['denumire_camera'] =  $rezcam_filtered[$key]['denumire_camera']."######".$rezcam_filtered[$key]['grila_name']."######".$rezcam_filtered[$key]['nr_nights'];
 if(strlen($rezcam_filtered[$key]['grila_name'])<5) { $rezcam_filtered[$key]['denumire_camera'] =  $rezcam_filtered[$key]['denumire_camera']."######".$rezcam_filtered[$key]['series_name']."######".$rezcam_filtered[$key]['nr_nights'];}	
 //[furnizor_comision_procent]
 $discount_tarif=1;
 $de_plata_la_furnizor[$key]=$rezcam_filtered[$key]['pret']-$rezcam_filtered[$key]['comision'];
 
 if(isset($rezcam_filtered[$key]['oferta_comision_procent']) and $rezcam_filtered[$key]['oferta_comision_procent']>0 ) $discount_tarif = $rezcam_filtered[$key]['oferta_comision_procent']; 
	else $discount_tarif = $rezcam_filtered[$key]['furnizor_comision_procent'];
		$rezcam_filtered[$key]['oferta_pret'] = $rezcam_filtered[$key]['oferta_pret'] * $discount_tarif;
		$rezcam_filtered[$key]['pret'] = $rezcam_filtered[$key]['pret'] * $discount_tarif;
		$rezcam_filtered[$key]['comision']=$rezcam_filtered[$key]['pret']-$de_plata_la_furnizor[$key];
 //if(!$err_logare_admin) echo $rezcam_filtered[$key_rezcam]['furnizor_comision_procent']."=".$rezcam_filtered[$key]['pret'] * $discount_tarif."--------";
 
 
}
	
//	if(!$err_logare_admin) { echo '<pre>';print_r($rezcam_filtered);echo '</pre>'; }	
	$rezcam = array();
	$i=0;
	if(sizeof($rezcam_filtered)>0) {
		$new_rezcam = array_map('unserialize', array_unique(array_map('serialize', $rezcam_filtered)));
		
		$rezcam_available = find_items($new_rezcam,'disponibilitate','stopsales');
				
		if(sizeof($rezcam_available)>0) $new_rezcam_available = array_map('unserialize', array_unique(array_map('serialize', $rezcam_available)));
		if(count($new_rezcam)>count($new_rezcam_available)) $show_unavailable = 'nu';
			else $show_unavailable = 'da';
		
		foreach($new_rezcam as $v_date) $arr_data_start[] = $v_date['data_start'];
		$arr_data_start = array_unique($arr_data_start);
		
		foreach($arr_data_start as $key_date => $date) {
			$filter_date[$key_date] = find_items($new_rezcam,'data_start',$date);
			if(sizeof($filter_date[$key_date])>0) {
				$rezcam_filter_date[$key_date] = array_map('unserialize', array_unique(array_map('serialize', $filter_date[$key_date])));
				
				
				foreach($rezcam_filter_date[$key_date] as $v_cam) $arr_cam[$key_date][] = $v_cam['denumire_camera'];
				$arr_cam[$key_date] = array_unique($arr_cam[$key_date]);
				
				
				
				//if(!$err_logare_admin) { echo '<pre>';print_r($arr_cam[$key_date]);echo '</pre>'; }
				foreach($arr_cam[$key_date] as $key_cam => $cam) {
					$filter_cam[$key_date][$key_cam] = find_items($rezcam_filter_date[$key_date],'denumire_camera',$cam);
							
					if(sizeof($filter_cam[$key_date][$key_cam])>0) {
						$rezcam_filter_cam[$key_date][$key_cam] = array_map('unserialize', array_unique(array_map('serialize', $filter_cam[$key_date][$key_cam])));
						
						
						foreach($rezcam_filter_cam[$key_date][$key_cam] as $v_masa) $arr_masa[$key_date][$key_cam][] = $v_masa['masa'];
						
						$arr_masa[$key_date][$key_cam] = array_unique($arr_masa[$key_date][$key_cam]);
						
						foreach($arr_masa[$key_date][$key_cam] as $key_masa => $masa) {
							
							//if(!$err_logare_admin) { echo '<pre>';print_r($filter_masa[$key_date][$key_cam][$key_masa]);echo '</pre>'; }	
							$i++;
							$filter_masa[$key_date][$key_cam][$key_masa] = find_items($rezcam_filter_cam[$key_date][$key_cam],'masa',$masa);
								
		
							if(sizeof($filter_masa[$key_date][$key_cam][$key_masa])>0) {
								$rezcam_filter_masa[$key_date][$key_cam][$key_masa] = array_map('unserialize', array_unique(array_map('serialize', $filter_masa[$key_date][$key_cam][$key_masa])));
								
								if($show_unavailable=='nu') {
									foreach($rezcam_filter_masa[$key_date][$key_cam][$key_masa] as $key_available => $available) {
										if($available['disponibilitate']=='stopsales') {
											unset($rezcam_filter_masa[$key_date][$key_cam][$key_masa][$key_available]);
										}
									}
								}
									
								if(count($rezcam_filter_masa[$key_date][$key_cam][$key_masa])>1) {
									//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam_filter_masa);echo '</pre>'; }	
									/*$rezcam_filter_masa[$key_date][$key_cam][$key_masa] = multid_sort($rezcam_filter_masa[$key_date][$key_cam][$key_masa], 'disponibilitate');
									$rezcam_filter_masa[$key_date][$key_cam][$key_masa] = array_map('unserialize', array_unique(array_map('serialize', $rezcam_filter_masa[$key_date][$key_cam][$key_masa])));*/
									$sort = array();
									foreach($rezcam_filter_masa[$key_date][$key_cam][$key_masa] as $key_sort => $value_sort) {
										
										
										$sort['disponibilitate'][$key_sort] = $value_sort['disponibilitate'];
										$sort['pret'][$key_sort] = $value_sort['pret'];
										}
									
									
									array_multisort($sort['disponibilitate'], SORT_ASC, $sort['pret'], SORT_ASC, $rezcam_filter_masa[$key_date][$key_cam][$key_masa]);
									
									foreach($rezcam_filter_masa[$key_date][$key_cam][$key_masa] as $key_offer => $offer) {
										if($key_offer>0) unset($rezcam_filter_masa[$key_date][$key_cam][$key_masa][$key_offer]);
									}
								}
								
								foreach($rezcam_filter_masa[$key_date][$key_cam][$key_masa] as $key1 => $value1) {
									foreach($value1 as $key2 => $value2) {
										$rezcam[$i][$key2] = $value2;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/*$rezcam = multid_sort($rezcam, 'pret');
	$rezcam = array_map('unserialize', array_unique(array_map('serialize', $rezcam)));*/
	//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam_filter_masa);echo '</pre>'; }	
				//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; }	


	$sort_pret = array();
	$tip_masa=array();
	foreach($rezcam as $key_sort_pret => $value_sort_pret) {
		if($value_sort_pret['disponibilitate']==''){$value_sort_pret['disponibilitate']='stopsales';}
		$sort_pret['disponibilitate'][$key_sort_pret] = $value_sort_pret['disponibilitate'];
		$sort_pret['pret'][$key_sort_pret] = $value_sort_pret['pret'];
		//$tip_masa['disponibilitate'][$key_sort_pret] = $value_sort_pret['disponibilitate'];
		$tip_masa[$key_sort_pret] =$value_sort_pret['masa'];
	}
	

				//if(!$err_logare_admin) { echo '<pre>';print_r($sort_pret);echo '</pre>'; }	

	array_multisort($sort_pret['disponibilitate'], SORT_ASC, $sort_pret['pret'], SORT_ASC, $rezcam);


	//if(!$err_logare_admin) { 
	//echo '<pre>';print_r($rezcam);echo '</pre>'; 
	//s}
	
	//if(!$err_logare_admin) { 
	if($tip_afisare=='scurt'){
	//echo '<pre>';print_r(array_unique($tip_masa));echo '</pre>'; 
	//$rezcam_n=array();
	
	
	//foreach($rezcam as $key_sort_pret_1 => $value_sort_pret_1)
	//{ if ($value_sort_pret_1['masa']== 'Mic dejun')
	//{
		//echo $key_sort_pret_1."<br/>";
	$tip_masa=array_unique($tip_masa);
	$j=0;
	foreach($tip_masa as $key_masa => $value_masa) 
	{ 	
	$rezcam_mic=find_items($rezcam,'masa',$value_masa);
	$rezcam_n[$j]=$rezcam_mic['0'];
	$j++;
	}
	$rezcam=$rezcam_n;
	foreach($rezcam as $key_sort_pret_1 => $value_sort_pret_1) {
		//$sort_pret['disponibilitate'][$key_sort_pret] = $value_sort_pret['disponibilitate'];
		if($sort_pret['pret'][$key_sort_pret_1]>100){
		$sort_pret['pret'][$key_sort_pret_1] = $value_sort_pret['pret'];
		//$tip_masa['disponibilitate'][$key_sort_pret] = $value_sort_pret['disponibilitate'];
		//$tip_masa[$key_sort_pret] = $value_sort_pret['masa'];
		}
	}
	
	//array_multisort($sort_pret['pret'], SORT_ASC, $rezcam);
	//if(!$err_logare_admin) { 
		//echo '<pre>';print_r($sort_pret['pret']);echo '</pre>';
		//echo '<pre>';print_r($rezcam);echo '</pre>';
	//}
	//}
	
	//}
	
	}
	//}	
	//if ($nr_inregistrari>0){$rezcam=array_splice($rezcam,0, $nr_inregistrari);}
	foreach($rezcam as $key => $value)
{
 $array_camera=  explode('######',$rezcam[$key]['denumire_camera']);
 $rezcam[$key]['denumire_camera']=$array_camera['0'];
   
}
	

	return $rezcam;
}

function array_equal($a, $b) {
    return (
         is_array($a) 
         && is_array($b) 
         && count($a) == count($b) 
         && array_diff($a, $b) === array_diff($b, $a)
    );
}





function array2csv(array &$array) {
	if(count($array) == 0) {
		return null;
	}
	ob_start();
	$df = fopen("php://output", 'w');
	//fputcsv($df, array_keys($array));
	foreach($array as $row) {
		fputcsv($df, $row,chr(9));
	}
	fclose($df);
	return ob_get_clean();
}

function download_send_headers($filename) {
	// disable caching
	$now = gmdate("D, d M Y H:i:s");
	header("Expires: Tue, 01 Jan 2111 06:00:00 GMT");
	header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
	header("Last-Modified: {$now} GMT");
	
	// force download  
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	
	// disposition / encoding on response body
	header("Content-Disposition: attachment;filename={$filename}");
	header("Content-Transfer-Encoding: binary");
}

function check_basket($id_oferta, $id_pret, $plecari, $data_plecare, $nr_nopti, $id_cam, $den_cam, $nr_adulti, $nr_copii, $tip_masa, $pret) {
	$occure = 0;
	
	$cos_array = explode('+;++;+', $_COOKIE['cos_cumparaturi']);
	foreach($cos_array as $codedData) {
		$det_cos = explode('##', base64_decode($codedData));
		$id_oferta_cos = $det_cos[0]; // id oferta
		$cos_id_pret = $det_cos[1]; // id pret
		$cos_plecare = $det_cos[2]; // tip preturi (plecari sau perioade)
		$cos_data = $det_cos[3]; // data plecarii
		$cos_zile = $det_cos[4]; // nr nopti
		$cos_pret = $det_cos[5]; // pret
		$cos_id_cam = $det_cos[6]; // id_cam
		$cos_nr_adulti = $det_cos[7]; // nr adulti
		$cos_nr_copii = $det_cos[8]; // nr copii
		$cos_copil1 = $det_cos[9]; // copil1
		$cos_copil2 = $det_cos[10]; // copil2
		$cos_copil3 = $det_cos[11]; // copil3
		$cos_masa = $det_cos[12]; // tip masa
		$cos_den_camera = $det_cos[13]; // denumire camera
		
		if($id_oferta==$id_oferta_cos and $id_pret==$cos_id_pret and $plecari==$cos_plecare and $data_plecare==$cos_data and $nr_nopti==$cos_zile and $id_cam==$cos_id_cam and $den_cam==$cos_den_camera and $nr_adulti==$cos_nr_adulti and $nr_copii==$cos_nr_copii and $tip_masa==$cos_masa and $pret==$cos_pret) $occure += 1;
	}
	
	if($occure>0) return true;
	else return false;

	/*$sel = "SELECT * FROM cos_cumparaturi
	WHERE id_oferta = '".$id_oferta."'
	AND id_pret = '".$id_pret."'
	AND plecari = '".$plecari."'
	AND data_plecare = '".$data_plecare."'
	AND nr_nopti = '".$nr_nopti."'
	AND id_cam = '".$id_cam."'
	AND den_cam = '".$den_cam."'
	AND nr_adulti = '".$nr_adulti."'
	AND nr_copii = '".$nr_copii."'
	AND tip_masa = '".$tip_masa."'
	AND pret = '".$pret."'
	AND tip_stergere IS NULL
	";
	$que = mysql_query($sel) or die(mysql_error());
	$row = mysql_fetch_array($que);
	
	if(mysql_num_rows($que)==1) return true;
	else return false;*/
}

function cache_prices($ukey, $id_hotel) {
	$prices = array();
	$i=0;
	
 $sel = "SELECT * FROM pret_cache WHERE ukey = '".$ukey."' AND id_hotel = '".$id_hotel."' ";
	$que = mysql_query($sel) or die(mysql_error());
	while($row = mysql_fetch_array($que)) {
		$i++;
                
               
		$prices[$i] = $row;
	 //$prices[$i][28]=1; 
        $prices[$i]['furnizor_comision_procent'] =1;
                }
               
        
	//if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	echo '<pre>';print_r ($prices);echo '</pre>';
	return $prices;
	


}

function cache_planes($ukey, $id_hotel,$id_zbor=NULL) {
	$flights = array();
	$i=0;
	
	$sel = "SELECT * FROM pret_planes_cache WHERE ukey = '".$ukey."' AND id_hotel = '".$id_hotel."' AND id_zbor='".$id_zbor."' ";
	$que = mysql_query($sel) or die(mysql_error());
	while($row = mysql_fetch_array($que)) {
		$i++;
		$flights[$row['id_oferta']]['flight_bookable'] = $row['flight_bookable'];
		$flights[$row['id_oferta']][$row['tip_cursa']][$row['key_flight']][$row['key_curse']]['from'] = $row['from'];
		$flights[$row['id_oferta']][$row['tip_cursa']][$row['key_flight']][$row['key_curse']]['to'] = $row['to'];
		$flights[$row['id_oferta']][$row['tip_cursa']][$row['key_flight']][$row['key_curse']]['data_plecare'] = $row['data_plecare'];
		$flights[$row['id_oferta']][$row['tip_cursa']][$row['key_flight']][$row['key_curse']]['data_sosire'] = $row['data_sosire'];
		$flights[$row['id_oferta']][$row['tip_cursa']][$row['key_flight']][$row['key_curse']]['companie'] = $row['companie'];
		$flights[$row['id_oferta']][$row['tip_cursa']][$row['key_flight']][$row['key_curse']]['nr_cursa'] = $row['nr_cursa'];
		$flights[$row['id_oferta']][$row['tip_cursa']][$row['key_flight']][$row['key_curse']]['seats'] = $row['seats'];
	}
	
	return $flights;
}
	
function afiseaza_orar_zbor($data)
{	
if(date("H:i",strtotime($data))=="00:00")
						{$data_out=date("d.m.Y", strtotime($data));}
						else
						{$data_out=date("d.m.Y - H:i", strtotime($data));}
return $data_out;
}
function extrage_data($string)
{
	
	//echo "cccc".$data=ereg('(\d+)[-.\/](\d+)[-.\/](\d+)','Reducere Early Booking  31.03.2015');
	//print_r(date_parse("Reducere Early Booking  31.03.2015"));
	$array_data=date_parse($string);
	
	$data=$array_data['day'].'.'.$array_data['month'].'.'.$array_data['year'];
	$data=date("d.m.Y", strtotime($data));
	if(date_parse($string)=='false'){$data='';}
// echo "OK".$string;
$patern='((0[1-9]|[1-2][0-9]|3[0-1])/(0[1-9]|1[0-2])/([0-9]{4}))';
if( preg_match($patern,$string))
{echo "ok";}
  
	//	 print_r($data_array);
   
	//echo $data."<br />";
	/* 
	preg_match( '/([0-9]?[0-9])[\.\-\/ ]+([0-1]?[0-9])[\.\-\/ ]+([0-9]{2,4})/', $string, $matches );
  if ( $matches ) {
    if ( $matches[1] )
      $day = $matches[1];
    if ( $matches[2] )
      $month = $matches[2];
    if ( $matches[3] )
      $year = $matches[3];
  }
	*/
$data=$array_data['day'].'.'.$array_data['month'].'.'.$array_data['year'];	
	
	return $data;
}

function extrage_data_solvex($string)
{ 
//echo $string."<br />";

preg_match("/((<|<=|%-)([0-9]{2})\.([0-9]{2}))|(([0-9]{2})\.([0-9]{2})\.([0-9]{4}))|(<=([0-9]{2})\.([0-9]{2})\.([0-9]{2}))|(([0-9]{2})\.([0-9]{2})(-min.5))/",$string, $data_solvex);
//str_replace('%b<','',$data_solvex['0']);
//print_r($data_solvex);

if($data_solvex['2']!=2017 || $data_solvex['2']!=2018||$data_solvex['2']!=17) { if(($data_solvex[sizeof($data_solvex)-1])<12) {$an='2018';} else {$an='2017';}; $data_solvex_nou=$data_solvex[sizeof($data_solvex)-2].'.'.$data_solvex[sizeof($data_solvex)-1].'.'.$an;}
if($data_solvex[sizeof($data_solvex)-1]=='-min.5'){ if(($data_solvex[sizeof($data_solvex)-2])<12) {$an='2018';} else {$an='2017';} $data_solvex_nou=$data_solvex[sizeof($data_solvex)-2].'.'.$data_solvex[sizeof($data_solvex)-2].'.'.$an;}
if($data_solvex[sizeof($data_solvex)-1]==16) { if(($data_solvex[sizeof($data_solvex)-3])<12) {$an='2018';} else {$an='2017';}  $data_solvex_nou=$data_solvex[sizeof($data_solvex)-2].'.'.$data_solvex[sizeof($data_solvex)-3].'.'.$an;}
return str_replace('%b<','',$data_solvex_nou);
	
	}

function extrage_procent($string)
{ 
preg_match("/([0-9]{1,2}|100)%/",$string, $comision_reducere);

return substr($comision_reducere['0'], 0, -1);;
	
	}
function hoteluri_localitate($id_localitate,$id_hotel=NULL,$latitudine_curenta=NULL,$longitudine_curenta=NULL)
{
	$hoteluri_localitate=array();
$select_hoteluri = "SELECT nume as denumire, localitati.denumire as localitate,hoteluri.id_hotel,stele,hoteluri.poza1, hoteluri.latitudine, hoteluri.longitudine 
FROM hoteluri 
Inner Join oferte ON hoteluri.id_hotel = oferte.id_hotel
Inner join localitati on hoteluri.locatie_id=localitati.id_localitate
WHERE hoteluri.locatie_id = '".$id_localitate."' and oferte.valabila='da' 
group by hoteluri.id_hotel";
$hoteluri = mysql_query($select_hoteluri) or die(mysql_error());
	while($row_hoteluri_localitate = mysql_fetch_array($hoteluri))
	 {
		$i++;
		$hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['denumire'] = $row_hoteluri_localitate['denumire'];
		$hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['stele'] = $row_hoteluri_localitate['stele'];
		$hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['localitate'] = $row_hoteluri_localitate['localitate'];
		$hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['latitudine'] = $row_hoteluri_localitate['latitudine'];
		$hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['longitudine'] = $row_hoteluri_localitate['longitudine'];
		$hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['id_hotel'] = $row_hoteluri_localitate['id_hotel'];
		$hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['poza1'] = $row_hoteluri_localitate['poza1'];
                if($latitudine_curenta>0 and $longitudine_curenta>0 and $id_hotel>0){
                $hoteluri_localitate[$row_hoteluri_localitate['id_hotel']]['distanta'] = distanta($row_hoteluri_localitate['latitudine'],$row_hoteluri_localitate['longitudine'],$latitudine_curenta,$longitudine_curenta,'M');
				
         }       }
return $hoteluri_localitate;
//echo '<pre>';print_r($hoteluri_localitate);echo '</pre>';	
//mysql_close($hoteluri);
}
function super_unique($array)
{
  $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

  foreach ($result as $key => $value)
  {
    if ( is_array($value) )
    {
      $result[$key] = super_unique($value);
    }
  }

  return $result;
}
function e_data($myDateString){
    return (bool)strtotime($myDateString);
}

function xml_attribute($object, $attribute)
{
    if(isset($object[$attribute]))
        return (string) $object[$attribute];
}

function _isValidXML($xml) {
    $doc = @simplexml_load_string($xml);
    if ($doc) {
        return true; //this is valid
    } else {
        return false; //this is not valid
    }
}
function nr_turisti() 
{
	$select_nr_turisti="Select count(id_pasager)as nr_pasageri from pasageri";
	$nr_turisti = mysql_query($select_nr_turisti) or die(mysql_error());
	$numar_pasageri= mysql_fetch_array($nr_turisti);
	return $numar_pasageri['nr_pasageri'];
	}
	
function currentPageURL() {
$pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
function distanta($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
      return ($miles * 1.609344);
      
  } else if ($unit == "M") {
      return round(($miles * 1.609344)*1000,0);
  } 
}
function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}
 function curPageURL()
     {
         $pageURL = 'http';
         if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
         $pageURL .= "://";
         if ($_SERVER["SERVER_PORT"] != "80") {
            
          //   $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
         }
         else {
             $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
         }
         return $pageURL;
     }
function link_document($seria_numarul,$tip_document)
{ 
  global $sitepath;
  global $key_criptare;
  $seria_document=preg_replace('/[0-9]/','',$seria_numarul);
  $nr_document=preg_replace('/([A-Z])-([A-Z])|([A-Z])/','',$seria_numarul);  
  $string_criptat=$seria_document.'*'.$nr_document.'*'.$tip_document;
  $link_document=$sitepath.'user-doc/'.encrypt($string_criptat,$key_criptare).'/';

	return $link_document;
	
	}
		
function redirect($url){
    if (headers_sent()){
      die('<script type="text/javascript">window.location.href="' . $url . '";</script>');
    }else{
      header('Location: ' . $url);
      die();
    } 
}
function parse_csv_file($csvfile) {
	$csv = Array();
    $rowcount = 0;
    if (($handle = fopen($csvfile, "r")) !== FALSE) {
        $max_line_length = defined('MAX_LINE_LENGTH') ? MAX_LINE_LENGTH : 10000;
        $header = fgetcsv($handle, $max_line_length);
        $header_colcount = count($header);
        while (($row = fgetcsv($handle, $max_line_length)) !== FALSE) {
            $row_colcount = count($row);
            if ($row_colcount == $header_colcount) {
               $entry = array_combine($header, $row);
                $csv[] = $entry;
				
            }
            else {
                error_log("csvreader: Invalid number of columns at line " . ($rowcount + 2) . " (row " . ($rowcount + 1) . "). Expected=$header_colcount Got=$row_colcount");
               echo"-1-";
			    //return null;
            }
            $rowcount++;
        }
       // print_r($csv);
	    //echo "Totally $rowcount rows found\n";
        fclose($handle);
    }
    else {
        error_log("csvreader: Could not read CSV \"$csvfile\"");
        return null;
    }
    //print_r($csv);
	return $csv;
}

function csvstring_to_array($string, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n") {
    // @author: Klemen Nagode
    $array = array();
    $size = strlen($string);
    $columnIndex = 0;
    $rowIndex = 0;
    $fieldValue="";
    $isEnclosured = false;
    for($i=0; $i<$size;$i++) {

        $char = $string{$i};
        $addChar = "";

        if($isEnclosured) {
            if($char==$enclosureChar) {

                if($i+1<$size && $string{$i+1}==$enclosureChar){
                    // escaped char
                    $addChar=$char;
                    $i++; // dont check next char
                }else{
                    $isEnclosured = false;
                }
            }else {
                $addChar=$char;
            }
        }else {
            if($char==$enclosureChar) {
                $isEnclosured = true;
            }else {

                if($char==$separatorChar) {

                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";

                    $columnIndex++;
                }elseif($char==$newlineChar) {
                    echo $char;
                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";
                    $columnIndex=0;
                    $rowIndex++;
                }else {
                    $addChar=$char;
                }
            }
        }
        if($addChar!=""){
            $fieldValue.=$addChar;

        }
    }

    if($fieldValue) { // save last field
        $array[$rowIndex][$columnIndex] = $fieldValue;
    }
    return $array;
}
function afiseaza_stele($nr_stele)
{
	for($i = 1; $i <= $nr_stele; $i++) 
{
	$stele_afisate.="<i class=\"icon-star\"></i>";
	
}
return $stele_afisate;
}
function calculeaza_valuta_combinata($bani,$curs_valutar_rezervare) 
{
	
if(sizeof($bani,1)>0) {
	$temp=array();
	foreach ($bani as $k => $v)  {
	foreach ($v['client'] as $key => $value) {
		array_push($temp,$key); 
		}
		
		}
	$temp=array_unique($temp);
	
	if(sizeof($temp)>1) {
	foreach ($bani as $k => $v)  {
	foreach ($v['client'] as $key => $value) 
	{ if (strtolower($key)=='eur') {$valoare_f=$value*$curs_valutar_rezervare;}else {$valoare_f=$value;}
	$valoare_finala+=$valoare_f;
	} 
		}
}
else {
	
	foreach ($bani as $k => $v)  {
	foreach ($v['client'] as $key => $value)  {
		$valoare_f=$value;
		$valoare_finala+=$valoare_f;
		
		}
		}
	
	
	
	//echo $valoare_f=$value;
	}
} 
else {$valoare_finala=0;}

//echo $curs_valutar_rezervare; 
//echo '<pre>';print_r($temp);echo '</pre>';	
	return round($valoare_finala,2);
	}
function verifica_localizarea($tara,$zona,$localitate=NULL)
{
	if(isset($_COOKIE["localizare"])) 
			{
				$localizare_array=explode('*',$_COOKIE['localizare']);
				if (strtolower($localizare_array['0'])!=strtolower($tara) and strtolower($localizare_array['0'])!=strtolower($zona))
				{
					
								setcookie( "localizare", strtolower($tara).'*'.strtolower($zona).'*'.$localiate, time() + 3600, '/' );

						$array_grad_ocupare=explode('*',$_COOKIE['grad_ocupare']);
								setcookie( "grad_ocupare",$array_grad_ocupare['0'].'*'.$array_grad_ocupare['1'].'*'.$array_grad_ocupare['2'].'*'.$array_grad_ocupare['3'].'*'.$array_grad_ocupare['4'].'*'.$array_grad_ocupare['5'], time() + 30*24*3600, '/' );	
							//	setcookie( "grad_ocupare", "", time() - 3600, '/' );
					
					}
				} else {
			setcookie( "localizare", strtolower($tara).'*'.strtolower($zona).'*'.$localiate, time() + 3600, '/' );
				}
	
	
	}	
function SplitIntoColumns ($InputString, $Columns, $SplitString) {
 
  
    $ColLength = strlen($InputString)/$Columns;
 
    // Split into columns
    for($ColCount = 1; $ColCount <= $Columns; $ColCount++) {
 
        // Find $SplitString, position to cut
        $Pos = strpos($InputString , $SplitString , $LastPos + $ColLength);
        if ($Pos === false) {
            $Pos = strlen($InputString);
        }
 
        // Cut out column
        $Output[$ColCount - 1] = substr($InputString, $LastPos, $Pos - $LastPos);
 
        $LastPos = $Pos;
     
    }
 
    return $Output;
 
 }	
function delete_all_between($beginning, $end, $string) {
  $beginningPos = strpos($string, $beginning);
  $endPos = strpos($string, $end);
  if ($beginningPos === false || $endPos === false) {
    return $string;
  }

  $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

  return str_replace($textToDelete, '', $string);
} 
?>
