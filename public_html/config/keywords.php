<?php class new_formula_keywords_new{
var $id_oferta;
var $tip_oferta;
var $denumire_oferta;
var $localitati_traversate1;
var $localitati_traversate2;
var $tari_traversate;
var $localitati_titlu="";
var $nr_taguri;

var $sir_adiacent="sejururi";

function new_formula_keywords_new($param){
	$this->id_oferta=$param['id_oferta'];
}

//oferta sejur inceput
function do_sejur(){
	$sel_of="
	SELECT
	oferte.denumire AS denumire_oferta,
	oferte.nr_zile,
	oferte.nr_nopti,
	hoteluri.nume as denumire_hotel,
	hoteluri.stele,
	localitati.denumire AS denumire_localitate,
	zone.denumire AS denumire_zona,
	tari.denumire AS denumire_tara,
	transport.denumire AS denumire_transport,
	usernames.denumire_utilizator as nume_agentie,
	usernames.mail as mail_agentie,
	tip_oferta.denumire_tip_oferta,
	tip_oferta.seo_short,
	tip_oferta.seo_nice
	FROM
	oferte
	Left Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	Left Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Left Join zone ON localitati.id_zona = zone.id_zona
	Left Join tari ON zone.id_tara = tari.id_tara
	Left Join transport ON oferte.id_transport = transport.id_trans
	Left Join usernames ON oferte.id_utilizator = usernames.id
	Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	Left Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	WHERE
	oferte.id_oferta='".$this->id_oferta."'
	
	";
	$rez_of=mysql_query($sel_of);
	$row_rez=@mysql_fetch_array($rez_of,MYSQL_ASSOC);
	$this->denumire_oferta=$row_rez['denumire_oferta'];
	$return = $this->do_nice_sejur($row_rez);
	unset($row_rez);
	@mysql_free_result($rez_of);
	return $return;
}

function do_nice_sejur($array){
	if($array['nr_nopti'] > '1') $nopti=$array['nr_nopti']." nopti"; else $nopti=$array['nr_nopti']." noapte";
	$return[1]=ucwords(strtolower($array['denumire_hotel']))." | ".ucwords($array['denumire_tara'])." | ".ucwords($array['denumire_transport'])." | ".$array['nume_agentie'];
	$return[0]="Oferta sejur ".ucwords(strtolower($array['denumire_oferta']))." in ".$array['denumire_tara'].", petrece-ti vacanta timp de ".$nopti." in  ".$array['denumire_zona']." avand cazare la  ".$array['denumire_hotel']." si calatoreste cu una din multele Agentii de turism, ".$array['nume_agentie'];
	$return[2]=$array['denumire_tip_oferta']." ".$array['denumire_tara'].", ".$array['denumire_zona'].", ".$array['denumire_hotel'].", ".$array['nume_agentie']." ,sejur ".ucwords(strtolower($array['denumire_oferta'])).", cazare, sejur, oferta, vacanta, agentii de turism, ".$array['denumire_tip_oferta']." ".$array['denumire_tara'].", sejur ".$array['denumire_zona'].", sejur ".$array['denumire_tara'].", ".$nopti.", cazare ".$array['denumire_tara'].", cazare ".$array['denumire_zona'];
	return $return;	
}//oferta sejur end

//oferta sejur last minute inceput
function do_sejur_last_minute(){
	$sel_of="
	SELECT
	oferte.denumire AS denumire_oferta,
	oferte.nr_zile,
	hoteluri.nume as denumire_hotel,
	hoteluri.stele,
	localitati.denumire AS denumire_localitate,
	zone.denumire AS denumire_zona,
	tari.denumire AS denumire_tara,
	transport.denumire AS denumire_transport,
	usernames.denumire_utilizator as nume_agentie,
	usernames.mail as mail_agentie,
	tip_oferta.denumire_tip_oferta,
	tip_oferta.seo_short,
	tip_oferta.seo_nice
	FROM
	oferte
	Left Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	Left Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Left Join zone ON localitati.id_zona = zone.id_zona
	Left Join tari ON zone.id_tara = tari.id_tara
	Left Join transport ON oferte.id_transport = transport.id_trans
	Left Join usernames ON oferte.id_utilizator = usernames.id
	Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	Left Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	WHERE
	oferte.id_oferta='".$this->id_oferta."'
	
	";
	$rez_of=mysql_query($sel_of);
	$row_rez=@mysql_fetch_array($rez_of,MYSQL_ASSOC);
	$this->denumire_oferta=$row_rez['denumire_oferta'];
	$return = $this->do_nice_sejur_detalii_last_minute($row_rez);
	unset($row_rez);
	@mysql_free_result($rez_of);
	return $return;
}

function do_nice_sejur_detalii_last_minute($array){
	$return[1]="Last minute ".$array['denumire_hotel']." - ".ucwords(strtolower($array['denumire_oferta']));
	$return[0]="Oferta last minute ".ucwords(strtolower($array['denumire_oferta']))." in ".$array['denumire_tara'].", cazeaza-te la ".$array['denumire_hotel']." sprin agentia de turism, ".$array['nume_agentie'];
	$return[2]=$array['denumire_tip_oferta']." ".$array['denumire_tara'].", ".$array['denumire_zona'].", ".$array['denumire_hotel'].", ".$array['nume_agentie']." ,sejur ".ucwords(strtolower($array['denumire_oferta'])).", cazare, sejur, oferta, vacanta, agentii de turism, ".$array['denumire_tip_oferta']." ".$array['denumire_tara'].", sejur ".$array['denumire_zona'].", sejur ".$array['denumire_tara'].", cazare ".$array['denumire_tara'].", cazare ".$array['denumire_zona'];
	return $return;	
}//oferta sejur last minute end

//oferta circuit inceput
function do_circuit(){
	$sel_of="
	SELECT
	continente.nume_continent as denumire_continent,
	circuite.denumire as denumire_oferta,
	circuite.nr_zile,
	info_circuite.obiective_turistice,
	usernames.denumire_utilizator,
	info_circuite.tag
	FROM
	continente
	Left Join circuite ON continente.id_continent = circuite.id_continent
	Left Join info_circuite ON circuite.id_circuit = info_circuite.id_circuit
	Left join usernames ON usernames.id = circuite.id_utilizator 
	WHERE
	circuite.id_circuit =  '".$this->id_oferta."'
	";
	$circuit_do = new CIRCUIT($this->id_oferta); 
	$circuit_do->load_localitati_and_tari_traversate(); 
	$this->localitati_traversate1 = implode(', circuit ',explode('-',$circuit_do->localitati_traversate));
	$this->localitati_traversate2 = $circuit_do->localitati_traversate;
	$this->tari_traversate = implode(', ',explode('-',$circuit_do->tari_traversate));
	//folosit la titlul circuitului
	$localitati_traversate = explode('-',$circuit_do->localitati_traversate);
	$i=1;
	$this->localitati_titlu = $localitati_traversate[0];
	while(strlen($this->localitati_titlu) < 50 && isset($localitati_traversate[$i])){
		$this->localitati_titlu.= ", ".$localitati_traversate[$i];
		$i++;
	}
	$rez_of=mysql_query($sel_of);
	$row_rez=mysql_fetch_array($rez_of,MYSQL_ASSOC);
	$this->denumire_oferta=$row_rez['denumire_oferta'];
	$return = $this->do_nice_circuit($row_rez);
	unset($row_rez);
	@mysql_free_result($rez_of);
	return $return;
}

function do_nice_circuit($array){
	$return[0]="Circuit ".$array['denumire_continent'].", circuit ".$this->localitati_traversate2.", ".$array['nr_zile']." zile, ".$array['tag'].", Agentia de turism ".$array['denumire_utilizator'];
	$return[1]="Vacanta in ".$array['denumire_continent'].", circuit in ".$this->localitati_traversate2.", durata ".$array['nr_zile']." zile, de la Agentia de turism ".$array['denumire_utilizator'];
	$return[2]=$this->denumire_oferta." | ".$array['denumire_utilizator'];
	return $return;	
}//oferta circuit end
 
//oferta cazare inceput
function do_cazare(){
	$sel_of="
	SELECT
	oferte_cazari.denumire_cazare as denumire_oferta,
	oferte_cazari.tag,
	hoteluri.nume as denumire_hotel,
	hoteluri.stele,
	localitati.denumire AS denumire_localitate,
	zone.denumire AS denumire_zona,
	tari.denumire AS denumire_tara,
	usernames.denumire_utilizator as nume_agentie,
	usernames.mail as mail_agentie
	FROM
	oferte_cazari
	Left Join hoteluri ON oferte_cazari.id_hotel = hoteluri.id_hotel
	Left Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Left Join zone ON localitati.id_zona = zone.id_zona
	Left Join tari ON zone.id_tara = tari.id_tara
	Left Join usernames ON oferte_cazari.id_utilizator = usernames.id
	WHERE
	oferte_cazari.id_cazare='".$this->id_oferta."'
	";
	$rez_of=mysql_query($sel_of);
	$row_rez=mysql_fetch_array($rez_of,MYSQL_ASSOC);
	$this->denumire_oferta=$row_rez['denumire_oferta'];
	$return = $this->do_nice_cazare($row_rez);
	unset($row_rez);
	@mysql_free_result($rez_of);
	return $return;
}

function do_nice_cazare($array){
	$return[0].="Cazare ".$array['denumire_tara'].", cazare ".$array['denumire_zona'].", cazare ".$array['denumire_localitate'].", ".$array['denumire_hotel'].", ".$array['stele']." stele ".$array['tag'].", Agentia de turism ".$array['nume_agentie'];
	$return[1]="Vacanta in ".$array['denumire_tara'].", ".$array['denumire_zona']." la ".$array['denumire_hotel']." ".$array['stele']." stele de la Agentia de turism ".$array['nume_agentie'];
	$return[2]="Cazare ".$array['denumire_tara'].", cazare ".$array['denumire_zona'].", cazare ".$array['denumire_localitate']." la ".$array['denumire_hotel']." ".$array['stele']." stele de la Agentia de turism ".$array['nume_agentie'];
	return $return;	
}//oferta circuit end

//pensiuni inceput
function do_pensiuni(){
$sel_of="
	SELECT
	pensiuni.denumire_hotel,
	tip_hotel.nume,
	pensiuni.categorie,
	localitati.denumire AS localitati_denumire,
	zone.denumire AS zone_denumire,
	tari.denumire AS tari_denumire,
	pensiuni.id_firma
	FROM
	pensiuni
	Inner Join localitati ON pensiuni.localitate = localitati.id_localitate
	Inner Join tip_hotel ON pensiuni.tip = tip_hotel.id
	Inner Join zone ON localitati.id_zona = zone.id_zona
	Inner Join tari ON zone.id_tara = tari.id_tara
	WHERE
	pensiuni.id_hotel='".$this->id_oferta."'
	";
	$rez_of=mysql_query($sel_of);
	$row_rez=mysql_fetch_array($rez_of,MYSQL_ASSOC);
	$this->denumire_oferta=$row_rez['denumire_hotel'];
	$return = $this->do_nice_pensiune($row_rez);
	unset($row_rez);
	@mysql_free_result($rez_of);
	return $return;
}

function do_nice_pensiune($array){
	$return[0] = $this->sir_adiacent.", ".$array['tari_denumire'];
	$return[0].= ", Pensiuni si hoteluri ".$array['zone_denumire'].", Pensiuni si hoteluri  ".$array['tari_denumire'].", Pensiuni si hoteluri ".$array['zone_denumire'];
	$return[0].= ", Pensiuni si hoteluri ".$array['denumire_hotel'];
	$return[1] = "Pensiuni si hoteluri in ".$array['tari_denumire'].", in zona ".$array['zone_denumire']." la hotelul  ".$array['nume']."-".$array['denumire_hotel'];
	$return[1].= " ".$array['categoria']." stele";
	
	$return[2].="Pensiuni si hoteluri Romania, ".$array['zone_denumire'].", ".$array['localitati_denumire'].", ".$array['nume']."-".$array['denumire_hotel']." ".$array['categorie']."*"." ".$array['id_firma'];
	//$return[2].= $array['nume']."-".$array['denumire_hotel'];
	return $return;	
 }
 function do_nice_pensiuni1(){
  if(!$_REQUEST['zona']) {
   $return[0] ="Pensiuni si Hoteluri Romania de la agentii de turism";
   $return[1] = "Pensiuni Romania, Hoteluri Romania, pensiuni de la peste 500 de agentii de turism ";
   $return[2] = "Pensiuni Romania, Litoral, Prahova, Delta Dunarii, Bran Moeciu, Moldova, Bucuresti si Ilfov, Apuseni, Parang, Poiana Brasov, agentii de turism "; 
  } else { 
   $return[0] ="Pensiuni si hoteluri judetul ".ucwords(desfa_link($_REQUEST['zona']))." de la agentii de turism";
   $return[1] = "Pensiuni Romania judetul ".ucwords(desfa_link($_REQUEST['zona']))." de la peste 500 de agentii de turism";
   $return[2] = "Pensiuni ".ucwords(desfa_link($_REQUEST['zona'])).", Hoteluri ".ucwords(desfa_link($_REQUEST['zona'])).", Pensiuni Romania, agentii de turism "; 
  }
   return $return;	
}
function do_nice_pensiuni2(){
   $return[0] ="Pensiuni si hoteluri ".ucwords(desfa_link($_REQUEST['oras']))." - judetul ".ucwords(desfa_link($_REQUEST['zona']))." de la agentii de turism";
   $return[1] = "Pensiuni ".ucwords(desfa_link($_REQUEST['oras'])).", hoteluri ".ucwords(desfa_link($_REQUEST['oras']))." din judetul ".ucwords(desfa_link($_REQUEST['zona']))." de la peste 500 de agentii de turism ";
   $return[2] = "Pensiuni ".ucwords(desfa_link($_REQUEST['oras'])).", Hoteluri ".ucwords(desfa_link($_REQUEST['oras'])).", Pensiuni ".ucwords(desfa_link($_REQUEST['zona'])).", Hoteluri ".ucwords(desfa_link($_REQUEST['zona'])).", Pensiuni Romania, agentii de turism ";
   return $return;	
}
// pensiuni end

//apare la sejur0 
function do_nice_sejur_new(){
include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
if(!$_REQUEST['tip']) {
$tr='';
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=$tr.", transport propriu"; else $tr=$tr.", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
$return[1]="Oferte sejur".$tr;
 $tip="SELECT
	tip_oferta.denumire_tip_oferta,
	tip_oferta.seo_short,
	tip_oferta.seo_nice
	FROM
	oferte
	Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	Inner Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	Right Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	Right Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Right Join zone ON localitati.id_zona = zone.id_zona
	Right Join tari ON zone.id_tara = tari.id_tara
	WHERE
	oferte.valabila =  'Da' AND
	tip_oferta.activ =  'da'
	GROUP BY
	tip_oferta.denumire_tip_oferta ";
	$que_tip=mysql_query($tip) or die(mysql_error());
	while($row=@mysql_fetch_array($que_tip)){
	
	$return[2].="oferte ".ucwords($row['denumire_tip_oferta']).", hoteluri, ";	
	}
	@mysql_free_result($que_tip);
	
	$return[2].=" destinatii, ".$cheie[sejuri_cautate].$this->sir_adiacent.$tr;
	$return[0]="Oferte, sejururi".$tr.", cazari";
	return $return;
	} else {
	$trans=''; $tr='';
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=$tr.", transport propriu"; else $tr=$tr.", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	$tip="select seo_short,seo_description from tip_oferta where denumire_tip_oferta = '".desfa_link($_REQUEST['tip'])."' ";
	$que=mysql_query($tip) or die(mysql_error());
	$row=mysql_fetch_array($que);
	$seo_short=$row['seo_short'];
	$seo_description=$row['seo_description'];
	if($seo_short) $den_tip=ucwords(strtolower($seo_short)); else $den_tip=ucwords(strtolower(desfa_link($_REQUEST['tip'])));
	$return[1]="Oferte ".$den_tip.$tr.", cazare ".$den_tip;
	$return[2]=ucwords(desfa_link($_REQUEST['tip'])).",  ".$cheie[$_REQUEST['tip']]." ".$this->sir_adiacent.$tr;
	$return[0]="Oferte ".desfa_link($_REQUEST['tip']).", sejururi".$tr.", cazari ".ucwords(desfa_link($_REQUEST['tip']))." hoteluri, circuite, early booking, ".$seo_description;
	}
	return $return;
}

//early booking
function do_nice_early_new(){
include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
if(!$_REQUEST['tip']) {
$trans=''; $tr='';
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=$tr.", transport propriu"; else $tr=$tr.", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
$return[1]="Oferte Early Booking ".$tr;
 $tip="SELECT
	tip_oferta.denumire_tip_oferta,
	tip_oferta.seo_short,
	tip_oferta.seo_nice
	FROM
	oferte
	Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	Inner Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	Right Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	Right Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Right Join zone ON localitati.id_zona = zone.id_zona
	Right Join tari ON zone.id_tara = tari.id_tara
	WHERE
	oferte.valabila =  'Da' AND
	tip_oferta.activ =  'da'
	GROUP BY
	tip_oferta.denumire_tip_oferta ";
	$que_tip=mysql_query($tip) or die(mysql_error());
	while($row=@mysql_fetch_array($que_tip)){
	$return[2].=ucwords($row['denumire_tip_oferta']).", ";	
	}
	@mysql_free_result($que_tip);
	
	$return[2].=" destinatii, ".$cheie[sejuri_cautate].$this->sir_adiacent.$tr;
	$return[0]="Oferte, Early Booking ".$tr.", cazari";
	return $return;
	} else {
	$trans=''; $tr='';
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=$tr.", transport propriu"; else $tr=$tr.", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	$return[1]="Oferte Early Booking ".ucwords(desfa_link($_REQUEST['tip'])).$tr.", cazare ".ucwords(desfa_link($_REQUEST['tip']));
	$return[2]=$inc.",  ".$cheie[$_REQUEST['tip']]." ".$this->sir_adiacent.$tr;
	$return[0]="Oferte, Early Booking ".$tr.", cazari ".ucwords(desfa_link($_REQUEST['tip']));
	}
	return $return;
}

//last minute
function do_nice_sejur_last_minute() {
	if(!$_REQUEST['tari']) {
	$return[0]="Last minute, oferte de la agentii de turism pentru ocaziituristice.ro";
	$return[1]="Last minute de la agentii de turism pentru portalul de turism ocaziituristice.ro. Oferte last minute adevarate.";
	$return[2]="Last minute, oferte last minute, agentii de turism, oferte speciale, ocaziituristice.ro, reduceri de sezon, portal de turism";
	} else {
	$return[0]="Last minute ".ucwords(desfa_link($_REQUEST['tari'])).", oferte last minute ".ucwords(desfa_link($_REQUEST['tari']));
	$return[1]="Oferte last minute ".ucwords(desfa_link($_REQUEST['tari']))." oferite de agentii de turism care te intampina cu oferte speciale in ".ucwords(desfa_link($_REQUEST['tari']))." doar rezervand last minute ".ucwords(desfa_link($_REQUEST['tari']))."";
	$return[2]="Last minute ".ucwords(desfa_link($_REQUEST['tari'])).", oferte last minute ".ucwords(desfa_link($_REQUEST['tari'])).", cazare ".ucwords(desfa_link($_REQUEST['tari'])).", oferte ".ucwords(desfa_link($_REQUEST['tari'])).", sejur last minute ".ucwords(desfa_link($_REQUEST['tari']))."";
	}
	return $return;
}

function do_nice_sejur2_new(){
$tara_seo=desfa_link($_REQUEST['tari']);
$tara_seo=desfa_link($_REQUEST['tari']);
$tip_seo=desfa_link($_REQUEST['tip']);
$return[1]="Oferte sejur ".ucwords($tara_seo).", cazare ".ucwords($tara_seo);
$return[0]="Sejururi ".ucwords($tara_seo).", cazare ".ucwords($tara_seo);
$return[2]="Destinatii ".ucwords($tara_seo).", sejururi ".ucwords($tara_seo).", cazare ".ucwords($tara_seo);
	return $return;	
}

function do_nice_sejur2_tematici(){
$tara_seo=desfa_link($_REQUEST['tari']);
$tip_seo=desfa_link($_REQUEST['tip']);
$return[1]=ucwords($tip_seo)." ".ucwords($tara_seo).", sejururi, cazare ".ucwords($tara_seo);
$return[0]=ucwords($tip_seo).", oferte sejurur ".ucwords($tara_seo).", cazare ".ucwords($tara_seo);
$return[2]="Destinatiile ".ucwords($tip_seo)." ".ucwords($tara_seo).", sejururile ".ucwords($tip_seo).", sejururi ".ucwords($tara_seo).", cazare ".ucwords($tip_seo).", cazare ".ucwords($tara_seo);

	return $return;	
}

//early booking
function do_nice_early2_new(){
include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
   if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=$tr.", transport propriu"; else $tr=$tr.", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	$inc="Early Booking";
	$return[1]="Sejururi ".$inc; if($_REQUEST['stele']) $return[1]=$return[1]." la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[1]=$return[1]." ".ucwords(trim(desfa_link($_REQUEST['tari']))).$tr;
	$return[0]="Sejururi ".$inc; if($_REQUEST['stele']) $return[0]=$return[0].", hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[0]=$return[0].", ".ucwords(desfa_link($_REQUEST['tari'])).$tr." Early Booking";
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]=$inc." ".ucwords(desfa_link($_REQUEST['tari'])).$tr; if($_REQUEST['stele']) $return[2]=$return[2].", de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[2]=$return[2].", Early Booking ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	return $return;	
}

function do_nice_sejur3_new(){
	include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
	 if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=", transport propriu"; else $tr=", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	if($_REQUEST['tip']) $inc="Oferte "; else $inc="Oferte sejur ";
	$return[1]=$inc.ucwords(desfa_link($_REQUEST['tip'])); if($_REQUEST['stele']) $return[1]=$return[1]." la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[1]=$return[1]." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zone'])))." ".$tr;
	$return[0]=$inc.ucwords(desfa_link($_REQUEST['tip'])); if($_REQUEST['stele']) $return[0]=$return[0].", hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[0]=$return[0].", ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).$tr." oferte, sejururi, cazari";
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]="Destinatiile ".ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).$tr; if($_REQUEST['stele']) $return[2]=$return[2].", de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[2]=$return[2].", sejururile ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	
	return $return;	
}

//early booking
function do_nice_early3_new(){
	include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
	if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=", transport propriu"; else $tr=", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	 $inc="Early Booking ";
	$return[1]="Sejururi ".$inc; if($_REQUEST['stele']) $return[1]=$return[1]." la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[1]=$return[1]." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zona'])))." ".$tr;
	$return[0]="Sejururi ".$inc; if($_REQUEST['stele']) $return[0]=$return[0].", hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[0]=$return[0].", ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zona']))).$tr." Early Booking";
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]=$inc." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zona']))).$tr; if($_REQUEST['stele']) $return[2]=$return[2].", de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; $return[2]=$return[2].", Early Booking ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	
	return $return;	
}

function do_nice_sejur4_new(){
	include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=$tr.", transport propriu"; else $tr=$tr.", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	if($_REQUEST['tip']) $inc="Oferte "; else $inc="Oferte sejur ";
	$return[1]=$inc.ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['zone']))." localitatea ".ucwords(desfa_link($_REQUEST['oras']))." - ".ucwords(desfa_link($_REQUEST['tari'])).", cazare ".ucwords(strtolower(desfa_link($_REQUEST['oras'])))." ".$tr;
	$return[0]="Cazare ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).", oferte ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).", cazare ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).", hoteluri ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).", hoteluri ".ucwords(strtolower(desfa_link($_REQUEST['zone'])))." , oferte ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).", ".$tr.", ".$inc.ucwords(desfa_link($_REQUEST['tip'])).", cazari, ". $sir_meta_keywords;
	
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]="Destinatiile ".ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari']))." ".ucwords(desfa_link($_REQUEST['zone'])).", oferte ".ucwords(strtolower(desfa_link($_REQUEST['tip']))).", ".ucwords(strtolower(desfa_link($_REQUEST['oras']))) .", cazare ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).$tr.", sejururile ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	
	return $return;	
}

//early booking
function do_nice_early4_new(){
	include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['transport']) { if($_REQUEST['transport']=='individual') $tr=$tr.", transport propriu"; else $tr=$tr.", cu ".ucwords(strtolower($_REQUEST['transport'])); }
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	$inc="Early Booking ";
	$return[1]="Sejururi ".$inc.ucwords(desfa_link($_REQUEST['zone']))." ".ucwords(desfa_link($_REQUEST['oras']))." ".ucwords(desfa_link($_REQUEST['tari'])).", cazare ".ucwords(strtolower(desfa_link($_REQUEST['oras'])))." ".$tr;
	$return[0]="cazare ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).", oferte ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).", cazare ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).", hoteluri ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).", hoteluri ".ucwords(strtolower(desfa_link($_REQUEST['zone'])))." , oferte ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).", ".$tr."  sejururi, cazari, ". $sir_meta_keywords;
	
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]=$inc." ".ucwords(desfa_link($_REQUEST['tari']))." ".ucwords(desfa_link($_REQUEST['zone'])).", oferte ".ucwords(strtolower(desfa_link($_REQUEST['tip']))).", ".ucwords(strtolower(desfa_link($_REQUEST['oras']))) .", cazare ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).$tr.", sejururile ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	
	return $return;	
}

function do_nice_cazare_new(){
include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
if(!$_REQUEST['tip']) {
$trans=''; $tr='';
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
$return[1]="Cazare ".$tr." de la agentii de turism";
 $tip="SELECT
	tip_oferta.denumire_tip_oferta,
	tip_oferta.seo_short,
	tip_oferta.seo_nice
	FROM
	oferte
	Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	Inner Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	Right Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	Right Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Right Join zone ON localitati.id_zona = zone.id_zona
	Right Join tari ON zone.id_tara = tari.id_tara
	WHERE
	oferte.valabila =  'Da' AND
	tip_oferta.activ =  'da' and
	oferte.id_transport = '1' and
	(oferte.nr_zile <= '1' or oferte.nr_zile is null)
	GROUP BY
	tip_oferta.denumire_tip_oferta ";
	$que_tip=mysql_query($tip) or die(mysql_error());
	while($row=@mysql_fetch_array($que_tip)){
	
	$return[2].="cazare ".ucwords($row['denumire_tip_oferta']).", hoteluri, ";	
	}
	@mysql_free_result($que_tip);
	
	$return[2].="Destinatii, ".$cheie[sejuri_cautate].$this->sir_adiacent.$tr;
	$return[0]="Cazarii ".$tr." de la peste 300 agentii de turism";
	return $return;
	} else {
	$trans=''; $tr='';
	if($_REQUEST['stele']) $trans=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['masa']) { $trans=$trans.", masa ".ucwords($_REQUEST['masa']); $tr=", masa ".ucwords($_REQUEST['masa']); }
	$tip="select seo_short,seo_description from tip_oferta where denumire_tip_oferta = '".desfa_link($_REQUEST['tip'])."' ";
	$que=mysql_query($tip) or die(mysql_error());
	$row=mysql_fetch_array($que);
	$seo_short=$row['seo_short'];
	$seo_description=$row['seo_description'];
	if($seo_short) $den_tip=ucwords(strtolower($seo_short)); else $den_tip=ucwords(strtolower(desfa_link($_REQUEST['tip'])));
	$return[1]="Cazare".$den_tip.$trans." de la agentii de turism ";
	$return[2]=ucwords(desfa_link($_REQUEST['tip'])).",  ".$cheie[$_REQUEST['tip']]." ".$this->sir_adiacent.$tr;
	$return[0]="Cazare ".desfa_link($_REQUEST['tip']).", sejururi".$trans." hoteluri, circuite, early booking, ".$seo_description." de la 500 de agentii de turism";
	}
	return $return;
}

function do_nice_cazare2_new(){
include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
	 if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	if($_REQUEST['tip']) $inc="Cazare "; else $inc="Cazare ";
	$return[1]=$inc.ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari']))." ".ucwords(desfa_link($_REQUEST['zone'])).$tr." de la agentii de turism";
	$return[0]=$inc.ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).$tr." cazari de la 500 agentii de turism";
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]="Oferte ".ucwords(desfa_link($_REQUEST['tip'])).", cazare ".ucwords(desfa_link($_REQUEST['tari'])).$tr.", ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	return $return;	
}

function do_nice_cazare3_new(){
	include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	if($_REQUEST['tip']) $inc="Cazare "; else $inc="Cazare ";
	$return[1]=$inc.ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zone'])))." ".$tr." de la agentii de turism";
	$return[0]=$inc.ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).$tr." cazari de la 500 agentii de turism";
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]="Cazarii ".ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(strtolower(desfa_link($_REQUEST['zone']))).$tr.", cazare ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	
	return $return;	
}

function do_nice_cazare4_new(){
	include($_SERVER['DOCUMENT_ROOT'].'/config/cuvinte_cheie_seo.php');
	if($_REQUEST['stele']) $tr=" de la hoteluri de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume'];
	if($_REQUEST['masa']) $tr=$tr.", masa ".ucwords(desfa_link($_REQUEST['masa'])); 
	if($_REQUEST['tip']) $inc="Cazare "; else $inc="Cazare ";
	$return[1]=$inc.ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).", localitatea ".ucwords(strtolower(desfa_link($_REQUEST['oras'])))." ".$tr." de la agentii de turism";
	$return[0]=$inc.ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).", localitatea ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).", zona ".ucwords(desfa_link($_REQUEST['zone'])).$tr." cazari de la 500 agentii de turism";
	if(strlen($cheie[$_REQUEST['tip']." ".$_REQUEST['tari']])>'0') $cuv=$cheie[$_REQUEST['tip']." ".$_REQUEST['tari']]; else $cuv=$cheie[$_REQUEST['tip']];
	$return[2]="Cazarii ".ucwords(desfa_link($_REQUEST['tip']))." ".ucwords(desfa_link($_REQUEST['tari'])).", zona ".ucwords(desfa_link($_REQUEST['zone'])).", localitatea ".ucwords(strtolower(desfa_link($_REQUEST['oras']))).$tr.", cazare ".ucwords(desfa_link($_REQUEST['tip'])).", ".$cuv." ".$this->sir_adiacent;
	
	return $return;	
}

function do_nice_circuite1_new(){
	$sel_of="
	SELECT
	continente.nume_continent
	FROM
	continente
	Inner Join circuite ON continente.id_continent = circuite.id_continent
	WHERE
	circuite.valabila = 'Da'
	GROUP BY continente.nume_continent ";
	$return[1] = "Circuite - ";
	$retunr[2]= "Circuite - ";
	$rez_of=mysql_query($sel_of);
	while($row_rez=mysql_fetch_array($rez_of)){
		$return[0]=$return[0]."Circuit ".$row_rez['nume_continent'].", ";
		$return[1]=$return[1].$row_rez['nume_continent'].", ";
		$return[2].=$row_rez['nume_continent'].", ";
	}
	@mysql_free_result($rez_of);
	$return[0] = rtrim($return[0],', ');
	$return[1] = rtrim($return[1],', ');
	return $return;	
}

function do_nice_circuite2_new(){		
$continente_sel =desfa_link($_REQUEST['continente']);
$cont="'".$continente_sel."'";
$id_continent=get_id_continent($continente_sel);
$sel_cont="SELECT
tari.denumire
FROM
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
WHERE
oferte.valabila =  'da' AND
tari.id_continent =  ".$id_continent." and
hoteluri.tip_unitate = 'Circuit'
GROUP BY tari.denumire
ORDER BY tari.denumire ASC ";
$que_cont=mysql_query($sel_cont) or die(mysql_error());
$return[0]="Circuit ".ucwords($continente_sel).", ";
$return[1]="Circuit ".ucwords($continente_sel).", ";
$return[2]="Circuit ".ucwords($continente_sel).", ";
$i=0; while($row_tari=mysql_fetch_array($que_cont)) {
			   if($i<3) {
				  $return[0].=" Circuite ".$row_tari['denumire'].", ";
				  $return[1].=" Circuite ".$row_tari['denumire'].", ";
				  $i++; }
			   $return[2].=" Circuite ".$row_tari['denumire'].", ";
	} @mysql_free_result($que_cont);
return $return;
}

function do_nice_circuite3_new()
{
  $cont=desfa_link($_REQUEST['continente']);
  $tara=desfa_link($_REQUEST['tara']);
  
  $return[0].=ucwords("Circuit ".$cont.", Circuite ".$tara);
  $return[1].=ucwords("Circuit ".$cont.", Circuite ".$tara);
  $return[2]=$return[2]."Circuit  ".$tara." continentul ".$cont;
  
  return $return;
}

function creaza_link($word)
{
	return "/tag/".str_replace(' ','+',$word).".html";
}

function afiseaza($class_personal="",$words)
{	
	$words=str_replace('sejur ','',$words);
	$words=str_replace('oferte ','',$words);
	$words=str_replace('circuit ','',$words);
	$words=str_replace('cazare ','',$words);
	$words=explode(", ", $words);
	foreach($words as $key => $val)
		$words[$key] = trim($val);
	$words = array_unique($words);
	$words = array_values($words);		
	for($i=0; $i<$this->nr_taguri; $i++)
	{	
		if($words[$i]){
			echo " <a href=\"".$this->creaza_link($words[$i])."\"";
			if($class_personal!="") echo " class=\"".$class_personal."\"";
			echo ">";
			echo ucwords($words[$i]);
			echo "</a> ";
			if($i<$this->nr_taguri-1 && $words[$i+1]) echo ", ";
		}
	}
}


}

?>