<?php function get_tari_cazare($id_tipuri, $id_transport, $id_zona, $id_localitate, $early) {
$selT="select
tari.id_tara,
tari.denumire
from
oferte ";
if($id_tipuri) $selT=$selT." inner join (select oferta_sejur_tip.id_oferta, oferta_sejur_tip.id_tip_oferta from oferta_sejur_tip left join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta where tip_oferta.id_tip_oferta in (".$id_tipuri.") Group by oferta_sejur_tip.id_oferta Order by tip_oferta.denumire_tip_oferta) as tipuri_oferta on tipuri_oferta.id_oferta = oferte.id_oferta ";
$selT=$selT." inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate 
inner join zone on localitati.id_zona = zone.id_zona 
inner join tari on zone.id_tara = tari.id_tara ";
if($early) $selT=$selT." inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur') ";
if($id_transport) $selT=$selT." inner join transport on (oferte.id_transport = transport.id_trans and oferte.id_transport = '".$id_transport."') ";
$selT=$selT." where
oferte.valabila = 'da' and oferte.cazare = 'da' ";
if($id_zona) $selT=$selT." and zone.id_zona = '".$id_zona."' ";
if($id_localitate) $selT=$selT." and localitati.id_localitate = '".$id_localitate."' ";
$selT=$selT." Group by tari.id_tara
Order by tari.denumire ";
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_assoc($queT)) {
$tip[$rowT['id_tara']]=$rowT['denumire'];
} @mysql_free_result($queT);
return $tip;
}

function get_zone_cazare($id_tipuri, $id_transport, $id_localitate, $id_tara, $early) {
$selT="select
zone.id_zona,
zone.denumire
from
oferte ";
if($id_tipuri) $selT=$selT." inner join (select oferta_sejur_tip.id_oferta, oferta_sejur_tip.id_tip_oferta from oferta_sejur_tip left join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta where tip_oferta.id_tip_oferta in (".$id_tipuri.") Group by oferta_sejur_tip.id_oferta Order by tip_oferta.denumire_tip_oferta) as tipuri_oferta on tipuri_oferta.id_oferta = oferte.id_oferta ";
$selT=$selT." inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate 
inner join zone on localitati.id_zona = zone.id_zona 
inner join tari on zone.id_tara = tari.id_tara ";
if($early) $selT=$selT." inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur') ";
if($id_transport) $selT=$selT." inner join transport on (oferte.id_transport = transport.id_trans and oferte.id_transport = '".$id_transport."') ";
$selT=$selT." where
oferte.valabila = 'da' and oferte.cazare = 'da' ";
if($id_localitate) $selT=$selT." and localitati.id_localtiate = '".$id_localitate."' ";
if($id_tara) $selT=$selT." and tari.id_tara = '".$id_tara."' ";
$selT=$selT." Group by zone.id_zona
Order by zone.denumire ";
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_assoc($queT)) {
$tip[$rowT['id_zona']]=$rowT['denumire'];
} @mysql_free_result($queT);
return $tip;
}

function get_localitate_cazare($id_tipuri, $id_transport, $id_zona, $id_tara, $early) {
$selT="select
localitati.id_localitate,
localitati.denumire,
localitati.poza1,
localitati.poza2,
zone.denumire as denumire_zona
from
oferte ";
if($id_tipuri) $selT=$selT." inner join (select oferta_sejur_tip.id_oferta, oferta_sejur_tip.id_tip_oferta from oferta_sejur_tip left join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta where tip_oferta.id_tip_oferta in (".$id_tipuri.") Group by oferta_sejur_tip.id_oferta Order by tip_oferta.denumire_tip_oferta) as tipuri_oferta on tipuri_oferta.id_oferta = oferte.id_oferta ";
$selT=$selT." inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate 
inner join zone on localitati.id_zona = zone.id_zona 
inner join tari on zone.id_tara = tari.id_tara ";
if($early) $selT=$selT." inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur') ";
if($id_transport) $selT=$selT." inner join transport on (oferte.id_transport = transport.id_trans and oferte.id_transport = '".$id_transport."') ";
$selT=$selT." where
oferte.valabila = 'da' and oferte.cazare = 'da' ";
if($id_zona) $selT=$selT." and zone.id_zona = '".$id_zona."' ";
if($id_tara) $selT=$selT." and tari.id_tara = '".$id_tara."' ";
$selT=$selT." Group by localitati.id_localitate
Order by localitati.denumire ";
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_assoc($queT)) {
$tip[$rowT['id_localitate']]=array('denumire'=>$rowT['denumire'],'poza1'=>$rowT['poza1'],'poza2'=>$rowT['poza2'],'denumire_zona'=>$rowT['denumire_zona']);
} @mysql_free_result($queT);
return $tip;
}

function get_hoteluri_cazare($id_tipuri, $id_transport, $id_localitate, $id_zona, $id_tara, $early, $limit) {
$selT="select
hoteluri.id_hotel,
hoteluri.nume,
hoteluri.stele,
hoteluri.poza1
from
oferte ";
if($id_tipuri) $selT=$selT." inner join (select oferta_sejur_tip.id_oferta, oferta_sejur_tip.id_tip_oferta from oferta_sejur_tip left join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta where tip_oferta.id_tip_oferta in (".$id_tipuri.") Group by oferta_sejur_tip.id_oferta Order by tip_oferta.denumire_tip_oferta) as tipuri_oferta on tipuri_oferta.id_oferta = oferte.id_oferta ";
$selT=$selT." inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate 
inner join zone on localitati.id_zona = zone.id_zona 
inner join tari on zone.id_tara = tari.id_tara ";
if($early) $selT=$selT." inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur') ";
if($id_transport) $selT=$selT." inner join transport on (oferte.id_transport = transport.id_trans and oferte.id_transport = '".$id_transport."') ";
$selT=$selT." where
oferte.valabila = 'da' ";
if($id_localitate) $selT=$selT." and localitati.id_localitate = '".$id_localitate."' ";
if($id_zona) $selT=$selT." and zone.id_zona = '".$id_zona."' ";
if($id_tara) $selT=$selT." and tari.id_tara = '".$id_tara."' ";
$selT=$selT." Group by hoteluri.id_hotel
Order by hoteluri.nume ";
if($limit) $selT=$selT." Limit ".$limit;
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_assoc($queT)) {
$tip[$rowT['id_hotel']]=array('nume'=>$rowT['nume'], 'stele'=>$rowT['stele'], 'poza'=>$rowT['poza1']);
} @mysql_free_result($queT);
return $tip;
}

function get_tiputi_cazare($id_transport, $id_localitate, $id_zona, $id_tara, $early, $id_hotel) {
$selT="select
tip_oferta.id_tip_oferta,
tip_oferta.denumire_tip_oferta
from
oferte
inner join oferta_sejur_tip on oferte.id_oferta = oferta_sejur_tip.id_oferta
inner join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta 
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate 
inner join zone on localitati.id_zona = zone.id_zona 
inner join tari on zone.id_tara = tari.id_tara ";
if($early) $selT=$selT." inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur') ";
if($id_transport) $selT=$selT." inner join transport on (oferte.id_transport = transport.id_trans and oferte.id_transport = '".$id_transport."') ";
$selT=$selT." where
oferte.valabila = 'da'
and tip_oferta.activ = 'da'
and tip_oferta.apare_site = 'da'
and tip_oferta.tip is not null ";
if($id_hotel) $selT=$selT." and hoteluri.id_hotel = '".$id_hotel."' ";
if($id_localitate) $selT=$selT." and localitati.id_localitate = '".$id_localitate."' ";
if($id_zona) $selT=$selT." and zone.id_zona = '".$id_zona."' ";
if($id_tara) $selT=$selT." and tari.id_tara = '".$id_tara."' ";
$selT=$selT." Group by tip_oferta.id_tip_oferta
Order by tip_oferta.denumire_tip_oferta ";
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_assoc($queT)) {
$tip[$rowT['id_tip_oferta']]=array('denumire'=>$rowT['denumire_tip_oferta']);
} @mysql_free_result($queT);
return $tip;
} ?>