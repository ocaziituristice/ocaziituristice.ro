<?php class AFISARE_LOCALITATE {

function __construct($id_tara, $id_localtiate) {
$this->id_tara=$id_tara;
$this->id_localitate=$id_localtiate;
$this->tipuri='';
}
function detalii_localitate() {
$selT="SELECT
zone.id_zona,
tari.id_tara,
zone.denumire as denumire_zona,
tari.denumire as denumire_tara,
localitati.denumire,
localitati.descriere,
localitati.poza1,
localitati.poza2,
localitati.poza3,
localitati.descriere_scurta,
localitati.descriere_seo,
localitati.titlu_seo,
localitati.cuvinte_cheie_seo,
localitati.latitudine,
localitati.longitudine
FROM
localitati
left join zone on localitati.id_zona = zone.id_zona
left join tari on zone.id_tara = tari.id_tara
where
localitati.id_localitate = '".$this->id_localitate."' ";
$queT=mysql_query($selT) or die(mysql_error());
$this->localitate=mysql_fetch_array($queT);
@mysql_free_result($queT);
$selC="select * from localitati_caracteristici where id_localitate = '".$this->id_localitate."' ";
$queC=mysql_query($selC) or die(mysql_error());
while($rowC=mysql_fetch_array($queC)) {
$this->localitate['caracteristica'][$rowC['id_caracteristica']]=$rowC['value'];
} @mysql_free_result($queC);
return $this->localitate;
}
function detalii_tara() {
$selT="select denumire, descriere, descriere_scurta, descriere_seo, titlu_seo, cuvinte_cheie_seo as cuvinte_seo, id_continent as continent, poza1, poza2, poza3, harta, steag, latitudine, longitudine from tari where id_tara = '".$this->id_tara."' ";
$queT=mysql_query($selT) or die(mysql_error());
$this->tara=mysql_fetch_array($queT);
@mysql_free_result($queT);
$selC="select * from tari_caracteristici where id_tara = '".$this->id_tara."' ";
$queC=mysql_query($selC) or die(mysql_error());
while($rowC=mysql_fetch_array($queC)) {
$this->tara['caracteristica'][$rowC['id_caracteristica']]=$rowC['value'];
} @mysql_free_result($queC);	
return $this->tara;
}

function get_hoteluri($ord_nume, $ord_stele, $id_transport, $stele, $masa) {
$hot=array();
$selT="select
hoteluri.id_hotel,
hoteluri.nume,
hoteluri.stele,
transport.denumire as denumire_transport,
transport.id_trans,
oferte.masa
from
oferte
inner join (select oferta_sejur_tip.id_oferta, oferta_sejur_tip.id_tip_oferta from oferta_sejur_tip left join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta where tip_oferta.id_tip_oferta in (".$this->tipuri.") Group by oferta_sejur_tip.id_oferta Order by tip_oferta.denumire_tip_oferta) as tipuri_oferta on tipuri_oferta.id_oferta = oferte.id_oferta
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate
inner join zone on localitati.id_zona = zone.id_zona 
inner join tari on zone.id_tara = tari.id_tara
inner join transport on oferte.id_transport = transport.id_trans
where
oferte.valabila = 'da' and hoteluri.tip_unitate <> 'Circuit' and oferte.last_minute = 'nu' ";
if($this->id_tara) $selT=$selT." and tari.id_tara = '".$this->id_tara."' ";
if($this->id_localitate) $selT=$selT." and localitati.id_localitate = '".$this->id_localitate."' ";
if($id_transport) $selT=$selT." and transport.id_trans = '".$id_transport."' ";
if($stele) $selT=$selT." and hoteluri.stele = '".$stele."' ";
if($masa) $selT=$selT." and trim(lower(oferte.masa)) = '".$masa."' ";
$selT=$selT." Group by oferte.id_transport, oferte.id_hotel ";
if($ord_stele) $selT=$selT." Order by hoteluri.stele ".$ord_stele.", hoteluri.nume ASC ";
if($ord_nume) $selT=$selT." Order by hoteluri.nume ".$ord_nume." ";
$queT=mysql_query($selT) or die(mysql_error());
$hot=array();
while($rowT=mysql_fetch_array($queT)) {
$hot[$rowT['id_hotel']]=array('nume'=>$rowT['nume'], 'stele'=>$rowT['stele']);
$hot2[$rowT['id_hotel']][$rowT['id_trans']]=$rowT['denumire_transport'];
$this->trans[$rowT['denumire_transport']]=$this->trans[$rowT['denumire_transport']]+1;
$this->stele[$rowT['stele']]=$this->stele[$rowT['stele']]+1;
$this->masa[$rowT['masa']]=$this->masa[$rowT['masa']]+1;
}
if(sizeof($hot)>0) {
foreach($hot as $key=>$value) {
$hot[$key]['transport']=$hot2[$key];
} }
@mysql_free_result($queT);
return $hot; }

function get_tip_oferta_tari_localitate() {
$selT="select 
tip_oferta.id_tip_oferta, 
tip_oferta.denumire_tip_oferta
from 
tip_oferta 
inner join tari_tip_sejur on (tari_tip_sejur.id_tara = '".$this->id_tara."' and tip_oferta.id_tip_oferta = tari_tip_sejur.id_tip_oferta)
inner join oferta_sejur_tip on oferta_sejur_tip.id_tip_oferta = tari_tip_sejur.id_tip_oferta
inner join oferte on oferta_sejur_tip.id_oferta = oferte.id_oferta
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
where 
tip_oferta.activ = 'da'
and tip_oferta.apare_site = 'da' 
and tip_oferta.tip is not null
and oferte.valabila = 'da'
and hoteluri.tip_unitate <> 'Circuit'
and oferte.last_minute = 'nu' ";
if($this->id_localitate) $selT=$selT." and hoteluri.locatie_id = '".$this->id_localitate."' ";
$selT=$selT." Group by tip_oferta.id_tip_oferta 
Order by tip_oferta.denumire_tip_oferta ASC ";
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_assoc($queT)) {
$tip[$rowT['id_tip_oferta']]=$rowT['denumire_tip_oferta'];
$this->tipuri=$this->tipuri."'".$rowT['id_tip_oferta']."',";
} @mysql_free_result($queT);
$this->tipuri=substr($this->tipuri,0,-1);
return $tip;
}
function af_filtru($link_p, $ord_nume, $ord_stele, $transport, $stele, $masa) { ?>
<div class="Hline"></div>
 <p>Transport</p>
 <?php if(sizeof($this->trans)>0) { krsort($this->trans); ?>
  <ul>
    <li><input type="checkbox" name="transport[]" id="optiune0" <?php if($transport=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu(<?php echo "'".$link_p."'"; ?>, this.value, <?php echo "'".$stele."'"; ?>, <?php echo "'".$masa."'"; ?>);" /> <label for="optiune0">Toate (<?php echo array_sum($this->trans); ?>)</label><br class="clear" /></li>
    <?php $i=0; foreach($this->trans as $key_t=>$value_t) { $i++; ?>
    <li><input type="checkbox" name="transport[]" id="optiune<?php echo $i; ?>" <?php if(fa_link($key_t)==$transport) { ?> checked="checked" <?php } ?> value="<?php echo fa_link($key_t); ?>" onclick="filtrare_restu(<?php echo "'".$link_p."'"; ?>, this.value, <?php echo "'".$stele."'"; ?>, <?php echo "'".$masa."'"; ?>);" /> <label for="optiune<?php echo $i; ?>"><?php echo $key_t; ?> (<?php echo $value_t; ?>)</label><br class="clear" /></li>
   <?php } ?>
 </ul>
 <?php } ?>
 <div class="Hline"></div>
 <p>Categorie Hotel</p>
 <?php if(sizeof($this->stele)>0) { krsort($this->stele); ?>
  <ul>
    <li><input type="checkbox" name="stele[]" id="optiunes0" <?php if($stele=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu(<?php echo "'".$link_p."'"; ?>, <?php echo "'".$transport."'"; ?>, this.value, <?php echo "'".$masa."'"; ?>);" /> <label for="optiunes0">Toate (<?php echo array_sum($this->stele); ?>)</label><br class="clear" /></li>
    <?php $i=0; foreach($this->stele as $key_s=>$value_s) { $i++; ?>
    <li><input type="checkbox" name="stele[]" id="optiunes<?php echo $i; ?>" <?php if($key_s==$stele) { ?> checked="checked" <?php } ?> value="<?php echo $key_s; ?>" onclick="filtrare_restu(<?php echo "'".$link_p."'"; ?>, <?php echo "'".$transport."'"; ?>, this.value, <?php echo "'".$masa."'"; ?>);" /> <label for="optiunes<?php echo $i; ?>"><?php echo $key_s; if($key_s>1) echo ' stele'; else echo ' stea'; ?> (<?php echo $value_s; ?>)</label><br class="clear" /></li>
   <?php } ?>
 </ul>
 <?php }?>
 <div class="Hline"></div>
 <p>Tip masa</p>
 <?php if(sizeof($this->masa)>0) { krsort($this->masa);  ?>
  <ul>
    <li><input type="checkbox" name="masa[]" id="optiunem0" <?php if($masa=='toate') { ?> checked="checked" <?php } ?> onclick="filtrare_restu(<?php echo "'".$link_p."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, this.value);" /> <label for="optiunem0">Toate (<?php echo array_sum($this->masa); ?>)</label><br class="clear" /></li>
    <?php $i=0; foreach($this->masa as $key_m=>$value_m) { $i++; ?>
    <li><input type="checkbox" name="masa[]" id="optiunem<?php echo $i; ?>" <?php if(fa_link($key_m)==$masa) { ?> checked="checked" <?php } ?> value="<?php echo fa_link($key_m); ?>" onclick="filtrare_restu(<?php echo "'".$link_p."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, this.value);" /> <label for="optiunem<?php echo $i; ?>"><?php echo $key_m; ?> (<?php echo $value_m; ?>)</label><br class="clear" /></li>
   <?php } ?>
 </ul>
 <?php }?>
 <div class="Hline"></div>

<?php }
}