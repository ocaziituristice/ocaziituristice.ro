<?php class CIRCUIT{
	var $id_circuit;
	var $denumire_continent;
	
	var $poza1;
	var $nume_poza1;
	
	var $denumire;
	var $denumire_lenght;
	var $oferta_speciala;
	
	var $nr_zile;
	
	var $obiective_turistice;
	var $obiective_turistice_lenght;
	
	var $localitati_traversate;
	var $localitati_traversate_lenght;
	
	var $tari_traversate;

	var $keywords_1_words;
	var $keywords_2_words;
	
	
	function __construct($id_circuit)
	{
		$this->id_circuit=$id_circuit;
		$this->denumire_continent=0;
		$this->denumire=0;
		$this->nr_zile=0;
		$this->obiective_turistice=0;
		$this->denumire_lenght=50;
	}
	
	function set_id_circuit($value)
	{
		$this->id_circuit=$value;
	}
	function set_denumire($value)
	{
		$this->denumire=$value;
	}
	function set_sigla($value)
	{
	    $this->sigla=$value;
    }
	function load_localitati_traversate()
	{
		$sel="
		SELECT
		GROUP_CONCAT(localitati.denumire SEPARATOR '-') AS localitati
		FROM
		loc_circuit
		Inner Join localitati ON loc_circuit.id_localitate = localitati.id_localitate
		WHERE loc_circuit.id_circuit='".$this->id_circuit."'
		GROUP BY loc_circuit.id_circuit		
		";
		$rez=mysql_query($sel);
		$row=mysql_fetch_array($rez);
		@mysql_free_result($rez);
		$this->localitati_traversate=$row["localitati"];
	}
	function set_localitati_traversate_lenght($value)
	{
		$this->localitati_traversate_lenght=$value;
	}
	function set_oferta_speciala($value)
	{
	    $this->oferta_speciala=$value;
	}
	function set_denumire_continent($value)
	{
		$this->denumire_continent=$value;
	}
	function set_denumire_lenght($value)
	{
		$this->denumire_lenght=$value;
	}
	function set_nr_zile($value)
	{
		$this->nr_zile=$value;
	}
	function set_obiective_turistice($value)
	{
		if($value)
		$this->obiective_turistice='<strong>Obiective:</strong> '.strip_tags($value);
	}
	function set_obiective_turistice_lenght($value)
	{
		$this->obiective_turistice_lenght=$value;
	}
	function set_denumire_circuit_lenght($value)
	{
		$this->set_denumire_circuit_lenght=$value;
	}
	function set_nume_poza1($value)
	{
		$this->nume_poza1=$value;
	}
	function set_poza1($value)
	{
		$this->poza1=$value;
	}
	function set_agentie_nume($value)
	{
		$this->agentie_nume=$value;
	}
	function set_pret($value)
	{
	  $this->pret=$value;	
	}
	function set_moneda($value)
	{
	  $this->moneda=$value;	
	}
	function resize($value, $limit)
	{
		if($limit==0) return $value;
		if(strlen($value)<=$limit) return $value;
		else
		{
			$value=substr($value,0,$limit)."...";
			return $value;
		}
	}
	function creaza_link()
	{
		$link=$GLOBALS['sitepath_class']."circuit/".fa_link($this->denumire_continent)."/".fa_link($this->denumire);
		if(strlen($link)>'100') $link=substr($link,0,100);
		$link=$link."-".$this->id_circuit.".html";
		return $link;
	}
	function afiseaza($template)
	{
		$link=$this->creaza_link();
		if(!$this->pret) {
		$pret=pret_minim_circuit($this->id_circuit);	
		if($pret[1]=='EURO') $moneda='&euro;'; elseif($pret[1]=='USD') $moneda='$'; else $moneda=$pret[1];	
		$pret_minim=$pret[0]." ".$moneda;
		} else {
		 if($this->moneda=='EURO') $moneda='&euro;'; elseif($this->moneda=='USD') $moneda='$'; else $moneda=$this->moneda;	
		 $pret_minim=$this->pret." ".$moneda;	
		}
		$denumire=$this->resize($this->denumire, $this->set_denumire_circuit_lenght);
		$obiective_turistice=$this->resize($this->obiective_turistice,$this->obiective_turistice_lenght);
		$localitati_traversate=$this->resize($this->localitati_traversate,$this->localitati_traversate_lenght);
		
		$tpl=new TPL($template);
		if(!$this->poza1) $poza=$GLOBALS['sitepath_class_parinte']."images/no_photo.jpg"; else $poza=$GLOBALS['sitepath_class_parinte'].'thumb_circuit/'.$this->poza1;
		$tpl->replace_tags(array(
		"id_circuit"=> $this->id_circuit,
		"denumire_continent"=> $this->denumire_continent,
		"poza1"=>$poza,
		"denumire"=> $denumire,
		"numar_zile"=> $this->nr_zile,
		"obiective_turistice"=>$obiective_turistice,
		"localitati_traversate"=>$localitati_traversate,
		"pret_minim"=> $pret_minim,
		"link"=>$link,
		"titlu_link"=>$this->denumire
		));
		
		$tpl->output_tpl();
	}
	
}

 class AFISARE_CIRCUITE { 
function __construct()
	{ //se creaza paginatia_______________________________________________
     	$this->tip_oferta=0;
		$this->tari=0;
		$this->zone=0;
		$this->agentie=0;
		$this->azi=date("Y-m-d");
		$this->last_minute=0;
		$this->random=0;
		$this->oferta_speciala=0;
		$this->pagini_id_uri="";
		$this->promovata=0;
		$this->jeka=0;
		$this->afisare_no_limit=0;
		$this->early='nu';
		$this->cazare='nu';
		$this->oferte_pagina=0;
		$this->templates_jeka="afisare_oferta_jeka.tpl";
		$this->templates_promo="afisare_oferta_lunga_promo.tpl";
		$this->templates="afisare_oferta_lunga.tpl";
		$this->denumire_tip_oferta='';
		$this->denTara='';
		$this->denZone='';
		$this->denTransport='';
		$this->denOrase='';
		$this->dimensiuneNumeHotel='';
		$nr_tot=0;
		$nr_jeka=0;
		$nr_promov=0;
	}
	
	//funtii pentru sortarea ofertelor__________________________
	function set_oferte_pagina($value)
	{
	  $this->oferte_pagina=$value;
	}
	function setLastMinute()
	{
		$this->last_minute=1;
	}
	function setRandom()
	{
		$this->random=1;
	}
	
	function setIdFav($value)
	{
		$this->favorite_id=$value;
	}
	
		
	function setTipOferta($tip)
	{
		$this->tip_oferta="'".get_id_tip_sejur(desfa_link($tip))."'";
		$this->denumire_tip_oferta=fa_link($tip);
	}
	
	function setKeywords($keyword_old)
	{
			if(strlen($keyword_old)>2) $this->keywords=$keyword_old;
	}
	
	function setContinent($continent)
	{
		$this->continent="'".get_id_continent(desfa_link($continent))."'";
		$this->denContinent=$continent;
	}
	
	function setTari($tari)
	{
		$this->tari="'".get_id_tara(desfa_link($tari))."'";
		$this->denTara=$tari;
	}

	function setOfertaSpeciala($value)
	{
		$this->oferta_speciala=$value;
	}
	
	function setAgentie($agentie)
	{
		$this->agentie=$agentie;
	}
	
	function setOrdonarePret($value)
	{
	  $this->ord_pret=$value;
	}
	
	function setOrdonareRelevanta($value)
	{
		$this->ord_relevanta=$value;
	}
	
	function setLocalitatiPlecare($value)
	{
		$this->localitati_plecare=get_id_localitate(desfa_link($value));
		$this->denLocalitatiPlecare=$value;
	}
	
	function set_oferte_vizitate($value)
	{
	  $this->oferte_vizitate=$value;	
	}
	
	//selectam ofertele care indeplinesc conditiile date
	function sel_toate_nr() {
	  $sel_nr="SELECT
		circuite.id_circuit
		FROM
		continente
		inner Join circuite ON continente.id_continent = circuite.id_continent ";
		if($this->tari) $sel_nr=$sel_nr." Left Join circuite_tari ON circuite.id_circuit = circuite_tari.id_circuit ";
		$sel_nr=$sel_nr." WHERE
		circuite.valabila = 'Da' ";
			if($this->continent) $sel_nr=$sel_nr." AND circuite.id_continent = ".$this->continent." ";
			if($this->tari) $sel_nr=$sel_nr." AND circuite_tari.id_tara = ".$this->tari." ";
			if($this->keywords){
					$sel_nr=$sel_nr."\n AND ( continente.nume_continent LIKE '%".$this->keywords."%' ";
					$sel_nr=$sel_nr."\n OR circuite.denumire LIKE '%".$this->keywords."%' )";
			}
		$sel_nr=$sel_nr." GROUP BY circuite.id_circuit ";
		return $sel_nr;		
	}
	function sel_toate_pagina() {
	    $sel_toate="
		SELECT
		continente.nume_continent,
		circuite.id_circuit,
		circuite.denumire,
		circuite.nr_zile,
		circuite.poza1,
		circuite.pret_minim,
		circuite.moneda,
		info_circuite.obiective_turistice
		FROM
		circuite
		inner Join continente ON continente.id_continent = circuite.id_continent
		Left Join info_circuite ON circuite.id_circuit = info_circuite.id_circuit ";
		if($this->tari) $sel_toate=$sel_toate." Left Join circuite_tari ON circuite.id_circuit = circuite_tari.id_circuit ";
		$sel_toate=$sel_toate." WHERE
		circuite.valabila = 'Da' ";
			if($this->continent) $sel_toate=$sel_toate." AND circuite.id_continent = ".$this->continent." ";
			if($this->tari) $sel_toate=$sel_toate." AND circuite_tari.id_tara = ".$this->tari." ";
			if($this->keywords){
					$sel_toate=$sel_toate."\n AND ( continente.nume_continent LIKE '%".$this->keywords."%' ";
					$sel_toate=$sel_toate."\n OR circuite.denumire LIKE '%".$this->keywords."%' )";
			}
		if($this->oferte_vizitate) $sel_toate=$sel_toate." and circuite.id_circuit in (".$this->oferte_vizitate.") ";	
		$sel_toate=$sel_toate." GROUP BY circuite.id_circuit ";
		return $sel_toate;
	}

function initializare() {
  //aflam cate oferte avem selectate. se lucreaza cu 3 selecturi fiind 3 tipuri de oferte pe o pagina.
  $sel_toate=$this->sel_toate_nr();
  $que_toate=mysql_query($sel_toate) or die(mysql_error());
  $this->nr_tot=mysql_num_rows($que_toate);
  @mysql_free_result($que_toate);
	
  if($this->nr_tot>'0') {
  $this->pag=floor($this->nr_tot/$this->oferte_pagina);
  if(fmod($this->nr_tot,$this->oferte_pagina)!='0') $this->pag=$this->pag+1; 
   } else $this->pag=1;
   
   return $this->nr_tot;
 }
 
 function paginare()
 {
  // se adauga div-le si se initializeaza afisarea
  $this->initializare();
  $cond="";
  if($this->denumire_tip_oferta) $cond=$cond."&conditie[setTipOferta]=".$this->denumire_tip_oferta;
  if($this->keywords) $cond=$cond."&conditie[setKeywords]=".$this->keywords;
  if($this->continent) $cond=$cond."&conditie[setContinent]=".$this->denContinent;
  if($this->denTara) $cond=$cond."&conditie[setTari]=".$this->denTara;
  if($this->oferta_speciala) $cond=$cond."&conditie[setOfertaSpeciala]=".$this->oferta_speciala;
  if($this->agentie) $cond=$cond."&conditie[setAgentie]=".$this->agentie;
  if($this->ord_pret) $cond=$cond."&conditie[setOrdonarePret]=".$this->ord_pret;
  if($this->localitati_plecare) $cond=$cond."&conditie[setLocalitatiPlecare]=".$this->denLocalitatiPlecare;
 
  $total=$this->nr_tot;
  if($total<>'1') $den_of="circuite"; else $den_of="circuit";
  if($this->pag<>'1') $den_pag="pagini"; else $den_pag="pagina";
  ?>
  <div class="paginationLine">
    <div class="paginationInfo"><?php echo $total.' '.$den_of.' | '.$this->pag.' '.$den_pag; ?></div>
    <div id="paginatie" class="pagination">&nbsp;</div>
  </div>
  <br class="clear" />
  <?php /*?><div id="loader" class="loading">&nbsp;</div><?php */?>
  <div id="afisare"><?php $this->afisare(1); ?></div>
  <br class="clear" />
  <div class="paginationLine">
    <div class="paginationInfo"><?php echo $total.' '.$den_of.' | '.$this->pag.' '.$den_pag; ?></div>
    <div id="paginatie1" class="pagination">&nbsp;</div>
  </div>
  <?php if($this->pag>'1') { ?>
<script type="text/javascript" src="/js/paginare.js"></script>
<script type="text/javascript">
//<![CDATA[
  makepagini('<?php echo $this->pag; ?>','1');
  
  function clic(pag) {
  <?php /*?>$('#loader').show();<?php */?>
  $("#afisare").load("/config/includes/class/class_circuite/afisare.php?nr_pagina="+pag+"&nr_pe_pag=<?php echo $this->oferte_pagina.$cond; ?>");
  }
//]]>
</script>
<?php
   }
 }
 
 function fara_paginare()
 { ?>
  <div id="afisare"><?php $this->afisare(1); ?></div>
<?php }
 
 function afisare($pagina) { ?>
 <script type="text/javascript">
//<![CDATA[
 <?php /*?>$('#loader').hide();<?php */?>
 //]]>
</script>
<?php 
  $sel_toate=$this->sel_toate_pagina();
  $from=$this->reverse_calculeaza_pagina($pagina, $this->oferte_pagina);
  if($this->ord_pret) $cond_normala=$cond_normala." ORDER BY circuite.pret_minim_lei ".$this->ord_pret." ";
  else $cond_normala=$cond_normala." ORDER BY circuite.data_adaugarii DESC, circuite.denumire ";
  $cond_normala=$cond_normala." LIMIT $from, $this->oferte_pagina ";
  $sel_normal=$sel_toate.$cond_normala;
  $que_normal=mysql_query($sel_normal) or die(mysql_error());
  $nr_tot=mysql_num_rows($que_normal); 
  if($nr_tot<'1') {
    $this->eroare();
  } else {
		while($row_toate=mysql_fetch_array($que_normal)){
	        $circuit=new CIRCUIT($row_toate["id_circuit"]);
			$circuit->set_poza1($row_toate["poza1"]);
			$circuit->set_denumire($row_toate["denumire"]);
			$circuit->set_denumire_continent($row_toate["nume_continent"]);
			$circuit->set_nr_zile($row_toate["nr_zile"]);
			$circuit->set_obiective_turistice($row_toate["obiective_turistice"]);
			$circuit->set_pret($row_toate["pret_minim"]);
			$circuit->set_moneda($row_toate["moneda"]);
			$circuit->set_obiective_turistice_lenght(255);
			$circuit->load_localitati_traversate();
			if(!$this->set_denumire_lenght) $circuit->set_denumire_circuit_lenght(50); else $circuit->set_denumire_circuit_lenght($this->set_denumire_lenght);			 							 
			if($this->oferta_speciala){
				if(!$this->set_denumire_lenght) $circuit->set_denumire_circuit_lenght(30); else $circuit->set_denumire_circuit_lenght($this->set_denumire_lenght);
				$circuit->set_obiective_turistice_lenght(255);
				$circuit->afiseaza($this->oferta_speciala);
			}
			else $circuit->afiseaza($_SERVER['DOCUMENT_ROOT']."/templates/afisare_circuit_lunga.tpl");
	 unset($circuit);
	} @mysql_free_result($que_normal);
  }
}
 function id_oferte_pagina($rezultate,$nr_oferte_total,$pagini)
	{	//preluam datele si le sortam astfel incat sa nu apara 2 oferte una langa alta de la aceasi agentie
		$doi=array();
		$j=0;
		$ofertele=array();
		while($j < $nr_oferte_total)
		{
			$doi[$rezultate[$j]['id_user']][]=$rezultate[$j]['id_oferta'];
			$ofertele[$rezultate[$j]['id_oferta']]=$rezultate[$j];
			$j++;
		}
		
		$agentii_nr=array_keys($doi);
		
		$i=0;
		foreach($agentii_nr as $key)
		{
			$oferte_agentii[$key]=count($doi[$key]);
			$pointer_agentii[$key]=0;
			$i++;
		}
		$j=0;
		
		while($j<=$nr_oferte_total && count($agentii_nr)!=0){
			foreach($agentii_nr as $key){
				$trei[$j]=$ofertele[$doi[$key][$pointer_agentii[$key]]];
				$oferte_agentii[$key]--;
				$pointer_agentii[$key]++;
				$j++;
			}
			$i=0;$jos=0;
			foreach($agentii_nr as $key){
				if($oferte_agentii[$key]==0){
					$down[$jos]=$i;
					$jos++;
				}
			$i++;
			}
			if(count($down)>0)
			foreach($down as $key){
				unset($agentii_nr[$key]);
			}
			unset($down);
			$agentii_nr=array_values($agentii_nr);
						
		}
		return($trei);
	}
 function reverse_calculeaza_pagina($numar_pagina,$nr_oferte_pagina)
	{   //returneaza numarul de la care se porneste afisarea in funtie de pagina
		$numar_pagina=$numar_pagina-1;
		$from=$numar_pagina*$nr_oferte_pagina;
		return $from;
	}
	
 function eroare($template="negasit_filtrare.tpl")
	{
		$template=$_SERVER['DOCUMENT_ROOT']."/templates/".$template;
		$tpl=new TPL($template);
		$tpl->output_tpl();
	}	
} ?>