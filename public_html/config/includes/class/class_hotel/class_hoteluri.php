<?php class HOTELURI {
	var $id_agentie;
	var $id_tara;
	var $denumire_tara;
	var $id_zona;
	var $denumire_zona;
	var $id_localitate;
	var $denumire_localitate;
	var $id_hotel;
	var $nume_hotel;
	var $nr_stele;
	var $nume_poza1;
	var $poza1;
	var $descriere_hotel;
	var $id_cazare;	
	var $denumire_cazare;
	var $descriere_hotel_lenght;
	var $nume_hotel_lenght;
	var $denumire_cazare_lenght;
	
	var $keywords_1_words;
	var $keywords_2_words;

	function __construct($id_hotel) {
		$this->id_hotel=$id_hotel;
	}

	function set_denumire_tara($value)
	{
		$this->denumire_tara=$value;
	}
	
	function set_denumire_zona($value)
	{
		$this->denumire_zona=$value;
	}
	
	function set_denumire_localitate($value)
	{
		$this->denumire_localitate=$value;
	}
	
	function set_nume_hotel($value)
	{
		$this->nume_hotel=$value;
	}
	function set_descriere_hotel($value)
	{
		$this->descriere_hotel=strip_tags($value);
	}
	function set_nr_stele($value)
	{
		$this->nr_stele=$value;
	}	
	
	function set_poza1($value)
	{
		$this->poza1=$value;
	}
	
	function set_descriere_hotel_lenght($value)
	{
		$this->descriere_hotel_lenght=$value;
	}

	function set_denumire_localitate_lenght($value)
	{
		$this->denumire_localitate_lenght=$value;
	}
	function set_denumire_zona_lenght($value)
	{
		$this->denumire_zona_lenght=$value;
	}
	function set_denumire_tara_lenght($value)
	{
		$this->denumire_tara_lenght=$value;
	}	
	function creaza_link()
	{   
		$linkul="/hoteluri/".fa_link($this->denumire_tara)."/".fa_link($this->denumire_localitate)."/".fa_link(trim($this->nume_hotel))."-".fa_link($this->id_hotel).".html";
		return $linkul;
	}
	function set_link_promo($value)
	{
	  $this->link_promovata=$value;	
	}
	function afiseaza($template)
	{
		if($this->link_promovata) $link=$this->link_promovata;
		else $link=$this->creaza_link();
		$tpl = new TPL($template);

			$stele='<span class="stele-mici-'.$this->nr_stele.'"></span>';
		
		$descriere_hotel=$this->descriere_hotel;
		if($this->descriere_hotel_lenght) {
		  if(strlen($descriere_hotel)>$this->descriere_hotel_lenght) {
		    $poz=strpos($descriere_hotel, ' ', $this->descriere_hotel_lenght);
			$descriere_hotel=substr($this->descriere_hotel,0,$poz);
			if(strlen($this->descriere_hotel)>$this->descriere_hotel_lenght) $descriere_hotel=$descriere_hotel."...";
			}
		}

		$denumire_localitate=$this->denumire_localitate;
		if($this->denumire_localitate_lenght) {
			$denumire_localitate=substr($this->denumire_localitate,0,$this->denumire_localitate_lenght);
			if(strlen($this->denumire_localitate)>$this->denumire_localitate_lenght) $denumire_localitate=$denumire_localitate."...";
		}
		$denumire_zona=$this->denumire_zona;
		if($this->denumire_zona_lenght) {
			$denumire_zona=substr($this->denumire_zona,0,$this->denumire_zona_lenght);
			if(strlen($this->denumire_zona)>$this->denumire_zona_lenght) $denumire_zona=$denumire_zona."...";
		}
		$denumire_tara=$this->denumire_tara;
		if($this->denumire_tara_lenght) {
			$denumire_tara=substr($this->denumire_tara,0,$this->denumire_tara_lenght);
			if(strlen($this->denumire_tara)>$this->denumire_tara_lenght) $denumire_tara=$denumire_tara."...";
		}
		$pret=pret_minim_sejur('', $this->id_hotel, '', '');
		if($pret[1]=='EURO') $moneda='&euro;'; elseif($pret[1]=='USD') $moneda='$'; else $moneda=$pret[1];
  $pret_of=$pret['pret']." ".$moneda;
		$denumire_hotel=$this->nume_hotel;
		if(!$this->poza1) $poza=$GLOBALS['sitepath_class_parinte']."images/no_photo.jpg"; else $poza=$GLOBALS['sitepath_class_parinte'].'thumb_hotel/'.$this->poza1;
		
		$tpl->replace_tags(array(
		"id_hotel"=> $this->id_hotel,
    	"denumire_hotel"=> $denumire_hotel,
		"stele"=>$stele,
		"descriere_hotel"=> $descriere_hotel,
		"poza1"=>$poza,
		"denumire_localitate"=> $denumire_localitate,
		"denumire_zona"=> $denumire_zona,
		"denumire_tara"=> $denumire_tara,
		"pret_minim"=>$pret_of,
		"exprimare_pret"=>$pret['exprimare_pret'],
		"link"=>$link
		));
		
		$tpl->output_tpl();
 	}

}

class AFISARE_HOTELURI { 
function __construct()
	{ //se creaza paginatia_______________________________________________
		$this->tari=0;
		$this->zone=0;
		$this->localitate='';
		$this->jeka='';
		$this->azi=date("Y-m-d");
		$this->early='';
	}
	
	//funtii pentru sortarea ofertelor__________________________
	function set_oferte_pagina($value)
	{
	  $this->oferte_pagina=$value;
	}
	function setJeka($value)
	{
	   $this->jeka=$value;
	}
	
	function setTari($tari)
	{
	  $this->id_tara=get_id_tara(desfa_link($tari));
	  $this->denTara=$tari;
	  $this->tari="'".$this->id_tara."'";
	}

		
	function setZone($zone)
	{
	  $this->id_zone=get_id_zona(desfa_link($zone), $this->id_tara);
	  $this->zone="'".$this->id_zone."'";
	  $this->denZone=$zone;
		
	}
	
	function setOrase($orase)
	{
		$this->orase="'".get_id_localitate(desfa_link($orase), $this->id_zone)."'";
		$this->denOrase=$orase;
	}
	
	function setStele($value)
	{ 
	   $this->stelele=$value;
	}
	function setOfertaSpeciala($value)
	{
		$this->oferta_speciala=$value;
	}
	function setExeptHotel($value)
	{
	  $this->exept_hotel=$value;
	}
	function setOrdonareStele($value)
	{
		$this->ord_stele=$value;
	}
	function setOrdonareNume($value)
	{
		$this->ord_nume=$value;
	}
	function setFaraOferte($val)
	{
		$this->fara_oferte='da';
	}
	function set_oferte_vizitate($value)
    {
	$this->oferte_vizitate=$value;	
    }
	function setEarly($value)
    {
	 $this->early=$value;
    }
	//selectam ofertele care indeplinesc conditiile date
	function sel_toate_nr() {
    $sel_toate="SELECT
	  sum(if(oferte.id_oferta is not null,1,0)) as nr,
	  hoteluri.id_hotel as id,
	  hoteluri.nume AS denumire_hotel,
	  hoteluri.poza1,
	  hoteluri.descriere AS descriere_hotel,
	  hoteluri.stele,
	  hoteluri.id_hotel,
	  localitati.denumire AS denumire_localitate,
	  zone.denumire AS denumire_zona,
	  tari.denumire AS denumire_tara
	  FROM hoteluri
	  INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	  INNER JOIN zone ON localitati.id_zona = zone.id_zona
	  INNER JOIN tari ON zone.id_tara = tari.id_tara
	  LEFT JOIN oferte ON (hoteluri.id_hotel = oferte.id_hotel AND oferte.valabila = 'da') ";
	  if($this->early=='da')  $sel_nr=$sel_nr." INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta AND early_booking.end_date >= NOW() AND early_booking.discount >'0' and early_booking.tip = 'sejur') ";
	  $sel_toate=$sel_toate." WHERE
	  hoteluri.tip_unitate <> 'Circuit' ";
	  if($this->oferte_vizitate) $sel_toate=$sel_toate." and hoteluri.id_hotel in (".$this->oferte_vizitate.") ";
	  if($this->tari) $sel_toate=$sel_toate." AND tari.id_tara = $this->tari ";
	  if($this->zone) $sel_toate=$sel_toate." AND zone.id_zona = ".$this->zone." ";
	  if($this->stelele) $sel_toate.=" AND hoteluri.stele = '$this->stelele' ";
	  if($this->exept_hotel) $sel_toate=$sel_toate." AND hoteluri.id_hotel <> '".$this->exept_hotel."' ";
	  if($this->orase) $sel_toate=$sel_toate." AND localitati.id_localitate = ".$this->orase." ";
	  if($this->fara_oferte) $sel_toate=$sel_toate." and nr is null ";
	  $sel_toate=$sel_toate." GROUP BY hoteluri.id_hotel ";
	  if($this->ord_nume) $sel_toate=$sel_toate." \n ORDER BY hoteluri.nume ".$this->ord_nume;
	  elseif($this->ord_stele) $sel_toate=$sel_toate." \n ORDER BY hoteluri.stele ".$this->ord_stele;
	  else $sel_toate=$sel_toate."\n ORDER BY nr DESC ";
	  //echo $sel_toate;	
  return $sel_toate;
}
	
function initializare() {
  //aflam cate oferte avem selectate. se lucreaza cu 3 selecturi fiind 3 tipuri de oferte pe o pagina.
  
  $sel_toate=$this->sel_toate_nr();
  $this->que_normal=mysql_query($sel_toate) or die(mysql_error());
  $this->nr_tot=mysql_num_rows($this->que_normal);
  
  if($this->nr_tot>'0') {
  $this->pag=floor($this->nr_tot/$this->oferte_pagina);
  if(fmod($this->nr_tot,$this->oferte_pagina)!='0') $this->pag=$this->pag+1; 
   } else $this->pag=0;
   
   if($this->nr_tot==0) $this->pag=1;
  
  return $this->nr_tot;
}
 
 function paginare()
 {
  // se adauga div-le si se initializeaza afisarea
  
  $this->initializare();
  $cond="";
  if($this->denTara) $cond=$cond."&conditie[setTari]=".$this->denTara;
  if($this->denZone) $cond=$cond."&conditie[setZone]=".$this->denZone;
  if($this->denOrase) $cond=$cond."&conditie[setOrase]=".$this->denOrase;
  if($this->stelele) $cond=$cond."&conditie[setStele]=".$this->stelele;
  if($this->oferta_speciala) $cond=$cond."&conditie[setOfertaSpeciala]=".$this->oferta_speciala;
  if($this->ord_nume) $cond=$cond."&conditie[setOrdonareNume]=".$this->ord_nume;
  if($this->ord_stele) $cond=$cond."&conditie[setOrdonareStele]=".$this->ord_stele;
  if($this->fara_oferte) $cond=$cond."&conditie[setFaraOferte]='da'";
  @mysql_free_result($this->que_normal); 
  $total=$this->nr_tot;
  if($total<>'1') $den_of="hoteluri"; else $den_of="hotel";
  if($this->pag<>'1') $den_pag="pagini"; else $den_pag="pagina";
  ?>
  <div class="paginationLine">
    <div class="paginationInfo"><?php echo $total.' '.$den_of.' | '.$this->pag.' '.$den_pag; ?></div>
    <div id="paginatie" class="pagination">&nbsp;</div>
  </div>
  <br class="clear" />
  <?php /*?><div id="loader" class="loading">&nbsp;</div><?php */?>
  <div id="afisare"><?php $this->afisare(1); ?></div>
  <br class="clear" />
  <div class="paginationLine">
    <div class="paginationInfo"><?php echo $total.' '.$den_of.' | '.$this->pag.' '.$den_pag; ?></div>
    <div id="paginatie1" class="pagination">&nbsp;</div>
  </div>
  
 <?php if($this->pag>'1') {
  ?>
<script type="text/javascript" src="/js/paginare.js"></script>   
<script type="text/javascript">
//<![CDATA[
  makepagini('<?php echo $this->pag; ?>','1');
  function clic(pag) {
  <!--$('#loader').show();-->
  $("#afisare").load("/config/includes/class/class_hotel/afisare.php?nr_pagina="+pag+"&nr_pe_pag=<?php echo $this->oferte_pagina.$cond; ?>");
  }
//]]>
</script>
<?php
   }
 }
 
 function fara_paginare()
 { ?>
  <div id="afisare"><?php $this->afisare(1); ?></div>
<?php
 }
 
 function afisare($pagina)
 { //pentru afisarea unei oferte se transmite clasei OFERTA valorile care le dorim sa le afisam si template-ul dorit
 ?>
 <?php /*?><script type="text/javascript">
//<![CDATA[
 $('#loader').hide();
 //]]>
</script><?php */?>
<?php 
   $from=$this->reverse_calculeaza_pagina($pagina, $this->oferte_pagina);
   $sel_toate=$this->sel_toate_nr();
   //echo $sel_toate." LIMIT $from, $this->oferte_pagina ";
   $this->que_normal=mysql_query($sel_toate." LIMIT $from, $this->oferte_pagina ") or die(mysql_error());
   $this->nr_tot=mysql_num_rows($this->que_normal);
   if($this->nr_tot=='0' && !$this->oferta_speciala) {
   if(!$this->stelele) $this->eroare(); else $this->eroare_filtre();
   } else {
	while($row_hotel=mysql_fetch_array($this->que_normal)) {   
	$oferta=new HOTELURI($row_hotel["id_hotel"]);
	$oferta->set_poza1($row_hotel["poza1"]);
	$oferta->set_nume_hotel($row_hotel["denumire_hotel"]);
	$oferta->set_denumire_localitate($row_hotel["denumire_localitate"]);
	$oferta->set_denumire_zona($row_hotel["denumire_zona"]);
	$oferta->set_denumire_tara($row_hotel["denumire_tara"]);
	$oferta->set_descriere_hotel($row_hotel["descriere_hotel"]);
	$oferta->set_nr_stele($row_hotel["stele"]);
	$oferta->set_descriere_hotel_lenght(220);
	if($this->oferta_speciala) {
	  $oferta->set_denumire_zona_lenght(10);
	  $oferta->set_denumire_tara_lenght(10);
	  $oferta->afiseaza($this->oferta_speciala);	
	} else $oferta->afiseaza($_SERVER['DOCUMENT_ROOT']."/templates/afisare_hotel_lunga.tpl");
		}	
	@mysql_free_result($this->que_normal);
  }
}

 function reverse_calculeaza_pagina($numar_pagina,$nr_oferte_pagina)
	{   //returneaza numarul de la care se porneste afisarea in funtie de pagina
		$numar_pagina=$numar_pagina-1;
		$from=$numar_pagina*$nr_oferte_pagina;
		return $from;
	}
	
 function eroare($template="negasit_filtrare.tpl")
	{
		$template=$_SERVER['DOCUMENT_ROOT']."/templates/".$template;
		$tpl=new TPL($template);
		$tpl->output_tpl();
	}
	function eroare_filtre($template="negasit_filtrare_filtre.tpl")
	{
		$template=$_SERVER['DOCUMENT_ROOT']."/templates/".$template;
		$tpl=new TPL($template);
		if($this->stelele) { $stele="la hoteluri de ".$this->stelele; if($this->stelele>'1') $stele=$stele." stele "; else  $stele=$stele." stea "; } else $stele='';
		if($this->denTransport) { if($this->denTransport=='individual') $tr=$tr.", transport propriu "; else $tr=$tr.", cu ".ucwords(strtolower($this->denTransport)); } else $tr='';
		if($this->masa) { $masa="cu masa ".$this->masa; } else $masa='';
		$tpl->replace_tags(array(
		"stele"=>$stele,
		"transport"=>$tr,
		"masa"=>$masa
		));
		$tpl->output_tpl();
	}
} ?>