<?php
class CUPOANE {
	function generate_link($id_campanie) {
		$linkCC = md5(md5($id_campanie));
		$linkCC = $GLOBALS['sitepath'].'cupoane-reducere/'.$linkCC;
		return $linkCC;
	}
	
	function decode_link($hash) {
		$sel_CC = "SELECT
		cupoane_campanii.*,
		tari.denumire AS den_tara,
		zone.denumire AS den_zona,
		localitati.denumire AS den_localitate
		FROM cupoane_campanii
		LEFT JOIN tari ON tari.id_tara = cupoane_campanii.id_tara
		LEFT JOIN zone ON zone.id_zona = cupoane_campanii.id_zona
		LEFT JOIN localitati ON localitati.id_localitate = cupoane_campanii.id_localitate
		WHERE md5(md5(id_campanie)) = '".$hash."' ";
		$que_CC = mysql_query($sel_CC) or die(mysql_error());
		$row_CC = mysql_fetch_assoc($que_CC);
		@mysql_free_result($row_CC);

		return $row_CC;
	}
	
	function generate_code($idcode) {
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$res = "";
		for($i = 0; $i < 8; $i++) {
			$res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
		$res = substr_replace($res, $idcode, 4, 0);
		return $res;
	}
	
	function insert_coupon($id_campanie, $data_end, $sex, $nume, $prenume, $email, $data_nasterii, $telefon) {
		$id_useri_fizice = insert_user($sex, $nume, $prenume, $email, $data_nasterii, $telefon, 'inregistrare cupon');
		
		$message = array();
		
		$sel_coupon = "SELECT id_cupon FROM cupoane WHERE id_campanie = '".$id_campanie."' AND id_useri_fizice = '".$id_useri_fizice."' AND session_id = '".session_id()."' ";
		$que_coupon = mysql_query($sel_coupon) or die(mysql_error());
		$row_coupon = mysql_fetch_assoc($que_coupon);
		if(mysql_num_rows($que_coupon)==0) {
			$ins_cupon = "INSERT INTO cupoane SET
			id_campanie = '".$id_campanie."',
			id_useri_fizice = '".$id_useri_fizice."',
			ip = '".$_SERVER['REMOTE_ADDR']."',
			session_id = '".session_id()."',
			activ = 'da' ";
			$rez_cupon = mysql_query($ins_cupon) or die(mysql_error());
			@mysql_free_result($rez_cupon);
			
			$sel_couponn = "SELECT * FROM cupoane ORDER BY id_cupon DESC LIMIT 0,1";
			$que_couponn = mysql_query($sel_couponn) or die(mysql_error());
			$row_couponn = mysql_fetch_assoc($que_couponn);
			@mysql_free_result($que_couponn);
			
			$message['cupon'] = $row_couponn;
			$idcoupon = $message['cupon']['id_cupon'];
			$cod_cupon = $this->generate_code($idcoupon);
			$message['cupon']['cod_cupon'] = $cod_cupon;
			
			$upd_cuponn = "UPDATE cupoane SET cod_cupon = '".$cod_cupon."' WHERE id_cupon = '".$idcoupon."' ";
			$rez_cuponn = mysql_query($upd_cuponn) or die(mysql_error());
			@mysql_free_result($rez_cuponn);
			
			setcookie('cupon', md5(md5($idcoupon)), $data_end, '/', 'ocaziituristice.ro');
			
			$message['error'] = '0';
		} else {
			$message['error'] = '1';
			$message['mesaje'][0] = 'EROARE! Nu puteti inregistra acelasi cupon de mai multe ori!';
		} @mysql_free_result($que_coupon);
		
		return $message;
	}
	
	function decode_cupon($hash, $activ=NULL) {
		if($activ==NULL) $activ = 'da';
		
		$sel_cupon = "SELECT 
		cupoane.*,
		cupoane_campanii.*,
		useri_fizice.*
		FROM cupoane
		INNER JOIN cupoane_campanii ON cupoane_campanii.id_campanie = cupoane.id_campanie
		INNER JOIN useri_fizice ON useri_fizice.id_useri_fizice = cupoane.id_useri_fizice
		WHERE md5(md5(cupoane.id_cupon)) = '".$hash."' 
		AND cupoane.activ = '".$activ."' ";
		$que_cupon = mysql_query($sel_cupon) or die(mysql_error());
		$row_cupon = mysql_fetch_assoc($que_cupon);
		@mysql_free_result($row_cupon);
		
		//echo '<pre>';print_r($row_cupon);echo '</pre>';
		return $row_cupon;
	}
	
	function valideaza_cupon($id_cupon, $id_tara, $id_zona, $id_localitate) {
		$sel_cupon = "SELECT 
		cupoane.*,
		cupoane_campanii.*
		FROM cupoane
		INNER JOIN cupoane_campanii ON cupoane_campanii.id_campanie = cupoane.id_campanie
		WHERE cupoane.id_cupon = '".$id_cupon."'
		AND cupoane.activ = 'da'
		";
		$que_cupon = mysql_query($sel_cupon) or die(mysql_error());
		$row_cupon = mysql_fetch_assoc($que_cupon);
		@mysql_free_result($row_cupon);
		
		$valid = 0;
		if($row_cupon['id_localitate']==$id_localitate or $row_cupon['id_localitate']==0) $valid += 1;
		if($row_cupon['id_zona']==$id_zona or $row_cupon['id_zona']==0) $valid += 1;
		if($row_cupon['id_tara']==$id_tara or $row_cupon['id_tara']==0) $valid += 1;
		
		if($valid==3) $cupon = 'da'; else $cupon = 'nu';
		return $cupon;
	}
	
	function inregistreaza_cupon($cod_cupon) {
		$sel_cupon = "SELECT 
		cupoane.*,
		cupoane_campanii.*,
		useri_fizice.*
		FROM cupoane
		INNER JOIN cupoane_campanii ON cupoane_campanii.id_campanie = cupoane.id_campanie
		INNER JOIN useri_fizice ON useri_fizice.id_useri_fizice = cupoane.id_useri_fizice
		WHERE cupoane.cod_cupon = '".$cod_cupon."'
		AND cupoane.activ = 'da' ";
		$que_cupon = mysql_query($sel_cupon) or die(mysql_error());
		$row_cupon = mysql_fetch_assoc($que_cupon);
		@mysql_free_result($row_cupon);
		
		if(mysql_num_rows($que_cupon)>0) {
			setcookie('cupon', md5(md5($row_cupon['id_cupon'])), strtotime($row_cupon['data_inceput'].' + '.$row_cupon['nr_zile'].' days'), '/', 'ocaziituristice.ro');
			return $row_cupon;
		} else {
			return 'eroare';
		}
	}
	
	function dezactiveaza_cupon($cod_cupon) {
		$upd_cupon = "UPDATE cupoane SET activ = 'nu' WHERE cod_cupon = '".$cod_cupon."' ";
		$rez_cupon = mysql_query($upd_cupon) or die(mysql_error());
		@mysql_free_result($rez_cupon);
		
		setcookie('cupon', '', time()-3600, '/', 'ocaziituristice.ro');
	}
}
?>