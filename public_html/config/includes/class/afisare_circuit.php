<?php class DETALII_CIRCUIT {
	function select_det_circuit($id_circuit) {
		//circuite_vizitate____
		$oferte_vizitate='';
		if($_COOKIE['oferte_vizit']) $oferte_vizitate=$_COOKIE['oferte_vizit'];
		if(strlen($oferte_vizitate)==0) $oferte_vizitate='`circuit-'.$id_circuit.'`';
			else {
			$of=explode(',', str_replace('`','',$oferte_vizitate));
			if(!in_array('circuit-'.$id_circuit, $of)) $of[]='circuit-'.$id_circuit;
			$oferte_vizitate='';
			$i=0;
			foreach($of as $key=>$value) {
				$i++;
				if((sizeof($of)<=5) || (sizeof($of)>5 && $i>1)) $oferte_vizitate=$oferte_vizitate.'`'.$value.'`,';
			}
			$oferte_vizitate=substr($oferte_vizitate,0,-1);
		}
		
		$timeout = time() + 60 * 60 * 24 * 5;
		setcookie("oferte_vizit", $oferte_vizitate, $timeout, "/");
		
		$sel="SELECT
		continente.nume_continent,
		circuite.denumire,
		circuite.nr_zile,
		circuite.nr_nopti,
		circuite.poza1,
		circuite.poza2,
		circuite.poza3,
		circuite.valabila,
		circuite.click,
		info_circuite.descrierea_circuitului,
		info_circuite.obiective_turistice,
		info_circuite.serv_incluse,
		info_circuite.serv_neincluse,
		info_circuite.alte_detalii,
		info_circuite.detalii_plecari,
		info_circuite.detalii_preturi,
		transport.denumire as denumire_transport
		FROM
		circuite
		Inner Join continente ON continente.id_continent = circuite.id_continent
		Inner Join info_circuite ON circuite.id_circuit = info_circuite.id_circuit
		left join transport on circuite.id_transport = transport.id_trans
		WHERE
		circuite.id_circuit =  '".$id_circuit."' ";
		$que=mysql_query($sel) or die(mysql_error());
		$param=mysql_fetch_array($que);
		@mysql_free_result($que);
		
		if(sizeof($param)<1) {
			header("HTTP/1.0 404 Not Found");
			//header("Location: ".$sitepath.'404.php');
			$handle = curl_init($sitepath.'404.php');
			curl_exec($handle);
			exit();
		}
		
		$click=$param['click']+1;
		$upd="update circuite set click = '".$click."' where circuite.id_circuit =  '".$id_circuit."' ";
		$queU=mysql_query($upd) or die(mysql_error());
		@mysql_free_result($queU);
		
		$sel="SELECT
		GROUP_CONCAT(localitati.denumire SEPARATOR '-') AS localitati
		FROM
		loc_circuit
		Inner Join localitati ON loc_circuit.id_localitate = localitati.id_localitate
		WHERE loc_circuit.id_circuit='".$id_circuit."'
		GROUP BY loc_circuit.id_circuit	";
		$rez=mysql_query($sel);
		$row=mysql_fetch_array($rez);
		@mysql_free_result($rez);
		$param['localitati_traversate']=$row["localitati"];
		
		$selE="select date_format(end_date , '%d.%m.%Y') as end_early, discount from early_booking where id_oferta = '".$id_circuit."' and tip = 'circuit' and end_date >= now() and discount > '0' Group by end_date Order by end_date ";
		$queE=mysql_query($selE) or die(mysql_error());
		while($rowE=mysql_fetch_array($queE)) {
			$param['early_time'][]=$rowE['end_early'];
			$param['early_disc'][]=$rowE['discount'];
		} @mysql_free_result($queE);
		
		$param['desc_seo']="Circuit ".$param['denumire_continent'].", circuit ".$param['localitati_traversate'].", ".$param['nr_zile']." zile ";
		$param['titlu_seo']="Vacanta in ".$param['denumire_continent'].", circuit in ".$param['localitati_traversate'].", durata ".$param['nr_zile']." zile";
		$param['keyw_seo']=$param['denumire'];
		return $param;
	}
	
	function select_preturi_circuit($id_circuit) {
		$preturi=array();
		$selP="select
		date_format(plecari_circuit.data_plecare , '%d.%m.%Y') as start,
		date_format(plecari_circuit.data_end , '%d.%m.%Y') as sfarsit,
		tip_camera.tip,
		plecari_circuit.data_plecare,
		plecari_circuit.data_end,
		plecari_circuit.pret, 
		plecari_circuit.moneda,
		tip_camera.denumire,
		plecari_circuit.tip_camera 
		from
		plecari_circuit
		left join tip_camera on plecari_circuit.tip_camera = tip_camera.id_camera
		where
		plecari_circuit.id_circuit = '".$id_circuit."'
		and (((plecari_circuit.data_end is null or plecari_circuit.data_end = '0000-00-00') and plecari_circuit.data_plecare >= now()) or ((plecari_circuit.data_end is not null and plecari_circuit.data_end <> '0000-00-00') and plecari_circuit.data_end>= now()))
		Group by plecari_circuit.data_plecare, plecari_circuit.data_end,
		plecari_circuit.pret,
		plecari_circuit.moneda,
		plecari_circuit.tip_camera
		Order by plecari_circuit.data_plecare, plecari_circuit.data_end,
		tip_camera.denumire ";
		$queP=mysql_query($selP) or die(mysql_error());
		$preturi['min_start']='9999-12-31';
		$preturi['max_end']='0000-00-00';
		$preturi['minim']=array('pret'=>9999, 'moneda'=>'EURO');
		while($rowP=mysql_fetch_array($queP)) {
			if($rowP['tip']=='principala') {
				if($preturi['minim']['pret']>$rowP['pret']) $preturi['minim']=array('pret'=>$rowP['pret'], 'moneda'=>$rowP['moneda']);
				if($preturi['min_start']>$rowP['data_plecare']) $preturi['min_start']=$rowP['data_plecare'];
				
				if($rowP['data_plecare']>=$rowP['data_end']) $max=$rowP['data_plecare']; else $max=$rowP['data_end'];
				if($preturi['max_end']<$max) $preturi['max_end']=$max;
				//__	
				$preturi['camera'][$rowP['tip_camera']]=$rowP['denumire'];
				if(sizeof($preturi['data_start'])==0) {
					$index=1; $preturi['data_start'][$index]=$rowP['start'];
					$preturi['data_start_normal'][$index]=$rowP['data_plecare'];
					$preturi['data_end_normal'][$index]=$rowP['data_end'];
					$preturi['data_end'][$index]=$rowP['sfarsit'];
					$preturi['pret'][$index][$rowP['tip_camera']]=$rowP['pret'];
					$preturi['moneda'][$index][$rowP['tip_camera']]=$rowP['moneda'];
				} else {
					$index='';
					foreach($preturi['data_start'] as $key=>$value) {
						if($value==$rowP['start'] && $rowP['sfarsit']==$preturi['data_end'][$key]) $index=$key;
					}
					if(!$index) {
						$index=sizeof($preturi['data_start'])+1;
						$preturi['data_start'][$index]=$rowP['start'];
						$preturi['data_end'][$index]=$rowP['sfarsit'];
						$preturi['data_start_normal'][$index]=$rowP['data_plecare'];
						$preturi['data_end_normal'][$index]=$rowP['data_end'];
						$preturi['pret'][$index][$rowP['tip_camera']]=$rowP['pret'];
						$preturi['moneda'][$index][$rowP['tip_camera']]=$rowP['moneda'];
					} else {
						$preturi['pret'][$index][$rowP['tip_camera']]=$rowP['pret'];
						$preturi['moneda'][$index][$rowP['tip_camera']]=$rowP['moneda'];
					}
				}
			} else $preturi['secundar'][$rowP['start']][$rowP['sfarsit']][$rowP['denumire']]=array('pret'=>$rowP['pret'],'moneda'=>$rowP['moneda']);
		} @mysql_free_result($queP);
		$max_s=explode('-',$preturi['min_start']);
		$preturi['min_start']=date('d.m.Y',mktime(0,0,0,$max_s[1],$max_s[2],$max_s[0]));
		$max_e=explode('-',$preturi['max_end']);
		$preturi['max_end']=date('d.m.Y',mktime(0,0,0,$max_e[1],$max_e[2],$max_e[0]));
		if($preturi['minim']['pret']=='9999') $preturi['minim']=array();
		return $preturi;
	}
	
	function get_transport_detalii($id_circuit) {
		$sel="select
		loc_plecari_circuit.*,
		localitati.denumire as denumire_localitate
		from
		loc_plecari_circuit
		left join localitati on loc_plecari_circuit.id_localitate = localitati.id_localitate
		where
		loc_plecari_circuit.id_circuit = '".$id_circuit."'
		Group by loc_plecari_circuit.ordonare, loc_plecari_circuit.id_localitate
		Order by localitati.denumire ASC ";
		$que=mysql_query($sel) or die(mysql_error());
		while($row=mysql_fetch_assoc($que)) {
			$autocar[]=$row;
		} @mysql_free_result($que);
		return $autocar;
	}
}