<?php class OFERTA
{
	var $id_tara;
	var $denumire_tara;
	var $id_zona;
	var $denumire_zona;
	var $id_localitate;
	var $denumire_localitate;
	var $id_hotel;
	var $nume_hotel;
	var $nr_stele;
	var $poza1;
	var $poza1_mare;
	var $descriere_hotel;
	var $comentariul_nostru;
	var $nota_total_medie;
	var $nr_comentarii;
	var $id_transport;
	var $denumire_transport;
	var $id_oferta;
	var $taxa_aeroport;
	var $denumire_oferta;
	var $denumire_oferta_scurta;
	var $descriere_hotel_lenght;
	var $nume_hotel_lenght;
	var $denumire_oferta_lenght;
	var $orase;
	var $exprimare_pret;
	var $denumire_tip_oferta;
	var $denumire_tip_oferta_lenght;
	var $date_plecare;

	function __construct( $id_oferta ) {
		$this->id_oferta = $id_oferta;
		$this->circuit = 'nu';
		$this->tplG = '';
	}
	function set_id_tara( $value ) {
		$this->id_tara = $value;
	}
	function setFiltru( array $value ) {
		$this->filtru = $value;
	}
	function set_denumire_tara( $value ) {
		$this->denumire_tara = $value;
	}
	function set_id_zona( $value ) {
		$this->id_zona = $value;
	}
	function set_denumire_zona( $value ) {
		$this->denumire_zona = $value;
	}
	function sed_orase( $value ) {
		$this->orase = $value;
	}
	function set_id_localitate( $value ) {
		$this->id_localitate = $value;
	}
	function set_denumire_localitate( $value ) {
		$this->denumire_localitate = $value;
	}
	function set_id_hotel( $value ) {
		$this->id_hotel = $value;
	}
	function set_nume_hotel( $value ) {
		$this->nume_hotel = $value;
	}
	function set_descriere_hotel( $value ) {
		$this->descriere_hotel = strip_tags( $value );
	}
	function set_comentariul_nostru( $value ) {
		$this->comentariul_nostru = strip_tags( $value );
	}
	function set_concept( $value ) {
		$this->concept = strip_tags( $value );
	}
	function set_descriere_hotel_tez( $value ) {
		$this->descriere_hotel_tez = strip_tags( $value );
	}
	function set_nota_total_medie( $value ) {
		$this->nota_total_medie = $value;
	}
	function set_nr_comentarii( $value ) {
		$this->nr_comentarii = $value;
	}
	function set_nr_stele( $value ) {
		$this->nr_stele = $value;
	}
	function set_poza1( $value ) {
		$this->poza1 = $value;
	}
	function set_poza1_mare( $value ) {
		$this->poza1_mare = $value;
	}
	function set_id_transport( $value ) {
		$this->id_transport = $value;
	}
	function set_denumire_transport( $value ) {
		$this->denumire_transport = $value;
	}
	function set_id_oferta( $value ) {
		$this->id_oferta = $value;
	}
	function set_denumire_oferta( $value ) {
		$this->denumire_oferta = $value;
	}
	function set_denumire_oferta_scurta( $value ) {
		$this->denumire_oferta_scurta = $value;
	}
	function set_denumire_tip_oferta( $value ) {
		$this->denumire_tip_oferta = $value;
	}
	function set_descriere_hotel_lenght( $value ) {
		$this->descriere_hotel_lenght = $value;
	}
	function set_nume_hotel_lenght( $value ) {
		$this->nume_hotel_lenght = $value;
	}
	function set_denumire_oferta_lenght( $value ) {
		$this->denumire_oferta_lenght = $value;
	}
	function set_denumire_localitate_lenght( $value ) {
		$this->denumire_localitate_lenght = $value;
	}
	function set_denumire_zona_lenght( $value ) {
		$this->denumire_zona_lenght = $value;
	}
	function set_denumire_tara_lenght( $value ) {
		$this->denumire_tara_lenght = $value;
	}
	function set_denumire_tip_oferta_lenght( $value ) {
		$this->denumire_tip_oferta_lenght = $value;
	}
	function set_exprimare_pret( $value ) {
		$this->exprimare_pret = $value;
	}
	function set_date_plecare( $value ) {
		$this->date_plecare = $value;
	}
	function set_oferta_speciala( $value ) {
		$this->oferta_speciala = $value;
	}
	function setEarlyBooking( $value ) {
		$this->EarlyBooking = $value;
	}
	function setTaxaAeroport( $value ) {
		$this->taxa_aeroport = $value;
	}
	function setLastMinute( $value ) {
		$this->lastminute = $value;
	}
	function setTipOfSp( $value ) {
		$this->tipofsp = $value;
	}
	function setSPOF( $value ) {
		$this->spof = $value;
	}
	function set_discount( $value ) {
		$this->discount = $value;
	}
	function set_earlyend( $value ) {
		$this->end_early = $value;
	}
	function set_nr_zile( $value ) {
		$this->nr_zile = $value;
	}
	function set_nr_nopti( $value ) {
		$this->nr_nopti = $value;
	}
	function set_pret( $value ) {
		$this->pret = $value;
	}
	function set_moneda( $value ) {
		$this->moneda = $value;
	}
	function set_checkin( $value ) {
		$this->checkin = $value;
		$this->date_min = date("Y-m-d", strtotime($this->checkin." - 4 days"));
		$this->date_max = date("Y-m-d", strtotime($this->checkin." + 4 days"));
	}
	function set_durata( $value ) {
		$this->durata = $value;
	}
	function set_masa( $value ) {
		$valueM=explode(',',desfa_link($value));
		$this->masa="'".implode("', '",$valueM)."'";
		//print_r($this->masa);
		//$this->masa = $value;
	}
	function set_tip_activ( $value ) {
		$this->tip_activ = $value;
	}
	function set_titlu_dreapta( $value ) {
		$this->titlu_dreapta = $value;
	}
	function set_titlu_localizare( $value ) {
		$this->titlu_localizare = $value;
	}
	function set_tip_fiu( $value ) {
		$this->denumire_tip_fiu = $value;
	}
	function set_denumire_continent( $value ) {
		$this->denumire_continent = $value;
	}
	function setCircuite( $value ) {
		$this->circuit = 'da';
	}
	function setNrOferte( $value ) {
		$this->nr_oferte = $value;
	}
	function setRec( $value ) {
		$this->recomandata = $value;
	}
	function setDetaliiRecomandata( $value ) {
		$this->detalii_recomandata = $value;
	}
	function setPretRecomandata( $value ) {
		$this->pret_recomandata = $value;
	}
	function setPlecare( $value ) {
		$this->plecare = $value;
	}
	function setTariVizitate( $value ) {
		$this->tari_vizitate = $value;
	}
	function meseHotel($value) {
		$this->mese_hotel = '';
		$sel_mese = "SELECT masa FROM hotel_meal WHERE id_hotel = '".$value."' ORDER BY id_masa ";
		$que_mese = mysql_query($sel_mese) or die(mysql_error());
		while($row_mese = mysql_fetch_array($que_mese)) {
			$this->mese_hotel .= $row_mese['masa'].', ';
		}
		$this->mese_hotel = substr($this->mese_hotel,0,-2);
	}
	function creaza_link() {
		if ( $this->circuit == 'nu' ) {
			if ( $this->earlybooking <> 'da' ) {
				$linkul = $GLOBALS['sitepath_class'] . fa_link( $this->denumire_tara ) . "/" . fa_link( $this->denumire_localitate ) . "/" . fa_link_oferta( $this->denumire_oferta );
				$linkul = $linkul . "-" . fa_link( $this->id_oferta ) . ".html";
			} else {
				$linkul = "/hoteluri/" . fa_link( $this->denumire_tara ) . "/" . fa_link( $this->denumire_localitate ) . "/" . fa_link_oferta( trim( $this->nume_hotel ) ) . "-" . fa_link( $this->id_hotel ) . ".html";
			}
		} else {
			$link = $GLOBALS['sitepath_class'] . "circuit/" . fa_link( $this->denumire_continent ) . "/" . fa_link_circuit( trim( $this->nume_hotel ) );
			$link = $link . "-" . $this->id_oferta . ".html";
			return $link;
		}
		return $linkul;
	}
	function setTplGrup( $temp ) {
		$this->tplG = $temp;
	}
	function afiseaza_sejur( $template ) {
		if ( $this->oferta_speciala == 'da' || !$this->tplG ) {
			// echo $this->EarlyBooking;
			if ( $this->EarlyBooking ) $early = '<div class="early-booking"></div>';
			elseif ( $this->spof ) $early = '<div class="oferta-speciala"></div>';
			elseif ( $this->lastminute ) $early = '<div class="last-minute"></div>';
			//$link = $this->creaza_link();
			if ( $this->circuit == 'nu' ) {
				$link = make_link_oferta($this->denumire_localitate, $this->nume_hotel, $this->denumire_oferta_scurta, $this->id_oferta);
			} else {
				$link = make_link_circuit($this->nume_hotel, $this->id_oferta);
			}
			$tpl = new TPL( $template );
			if ( !$this->pret ) {
				$pret = pret_minim_sejur( $this->id_oferta, '', '', '' );
				$pret = new_price($pret[0]).' '.moneda($pret[1]);
			} else {
				$pret = new_price($this->pret).' '.moneda($this->moneda);
			}
			$stele = '<span class="stele-mici-' . $this->nr_stele . '"></span>';
			$zile = $this->nr_zile;
			if ( $zile > '1' ) $zile = $zile . " zile";
			else $zile = $zile . " zi";
			if ( $this->nr_nopti > 1 ) {
				$nopti = $this->nr_nopti;
				if ( $nopti > '1' ) $nopti = $nopti . " nopti";
				else $nopti = $nopti . " noapte";
				$nr_nopti = $nopti;
			}
			$dutara1 = $zile;
			if ( $nopti ) $dutara1 = $dutara1 . ' / ' . $nopti;
			$denumire_oferta = $this->denumire_oferta;
			$nume_hotel = $this->nume_hotel;
			$denumire_localitate = $this->denumire_localitate;
			if ( $this->denumire_localitate_lenght ) {
				$denumire_localitate = substr( $this->denumire_localitate, 0, $this->denumire_localitate_lenght );
				if ( strlen( $this->denumire_localitate ) > $this->denumire_localitate_lenght ) $denumire_localitate = $denumire_localitate . "...";
			}
			if ( trim( strtolower( $this->denumire_localitate ) ) <> trim( strtolower( $this->denumire_zona ) ) ) {
				$denumire_zona = $this->denumire_zona;
				if ( $this->denumire_zona_lenght ) {
					$denumire_zona = substr( $this->denumire_zona, 0, $this->denumire_zona_lenght );
					if ( strlen( $this->denumire_zona ) > $this->denumire_zona_lenght ) $denumire_zona = $denumire_zona . "...";
				}
				$denumire_zona = $denumire_zona;
			} else $denumire_zona = '';
			$zona = $this->denumire_zona;
			$denumire_tara = $this->denumire_tara;
			if ( $this->denumire_tara_lenght ) {
				$denumire_tara = substr( $this->denumire_tara, 0, $this->denumire_tara_lenght );
				if ( strlen( $this->denumire_tara ) > $this->denumire_tara_lenght ) $denumire_tara = $denumire_tara . "...";
			}
			$denumire_tip_oferta = $this->denumire_tip_oferta;
			if ( $this->denumire_tip_oferta_lenght ) {
				$denumire_tip_oferta = substr( $this->denumire_tip_oferta, 0, $this->denumire_tip_oferta_lenght );
				if ( strlen( $this->denumire_tip_oferta ) > $this->denumire_tip_oferta_lenght ) $denumire_tip_oferta = $denumire_tip_oferta . "...";
			}
			$denumire_hotel = $nume_hotel;
			if ( !$this->poza1 ) $poza = $GLOBALS['sitepath_class_parinte'] . "images/no_photo.jpg";
			else $poza = $GLOBALS['sitepath_class_parinte'] . 'thumb_hotel/' . $this->poza1;
			if ( !$this->poza1_mare ) $poza_mare = $GLOBALS['sitepath_class_parinte'] . "images/no_photo.jpg";
			else $poza_mare = $GLOBALS['sitepath_class_parinte'] . 'img_mediu_hotel/' . $this->poza1_mare;
			if ( $this->circuit == 'da' ) {
				$this->titlu_dreapta = '&nbsp;';
				$titlu_localizare = 'Continent: <strong>' . $this->denumire_continent . '</strong>';
			}
			else if ( $this->titlu_localizare ) $titlu_localizare = 'Localitate: <strong>' . $this->titlu_localizare . '</strong>';
			else $titlu_localizare = '&nbsp;';

			if ( $this->denumire_tip_oferta ) $poza_fiu = '<img src="' . $GLOBALS['sitepath_class'] . 'images/tematici/mici/' . fa_link( $this->denumire_tip_oferta ) . '.jpg" style="border:1px solid #CCC" alt="' . $this->denumire_tip_oferta . '" title="' . $this->denumire_tip_oferta . '" />';
			else $poza_fiu = '&nbsp;';

			if ( $this->detalii_recomandata ) $detalii_recomandata='<span class="title">'.$this->detalii_recomandata.'</span>';

			if ( $this->pret_recomandata ) $pret_recomandata='<span class="offer">'.$this->pret_recomandata.'</span>';

			if ( $this->lastminute && $this->circuit <> 'da' ) $den = $denumire_oferta;
			else $den = $denumire_hotel;
			$link_localitate = $GLOBALS['sitepath_class'] . 'oferte-' . $this->tip_activ . '/' . fa_link( $this->denumire_tara ) . '/' . fa_link( $this->denumire_zona ) . '/' . fa_link( $this->denumire_localitate ) . '/';
			$link_tara = $GLOBALS['sitepath_class'] . 'oferte-' . $this->tip_activ . '/' . fa_link( $this->denumire_tara ) . '/';
			
			$tipul_mesei = str_replace("'","",$this->masa);
			
			$tpl->replace_tags( array(
				"id_oferta" => $this->id_oferta,
				"denumire_hotel" => $denumire_hotel,
				"denumire_oferta" => $den,
				"den_oferta" => $denumire_oferta,
				"stele" => $stele,
				"poza1" => $poza,
				"poza1_mare" => $poza_mare,
				"denumire_localitate" => $denumire_localitate,
				"denumire_zona" => $denumire_zona,
				"denumire_tara" => $denumire_tara,
				"denumire_transport" => $this->denumire_transport,
				"exprimare_pret" => $this->exprimare_pret,
				"link" => $link,
				"titlu_link" => $this->denumire_oferta,
				"pret_minim" => $pret,
				"pret_old" => $pret_nou,
				"procent_early" => $this->discount . "%",
				"nr_zile_nou" => $dutara1,
				"data_early" => $this->end_early,
				"masa" => $tipul_mesei,
				"titlu_zona" => $zona,
				"titlu_dreapta" => $this->titlu_dreapta,
				"titlu_localizare" => $titlu_localizare,
				"denumire_continent" => $this->denumire_continent,
				"plecare" => $this->detalii_recomandata,
				"detalii_rec" => $detalii_recomandata,
				"pret_recomandata" => $pret_recomandata,
				"nr_nopti" => $nr_nopti,
				"link_localitate" => $link_localitate,
				"link_tara" => $link_tara,
				"plecare" => $this->plecare,
				"tari_vizitate" => $this->tari_vizitate,
				"early" => $early,
				"data_plecare" => $this->date_plecare,
			) );

			$tpl->output_tpl();
		}
		else {
			// echo $this->EarlyBooking;
			/*if ( $this->taxa_aeroport == 'da' )$taxa_aeroport = '<div class="taxa-aeroport NEW-round6px"><img src="' . $GLOBALS['sitepath_class'] . 'images/icon_avion.png" alt="Taxa Aeroport" /> Taxele de aeroport <span>incluse</span></div>';*/
			/*if ( $this->taxa_aeroport == 'da' )$taxa_aeroport = '<div class="taxa-aeroport-long NEW-round6px clearfix"><img src="' . $GLOBALS['sitepath_class'] . 'images/icon_avion_small.png" alt="Taxa Aeroport" /> <span class="first">Taxele de aeroport</span> <span class="second">incluse</span></div>';*/
			if ( $this->EarlyBooking ) $early = '<div class="early-booking NEW-round6px"></div>';
			elseif ( $this->spof ) $early = '<div class="oferta-speciala NEW-round6px"></div>';
			elseif ( $this->lastminute ) $early = '<div class="last-minute NEW-round6px"></div>';

			if ( $this->tplG ) $template = $this->tplG;
			else $template = $_SERVER['DOCUMENT_ROOT'] . "/templates/oferte_grupate.tpl";
			$tpl = new TPL( $template );
			$linkul_h = make_link_oferta($this->denumire_localitate, $this->nume_hotel, NULL, NULL);
			$stele = '<span class="stele-mici-' . $this->nr_stele . '"></span>';
			if ( !$this->poza1 ) $poza = $GLOBALS['sitepath_class_parinte'] . "images/no_photo.jpg";
			else $poza = $GLOBALS['sitepath_class_parinte'] . 'thumb_hotel/' . $this->poza1;
			if ( !$this->poza1_mare ) $poza_mare = $GLOBALS['sitepath_class_parinte'] . "images/no_photo.jpg";
			else $poza_mare = $GLOBALS['sitepath_class_parinte'] . 'img_mediu_hotel/' . $this->poza1_mare;

			$denumire_localitate = $this->denumire_localitate;
			$denumire_localitate = $this->denumire_localitate;
			if ( $this->denumire_localitate_lenght ) {
				$denumire_localitate = substr( $this->denumire_localitate, 0, $this->denumire_localitate_lenght );
				if ( strlen( $this->denumire_localitate ) > $this->denumire_localitate_lenght ) $denumire_localitate = $denumire_localitate . "...";
			}
			if ( trim( strtolower( $this->denumire_localitate ) ) <> trim( strtolower( $this->denumire_zona ) ) ) {
				$denumire_zona = $this->denumire_zona;
				if ( $this->denumire_zona_lenght ) {
					$denumire_zona = substr( $this->denumire_zona, 0, $this->denumire_zona_lenght );
					if ( strlen( $this->denumire_zona ) > $this->denumire_zona_lenght ) $denumire_zona = $denumire_zona . "...";
				}

				$denumire_zona = $denumire_zona;
			}
			else $denumire_zona = '';
			$zona = $this->denumire_zona;
			$denumire_tara = $this->denumire_tara;
			if ( $this->denumire_tara_lenght ) {
				$denumire_tara = substr( $this->denumire_tara, 0, $this->denumire_tara_lenght );
				if ( strlen( $this->denumire_tara ) > $this->denumire_tara_lenght ) $denumire_tara = $denumire_tara . "...";
			}
			$nume_hotel = $this->nume_hotel;
			if ( $this->nume_hotel_lenght ) {
				$nume_hotel = substr( $this->nume_hotel, 0, $this->nume_hotel_lenght );
			}

			$selOf = "SELECT
			oferte.id_oferta,
			oferte.denumire,
			oferte.nr_zile,
			oferte.nr_nopti,
			oferte.masa,
			oferte.pret_minim,
			oferte.moneda,
			oferte.exprimare_pret,
			oferte.cazare,
			oferte.taxa_aeroport,
			oferte.denumire_scurta,
			oferte.oferta_speciala,
			oferte.tip_preturi,
			oferte.last_minute,
			transport.denumire as denumire_transport, ";
			if($this->checkin) $selOf .= " new_prices.data_start AS new_data_start,
			new_prices.pret AS new_price, ";
			$selOf .= " tip_oferta.denumire_tip_oferta,
			oferte_speciale.denumire_tip_oferta AS ofsp_denumire,
			oferte_speciale.descriere_scurta AS ofsp_descriere,
			oferte_speciale.reducere AS ofsp_reducere,
			oferte_speciale.data_inceput_eveniment AS ofsp_start,
			oferte_speciale.data_sfarsit_eveniment AS ofsp_end
			FROM oferte
			INNER JOIN transport ON oferte.id_transport = transport.id_trans
			INNER JOIN hotel_meal ON oferte.id_hotel = hotel_meal.id_hotel
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			LEFT JOIN tip_oferta ON (oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta AND tip_oferta.apare_site = 'da')
			LEFT JOIN tip_oferta AS oferte_speciale ON (oferta_sejur_tip.id_tip_oferta = oferte_speciale.id_tip_oferta AND oferte_speciale.tip = 'oferte_speciale') ";
			if ( $this->filtru['luna_plecare'] ) $selOf = $selOf . " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->filtru['luna_plecare'] . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->filtru['st'] . "' AND data_pret_oferta.data_end >= '" . $this->filtru['sf'] . "'))) ";
			if ( $this->filtru['loc_p_avion'] ) $selOf = $selOf . " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
			INNER JOIN aeroport ON (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport AND aeroport.id_localitate = '" . $this->filtru['loc_p_avion'] . "') ";
			if ( $this->filtru['loc_p_autocar'] ) $selOf = $selOf . " INNER JOIN oferte_transport_autocar ON (oferte.id_oferta = oferte_transport_autocar.id_oferta AND oferte_transport_autocar.id_localitate = '".$this->filtru['loc_p_autocar']."') ";
			if ( $this->filtru['early'] ) $selOf = $selOf . " INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta AND early_booking.end_date >= now() and early_booking.tip = 'sejur') ";
			if($this->checkin) $selOf = $selOf . " INNER JOIN (SELECT oferte.id_oferta, data_pret_oferta.data_start, data_pret_oferta.pret FROM oferte INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta WHERE oferte.valabila = 'da' AND oferte.id_hotel = '".$this->id_hotel."' AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->checkin."' AND data_pret_oferta.data_end >= '".$this->checkin."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ORDER BY ABS(DATEDIFF(data_pret_oferta.data_start, '".$this->checkin."')) ASC, data_pret_oferta.pret ASC) AS new_prices ON new_prices.id_oferta = oferte.id_oferta ";
			$selOf = $selOf . " WHERE oferte.valabila = 'da' ";
			$selOf = $selOf . " AND oferte.id_hotel = '" . $this->id_hotel . "' ";
			if ( $this->recomandata == 'da' ) $selOf = $selOf . " AND oferte.recomandata = 'da' ";
			if ( $this->lastminute ) $selOf = $selOf . " AND oferte.last_minute = 'da' ";
			if ( $this->tipofsp==1 ) $selOf = $selOf . " AND oferte_speciale.tip = 'oferte_speciale' ";
			if ( $this->filtru['id_tip'] ) $selOf = $selOf . " AND oferta_sejur_tip.id_tip_oferta in (" . $this->filtru['id_tip'] . ") ";
			if ( $this->filtru['masa'] ) $selOf = $selOf . " AND LOWER(hotel_meal.masa) IN (".$this->filtru['masa'].") ";
			if ( $this->filtru['id_transport'] ) $selOf = $selOf . " AND oferte.id_transport = '" . $this->filtru['id_transport'] . "' ";
			/*$selOf = $selOf . " GROUP BY oferte.id_oferta
			ORDER BY oferte.recomandata ASC, oferte.nr_zile, oferte.nr_nopti, oferte.pret_minim_lei ";*/
			$selOf = $selOf . " GROUP BY oferte.id_oferta
			ORDER BY oferte.oferta_speciala DESC, oferte.pret_minim ASC ";
			//echo $selOf;
			$queOf = mysql_query( $selOf ) or die( mysql_error() );
			
			$nr_t = 0;
			$i = 0;
			$nr_oferte = mysql_num_rows( $queOf );
			
			while ( $rowOf = mysql_fetch_array( $queOf ) ) {
				$nr_t++;
				$loc = '';
				$cod_plecare = '';
				$oferta_speciala = '';
				$transport = $rowOf['denumire_transport'];
				if($transport == 'Avion') $icon_transport = 'icon_small_transport_avion.png';
				if($transport == 'Autocar') $icon_transport = 'icon_small_transport_autocar.png';
				if($transport == 'Fara transport') $icon_transport = 'icon_small_transport_individual.png';
				if($transport == 'Avion si Autocar local') $icon_transport = 'icon_small_transport_avion_si_autocar_local.png';
				if ( $transport == 'Avion' ) {
					$sel = "SELECT
					GROUP_CONCAT(DISTINCT localitati.denumire ORDER BY oferte_transport_avion.ordonare SEPARATOR ', ') AS loc_plecare,
					GROUP_CONCAT(DISTINCT aeroport.cod ORDER BY oferte_transport_avion.ordonare SEPARATOR ', ') AS cod_plecare
					FROM oferte_transport_avion
					LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
					LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
					WHERE oferte_transport_avion.id_oferta = '" . $rowOf['id_oferta'] . "'
					AND oferte_transport_avion.tip = 'dus' ";
					$que = mysql_query( $sel ) or die( mysql_error() );
					$loc_plecare = mysql_fetch_array( $que );
					@mysql_free_result( $que );
					if($loc_plecare['loc_plecare']) $loc = '<span class="bold black">'.$loc_plecare['loc_plecare'].'</span>';
					if($loc_plecare['cod_plecare']) $cod_plecare = '<span class="plecare bold black" title="Plecare din '.strtoupper($loc_plecare['loc_plecare']).'">'.strtoupper($loc_plecare['cod_plecare']).'</span>';
				}
				if ( $nr_t % 2 == 1 ) $class_tr = 'impar'; else $class_tr = 'par';
				$numar_minim_nopti = '';

				/*if($this->checkin) {
					if($rowOf['tip_preturi']=='plecari') {
						$sel_checkin_price = "SELECT pret, data_start FROM data_pret_oferta WHERE id_oferta = '".$rowOf['id_oferta']."' AND data_start > NOW() AND data_start >= '".$date_min."' AND data_start <= '".$date_max."' ORDER BY ABS(DATEDIFF(data_start, '".$this->checkin."')) ASC, pret ASC LIMIT 0,1 ";
						$que_checkin_price = mysql_query($sel_checkin_price) or die(mysql_error());
						$row_checkin_price = mysql_fetch_array($que_checkin_price);
						
						$new_pret = new_price($row_checkin_price['pret']);
						if($row_checkin_price['data_start']) $date_checkin = date("d.m", strtotime($row_checkin_price['data_start']));
					} else if($rowOf['tip_preturi']=='perioade') {
						$sel_checkin_price = "SELECT pret FROM data_pret_oferta WHERE id_oferta = '".$rowOf['id_oferta']."' AND data_start <= '".$this->checkin."' AND data_end >= '".$this->checkin."' ORDER BY pret ASC LIMIT 0,1 ";
						$que_checkin_price = mysql_query($sel_checkin_price) or die(mysql_error());
						$row_checkin_price = mysql_fetch_array($que_checkin_price);
						
						$new_pret = new_price($row_checkin_price['pret']);
						$date_checkin = date("d.m", strtotime($this->checkin));
					}
				}*/
				if($this->checkin) {
					if($rowOf['tip_preturi']=='plecari') {
						$new_pret = new_price($rowOf['new_price']);
						if($rowOf['new_data_start']) $date_checkin = date("d.m", strtotime($rowOf['new_data_start']));
					} else if($rowOf['tip_preturi']=='perioade') {
						$new_pret = new_price($rowOf['new_price']);
						$date_checkin = date("d.m", strtotime($this->checkin));
					}
				}
				
				if($new_pret=='') {
					if ( $rowOf['pret_minim'] ) {
						$pret = new_price($rowOf['pret_minim']).' '.moneda($rowOf['moneda']);
					} else {
						$pret = pret_minim_sejur( $this->id_oferta, '', '', '' );
						$pret = new_price($pret[0])." ".moneda($pret[1]);
					}
				} else {
					$pret = $new_pret.' '.moneda($rowOf['moneda']);
				}
				
				if($rowOf['ofsp_start']<=date("Y-m-d") and $rowOf['ofsp_end']>=date("Y-m-d")) {
					$pret_nou = explode(" ",$pret);
					$price_old = $pret_nou[0]+$rowOf['ofsp_reducere'].' '.$pret_nou[1];
					$oferta_speciala = '<span class="red bold italic bigger-12em" title="'.$rowOf['ofsp_descriere'].'">Oferta Speciala <i class="icon-info-circled green bigger-11em"></i></span>';
				}
				
				if ( $rowOf['cazare'] == 'nu' ) {
					$den = trim( $rowOf['denumire_scurta'] );
				} else {
					$den = trim( $rowOf['denumire_scurta'] );
					//$den ='Cazare';
					//$zile = 'pe noapte';
				}
				if ( $rowOf['nr_nopti'] > 1 ) {
					$durata = $rowOf['nr_nopti'].' nopti';
				}
				/*else {
					$durata = $rowOf['nr_nopti'].' noapte';
				}*/
				/*$linkul = $GLOBALS['sitepath_class'] . fa_link( $this->denumire_tara ) . "/" . fa_link( $this->denumire_localitate ) . "/" . fa_link_oferta( $rowOf['denumire'] );
				$linkul = $linkul . "-" . $rowOf['id_oferta'] . ".html";*/
				if ( $this->circuit == 'nu' ) {
					$linkul = make_link_oferta($this->denumire_localitate, $nume_hotel, $rowOf['denumire_scurta'], $rowOf['id_oferta']);
				} else {
					$link = make_link_circuit($nume_hotel, $rowOf['id_oferta']);
				}
				
				if($rowOf['taxa_aeroport']=='da') {
					$taxa_aeroport = '<span class="red" title="Taxe aeroport INCLUSE">Taxe INCLUSE</span>';
					$taxa_aeroport2 = '<span class="icons" style="background-image:url(/images/icon_tax_free.png)" title="Taxe aeroport INCLUSE"></span>';
				} else {
					$taxa_aeroport = '';
					$taxa_aeroport2 = '';
				}
				// $oferte=$oferte.'<tr onmouseover="this.className=\'vtip hover\';" onmouseout="this.className=\'vtip '.$class_tr.'\';" class="vtip '.$class_tr.'" title="'.$rowOf['denumire'].'"><td class="text-left"><a href="'.$linkul.'" class="link-blue title" title="'.$rowOf['denumire'].'" rel="nofollow"><strong>'.$den.'</strong> '.$durata.$taxa_aeroport.'</a></td>
				/*$oferte = $oferte.'<tr onmouseover="this.className=\'hover\';" onmouseout="this.className=\''.$class_tr.'\';" class="'.$class_tr.'">
	<td class="text-left"><strong class="black">'.$rowOf['denumire'].'</strong> '.$durata.$taxa_aeroport.'</a></td>
	<td class="text-center">'.$rowOf['masa'].'</td>
	<td class="text-center">'.$rowOf['denumire_transport'].$loc.$taxa_aeroport2.'</td>
	<td class="text-center"><span class="pret">'.$pret.'</span> '.$rowOf['exprimare_pret'].'</td>
	<td class="text-center last" style="vertical-align:middle;"><a href="'.$linkul.'" rel="nofollow"><img src="'.$GLOBALS['sitepath_class'].'images/button_detalii.png" alt="'.$rowOf['denumire'].'" /></a></td>
</tr>';*/
				if(!$date_checkin and $this->checkin) {
					$oferte .= '';
					$nr_t = $nr_t - 1;
					$nr_oferte = $nr_oferte - 1;
				} else {
					$oferte .= '<tr class="pointer ';
					if($oferta_speciala) $oferte .= 'bkg-promo';
						else { if($nr_t%2==0) $oferte .= 'bkg-white'; else $oferte .= 'bkg-grey'; }
					$oferte .= '" data-url="'.$linkul.'" title="'.$rowOf['denumire'].'">';
					$oferte .= '<td class="text-center w120 nowrap"><span class="bold blue">';
					if($oferta_speciala) $oferte .= $oferta_speciala.'<br>';
					if($rowOf['cazare']=='da') {
						$oferte .= /*'Tarif pe noapte'*/$rowOf['denumire_scurta'];
					} else if($rowOf['last_minute']=='da') {
						$oferte .= '<span class="red">LAST MINUTE</span><br>';
					} else {
						$oferte .= 'Sejur ';
						if($date_checkin) $oferte .= '<span class="green bigger-12em">'.$date_checkin.'</span> - ';
					}
					$oferte .= $durata.'</span>';
					if($rowOf['denumire_tip_oferta']) $oferte .= '<span class="block smaller-09em italic bold green">'.$rowOf['denumire_tip_oferta'].'</span>';
					$oferte .= '</td>';
					$oferte .= '<td class="text-center">';
					if($rowOf['cazare']=='da') $oferte .= $this->mese_hotel; else $oferte .= $rowOf['masa'];
					$oferte .= '</td>';
					$oferte .= '<td class="text-center w120 nowrap">';
					$oferte .= '<span class="icons transport" style="background-image:url(/images/oferte/'.$icon_transport.')';
					if($transport == 'Avion si Autocar local') $oferte .= '; width:70px !important;';
					$oferte .= '" title="Transport '.strtoupper(str_replace("Fara transport", "Individual", $transport)).'"></span>';
					if($transport == 'Fara transport') $oferte .= '<strong class="blue">Individual</strong>';
					$oferte .= $cod_plecare.''.$taxa_aeroport2;
					$oferte .= '</td>';
					$oferte .= '<td class="text-center w140 nowrap">';
					if($price_old > $pret) $oferte .= '<span class="pret-old bold">&nbsp;'.$price_old.'&nbsp;</span>';
					$oferte .= '<span class="pret red">'.$pret.'</span>'.$rowOf['exprimare_pret'].'</td>';
					$oferte .= '<td class="text-center w120"><a href="'.$linkul.'"><img src="/images/but_nou_vezi_detalii.png" alt="Vezi detalii" style="height:25px;"></a></td>';
					$oferte .= '</tr>';
				}
				
				if($this->comentariul_nostru) {
					$comentariu = $this->comentariul_nostru;
				} else if($this->descriere_hotel_tez) {
					$comentariu = truncate_str($this->descriere_hotel_tez, 270).' ...';
				} else if($this->descriere_hotel) {
					$comentariu = truncate_str($this->descriere_hotel, 270).' ...';
				}

				if($this->nr_comentarii>0 and $this->nota_total_medie>0) {
					$nota_review = '<div class="float-left w260"><a href="'.$linkul.'#comentarii" rel="nofollow" class="rating text-left NEW-round4px clearfix"><div class="nota_img"><div class="inner" style="width:'.($this->nota_total_medie*10).'px;"></div></div> &nbsp; <span class="bold blue bigger-11em">'.round($this->nota_total_medie, 2).' / 10</span> - '.$this->nr_comentarii.' voturi</a></div>';
				} else {
					$nota_review = '<div class="float-left w260"><a href="'.$linkul.'#comentarii" rel="nofollow" class="rating text-left NEW-round4px"><span class="red bold underline">Adauga comentariu</span></a></div>';
				}
			}
			
			@mysql_free_result( $queOf );
			//if ( $nr_oferte > 1 ) 
			$linkul_h1 = '<a href="' . $linkul_h . '" title="' . $nume_hotel . '" class="link-blue">' . $nume_hotel . '</a>';
			//else {
				//$linkul_h1 = '<a href="' . $linkul . '" class="link-blue">' . $nume_hotel . '</a>';
				//$linkul_h = $linkul;
			//}
			
			if($nr_oferte>1) $linkul_h = $linkul_h; else $linkul_h = $linkul;
			
			$class_tabel = 'search-rooms';
			
			if($this->concept) $recomandat_pentru = '<div class="concept">Recomandat pentru: <span class="bigger-12em blue bold">'.$this->concept.'</span></div>';
			
			$_SESSION['offer'][$this->id_hotel] = $oferte;
			
			$tpl->replace_tags( array(
				"denumire_hotel" => $nume_hotel,
				"id_hotel" => $this->id_hotel,
				"denumire_zona" => $denumire_zona,
				"denumire_localitate" => $denumire_localitate,
				"stele" => $stele,
				"poza1" => $poza,
				"poza1_mare" => $poza_mare,
				//"oferte" => $_SESSION['offer'][$this->id_hotel],
				//"oferte" => $oferte,
				"titlu_link" => 'Oferte ' . $nume_hotel,
				"link" => $linkul_h1,
				"link_h" => $linkul_h,
				"early" => $early,
				"taxa_aeroport" => $taxa_aeroport,
				"class_tabel" => $class_tabel,
				"comentariu" => $comentariu,
				"nota_review" => $nota_review,
				"titlu_dreapta" => $this->titlu_dreapta,
				"recomandat_pentru" => $recomandat_pentru
			) );

			$tpl->output_tpl();
		}
	}
}

class AFISARE_SEJUR_NORMAL
{
	function __construct() // se creaza paginatia_______________________________________________
	{
		$this->tip_oferta = 0;
		$this->tari = 0;
		$this->zone = 0;
		$this->azi = date( "Y-m-d" );
		$this->last_minute = 0;
		$this->random = 0;
		$this->afiseaza_banner = 'da';
		$this->oferta_speciala = 0;
		$this->pagini_id_uri = "";
		$this->afisare_no_limit = 0;
		$this->early = 'nu';
		$this->cazare = 'nu';
		$this->oferte_pagina = 0;
		$this->templates = "afisare_oferta_lunga.tpl";
		$this->denumire_tip_oferta = '';
		$this->denTara = '';
		$this->denZone = '';
		$this->denTransport = '';
		$this->denOrase = '';
		$this->dimensiuneNumeHotel = '';
		$this->tip_oferta1 = '';
		$this->tipofsp = 0;
		$nr_tot = 0;
		$this->set_circuit = '';
		$this->tari_circ = '';
		$this->id_tara_circuit = '';
		$this->last_minute = 0;
		$this->search = '';
		$this->nr_aparitiiP = 2;
		$this->arrF = array();
		$this->filtruA = array();
		$this->recomandata = '';
		$this->trans_nou = array();
		$this->distanta = array();
	}
	// funtii pentru sortarea ofertelor__________________________
	function set_oferte_pagina($value) {
		$this->oferte_pagina = $value;
	}
	function set_cazare($value) {
		$this->cazare = $value;
	}
	function setRecomandata($value) {
		$this->recomandata = $value;
	}
	function setEarlyBooking($value) {
		$this->early_booking = $value;
	}
	function setLastMinute() {
		$this->last_minute = 1;
	}
	function setRandom() {
		$this->random = 1;
	}
	function setBannere($value) {
		$this->afiseaza_banner = $value;
	}
	function setIdFav($value) {
		$this->favorite_id = $value;
	}
	function setTipOferta($tip) {
		$id_parinte = get_id_tip_sejur( desfa_link( $tip ) );
		$this->tip_oferta = "'" . $id_parinte . "'";
		$this->denumire_tip_oferta = $tip;
		$this->tip_oferta1 = get_id_tip_sejur_fii( $id_parinte );
		if ( $this->tip_oferta1['id_tipuri'] ) $this->tip_oferta = $this->tip_oferta . ',' . $this->tip_oferta1['id_tipuri'];
		$this->filtruA['id_tip'] = $this->tip_oferta;
	}
	function setTipOfSp() {
		$this->tipofsp = 1;
	}
	function setKeywords( $keyword_old ) {
		if ( strlen( $keyword_old ) > 2 ) $this->keywords = $keyword_old;
	}
	function setCautaHotel( $value ) {
		if ( strlen( $value ) > 2 ) $this->cautahotel = $value;
	}
	function setTari( $tari ) {
		$this->id_tara = get_id_tara( desfa_link( $tari ) );
		$this->denTara = $tari;
		$this->tari = "'" . $this->id_tara . "'";
	}
	function setTariCircuit( array $tari ) {
		foreach( $tari as $key => $value )
		{
			if ( $value )
			{
				$this->id_tara_circuit = $this->id_tara_circuit . "'" . get_id_tara( desfa_link( $value ) ) . "',";
			}
		}
		$this->id_tara_circuit = substr( $this->id_tara_circuit, 0, - 1 );
	}
	function setZone( $zone ) {
		$this->id_zone = get_id_zona( desfa_link( $zone ), $this->id_tara );
		$this->zone = "'" . $this->id_zone . "'";
		$this->denZone = $zone;
	}
	function setOrase( $orase ) {
		$this->orase = "'" . get_id_localitate( desfa_link( $orase ), $this->id_zone ) . "'";
		$this->denOrase = $orase;
	}
	function setOfertaSpeciala( $value ) {
		$this->oferta_speciala = $value;
	}
	function setIduri( $value ) {
		$i = 0;
		$this->iduri = "'" . $value[$i] . "'";
		$i++;
		while ( $value[$i] )
		{
			$this->iduri = $this->iduri . ", '" . $value[$i] . "'";
			$i++;
		}
	}
	function setTransport( $value ) {
		$id_transport=get_id_transport(desfa_link($value));
		$this->transport=$id_transport;
		$this->filtruA['id_transport']=$id_transport;
		$this->denTransport=$value;
	}
	function setIdHotel( $value ) {
		$this->id_hotel = $value;
	}
	function setEarly( $value ) {
		$this->early = $value;
		$this->filtruA['early'] == 'da';
	}
	function setMasa( $value ) {
		$valueM=explode(',',desfa_link($value));
		$this->masa="'".implode("', '",$valueM)."'";
		//print_r($this->masa);
		//$this->masa = desfa_link( $value );
		$this->filtruA['masa'] = $this->masa;
	}
	function setConcept( $value ) {
		$valueC=explode(',',desfa_link($value));
		$this->concept="'".implode("', '",$valueC)."'";
		//$this->concept = desfa_link( $value );
		$this->filtruA['concept'] = $this->concept;
	}
	function setFacilitati( $value ) {
		$valueF = explode(',', desfa_link($value));
		$this->facilitati = "'".implode("', '", $valueF)."'";
		$this->filtruA['facilitati'] = $this->facilitati;
	}
	function setOrdonarePret( $value ) {
		$this->ord_pret = $value;
	}
	function setOrdonareStele( $value ) {
		$this->ord_stele = $value;
	}
	function setOrdonareRelevanta( $value ) {
		$this->ord_relevanta = $value;
	}
	function setOrdonareNumeH( $value ) {
		$this->ord_numeH = $value;
	}
	function setStele( $value ) {
		$this->stele = $value;
	}
	function set_oferte_vizitate( $value ) {
		$this->oferte_vizitate = $value;
	}
	function setDimensiuneNumeHotel( $value ) {
		$this->dimensiuneNumeHotel = $value;
	}
	function setCircuit( $value ) {
		$this->set_circuit = $value;
	}
	function setExceptie( $value ) {
		$this->set_exceptie = $value;
	}
	function setExceptieH( $value ) {
		$this->set_exceptieH = $value;
	}
	function setContinent( $value ) {
		$this->continent = "'" . get_id_continent( desfa_link( $value ) ) . "'";
		$this->denContinent = $value;
	}
	function setTaraCircuit( $value ) {
		$this->tari_circ = "'" . get_id_tara( desfa_link( $value ) ) . "'";
		$this->denTaraCircuit = $value;
	}
	function setDataPlecarii( $value ) {
		$this->data_plecarii = $value;
	}
	function setPlecare( $value ) {
		$this->plecare = $value;
	}
	function setPlecareAvion( $value ) {
		$this->loc_p_avion = $value;
		$this->filtruA['loc_p_avion'] = $value;
	}
	function setPlecareAutocar( $value ) {
		$this->loc_p_autocar = $value;
		$this->filtruA['loc_p_autocar'] = $value;
	}
	function setStartDate( $value ) {
		$this->start = $value;
	}
	function setEndDate( $value ) {
		$this->endd = $value;
	}
	function setSearch( $val ) {
		$this->search = $val;
	}
	function setDistanta( array $val ) {
		$this->distanta = $val;
	}
	function setCheckIn( $value ) {
		$this->check_in = date("Y-m-d", strtotime($value));
		$this->date_min = date("Y-m-d", strtotime($this->check_in." - 4 days"));
		$this->date_max = date("Y-m-d", strtotime($this->check_in." + 4 days"));
	}
	function setLunaPlecare( $value ) {
		$this->luna_plecare = $value;
		$lun = explode( '-', $value );
		$this->st = $lun[1] . '-' . $lun[0] . '-31';
		$this->sf = $lun[1] . '-' . $lun[0] . '-01';
		$this->filtruA['st'] = $this->st;
		$this->filtruA['sf'] = $this->sf;
		$this->filtruA['luna_plecare'] = $this->luna_plecare;
	}
	function setDurata( $value ) {
		$this->durata = $value;
	}
	function afisareOferte( $value ) {
		$this->afisare_oferte = $value;
	}


	function sel_toate_nr() {
		$sel_nr = "SELECT
		oferte.id_oferta,
		oferte.id_hotel,
		hoteluri.stele,
		transport.denumire AS denumire_transport,
		transport.id_trans,
		oferte.masa,
		oferte.nr_zile,
		oferte.nr_nopti,
		hoteluri.distanta_fata_de,
		hoteluri.distanta, ";
		if ( $this->facilitati ) $sel_nr = $sel_nr . "facilities.facilitati, ";
		$sel_nr = $sel_nr . "hoteluri.concept
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_nr = $sel_nr . " inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->loc_p_avion ) $sel_nr = $sel_nr . "inner Join oferte_transport_avion on oferte.id_oferta = oferte_transport_avion.id_oferta
inner Join aeroport on (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport and aeroport.id_localitate = '" . $this->loc_p_avion . "') ";
		if ( $this->loc_p_autocar ) $sel_nr = $sel_nr . " INNER JOIN oferte_transport_autocar ON (oferte.id_oferta = oferte_transport_autocar.id_oferta AND oferte_transport_autocar.id_localitate = '".$this->loc_p_autocar."') ";
		if ( $this->luna_plecare ) $sel_nr = $sel_nr . " inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and ((oferte.tip_preturi = 'plecari' and date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') or (oferte.tip_preturi = 'perioade' and data_pret_oferta.data_start <='" . $this->st . "' and data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_nr = $sel_nr . " inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.tip = 'sejur') ";
		if ( $this->start ) $sel_nr = $sel_nr . " inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and (data_pret_oferta.data_start<='" . $this->start . "' and data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_nr = $sel_nr . "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans
		LEFT JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		LEFT JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment) ";
		if ( $this->data_plecarii ) $sel_nr = $sel_nr . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari') ";
		if ( $this->tari_circ ) $sel_nr = $sel_nr . " Left Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_nr = $sel_nr . " Inner Join traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte and traseu_circuit.id_tara in (" . $this->id_tara_circuit . ") and traseu_circuit.tara_principala = 'da') ";
		if ( $this->facilitati ) $sel_nr = $sel_nr . " LEFT JOIN (SELECT GROUP_CONCAT(lower(caracteristici_hotel.denumire) SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if($this->check_in) $sel_nr = $sel_nr . " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_nr = $sel_nr . " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_nr = $sel_nr . " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_nr = $sel_nr . " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_nr = $sel_nr . " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_nr = $sel_nr . " AND oferte.last_minute = 'da' ";
		if ( $this->data_plecarii ) $sel_nr = $sel_nr . " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->cazare == 'da' ) $sel_nr = $sel_nr . " AND oferte.cazare = 'da' ";
		if ( $this->masa ) $sel_nr = $sel_nr . " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_nr = $sel_nr . " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->continent ) $sel_nr = $sel_nr . " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_nr = $sel_nr . " AND oferta_sejur_tip.id_tip_oferta in (" . $this->tip_oferta . ") ";
		if ( $this->tipofsp==1 ) $sel_nr = $sel_nr . " AND ofsp2.tip = 'oferte_speciale' ";
		if ( $this->tari && !$this->tari_circ ) $sel_nr = $sel_nr . " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_nr = $sel_nr . " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_nr = $sel_nr . " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_nr = $sel_nr . " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_nr = $sel_nr . " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_nr = $sel_nr . " and oferte.recomandata = 'da' ";
		if ( $this->transport ) $sel_nr = $sel_nr . " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->id_hotel ) $sel_nr = $sel_nr . " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->stele ) $sel_nr = $sel_nr . " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->concept ) $sel_nr = $sel_nr . " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_nr = $sel_nr . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		if ( $this->set_exceptieH ) $sel_nr = $sel_nr . " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->durata ) $sel_nr = $sel_nr . " AND oferte.nr_zile IN (" . $this->durata . ") ";
		if ( $this->cautahotel ) {
			$sel_nr = $sel_nr . "\n AND ( localitati.denumire LIKE '%" . $this->cautahotel . "%' ";
			$sel_nr = $sel_nr . "\n OR zone.denumire LIKE '%" . $this->cautahotel . "%' ";
			$sel_nr = $sel_nr . "\n OR tari.denumire LIKE '%" . $this->cautahotel . "%' )";
		}
		if ( $this->keywords ) {
			$sel_nr = $sel_nr . "\n AND ( localitati.denumire LIKE '%" . $this->keywords . "%' ";
			$sel_nr = $sel_nr . "\n OR zone.denumire LIKE '%" . $this->keywords . "%' ";
			$sel_nr = $sel_nr . "\n OR tari.denumire LIKE '%" . $this->keywords . "%' )";
		}
		//echo $sel_nr;
		return $sel_nr;
	}


	function sel_toate() {
		if ( $this->cautahotel ) {
			$sel_toate = "SELECT
			count(DISTINCT oferte.id_oferta) as nr_normale,
			sum(if(oferte.taxa_aeroport='da','1','0')) as taxa_aeroport,
			max(oferte.data_adaugarii) as ultima_adaugare,
			oferte.id_oferta,
			oferte.denumire AS denumire_oferta,
			oferte.denumire_scurta AS denumire_oferta_scurta,
			oferte.exprimare_pret,
			oferte.nr_zile,
			oferte.nr_nopti,
			oferte.masa,
			oferte.last_minute,
			hoteluri.id_hotel,
			hoteluri.poza1,
			hoteluri.nume as denumire_hotel,
			hoteluri.stele,
			hoteluri.concept,
			hoteluri.detalii_concept,
			hoteluri.new_descriere,
			hoteluri.descriere as descriere_hotel,
			(SELECT AVG(reviews.nota_total) FROM reviews WHERE reviews.id_hotel = hoteluri.id_hotel AND reviews.activ = 'da' GROUP BY reviews.id_hotel) AS nota_total_medie,
			(SELECT COUNT(reviews.id) FROM reviews WHERE reviews.id_hotel = hoteluri.id_hotel AND reviews.activ = 'da' GROUP BY reviews.id_hotel) AS nr_comentarii,
			continente.nume_continent,
			hoteluri.tip_unitate,
			tari.denumire as denumire_tara,
			zone.denumire as denumire_zona,
			zone.apare_templait_dreapta,
			localitati.denumire as denumire_localitate,
			oferte.pret_minim,
			oferte.moneda,
			oferte.detalii_recomandata,
			oferte.pret_recomandata,
			oferte.recomandata, ";
			if ( $this->start ) $sel_toate = $sel_toate . "
			preturi.pret as pret_minim,
			preturi.moneda, ";
			if ( $this->set_circuit ) $sel_toate = $sel_toate . " plec.plecare,
			traseu.tari_vizitate, ";
			if ( $this->last_minute ) $sel_toate = $sel_toate . " data_plecarii.data_start AS data_plecare, ";
			if ( $this->tip_oferta ) $sel_toate = $sel_toate . " tip_oferta.denumire_tip_oferta,
			tip_oferta.parinte, ";
			$sel_toate = $sel_toate . " early.discount,
			date_format(early.end_date, '%d/%m/%Y')  as early_end,
			transport.denumire as denumire_transport,
			facilities.facilitati
			FROM oferte ";
			if ( $this->tip_oferta ) $sel_toate = $sel_toate . " inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta inner join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta ";
			if ( $this->loc_p_avion ) $sel_toate = $sel_toate . "inner Join oferte_transport_avion on oferte.id_oferta = oferte_transport_avion.id_oferta
	inner Join aeroport on (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport and aeroport.id_localitate = '" . $this->loc_p_avion . "') ";
			if ( $this->loc_p_autocar ) $sel_toate = $sel_toate . " INNER JOIN oferte_transport_autocar ON (oferte.id_oferta = oferte_transport_autocar.id_oferta AND oferte_transport_autocar.id_localitate = '".$this->loc_p_autocar."') ";
			if ( $this->luna_plecare ) $sel_toate = $sel_toate . " inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and ((oferte.tip_preturi = 'plecari' and date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') or (oferte.tip_preturi = 'perioade' and data_pret_oferta.data_start <='" . $this->st . "' and data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
			$sel_toate = $sel_toate . " Left join (select * from early_booking where early_booking.end_date >= now() and early_booking.tip = 'sejur' Group by id_oferta, end_date, discount Order by end_date) as early on oferte.id_oferta = early.id_oferta ";
			if ( $this->start ) $sel_toate = $sel_toate . " inner join(select data_pret_oferta.pret, data_pret_oferta.moneda, data_pret_oferta.id_oferta from data_pret_oferta where data_pret_oferta.data_start<='" . $this->start . "' and data_pret_oferta.data_end>='" . $this->endd . "' Group by data_pret_oferta.id_oferta Order by data_pret_oferta.pret) as preturi on oferte.id_oferta = preturi.id_oferta ";
			$sel_toate = $sel_toate . " inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
			inner Join zone ON localitati.id_zona = zone.id_zona
			inner Join tari ON zone.id_tara = tari.id_tara
			left join continente on hoteluri.id_continent = continente.id_continent
			inner join transport on oferte.id_transport = transport.id_trans ";
			if ( $this->set_circuit ) $sel_toate = $sel_toate . "left join (select GROUP_CONCAT(DISTINCT date_format(data_start, '%M') Order by data_start SEPARATOR ', ') as plecare, id_oferta from data_pret_oferta Group by id_oferta) as plec on oferte.id_oferta = plec.id_oferta
			left join (select GROUP_CONCAT(DISTINCT tari.denumire Order by tari.denumire ASC SEPARATOR ', ') as tari_vizitate, traseu_circuit.id_hotel_parinte from traseu_circuit inner join tari on traseu_circuit.id_tara = tari.id_tara where traseu_circuit.tara_principala = 'da' Group by traseu_circuit.id_hotel_parinte) as traseu on hoteluri.id_hotel = traseu.id_hotel_parinte ";
			if ( $this->last_minute ) $sel_toate = $sel_toate . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
			if ( $this->tari_circ ) $sel_toate = $sel_toate . "Left Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
			if ( $this->id_tara_circuit ) $sel_toate = $sel_toate . " Inner Join traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte and traseu_circuit.id_tara in (" . $this->id_tara_circuit . ") and traseu_circuit.tara_principala = 'da') ";
			$sel_toate = $sel_toate . " LEFT JOIN (SELECT GROUP_CONCAT(lower(caracteristici_hotel.denumire) SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
			if($this->check_in) $sel_toate = $sel_toate . " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
			$sel_toate = $sel_toate . " WHERE oferte.valabila = 'da' ";
			if($this->check_in) $sel_toate = $sel_toate . " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
			if ( !$this->search && !$this->last_minute && !$this->recomandata )
			{
				if ( !$this->set_circuit ) $sel_toate = $sel_toate . " and hoteluri.tip_unitate <> 'Circuit' ";
				else $sel_toate = $sel_toate . " and hoteluri.tip_unitate = 'Circuit' ";
			}
			if ( $this->cazare == 'da' ) $sel_toate = $sel_toate . " and oferte.cazare = 'da' ";
			if ( $this->last_minute ) $sel_toate = $sel_toate . " and oferte.last_minute = 'da' ";
			if ( $this->data_plecarii ) $sel_toate = $sel_toate . " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
			if ( $this->early == 'da' ) $sel_toate = $sel_toate . " and early.discount is not null";
			if ( $this->recomandata ) $sel_toate = $sel_toate . " and oferte.recomandata = 'da' ";
			if ( $this->tari && !$this->tari_circ ) $sel_toate = $sel_toate . " AND tari.id_tara IN (" . $this->tari . ")";
			if ( $this->id_hotel ) $sel_toate = $sel_toate . " AND hoteluri.id_hotel  ='" . $this->id_hotel . "' ";
			if ( $this->random ) $sel_toate = $sel_toate . " AND hoteluri.poza1 is not null ";
			if ( $this->set_exceptie ) $sel_toate = $sel_toate . " AND oferte.id_oferta <> '" . $this->set_exceptie . "' ";
			if ( $this->set_exceptieH ) $sel_toate = $sel_toate . " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
			$sel_toate = $sel_toate . "\n AND ( localitati.denumire LIKE '%" . $this->cautahotel . "%' ";
			$sel_toate = $sel_toate . "\n OR zone.denumire LIKE '%" . $this->cautahotel . "%' ";
			$sel_toate = $sel_toate . "\n OR hoteluri.nume LIKE '%" . $this->cautahotel . "%' ";
			$sel_toate = $sel_toate . "\n OR oferte.denumire LIKE '%" . $this->cautahotel . "%' ";
			$sel_toate = $sel_toate . "\n OR tari.denumire LIKE '%" . $this->cautahotel . "%' )";
			if ( !$this->last_minute && !$this->afisare_oferte )
				$sel_toate = $sel_toate . " Group by oferte.last_minute, oferte.id_hotel ";
			else $sel_toate = $sel_toate . " Group by oferte.id_oferta";
			if ( $this->ord_stele ) $sel_toate = $sel_toate . " ORDER BY hoteluri.stele " . $this->ord_stele . " ";
			elseif ( $this->ord_relevanta ) $sel_toate = $sel_toate . " ORDER BY oferte.click " . $this->ord_relevanta . " ";
			elseif ( $this->ord_pret ) $sel_toate = $sel_toate . " ORDER BY (oferte.pret_minim_lei / oferte.nr_nopti) " . $this->ord_pret . " ";
			elseif ( $this->ord_numeH ) $sel_toate = $sel_toate . " ORDER BY hoteluri.nume " . $this->ord_numeH . " ";
			elseif ( $this->random ) $sel_toate = $sel_toate . " ORDER BY RAND() ";
			/*elseif ( $_SERVER['PHP_SELF']=='/tara_new1.php') $sel_toate = $sel_toate . " ORDER BY oferte.pret_minim_lei ASC ";*/
			elseif ( $_SERVER['PHP_SELF']=='/tara_new1.php') $sel_toate = $sel_toate . "  ORDER BY hoteluri.oferta_speciala DESC, hoteluri.counter DESC, oferte.ultima_modificare DESC ";
			elseif ( $this->last_minute ) $sel_toate = $sel_toate . " ORDER BY data_plecare ASC ";
			/*else $sel_toate = $sel_toate . " ORDER BY recomandata, oferte.pret_minim_lei ASC ";*/
			else $sel_toate = $sel_toate . "  ORDER BY hoteluri.oferta_speciala DESC, hoteluri.counter DESC, oferte.ultima_modificare DESC ";
			if ( $this->nr_oferte_per_pagina ) $sel_toate = $sel_toate . " LIMIT " . $this->from . ", " . $this->nr_oferte_per_pagina . " ";
	
	
		} else {
	
	
			$sel_toate = "SELECT
			COUNT(DISTINCT oferte.id_oferta) AS nr_normale,
			SUM(IF(oferte.taxa_aeroport='da','1','0')) AS taxa_aeroport,
			MAX(oferte.data_adaugarii) AS ultima_adaugare,
			oferte.id_oferta,
			oferte.denumire AS denumire_oferta,
			oferte.denumire_scurta AS denumire_oferta_scurta,
			oferte.exprimare_pret,
			oferte.nr_zile,
			oferte.nr_nopti,
			oferte.masa,
			oferte.last_minute,
			hoteluri.id_hotel,
			hoteluri.poza1,
			hoteluri.nume AS denumire_hotel,
			hoteluri.stele,
			hoteluri.concept,
			hoteluri.detalii_concept,
			hoteluri.new_descriere,
			hoteluri.descriere AS descriere_hotel,
			(SELECT AVG(reviews.nota_total) FROM reviews WHERE reviews.id_hotel = hoteluri.id_hotel AND reviews.activ = 'da' GROUP BY reviews.id_hotel) AS nota_total_medie,
			(SELECT COUNT(reviews.id) FROM reviews WHERE reviews.id_hotel = hoteluri.id_hotel AND reviews.activ = 'da' GROUP BY reviews.id_hotel) AS nr_comentarii,
			continente.nume_continent,
			hoteluri.tip_unitate,
			tari.denumire AS denumire_tara,
			zone.denumire AS denumire_zona,
			zone.apare_templait_dreapta,
			localitati.denumire AS denumire_localitate,
			oferte.pret_minim,
			oferte.moneda,
			oferte.detalii_recomandata,
			oferte.pret_recomandata,
			oferte.recomandata, ";
			if ( $this->start ) $sel_toate = $sel_toate . "
			preturi.pret AS pret_minim,
			preturi.moneda, ";
			if ( $this->set_circuit ) $sel_toate = $sel_toate . " plec.plecare,
			traseu.tari_vizitate, ";
			/*if ( $this->last_minute ) $sel_toate = $sel_toate . " plecam.date_plecare, ";*/
			/*if ( $this->data_plecarii ) $sel_toate = $sel_toate . " plecarii.data_plecarii, ";*/
			if ( $this->last_minute ) $sel_toate = $sel_toate . " data_plecarii.data_start AS data_plecare, ";
			if ( $this->tip_oferta ) $sel_toate = $sel_toate . " tip_oferta.denumire_tip_oferta,
			tip_oferta.parinte, ";
			if ( $this->check_in ) $sel_toate = $sel_toate . " new_prices.data_start AS new_data_start,
			new_prices.pret AS new_price, ";
			$sel_toate = $sel_toate . " early.discount,
			date_format(early.end_date, '%d/%m/%Y') AS early_end,
			transport.denumire AS denumire_transport,
			ofsp2.denumire_tip_oferta,
			ofsp2.tip,
			facilities.facilitati
			FROM oferte ";
			if ( $this->tip_oferta ) $sel_toate = $sel_toate . " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta ";
			if ( $this->loc_p_avion ) $sel_toate = $sel_toate . " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
			INNER JOIN aeroport ON (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport AND aeroport.id_localitate = '" . $this->loc_p_avion . "') ";
			if ( $this->loc_p_autocar ) $sel_toate = $sel_toate . " INNER JOIN oferte_transport_autocar ON (oferte.id_oferta = oferte_transport_autocar.id_oferta AND oferte_transport_autocar.id_localitate = '".$this->loc_p_autocar."') ";
			if ( $this->luna_plecare ) $sel_toate = $sel_toate . " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
			$sel_toate = $sel_toate . " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
			if ( $this->start ) $sel_toate = $sel_toate . " INNER JOIN(SELECT data_pret_oferta.pret, data_pret_oferta.moneda, data_pret_oferta.id_oferta FROM data_pret_oferta WHERE data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "' GROUP BY data_pret_oferta.id_oferta ORDER BY data_pret_oferta.pret) AS preturi ON oferte.id_oferta = preturi.id_oferta ";
			$sel_toate = $sel_toate . " INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			INNER JOIN tari ON zone.id_tara = tari.id_tara
			LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
			INNER JOIN transport ON oferte.id_transport = transport.id_trans
			LEFT JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
			LEFT JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment) ";
			if ( $this->set_circuit ) $sel_toate = $sel_toate . " LEFT JOIN (SELECT GROUP_CONCAT(DISTINCT date_format(data_start, '%M') ORDER BY data_start SEPARATOR ', ') AS plecare, id_oferta FROM data_pret_oferta WHERE data_start>now() GROUP BY id_oferta) AS plec ON oferte.id_oferta = plec.id_oferta
			LEFT JOIN (SELECT GROUP_CONCAT(DISTINCT tari.denumire ORDER BY tari.denumire ASC SEPARATOR ', ') AS tari_vizitate, traseu_circuit.id_hotel_parinte FROM traseu_circuit INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara WHERE traseu_circuit.tara_principala = 'da' GROUP BY traseu_circuit.id_hotel_parinte) AS traseu ON hoteluri.id_hotel = traseu.id_hotel_parinte ";
			/*if ( $this->last_minute ) $sel_toate = $sel_toate . " LEFT JOIN (SELECT GROUP_CONCAT(DISTINCT date_format(data_start, '%d %M') ORDER BY data_start SEPARATOR ', ') AS date_plecare, id_oferta FROM data_pret_oferta WHERE data_start>now() GROUP BY id_oferta) AS plecam ON oferte.id_oferta = plecam.id_oferta ";*/
			/*if ( $this->data_plecarii ) $sel_toate = $sel_toate . " LEFT JOIN (SELECT GROUP_CONCAT(DISTINCT data_start ORDER BY data_start SEPARATOR ', ') AS data_plecarii, id_oferta FROM data_pret_oferta WHERE data_start>now() GROUP BY id_oferta) AS plecarii ON oferte.id_oferta = plecarii.id_oferta ";*/
			if ( $this->last_minute ) $sel_toate = $sel_toate . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
			if ( $this->tari_circ ) $sel_toate = $sel_toate . " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
			if ( $this->id_tara_circuit ) $sel_toate = $sel_toate . " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
			$sel_toate = $sel_toate . " LEFT JOIN (SELECT GROUP_CONCAT(lower(caracteristici_hotel.denumire) SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
			if($this->check_in) $sel_toate = $sel_toate . " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
			if($this->check_in) $sel_toate = $sel_toate . " LEFT JOIN (SELECT oferte.id_oferta, data_pret_oferta.data_start, data_pret_oferta.pret FROM oferte INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta WHERE oferte.valabila = 'da' AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ORDER BY ABS(DATEDIFF(data_pret_oferta.data_start, '".$this->check_in."')) ASC, data_pret_oferta.pret ASC) AS new_prices ON new_prices.id_oferta = oferte.id_oferta ";
			$sel_toate = $sel_toate . " WHERE oferte.valabila = 'da' ";
			if($this->check_in) $sel_toate = $sel_toate . " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
			if ( !$this->search && !$this->last_minute )
			{
				if ( !$this->set_circuit ) $sel_toate = $sel_toate . " AND hoteluri.tip_unitate <> 'Circuit' ";
				else $sel_toate = $sel_toate . " AND hoteluri.tip_unitate = 'Circuit' ";
			}
			if ( $this->cazare == 'da' ) $sel_toate = $sel_toate . " AND oferte.cazare = 'da' ";
			if ( $this->last_minute ) $sel_toate = $sel_toate . " AND oferte.last_minute = 'da' ";
			if ( $this->data_plecarii ) $sel_toate = $sel_toate . " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
			if ( $this->early == 'da' ) $sel_toate = $sel_toate . " AND early.discount is not null";
			if ( $this->continent ) $sel_toate = $sel_toate . " AND hoteluri.id_continent = " . $this->continent . " ";
			if ( $this->masa ) $sel_toate = $sel_toate . " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
			if ( $this->distanta ) $sel_toate = $sel_toate . " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
			if ( $this->tip_oferta ) $sel_toate = $sel_toate . " AND oferta_sejur_tip.id_tip_oferta in (" . $this->tip_oferta . ") ";
			if ( $this->tipofsp==1 ) $sel_toate = $sel_toate . " AND ofsp2.tip = 'oferte_speciale' ";
			if ( $this->stele ) $sel_toate = $sel_toate . " AND hoteluri.stele IN (" . $this->stele . ") ";
			if ( $this->concept ) $sel_toate = $sel_toate . " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
			if ( $this->facilitati ) {
				$facilities = explode(",", $this->facilitati);
				foreach($facilities as $k_facilities => $v_facilities) {
					$v_facilities = str_replace("'", "", trim($v_facilities));
					$sel_toate = $sel_toate . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
				}
			}
			if ( $this->tari && !$this->tari_circ ) $sel_toate = $sel_toate . " AND tari.id_tara IN (" . $this->tari . ")";
			if ( $this->tari_circ && !$this->tari ) $sel_toate = $sel_toate . " AND traseu_circuit.id_tara = " . $this->tari_circ . " and traseu_circuit.tara_principala = 'da' ";
			if ( $this->tari && $this->tari_circ ) $sel_toate = $sel_toate . " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
			if ( $this->zone ) $sel_toate = $sel_toate . " AND zone.id_zona IN (" . $this->zone . ")";
			if ( $this->recomandata ) $sel_toate = $sel_toate . " AND oferte.recomandata = 'da' ";
			if ( $this->orase ) $sel_toate = $sel_toate . " AND localitati.id_localitate IN (" . $this->orase . ")";
			if ( $this->transport ) $sel_toate = $sel_toate . " AND oferte.id_transport = '" . $this->transport . "' ";
			if ( $this->id_hotel ) $sel_toate = $sel_toate . " AND hoteluri.id_hotel  ='" . $this->id_hotel . "' ";
			if ( $this->random ) $sel_toate = $sel_toate . " AND hoteluri.poza1 is not null ";
			if ( $this->set_exceptie ) $sel_toate = $sel_toate . " AND oferte.id_oferta <> '" . $this->set_exceptie . "' ";
			if ( $this->set_exceptieH ) $sel_toate = $sel_toate . " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
			if ( $this->oferte_vizitate ) $sel_toate = $sel_toate . " and oferte.id_oferta in (" . $this->oferte_vizitate . ") ";
			if ( $this->durata ) $sel_toate = $sel_toate . " AND oferte.nr_zile IN (" . $this->durata . ") ";
			if ( $this->keywords )
			{
				$sel_toate = $sel_toate . "\n AND ( localitati.denumire LIKE '%" . $this->keywords . "%' ";
				$sel_toate = $sel_toate . "\n OR zone.denumire LIKE '%" . $this->keywords . "%' ";
				$sel_toate = $sel_toate . "\n OR hoteluri.nume LIKE '%" . $this->keywords . "%' ";
				$sel_toate = $sel_toate . "\n OR oferte.denumire LIKE '%" . $this->keywords . "%' ";
				$sel_toate = $sel_toate . "\n OR tari.denumire LIKE '%" . $this->keywords . "%' )";
			}
			if ( !$this->last_minute && !$this->afisare_oferte ) $sel_toate = $sel_toate . " GROUP BY oferte.last_minute, oferte.id_hotel ";
			else $sel_toate = $sel_toate . " GROUP BY oferte.id_hotel ";
			if ( $this->ord_stele ) $sel_toate = $sel_toate . " ORDER BY hoteluri.stele " . $this->ord_stele . " ";
			elseif ( $this->ord_relevanta ) $sel_toate = $sel_toate . " ORDER BY oferte.click " . $this->ord_relevanta . " ";
			elseif ( $this->ord_pret ) {
				if($this->check_in) $sel_toate = $sel_toate . " ORDER BY (new_price / oferte.nr_nopti) " . $this->ord_pret . " ";
				else $sel_toate = $sel_toate . " ORDER BY (oferte.pret_minim_lei / oferte.nr_nopti) " . $this->ord_pret . " ";
			}
			elseif ( $this->ord_numeH ) $sel_toate = $sel_toate . " ORDER BY hoteluri.nume " . $this->ord_numeH . " ";
			elseif ( $this->random ) $sel_toate = $sel_toate . " ORDER BY RAND() ";
			/*elseif ( $_SERVER['PHP_SELF']=='/tara_new1.php') $sel_toate = $sel_toate . " ORDER BY oferte.pret_minim_lei ASC ";*/
			elseif ( $_SERVER['PHP_SELF']=='/tara_new1.php') $sel_toate = $sel_toate . " ORDER BY ofsp2.ordine DESC, hoteluri.oferta_speciala DESC, hoteluri.counter DESC, oferte.ultima_modificare DESC ";
			elseif ( $this->last_minute ) $sel_toate = $sel_toate . " ORDER BY data_plecare ASC ";
			/*else $sel_toate = $sel_toate . " ORDER BY recomandata, oferte.pret_minim_lei ASC ";*/
			//else $sel_toate = $sel_toate . " ORDER BY recomandata, oferte.ultima_modificare DESC ";
			/*else $sel_toate = $sel_toate . " ORDER BY oferte.ultima_modificare DESC ";*/
			else $sel_toate = $sel_toate . " ORDER BY ofsp2.ordine DESC, hoteluri.oferta_speciala DESC, hoteluri.counter DESC, oferte.ultima_modificare DESC ";
			if ( $this->nr_oferte_per_pagina )
				$sel_toate = $sel_toate . " LIMIT " . $this->from . ", " . $this->nr_oferte_per_pagina . " ";
		} //end if ( $this->cautahotel )
	//echo $sel_toate;
	return $sel_toate;
	}

	function numar_oferte() {
		$sql = $this->sel_toate_nr();
		if ( $this->last_minute ) $sql = $sql . " Group by oferte.id_oferta ";
		else $sql = $sql . " Group by oferte.id_hotel, oferte.last_minute ";
		$rez = mysql_query( $sql ) or die( mysql_error() );
		$numar_oferte_total = mysql_num_rows( $rez );
		@mysql_free_result( $rez );
		return $numar_oferte_total;
	}

	function get_filtru_mare() {
		$ordine_masa = array( 'All Exclusive All Inclusive' => 1, 'All Inclusive' => 2, 'Bonuri valorice' => 3, 'Demipensiune' => 4, 'Fisa cont' => 5, 'Mic Dejun' => 6, 'Nici o masa' => 7, 'Pensiune completa' => 8, 'Ultra all inclusive' => 9 );
		
		$sel_filtru = "SELECT
		COUNT(oferte.id_hotel) AS numar_hoteluri,
		zone.denumire AS denumire_zona,
		zone.id_zona AS id_zona,
		oferte.id_oferta,
		oferte.id_hotel,
		oferte.masa,
		oferte.nr_zile,
		oferte.nr_nopti,
		hoteluri.stele,
		hoteluri.distanta_fata_de,
		hoteluri.distanta,
		hoteluri.concept,
		transport.denumire AS denumire_transport,
		transport.id_trans,
		facilities.facilitati
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_filtru = $sel_filtru . " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru = $sel_filtru . " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru = $sel_filtru . " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru = $sel_filtru . "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru = $sel_filtru . " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru = $sel_filtru . " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru = $sel_filtru . " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if($this->check_in) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru = $sel_filtru . " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru = $sel_filtru . " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru = $sel_filtru . " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru = $sel_filtru . " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru = $sel_filtru . " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru = $sel_filtru . " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru = $sel_filtru . " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru = $sel_filtru . " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru = $sel_filtru . " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru = $sel_filtru . " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru = $sel_filtru . " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru = $sel_filtru . " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru = $sel_filtru . " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru = $sel_filtru . " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru = $sel_filtru . " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru = $sel_filtru . " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru = $sel_filtru . " AND early.discount is not null";
		//if ( $this->transport ) $sel_filtru = $sel_filtru . " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->stele ) $sel_filtru = $sel_filtru . " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) $sel_filtru = $sel_filtru . " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru = $sel_filtru . " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->concept ) $sel_filtru = $sel_filtru . " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		if ( $this->durata ) $sel_filtru = $sel_filtru . " AND oferte.nr_zile IN (" . $this->durata . ") ";
		$sel_filtru = $sel_filtru . " GROUP BY oferte.id_hotel ";
		//if($_SESSION['mail']=='daniel@ocaziituristice.ro') echo $sel_filtru ;
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {

			//$this->id_hotel[$row_filtru['numar_hoteluri']] = $this->id_hotel[$row_filtru['numar_hoteluri']] + 1;

			/*if ( $row_filtru['denumire_transport'] == 'Avion' ) {
				$selFTA = "SELECT localitati.denumire
					FROM oferte_transport_avion
					LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
					LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
					WHERE
					oferte_transport_avion.id_oferta = '" . $row_filtru['id_oferta'] . "'
					AND oferte_transport_avion.tip = 'dus'
					GROUP BY localitati.denumire
					ORDER BY localitati.denumire
				";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$this->plecare_avion[$loc_FTA['denumire']] = $this->plecare_avion[$loc_FTA['denumire']] + 1;
				}
				@mysql_free_result( $queFTA );
			} elseif ( $row_filtru['denumire_transport'] == 'Autocar' ) {
				$selFTA = "SELECT localitati.denumire
					FROM oferte_transport_autocar
					LEFT JOIN localitati on oferte_transport_autocar.id_localitate = localitati.id_localitate
					WHERE
					oferte_transport_autocar.id_oferta = '" . $row_filtru['id_oferta'] . "'
					GROUP BY localitati.denumire
					ORDER BY localitati.denumire
				";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$this->plecare_autocar[$loc_FTA['denumire']] = $this->plecare_autocar[$loc_FTA['denumire']] + 1;
				}
				@mysql_free_result( $queFTA );
			}

			if ( $this->trans_filtru[$row_filtru['denumire_transport']] ) $this->trans_filtru[$row_filtru['denumire_transport']] = $this->trans_filtru[$row_filtru['denumire_transport']] + 1;
			else $this->trans_filtru[$row_filtru['denumire_transport']] = 1;*/
			/*if($this->id_hotel[$row_filtru['id_hotel']]>1) {
				$this->stele_filtru[$row_filtru['stele']] = $this->stele_filtru[$row_filtru['stele']];
			} else {
				$this->stele_filtru[$row_filtru['stele']] = $this->stele_filtru[$row_filtru['stele']] + 1;
			}*/
			/*$this->nr_zile_filtru[$row_filtru['nr_zile']] = $this->nr_zile_filtru[$row_filtru['nr_zile']] + 1;*/
			/*$this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] = $this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] + 1;*/
			/*if ( $row_filtru['distanta'] ) {
				if ( $row_filtru['distanta'] < 10 ) $z = 1;
				elseif ( $row_filtru['distanta'] < 100 ) $z = 2;
				elseif ( $row_filtru['distanta'] < 300 ) $z = 3;
				elseif ( $row_filtru['distanta'] >= 300 ) $z = 4;
				$this->distanta_filtru[trim( $row_filtru['distanta_fata_de'] )][$z] = $this->distanta_filtru[trim( $row_filtru['distanta_fata_de'] )][$z] + 1;
			}*/
			/*if ( $row_filtru['concept'] ) {
				$this->concept_filtru[$row_filtru['concept']] = $this->concept_filtru[$row_filtru['concept']] + 1;
			}*/
			/*if ( $row_filtru['facilitati'] ) {
				$facil = explode(",", $row_filtru['facilitati']);
				foreach($facil as $k_facil => $v_facil) {
					$this->facilitati_filtru[$v_facil] = $this->facilitati_filtru[$v_facil] + 1;
				}
			}*/
			if ( $row_filtru['denumire_zona'] ) {
				$this->zone_filtru[$row_filtru['denumire_zona']] = $this->zone_filtru[$row_filtru['denumire_zona']]+1;
			}
			$sel_EB = "SELECT * FROM early_booking WHERE ".$row_filtru['id_oferta']." = id_oferta AND end_date >= NOW() AND tip = 'sejur' ";
			$que_EB = mysql_query($sel_EB) or die(mysql_error());
			$totalRows_EB = mysql_num_rows($que_EB);
			if($totalRows_EB>0) $this->early_booking = $this->early_booking + 1;
		} @mysql_free_result( $rez_filtru );
		

		/********** masa_filtru **********/
		$sel_filtru = "SELECT hotels_meals.masa
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel ";
		if ( $this->tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru .= "
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(hotel_meal.masa SEPARATOR ',') AS masa, hotel_meal.id_hotel FROM hotel_meal GROUP BY hotel_meal.id_hotel) AS hotels_meals ON hotels_meals.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($this->check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $this->data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		//if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		//echo $sel_filtru;
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->masa_filtru = '';
		$this->masa_filtru = array();
		$count_meal_hotels = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['masa'] ) {
				$meal = explode(",", $row_filtru['masa']);
				foreach($meal as $k_meal => $v_meal) {
					$this->masa_filtru[$ordine_masa[$v_meal]][$v_meal] = $this->masa_filtru[$ordine_masa[$v_meal]][$v_meal] + 1;
				}
			}
			/*if(!in_array($row_filtru['id_hotel'], $count_meal_hotels)) {
				$count_meal_hotels[] = $row_filtru['id_hotel'];
				$this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] = $this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] + 1;
			}*/
			//$this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] = $row_filtru['count_hotels'];
		} @mysql_free_result( $rez_filtru );
		/********** masa_filtru **********/


		/********** stele_filtru **********/
		$sel_filtru = "SELECT hoteluri.stele
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($this->check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $this->data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		//if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->stele_filtru = '';
		$this->stele_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if($this->id_hotel[$row_filtru['id_hotel']]>1) {
				$this->stele_filtru[$row_filtru['stele']] = $this->stele_filtru[$row_filtru['stele']];
			} else {
				$this->stele_filtru[$row_filtru['stele']] = $this->stele_filtru[$row_filtru['stele']] + 1;
			}
		} @mysql_free_result( $rez_filtru );
		/********** stele_filtru **********/


		/********** nr_zile_filtru **********/
		$sel_filtru = "SELECT oferte.nr_zile
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($this->check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $this->data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->nr_zile_filtru = '';
		$this->nr_zile_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if($this->id_hotel[$row_filtru['id_hotel']]>1) {
				$this->nr_zile_filtru[$row_filtru['nr_zile']] = $this->nr_zile_filtru[$row_filtru['nr_zile']];
			} else {
				$this->nr_zile_filtru[$row_filtru['nr_zile']] = $this->nr_zile_filtru[$row_filtru['nr_zile']] + 1;
			}
		} @mysql_free_result( $rez_filtru );
		/********** nr_zile_filtru **********/


		/********** distanta_filtru **********/
		$sel_filtru = "SELECT hoteluri.distanta_fata_de,
		hoteluri.distanta
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($this->check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $this->data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->distanta_filtru = '';
		$this->distanta_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['distanta'] ) {
				if ( $row_filtru['distanta'] < 10 ) $z = 1;
				elseif ( $row_filtru['distanta'] < 100 ) $z = 2;
				elseif ( $row_filtru['distanta'] < 500 ) $z = 3;
				elseif ( $row_filtru['distanta'] >= 500 ) $z = 4;
				$this->distanta_filtru[trim( $row_filtru['distanta_fata_de'] )][$z] = $this->distanta_filtru[trim( $row_filtru['distanta_fata_de'] )][$z] + 1;
			}
		} @mysql_free_result( $rez_filtru );
		/********** distanta_filtru **********/


		/********** concept_filtru **********/
		$sel_filtru = "SELECT hoteluri.concept
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($this->check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $this->data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		//if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->concept_filtru = '';
		$this->concept_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['concept'] ) {
				$this->concept_filtru[$row_filtru['concept']] = $this->concept_filtru[$row_filtru['concept']] + 1;
			}
		} @mysql_free_result( $rez_filtru );
		/********** concept_filtru **********/


		/********** facilitati_filtru **********/
		$sel_filtru = "SELECT facilities.facilitati
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($this->check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $this->data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		//echo $sel_filtru;
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->facilitati_filtru = '';
		$this->facilitati_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['facilitati'] ) {
				$facil = explode(",", $row_filtru['facilitati']);
				foreach($facil as $k_facil => $v_facil) {
					$this->facilitati_filtru[$v_facil] = $this->facilitati_filtru[$v_facil] + 1;
				}
			}
		} @mysql_free_result( $rez_filtru );
		/********** facilitati_filtru **********/


		/********** trans_filtru **********/
		$sel_filtru = "SELECT transport.denumire AS denumire_transport,
		oferte.id_oferta
		FROM oferte ";
		if ( $this->tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $this->tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $this->luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		if ( $this->early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $this->start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $this->id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($this->check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($this->check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$this->check_in."' AND data_pret_oferta.data_end >= '".$this->check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$this->date_min."' AND data_pret_oferta.data_start <= '".$this->date_max."' ) ) ";
		if ( !$this->search && !$this->last_minute && !$this->recomandata ) {
			if ( !$this->set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $this->last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $this->cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $this->continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		if ( $this->tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		if ( $this->tari && !$this->tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		if ( !$this->tari && $this->tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $this->tari && $this->tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $this->zone ) $sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		if ( $this->orase ) $sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		if ( $this->recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $this->id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		if ( $this->set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		if ( $this->early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		//if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $this->data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";
		if ( $this->distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode(",", $this->facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		if ( $this->durata ) $sel_filtru = $sel_filtru . " AND oferte.nr_zile IN (" . $this->durata . ") ";
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->trans_filtru = '';
		$this->trans_filtru = array();
		$this->plecare_avion = '';
		$this->plecare_avion = array();
		$this->plecare_autocar = '';
		$this->plecare_autocar = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['denumire_transport'] == 'Avion' or $row_filtru['denumire_transport'] == 'Avion si Autocar local' ) {
				$selFTA = "SELECT localitati.denumire
					FROM oferte_transport_avion
					LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
					LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
					LEFT JOIN zone ON localitati.id_zona = zone.id_zona
					WHERE oferte_transport_avion.id_oferta = '" . $row_filtru['id_oferta'] . "'
					AND zone.id_tara = '1'
					AND oferte_transport_avion.tip = 'dus'
					GROUP BY localitati.denumire
					ORDER BY localitati.denumire
				";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$this->plecare_avion[$loc_FTA['denumire']] = $this->plecare_avion[$loc_FTA['denumire']] + 1;
				}
				@mysql_free_result( $queFTA );
			} elseif ( $row_filtru['denumire_transport'] == 'Autocar' ) {
				$selFTA = "SELECT localitati.denumire
					FROM oferte_transport_autocar
					LEFT JOIN localitati on oferte_transport_autocar.id_localitate = localitati.id_localitate
					WHERE
					oferte_transport_autocar.id_oferta = '" . $row_filtru['id_oferta'] . "'
					GROUP BY localitati.denumire
					ORDER BY localitati.denumire
				";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$this->plecare_autocar[$loc_FTA['denumire']] = $this->plecare_autocar[$loc_FTA['denumire']] + 1;
				}
				@mysql_free_result( $queFTA );
			}

			if ( $this->trans_filtru[$row_filtru['denumire_transport']] ) $this->trans_filtru[$row_filtru['denumire_transport']] = $this->trans_filtru[$row_filtru['denumire_transport']] + 1;
			else $this->trans_filtru[$row_filtru['denumire_transport']] = 1;
		} @mysql_free_result( $rez_filtru );
		/********** trans_filtru **********/

		
		$filtru = array(
			'zone' => $this->zone_filtru,
			'masa' => $this->masa_filtru,
			'stele' => $this->stele_filtru,
			'distanta' => $this->distanta_filtru,
			'concept' => $this->concept_filtru,
			'facilitati' => $this->facilitati_filtru,
			'trans' => $this->trans_filtru,
			'plecare_avion' => $this->plecare_avion,
			'plecare_autocar' => $this->plecare_autocar,
			'nr_zile' => $this->nr_zile_filtru,
			'early_booking' => $this->early_booking
		);
		return $filtru;
	}

	function get_filtru() {
		$sql = $this->sel_toate_nr();
		$sql = $sql . " GROUP BY oferte.id_hotel ";
		$rez = mysql_query( $sql ) or die( mysql_error() );
		$numar_oferte_total = mysql_num_rows( $rez );
		while ( $row_toate = mysql_fetch_array( $rez ) ) {
			if ( $row_toate['denumire_transport'] == 'Avion' ) {
				$selFTA = "SELECT localitati.denumire
					FROM oferte_transport_avion
					LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
					LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
					WHERE
					oferte_transport_avion.id_oferta = '" . $row_toate['id_oferta'] . "'
					AND oferte_transport_avion.tip = 'dus'
					GROUP BY localitati.denumire
					ORDER BY localitati.denumire
				";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$this->plecare_avion_nr[$loc_FTA['denumire']] = $this->plecare_avion_nr[$loc_FTA['denumire']] + 1;
				}
				@mysql_free_result( $queFTA );
			}
			elseif ( $row_filtru['denumire_transport'] == 'Autocar' ) {
				$selFTA = "SELECT localitati.denumire
					FROM oferte_transport_autocar
					LEFT JOIN localitati ON oferte_transport_autocar.id_localitate = localitati.id_localitate
					WHERE
					oferte_transport_autocar.id_oferta = '" . $row_filtru['id_oferta'] . "'
					GROUP BY localitati.denumire
					ORDER BY localitati.denumire
				";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$this->plecare_autocar_nr[$loc_FTA['denumire']] = $this->plecare_avion_nr[$loc_FTA['denumire']] + 1;
				}
				@mysql_free_result( $queFTA );
			}
			if ( $this->trans_nou1[$row_toate['denumire_transport']] ) $this->trans_nou1[$row_toate['denumire_transport']] = $this->trans_nou1[$row_toate['denumire_transport']] + 1;
			else $this->trans_nou1[$row_toate['denumire_transport']] = 1;
			$this->stele_nou1[$row_toate['stele']] = $this->stele_nou1[$row_toate['stele']] + 1;
			$this->nr_zile_nou1[$row_toate['nr_zile']] = $this->nr_zile_nou1[$row_toate['nr_zile']] + 1;
			$this->masa_nou1[$row_toate['masa']] = $this->masa_nou1[$row_toate['masa']] + 1;
			if ( $row_toate['distanta'] ) {
				if ( $row_toate['distanta'] < 10 ) $z = 1;
				elseif ( $row_toate['distanta'] < 100 ) $z = 2;
				elseif ( $row_toate['distanta'] < 300 ) $z = 3;
				elseif ( $row_toate['distanta'] >= 300 ) $z = 4;
				$this->distanta1[trim( $row_toate['distanta_fata_de'] )][$z] = $this->distanta1[trim( $row_toate['distanta_fata_de'] )][$z] + 1;
			}
			if ( $row_toate['denumire_zona'] ) {
				$this->zone_filtru1[$row_toate['denumire_zona']] = $this->zone_filtru1[$row_toate['denumire_zona']];
			}
			if ( $row_toate['concept'] ) {
				$this->concept_nou1[$row_toate['concept']] = $this->concept_nou1[$row_toate['concept']] + 1;
			}
		}
		@mysql_free_result( $rez );
		$filtru1 = array(
			'zone' => $this->zone_filtru1,
			'masa' => $this->masa_nou1,
			'trans' => $this->trans_nou1,
			'stele' => $this->stele_nou1,
			'distanta' => $this->distanta1,
			'plecare_avion' => $this->plecare_avion_nr,
			'plecare_autocar' => $this->plecare_autocar_nr,
			'nr_zile' => $this->nr_zile_nou1,
			'concept' => $this->concept_nou1
		);

		return $filtru1;
	}

	function setAfisare( $from, $nr_oferte_per_pagina ) {
		$this->nr_oferte_per_pagina = $nr_oferte_per_pagina;
		$this->from = $this->reverse_calculeaza_pagina( $from );
	}

	function link_pagini( $rezultate ) {
		if ( $rezultate > 1 ) {
			$link = $this->link_pagini . $this->link_pag;
			$de_inlocuit = array( '###' );
			$inlocuit = array( $rezultate );
			$link_nume = str_replace( $de_inlocuit, $inlocuit, strtolower( $link ) );
		}
		else $link_nume = $this->link_pagini;
		$link_nume = $link_nume . $this->filtru_link;
		return $link_nume;
	}

	function calculeaza_pagina( $from ) {
		$nr_pagina = (int)( $from / $this->nr_oferte_per_pagina );
		$nr_pagina++;
		return $nr_pagina;
	}

	function reverse_calculeaza_pagina( $numar_pagina ) {
		$this->pagina_curenta = $numar_pagina;
		$numar_pagina = $numar_pagina - 1;
		$from = $numar_pagina * $this->nr_oferte_per_pagina;
		return $from;
	}

	function initializeaza_pagini( $link, $pag, $filtru ) {
		$this->numar_oferte = $this->numar_oferte();
		$this->link_pag = $pag;
		$this->link_pagini = $link;
		$this->filtru_link = $filtru;
	}
	
	function nr_maxim_pagini() {
		if ( $this->numar_oferte > 0 ) {
			$nr_pagini = floor( $this->numar_oferte / $this->nr_oferte_per_pagina );
			if ( $this->numar_oferte % $this->nr_oferte_per_pagina != 0 ) $nr_pagini++;
			return $nr_pagini;
		}
	}
	
	function meta_pages($tari=NULL, $zone=NULL, $oras=NULL) {
		$link_request = parse_url($_SERVER['REQUEST_URI']);
		
		$meta_pages['link_canonical'] = $GLOBALS['sitepath'];
		if($tari!=NULL) $meta_pages['link_canonical'] .= 'sejur-'.$tari.'/';
		if($zone!=NULL) {
			if($tari==$zone) $meta_pages['link_canonical'] .= '';
				else $meta_pages['link_canonical'] .= $zone.'/';
		}
		if($oras!=NULL) {
			if($zone==$oras) $meta_pages['link_canonical'] .= '';
				else $meta_pages['link_canonical'] .= $oras.'/';
		}
		
		if(!isset($_REQUEST['from'])) $meta_pages['meta_prev'] = '';
			else {
				$meta_pages['meta_prev'] = '<link rel="prev" href="'.$meta_pages['link_canonical'];
				if($zone==$oras and $zone!='' and $oras!='') $meta_pages['meta_prev'] .= $oras.'/';
				if($_REQUEST['from']-1>1) $meta_pages['meta_prev'] .= 'pag-'.($_REQUEST['from']-1).'/';
				$meta_pages['meta_prev'] .= '?'.$link_request['query'].'" />';
			}
		
		if($_REQUEST['from']==$this->nr_maxim_pagini() or (!isset($_REQUEST['from']) and $this->nr_maxim_pagini()==1)) $meta_pages['meta_next'] = '';
			else {
				if(!isset($_REQUEST['from'])) $request_from = 1; else $request_from = $_REQUEST['from'];
				$meta_pages['meta_next'] = '<link rel="next" href="'.$meta_pages['link_canonical'];
				if($zone==$oras and $zone!='' and $oras!='') $meta_pages['meta_next'] .= $oras.'/';
				$meta_pages['meta_next'] .= 'pag-'.($request_from+1).'/?';
				$meta_pages['meta_next'] .= $link_request['query'].'" />';
			}
		
		return $meta_pages;
	}

	function printPagini() {
		if ( $this->numar_oferte > 0 ) {
			$nr_pagini = floor( $this->numar_oferte / $this->nr_oferte_per_pagina );
			if ( $this->numar_oferte % $this->nr_oferte_per_pagina != 0 ) $nr_pagini++;
			$pagina_curenta = $this->pagina_curenta;
			if ( $nr_pagini > 0 ) {
				echo "<strong>Pagini:</strong> ";
				$begin_ct = 1;
				$end_ct = $nr_pagini;
				$this->pagini = '';
				if ( $pagina_curenta >= 4 ) $begin_ct = $pagina_curenta - 3;
				if ( $pagina_curenta > 1 ) {
					$this->pagini = '<a href="'.w3c_and($this->link_pagini(1)).'" title="Pagina 1" class="nav">&laquo;</a> ';
				}
				$afis = $pagina_curenta - 1;
				if ( $pagina_curenta > 1 ) $this->pagini = $this->pagini.'<a href="'.w3c_and($this->link_pagini($afis)).'"  title="Pagina '.$afis.'" class="nav">&lt;</a> ';
				if ( $pagina_curenta >= '5' ) $this->pagini = $this->pagini.'... ';
				if ( $nr_pagini > 4 ) {
					if ( $pagina_curenta < 4 ) $end_ct = 7;
					else $end_ct = $pagina_curenta + 3;
					if ( $end_ct > $nr_pagini ) $end_ct = $nr_pagini;
				}
				for( $i = $begin_ct; $i <= $end_ct; $i++ ) {
					if ( $i == $pagina_curenta ) {
						$this->pagini = $this->pagini.'<span class="sel">'.$i.'</span> ';
					}
					else {
						$this->pagini = $this->pagini.'<a href="'.w3c_and($this->link_pagini($i)).'" title="Pagina '.$i.'">'.$i.'</a> ';
					}
				}
				if ( $pagina_curenta <= $nr_pagini - 4 ) $this->pagini = $this->pagini.'... ';
				$afis = $pagina_curenta + 1;
				if ( $afis <= $nr_pagini ) $this->pagini = $this->pagini.'<a href="'.w3c_and($this->link_pagini($afis)).'" title="Pagina '.$afis.'" class="nav">&gt;</a>&nbsp;';
				if ( $afis <= $nr_pagini ) $this->pagini = $this->pagini.'<a href="'.w3c_and($this->link_pagini($nr_pagini)).'" title="Pagina '.$nr_pagini.'" class="nav">&raquo;</a>';
				echo $this->pagini;
			}
		}
	}

	function eroare( $template = "negasit_filtrare.tpl" ) {
		$template = $_SERVER['DOCUMENT_ROOT'] . "/templates/" . $template;
		$tpl = new TPL( $template );
		
		$titlu = 'Nu exista nici o oferta disponibila pentru <span class="red">';
		if($_REQUEST['tip']) $titlu .= ucwords(desfa_link($_REQUEST['tip'])).' ';
		if($_REQUEST['oras']) $titlu .= ucwords(desfa_link($_REQUEST['oras'])).' / ';
		if($_REQUEST['zone']) $titlu .= ucwords(desfa_link($_REQUEST['zone'])).' din ';
		if($_REQUEST['tari']) $titlu .= ucwords(desfa_link($_REQUEST['tari'])).'</span>';
		
		$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
		
		$tpl->replace_tags(array(
			"titlu"=>$titlu,
			"link-return"=>substr($sitepath, 0, -1).$uri_parts[0]
		));
		$tpl->output_tpl();

				/*$oferta->set_denumire_zona( $row_normal["denumire_zona"] );
				$oferta->set_denumire_tara( $row_normal["denumire_tara"] );
					$oferta->setTplGrup( $_SERVER['DOCUMENT_ROOT'] . "/templates/sejur_tara_tematica_grup.tpl" );*/
	}

	function config_paginare( $tip ) {
		$this->classpaginare = $tip;
		$this->nr_aparitiiP = 1;
	}

	function afiseaza() {
		$sel_toate = $this->sel_toate();
		$rez_toate = mysql_query( $sel_toate ) or die( mysql_error() );
		$no = mysql_num_rows( $rez_toate );
		if ( $no == 0 && !$this->oferta_speciala ) {
			$this->eroare();
		}
		if ( $no > 0 ) {
			$i = 0;

			$selB = "SELECT denumire FROM banner WHERE activ = 'da' ";
			if ( $this->tip_oferta ) $selB = $selB . " AND id_tip IN (" . $this->tip_oferta . ") ";
			else $selB = $selB . " AND id_tip = '0' ";
			if ( $this->orase ) $selB = $selB . " AND id_oras = " . $this->orase . " ";
			elseif ( $this->zone ) $selB = $selB . " AND id_zona = " . $this->zone . " ";
			elseif ( $this->tari ) $selB = $selB . " AND id_tara = " . $this->tari . " ";
			else $selB = $selB . " AND id_tara = '0' AND id_zona = '0' AND id_oras = '0' ";
			$rezB = mysql_query( $selB ) or die( mysql_error() );
			if ( mysql_num_rows( $rezB ) == 0 ) {
				$selB = "SELECT denumire FROM banner WHERE id_tip = '0' AND id_tara = '0' AND id_zona = '0' AND id_oras = '0' AND activ = 'da' ";
				$rezB = mysql_query( $selB ) or die( mysql_error() );
			}

			$banner_que = array();
			$ii = 0;
			while($rowB = mysql_fetch_array($rezB)) {
				$ii++;
				$banner_que[$ii] = $rowB['denumire'];
			}
			@mysql_free_result( $rezB );

			while ( $row_normal = mysql_fetch_array( $rez_toate ) ) {
				$i++;
				$nr_total_oferte = $this->numar_oferte();
				if($nr_total_oferte<10) {
					$banner_poz = round($nr_total_oferte/2)+1;
					$banner_poz = array($banner_poz);
				}
				else {
					$banner_poz = array('6','11','16');
				}
				if (in_array($i, $banner_poz)) {
					if (!empty($banner_que) and ($this->afiseaza_banner=='da')) {
						include($_SERVER['DOCUMENT_ROOT'].'/banner/'.$banner_que[array_rand($banner_que)]);
					}
				}

				$oferta = new OFERTA( $row_normal["id_oferta"] );
				$oferta->setNrOferte( $row_normal['nr_normale'] );
				if ( sizeof( $this->filtruA ) > 0 ) $oferta->setFiltru( $this->filtruA );
				$oferta->set_denumire_continent( $row_normal["nume_continent"] );
				$oferta->set_id_hotel( $row_normal["id_hotel"] );
				$oferta->set_denumire_oferta( $row_normal["denumire_oferta"] );
				$oferta->set_denumire_oferta_scurta( $row_normal["denumire_oferta_scurta"] );
				$oferta->set_nr_zile( $row_normal["nr_zile"] );
				$oferta->set_nr_nopti( $row_normal["nr_nopti"] );
				$oferta->set_poza1( $row_normal["poza1"] );
				$oferta->set_poza1_mare( $row_normal["poza1"] );
				$oferta->set_nume_hotel( $row_normal["denumire_hotel"] );
				if ( $this->dimensiuneNumeHotel ) $oferta->set_nume_hotel_lenght( $this->dimensiuneNumeHotel );
				$oferta->set_denumire_localitate( $row_normal["denumire_localitate"] );
				if ( $this->denumire_tip_oferta ) $oferta->set_tip_activ( $this->denumire_tip_oferta );
				$oferta->set_masa( $row_normal["masa"] );
				$oferta->set_denumire_zona( $row_normal["denumire_zona"] );
				$oferta->set_denumire_tara( $row_normal["denumire_tara"] );
				$oferta->set_descriere_hotel( $row_normal["descriere_hotel"] );
				$oferta->set_descriere_hotel_tez( $row_normal["new_descriere"] );
				$oferta->set_comentariul_nostru($row_normal["detalii_concept"]);
				$oferta->set_concept($row_normal["concept"]);
				$oferta->set_nota_total_medie($row_normal["nota_total_medie"]);
				$oferta->set_nr_comentarii($row_normal["nr_comentarii"]);
				$oferta->set_nr_stele( $row_normal["stele"] );
				$oferta->set_denumire_transport( $row_normal["denumire_transport"] );
				$oferta->set_descriere_hotel_lenght( 255 );
				if ( $row_normal["parinte"] && $this->tip_oferta1['id_tipuri'] ) $oferta->set_denumire_tip_oferta( $row_normal["denumire_tip_oferta"] );
				$oferta->set_exprimare_pret( $row_normal["exprimare_pret"] );
				$oferta->set_pret( $row_normal["pret_minim"] );
				$oferta->set_moneda( $row_normal["moneda"] );
				$oferta->set_date_plecare(denLuniRo(denZileRo(date('d F - l',strtotime($row_normal["data_plecare"])))));
				$oferta->setDetaliiRecomandata( $row_normal['detalii_recomandata'] );
				$oferta->setPretRecomandata( $row_normal['pret_recomandata'] );
				$oferta->meseHotel($row_normal["id_hotel"]);
				$oferta->set_denumire_oferta_lenght( 65 );
				if($this->check_in) $oferta->set_checkin($this->check_in);
				if ( $row_normal['tip_unitate'] == 'Circuit' ) {
					$oferta->setCircuite('da');
					$oferta->setPlecare(denLuniRo($row_normal['plecare']));
					$oferta->setTariVizitate($row_normal['tari_vizitate']);
				}
				if ( $row_normal['apare_templait_dreapta'] == 'da' ) {
					$oferta->set_titlu_dreapta( $row_normal["denumire_zona"] . ' / ' . $row_normal["denumire_localitate"] );
				}
				else $oferta->set_titlu_dreapta( $row_normal["denumire_localitate"] );
				// if($row_normal["discount"] && $row_normal["discount"]<>'0')
				// echo (mktime($row_normal["early_end"])-time()) ;
				
				$ebend = strptime($row_normal["early_end"], '%d/%m/%Y');
				$eb_timestamp = mktime(0, 0, 0, $ebend['tm_mon']+1, $ebend['tm_mday'], $ebend['tm_year']+1900);
				if ( $eb_timestamp > time() ) {
					$oferta->setEarlyBooking( '1' );
					$oferta->set_discount( $row_normal["discount"] );
					$oferta->set_earlyend( $row_normal["early_end"] );
				}
				if ( $row_normal['tip']=='oferte_speciale' ) $oferta->setSPOF( 'da' );
				if ( $row_normal['taxa_aeroport'] > 0 ) $oferta->setTaxaAeroport( 'da' );
				if ( $row_normal['last_minute'] == 'da' ) $oferta->setLastMinute( 'da' );
				if ( $this->tipofsp==1 ) $oferta->setTipOfSp(1);
				if ( $this->oferta_speciala ) {
					$oferta->set_oferta_speciala( 'da' );
					$oferta->afiseaza_sejur( $this->oferta_speciala );
				}
				elseif (($_SERVER['PHP_SELF']=='/last_minute.php') or ($_SERVER['PHP_SELF']=='/last_minute_tara.php')) {
					$oferta->afiseaza_sejur( $_SERVER['DOCUMENT_ROOT'] . "/templates/last_minute.tpl" );
				/***** modificare 25.10.2012 *****/
				//}
				//elseif ( $row_normal['recomandata'] == 'da' ) {
				//	$oferta->setRec( 'da' );
				//	if(($_SERVER['PHP_SELF']=='/zone_new1.php') or ($_SERVER['PHP_SELF']=='/localitate_new1.php')) {
				//		/*if($row_normal['tip_unitate']=='Circuit') {
				//			$oferta->afiseaza_sejur( $_SERVER['DOCUMENT_ROOT'] . "/templates/recomandare_circuit.tpl" );
				//		} else {*/
				//			$oferta->setTplGrup( $_SERVER['DOCUMENT_ROOT'] . "/templates/recomandare_grupate.tpl" );
				//		/*}*/
				//	} else {
				//		$oferta->setTplGrup( $_SERVER['DOCUMENT_ROOT'] . "/templates/sejur_tara_tematica_grup.tpl" );
				//	}
				//	$oferta->afiseaza_sejur( $_SERVER['DOCUMENT_ROOT'] . "/templates/sejur_tara_tematica.tpl" );
				/***** modificare 25.10.2012 *****/
				} elseif ( $this->classpaginare ) {
					//$oferta->setTplGrup( $_SERVER['DOCUMENT_ROOT'] . "/templates/sejur_tara_tematica_grup.tpl" );
					$oferta->setTplGrup( $_SERVER['DOCUMENT_ROOT'] . "/templates/new_afisare_oferte_grup.tpl" );
					$oferta->afiseaza_sejur( $_SERVER['DOCUMENT_ROOT'] . "/templates/sejur_tara_tematica.tpl" );
				}
				else $oferta->afiseaza_sejur( $_SERVER['DOCUMENT_ROOT'] . "/templates/circuit_main.tpl" );
				unset( $oferta );
			}
		}
		@mysql_free_result( $rez_toate );
	}
}