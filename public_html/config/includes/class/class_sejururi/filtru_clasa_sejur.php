<?php 
/********** masa_filtru **********/
		$time_start_masa = microtime( true );
		$sel_filtru      = "SELECT hotels_meals.masa
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel ";
		if ( $this->tip_oferta ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		}
		if ( $this->tipofsp == 1 ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		}
		if ( $this->luna_plecare ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		}
		if ( $this->start ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		}
		$sel_filtru .= "
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) {
			$sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		}
		if ( $this->id_tara_circuit ) {
			$sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		}
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(hotel_meal.masa SEPARATOR ',') AS masa, hotel_meal.id_hotel FROM hotel_meal GROUP BY hotel_meal.id_hotel) AS hotels_meals ON hotels_meals.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) {
			$sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		}
		if ( $this->check_in ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		}
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if ( $this->check_in ) {
			$sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '" . $this->check_in . "' AND data_pret_oferta.data_end >= '" . $this->check_in . "' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '" . $this->date_min . "' AND data_pret_oferta.data_start <= '" . $this->date_max . "' ) ) ";
		}
		if ( ! $this->search && ! $this->last_minute && ! $this->recomandata ) {
			if ( ! $this->set_circuit ) {
				$sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			} else {
				$sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
			}
		}
		if ( $this->last_minute ) {
			$sel_filtru .= " AND oferte.last_minute = 'da' ";
		}
		if ( $this->cazare == 'da' ) {
			$sel_filtru .= " AND oferte.cazare = 'da' ";
		}
		if ( $this->continent ) {
			$sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		}
		if ( $this->tip_oferta ) {
			$sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		}
		if ( $this->tari && ! $this->tari_circ ) {
			$sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		}
		if ( ! $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		}
		if ( $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		}
		if ( $this->zone ) {
			$sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		}
		if ( $this->orase ) {
			$sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		}
		if ( $this->recomandata ) {
			$sel_filtru .= " AND oferte.recomandata = 'da' ";
		}
		if ( $this->id_hotel ) {
			$sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		}
		if ( $this->set_exceptieH ) {
			$sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " AND early.discount is not null";
		}
		if ( $this->transport ) {
			$sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		}
		if ( $this->transport_neselectat ) {
			$sel_filtru .= " AND oferte.id_transport = '" . $this->transport_neselectat . "' ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		}
		if ( $this->data_plecarii ) {
			$sel_filtru .= " AND data_plecarii.data_start = '" . $this->data_plecarii . "' ";
		}
		if ( $this->stele ) {
			$sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		}
		/*if ( $this->masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$this->masa.") ";*/
		if ( $this->distanta ) {
			$sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		}
		if ( $this->concept ) {
			$sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		}
		if ( $this->facilitati ) {
			$facilities = explode( ",", $this->facilitati );
			foreach ( $facilities as $k_facilities => $v_facilities ) {
				$v_facilities = str_replace( "'", "", trim( $v_facilities ) );
				$sel_filtru   = $sel_filtru . " AND facilities.facilitati LIKE '%" . $v_facilities . "%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		/*echo $sel_filtru;*/
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->masa_filtru = '';
		$this->masa_filtru = array();
		$count_meal_hotels = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['masa'] ) {
				$meal = explode( ",", $row_filtru['masa'] );
				foreach ( $meal as $k_meal => $v_meal ) {
					$this->masa_filtru[ $ordine_masa[ $v_meal ] ][ $v_meal ] = $this->masa_filtru[ $ordine_masa[ $v_meal ] ][ $v_meal ] + 1;
				}
			}
			/*if(!in_array($row_filtru['id_hotel'], $count_meal_hotels)) {
				$count_meal_hotels[] = $row_filtru['id_hotel'];
				$this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] = $this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] + 1;
			}*/
			/*$this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] = $row_filtru['count_hotels'];*/
		}
		@mysql_free_result( $rez_filtru );
		$time_end_masa = microtime( true );
		/*	if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	echo "filtru_masa=".$execution_time_masa = ($time_end_masa - $time_start_masa);*/
		/********** masa_filtru **********/
/********** stele_filtru **********/
		$time_start_stele = microtime( true );
		$sel_filtru       = "SELECT hoteluri.stele
		FROM oferte ";
		if ( $this->tip_oferta ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		}
		if ( $this->tipofsp == 1 ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		}
		if ( $this->luna_plecare ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		}
		if ( $this->start ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		}
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		left JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) {
			$sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		}
		if ( $this->id_tara_circuit ) {
			$sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		}
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) {
			$sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		}
		if ( $this->check_in ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		}
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if ( $this->check_in ) {
			$sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '" . $this->check_in . "' AND data_pret_oferta.data_end >= '" . $this->check_in . "' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '" . $this->date_min . "' AND data_pret_oferta.data_start <= '" . $this->date_max . "' ) ) ";
		}
		if ( ! $this->search && ! $this->last_minute && ! $this->recomandata ) {
			if ( ! $this->set_circuit ) {
				$sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			} else {
				$sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
			}
		}
		if ( $this->last_minute ) {
			$sel_filtru .= " AND oferte.last_minute = 'da' ";
		}
		if ( $this->cazare == 'da' ) {
			$sel_filtru .= " AND oferte.cazare = 'da' ";
		}
		if ( $this->continent ) {
			$sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		}
		if ( $this->tip_oferta ) {
			$sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		}
		if ( $this->tari && ! $this->tari_circ ) {
			$sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		}
		if ( ! $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		}
		if ( $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		}
		if ( $this->zone ) {
			$sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		}
		if ( $this->orase ) {
			$sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		}
		if ( $this->recomandata ) {
			$sel_filtru .= " AND oferte.recomandata = 'da' ";
		}
		if ( $this->id_hotel ) {
			$sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		}
		if ( $this->set_exceptieH ) {
			$sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " AND early.discount is not null";
		}
		if ( $this->transport ) {
			$sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		}
		if ( $this->transport_neselectat ) {
			$sel_filtru .= " AND oferte.id_transport = '" . $this->transport_neselectat . "' ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		}
		if ( $this->data_plecarii ) {
			$sel_filtru .= " AND data_plecarii.data_start = '" . $this->data_plecarii . "' ";
		}
		//if ( $this->stele ) $sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		if ( $this->masa ) {
			$sel_filtru .= " AND LOWER(hotel_meal.masa) IN (" . $this->masa . ") ";
		}
		if ( $this->distanta ) {
			$sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		}
		if ( $this->concept ) {
			$sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		}
		if ( $this->facilitati ) {
			$facilities = explode( ",", $this->facilitati );
			foreach ( $facilities as $k_facilities => $v_facilities ) {
				$v_facilities = str_replace( "'", "", trim( $v_facilities ) );
				$sel_filtru   = $sel_filtru . " AND facilities.facilitati LIKE '%" . $v_facilities . "%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->stele_filtru = '';
		$this->stele_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $this->id_hotel[ $row_filtru['id_hotel'] ] > 1 ) {
				$this->stele_filtru[ $row_filtru['stele'] ] = $this->stele_filtru[ $row_filtru['stele'] ];
			} else {
				$this->stele_filtru[ $row_filtru['stele'] ] = $this->stele_filtru[ $row_filtru['stele'] ] + 1;
			}
		}
		@mysql_free_result( $rez_filtru );
		$time_end_stele = microtime( true );
		//	if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	echo "filtru_stele=".$execution_time_stele = ($time_end_stele - $time_start_stele);
		/********** stele_filtru **********/


/********** distanta_filtru **********/
		$time_start_distanta = microtime( true );
		$sel_filtru          = "SELECT hoteluri.distanta_fata_de,
		hoteluri.distanta
		FROM oferte ";
		if ( $this->tip_oferta ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		}
		if ( $this->tipofsp == 1 ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		}
		if ( $this->luna_plecare ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		}
		if ( $this->start ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		}
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		left JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) {
			$sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		}
		if ( $this->id_tara_circuit ) {
			$sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		}
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) {
			$sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		}
		if ( $this->check_in ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		}
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if ( $this->check_in ) {
			$sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '" . $this->check_in . "' AND data_pret_oferta.data_end >= '" . $this->check_in . "' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '" . $this->date_min . "' AND data_pret_oferta.data_start <= '" . $this->date_max . "' ) ) ";
		}
		if ( ! $this->search && ! $this->last_minute && ! $this->recomandata ) {
			if ( ! $this->set_circuit ) {
				$sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			} else {
				$sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
			}
		}
		if ( $this->last_minute ) {
			$sel_filtru .= " AND oferte.last_minute = 'da' ";
		}
		if ( $this->cazare == 'da' ) {
			$sel_filtru .= " AND oferte.cazare = 'da' ";
		}
		if ( $this->continent ) {
			$sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		}
		if ( $this->tip_oferta ) {
			$sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		}
		if ( $this->tari && ! $this->tari_circ ) {
			$sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		}
		if ( ! $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		}
		if ( $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		}
		if ( $this->zone ) {
			$sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		}
		if ( $this->orase ) {
			$sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		}
		if ( $this->recomandata ) {
			$sel_filtru .= " AND oferte.recomandata = 'da' ";
		}
		if ( $this->id_hotel ) {
			$sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		}
		if ( $this->set_exceptieH ) {
			$sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " AND early.discount is not null";
		}
		if ( $this->transport ) {
			$sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		}
		if ( $this->data_plecarii ) {
			$sel_filtru .= " AND data_plecarii.data_start = '" . $this->data_plecarii . "' ";
		}
		if ( $this->stele ) {
			$sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		}
		if ( $this->masa ) {
			$sel_filtru .= " AND LOWER(hotel_meal.masa) IN (" . $this->masa . ") ";
		}
		if ( $this->distanta ) {
			$sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		}
		if ( $this->concept ) {
			$sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		}
		if ( $this->facilitati ) {
			$facilities = explode( ",", $this->facilitati );
			foreach ( $facilities as $k_facilities => $v_facilities ) {
				$v_facilities = str_replace( "'", "", trim( $v_facilities ) );
				$sel_filtru   = $sel_filtru . " AND facilities.facilitati LIKE '%" . $v_facilities . "%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->distanta_filtru = '';
		$this->distanta_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['distanta'] ) {
				if ( $row_filtru['distanta'] < 10 ) {
					$z = 1;
				} elseif ( $row_filtru['distanta'] < 100 ) {
					$z = 2;
				} elseif ( $row_filtru['distanta'] < 500 ) {
					$z = 3;
				} elseif ( $row_filtru['distanta'] >= 500 ) {
					$z = 4;
				}
				$this->distanta_filtru[ trim( $row_filtru['distanta_fata_de'] ) ][ $z ] = $this->distanta_filtru[ trim( $row_filtru['distanta_fata_de'] ) ][ $z ] + 1;
			}
		}
		@mysql_free_result( $rez_filtru );
		$time_end_distanta = microtime( true );
		//	if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	echo "filtru_distanta=".$execution_time_distanta = ($time_end_distanta - $time_start_distanta);
		/********** distanta_filtru **********/
		
/********** concept_filtru **********/
		$time_start_concept = microtime( true );
		$sel_filtru         = "SELECT hoteluri.concept
		FROM oferte ";
		if ( $this->tip_oferta ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		}
		if ( $this->tipofsp == 1 ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		}
		if ( $this->luna_plecare ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		}
		if ( $this->start ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		}
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		left JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) {
			$sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		}
		if ( $this->id_tara_circuit ) {
			$sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		}
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) {
			$sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		}
		if ( $this->check_in ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		}
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if ( $this->check_in ) {
			$sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '" . $this->check_in . "' AND data_pret_oferta.data_end >= '" . $this->check_in . "' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '" . $this->date_min . "' AND data_pret_oferta.data_start <= '" . $this->date_max . "' ) ) ";
		}
		if ( ! $this->search && ! $this->last_minute && ! $this->recomandata ) {
			if ( ! $this->set_circuit ) {
				$sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			} else {
				$sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
			}
		}
		if ( $this->last_minute ) {
			$sel_filtru .= " AND oferte.last_minute = 'da' ";
		}
		if ( $this->cazare == 'da' ) {
			$sel_filtru .= " AND oferte.cazare = 'da' ";
		}
		if ( $this->continent ) {
			$sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		}
		if ( $this->tip_oferta ) {
			$sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		}
		if ( $this->tari && ! $this->tari_circ ) {
			$sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		}
		if ( ! $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		}
		if ( $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		}
		if ( $this->zone ) {
			$sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		}
		if ( $this->orase ) {
			$sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		}
		if ( $this->recomandata ) {
			$sel_filtru .= " AND oferte.recomandata = 'da' ";
		}
		if ( $this->id_hotel ) {
			$sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		}
		if ( $this->set_exceptieH ) {
			$sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " AND early.discount is not null";
		}
		if ( $this->transport ) {
			$sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		}
		if ( $this->data_plecarii ) {
			$sel_filtru .= " AND data_plecarii.data_start = '" . $this->data_plecarii . "' ";
		}
		if ( $this->stele ) {
			$sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		}
		if ( $this->masa ) {
			$sel_filtru .= " AND LOWER(hotel_meal.masa) IN (" . $this->masa . ") ";
		}
		if ( $this->distanta ) {
			$sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		}
		//if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $this->facilitati ) {
			$facilities = explode( ",", $this->facilitati );
			foreach ( $facilities as $k_facilities => $v_facilities ) {
				$v_facilities = str_replace( "'", "", trim( $v_facilities ) );
				$sel_filtru   = $sel_filtru . " AND facilities.facilitati LIKE '%" . $v_facilities . "%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->concept_filtru = '';
		$this->concept_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['concept'] ) {
				$this->concept_filtru[ $row_filtru['concept'] ] = $this->concept_filtru[ $row_filtru['concept'] ] + 1;
			}
		}
		@mysql_free_result( $rez_filtru );
		$time_end_concept = microtime( true );
		//	if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	echo "filtru_concept=".$execution_time_concept = ($time_end_concept - $time_start_concept);
		/********** concept_filtru **********/

/********** facilitati_filtru **********/
		$time_start_facilitati = microtime( true );
		$sel_filtru            = "SELECT facilities.facilitati
		FROM oferte ";
		if ( $this->tip_oferta ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		}
		if ( $this->tipofsp == 1 ) {
			$sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		}
		if ( $this->luna_plecare ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $this->luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $this->st . "' AND data_pret_oferta.data_end >= '" . $this->sf . "'))) ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		}
		if ( $this->start ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $this->start . "' AND data_pret_oferta.data_end>='" . $this->endd . "')) ";
		}
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		left JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $this->tari_circ ) {
			$sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		}
		if ( $this->id_tara_circuit ) {
			$sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $this->id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		}
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $this->data_plecarii ) {
			$sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		}
		if ( $this->check_in ) {
			$sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		}
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if ( $this->check_in ) {
			$sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '" . $this->check_in . "' AND data_pret_oferta.data_end >= '" . $this->check_in . "' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '" . $this->date_min . "' AND data_pret_oferta.data_start <= '" . $this->date_max . "' ) ) ";
		}
		if ( ! $this->search && ! $this->last_minute && ! $this->recomandata ) {
			if ( ! $this->set_circuit ) {
				$sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			} else {
				$sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
			}
		}
		if ( $this->last_minute ) {
			$sel_filtru .= " AND oferte.last_minute = 'da' ";
		}
		if ( $this->cazare == 'da' ) {
			$sel_filtru .= " AND oferte.cazare = 'da' ";
		}
		if ( $this->continent ) {
			$sel_filtru .= " AND hoteluri.id_continent = " . $this->continent . " ";
		}
		if ( $this->tip_oferta ) {
			$sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $this->tip_oferta . ") ";
		}
		if ( $this->tari && ! $this->tari_circ ) {
			$sel_filtru .= " AND zone.id_tara = " . $this->tari . " ";
		}
		if ( ! $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		}
		if ( $this->tari && $this->tari_circ ) {
			$sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $this->tari . ")) OR (traseu_circuit.id_tara = " . $this->tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		}
		if ( $this->zone ) {
			$sel_filtru .= " AND zone.id_zona = " . $this->zone . " ";
		}
		if ( $this->orase ) {
			$sel_filtru .= " AND localitati.id_localitate = " . $this->orase . " ";
		}
		if ( $this->recomandata ) {
			$sel_filtru .= " AND oferte.recomandata = 'da' ";
		}
		if ( $this->id_hotel ) {
			$sel_filtru .= " AND hoteluri.id_hotel ='" . $this->id_hotel . "' ";
		}
		if ( $this->set_exceptieH ) {
			$sel_filtru .= " AND oferte.id_hotel <> '" . $this->set_exceptieH . "' ";
		}
		if ( $this->early == 'da' ) {
			$sel_filtru .= " AND early.discount is not null";
		}
		if ( $this->transport ) {
			$sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		}
		if ( $this->loc_p_avion ) {
			$sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		}
		if ( $this->loc_p_autocar ) {
			$sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		}
		if ( $this->data_plecarii ) {
			$sel_filtru .= " AND data_plecarii.data_start = '" . $this->data_plecarii . "' ";
		}
		if ( $this->stele ) {
			$sel_filtru .= " AND hoteluri.stele IN (" . $this->stele . ") ";
		}
		if ( $this->masa ) {
			$sel_filtru .= " AND LOWER(hotel_meal.masa) IN (" . $this->masa . ") ";
		}
		if ( $this->distanta ) {
			$sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $this->distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $this->distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $this->distanta[2] ) . "' ";
		}
		if ( $this->concept ) {
			$sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		}
		if ( $this->facilitati ) {
			$facilities = explode( ",", $this->facilitati );
			foreach ( $facilities as $k_facilities => $v_facilities ) {
				$v_facilities = str_replace( "'", "", trim( $v_facilities ) );
				$sel_filtru   = $sel_filtru . " AND facilities.facilitati LIKE '%" . $v_facilities . "%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		//echo $sel_filtru;
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$this->facilitati_filtru = '';
		$this->facilitati_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['facilitati'] ) {
				$facil = explode( ",", $row_filtru['facilitati'] );
				foreach ( $facil as $k_facil => $v_facil ) {
					$this->facilitati_filtru[ $v_facil ] = $this->facilitati_filtru[ $v_facil ] + 1;
				}
			}
		}
		@mysql_free_result( $rez_filtru );
		$time_end_facilitati = microtime( true );
		//	if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	{echo "filtru_facilitati=".$execution_time_facilitati = ($time_end_facilitati - $time_start_facilitati);}
		/********** facilitati_filtru **********/

		

?>