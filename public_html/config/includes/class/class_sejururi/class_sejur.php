<?php class OFERTA {
  var $id_tara;
  var $denumire_tara;
  var $id_zona;
  var $denumire_zona;
  var $id_localitate;
  var $denumire_localitate;
  var $id_hotel;
  var $nume_hotel;
  var $nr_stele;
  var $poza1;
  var $descriere_hotel;
  var $id_transport;
  var $denumire_transport;
  var $id_oferta;	
  var $denumire_oferta;
  var $descriere_hotel_lenght;
  var $nume_hotel_lenght;
  var $denumire_oferta_lenght;
  var $orase;
  var $exprimare_pret;
  var $denumire_tip_oferta;
  var $denumire_tip_oferta_lenght;

  function __construct($id_oferta) {
	  $this->id_oferta=$id_oferta;
	  $this->circuit='nu';
	  $this->tplG='';
  }
  function set_id_tara($value)
  {
	  $this->id_tara=$value;
  }
  function setFiltru(array $value) {
	 $this->filtru=$value;
  }
  function set_denumire_tara($value)
  {
	  $this->denumire_tara=$value;
  }
  function set_id_zona($value)
  {
	  $this->id_zona=$value;
  }
  function set_denumire_zona($value)
  {
	  $this->denumire_zona=$value;
  }
  function sed_orase($value)
  {
   $this->orase=$value;
   }
  function set_id_localitate($value)
  {
	  $this->id_localitate=$value;
  }
  function set_denumire_localitate($value)
  {
	  $this->denumire_localitate=$value;
  }
  function set_id_hotel($value)
  {
	  $this->id_hotel=$value;
  }
  function set_nume_hotel($value)
  {
	  $this->nume_hotel=$value;
  }
  function set_descriere_hotel($value)
  {
	  $this->descriere_hotel=strip_tags($value);
  }
  function set_nr_stele($value)
  {
	  $this->nr_stele=$value;
  }
  function set_poza1($value)
  {
	  $this->poza1=$value;
  }	
  function set_id_transport($value)
  {
	  $this->id_transport=$value;
  }
  function set_denumire_transport($value)
  {
	  $this->denumire_transport=$value;
  }
  function set_id_oferta($value)
  {
	  $this->id_oferta=$value;
  }
  function set_denumire_oferta($value)
  {
	  $this->denumire_oferta=$value;
  }
  function set_denumire_tip_oferta($value)
  {
	  $this->denumire_tip_oferta=$value;
  }	
  
  function set_descriere_hotel_lenght($value)
  {
	  $this->descriere_hotel_lenght=$value;
  }

  function set_nume_hotel_lenght($value)
  {
	  $this->nume_hotel_lenght=$value;
  }
  function set_denumire_oferta_lenght($value)
  {
	  $this->denumire_oferta_lenght=$value;
  }
  function set_denumire_localitate_lenght($value)
  {
	  $this->denumire_localitate_lenght=$value;
  }
  function set_denumire_zona_lenght($value)
  {
	  $this->denumire_zona_lenght=$value;
  }
  function set_denumire_tara_lenght($value)
  {
	  $this->denumire_tara_lenght=$value;
  }
  function set_denumire_tip_oferta_lenght($value)
  {
	  $this->denumire_tip_oferta_lenght=$value;
  }
  function set_exprimare_pret($value)
  {
	  $this->exprimare_pret=$value;
  }
  function set_oferta_speciala($value)
  {
	  $this->oferta_speciala=$value;
  }
  function setEarlyBooking($value)
  {
	$this->earlybooking=$value;
  }
  function setLastMinute($value)
  {
	$this->lastminute=$value;
  }
  function set_discount($value)
  {
   $this->discount=$value;
  }
  function set_earlyend($value)
  {
	$this->end_early=$value;
  }
  function set_nr_zile($value)
  {
	$this->nr_zile=$value;
  }
  function set_nr_nopti($value)
  {
	$this->nr_nopti=$value;
  }
  function set_pret($value)
  {
	$this->pret=$value;	
  }
  function set_moneda($value)
  {
	$this->moneda=$value;	
  }
  function set_durata($value)
  {
	$this->durata=$value;
  }  
  function set_masa($value)
  {
	$this->masa=$value;
  }
  
  function set_tip_activ($value) 
  {
		$this->tip_activ=$value;
  } 
  
  function set_titlu_dreapta($value) {
	$this->titlu_dreapta=$value;
  }
  function set_titlu_localizare($value) {
	$this->titlu_localizare=$value;
  }
  function set_tip_fiu($value) {
   $this->denumire_tip_fiu=$value;  
  }
  function set_denumire_continent($value)
  {
		$this->denumire_continent=$value;
  }
  function setCircuite($value) {
	$this->circuit='da';
  }
  function setNrOferte($value) {
	$this->nr_oferte=$value;  
  }
  function setDetaliiRecomandata($value) {
	$this->detalii_recomandata=$value;  
  }
  function creaza_link() {
  if($this->circuit=='nu') {  
  if($this->earlybooking<>'da') {
	  $linkul=$GLOBALS['sitepath_class'].fa_link($this->denumire_tara)."/".fa_link($this->denumire_localitate)."/".fa_link_oferta($this->denumire_oferta);
	  //if(strlen($linkul)>'100') $linkul=substr($linkul,0,100);
	  $linkul=$linkul."-".fa_link($this->id_oferta).".html";
  } else {
	$linkul="/hoteluri/".fa_link($this->denumire_tara)."/".fa_link($this->denumire_localitate)."/".fa_link_oferta(trim($this->nume_hotel))."-".fa_link($this->id_hotel).".html";
  }
  } else {
	$link=$GLOBALS['sitepath_class']."circuit/".fa_link($this->denumire_continent)."/".fa_link_circuit(trim($this->nume_hotel));
	//if(strlen($link)>'100') $link=substr($link,0,100);
	$link=$link."-".$this->id_oferta.".html";
	return $link;  
  }
	  return $linkul;
  }
  
  function setTplGrup($temp) {
	$this->tplG=$temp;  
  }
function afiseaza_sejur($template) {
if(($this->nr_oferte==1 && !$this->tplG) || $this->earlybooking=='da' || $this->oferta_speciala=='da') {	  
  $link=$this->creaza_link();
  $tpl = new TPL($template);
  if(!$this->pret) {
  $pret=pret_minim_sejur($this->id_oferta, '', '', '');
  if($pret[1]=='EURO') $moneda='&euro;'; elseif($pret[1]=='USD') $moneda='$'; else $moneda=$pret[1];
  $pret=$pret[0]." ".$moneda;
  } else {
  if($this->moneda=='EURO') $moneda='&euro;'; elseif($this->moneda=='USD') $moneda='$'; else $moneda=$this->moneda;
  $pret=$this->pret." ".$moneda; }
$stele='<span class="stele-mici-'.$this->nr_stele.'"></span>';
	  if($this->nr_zile>2 && $this->nr_nopti>1) {
	  $zile=$this->nr_zile; if($zile>'1') $zile=$zile." zile"; else  $zile=$zile." zi";
	  if($this->nr_nopti>1) { $nopti=$this->nr_nopti; if($nopti>'1') $nopti=$nopti." nopti"; else $nopti=$nopti." noapte"; $nr_nopti=$nopti; }
	  $dutara1='<p class="titlu">Durata:</p><p class="valoare">'.$zile.' / '.$nopti.'</p>';
	  $dutara='Durata sejur: <span style="font-weight:bold;">'.$zile.' / '.$nopti.'</span>'; }
	  if($this->masa) $masa=$this->masa; else $masa='';
	  $denumire_oferta=$this->denumire_oferta;
		  if($this->denumire_oferta_lenght) {
			  $denumire_oferta=substr($this->denumire_oferta,0,$this->denumire_oferta_lenght);
			  if(strlen($this->denumire_oferta)>$this->denumire_oferta_lenght) $denumire_oferta=$denumire_oferta."...";
		  }
	  $nume_hotel=$this->nume_hotel;
	  if($this->nume_hotel_lenght) {
		  $nume_hotel=substr($this->nume_hotel,0,$this->nume_hotel_lenght);
	  }
	  
	  $denumire_localitate=$this->denumire_localitate;
	  if($this->denumire_localitate_lenght) {
		  $denumire_localitate=substr($this->denumire_localitate,0,$this->denumire_localitate_lenght);
		  if(strlen($this->denumire_localitate)>$this->denumire_localitate_lenght) $denumire_localitate=$denumire_localitate."...";
	  }
	  if(trim(strtolower($this->denumire_localitate))<>trim(strtolower($this->denumire_zona)) ) {
	  $denumire_zona=$this->denumire_zona;
	  if($this->denumire_zona_lenght) {
		  $denumire_zona=substr($this->denumire_zona,0,$this->denumire_zona_lenght);
		  if(strlen($this->denumire_zona)>$this->denumire_zona_lenght) $denumire_zona=$denumire_zona."...";
	  }
	  
	  $denumire_zona=$denumire_zona." /";
	  } else $denumire_zona='';
	  $zona=$this->denumire_zona;
	  $denumire_tara=$this->denumire_tara;
	  if($this->denumire_tara_lenght) {
		  $denumire_tara=substr($this->denumire_tara,0,$this->denumire_tara_lenght);
		  if(strlen($this->denumire_tara)>$this->denumire_tara_lenght) $denumire_tara=$denumire_tara."...";
	  }
	  
	  $denumire_tip_oferta=$this->denumire_tip_oferta;
	  if($this->denumire_tip_oferta_lenght) {
		  $denumire_tip_oferta=substr($this->denumire_tip_oferta,0,$this->denumire_tip_oferta_lenght);
		  if(strlen($this->denumire_tip_oferta)>$this->denumire_tip_oferta_lenght) $denumire_tip_oferta=$denumire_tip_oferta."...";
	  }		
	  $denumire_hotel=$nume_hotel;
	  if(!$this->poza1) $poza=$GLOBALS['sitepath_class_parinte']."images/no_photo.jpg"; else $poza=$GLOBALS['sitepath_class_parinte'].'thumb_hotel/'.$this->poza1;
	  if($this->circuit=='da') { $this->titlu_dreapta = '&nbsp;'; $titlu_localizare='Continent: <strong>'.$this->denumire_continent.'</strong>';
	  } else if($this->titlu_localizare) $titlu_localizare='Localitate: <strong>'.$this->titlu_localizare.'</strong>'; else $titlu_localizare='&nbsp;';
	  
	  if($this->denumire_tip_oferta) $poza_fiu='<img src="'.$GLOBALS['sitepath_class'].'images/tematici/mici/'.fa_link($this->denumire_tip_oferta).'.jpg" style="border:1px solid #CCC" alt="'.$this->denumire_tip_oferta.'" title="'.$this->denumire_tip_oferta.'" />'; else $poza_fiu='&nbsp;';
   if($this->lastminute && $this->circuit<>'da') $den=$denumire_oferta; else $den=$denumire_hotel;
   $link_localitate=$GLOBALS['sitepath_class'].'oferte-'.$this->tip_activ.'/'.fa_link($this->denumire_tara).'/'.fa_link($this->denumire_zona).'/'.fa_link($this->denumire_localitate).'/';
   $link_tara=$GLOBALS['sitepath_class'].'oferte-'.$this->tip_activ.'/'.fa_link($this->denumire_tara).'/';
	  $tpl->replace_tags(array(
	  "id_oferta"=> $this->id_oferta,
	  "denumire_hotel"=> $denumire_hotel,
	  "denumire_oferta"=> $den,
	  "den_oferta"=>$denumire_oferta,
	  "nume_hotel"=> $denumire_hotel,
	  "stele"=>$stele,
	  "poza1"=>$poza,
	  "denumire_localitate"=> $denumire_localitate,
	  "denumire_zona"=> $denumire_zona,
	  "denumire_tara"=> $denumire_tara,
	  "denumire_transport"=> $this->denumire_transport,
	  "exprimare_pret"=>$this->exprimare_pret,
	  "tip_oferta"=>$denumire_tip_oferta,
	  "class"=>$this->clasa,
	  "link"=>$link,
	  "titlu_link"=>$this->denumire_oferta,
	  "oferta_speciala"=>$this->oferta_speciala,
	  "alta"=>$this->alta,
	  "pret_minim"=>$pret,
	  "pret_old"=>$pret_nou,
	  "procent_early" =>$this->discount."%",
	  "nr_zile" =>$dutara,
	  "nr_zile_nou"=>$dutara1,
	  "data_early" =>$this->end_early,
	  "tematica"=>$tip,
	  "masa"=>$masa,
	  "titlu_zona"=>$zona,
	  "titlu_dreapta"=>$this->titlu_dreapta,
	  "titlu_localizare"=>$titlu_localizare,
	  "tip_activ"=>$this->tip_activ,
	  "poza_fiu"=>$poza_fiu,
	  "denumire_continent"=>$this->denumire_continent,
	  "plecare"=>$this->detalii_recomandata,
	  "nr_nopti"=>$nr_nopti,
	  "link_localitate"=>$link_localitate,
	  "link_tara"=>$link_tara
	  ));
	  
	  $tpl->output_tpl();
  } else {
  if($this->tplG) $template=$this->tplG; else $template=$_SERVER['DOCUMENT_ROOT']."/templates/oferte_grupate.tpl";
  $tpl = new TPL($template);
  $linkul_h="/hoteluri/".fa_link($this->denumire_tara)."/".fa_link($this->denumire_localitate)."/".fa_link_oferta(trim($this->nume_hotel))."-".fa_link($this->id_hotel).".html";	 
  $stele='<span class="stele-mici-'.$this->nr_stele.'"></span>';
  if(!$this->poza1) $poza=$GLOBALS['sitepath_class_parinte']."images/no_photo.jpg"; else $poza=$GLOBALS['sitepath_class_parinte'].'thumb_hotel/'.$this->poza1;
  
  $denumire_localitate=$this->denumire_localitate;
   $denumire_localitate=$this->denumire_localitate;
   if($this->denumire_localitate_lenght) {
   $denumire_localitate=substr($this->denumire_localitate,0,$this->denumire_localitate_lenght);
   if(strlen($this->denumire_localitate)>$this->denumire_localitate_lenght) $denumire_localitate=$denumire_localitate."...";}
   if(trim(strtolower($this->denumire_localitate))<>trim(strtolower($this->denumire_zona)) ) {
	  $denumire_zona=$this->denumire_zona;
	  if($this->denumire_zona_lenght) {
		  $denumire_zona=substr($this->denumire_zona,0,$this->denumire_zona_lenght);
		  if(strlen($this->denumire_zona)>$this->denumire_zona_lenght) $denumire_zona=$denumire_zona."...";
	  }
	  
	  $denumire_zona=$denumire_zona." /";
	  } else $denumire_zona='';
	  $zona=$this->denumire_zona;
	  $denumire_tara=$this->denumire_tara;
	  if($this->denumire_tara_lenght) {
		  $denumire_tara=substr($this->denumire_tara,0,$this->denumire_tara_lenght);
		  if(strlen($this->denumire_tara)>$this->denumire_tara_lenght) $denumire_tara=$denumire_tara."...";
	  }
   $nume_hotel=$this->nume_hotel;
	if($this->nume_hotel_lenght) {
	$nume_hotel=substr($this->nume_hotel,0,$this->nume_hotel_lenght); }
    $selOf="select 
	oferte.id_oferta, 
	oferte.denumire,
	oferte.nr_zile,
	oferte.nr_nopti,
	transport.denumire as denumire_transport,
	oferte.masa,
	oferte.pret_minim,
	oferte.moneda,
	oferte.exprimare_pret,
	oferte.cazare,
	tip_oferta.denumire_tip_oferta,
	oferte.denumire_scurta
	from
	oferte
	inner join transport on oferte.id_transport = transport.id_trans
	left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	left join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
	where
	oferte.valabila = 'da' and
	oferte.last_minute = 'nu' and
	oferte.id_hotel = '".$this->id_hotel."' ";
	if($this->filtru['id_tip'])  $selOf= $selOf." and oferta_sejur_tip.id_tip_oferta in (".$this->filtru['id_tip'].") ";
	if($this->filtru['masa'])  $selOf= $selOf." and TRIM(LOWER(oferte.masa)) = '".$this->filtru['masa']."' ";
	if($this->filtru['id_transport'])  $selOf= $selOf." and oferte.id_transport = '".$this->filtru['id_transport']."' ";
	 $selOf= $selOf." Group by oferte.id_oferta
	Order by transport.denumire, oferte.masa, oferte.nr_zile, oferte.nr_nopti, oferte.pret_minim_lei ";
	$queOf=mysql_query($selOf) or die(mysql_error()); $i=0;
	$nr_oferte=mysql_num_rows($queOf);
	while($rowOf=mysql_fetch_array($queOf)) { $loc='';
	if($rowOf['denumire_transport']=='Avion') {
$sel="select GROUP_CONCAT(DISTINCT localitati.denumire Order by oferte_transport_avion.ordonare SEPARATOR ', ') as loc_plecare
from
oferte_transport_avion
left join aeroport on oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
left join localitati on aeroport.id_localitate = localitati.id_localitate
where
oferte_transport_avion.id_oferta = '".$rowOf['id_oferta']."' and oferte_transport_avion.tip = 'dus' ";
$que=mysql_query($sel) or die(mysql_error());
$loc_plecare=mysql_fetch_array($que);
@mysql_free_result($que);
if($loc_plecare['loc_plecare']) $loc=' - plecare '.$loc_plecare['loc_plecare'];
}
	if($nr_t%2==1) $class_tr='impar'; else $class_tr='par';
$numar_minim_nopti='';	
	if($rowOf['pret_minim']) { if($rowOf['moneda']=='EURO') $moneda='&euro;'; elseif($rowOf['moneda']=='USD') $moneda='$'; else $moneda=$rowOf['moneda'];
	$pret=$rowOf['pret_minim'].' '.$moneda; } else {
	$pret=pret_minim_sejur($this->id_oferta, '', '', '');
  if($pret[1]=='EURO') $moneda='&euro;'; elseif($pret[1]=='USD') $moneda='$'; else $moneda=$pret[1];
  $pret=$pret[0]." ".$moneda; }
	if($rowOf['cazare']=='nu') { if($rowOf['nr_nopti']>0) { $zile=$rowOf['nr_nopti']; if($rowOf['nr_nopti']>1) $zile=$zile.' nopti'; else $zile=$zile.' noapte';
	
	if($rowOf['denumire_scurta'] && $this->tplG) $zile=$rowOf['denumire_scurta'].' '.$zile;  }
	} else $zile ='Cazare';
	$linkul=$GLOBALS['sitepath_class'].fa_link($this->denumire_tara)."/".fa_link($this->denumire_localitate)."/".fa_link_oferta($rowOf['denumire']);
	  if(strlen($linkul)>'100') $linkul=substr($linkul,0,100);
	  $linkul=$linkul."-".$rowOf['id_oferta'].".html";
	 $oferte=$oferte.'<tr onmouseover="this.className=\'hover\';" onmouseout="this.className=\''.$class_tr.'\';" class="'.$class_tr.'"  onclick="document.location.href=\''.$linkul.'\'" style="cursor: pointer;">
	 <td align="left" valign="top" class="vtip nowrap" title="'.$rowOf['denumire'].'"><a href="'.$linkul.'"  class="link-blue">'.$zile.'</a></td>
	 <td align="center" valign="top" class="vtip" title="'.$rowOf['denumire'].'">'.$rowOf['masa'].'</td>
	 <td align="center" valign="top" class="vtip" title="'.$rowOf['denumire'].'">'.$rowOf['denumire_transport'].$loc.'</td>
	 <td align="center" valign="top" class="last vtip" title="'.$rowOf['denumire'].'"><span class="pret">'.$pret.'</span> '.$rowOf['exprimare_pret'].'</td>
	</tr>';	
	} @mysql_free_result($queOf);
   $tpl->replace_tags(array(
   "denumire_hotel"=> $nume_hotel,
   "denumire_zona"=> $denumire_zona,
   "denumire_localitate"=> $denumire_localitate,
   "stele"=>$stele,
   "poza1"=>$poza,
   "oferte"=>$oferte,
   "titlu_link"=>'Oferte '.$nume_hotel,
   "link"=>$linkul_h
   ));
	  
	  $tpl->output_tpl();
  }  
 }
}

class AFISARE_SEJUR {

function __construct() { //se creaza paginatia_______________________________________________
$this->tip_oferta=0;
$this->tari=0;
$this->zone=0;
$this->azi=date("Y-m-d");
$this->last_minute=0;
$this->random=0;
$this->oferta_speciala=0;
$this->pagini_id_uri="";
$this->afisare_no_limit=0;
$this->early='nu';
$this->cazare='nu';
$this->oferte_pagina=0;
$this->templates="afisare_oferta_lunga.tpl";
$this->denumire_tip_oferta='';
$this->denTara='';
$this->denZone='';
$this->denTransport='';
$this->denOrase='';
$this->dimensiuneNumeHotel='';
$this->tip_oferta1='';
$nr_tot=0;
$this->set_circuit='';
$this->tari_circ='';
$this->id_tara_circuit='';
$this->last_minute=0;
$this->search='';
$this->nr_aparitiiP=2;
$this->arrF=array();
$this->filtruA=array();
$this->recomandata='';
}
  
  //funtii pentru sortarea ofertelor__________________________
  function set_oferte_pagina($value)
  {
	$this->oferte_pagina=$value;
  }
  function set_cazare($value)
  {
	$this->cazare=$value;
  }
  function setRecomandata($value)
  {
	  $this->recomandata=$value;
  }
  function setLastMinute()
  {
	  $this->last_minute=1;
  }
  function setRandom()
  {
	  $this->random=1;
  }
  
  function setIdFav($value)
  {
	  $this->favorite_id=$value;
  }
  
	  
  function setTipOferta($tip)
  {
	  $id_parinte=get_id_tip_sejur(desfa_link($tip));
	  $this->tip_oferta="'".$id_parinte."'";
	  $this->denumire_tip_oferta=$tip;
	  $this->tip_oferta1=get_id_tip_sejur_fii($id_parinte); if($this->tip_oferta1['id_tipuri']) $this->tip_oferta=$this->tip_oferta.','.$this->tip_oferta1['id_tipuri'];
	  $this->filtruA['id_tip']=$this->tip_oferta;
  }
  
  function setKeywords($keyword_old)
  {
		  if(strlen($keyword_old)>2) $this->keywords=$keyword_old;
  }
  
  
  function setTari($tari)
  {
	  $this->id_tara=get_id_tara(desfa_link($tari));
	  $this->denTara=$tari;
	  $this->tari="'".$this->id_tara."'";
  }

  function setTariCircuit(array $tari)
  {
	  foreach($tari as $key=>$value) {
	  if($value) {
	  $this->id_tara_circuit=$this->id_tara_circuit."'".get_id_tara(desfa_link($value))."',"; } }
	$this->id_tara_circuit=substr($this->id_tara_circuit,0,-1);
  }
	  
  function setZone($zone)
  {
	  $this->id_zone=get_id_zona(desfa_link($zone), $this->id_tara);
	  $this->zone="'".$this->id_zone."'";
	  $this->denZone=$zone;
	  
  }
  
  function setOrase($orase)
  {
	  $this->orase="'".get_id_localitate(desfa_link($orase), $this->id_zone)."'";
	  $this->denOrase=$orase;
  }
  
  function setOfertaSpeciala($value)
  {
	  $this->oferta_speciala=$value;
  }
  
  function setIduri($value)
  {
	  $i=0;
	  $this->iduri="'".$value[$i]."'";
	  $i++;
	  while($value[$i])
	  {
		  $this->iduri=$this->iduri.", '".$value[$i]."'";
		  $i++;
	  }
	  
  }
  
  function setTransport($value)
  {
	  $id_transport=get_id_transport(desfa_link($value));
	  $this->transport="'".$id_transport."'";
	  $this->filtruA['id_transport']=$id_transport;
	  $this->denTransport=$value;
  }
  
  function setIdHotel($value)
  {
	 $this->id_hotel=$value;
  }
  function setEarly($value)
  {
	 $this->early=$value;
  }
  function setMasa($value)
  {
	$this->masa=desfa_link($value);
	$this->filtruA['masa']=$this->masa;
  }
  function setOrdonarePret($value)
  {
	$this->ord_pret=$value;
  }
  function setOrdonareStele($value)
  {
	  $this->ord_stele=$value;
  }
  function setOrdonareRelevanta($value)
  {
	  $this->ord_relevanta=$value;
  }
  function setOrdonareNumeH($value)
  {
	 $this->ord_numeH=$value;
  }
  function setLocalitatiPlecare($value)
  {
	  $this->localitati_plecare=get_id_localitate(desfa_link($value));
	  $this->denLocalitatiPlecare=$value;
  }
  function setStele($value)
  {
	  $this->stele=$value;
  }
  function set_oferte_vizitate($value)
  {
	$this->oferte_vizitate=$value;	
  }
  function setDimensiuneNumeHotel($value)
  {
	 $this->dimensiuneNumeHotel=$value;
  }
  function setCircuit($value)
  {
	 $this->set_circuit=$value;
  }
  function setExceptie($value)
  {
	 $this->set_exceptie=$value;
  }
  function setExceptieH($value)
  {
	 $this->set_exceptieH=$value;
  }
  function setContinent($value) {
   $this->continent="'".get_id_continent(desfa_link($value))."'";
   $this->denContinent=$value;
  }
  
  function setTaraCircuit($value) {
	  $this->tari_circ="'".get_id_tara(desfa_link($value))."'";
	  $this->denTaraCircuit=$value;
  }
  
  function setPlecare($value) {
	$this->plecare=$value;  
  }
  function setStartDate($value) {
	$this->start=$value;  
  }
  function setEndDate($value) {
	$this->endd=$value;  
  }
  function setSearch($val)
  {
    $this->search=$val;
  }
  //selectam ofertele care indeplinesc conditiile date
  function sel_toate_nr() {
	$sel_nr="SELECT
	oferte.id_oferta,
	oferte.id_hotel,
	hoteluri.stele,
    transport.denumire as denumire_transport,
    transport.id_trans,
    oferte.masa,
	oferte.nr_zile,
	oferte.nr_nopti,
	hoteluri.distanta_fata_de,
	hoteluri.distanta
	FROM 
	oferte ";
	if($this->tip_oferta) $sel_nr=$sel_nr." inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
	if($this->plecare) $sel_nr=$sel_nr." inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and data_pret_oferta.data_start = '".$this->plecare."') ";
	if($this->early=='da')  $sel_nr=$sel_nr." inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur') ";
	if($this->start) $sel_nr=$sel_nr." inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and (data_pret_oferta.data_start<='".$this->start."' and data_pret_oferta.data_end>='".$this->endd."')) ";
	$sel_nr=$sel_nr."
	inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	inner Join zone ON localitati.id_zona = zone.id_zona
	inner join tari on zone.id_tara = tari.id_tara
	inner join transport on oferte.id_transport = transport.id_trans ";
	if($this->tari_circ) $sel_nr=$sel_nr." Left Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
	if($this->id_tara_circuit) $sel_nr=$sel_nr." Inner Join traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte and traseu_circuit.id_tara in (".$this->id_tara_circuit.") and traseu_circuit.tara_principala = 'da') ";
	$sel_nr=$sel_nr." WHERE
	oferte.valabila = 'da' ";
	if(!$this->search && !$this->last_minute && $this->early<>'da' && !$this->recomandata) { if(!$this->set_circuit) $sel_nr=$sel_nr." and hoteluri.tip_unitate <> 'Circuit' "; else $sel_nr=$sel_nr." and hoteluri.tip_unitate = 'Circuit' "; }
	if($this->last_minute) $sel_nr=$sel_nr." and oferte.last_minute = 'da' ";
	if($this->cazare=='da') $sel_nr=$sel_nr." and oferte.cazare = 'da' ";
	if($this->masa)  $sel_nr=$sel_nr." and oferte.masa = '".$this->masa."' ";
	if($this->continent) $sel_nr=$sel_nr." and hoteluri.id_continent = ".$this->continent." ";
	if($this->tip_oferta) $sel_nr=$sel_nr." AND oferta_sejur_tip.id_tip_oferta in (".$this->tip_oferta.") ";
	if($this->tari && !$this->tari_circ) $sel_nr=$sel_nr." AND zone.id_tara = ".$this->tari." ";
	if(!$this->tari && $this->tari_circ) $sel_nr=$sel_nr." AND  traseu_circuit.id_tara = ".$this->tari_circ." ";
	if($this->tari && $this->tari_circ) $sel_nr=$sel_nr." AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (".$this->tari.")) OR (traseu_circuit.id_tara = ".$this->tari_circ." and traseu_circuit.tara_principala = 'da')) ";
	if($this->zone) $sel_nr=$sel_nr." AND zone.id_zona = ".$this->zone." ";
	if($this->orase)  $sel_nr=$sel_nr." AND localitati.id_localitate = ".$this->orase." ";
	 if($this->recomandata) $sel_nr=$sel_nr." and oferte.recomandata = 'da' ";
	if($this->transport) $sel_nr=$sel_nr." AND oferte.id_transport = ".$this->transport." ";
	if($this->id_hotel) $sel_nr=$sel_nr." AND hoteluri.id_hotel ='".$this->id_hotel."' ";
	if($this->stele) $sel_nr=$sel_nr." and hoteluri.stele ='".$this->stele."' ";
	if($this->set_exceptieH) $sel_nr=$sel_nr." AND oferte.id_hotel <> '".$this->set_exceptieH."' ";
	if($this->keywords){
	  $sel_nr=$sel_nr."\n AND ( localitati.denumire LIKE '%".$this->keywords."%' ";
	  $sel_nr=$sel_nr."\n OR zone.denumire LIKE '%".$this->keywords."%' ";
	  $sel_nr=$sel_nr."\n OR tari.denumire LIKE '%".$this->keywords."%' )";
	}
	if(!$this->last_minute)  $sel_nr=$sel_nr." Group by oferte.id_hotel"; else $sel_nr=$sel_nr." Group by oferte.id_oferta";
  //echo $sel_nr;
  return $sel_nr;		
  }
  function sel_toate_pagina() {
  $sel_toate="SELECT
  count(DISTINCT oferte.id_oferta) as nr_normale,
  oferte.id_oferta,
  oferte.denumire AS denumire_oferta,
  oferte.exprimare_pret,
  oferte.nr_zile,
  oferte.nr_nopti,
  oferte.masa,
  oferte.last_minute,
  hoteluri.id_hotel,
  hoteluri.poza1,
  hoteluri.nume as denumire_hotel,
  hoteluri.stele,
  hoteluri.descriere as descriere_hotel,
  continente.nume_continent,
  hoteluri.tip_unitate,
  oferte.exprimare_pret,
  tari.denumire as denumire_tara,
  zone.denumire as denumire_zona,
  zone.apare_templait_dreapta,
  localitati.denumire as denumire_localitate,
  oferte.pret_minim,
  oferte.moneda,
  oferte.detalii_recomandata, ";
  if($this->start) $sel_toate=$sel_toate."
  preturi.pret as pret_minim,
  preturi.moneda, ";
  if($this->tip_oferta)  $sel_toate=$sel_toate." tip_oferta.denumire_tip_oferta,
  tip_oferta.parinte, ";
  if($this->early=='da' && !$this->oferta_speciala) $sel_toate=$sel_toate." early.discount, 
  date_format(early.end_date, '%d/%m/%Y')  as early_end, ";
  $sel_toate=$sel_toate." transport.denumire as denumire_transport
  FROM
  oferte ";
  if($this->tip_oferta) $sel_toate=$sel_toate." inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta inner join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta ";
  if($this->plecare) $sel_toate=$sel_toate." inner join data_pret_oferta on (oferte.id_oferta = data_pret_oferta.id_oferta and data_pret_oferta.data_start = '".$this->plecare."') ";
  if($this->early=='da' && !$this->oferta_speciala) $sel_toate=$sel_toate." inner Join (select * from early_booking where early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur' Group by id_oferta, end_date, discount Order by end_date) as early on oferte.id_oferta = early.id_oferta ";
  if($this->start) $sel_toate=$sel_toate." inner join(select data_pret_oferta.pret, data_pret_oferta.moneda, data_pret_oferta.id_oferta from data_pret_oferta where data_pret_oferta.data_start<='".$this->start."' and data_pret_oferta.data_end>='".$this->endd."' Group by data_pret_oferta.id_oferta Order by data_pret_oferta.pret) as preturi on oferte.id_oferta = preturi.id_oferta ";
  $sel_toate=$sel_toate." inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
  inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
  inner Join zone ON localitati.id_zona = zone.id_zona
  inner Join tari ON zone.id_tara = tari.id_tara
  left join continente on hoteluri.id_continent = continente.id_continent
  inner join transport on oferte.id_transport = transport.id_trans ";
  if($this->tari_circ) $sel_toate=$sel_toate."Left Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
  if($this->id_tara_circuit) $sel_toate=$sel_toate." Inner Join traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte and traseu_circuit.id_tara in (".$this->id_tara_circuit.") and traseu_circuit.tara_principala = 'da') ";
  $sel_toate=$sel_toate." WHERE
  oferte.valabila = 'da' ";
  if(!$this->search && !$this->last_minute && $this->early<>'da' && !$this->recomandata) { if(!$this->set_circuit) $sel_toate=$sel_toate." and hoteluri.tip_unitate <> 'Circuit' "; else $sel_toate=$sel_toate." and hoteluri.tip_unitate = 'Circuit' "; }
  if($this->cazare=='da') $sel_toate=$sel_toate." and oferte.cazare = 'da' ";
  if($this->last_minute) $sel_toate=$sel_toate." and oferte.last_minute = 'da' ";
  if($this->continent) $sel_toate=$sel_toate." and hoteluri.id_continent = ".$this->continent." ";
  if($this->masa)  $sel_toate=$sel_toate." and oferte.masa = '".$this->masa."' ";
  if($this->tip_oferta) $sel_toate=$sel_toate." AND oferta_sejur_tip.id_tip_oferta in (".$this->tip_oferta.") ";
  if($this->stele) $sel_toate=$sel_toate." and hoteluri.stele ='".$this->stele."' ";
  if($this->tari && !$this->tari_circ) $sel_toate=$sel_toate." AND tari.id_tara IN (".$this->tari.")";
  if($this->tari_circ && !$this->tari) $sel_toate=$sel_toate." AND traseu_circuit.id_tara = ".$this->tari_circ." ";
  if($this->tari && $this->tari_circ) $sel_toate=$sel_toate." AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (".$this->tari.")) OR (traseu_circuit.id_tara = ".$this->tari_circ." and traseu_circuit.tara_principala = 'da')) ";
  if($this->zone) $sel_toate=$sel_toate." AND zone.id_zona IN (".$this->zone.")";
  if($this->recomandata) $sel_toate=$sel_toate." and oferte.recomandata = 'da' ";
  if($this->orase)  $sel_toate=$sel_toate." AND localitati.id_localitate IN (".$this->orase.")";
  if($this->transport) $sel_toate=$sel_toate." AND oferte.id_transport = ".$this->transport." ";
  if($this->id_hotel) $sel_toate=$sel_toate." AND hoteluri.id_hotel  ='".$this->id_hotel."' ";
  if($this->random) $sel_toate=$sel_toate." AND hoteluri.poza1 is not null ";
  if($this->set_exceptie) $sel_toate=$sel_toate." AND oferte.id_oferta <> '".$this->set_exceptie."' ";
  if($this->set_exceptieH) $sel_toate=$sel_toate." AND oferte.id_hotel <> '".$this->set_exceptieH."' ";
  if($this->keywords){
		  $sel_toate=$sel_toate."\n AND ( localitati.denumire LIKE '%".$this->keywords."%' ";
		  $sel_toate=$sel_toate."\n OR zone.denumire LIKE '%".$this->keywords."%' ";
		  $sel_toate=$sel_toate."\n OR hoteluri.nume LIKE '%".$this->keywords."%' ";
		  $sel_toate=$sel_toate."\n OR oferte.denumire LIKE '%".$this->keywords."%' ";
		  $sel_toate=$sel_toate."\n OR tari.denumire LIKE '%".$this->keywords."%' )";
  }
  //echo $sel_toate;
  return $sel_toate;
 }

function initializare() {
//aflam cate oferte avem selectate. se lucreaza cu 3 selecturi fiind 3 tipuri de oferte pe o pagina.
$sel_toate=$this->sel_toate_nr();
$que_toate=mysql_query($sel_toate) or die(mysql_error());
$nr_hot=0;
while($row_toate=mysql_fetch_array($que_toate)) {
if(!$this->arrF['hotel'][$row_toate['id_hotel']]) { ++$nr_hot; $this->arrF['hotel'][$row_toate['id_hotel']]='da'; }
$this->trans_nou[$row_toate['denumire_transport']]=$this->trans_nou[$row_toate['denumire_transport']]+1;
$this->stele_nou[$row_toate['stele']]=$this->stele_nou[$row_toate['stele']]+1;
$this->masa_nou[$row_toate['masa']]=$this->masa_nou[$row_toate['masa']]+1;
$zile=$row_toate['nr_zile']; if($row_toate['nr_zile']>1) $zile=$zile.' zile'; else $zile=$zile.' zi'; if($row_toate['nr_nopti']) { $zile=$zile.' / '.$row_toate['nr_nopti']; if($row_toate['nr_nopti']>1) $zile=$zile.' nopti'; else $zile=$zile.' noapte'; }
$this->durata_nou[$zile]=$this->durata_nou[$zile]+1;
if($row_toate['distanta']<100) $z=1;
 elseif($row_toate['distanta']<300) $z=2;
  elseif($row_toate['distanta']>=300) $z=3;
$this->distanta[trim($row_toate['distanta_fata_de'])][$z]=$this->distanta[trim($row_toate['distanta_fata_de'])][$z]+1;
} 
$this->nr_tot=mysql_num_rows($que_toate);
@mysql_free_result($que_toate);
 
if($this->nr_tot>'0') {
$this->pag=floor($this->nr_tot/$this->oferte_pagina);
if(fmod($this->nr_tot,$this->oferte_pagina)!='0') $this->pag=$this->pag+1; 
 } else $this->pag=0;

return $this->nr_tot;
}
function get_filtru() {
  return $filtru=array('masa'=>$this->masa_nou, 'trans'=>$this->trans_nou, 'stele'=>$this->stele_nou, 'durata'=>$this->durata_nou, 'distanta'=>$this->distanta);
}
function config_paginare($tip) {
 $this->classpaginare=$tip;
 $this->nr_aparitiiP=1;
}
function paginare() {
// se adauga div-le si se initializeaza afisarea
$this->initializare();
$cond="";
if($this->classpaginare) $cond=$cond."&conditie[config_paginare]=".$this->classpaginare;
if($this->cazare) $cond=$cond."&conditie[set_cazare]=".$this->cazare;
if($this->denumire_tip_oferta) $cond=$cond."&conditie[setTipOferta]=".$this->denumire_tip_oferta;
if($this->keywords) $cond=$cond."&conditie[setKeywords]=".$this->keywords;
if($this->denTara) $cond=$cond."&conditie[setTari]=".$this->denTara;
if($this->denZone) $cond=$cond."&conditie[setZone]=".$this->denZone;
if($this->denOrase) $cond=$cond."&conditie[setOrase]=".$this->denOrase;
if($this->denTransport) $cond=$cond."&conditie[setTransport]=".$this->denTransport;
if($this->id_hotel) $cond=$cond."&conditie[setIdHotel]=".$this->id_hotel;
if($this->early) $cond=$cond."&conditie[setEarly]=".$this->early;
if($this->masa) $cond=$cond."&conditie[setMasa]=".fa_link($this->masa);
if($this->ord_stele) $cond=$cond."&conditie[setOrdonareStele]=".$this->ord_stele;
if($this->ord_relevanta) $cond=$cond."&conditie[setOrdonareRelevanta]=".$this->ord_relevanta;
if($this->ord_pret) $cond=$cond."&conditie[setOrdonarePret]=".$this->ord_pret;
if($this->set_circuit) $cond=$cond."&conditie[setCircuit]=".$this->set_circuit;
if($this->continent) $cond=$cond."&conditie[setContinent]=".$this->denContinent;
if($this->denTaraCircuit) $cond=$cond."&conditie[setTaraCircuit]=".$this->denTaraCircuit;
if($this->start) $cond=$cond."&conditie[setStartDate]=".$this->start;
if($this->endd) $cond=$cond."&conditie[setEndDate]=".$this->endd;
if($this->stele) $cond=$cond."&conditie[setStele]=".$this->stele;
if($this->dimensiuneNumeHotel) $cond=$cond."&conditie[setDimensiuneNumeHotel]=".$this->dimensiuneNumeHotel;
if($this->search=='da') $cond=$cond."&conditie[setSearch]=da";

$total=$this->nr_tot;
if($total<>'1') $den_of="oferte"; else $den_of="oferta";
if($this->pag<>'1') $den_pag="pagini"; else $den_pag="pagina";
if(!$this->classpaginare) { ?>
<div class="paginationLine">
  <div class="paginationInfo"><?php echo $total.' '.$den_of.' | '.$this->pag.' '.$den_pag; ?></div>
  <div id="paginatie" class="pagination">&nbsp;</div>
</div>
<br class="clear" />
<?php /*?><div id="loader" class="loading">&nbsp;</div><?php */?>
<!--<script type="text/javascript">
$('#loader').show();
//]]>
</script>-->
<div id="afisare"><?php $this->afisare(1); ?></div>
<br class="clear" />
<div class="paginationLine">
  <div class="paginationInfo"><?php echo $total.' '.$den_of.' | '.$this->pag.' '.$den_pag; ?></div>
  <div id="paginatie1" class="pagination">&nbsp;</div>
</div>
<?php } else { ?>
 <div class="filter-pag">
    <div class="filter">
      <strong>Ordoneaza dupa: &nbsp; </strong>
      <form name="ord" id="ord" method="get" action="<?php echo 'localitate_new1.php'; ?>">
      <input type="hidden" name="tari" value="<?php echo $this->denTara; ?>" />
      <input type="hidden" name="zone" value="<?php echo $this->denZone; ?>" />
      <input type="hidden" name="oras" value="<?php echo $this->denOrase; ?>" />
      <?php if($this->denTransport) { ?> <input type="hidden" name="transport" value="<?php echo $this->denTransport; ?>" /><?php }
      if($this->masa) { ?> <input type="hidden" name="masa" value="<?php echo fa_link($this->masa); ?>" /><?php }
	  if($this->stele) { ?> <input type="hidden" name="stele" value="<?php echo $this->stele; ?>" /><?php } ?>
      <select name="ordonare" onchange="document.ord.submit();">
        <option value="data-introducere" <?php if(!$this->ord_numeH && !$this->ord_pret) { ?> selected="selected"<?php } ?>>Top vanzari</option>
        <option value="tip_pret-asc" <?php if($this->ord_pret=='asc') { ?> selected="selected" <?php } ?>>Pret crescator</option>
        <option value="tip_pret-desc" <?php if($this->ord_pret=='desc') { ?> selected="selected" <?php } ?>>Pret descrescator</option>
        <option value="tip_numH-asc" <?php if($this->ord_numeH=='asc') { ?> selected="selected" <?php } ?>>Nume hotel crescator</option>
        <option value="tip_numH-desc" <?php if($this->ord_numeH=='desc') { ?> selected="selected" <?php } ?>>Nume hotel descrescator</option>
      </select>
     </form>
    </div>
  <div class="paginatie" align="right" id="paginatie">&nbsp;</div>
  <br class="clear" />
 </div>
 <br class="clear" /><br />
 <div id="afisare"><?php $this->afisare(1); ?></div>
 <br class="clear" />
<?php } ?>
<?php if($this->pag>'1') { ?>
<script type="text/javascript" src="/js/paginare.js"></script>
<script type="text/javascript">
//<![CDATA[
makepagini('<?php echo $this->pag; ?>','1',<?php echo $this->nr_aparitiiP; ?>);
function clic(pag) {
<!--$('#loader').show();-->
$("#afisare").load("/config/includes/class/class_sejururi/afisare.php?nr_pagina="+pag+"&nr_pe_pag=<?php echo $this->oferte_pagina.$cond; ?>");
}
//]]>
</script>
<?php
 }

}
function numar_oferte_total()
{  
  $sel_toate=$this->sel_toate_pagina();
  $cond_normala=$cond_normala." GROUP BY oferte.id_oferta limit 1 ";
  //echo $sel_toate.$cond_normala;
  $que_normal=mysql_query($sel_toate.$cond_normala) or die(mysql_error());
  $nr_tot=mysql_num_rows($que_normal);
  @mysql_free_result($que_normal);
  return $nr_tot;
}


function fara_paginare() { ?>
<div id="afisare"><?php $this->afisare(1); ?></div>
<?php }

function afisare($pagina) { ?>
<!--<script type="text/javascript">
//<![CDATA[
$('#loader').hide();
//]]>
</script>-->
<?php $sel_toate=$this->sel_toate_pagina();

$from=$this->reverse_calculeaza_pagina($pagina, $this->oferte_pagina);
if($this->oferte_vizitate) $cond_normala=" and oferte.id_oferta in (".$this->oferte_vizitate.") "; 
if(!$this->last_minute) $cond_normala=$cond_normala." GROUP BY oferte.id_hotel "; else $cond_normala=$cond_normala." GROUP BY oferte.id_oferta";
if($this->ord_stele) $cond_normala=$cond_normala." ORDER BY hoteluri.stele ".$this->ord_stele." ";
elseif($this->ord_relevanta) $cond_normala=$cond_normala." ORDER BY oferte.click ".$this->ord_relevanta." ";
elseif($this->ord_pret) $cond_normala=$cond_normala." ORDER BY oferte.pret_minim_lei ".$this->ord_pret." ";
elseif($this->ord_numeH) $cond_normala=$cond_normala." ORDER BY hoteluri.nume ".$this->ord_numeH." ";
elseif($this->random) $cond_normala=$cond_normala." ORDER BY RAND() ";
else $cond_normala=$cond_normala." ORDER BY oferte.ultima_modificare DESC, oferte.data_adaugarii DESC "; 
$cond_normala=$cond_normala." LIMIT $from, $this->oferte_pagina ";
$sel_normal=$sel_toate.$cond_normala;
$que_normal=mysql_query($sel_normal) or die(mysql_error());
$nr_tot=mysql_num_rows($que_normal); 
if($nr_tot<'1' && !$this->oferta_speciala) {
  $this->eroare();
} else {
  if($nr_tot>'0') {
	  while($row_normal=mysql_fetch_array($que_normal)){
		  $oferta=new OFERTA($row_normal["id_oferta"]);
		  if($row_normal['tip_unitate']=='Circuit') $oferta->setCircuite('da');
		  $oferta->setNrOferte($row_normal['nr_normale']);
		  if(sizeof($this->filtruA)>0) $oferta->setFiltru($this->filtruA);
		  $oferta->set_denumire_continent($row_normal["nume_continent"]);
		  $oferta->set_id_hotel($row_normal["id_hotel"]);
		  $oferta->set_denumire_oferta($row_normal["denumire_oferta"]);
		  $oferta->set_nr_zile($row_normal["nr_zile"]);
		  $oferta->set_nr_nopti($row_normal["nr_nopti"]);
		  $oferta->set_poza1($row_normal["poza1"]);
		  $oferta->set_nume_hotel($row_normal["denumire_hotel"]);
		  if($this->dimensiuneNumeHotel) $oferta->set_nume_hotel_lenght($this->dimensiuneNumeHotel);
		  $oferta->set_denumire_localitate($row_normal["denumire_localitate"]);
		  if($this->denumire_tip_oferta) $oferta->set_tip_activ($this->denumire_tip_oferta);
		  $oferta->set_masa($row_normal["masa"]);
		  $oferta->set_denumire_zona($row_normal["denumire_zona"]);
		  $oferta->set_denumire_tara($row_normal["denumire_tara"]);
		  $oferta->set_descriere_hotel($row_normal["descriere_hotel"]);
		  $oferta->set_nr_stele($row_normal["stele"]);
		  $oferta->set_denumire_transport($row_normal["denumire_transport"]);
		  $oferta->set_descriere_hotel_lenght(255);
		  if($row_normal["parinte"] && $this->tip_oferta1['id_tipuri']) $oferta->set_denumire_tip_oferta($row_normal["denumire_tip_oferta"]);
		  $oferta->set_exprimare_pret($row_normal["exprimare_pret"]);
		  $oferta->set_pret($row_normal["pret_minim"]);
		  $oferta->set_moneda($row_normal["moneda"]);
		  $oferta->setDetaliiRecomandata($row_normal['detalii_recomandata']);
		  $oferta->set_denumire_oferta_lenght(65);
		  if($row_normal['tip_unitate']=='Circuit') $oferta->set_nume_hotel_lenght(85); else $oferta->set_nume_hotel_lenght(65);
		  if($row_normal['apare_templait_dreapta']=='da') {
			$oferta->set_titlu_dreapta($row_normal["denumire_zona"]);
			$oferta->set_titlu_localizare($row_normal["denumire_localitate"]);  
		  } else {
		 $oferta->set_titlu_dreapta($row_normal["denumire_localitate"]); }
		  if($row_normal["discount"] && $row_normal["discount"]<>'0' && !$this->oferta_speciala) {
		  $oferta->setEarlyBooking('da');
		  $oferta->set_discount($row_normal["discount"]);
		  $oferta->set_earlyend($row_normal["early_end"]);
		  $oferta->afiseaza_sejur($_SERVER['DOCUMENT_ROOT']."/templates/new_turism_extern_tara_early_booking.tpl");
		  } elseif($this->oferta_speciala){
		   $oferta->set_oferta_speciala('da');	
		   $oferta->afiseaza_sejur($this->oferta_speciala);	
		  } elseif($this->last_minute || $row_normal['last_minute']=='da') {
			$oferta->setLastMinute('da');
			$oferta->afiseaza_sejur($_SERVER['DOCUMENT_ROOT']."/templates/new_turism_extern_tara.tpl"); 
		  } elseif($this->classpaginare) {
			$oferta->setTplGrup($_SERVER['DOCUMENT_ROOT']."/templates/sejur_tara_tematica_grup.tpl");
		    $oferta->afiseaza_sejur($_SERVER['DOCUMENT_ROOT']."/templates/sejur_tara_tematica.tpl");
		  } else $oferta->afiseaza_sejur($_SERVER['DOCUMENT_ROOT']."/templates/new_turism_extern_tara.tpl");
		  unset($oferta);
	 } @mysql_free_result($que_normal);
   }
 }
 
}

function reverse_calculeaza_pagina($numar_pagina,$nr_oferte_pagina) {   
//returneaza numarul de la care se porneste afisarea in funtie de pagina
  $numar_pagina=$numar_pagina-1;
  $from=$numar_pagina*$nr_oferte_pagina;
  return $from;
}
  
function eroare($template="negasit_filtrare.tpl") {
 $template=$_SERVER['DOCUMENT_ROOT']."/templates/".$template;
 $tpl=new TPL($template);
 $tpl->output_tpl();
}	
} ?>