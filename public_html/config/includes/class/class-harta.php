<?php
class GOOGLE_API_3
{

	var $code='';  // Do not edit this.
	var $zoom=16; // Zoop Level.
	var $center_lat=''; // google map center location
	var $center_lng=''; // google map center location
	var $divID='map'; // The div id where you want to 	place your google map
	var $marker=array(); // Array to store markers information. 
	var $instance=1;
	function __construct()
	{
		echo '<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&hl=en&key=AIzaSyAo4hrP9cMAfhNXt4R1sH5wL3y-lMjuAQ4"></script>';
	}
	// Intialized google map scripts.
	private function start()
	{
		
		$this->code='
		<script type="text/javascript">
      (function() {
        window.onload = function(){
        	// Creating a LatLng object containing the coordinate for the center of the map  
          var latlng = new google.maps.LatLng('.$this->center_lat.', '.$this->center_lng.');  
          // Creating an object literal containing the properties we want to pass to the map  
          var options = {  
          	zoom: '.$this->zoom.',
          	center: latlng,
          	mapTypeId: google.maps.MapTypeId.ROADMAP
          };  
          // Calling the constructor, thereby initializing the map  
          var map = new google.maps.Map(document.getElementById("'.$this->divID.'"), options); ';
		   
          
		  for($i=0;$i<count($this->marker);$i++)
		  {
		  
			 $this->code.=' var marker'.$i.' = new google.maps.Marker({
				position: new google.maps.LatLng('.$this->marker[$i]['lat'].', '.$this->marker[$i]['lng'].'), 
				map: '.$this->marker[$i]['map'].',
				title: "'.$this->marker[$i]['title'].'",
				clickable: '.$this->marker[$i]['click'].',
				icon: "'.$this->marker[$i]['icon'].'"
			  });';
		  
		  // Creating an InfoWindow object
			if($this->marker[$i]['info']!='')
			{
				$this->code.=' var infowindow'.$i.' = new google.maps.InfoWindow({content: "'.$this->marker[$i]['info'].'"}); ';
	   			$this->code.=" google.maps.event.addListener(marker".$i.", 'click', function() { infowindow".$i.".open(map, marker".$i."); });"; 
			}
	}
    
	
	$this->code.='	}
      })();
		</script>';
		
	}

	// Add markers to google map.
	
	public function addMarker($lat='14.0730',$lng='56.0848',$click='false',$title='My WorkPlace',$info='Hello World',$icon='',$map='map')
	{
		$count=count($this->marker);	
		$this->marker[$count]['lat']=$lat;
		$this->marker[$count]['lng']=$lng;
		$this->marker[$count]['map']=$map;
		$this->marker[$count]['title']=$title;
		$this->marker[$count]['click']=$click;
		$this->marker[$count]['icon']=$icon;
		$this->marker[$count]['info']=$info;
	}
	
	// Call this function to create a google map.
	
	public function showmap()
	{
		$this->start();
		$this->instance++;
		return $this->code;
	}
}

?>
