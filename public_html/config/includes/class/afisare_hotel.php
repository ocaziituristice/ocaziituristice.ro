<?php class DETALII_HOTEL {
	function select_camp_hotel($id_hotel) {
		$selT="SELECT
		hoteluri.locatie_id,
		hoteluri.nume as denumire,
		hoteluri.descriere,
		hoteluri.descriere_scurta,
		hoteluri.descriere_seo,
		hoteluri.titlu_seo,
		hoteluri.cuvinte_cheie_seo as cuvinte_seo,
		hoteluri.new_descriere,
		hoteluri.new_camera,
		hoteluri.new_teritoriu,
		hoteluri.new_relaxare,
		hoteluri.new_pentru_copii,
		hoteluri.new_plaja,
		hoteluri.poza1,
		hoteluri.poza2,
		hoteluri.poza3,
		hoteluri.poza4,
		hoteluri.poza5,
		hoteluri.poza6,
		hoteluri.poza7,
		hoteluri.poza8,
		hoteluri.poza9,
		hoteluri.poza10,
		hoteluri.poza11,
		hoteluri.poza12,
		hoteluri.poza13,
		hoteluri.poza14,
		hoteluri.poza15,
		hoteluri.poza16,
		hoteluri.poza17,
		hoteluri.poza18,
		hoteluri.poza19,
		hoteluri.poza20,
		hoteluri.latitudine,
		hoteluri.longitudine,
		hoteluri.adresa,
		hoteluri.stele,
		hoteluri.cod_postal,
		hoteluri.email,
		hoteluri.website,
		hoteluri.tip_unitate,
		hoteluri.concept,
		hoteluri.detalii_concept,
		hoteluri.distanta_fata_de,
		hoteluri.distanta,
		hoteluri.general,
		hoteluri.servicii,
		hoteluri.internet,
		hoteluri.parcare,
		hoteluri.plaja,
		hoteluri.allinclusive,
		hoteluri.check_in,
		hoteluri.check_out,
		hoteluri.anulare_plata,
		hoteluri.info_copii,
		hoteluri.accepta_animale,
		hoteluri.carduri,
		hoteluri.nr,
		hoteluri.observatii,
		hoteluri.garantam,
		hoteluri.apare_grad,
		hoteluri.titlu_seo,
		hoteluri.descriere_seo,
		hoteluri.obiectice_turistice,
		hoteluri.tripadvisor,
		hoteluri.fisier_upload,
		hoteluri.min_price,
		hoteluri.moneda,
		hoteluri.cod_remarketing,
		hoteluri.cautare_live,
		hoteluri.tur_virtual_3d,
		hoteluri.video_youtube,
		hoteluri.data_early_booking,
		hoteluri.comision_reducere,
		localitati.id_localitate AS id_localitate,
		localitati.denumire AS localitate,
		zone.id_zona AS id_zona,
		zone.denumire AS zona,
		tari.id_tara AS id_tara,
		tari.denumire AS tara,
		tari.country_code,
		continente.nume_continent
		FROM hoteluri
		LEFT JOIN localitati on hoteluri.locatie_id = localitati.id_localitate
		LEFT JOIN zone on localitati.id_zona = zone.id_zona
		LEFT JOIN tari on zone.id_tara = tari.id_tara
		LEFT JOIN continente on hoteluri.id_continent = continente.id_continent
		WHERE hoteluri.id_hotel = '".$id_hotel."'
		";
		$queT=mysql_query($selT) or die(mysql_error());
		$param=mysql_fetch_array($queT);
		@mysql_free_result($queT);
		/*if(mysql_num_rows($queT)==0) {
			header("HTTP/1.0 404 Not Found");
			//header("Location: ".$sitepath.'404.php');
			$handle = curl_init($sitepath.'404.php');
			curl_exec($handle);
			exit();
		}*/
		
	
		
		
		return $param;
	}
function camere_hotel($id_hotel)
{ 
$sel_detcam = "SELECT camere_hotel.detalii, tip_camera.denumire FROM camere_hotel INNER JOIN tip_camera ON tip_camera.id_camera = camere_hotel.id_camera WHERE id_hotel='".$id_hotel."' AND (detalii<>NULL OR detalii<>'')";
$que_detcam = mysql_query($sel_detcam) or die(mysql_error());
if(mysql_num_rows($que_detcam)>'0') {
$i=0;
  while($row_detcam = mysql_fetch_array($que_detcam)){
$camere_hotel[$i]['denumire_camera']=$row_detcam['denumire'];
$camere_hotel[$i]['descriere']=$row_detcam['detalii'];
$i++;
	} 
 } 
	return $camere_hotel;
	}
		
function stop_sales($id_hotel,$data_start,$data_end)
	{
	$selE="select * from sold_out where id_hotel = '".$id_hotel."' and data_end>='".$data_start."' and data_start<='".$data_end."' Group by data_start, data_end, ordonare Order by ordonare ";
		$queE=mysql_query($selE) or die(mysql_error());
		while($rowE=mysql_fetch_array($queE)) {
			$stop_s[$rowE['camera']]['data_start_sold_out']=$rowE['data_start'];
			$stop_s[$rowE['camera']]['data_end_sold_out']=$rowE['data_end'];
			//$param['cam_sold_out']=;
		} @mysql_free_result($queE);	
if(count($stop_s)>0) {
foreach($stop_s as $camera=>$datess){
 if($data_start>=$datess['data_start_sold_out'] and $data_start<=$datess['data_end_sold_out'])$stop_s[$camera]['stop_sales']='da'; 

 if($data_start<=$datess['data_start_sold_out'] and $data_end>=$datess['data_start_sold_out'])$stop_s[$camera]['stop_sales']='da'; 

}
	}		
		
return $stop_s;		
		}
	
	function hotel_vizitat($id_hotel)
	{
	
		if(!$GLOBALS['hotel_vazut']) { //hoteluri_vizitate____
			$oferte_vizitate='';
			if($_COOKIE['oferte_vizit']) $oferte_vizitate=$_COOKIE['oferte_vizit'];
			if(strlen($oferte_vizitate)==0) $oferte_vizitate=$id_hotel.'+'.$id_hotel; else {
				$of=explode(',',$oferte_vizitate);
				$g=0;
				foreach($of as $key=>$value) {
					$of1=explode('+',$value);
					if(trim($of1[1])==$id_hotel)
					$g=1;
				}
				if($g==0) {
					$of[]=$id_hotel.'+'.$id_hotel;
				}
				$oferte_vizitate='';
				$fr=sizeof($of)-1;
				if($fr>3) $st=$fr-3; else $st=0;
				$j=0;
				
				for($i=$st; $i<=$fr; $i++) {
					$j++;
					$oferte_vizitate=$oferte_vizitate.$of[$i].',';
				}
				
				$oferte_vizitate=substr($oferte_vizitate,0,-1); if($j==3) $i=++$fr;
			}
			$timeout = time() + 60 * 60 * 24 * 5;
			setcookie("oferte_vizit", $oferte_vizitate, $timeout, "/");
		}
	
	
	
	
	/*
	//if(!$GLOBALS['hotel_vazut']) { //hoteluri_vizitate____
			$hoteluri_vizitate='';
			if($_COOKIE['hoteluri_vizit']) $hoteluri_vizitate=$_COOKIE['hoteluri_vizit'];
			if(strlen($hoteluri_vizitate)==0) $hoteluri_vizitate=$id_hotel; else {
				$hot=explode(',',$hoteluri_vizitate);
				
				
				//$g=0;
				//foreach($of as $key=>$value) {
				
					//$of1=explode('+',$value);
					//if(trim($of1[1])==$this->id_hotel)
					//$g=1;
				//}
				//if($g==0) {
					//$of[]=$id_oferta.'+'.$this->id_hotel;
				//}
				$hoteluri_vizitate='';
			//if(in_array($id_hotel,$hot)
			$fr=sizeof($hot)-1;
				if($fr>3) $st=$fr-3; else $st=0;
				$j=0;
				
				for($i=$st; $i<=$fr; $i++) {
					$j++;
					$hoteluri_vizitate.=$hot[$i].',';
				}
				
				$hoteluri_vizitate=substr($hoteluri_vizitate,0,-1); //if($j==3) $i=++$fr;
			}
			$hoteluri_vizitate=$hoteluri_vizitate.",".$id_hotel;
			$timeout = time() + 60 * 60 * 24 * 5;
			//$hoteluri_vizitate=$id_hotel;
			setcookie("hoteluri_vizit", $hoteluri_vizitate, $timeout, "/");
		//}
	
	*/
	
		
	}
	
	
	
	
	
	function get_oferte($id_hotel, $exceptie=NULL) {
		$selOf="SELECT
		oferte.id_oferta,
		oferte.denumire,
		oferte.denumire_scurta,
		transport.denumire AS denumire_transport,
		oferte.masa,
		oferte.pret_minim,
		oferte.moneda,
		oferte.exprimare_pret,
		oferte.taxa_aeroport,
		oferte.nr_zile,
		oferte.nr_nopti,
		oferte.oferta_speciala,
		oferte.tip_preturi,
		oferte.last_minute,
		tip_oferta.denumire_tip_oferta,
		GROUP_CONCAT(DISTINCT tip_oferta.id_tip_oferta,'+', tip_oferta.culoare SEPARATOR ', ') AS tip_denumire,
		GROUP_CONCAT(tip_oferta.tip SEPARATOR ', ') AS tip_denumire_tip,
		oferte_speciale.denumire_tip_oferta AS ofsp_denumire,
		oferte_speciale.descriere_scurta AS ofsp_descriere,
		oferte_speciale.reducere AS ofsp_reducere,
		oferte_speciale.data_inceput_eveniment AS ofsp_start,
		oferte_speciale.data_sfarsit_eveniment AS ofsp_end
		FROM oferte
		LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		LEFT JOIN tip_oferta ON (oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta AND tip_oferta.activ = 'da' AND tip_oferta.apare_site = 'da' AND tip_oferta.tip IS NOT NULL)
		LEFT JOIN transport ON oferte.id_transport = transport.id_trans
		LEFT JOIN tip_oferta AS oferte_speciale ON (oferta_sejur_tip.id_tip_oferta = oferte_speciale.id_tip_oferta AND oferte_speciale.tip = 'oferte_speciale')
		WHERE oferte.valabila = 'da'
		AND oferte.id_hotel = '".$id_hotel."'
		";
		if($exceptie) $selOf=$selOf." AND oferte.id_oferta <> '".$exceptie."' ";
		$selOf=$selOf." GROUP BY oferte.id_oferta
		ORDER BY oferte.pret_minim_lei ";
		//echo $selOf;
		$queOf=mysql_query($selOf) or die(mysql_error());
		$t=array();
		$t2=array();
		while($rowOf=mysql_fetch_assoc($queOf)) {
			$t=explode(', ',$rowOf['tip_denumire']);
			$t2=explode(', ',$rowOf['tip_denumire_tip']);
			$z=array_combine($t2, $t);
			if($z['programe']) $poza=$z['programe'];
				elseif($z['tematici']) $poza=$z['tematici'];
				elseif($z['evenimente']) $poza=$z['evenimente'];
			$rowOf['poza_tip']=$poza;
			$Of[]=$rowOf;
		}
		@mysql_free_result($queOf);
		return $Of;
	}
	
	function oferte_online($id_hotel, $plecdata=NULL, $id_transport=NULL, $id_oras_plec=NULL) {
		$details['tip_transport'] = array();
		$details['tip_masa'] = array();
		$details['oras_plecare'] = array();
		$details['oras_destinatie'] = array();
		$details['data_start'] = array();
		$details['data_end'] = array();
		$details['id_oferta'] = array();
		$details['tip_preturi'] = array();
		$details['cazare'] = array();
		$details['masa'] = array();
		$details['transport'] = array();
		$details['id_oferta_pivot'] = array();
		$details['nr_formula_pivot'] = array();
		$details['nr_nopti'] = array();
		
		$sel = "SELECT DISTINCT(oferte.id_transport), transport.denumire
		FROM oferte
		INNER JOIN transport ON oferte.id_transport = transport.id_trans
		WHERE oferte.valabila = 'da'
		AND oferte.id_hotel = '".$id_hotel."'
		ORDER BY transport.ordine ASC ";
		$que = mysql_query($sel) or die(mysql_error());
		if(mysql_num_rows($que)>0) {
			while($row = mysql_fetch_assoc($que)) {
				$id_trans = $row['id_transport'];			
				$details['tip_transport'][$id_trans] = $row['denumire'];
				
				if($id_trans==2) {
					$sel_autocar = "SELECT
					oferte_transport_autocar.*,
					localitati.denumire AS denumire_localitate
					FROM oferte_transport_autocar
					INNER JOIN localitati ON oferte_transport_autocar.id_localitate = localitati.id_localitate
					INNER JOIN oferte ON oferte_transport_autocar.id_oferta = oferte.id_oferta
					WHERE oferte.id_hotel = '".$id_hotel."'
					AND oferte.valabila = 'da'
					GROUP BY oferte_transport_autocar.ordonare, oferte_transport_autocar.id_localitate
					ORDER BY localitati.denumire ASC ";
					$que_autocar = mysql_query($sel_autocar) or die(mysql_error());
					while($row_autocar = mysql_fetch_assoc($que_autocar)) {
						$details[$row['denumire']][$row_autocar['id_localitate']] = $row_autocar['denumire_localitate'];
					} @mysql_free_result($que_autocar);
				}
				
				if($id_trans==4 or $id_trans==5) {
					$sel_avion = "SELECT
					oferte_transport_avion.*,
					localitate_plecare.id_localitate,
					localitate_plecare.denumire AS denumire_localitate
					FROM oferte_transport_avion
					INNER JOIN aeroport AS aeroport_plecare ON oferte_transport_avion.aeroport_plecare = aeroport_plecare.id_aeroport
					INNER JOIN localitati AS localitate_plecare ON aeroport_plecare.id_localitate = localitate_plecare.id_localitate
					INNER JOIN zone ON localitate_plecare.id_zona = zone.id_zona
					INNER JOIN oferte ON oferte_transport_avion.id_oferta = oferte.id_oferta
					WHERE oferte.id_hotel = '".$id_hotel."'
					AND oferte.valabila = 'da'
					AND zone.id_tara = '1'
					AND oferte_transport_avion.tip = 'dus'
					ORDER BY oferte_transport_avion.ordonare ASC, localitate_plecare.denumire ASC ";
					$que_avion = mysql_query($sel_avion) or die(mysql_error());
					while($row_avion = mysql_fetch_assoc($que_avion)) {
						$details[$row['denumire']][$row_avion['id_localitate']] = $row_avion['denumire_localitate'];
					} @mysql_free_result($que_avion);
				}
				
				if($id_trans==1) {
					$details[$row['denumire']][51] = get_den_localitate(51);
				}
			}
		}
		@mysql_free_result($que);

		if(count($details['tip_transport'])==1 and key($details['tip_transport'])!=$id_transport) $id_transport = key($details['tip_transport']);
		
		$sel = "SELECT DISTINCT(masa), id_masa FROM hotel_meal WHERE id_hotel = '".$id_hotel."' ";
		$que = mysql_query($sel) or die(mysql_error());
		while($row = mysql_fetch_assoc($que)) {
			$details['tip_masa'][$row['id_masa']] = $row['masa'];
		}
		@mysql_free_result($que);

		$sel_min_nopti = "SELECT MAX(nr_min) AS min_nopti
		FROM nr_nopti_minim
		INNER JOIN oferte ON nr_nopti_minim.id_oferta = oferte.id_oferta
		WHERE oferte.valabila = 'da'
		AND oferte.id_hotel = '".$id_hotel."' ";
		if(isset($id_transport)) $sel_min_nopti = $sel_min_nopti . " AND oferte.id_transport = '".$id_transport."' ";
		$sel_min_nopti = $sel_min_nopti . " GROUP BY oferte.id_hotel ";
		$que_min_nopti = mysql_query($sel_min_nopti) or die(mysql_error());
		$row_min_nopti = mysql_fetch_array($que_min_nopti);
		
		if(mysql_num_rows($que_min_nopti)>0) $details['min_nopti'] = $row_min_nopti['min_nopti'];
		
		$nr_nights = array();
		$sel_nights = "SELECT nr_nopti FROM oferte WHERE oferte.valabila = 'da' AND id_hotel = '".$id_hotel."' ";
		$que_nights = mysql_query($sel_nights) or die(mysql_error());
		while($row_nights = mysql_fetch_assoc($que_nights)) $nr_nights[] = $row_nights['nr_nopti'];
		
		if(in_array('1', $nr_nights)) {
			if($details['min_nopti']>1) $min_nopti = $details['min_nopti']; else $min_nopti = $GLOBALS['$nr_nopti_minim'];
			for($i=$min_nopti; $i<=30; $i++) $details['no_nights'][] = $i;
		} else {
			$details['no_nights'] = array();
			foreach($nr_nights as $value_nights) $details['no_nights'][] = $value_nights;
			$details['no_nights'] = array_unique($details['no_nights']);
			asort($details['no_nights']);
		}
		
		$sel = "SELECT DISTINCT(oferte.furnizor)
		FROM oferte ";
		//if($plecdata!='') $sel = $sel . " LEFT JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel = $sel . " WHERE oferte.valabila = 'da'
		AND oferte.id_hotel = '".$id_hotel."' ";
		if(isset($id_transport)) $sel = $sel . " AND oferte.id_transport = '".$id_transport."' ";
		//if($plecdata!='') $sel = $sel . " AND data_pret_oferta.data_start = '".$plecdata."' ";
		$que = mysql_query($sel) or die(mysql_error());
		while($row = mysql_fetch_assoc($que)) {
			$details['furnizor'][] = $row['furnizor'];
			
			$sel_off = "SELECT DISTINCT(oferte.id_oferta),
			oferte.tip_preturi,
			oferte.cazare,
			oferte.masa,
			oferte.denumire,
			oferte.denumire_scurta,
			oferte.nota,
			oferte.id_transport,
			oferte.id_oferta_pivot,
			oferte.nr_formula_pivot,
			oferte.taxa_aeroport,
			oferte.nr_nopti,
			oferte.last_minute,
			furnizori.comision_procent as furnizor_comision_procent,
			oferte.discount_tarif
			FROM oferte
			Inner join furnizori on oferte.furnizor=furnizori.id_furnizor
			WHERE oferte.valabila = 'da'
			AND oferte.id_hotel = '".$id_hotel."'
			AND oferte.furnizor = '".$row['furnizor']."' ";
			if(isset($id_transport)) $sel_off = $sel_off . " AND oferte.id_transport = '".$id_transport."' ";
			$sel_off;
			$que_off = mysql_query($sel_off) or die(mysql_error());
			if(mysql_num_rows($que_off)>0) {
				while($row_off = mysql_fetch_assoc($que_off)) {
					$sel_oras_plecare = "SELECT
					localitati.id_localitate,
					localitati.denumire AS loc_plecare,
					aeroport.tez_id_aeroport,
					oferte_transport_avion.tip
					FROM oferte_transport_avion
					LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
					LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
					LEFT JOIN zone ON localitati.id_zona = zone.id_zona
					WHERE oferte_transport_avion.id_oferta = '".$row_off['id_oferta']."'
					AND oferte_transport_avion.tip = 'dus'
					ORDER BY oferte_transport_avion.ordonare ASC ";
					$que_oras_plecare = mysql_query($sel_oras_plecare) or die(mysql_error());
					$row_oras_plecare = mysql_fetch_array($que_oras_plecare);
					if(mysql_num_rows($que_oras_plecare)>0) {
						$details['oras_plecare'][$row_off['id_oferta']][0] = $row_oras_plecare['id_localitate'];
						$details['oras_plecare'][$row_off['id_oferta']][1] = $row_oras_plecare['loc_plecare'];
						$details['oras_plecare'][$row_off['id_oferta']][2] = $row_oras_plecare['tez_id_aeroport'];
						if($id_oras_plec and $row_oras_plecare['id_localitate']==$id_oras_plec) $details['oferte_filtrate'][] = $row_off['id_oferta'];

						$sel_oras_intoarcere = "SELECT
						localitati.id_localitate,
						localitati.denumire AS loc_plecare,
						aeroport.tez_id_aeroport,
						oferte_transport_avion.tip
						FROM oferte_transport_avion
						LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
						LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
						LEFT JOIN zone ON localitati.id_zona = zone.id_zona
						WHERE oferte_transport_avion.id_oferta = '".$row_off['id_oferta']."'
						AND oferte_transport_avion.tip = 'intors'
						ORDER BY oferte_transport_avion.ordonare DESC ";
						$que_oras_intoarcere = mysql_query($sel_oras_intoarcere) or die(mysql_error());
						$row_oras_intoarcere = mysql_fetch_array($que_oras_intoarcere);
							$details['oras_destinatie'][$row_off['id_oferta']][0] = $row_oras_intoarcere['id_localitate'];
							$details['oras_destinatie'][$row_off['id_oferta']][1] = $row_oras_intoarcere['loc_plecare'];
							$details['oras_destinatie'][$row_off['id_oferta']][2] = $row_oras_intoarcere['tez_id_aeroport'];
					} else {
						$details['oras_plecare'][$row_off['id_oferta']][0] = 51;
						$details['oras_plecare'][$row_off['id_oferta']][1] = get_den_localitate(51);
						$details['oras_destinatie'][$row_off['id_oferta']][0] = get_id_localitate_by_id_hotel($id_hotel);
						$details['oras_destinatie'][$row_off['id_oferta']][1] = get_den_localitate($details['oras_destinatie'][$row_off['id_oferta']][0]);
					}
$selE="select date_format(end_date , '%d.%m.%Y') as end_early, discount, early_inclus, text_early from early_booking where id_oferta = '".$row_off['id_oferta']."' and tip = 'sejur' and end_date >= now() Group by end_date Order by end_date ";
		$queE=mysql_query($selE) or die(mysql_error());
		while($rowE=mysql_fetch_array($queE)) {
			$details['early_time'][$row_off['id_oferta']]=$rowE['end_early'];
			$details['early_disc'][$row_off['id_oferta']]=$rowE['discount'];
			$details['early_inclus'][$row_off['id_oferta']]=$rowE['early_inclus'];
			$details['text_early'][$row_off['id_oferta']]=$rowE['text_early'];
		} @mysql_free_result($queE);
							
					$details['id_oferta'][$row['furnizor']][] = $row_off['id_oferta'];
					$details['tip_preturi'][$row_off['id_oferta']] = $row_off['tip_preturi'];
					$details['cazare'][$row_off['id_oferta']] = $row_off['cazare'];
					$details['nr_nopti'][$row_off['id_oferta']] = $row_off['nr_nopti'];
					$details['masa'][$row_off['id_oferta']] = $row_off['masa'];
					$details['denumire'][$row_off['id_oferta']] = $row_off['denumire'];
					$details['denumire_scurta'][$row_off['id_oferta']] = $row_off['denumire_scurta'];
					$details['nota'][$row_off['id_oferta']] = $row_off['nota'];
					$details['descriere_oferta'][$row_off['id_oferta']] = $row_off['descriere_oferta'];
					$details['transport'][$row_off['id_oferta']] = $details['tip_transport'][$row_off['id_transport']];
					$details['id_oferta_pivot'][$row_off['id_oferta']] = $row_off['id_oferta_pivot'];
					$details['nr_formula_pivot'][$row_off['id_oferta']] = $row_off['nr_formula_pivot'];
					$details['taxa_aeroport'][$row_off['id_oferta']] = $row_off['taxa_aeroport'];
					$details['discount_tarif'][$row_off['id_oferta']] = $row_off['discount_tarif'];
					$details['furnizor_comision_procent'][$row_off['id_oferta']] = $row_off['furnizor_comision_procent'];
					if($row_off['last_minute']=='da') {$details['last_minute']=$details['last_minute']+1;} else {$details['last_minute'] =$details['last_minute']+0;}
				}
			}
			@mysql_free_result($que_off);
		}
		@mysql_free_result($que);
		
		if(count($details['oferte_filtrate'])>0) {
			$idoffs = '';
			foreach($details['oferte_filtrate'] as $key_off) {
				$idoffs .= $key_off.",";
			}
			$idoffs = substr($idoffs, 0, -1);
		}
		
		$sel_dates = "SELECT data_pret_oferta.data_start, data_pret_oferta.data_end, data_pret_oferta.id_oferta as id_oferta
		FROM data_pret_oferta
		INNER JOIN oferte ON data_pret_oferta.id_oferta = oferte.id_oferta
		WHERE oferte.valabila = 'da' ";
		if(count($details['oferte_filtrate'])>0) $sel_dates = $sel_dates . " AND oferte.id_oferta IN (".$idoffs.") ";
		$sel_dates = $sel_dates . " AND oferte.id_hotel = '".$id_hotel."' ";
		//if(isset($id_transport)) $sel_dates = $sel_dates . " AND oferte.id_transport = '".$id_transport."' ";
		$sel_dates = $sel_dates . " ORDER BY data_pret_oferta.data_start ASC ";
		$que_dates = mysql_query($sel_dates) or die(mysql_error());
		if(mysql_num_rows($que_dates)>0) {
			while($row_dates = mysql_fetch_array($que_dates)) {
				if($row_dates['data_end']=='0000-00-00' and $row_dates['data_start'] < $GLOBALS['date_now']) {} else {
					$details['data_start'][] = $row_dates['data_start'];
					$details['data_end'][] = $row_dates['data_end'];
					$details['data_start_oferta'][$row_dates['id_oferta']][] = $row_dates['data_start'];
					$details['data_end_oferta'][$row_dates['id_oferta']][] = $row_dates['data_end'];
				}
			} @mysql_free_result($que_dates);
			$details['data_start'] = array_unique($details['data_start']);
			//$details['data_end'] = array_unique($details['data_end']);
		}
		
		$sel_mins = "SELECT
		'".$details['data_start'][key($details['data_start'])]."' AS min_data,
		IF(MAX(data_pret_oferta.data_end)='0000-00-00', MAX(data_pret_oferta.data_start), MAX(data_pret_oferta.data_end)) AS max_data,
		MIN(oferte.nr_nopti) AS min_nopti
		FROM data_pret_oferta
		INNER JOIN oferte ON data_pret_oferta.id_oferta = oferte.id_oferta
		WHERE oferte.valabila = 'da'
		AND oferte.id_hotel = '".$id_hotel."' ";
		if(isset($id_transport)) $sel_mins = $sel_mins . " AND oferte.id_transport = '".$id_transport."' ";
		$sel_mins = $sel_mins . " GROUP BY oferte.id_hotel ";
		$que_mins = mysql_query($sel_mins) or die(mysql_error());
		$row_mins = mysql_fetch_assoc($que_mins);
		@mysql_free_result($que_mins);
		
		$details['min_data'] = $row_mins['min_data'];
		$details['max_data'] = $row_mins['max_data'];
		$details['min_nopti'] = $row_mins['min_nopti'];
		
		
		
		
		
		return $details;
	}
	
	function servicii_suplimentare ($oferte)
	{ 
		
	}
		
		
	function offer_services($id_hotel, $id_transport, $nr_nopti=NULL, $oras_plecare=NULL, $oras_destinatie=NULL, $id_oferta=NULL,$e_circuit=false,$servicii_manual='nu') {
		
		$is=0;
		$services = array();
		$id_transport;
		if($id_transport==1) {
			/*$services['servicii_incluse'][] = 'Cazare cu masă conform ofertei';
			if(get_id_tara_by_id_hotel($id_hotel)!=1) {
				$services['servicii_neincluse'][] = 'Asigurare de sănătate';
				$services['servicii_neincluse'][] = 'Asigurare de stornare';
			}*/
		} else if($id_transport==4 and !$e_circuit) {
			$services['servicii_incluse'][] = ''.$nr_nopti.' nopți cazare cu masă conform ofertei';
			$services['servicii_incluse'][] = 'Bilet avion '.$oras_plecare.' - '.$oras_destinatie.' și retur';
			$services['servicii_incluse'][] = 'Taxele de aeroport';
			$services['servicii_incluse'][] = 'Bagaj de mână și bagaj de cală';
			$services['servicii_incluse'][] = 'Transfer aeroport - hotel - aeroport';
			//$services['servicii_incluse'][] = 'Asigurare de sănătate și stornare - oferită bonus';
			$services['servicii_incluse'][] = 'Asistență turistică în limba română';
		} else if($id_transport==2 and !$e_circuit) {
			$services['servicii_incluse'][] = ''.$nr_nopti.' nopți cazare cu masă conform ofertei';
			$services['servicii_incluse'][] = 'Transport cu autocarul';
			$services['servicii_incluse'][] = 'Asistență turistică în limba română';
		}
		
		//$services['conditii_plata'][] = 'Avans 30% la înscriere pentru rezervarea fermă a locurilor';
		//$services['conditii_plata'][] = 'Restul de plată cu minim 20 de zile înainte de începerea sejurului, cu excepție pentru ofertele Early Booking, care trebuie achitate până la data expirării perioadei de Early Booking';
		
		if($id_oferta!='') {
			 $selS = "SELECT oferte_servicii.*, oferte.servicii_manual
			FROM oferte_servicii, oferte
			WHERE oferte_servicii.id_oferta = '".$id_oferta."' and oferte.id_oferta='".$id_oferta."' 
			AND ((tip_serv = 'Suplimente' AND tip_supliment <> '33') OR tip_serv <> 'Suplimente')
			GROUP BY denumire, value, moneda, tip_serv, obligatoriu, ordonare
			ORDER BY ordonare ";
			$queS = mysql_query($selS) or die(mysql_error());
			$rowS = mysql_fetch_array($queS);
			
			if(count($services['servicii_incluse']) > count($services['servicii_neincluse'])) $is = count($services['servicii_incluse']);
				else if(count($services['servicii_incluse']) < count($services['servicii_neincluse'])) $is = count($services['servicii_neincluse']);
			//$rowS = mysql_fetch_row($queS);
			
			if($rowS['servicii_manual']=='da'){unset($services['servicii_incluse']);}
			//mysql_close($queS); 

			$queS = mysql_query($selS) or die(mysql_error());
			while($rowS = mysql_fetch_array($queS)) {
			//foreach($rowSS as $rowS){
			$is++;
			//echo	$rowS['tip_serv'];
				if($rowS['tip_serv']=='Suplimente') {$services['suplimente'][$is]=schimba_caractere(ucfirst($rowS['denumire']));}
				if($rowS['tip_serv']=='Servicii incluse') {
					//if($id_transport!=4 and $id_transport!=2)
					 $services['servicii_incluse'][$is] = schimba_caractere(ucfirst($rowS['denumire']));
				} elseif($rowS['tip_serv']=='Servicii neincluse' or $rowS['tip_serv']=='Suplimente') {
					$services['servicii_neincluse'][$is] = schimba_caractere(ucfirst($rowS['denumire']));
					if($rowS['value']) {
						$services['servicii_neincluse'][$is] .= ' - <strong>'.$rowS['value'].' '.moneda($rowS['moneda']).' '.$rowS['exprimare'].' '.$rowS['pasager'].'</strong>';
					}
					if($rowS['obligatoriu']=='da') $services['servicii_neincluse'][$is] .= ' - <em class="red bold">Obligatoriu</em>';
						else $services['servicii_neincluse'][$is] .= ' - <em class="blue">Opțional</em>';
				}
			}
		}
		return $services;
	}
}