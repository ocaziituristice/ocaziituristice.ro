<?php

class DETALII_SEJUR extends DETALII_HOTEL
{
    function getTara($tara)
    {
        $this->tara = $tara;
    }

    function select_det_sejur($id_oferta)
    {
        $sel = "SELECT
		oferte.*,
		transport.denumire AS transport,
		transport.id_trans AS id_transport
		FROM oferte
		LEFT JOIN transport ON oferte.id_transport = transport.id_trans
		WHERE oferte.id_oferta = '" . $id_oferta . "' ";
        $que = mysql_query($sel) or die(mysql_error());
        $param = mysql_fetch_array($que);
        if (mysql_num_rows($que) == 0 && $GLOBALS['tip_unitate'] == 'Circuit') {
            header("HTTP/1.0 301 Moved Permanentlyd");
            header("Location: " . $sitepath . "/circuite/");
            exit();
        }
        /*if(mysql_num_rows($que)==0 && !$this->tara) {
            header("HTTP/1.0 404 Not Found");
            //header("Location: ".$sitepath.'404.php');
            $handle = curl_init($sitepath.'404.php');
            curl_exec($handle);
            exit();
        } elseif(mysql_num_rows($que)==0) {
            header("HTTP/1.0 301 Moved Permanentlyd");
            header("Location: ".$GLOBALS['sitepath_class'].'sejur-'.$this->tara.'/');
            exit();
        }*/
        @mysql_free_result($que);

        $this->tip_preturi = $param['tip_preturi'];
        $this->nr_nopti = $param['nr_nopti'];
        $this->id_hotel = $param['id_hotel'];
        $this->avans_plata = $param['avans_plata'];
        $this->nr_zile_plata = $param['nr_zile_plata'];
        $this->discount_tarif = $param['discount_tarif'];
        $this->update_date = $param['update_date'];


        if (!$GLOBALS['make_vizualizata']) { //hoteluri_vizitate____
            $oferte_vizitate = '';
            if ($_COOKIE['oferte_vizit']) {
                $oferte_vizitate = $_COOKIE['oferte_vizit'];
            }
            if (strlen($oferte_vizitate) == 0) {
                $oferte_vizitate = $id_oferta . '+' . $this->id_hotel;
            } else {
                $of = explode(',', $oferte_vizitate);
                $g = 0;
                foreach ($of as $key => $value) {
                    $of1 = explode('+', $value);
                    if (trim($of1[1]) == $this->id_hotel) {
                        $g = 1;
                    }
                }
                if ($g == 0) {
                    $of[] = $id_oferta . '+' . $this->id_hotel;
                }
                $oferte_vizitate = '';
                $fr = sizeof($of) - 1;
                if ($fr > 3) {
                    $st = $fr - 3;
                } else {
                    $st = 0;
                }
                $j = 0;

                for ($i = $st; $i <= $fr; $i++) {
                    $j++;
                    $oferte_vizitate = $oferte_vizitate . $of[$i] . ',';
                }

                $oferte_vizitate = substr($oferte_vizitate, 0, -1);
                if ($j == 3) {
                    $i = ++$fr;
                }
            }
            $timeout = time() + 60 * 60 * 24 * 5;
            setcookie("oferte_vizit", $oferte_vizitate, $timeout, "/");
        }
        $click = $param['click'] + 1;

        $upd = "UPDATE oferte SET click = '" . $click . "' WHERE oferte.id_oferta = '" . $id_oferta . "' ";
        $queU = mysql_query($upd) or die(mysql_error());
        @mysql_free_result($queU);

        $selE = "SELECT MIN(data_start) AS period_data_start, ";
        if ($param['tip_preturi'] == 'plecari') {
            $selE = $selE . " MAX(data_start) AS period_data_end ";
        } else {
            $selE = $selE . " MAX(data_end) AS period_data_end ";
        }
        $selE = $selE . " FROM data_pret_oferta WHERE id_oferta = '" . $id_oferta . "' ";
        $queE = mysql_query($selE) or die(mysql_error());
        $rowE = mysql_fetch_array($queE);
        $param['period_data_start'] = $rowE['period_data_start'];
        $param['period_data_end'] = $rowE['period_data_end'];
        @mysql_free_result($queE);

        $selE = "SELECT data_start, data_end, nr_min FROM nr_nopti_minim WHERE id_oferta = '" . $id_oferta . "' ";
        $queE = mysql_query($selE) or die(mysql_error());
        while ($rowE = mysql_fetch_array($queE)) {
            $param['data_start_nr_min'][] = $rowE['data_start'];
            $param['data_end_nr_min'][] = $rowE['data_end'];
            $param['nr_min'][] = $rowE['nr_min'];
        }
        @mysql_free_result($queE);

        $selE = "select date_format(end_date , '%d.%m.%Y') as end_early, discount, early_inclus, text_early from early_booking where id_oferta = '" . $id_oferta . "' and tip = 'sejur' and end_date >= now() Group by end_date Order by end_date ";
        $queE = mysql_query($selE) or die(mysql_error());
        while ($rowE = mysql_fetch_array($queE)) {
            $param['early_time'][] = $rowE['end_early'];
            $param['early_disc'][] = $rowE['discount'];
            $param['early_inclus'][] = $rowE['early_inclus'];
            $param['text_early'][] = $rowE['text_early'];
        }
        @mysql_free_result($queE);

        $selE = "select * from sold_out where id_oferta = '" . $id_oferta . "' Group by data_start, data_end, ordonare Order by ordonare ";
        $queE = mysql_query($selE) or die(mysql_error());
        while ($rowE = mysql_fetch_array($queE)) {
            $param['data_start_sold_out'][] = $rowE['data_start'];
            $param['data_end_sold_out'][] = $rowE['data_end'];
            $param['cam_sold_out'][] = $rowE['camera'];
        }
        @mysql_free_result($queE);

        $selE = "select date_format(data_start, '%d.%m.%Y') as start, date_format(data_end, '%d.%m.%Y') as end, zile_deaplicare,  zile_aplicare from reduceri_speciale where id_oferta = '" . $id_oferta . "' Group by data_start, data_end, zile_deaplicare, zile_aplicare, ordonare Order by data_start, data_end ";
        $queE = mysql_query($selE) or die(mysql_error());
        while ($rowE = mysql_fetch_array($queE)) {
            $param['data_start_reduceri_speciale'][] = $rowE['start'];
            $param['data_end_reduceri_speciale'][] = $rowE['end'];
            $param['zile_deaplicare'][] = $rowE['zile_deaplicare'];
            $param['zile_aplicare'][] = $rowE['zile_aplicare'];
        }
        @mysql_free_result($queE);

        $selS = "select * from oferte_servicii where id_oferta = '" . $id_oferta . "' ";
        $selS = $selS . " and ((tip_serv = 'Suplimente' and tip_supliment <> '33') or tip_serv <> 'Suplimente') ";
        $selS = $selS . " Group by denumire, value, moneda, tip_serv, obligatoriu, ordonare Order by ordonare ";
        $queS = mysql_query($selS) or die(mysql_error());
        $v1 = 0;
        $v2 = 0;
        $v3 = 0;
        $v4 = 0;
        while ($rowS = mysql_fetch_array($queS)) {
            if ($rowS['tip_serv'] == 'Servicii incluse') {
                $v1++;
                $param['denumire_v1'][$v1] = $rowS['denumire'];
                $param['value_v1'][$v1] = $rowS['value'];
                $param['moneda_v1'][$v1] = $rowS['moneda'];
                $param['obligatoriu_v1'][$v1] = $rowS['obligatoriu'];
                if ($rowS['tip_supliment'] == 1) {
                    $param['taxa_avion'] = ['inclus' => 'da'];
                }
            } elseif ($rowS['tip_serv'] == 'Servicii neincluse') {
                $v2++;
                $param['denumire_v2'][$v2] = $rowS['denumire'];
                $param['value_v2'][$v2] = $rowS['value'];
                $param['moneda_v2'][$v2] = $rowS['moneda'];
                $param['exprimare_v2'][$v2] = $rowS['exprimare'];
                $param['pasager_v2'][$v2] = $rowS['pasager'];
                $param['obligatoriu_v2'][$v2] = $rowS['obligatoriu'];
                if ($rowS['tip_supliment'] == 1) {
                    $param['taxa_avion'] = ['inclus' => 'nu', 'pret' => $rowS['value'], 'moneda' => $rowS['moneda']];
                }
            } elseif ($rowS['tip_serv'] == 'Suplimente') {
                $v3++;
                $param['denumire_v3'][$v3] = $rowS['denumire'];
                $param['value_v3'][$v3] = $rowS['value'];
                $param['moneda_v3'][$v3] = $rowS['moneda'];
                $param['exprimare_v3'][$v3] = $rowS['exprimare'];
                $param['pasager_v3'][$v3] = $rowS['pasager'];
                $param['obligatoriu_v3'][$v3] = $rowS['obligatoriu'];
                if (isset($rowS['data_start']) and $rowS['data_start'] != '0000-00-00') {
                    $param['data_start_v3'][$v3] = $rowS['data_start'];
                    $param['data_end_v3'][$v3] = $rowS['data_end'];
                }
            } elseif ($rowS['tip_serv'] == 'Reduceri') {
                $v4++;
                $param['denumire_v4'][$v4] = $rowS['denumire'];
                $param['value_v4'][$v4] = $rowS['value'];
                $param['moneda_v4'][$v4] = $rowS['moneda'];
            }
        }

        $selTip = "SELECT
			tip_oferta.id_tip_oferta,
			tip_oferta.denumire_tip_oferta
			FROM oferte
			INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
			WHERE
			oferte.valabila = 'da'
			AND oferte.id_oferta = '" . $id_oferta . "'
			AND tip_oferta.activ = 'da'
			AND tip_oferta.apare_site = 'da'
			GROUP BY tip_oferta.id_tip_oferta
		";
        $queTip = mysql_query($selTip) or die(mysql_error());
        while ($rotTip = mysql_fetch_assoc($queTip)) {
            $param['tip_oferta'][] = $rotTip['id_tip_oferta'];
            $param['tip_oferta_denumire'][] = $rotTip['denumire_tip_oferta'];
        }
        @mysql_free_result($queTip);

        $selSPO = "SELECT tip_oferta.* FROM oferte
			INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
			WHERE oferte.valabila = 'da'
			AND oferte.id_oferta = '" . $id_oferta . "'
			AND tip_oferta.activ = 'da'
			AND tip_oferta.tip = 'oferte_speciale'
			AND CURDATE() BETWEEN tip_oferta.data_inceput_eveniment AND tip_oferta.data_sfarsit_eveniment
			GROUP BY tip_oferta.id_tip_oferta
		";
        $queSPO = mysql_query($selSPO) or die(mysql_error());
        $rowSPO = mysql_fetch_assoc($queSPO);
        if (mysql_num_rows($queSPO) > 0) {
            $param['spo_titlu'] = 'Oferta Speciala';
            $param['spo_descriere'] = $rowSPO['descriere_scurta'];
            $param['spo_reducere'] = $rowSPO['reducere'];
            $param['spo_start'] = $rowSPO['data_inceput_eveniment'];
            $param['spo_end'] = $rowSPO['data_sfarsit_eveniment'];
        }

        return $param;
    }

    function select_preturi_sejur($id_oferta, $data_start, $data_end)
    {
        setlocale(LC_TIME, ['ro.utf-8', 'ro_RO.UTF-8', 'ro_RO.utf-8', 'ro', 'ro_RO', 'ro_RO.ISO8859-2']);
        $tipuri = [];

        $selTip = "select
		oferte.id_oferta,
		oferte.denumire,
		tip_oferta.denumire_tip_oferta,
		tip_oferta.id_tip_oferta,
		tip_oferta.data_inceput_eveniment as start,
		tip_oferta.data_sfarsit_eveniment as end
		from
		oferte
		inner join oferta_sejur_tip on oferte.id_oferta = oferta_sejur_tip.id_oferta
		inner join tip_oferta on oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
		where
		oferte.valabila = 'da'
		and oferte.id_hotel = '" . $this->id_hotel . "'
		and oferte.id_oferta <> '" . $id_oferta . "'
		and tip_oferta.activ = 'da'
		and tip_oferta.apare_site = 'da'
		and tip_oferta.data_inceput_eveniment is not null
		and tip_oferta.data_sfarsit_eveniment is not null
		Group by tip_oferta.id_tip_oferta
		Order by tip_oferta.data_inceput_eveniment, tip_oferta.data_sfarsit_eveniment ";
        $queTip = mysql_query($selTip) or die(mysql_error());
        while ($rotTip = mysql_fetch_assoc($queTip)) {
            $tipuri[] = $rotTip;
        }
        @mysql_free_result($queTip);

        $selS = "select * from oferte_servicii where id_oferta = '" . $id_oferta . "' and tip_serv = 'Suplimente' and tip_supliment = '3' Group by denumire, value, moneda, tip_serv, obligatoriu, ordonare Order by ordonare ";
        $queS = mysql_query($selS) or die(mysql_error());
        $nr_serv = mysql_num_rows($queS);
        while ($rowS1 = mysql_fetch_assoc($queS)) {
            $rowS[] = $rowS1;
        }
        @mysql_free_result($queS);

        $preturi = [];
        if ($data_start) {
            $s = explode('-', $data_start);
            $start = date("Y-m-d", mktime(0, 0, 0, $s[1], $s[2] - 5, $s[0]));
            $end = date("Y-m-d", mktime(0, 0, 0, $s[1], $s[2] + 5, $s[0]));
        }

        $selP = "SELECT ";
        if ($this->tip_preturi == 'plecari') {
            $selP = $selP . "data_start AS start,
		null AS sfarsit,
		0000-00-00 AS data_end, ";
        } else {
            $selP = $selP . " date_format(data_pret_oferta.data_start , '%d.%m.%Y') AS start,
		date_format(data_pret_oferta.data_end , '%d.%m.%Y') AS sfarsit,
		data_pret_oferta.data_end, ";
        }
        $selP = $selP . " tip_camera.tip,
		data_pret_oferta.data_start,
		data_pret_oferta.pret, 
		data_pret_oferta.moneda,
		tip_camera.denumire,
		data_pret_oferta.tip_camera,
		data_pret_oferta.id_hotel,
		data_pret_oferta.of_logictour,
		tip_masa.denumire AS tip_masa
		FROM data_pret_oferta 
		LEFT JOIN tip_camera on data_pret_oferta.tip_camera = tip_camera.id_camera 
		LEFT JOIN camere_hotel on data_pret_oferta.tip_camera=camere_hotel.id_camera
		LEFT JOIN tip_masa on data_pret_oferta.id_masa = tip_masa.id_masa
		WHERE camere_hotel.id_hotel='" . $this->id_hotel . "'
		AND data_pret_oferta.id_oferta = '" . $id_oferta . "' ";
        if ($data_start) {
            $selP = $selP . " AND (data_pret_oferta.data_start>='" . $start . "' AND data_pret_oferta.data_start<='" . $end . "') ";
        } else {
            $selP = $selP . " AND (((data_pret_oferta.data_end IS NULL OR data_pret_oferta.data_end = '0000-00-00') AND data_pret_oferta.data_start >= NOW()) OR ((data_pret_oferta.data_end IS NOT NULL AND data_pret_oferta.data_end <> '0000-00-00') AND data_pret_oferta.data_end>= NOW())) ";
        }
        $selP = $selP . " GROUP BY data_pret_oferta.id_pret,
		data_pret_oferta.data_start,
		data_pret_oferta.data_end,
		data_pret_oferta.pret,
		data_pret_oferta.moneda,
		data_pret_oferta.tip_camera
		ORDER BY data_pret_oferta.data_start,
		data_pret_oferta.data_end, 
		camere_hotel.ordine ";
        //echo $selP;
        $queP = mysql_query($selP) or die(mysql_error());
        if (mysql_num_rows($queP) > 0) {
            $preturi['min_start'] = '9999-12-31';
            $preturi['max_end'] = '0000-00-00';
            $preturi['minim'] = ['pret' => 9999, 'moneda' => 'EURO'];
            while ($rowP = mysql_fetch_array($queP)) {
                $preturi['id_hotel'] = $rowP['id_hotel'];
                $preturi['of_logictour'] = $rowP['of_logictour'];
                if ($preturi['minim']['pret'] > $rowP['pret']) {
                    $preturi['minim'] = ['pret' => $rowP['pret'], 'moneda' => $rowP['moneda']];
                }
                if ($preturi['min_start'] > $rowP['data_start']) {
                    $preturi['min_start'] = $rowP['data_start'];
                }
                if ($rowP['data_start'] >= $rowP['data_end']) {
                    $max = date('Y-m-d', strtotime($rowP['data_start'] . ' + ' . $this->nr_nopti . ' days'));
                } else {
                    $max = $rowP['data_end'];
                }
                if ($preturi['max_end'] < $max) {
                    $preturi['max_end'] = $max;
                }
                $preturi['camera'][$rowP['tip_camera']] = $rowP['denumire'];

                /*if(sizeof($preturi['tip_masa'])==0) {
                    $index_m=1;
                    $preturi['tip_masa'][$index_m]=$rowP['tip_masa'];
                } else {
                    $index_m='';
                    foreach($preturi['tip_masa'] as $key_m=>$value_m) {
                        if($value_m==$rowP['tip_masa']) $index_m=$key_m;
                    }
                    if(!$index_m) {
                        $index_m=sizeof($preturi['tip_masa'])+1;
                        $preturi['tip_masa'][$index_m]=$rowP['tip_masa'];
                    }
                }*/

                if ($this->tip_preturi == 'plecari') {
                    $start_per = ucwords(strftime('%d %B %Y', strtotime($rowP['start'])));
                } else {
                    $start_per = $rowP['start'];
                }
                if (sizeof($preturi['data_start']) == 0) {
                    $index = 1;
                    $preturi['data_start'][$index] = $start_per;
                    $preturi['data_end'][$index] = $rowP['sfarsit'];
                    $preturi['data_start_normal'][$index] = $rowP['data_start'];
                    $preturi['data_end_normal'][$index] = $rowP['data_end'];
                    $preturi['pret'][$index][$rowP['tip_camera']] = $rowP['pret'];
                    $preturi['moneda'][$index][$rowP['tip_camera']] = $rowP['moneda'];
                } else {
                    $index = '';
                    foreach ($preturi['data_start'] as $key => $value) {
                        if ($value == $start_per && $rowP['sfarsit'] == $preturi['data_end'][$key]) {
                            $index = $key;
                        }
                    }
                    if (!$index) {
                        $index = sizeof($preturi['data_start']) + 1;
                        $preturi['data_start'][$index] = $start_per;
                        $preturi['data_end'][$index] = $rowP['sfarsit'];
                        $preturi['data_start_normal'][$index] = $rowP['data_start'];
                        $preturi['data_end_normal'][$index] = $rowP['data_end'];
                        $preturi['pret'][$index][$rowP['tip_camera']] = $rowP['pret'];
                        $preturi['moneda'][$index][$rowP['tip_camera']] = $rowP['moneda'];
                    } else {
                        $preturi['pret'][$index][$rowP['tip_camera']] = $rowP['pret'];
                        $preturi['moneda'][$index][$rowP['tip_camera']] = $rowP['moneda'];
                    }
                }


                /*//if($rowP['tip']=='principala') {
                //__
                //$preturi['camera'][$rowP['tip_camera']]=$rowP['denumire'];
                if($this->tip_preturi=='plecari') $start_per=ucwords(strftime('%d %B %Y',strtotime($rowP['start'])));
                    else $start_per=$rowP['start'];
                if(sizeof($preturi['data_start'])==0) {
                    $index=1;
                    $preturi['data_start'][$index]=$start_per;
                    $preturi['data_start_normal'][$index]=$rowP['data_start'];
                    $preturi['data_end_normal'][$index]=$rowP['data_end'];
                    $preturi['data_end'][$index]=$rowP['sfarsit'];
                        //$preturi['pret'][$index][$rowP['tip_camera']]=$rowP['pret'];
                        //$preturi['moneda'][$index][$rowP['tip_camera']]=$rowP['moneda'];
                        //$preturi['tip_masa'][$index][$rowP['tip_camera']]=$rowP['tip_masa'];
                        $preturi['pret'][$index][]=$rowP['pret'];
                        $preturi['moneda'][$index][]=$rowP['moneda'];
                        $preturi['tip_masa'][$index][]=$rowP['tip_masa'];
                } else {
                    $index='';
                    foreach($preturi['data_start'] as $key=>$value) {
                        if($value==$start_per && $rowP['sfarsit']==$preturi['data_end'][$key]) $index=$key;
                    }
                    if(!$index) {
                        $index=sizeof($preturi['data_start'])+1;
                        $preturi['data_start'][$index]=$start_per;
                        $preturi['data_end'][$index]=$rowP['sfarsit'];
                        $preturi['data_start_normal'][$index]=$rowP['data_start'];
                        $preturi['data_end_normal'][$index]=$rowP['data_end'];
                            //$preturi['pret'][$index][$rowP['tip_camera']]=$rowP['pret'];
                            //$preturi['moneda'][$index][$rowP['tip_camera']]=$rowP['moneda'];
                            //$preturi['tip_masa'][$index][$rowP['tip_camera']]=$rowP['tip_masa'];
                    } else {
                            //$preturi['pret'][$index][$rowP['tip_camera']]=$rowP['pret'];
                            //$preturi['moneda'][$index][$rowP['tip_camera']]=$rowP['moneda'];
                            //$preturi['tip_masa'][$index][$rowP['tip_camera']]=$rowP['tip_masa'];
                    }
                        $preturi['pret'][$index][]=$rowP['pret'];
                        $preturi['moneda'][$index][]=$rowP['moneda'];
                        $preturi['tip_masa'][$index][]=$rowP['tip_masa'];
                }*/
            }
            //else $preturi['secundar'][$start_per][$rowP['sfarsit']][$rowP['denumire']]=array('pret'=>$rowP['pret'],'moneda'=>$rowP['moneda']);
            //}
            //print_r($preturi);
            /*$max_s=explode('-',$preturi['min_start']);
            $preturi['min_start']=date('d.m.Y',mktime(0,0,0,$max_s[1],$max_s[2],$max_s[0]));
            $max_e=explode('-',$preturi['max_end']);
            $preturi['max_end']=date('d.m.Y',mktime(0,0,0,$max_e[1],$max_e[2],$max_e[0]));*/
            if ($preturi['minim']['pret'] == '9999') {
                $preturi['minim'] = [];
            }
            if (sizeof($tipuri) > 0) {
                foreach ($preturi['data_start_normal'] as $key_d => $data_start) {
                    foreach ($tipuri as $key_t => $value_t) {
                        if ($preturi['data_end_normal'][$key_d] && $preturi['data_end_normal'][$key_d] <> '00.00.0000') {
                            if ($data_start <= $value_t['start'] && $preturi['data_end_normal'][$key_d] >= $value_t['end']) {
                                $preturi['include_tip'][$key_d][] = $value_t;
                            }
                        } else {
                            if ($data_start >= $value_t['start'] && $data_start <= $value_t['end']) {
                                $preturi['include_tip'][$key_d][] = $value_t;
                            }
                        }
                    }
                }
            }
        }
        @mysql_free_result($queP);
        //print_r($rowS);
        if ($nr_serv > 0 && sizeof($preturi['data_start_normal']) > 0) {
            foreach ($preturi['data_start_normal'] as $key_d => $data_start) {
                foreach ($rowS as $key1 => $value1) {
                    if ($value1['data_start'] <= $data_start && $value1['data_end'] >= $preturi['data_end_normal'][$key_d]) {
                        $preturi['secundar'][$data_start][$preturi['data_end_normal'][$key_d]][$value1['denumire']] = [
                            'pret' => $value1['value'],
                            'moneda' => $value1['moneda']
                        ];
                    }
                }
            }
        }

        //print_r($preturi['secundar']);
        return $preturi;
    }

    function select_preturi_pivot($id_hotel, $id_of, $nr_formula)
    {
        $sel_ppv = "SELECT
		pret_pivot_adaugat.data_start,
		pret_pivot_adaugat.adulti,
		pret_pivot_adaugat.copii,
		pret_pivot_adaugat.copil1,
		pret_pivot_adaugat.copil2,
		pret_pivot_adaugat.copil3,
		CONCAT(pret_pivot_adaugat.adulti,' Adl +',' Copil 1 (',pret_pivot_adaugat.copil1,') +',' Copil 2 (',pret_pivot_adaugat.copil2,') +',' Copil 3 (',pret_pivot_adaugat.copil3,')') AS grad_ocupare,
		tip_camera.denumire AS denumire_tip_camera,
		tip_masa.denumire AS denumire_tip_masa
		FROM pret_pivot_adaugat
		LEFT JOIN tip_camera ON tip_camera.id_camera=pret_pivot_adaugat.tip_camera
		LEFT JOIN tip_masa ON tip_masa.id_masa=pret_pivot_adaugat.id_masa
		WHERE pret_pivot_adaugat.pret_pivot='nu'
		AND pret_pivot_adaugat.id_hotel='" . $id_hotel . "'
		AND pret_pivot_adaugat.id_oferta='" . $id_of . "'
		AND pret_pivot_adaugat.nr_formula='" . $nr_formula . "'
		";
        $que_ppv = mysql_query($sel_ppv) or die(mysql_error());
        if (mysql_num_rows($que_ppv) > 0) {
            while ($row_ppv = mysql_fetch_array($que_ppv)) {

                /*if(sizeof($preturi_pivot['tip_masa'])==0) {
                    $index_m=1;
                    $preturi_pivot['tip_masa'][$index_m]=$row_ppv['denumire_tip_masa'];
                } else {
                    $index_m='';
                    foreach($preturi_pivot['tip_masa'] as $key_m=>$value_m) {
                        if($value_m==$row_ppv['denumire_tip_masa']) $index_m=$key_m;
                    }
                    if(!$index_m) {
                        $index_m=sizeof($preturi_pivot['tip_masa'])+1;
                        $preturi_pivot['tip_masa'][$index_m]=$row_ppv['denumire_tip_masa'];
                    }
                }*/

                if (sizeof($preturi_pivot['tip_camera']) == 0) {
                    $index_c = 1;
                    $preturi_pivot['tip_camera'][$index_c] = $row_ppv['denumire_tip_camera'];
                } else {
                    $index_c = '';
                    foreach ($preturi_pivot['tip_camera'] as $key_c => $value_c) {
                        if ($value_c == $row_ppv['denumire_tip_camera']) {
                            $index_c = $key_c;
                        }
                    }
                    if (!$index_c) {
                        $index_c = sizeof($preturi_pivot['tip_camera']) + 1;
                        $preturi_pivot['tip_camera'][] = $row_ppv['denumire_tip_camera'];
                    }
                }

                if (sizeof($preturi_pivot['grad_ocupare']) == 0) {
                    $index_g = 1;
                    $preturi_pivot['grad_ocupare'][$index_g] = $row_ppv['grad_ocupare'];
                } else {
                    $index_g = '';
                    foreach ($preturi_pivot['grad_ocupare'] as $key_g => $value_g) {
                        if ($value_g == $row_ppv['grad_ocupare']) {
                            $index_g = $key_g;
                        }
                    }
                    if (!$index_g) {
                        $index_g = sizeof($preturi_pivot['grad_ocupare']) + 1;
                        $preturi_pivot['grad_ocupare'][] = $row_ppv['grad_ocupare'];
                    }
                }

            }
            @mysql_free_result($que_ppv);

            //asort($preturi_pivot['tip_masa']);
            asort($preturi_pivot['tip_camera']);
            asort($preturi_pivot['grad_ocupare']);
        }

        //print_r($preturi_pivot);
        return $preturi_pivot;
    }

    function select_preturi_circuit($id_oferta)
    {
        $preturi = [];
        $selS = "select * from oferte_servicii where id_oferta = '" . $id_oferta . "' and tip_serv = 'Suplimente' and tip_supliment = '3' Group by denumire, value, moneda, tip_serv, obligatoriu, ordonare Order by ordonare ";
        $queS = mysql_query($selS) or die(mysql_error());
        $nr_serv = mysql_num_rows($queS);
        while ($rowS1 = mysql_fetch_assoc($queS)) {
            $rowS[] = $rowS1;
        }
        @mysql_free_result($queS);

        $selP = "SELECT
		data_pret_oferta.data_start as start,
		data_pret_oferta.data_end as sfarsit,
		tip_camera.tip,
		data_pret_oferta.data_start,
		data_pret_oferta.data_end,
		data_pret_oferta.pret, 
		data_pret_oferta.moneda,
		tip_camera.denumire,
		data_pret_oferta.tip_camera 
		FROM data_pret_oferta 
		left join tip_camera on data_pret_oferta.tip_camera = tip_camera.id_camera 
		WHERE data_pret_oferta.id_oferta = '" . $id_oferta . "' 
		AND (((data_pret_oferta.data_end is null or data_pret_oferta.data_end = '0000-00-00') and data_pret_oferta.data_start >= now()) or ((data_pret_oferta.data_end is not null and data_pret_oferta.data_end <> '0000-00-00') and data_pret_oferta.data_end>= now())) 
		GROUP BY data_pret_oferta.data_start, data_pret_oferta.data_end,
		data_pret_oferta.pret, 
		data_pret_oferta.moneda, 
		data_pret_oferta.tip_camera 
		ORDER BY data_pret_oferta.data_start, data_pret_oferta.data_end, 
		tip_camera.denumire ";
        $queP = mysql_query($selP) or die(mysql_error());

        if (mysql_num_rows($queP) > 0) {
            $preturi['minim'] = ['pret' => 9999, 'moneda' => 'EURO'];
            while ($rowP = mysql_fetch_array($queP)) {
                if ($rowP['tip'] == 'principala') {
                    if ($preturi['minim']['pret'] > $rowP['pret']) {
                        $preturi['minim'] = ['pret' => $rowP['pret'], 'moneda' => $rowP['moneda']];
                    }
                    //__
                    $preturi['camera'][$rowP['tip_camera']] = $rowP['denumire'];
                    if (sizeof($preturi['data_start']) == 0) {
                        $index = 1;
                        $preturi['data_start'][$index] = $rowP['start'];
                        $preturi['data_start_normal'][$index] = $rowP['data_start'];
                        $preturi['data_end_normal'][$index] = $rowP['data_end'];
                        $preturi['data_end'][$index] = $rowP['sfarsit'];
                        $preturi['pret'][$index][$rowP['tip_camera']] = $rowP['pret'];
                        $preturi['moneda'][$index][$rowP['tip_camera']] = $rowP['moneda'];
                    } else {
                        $index = '';
                        foreach ($preturi['data_start'] as $key => $value) {
                            if ($value == $rowP['start'] && $rowP['sfarsit'] == $preturi['data_end'][$key]) {
                                $index = $key;
                            }
                        }
                        if (!$index) {
                            $index = sizeof($preturi['data_start']) + 1;
                            $preturi['data_start'][$index] = $rowP['start'];
                            $preturi['data_end'][$index] = $rowP['sfarsit'];
                            $preturi['data_start_normal'][$index] = $rowP['data_start'];
                            $preturi['data_end_normal'][$index] = $rowP['data_end'];
                            $preturi['pret'][$index][$rowP['tip_camera']] = $rowP['pret'];
                            $preturi['moneda'][$index][$rowP['tip_camera']] = $rowP['moneda'];
                        } else {
                            $preturi['pret'][$index][$rowP['tip_camera']] = $rowP['pret'];
                            $preturi['moneda'][$index][$rowP['tip_camera']] = $rowP['moneda'];
                        }
                    }
                    if ($preturi['min_start'] > $rowP['data_start']) {
                        $preturi['min_start'] = $rowP['data_start'];
                    }
                    if ($rowP['data_start'] >= $rowP['data_end']) {
                        $max = date('Y-m-d', strtotime($rowP['data_start'] . ' + ' . $this->nr_nopti . ' days'));
                    } else {
                        $max = $rowP['data_end'];
                    }
                    if ($preturi['max_end'] < $max) {
                        $preturi['max_end'] = $max;
                    }
                } else {
                    $preturi['secundar'][$rowP['start']][$rowP['sfarsit']][$rowP['denumire']] = ['pret' => $rowP['pret'], 'moneda' => $rowP['moneda']];
                }
            }

            if ($preturi['minim']['pret'] == '9999') {
                $preturi['minim'] = [];
            }
        }
        @mysql_free_result($queP);
        //print_r($rowS);

        if ($nr_serv > 0 && sizeof($preturi['data_start']) > 0) {
            foreach ($preturi['data_start'] as $key_d => $data_start) {
                foreach ($rowS as $key1 => $value1) {
                    if ($value1['data_start'] <= $data_start && $value1['data_end'] >= $data_start) {
                        $preturi['secundar'][$data_start][$preturi['data_end'][$key_d]][$value1['denumire']] = [
                            'pret' => $value1['value'],
                            'moneda' => $value1['moneda']
                        ];
                    }
                }
            }
        }

        //echo '<pre>';print_r($preturi);echo '</pre>';
        return $preturi;
    }

    function get_avion_sejur($id_oferta)
    {
        $sel = "SELECT
		oferte_transport_avion.*,
		tara_plecare.denumire AS denumire_tara_plecare,
		tara_sosire.denumire AS denumire_tara_sosire,
		aeroport_plecare.denumire AS denumire_aeroport_plecare,
		aeroport_sosire.denumire AS denumire_aeroport_sosire,
		companii_aeriene.denumire_companie,
		localitate_sosire.denumire AS denumire_loc_sosire,
		localitate_sosire.id_localitate as id_loc_sosire,
		localitate_plecare.id_localitate AS id_loc_plecare,
		localitate_plecare.denumire AS denumire_loc_plecare
		
		FROM oferte_transport_avion
		LEFT JOIN aeroport AS aeroport_plecare ON oferte_transport_avion.aeroport_plecare = aeroport_plecare.id_aeroport
		LEFT JOIN localitati AS localitate_plecare ON aeroport_plecare.id_localitate = localitate_plecare.id_localitate
		LEFT JOIN tari AS tara_plecare ON aeroport_plecare.id_tara = tara_plecare.id_tara
		LEFT JOIN aeroport AS aeroport_sosire ON oferte_transport_avion.aeroport_sosire = aeroport_sosire.id_aeroport
		LEFT JOIN localitati AS localitate_sosire ON aeroport_sosire.id_localitate = localitate_sosire.id_localitate
		LEFT JOIN tari AS tara_sosire ON aeroport_sosire.id_tara = tara_sosire.id_tara
		LEFT JOIN companii_aeriene ON oferte_transport_avion.companie = companii_aeriene.id_companie
		WHERE oferte_transport_avion.id_oferta = '" . $id_oferta . "'
		GROUP BY oferte_transport_avion.ordonare
		ORDER BY oferte_transport_avion.tip DESC, oferte_transport_avion.companie, oferte_transport_avion.ordonare
		";
        $que = mysql_query($sel) or die(mysql_error());
        while ($row = mysql_fetch_assoc($que)) {
            $aeroport[] = $row;
        }
        @mysql_free_result($que);

        return $aeroport;
    }

    function get_autocar_sejur($id_oferta)
    {
        $sel = "SELECT
		oferte_transport_autocar.*,
		localitati.denumire AS denumire_localitate
		FROM oferte_transport_autocar
		LEFT JOIN localitati ON oferte_transport_autocar.id_localitate = localitati.id_localitate
		WHERE oferte_transport_autocar.id_oferta = '" . $id_oferta . "'
		GROUP BY oferte_transport_autocar.ordonare, oferte_transport_autocar.id_localitate
		ORDER BY oferte_transport_autocar.ora ASC, localitati.denumire ASC ";
        $que = mysql_query($sel) or die(mysql_error());
        while ($row = mysql_fetch_assoc($que)) {
            $autocar[] = $row;
        }
        @mysql_free_result($que);

        return $autocar;
    }

    function get_excursii($id_oferta)
    {
        $excursii = [];
        $t = 0;

        $selExcursii = "SELECT
		excursii.*,
		oferte_excursii.tip,
		preturi.pret,
		preturi.moneda,
		tari.denumire AS tara
		FROM excursii
		INNER JOIN oferte_excursii ON excursii.id_excursie = oferte_excursii.id_excursie
		LEFT JOIN (SELECT
			excursii_preturi.pret,
			excursii_preturi.moneda,
			excursii_preturi.id_excursie
			FROM excursii_preturi
			GROUP BY excursii_preturi.id_excursie
			ORDER BY excursii_preturi.pret DESC
		) AS preturi ON excursii.id_excursie = preturi.id_excursie
		INNER JOIN tari ON excursii.id_tara = tari.id_tara
		WHERE oferte_excursii.id_oferta = '" . $id_oferta . "'
		GROUP BY excursii.id_excursie ";
        $queExcursii = mysql_query($selExcursii) or die(mysql_error());
        while ($row = mysql_fetch_assoc($queExcursii)) {
            $excursii[] = $row;
        }
        @mysql_free_result($queExcursii);

        return $excursii;
    }

    function alete_oferte($id_hotel, $id_tip, $orase, $exceptie)
    {
        $oferte = [];

        $sel_toate = "SELECT
		oferte.id_oferta,
		oferte.denumire AS denumire_oferta,
		oferte.exprimare_pret,
		oferte.nr_zile,
		oferte.nr_nopti,
		oferte.masa,
		hoteluri.poza1,
		hoteluri.nume as denumire_hotel,
		hoteluri.stele,
		hoteluri.descriere as descriere_hotel,
		oferte.exprimare_pret,
		tari.denumire as denumire_tara,
		zone.denumire as denumire_zona,
		localitati.denumire as denumire_localitate,
		oferte.pret_minim,
		oferte.moneda, ";
        if ($early == 'da') {
            $sel_toate = $sel_toate . " early.discount, 
		date_format(early.end_date, '%d/%m/%Y')  as early_end, ";
        }
        $sel_toate = $sel_toate . " transport.denumire as denumire_transport
		FROM oferte ";
        if ($id_tip) {
            $sel_toate = $sel_toate . " inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
        }
        if ($early == 'da') {
            $sel_toate = $sel_toate . " inner Join (select * from early_booking where early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur' Group by id_oferta, end_date, discount Order by end_date) as early on oferte.id_oferta = early.id_oferta ";
        }
        $sel_toate = $sel_toate . " inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		inner Join zone ON localitati.id_zona = zone.id_zona
		inner Join tari ON zone.id_tara = tari.id_tara
		inner join transport on oferte.id_transport = transport.id_trans ";
        $sel_toate = $sel_toate . " WHERE oferte.valabila = 'da' ";
        if ($cazare == 'da') {
            $sel_toate = $sel_toate . " and (oferte.nr_zile <= '2' or oferte.nr_zile is null) and oferte.id_transport = '1' ";
        }
        if ($masa) {
            $sel_toate = $sel_toate . " and oferte.masa = '" . $masa . "' ";
        }
        if ($id_tip) {
            $sel_toate = $sel_toate . " AND oferta_sejur_tip.id_tip_oferta in (" . $id_tip . ") ";
        }
        if ($stele) {
            $sel_toate = $sel_toate . " and hoteluri.stele ='" . $stele . "' ";
        }
        if ($tari) {
            $sel_toate = $sel_toate . " AND tari.id_tara IN (" . $tari . ")";
        }
        if ($zone) {
            $sel_toate = $sel_toate . " AND zone.id_zona IN (" . $zone . ")";
        }
        if ($orase) {
            $sel_toate = $sel_toate . " AND localitati.id_localitate IN (" . $orase . ")";
        }
        if ($transport) {
            $sel_toate = $sel_toate . " AND oferte.id_transport = " . $transport . " ";
        }
        if ($id_hotel) {
            $sel_toate = $sel_toate . " AND hoteluri.id_hotel  ='" . $id_hotel . "' ";
        }
        if ($exceptie) {
            $sel_toate = $sel_toate . " and oferte.id_oferta <> '" . $exceptie . "' ";
        }
        $sel_toate = $sel_toate . " Group by oferte.id_oferta ";
        $que = mysql_query($sel_toate) or die(mysql_error());
        if (mysql_num_rows($que) > 0) {
            while ($row = mysql_fetch_assoc($que)) {
                $oferte[] = $row;
            }
        }
        @mysql_free_result($que);

        return $oferte;
    }
}
