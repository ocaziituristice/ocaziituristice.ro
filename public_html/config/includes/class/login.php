<?php

use App\Models\Useri;

class LOGIN extends SEND_EMAIL
{

    function __construct($tabel)
    {
        $this->err = '';
        $this->tabel = $tabel;
    }

    function logare($user, $pass, $link_ref)
    {
        $user = Useri::findFirst([
            'conditions' => 'email = :email: and password = :passwd: and activ = :activ:',
            'bind' => ['email' => $user, 'passwd' => md5($pass), 'activ' => 'da']
        ]);

        if (!$user instanceof Useri) {
            $err = 'Username-ul sau parola este gresita!<br/>';
            return $err;
        } else {

            $_SESSION['id_user_adm'] = $user->getIdUser();
            $_SESSION['mail'] = $user->getEmail();
            $_SESSION['password'] = $user->getPassword();
            $_SESSION['nume'] = $user->getNume();
            $_SESSION['tip'] = $user->getTip();
            $_SESSION['acceptat'] = $user->getAcceptat();
            $_SESSION['user_mobil'] = $user->getMobil();
            $_SESSION['api_smart_bill'] = $user->getApiSmartBill();
            $_SESSION['acces'] = $user->getAcces();

            $user->setUltimaLogare(date('Y-m-d H:i:s'))->setIp($_SERVER['REMOTE_ADDR'])->save();

            header("LOCATION: " . $link_ref);
        }
    }

    function verifica_user()
    {


        $sel = "SELECT id_user FROM $this->tabel WHERE email = '" . $_SESSION['mail'] . "' AND password = '" . $_SESSION['password'] . "' AND id_user = '" . $_SESSION['id_user_adm'] . "' ";
        $que = mysql_query($sel) or die(mysql_error());
        if (mysql_num_rows($que) <> 1) {
            $err = 'Username-ul sau parola este gresita!<br/>';

            return $err;
        }
    }

    function creare_cont($email, $nume, $tip)
    {
        $mess = '';
        if (strlen($email) < '3') {
            $mess = 'Campul email este obligatoriu!<br/>';
        } elseif (!exista_mail($this->tabel, 'email', $email)) {
            $mess = 'Aceasta adresa de email exista in baza noastra de date!<br/>';
        } elseif (!validate_email($email)) {
            $mess = 'Adresa de email nu este corecta!<br/>';
        }
        if (strlen(trim($nume)) < 3) {
            $mess = $mess . 'Campul nume este obligatoriu!<br/>';
        }
        if (!$tip) {
            $mess = $mess . 'Nu ati selectat tipul de acces!<br/>';
        }
        if (!$mess) {
            $pass = generatePassword('8');
            $pass_cr = md5($pass);

            $ins = "insert into $this->tabel (email, password, nume, tip, acceptat) VALUES ('" . $email . "', '" . $pass_cr . "', '" . $nume . "', '" . $tip . "', 'nu')";
            $queI = mysql_query($ins) or die(mysql_error());
            $id_user = mysql_insert_id();
            @mysql_free_result($queI);

            $ins = "insert into referinta (ref1, ref2) VALUES ('" . $id_user . "', '" . $pass . "')";
            $queI = mysql_query($ins) or die(mysql_error());
            $id_user = mysql_insert_id();
            @mysql_free_result($queI);

            $GLOBALS['home'] = 'www.ocaziituristice.ro' . $GLOBALS['path_adm'];
            $GLOBALS['mail'] = $email;
            $GLOBALS['parola'] = $pass;
            $param['templates'] = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['path_adm'] . 'mail/templates/cont_nou.htm';
            $param['subject'] = 'Creare cont nou';
            $param['to_email'] = $email;
            $param['to_nume'] = $nume;
            $param['fr_email'] = $GLOBALS['email_contact'];
            $param['fr_nume'] = $GLOBALS['denumire_agentie'];
            $this->send($param);

            echo '<script> alert(\'Userul a fost creat!\'); document.location.href=\'creare_cont.php\'; </script>';
        }

        return $mess;
    }

    function schimba_parola($vechea_parola, $parola, $re_parola)
    {
        $sel = "select $this->tabel.password from $this->tabel inner join referinta on $this->tabel.id_user = referinta.ref1 where $this->tabel.id_user= '" . $_SESSION['id_user_adm'] . "' and referinta.ref2 = '$vechea_parola' ";
        $que = mysql_query($sel) or die(mysql_error());
        if (mysql_num_rows($que) > '0') {
            if ($parola == $re_parola) {
                $noua_parola = md5($parola);

                $ins = "update $this->tabel set password = '$noua_parola' where id_user = '" . $_SESSION['id_user_adm'] . "' ";
                $que_u = mysql_query($ins) or die(mysql_error());
                @mysql_free_result($que_u);

                $ins = "update referinta set ref2 = '$parola' where ref1 = '" . $_SESSION['id_user_adm'] . "' ";
                $que_u = mysql_query($ins) or die(mysql_error());
                @mysql_free_result($que_u);

                if ($_COOKIE['password']) {
                    $timeout = time() + 60 * 60 * 24 * 1;
                    setcookie('password', "$parola", $timeout);
                    $user = $_COOKIE['email'];
                    setcookie('email', "$user", $timeout);
                }
                echo '<script>alert("Parola a fost schimbata!"); </script>';

                echo "<meta http-equiv=\"refresh\" content=\"0;url=logout.php\">";
            } else {
                $mesaj = "<center><h4><font color=#ff0000>Nu ati reintrodus bine noua parola!</font></h4></center>";
            }
        } else {
            $mesaj = "<center><h4><font color=#ff0000>Nu ati introdus corect vechea parola!</font></h4></center>";
        }

        return $mesaj;
    }

} ?>