<?php class SEND_EMAIL {
	
public function send($param) {
	require_once($_SERVER['DOCUMENT_ROOT'].'/mail/classes/class.formatmail.php');
	require($_SERVER['DOCUMENT_ROOT'].'/config/seteri.php');
	$this->utilizator=new FormatMail($param['templates']);
	//this->utilizator->Mailer->IsMail();
	$this->utilizator->Mailer->IsSMTP();
	$this->utilizator->Mailer->Host=$GLOBALS['host_email'];
	$this->utilizator->Mailer->SMTPAuth=true;
	$this->utilizator->Mailer->Username=$GLOBALS['user_email'];
	$this->utilizator->Mailer->Password=$GLOBALS['pass_email'];
	$this->utilizator->Mailer->From=$GLOBALS['email_contact'];
	$this->utilizator->Mailer->FromName=$GLOBALS['denumire_agentie'];
	$this->utilizator->Mailer->Subject=$param['subject'];
	$this->utilizator->Mailer->AddAddress($param['to_email'], $param['to_nume']);
	$this->utilizator->Mailer->AddReplyTo($param['fr_email'], $param['fr_nume']);
	$this->utilizator->Mailer->ReturnPath=$GLOBALS['email_contact'];
	$this->utilizator->Send();
}

public function send_mail_normal($param) {
	require_once($_SERVER['DOCUMENT_ROOT'].'/mail/classes/class.formatmail.php');
	require($_SERVER['DOCUMENT_ROOT'].'/config/seteri.php');
	$this->utilizator=new FormatMail($param['templates']);
	$this->utilizator->Mailer->IsMail();
	$this->utilizator->Mailer->From=$param['from_email'];
	$this->utilizator->Mailer->FromName=$GLOBALS['denumire_agentie'];
	$this->utilizator->Mailer->Subject=$param['subject'];
	$this->utilizator->Mailer->AddAddress($param['to_email'], $param['to_nume']);
	$this->utilizator->Mailer->AddReplyTo($param['fr_email'], $param['fr_nume']);
	$this->utilizator->Mailer->ReturnPath=$param['from_email'];
	$this->utilizator->Send();
} 

public function send_rezervari($param) {
	require_once($_SERVER['DOCUMENT_ROOT'].'/mail/classes/class.formatmail.php');
	require($_SERVER['DOCUMENT_ROOT'].'/config/seteri.php');
	$this->utilizator=new FormatMail($param['templates']);
	$this->utilizator->Mailer->IsSMTP();
	$this->utilizator->Mailer->Host=$GLOBALS['host_email'];
	$this->utilizator->Mailer->SMTPAuth=true;
	$this->utilizator->Mailer->Username=$GLOBALS['rezervare_user'];
	$this->utilizator->Mailer->Password=$GLOBALS['rezervare_pass'];
	$this->utilizator->Mailer->From=$GLOBALS['email_rezervare'];
	$this->utilizator->Mailer->FromName=$GLOBALS['denumire_agentie'];
	$this->utilizator->Mailer->Subject=$param['subject'];
	$this->utilizator->Mailer->AddAddress($param['to_email'], $param['to_nume']);
	$this->utilizator->Mailer->AddReplyTo($param['fr_email'], $param['fr_nume']);
	$this->utilizator->Mailer->ReturnPath=$GLOBALS['email_rezervare'];
	$this->utilizator->Send();
} 

public function send_mail_adm($param,$attachment) {
	require_once($_SERVER['DOCUMENT_ROOT'].'/mail/classes/class.formatmail.php');
	require($_SERVER['DOCUMENT_ROOT'].'/config/seteri.php');
	$this->utilizator=new FormatMail($param['templates']);
	$this->utilizator->Mailer->IsSMTP();
	$this->utilizator->Mailer->Host=$GLOBALS['host_email'];
	//$this->utilizator->Mailer->Port=$GLOBALS['port_email'];
	$this->utilizator->Mailer->SMTPAuth=true;
	$this->utilizator->Mailer->Username=$GLOBALS['rezervare_user'];
	$this->utilizator->Mailer->Password=$GLOBALS['rezervare_pass'];
	$this->utilizator->Mailer->From=$_SESSION['mail'];
	$this->utilizator->Mailer->FromName=$_SESSION['nume'];
	$this->utilizator->Mailer->Subject=$param['subject'];
	$this->utilizator->Mailer->AddAddress($param['to_email'], $param['to_nume']);
	$this->utilizator->Mailer->AddReplyTo($param['fr_email'], $param['fr_nume']);
	$this->utilizator->Mailer->ReturnPath=$_SESSION['mail'];
	//$this->utilizator->Mailer->SMTPDebug = 1;
	if(isset($attachment)) {
		foreach($attachment as $key) {
			$this->utilizator->Mailer->AddAttachment($key);
		}
	}
	$this->utilizator->Send();
} 

} ?>