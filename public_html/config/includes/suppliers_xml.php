<?php
class ws_geography {
	public $Id;
	public $Name;
	public $IntName;
	public $ChildLabel;
	public $Description;
	public $Image;
	public $Children = array();
	public $MinVal;
	public $MaxVal;
	public $TreeLevel;
}
class ws_packagesearch {
	public $Currency;
	public $IsTour = false;
	public $IsFlight = false;
	public $IsBus = false;
	public $Departure;
	public $Destination;
	public $Hotel;
	public $DepartureDate;
	public $Duration;
	public $MinStars;
	public $Rooms = array();
	public $ShowBlackedOut = true;
}
class ws_room {
	public $Adults=0;
	public $ChildAges = array();
}

class XML_REZEDA {
	var $databaseName;
	var $userName;
	var $password;
	
	var $sUrl_Authentication = 'http://5.2.132.78/tvwebServicesNew/Login.asmx';
	var $sUrl_Common = 'http://5.2.132.78/tvwebServicesNew/Common.asmx';
	var $sUrl_StaticData = 'http://5.2.132.78/tvwebServicesNew/StaticData.asmx';
	var $sUrl_Hotel = 'http://5.2.132.78/tvwebServicesNew/Hotel.asmx';
	var $sUrl_Flight='http://5.2.132.78/tvwebServicesNew/Flight.asmx';
	var $sUrl_Reservation = 'http://b2b.adagiotour.com/sws/Reservation.asmx';
	var $sUrl_SejourControl = 'http://b2b.adagiotour.com/sws/SejourControl.asmx';
	var $sUrl_SejourHotelInfo = 'http://b2b.adagiotour.com/sws/SejourHotelInfo.asmx';
	
	
	var $charset_utf8 = 'utf-8';
	var $charset_utf16 = 'utf-16';	

	function send_request($xml_part,$encoding, $sUrl) {
	$sXml = '<?xml version="1.0" encoding="'.$encoding.'"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body>'.$xml_part.'</soap:Body></soap:Envelope>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $sUrl);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($sXml) ));
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		$sResponse = curl_exec($ch);
		$sResponse = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $sResponse);
		return $sResponse;
	}
	
	function Login() {
		$xml_part = '<tryLogin xmlns="http://www.tourvisio.com/">
			<db>'.$this->databaseName.'</db>
			<agency>'.$this->AgencyName.'</agency>
			<user>'.$this->userName.'</user>
			<pwd>'.$this->password.'</pwd>		
		</tryLogin>';
		return $xml_part;
	}
	

	
	
	function Logout($token) {
		$xml_part = '<tryLogout xmlns="http://www.tourvisio.com/">
			<token>'.$token.'</token>
		</tryLogout>';
		return $xml_part;
	}
	
	function Country($token)  
	{
	$xml_part = '<GetCountry xmlns="http://www.tourvisio.com/">
      <token>'.$token.'</token>
   
    </GetCountry>';
		return $xml_part;
		
		
		}
	function Hotels($token) {
		$xml_part = '<SearchHotelList xmlns="http://www.tourvisio.com/">
	      <token>'.$token.'</token>
      <location>4</location>
      <minRecordDate>0001-01-01T00:00:00</minRecordDate>
      
    </SearchHotelList>';
		return $xml_part;
	}
	
	

function GetDepCity($token) {
$xml_part ='<GetDepCity xmlns="http://www.tourvisio.com/">
        <token>'.$token.'</token>
      <errorMsg>string</errorMsg>
    </GetDepCity>';
return $xml_part;	
}

function GetArrCity($token,$id_tara,$id_oras_plecare) {
 $xml_part ='<GetArrCity xmlns="http://www.tourvisio.com/">
      <token>'.$token.'</token>
      <country>'.$id_tara.'</country>
      <depCity>'.$id_oras_plecare.'</depCity>
      <packType>H</packType>
      <errorMsg>string</errorMsg>
    </GetArrCity>';
return $xml_part;	
}

function GetFlightListForB2C($token,$id_oras_plecare){
 $xml_part ='
    <GetFlightListForB2C xmlns="http://www.tourvisio.com/">
      <token>'.$token.'</token>
      <depCity>'.$id_oras_plecare.'</depCity>
      <checkAvailability>true</checkAvailability>
      <errorMsg>string</errorMsg>
    </GetFlightListForB2C>';
return $xml_part;
}

	
}


class XML_ADAGIO {
	var $databaseName;
	var $userName;
	var $password;
	
	var $sUrl_Authentication = 'http://b2b.adagiotour.com/sws/Authentication.asmx';
	var $sUrl_Common = 'http://b2b.adagiotour.com/sws/Common.asmx';
	var $sUrl_Hotel = 'http://b2b.adagiotour.com/sws/Hotel.asmx';
	var $sUrl_Reservation = 'http://b2b.adagiotour.com/sws/Reservation.asmx';
	var $sUrl_SejourControl = 'http://b2b.adagiotour.com/sws/SejourControl.asmx';
	var $sUrl_SejourHotelInfo = 'http://b2b.adagiotour.com/sws/SejourHotelInfo.asmx';
	
	var $charset_utf8 = 'utf-8';
	var $charset_utf16 = 'utf-16';	

	function send_request($xml_part, $encoding, $sUrl) {
	$sXml = '<?xml version="1.0" encoding="'.$encoding.'"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body>'.$xml_part.'</soap:Body></soap:Envelope>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $sUrl);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($sXml) ));
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		$sResponse = curl_exec($ch);
		$sResponse = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $sResponse);
		
		return $sResponse;
	}


	
	function TEST($token) {
		/*$xml_part = '<GetSPOTypeDS xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<insertEmptyRecord>false</insertEmptyRecord>
		</GetSPOTypeDS>';*/
		$xml_part = '<GetDetailSpo xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<hotelCode>BLTITA</hotelCode>
			<SPONumber>37</SPONumber>
		</GetDetailSpo>';
		return $xml_part;
	}
	
	function Login() {
		$xml_part = '<Login xmlns="http://www.sansejour.com/">
			<databaseName>'.$this->databaseName.'</databaseName>
			<userName>'.$this->userName.'</userName>
			<password>'.$this->password.'</password>
			<Language>EN</Language>
			<ipAdrs>::1</ipAdrs>
			<dmn />
			<ipCode />
		</Login>';
		return $xml_part;
	}
	
	function Logout($token) {
		$xml_part = '<Logout xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
		</Logout>';
		return $xml_part;
	}
	
	function Hotels($token) {
		$xml_part = '<GetHotelListDS xmlns="http://www.sansejour.com/">
	      <token>'.$token.'</token>
    	  <region />
	      <trfregion />
    	  <category />
	      <room />
    	  <roomtype />
	      <board />
    	  <web>false</web>
	      <mail>false</mail>
    	</GetHotelListDS>';
		return $xml_part;
	}
	
	function HotelRooms($token, $cod_hotel) {
		$xml_part = '<GetRoomTypeDS xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<hotelCode>'.$cod_hotel.'</hotelCode>
			<insertEmptyRecord>false</insertEmptyRecord>
		</GetRoomTypeDS>';
		return $xml_part;
	}
	
	function Prices($token, $plecdata, $pleczile, $hotel, $adulti, $copii, $copil1=NULL, $copil2=NULL, $copil3=NULL) {
		$xml_part = '<PriceSearch xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<searchRequest>
				<CheckIn>'.$plecdata.'T00:00:00</CheckIn>
				<Night>'.$pleczile.'</Night>
				<HoneyMoon>false</HoneyMoon>
				<OnlyAvailable>false</OnlyAvailable>
				<Currency />
				<HotelCode>'.$hotel.'</HotelCode>
				<RegionCode />
				<CategoryCode />
				<RoomCode />
				<RoomTypeCode />
				<BoardCode />
				<CalculateHandlingFee>false</CalculateHandlingFee>
				<HandFeeType />
				<CalculateTransfer>false</CalculateTransfer>
				<TransferCriteria>
					<ArrivalTransferType />
					<DepartureTransferOK>false</DepartureTransferOK>
					<DepartureTransferType />
					<Airport />
					<TransferRegion />
				</TransferCriteria>
				<RoomCriterias>
					<RoomCriteria>
						<Adult>'.$adulti.'</Adult>
						<Child>'.$copii.'</Child>';
		if($copii>0) {
			$xml_part .= '<ChildAges>';
				if($copil1) $xml_part .= '<int>'.$copil1.'</int>';
				if($copil2) $xml_part .= '<int>'.$copil2.'</int>';
				if($copil3) $xml_part .= '<int>'.$copil3.'</int>';
			$xml_part .= '</ChildAges>';
		} else {
			$xml_part .= '<ChildAges/>';
		}
		$xml_part .= '</RoomCriteria>
				</RoomCriterias>
			</searchRequest>
		</PriceSearch>';
		return $xml_part;
	}
	
	function SpoDetails($token, $hotel, $spo_number) {
	$xml_part = '<GetDetailSpo xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<hotelCode>'.$hotel.'</hotelCode>
			<SPONumber>'.$spo_number.'</SPONumber>
		</GetDetailSpo>';
		return $xml_part;
	}	
	function anulare($token,$cinDate,$nr_zile ,$hotel, $tip_camera,$cod_camera)
	 {
/*$xml_part = '<New_Allotment_Control xmlns="http://www.sansejour.com/">
      <token>'.$token.'</token>
      <cinDate>'.$cinDate.'T00:00:00</cinDate>
      <day>'.$nr_zile.'</day>
      <hotelCode>'.$hotel.'</hotelCode>
      <roomTypeCode>'.$tip_camera.'</roomTypeCode>
      <roomCode>'.$cod_camera.'</roomCode>
      <allotmentType>N</allotmentType>
      <isUrun>false</isUrun>
      <releaseerrorMsg />
      <alloterrorMsg />
      <ekAllotStr />
      <roomCount>1</roomCount>
    </New_Allotment_Control>';*/
	
	
	
	
 $xml_part = '<ReleaseControl xmlns="http://www.sansejour.com/">
      <token>'.$token.'</token>
      <cinDate>'.$cinDate.'T00:00:00</cinDate>
     <hotelCode>'.$hotel.'</hotelCode>
     <roomCode>'.$cod_camera.'</roomCode>
       <roomTypeCode>'.$tip_camera.'</roomTypeCode>
      <AllotmentType>N</AllotmentType>
      <errorMsg />
      <rDays>10</rDays>
      <rBegin>'.$cinDate.'T00:00:00</rBegin>
      <rEnd>'.$cinDate.'T00:00:00</rEnd>
      <resAllow>10</resAllow>
    </ReleaseControl>';
	
	
		return $xml_part;
	}	
	
	
}

class XML_MOUZENIDIS {
	var $login;
	var $hashkey;
	
	//http://tourml.mouzenidis.com/ws/TourML.asmx/GetReferences?queryType=CountryById&param=0&login=dreamvoyage&Password=jTzzFb1PElzvxOwsNvGiE7s1Vxw
	var $main_link = 'http://tourml.mouzenidis.com/ws/TourML.asmx';
	var $request_link = 'http://217.23.143.180/search/serviceMainSearch.svc';
	
	function construct_link($query, $param) {
		$link = $this->main_link.'/GetReferences?queryType='.$query.'&param='.$param.'&login='.$this->login.'&Password='.$this->hashkey;
		return $link;
	}
	
	function get_countries() {
		$sResponse = file_get_contents($this->construct_link('CountryById', '0'));
		$countries = new SimpleXMLElement($sResponse);
		
		$array = array();
		foreach($countries->references->countries->country as $key => $value) {
			$array[(string) $value['key']] = (string) $value['nameLat'];
		}
		asort($array);
		//echo '<pre>';print_r($array);echo '</pre>';
		return $array;
	}
	
	function get_cities($country_id) {
		$sResponse = file_get_contents($this->construct_link('CityByCountryId', $country_id));
		$cities = new SimpleXMLElement($sResponse);
		
		$array = array();
		foreach($cities->references->cities->city as $key => $value) {
			$array[(string) $value['key']] = (string) $value['nameLat'];
		}
		asort($array);
		//echo '<pre>';print_r($array);echo '</pre>';
		return $array;
	}
	
	function get_hotel_by_country($country_id) {
		$sResponse = file_get_contents($this->construct_link('HotelByCountryId', $country_id));
		$hotels = new SimpleXMLElement($sResponse);
		
		$array = array();
		foreach($hotels->references->cities->city as $k_city => $v_city) {
			$array['cities'][(string) $v_city['key']] = (string) $v_city['nameLat'];
		}
		foreach($hotels->references->categories->category as $k_categ => $v_categ) {
			$array['stele'][(string) $v_categ['key']] = (string) $v_categ['stdKey'];
		}
		foreach($hotels->references->hotels->hotel as $k_hotel => $v_hotel) {
			$idh[$k_hotel] = (string) $v_hotel['key'];
			$array['hotels'][$idh[$k_hotel]]['nume'] = (string) $v_hotel['nameLat'];
			$array['hotels'][$idh[$k_hotel]]['tara'] = (string) $v_hotel['countryKey'];
			$array['hotels'][$idh[$k_hotel]]['oras'] = (string) $v_hotel['cityKey'];
		}
		foreach($hotels->references->buildings->building as $k_stars => $v_stars) {
			$array['hotels'][(string) $v_stars['hotelKey']]['stele'] = $array['stele'][(string) $v_stars['categoryKey']];
		}
		//echo '<pre>';print_r($array);echo '</pre>';
		return $array;
	}
	
	function get_hotel_rooms($hotel_id) {
		$sResponse = file_get_contents($this->construct_link('HotelById', $hotel_id));
		$cities = new SimpleXMLElement($sResponse);
		
		$array = array();
		foreach($cities->references->roomTypes->roomType as $key => $value) {
			$array['rooms'][(string) $value['key']] = (string) $value['nameLat'];
		}
		asort($array);
		echo '<pre>';print_r($array);echo '</pre>';
		return $array;
	}
}

class XML_TRIP_CORPORATION {
	var $databaseName;
	var $userName;
	var $password;
	
	var $sUrl_Authentication = 'https://b2b.hotels-bulgaria-online.com/sws/Authentication.asmx';
	var $sUrl_Common = 'https://b2b.hotels-bulgaria-online.com/sws/Common.asmx';
	var $sUrl_Hotel = 'https://b2b.hotels-bulgaria-online.com/sws/Hotel.asmx';
	var $sUrl_Reservation = 'https://b2b.hotels-bulgaria-online.com/sws/Reservation.asmx';
	var $sUrl_SejourControl = 'https://b2b.hotels-bulgaria-online.com/sws/SejourControl.asmx';
	var $sUrl_SejourHotelInfo = 'https://b2b.hotels-bulgaria-online.com/sws/SejourHotelInfo.asmx';
	
	var $charset_utf8 = 'utf-8';
	var $charset_utf16 = 'utf-16';	

	function send_request($xml_part, $encoding, $sUrl) {
	
	$sXml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>'.$xml_part.'</soap:Body></soap:Envelope>';
 // if(!$err_logare_admin) { echo '<pre>';print_r($sXml);echo '</pre>'; }
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $sUrl);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,true);
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-s8', 'Content-Length: '.strlen($sXml) ));
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	
	 curl_error($ch);
	$sResponse = curl_exec($ch);
	
	
 $sResponse = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $sResponse);
		
	 return $sResponse;
	}
	function spolist($token) {
	 $xml_part = '<GetSPOTypeDS xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<insertEmptyRecord>false</insertEmptyRecord>
		</GetSPOTypeDS>';
		/*echo $xml_part = '<GetDetailSpo xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<hotelCode>'.$hotel.'</hotelCode>
			<SPONumber>4</SPONumber>
		</GetDetailSpo>'*/;
		return $xml_part;
	}
	function spodetails($token,$hotel,$spo_number) {
	/*echo	 $xml_part = '<GetSPOTypeDS xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<insertEmptyRecord>false</insertEmptyRecord>
		</GetSPOTypeDS>';*/
		$xml_part = '<GetDetailSpo xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<hotelCode>'.$hotel.'</hotelCode>
			<SPONumber>3</SPONumber>
		</GetDetailSpo>';
		return $xml_part;
	}
	
	function Login() {
		$xml_part = '<Login xmlns="http://www.sansejour.com/">
			<databaseName>'.$this->databaseName.'</databaseName>
			<userName>'.$this->userName.'</userName>
			<password>'.$this->password.'</password>
			<Language>EN</Language>
		<dmn></dmn>
      <ipAdrs></ipAdrs>
      <ipCode></ipCode>
		</Login>';
		
	//echo $xml_part;
	return $xml_part;
	}
	
	function Logout($token) {
		$xml_part = '<Logout xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
		</Logout>';
		return $xml_part;
	}
	
	function Hotels($token) {
	$xml_part ='<GetHotelListDS xmlns="http://www.sansejour.com/">
      <token>'.$token.'</token>
      <region></region>
      <trfregion />
      <category />
      <room />
      <roomtype />
      <board />
      <web>false</web>
      <mail>false</mail>
    </GetHotelListDS>';
		return $xml_part;
	}
	
	function HotelRooms($token, $cod_hotel) {
		$xml_part = '<GetRoomTypeDS xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<hotelCode>'.$cod_hotel.'</hotelCode>
			<insertEmptyRecord>false</insertEmptyRecord>
		</GetRoomTypeDS>';
		return $xml_part;
	}
	
	function Prices($token, $plecdata, $pleczile, $hotel, $adulti, $copii, $copil1=0, $copil2=0, $copil3=0) {






echo $xml_part = '<priceSearchWithTrf2 xmlns="http://www.sansejour.com/">
<token>'.$token.'</token>
<selldate>'.date('Y-m-d').'T00:00:00</selldate>
<cindate1>'.$plecdata.'T00:00:00</cindate1>
<cindate2>'.date('Y-m-d', strtotime($plecdata.' + '.$pleczile.' days')).'T00:00:00</cindate2>
<region/>
<category />
<room></room>
<roomtype></roomtype>
<board></board>
<hotel>'.$hotel.'</hotel>
<price1>0</price1>
<price2>1000000</price2>
<adl>'.$adulti.'</adl>
<chd>'.$copii.'</chd>
<cy1>'.$copil1.'</cy1>
<cy2>'.$copil2.'</cy2>
<cy3>'.$copil3.'</cy3>
<cy4>0</cy4>
<kg1>'.$pleczile.'</kg1>
<kg2>0</kg2>
<kg3>0</kg3>
<kg4>0</kg4>
<Currency>EUR</Currency>
<SPOType>-2</SPOType>
<priceSearchtype>1</priceSearchtype>
<honeym>N</honeym>
<comRatio>0</comRatio>
<comAmount>0</comAmount>
<comAmountDaily>0</comAmountDaily>
<flight>0</flight>
<other>0</other>
<ohm>N</ohm>
<calcTrf>false</calcTrf>
<arrTrfType/>
<depTrfType/>
<deptrfOk>false</deptrfOk>
<airport/>
<comissionType>false</comissionType>
<insuranceTotal>0</insuranceTotal>
<isInsuranceTotal>false</isInsuranceTotal>
<flightCur></flightCur>
<insuranceCur></insuranceCur>
<otherCur/>
<trfRegion/>
<handFeeType>-1</handFeeType>
<GetMinPrice>false</GetMinPrice>
<calcKickBackWebUser>-1</calcKickBackWebUser>
<withAvailRoomCount>true</withAvailRoomCount>
<IsCalculateEB>false</IsCalculateEB>
</priceSearchWithTrf2>';
		return $xml_part;
	}
/*
StopSaleControl

REQUEST

<token>5b9daff3-f2a0-42f3-b516-8f350f3b6588</token>

<hotelCode>ADAM</hotelCode>

<checkIn>19.12.2009 00:00:00</checkIn>

<checkOut>26.12.2009 00:00:00</checkOut>

<room>DBL</room>

<roomType>DSG</roomType>

RESPONSE

<StopSaleControlResult>true</StopSaleControlResult>

AllotmentControl

REQUEST

<token>5b9daff3-f2a0-42f3-b516-8f350f3b6588</token>

<errorMsg></errorMsg>

<CinDate>12/19/2009 12:00:00 AM</CinDate>

<COutDate>12/26/2009 12:00:00 AM</COutDate>

<HotelCode>ADAM</HotelCode>

<RoomCode>DBL</RoomCode>

<RoomTypeCode>DSG</RoomTypeCode>

<AllotmentType>N</AllotmentType>

<RoomCount>1</RoomCount>

RESPONSE

<AllotmentControlResult>true</AllotmentControlResult>

<errorMsg></errorMsg>



	$xml_part = '<priceSearch2 xmlns="http://www.sansejour.com/">
               <token>'.$token.'</token>
         <selldate>2014-10-10T00:00:00</selldate>
         <cindate1>2014-12-04T00:00:00</cindate1>
         <cindate2>2014-12-14T00:00:00</cindate2>
                <region>SOF</region>
                 <category></category>
                  <room></room>
                  <roomtype></roomtype>
                  <board></board>
                  <hotel>LIONBG</hotel>
         <price1>0</price1>
         <price2>10000</price2>
         <adl>2</adl>
         <chd>0</chd>
         <cy1></cy1>
         <cy2></cy2>
         <cy3></cy3>
         <cy4></cy4>
         <kg1>7</kg1>
         <kg2></kg2>
         <kg3></kg3>
         <kg4></kg4>
         <Currency></Currency>
         <SPOType>-2</SPOType>
         <priceSearchtype>0</priceSearchtype>
         <honeym>N</honeym>
         <comRatio>0.0</comRatio>
         <comAmount>0.0</comAmount>
         <comAmountDaily>1</comAmountDaily>
         <flight>0.0</flight>
         <other>0.0</other>
         <ohm>N</ohm>
         <comissionType>false</comissionType>
         <insuranceTotal>double</insuranceTotal>
         <isInsuranceTotal>false</isInsuranceTotal>
         <flightCur>AUD</flightCur>
         <insuranceCur>AUD</insuranceCur>
         <otherCur>AUD</otherCur>
         <onlyavailable>0</onlyavailable>
         <trfRegion>SOF</trfRegion>
         <handFeeType></handFeeType>
         <GetMinPrice>0</GetMinPrice>
         <calcKickBackWebUser>false</calcKickBackWebUser>
      </priceSearch2>';
		//return $xml_part;
	}	*/
	

/*function Prices($token, $plecdata, $pleczile, $hotel, $adulti, $copii, $copil1=NULL, $copil2=NULL, $copil3=NULL) {
		$xml_part = '<PriceSearch2 xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<searchRequest>
				<CheckIn>'.$plecdata.'T00:00:00</CheckIn>
				<Night>'.$pleczile.'</Night>
				<HoneyMoon>false</HoneyMoon>
				<OnlyAvailable>false</OnlyAvailable>
				<Currency />
				<HotelCode>'.$hotel.'</HotelCode>
				<RegionCode />
				<CategoryCode />
				<RoomCode />
				<RoomTypeCode />
				<BoardCode />
				<CalculateHandlingFee>false</CalculateHandlingFee>
				<HandFeeType />
				<CalculateTransfer>false</CalculateTransfer>
				<TransferCriteria>
					<ArrivalTransferType />
					<DepartureTransferOK>false</DepartureTransferOK>
					<DepartureTransferType />
					<Airport />
					<TransferRegion />
				</TransferCriteria>
				<RoomCriterias>
					<RoomCriteria>
						<Adult>'.$adulti.'</Adult>
						<Child>'.$copii.'</Child>';
		if($copii>0) {
			$xml_part .= '<ChildAges>';
				if($copil1) $xml_part .= '<int>'.$copil1.'</int>';
				if($copil2) $xml_part .= '<int>'.$copil2.'</int>';
				if($copil3) $xml_part .= '<int>'.$copil3.'</int>';
			$xml_part .= '</ChildAges>';
		} else {
			$xml_part .= '<ChildAges/>';
		}
		$xml_part .= '</RoomCriteria>
				</RoomCriterias>
			</searchRequest>
		</PriceSearch2>';
		return $xml_part;
	}*/
	

	
function disponibilitate($token, $plecdata, $pleczile, $hotel, $room,$room_type,$tip_alotment) {
$xml_part = '<New_Allotment_Control xmlns="http://www.sansejour.com/">
			<token>'.$token.'</token>
			<cinDate>'.$plecdata.'T00:00:00</cinDate>
			<day>'.$pleczile.'</day>
			<hotelCode>'.$hotel.'</hotelCode>
			<roomTypeCode>'.$room.'</roomTypeCode>
			<roomCode>'.$room_type.'</roomCode>
			<allotmentType>'.$tip_alotment.'</allotmentType>
			<isUrun>false</isUrun>
<releaseerrorMsg />
<alloterrorMsg />
<ekAllotStr />
<roomCount>1</roomCount>
			</New_Allotment_Control>';

 $xml_part = '<Allotments xmlns="http://www.sansejour.com/">
      <token>'.$token.'</token>
      <DateFrom>'.$plecdata.'T00:00:00</DateFrom>
      <DateTo>'.date('Y-m-d', strtotime($plecdata.' + '.$pleczile.' days')).'T00:00:00</DateTo>
      <hotel>'.$hotel.'</hotel>
    </Allotments>';
return $xml_part ;
}

}


?>