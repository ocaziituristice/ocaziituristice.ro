<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Plata online <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Termeni si conditii Ocazii Turistice" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <h1 class="blue">Plata online</h1>
        
        <div class="Hline"></div>
        
        <div class="pad20 article" align="justify">

		  <?php /*?><h2 class="green"><img src="<?php echo $imgpath; ?>/icon_thumbs_up.png" alt="" style="vertical-align:bottom;" /> &nbsp; Plata dumneavoastra a fost inregistrata cu succes! Va multumim!</h2><?php */?>

		  <h2 class="red"><img src="<?php echo $imgpath; ?>/icon_warning.png" alt="" style="vertical-align:middle;" /> &nbsp; ATENTIE! Plata dumneavoastra a fost <strong>refuzata</strong>, iar cardul <strong>NU</strong> a fost debitat!</h2>
          <h2 class="blue">Va rugam mai incercati o data sa efectuati plata si daca primiti acelasi mesaj, va rugam sa ne contactati.</h2>
          <p>&nbsp;</p>
          <h2>Posibile cauze pentru refuzarea platii:</h2>
          <ul>
            <li>Fonduri insuficiente pe card</li>
            <li>Cardul este blocat pentru platile online</li>
            <li>Probleme tehnice cu sistemul de plati online</li>
          </ul>
          <h2>Va multumim pentru intelegere!</h2>

          <p>&nbsp;</p>
          <a href="<?php echo $sitepath; ?>" class="link-blue">revino la prima pagina &raquo;</a>
          <br /><br />
          <a href="http://blog.ocaziituristice.ro" class="link-blue" target="_blank">citeste blogul nostru pentru mai multe impresii &raquo;</a>

        </div>
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>