<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Agentie de turism online | Ocaziituristice.ro</title>
<meta name="description" content="Mergi la sigur cu Ocaziituristice.ro. Agentie de turism din Bucuresti cu o oferta variata pentru pachete turistice in Romania si externe. Profita acum de reduceri" />
<meta name="keywords" content="agentie de turism, sejururi, last minute, early booking, circuite, oferte revelion, city break, oferte balneo" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner" style="padding:10px 0 0 0;">
    <div class="NEW-column-full">
      <div id="NEW-index" class="clearfix">
      
        <?php /*?><div class="tmain blue"><h1>Agentia de turism</h1>
         DREAM VOYAGE va pune la dispozitie pe <strong class="black">www.ocaziituristice.ro</strong> o gama variata de pachete turistice.</div><?php */?>
        
        <div class="NEW-search NEW-round8px">
		  <div class="chapter-title white">Cauta vacanta:</div>
          <div id="afis_filtru"></div>
        </div>
        
        <div class="float-right" style="width:720px; height:285px;">
        <div id="jflowSlider" class="NEW-round8px">
          <div id="mySlides">
          
            <div id="slide0" class="slide">
              <a href="/hoteluri-costa-da-caparica/hotel-costa-da-caparica/seniori-la-lisabona-hotel-costa-da-caparica-9797.html"><img src="/images/index/banners_300813_caparica.jpg" alt="Seniori Costa Da Caparica" class="NEW-round8px" /></a>
            </div>  

            <div id="slide1" class="slide">
              <a href="/hoteluri-ticleni/hotel-sara/revelion-all-inclusive-6119.html"><img src="/images/index/banners_300813_ticleni.jpg" alt="Revelion Ticleni" class="NEW-round8px" /></a>
            </div>   

            <div id="slide2" class="slide">
              <a href="/hoteluri-dubai/hotel-five-continents-casselles-ghantoot/seniori-in-dubai-otp-hotel-five-continents-casselles-ghantoot-10090.html"><img src="/images/index/banners_300813_dubai.jpg" alt="Seniori Dubai" class="NEW-round8px" /></a>
            </div>   

            <div id="slide3" class="slide">
              <a href="/circuite/asia/israel/"><img src="/images/index/banners_300813_israel.jpg" alt="Pelerinaje Israel" class="NEW-round8px" /></a>
            </div>   

            <div id="slide4" class="slide">
              <a href="/hoteluri-cannes/hotel-thomas/seniori-la-cannes-10208.html"><img src="/images/index/banners_300813_cannes.jpg" alt="Seniori Cannes" class="NEW-round8px" /></a>
            </div>   

            <?php /*?><div id="slide5" class="slide">    
              <a href="/oferte-paste/bulgaria/"><img src="/images/index/banner_paste-bulgaria_03-01-2012.jpg" alt="Paste Bulgaria" class="NEW-round8px" /></a>
              <div class="slideContent">
                <h3>Super Oferte de Paste !</h3>
                <p>Petrece Sarbatorile Pascale pe litoralul Bulgaresc.</p>
              </div>
            </div><?php */?>
            
            <?php /*<div id="slide5" class="slide">
              <a href="/hoteluri-istanbul/hotel-de-3-stele/istanbul-4-nopti-7511.html"><img src="/images/index/banner-istanbul.jpg" alt="Super oferta Istanbul" class="NEW-round8px" /></a>
            </div>  */  ?>	

          </div>
          <div id="myController">
            <span class="jFlowControl">1</span>
            <span class="jFlowControl">2</span>
            <span class="jFlowControl">3</span>
            <span class="jFlowControl">4</span>
            <span class="jFlowControl">5</span>
            <?php /*?><span class="jFlowControl">6</span><?php */?>
          </div>
          <div class="jFlowPrev"></div>
          <div class="jFlowNext"></div>
        </div>
        </div>
        
        <br class="clear">
        
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate_index.php"); ?>
        
        <br class="clear">
        
        <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix">
          <h2 class="blue float-left"><a href="/oferte-litoral/" title="Oferte Litoral 2013">Litoral 2013</a></h2>
          <a href="/oferte-litoral/" class="float-right link-black" rel="nofollow">vezi toate ofertele Litoral 2013</a>
          <br class="clear">
          <div class="item">
            <a href="/sejur-romania/litoral/mamaia/" rel="nofollow"><img src="/images/index/litoral-mamaia.jpg" alt="Cazare Mamaia" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/mamaia/" class="link-black" title="Cazare Mamaia">Cazare Mamaia 2013</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/eforie-nord/" rel="nofollow"><img src="/images/index/litoral-eforie-nord.jpg" alt="Cazare Eforie Nord" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/eforie-nord/" class="link-black" title="Cazare Eforie Nord">Cazare Eforie Nord 2013</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/neptun/" rel="nofollow"><img src="/images/index/litoral-neptun.jpg" alt="Cazare Neptun" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/neptun/" class="link-black" title="Cazare Neptun">Cazare Neptun 2013</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/jupiter/" rel="nofollow"><img src="/images/index/litoral-jupiter.jpg" alt="Cazare Jupiter" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/jupiter/" class="link-black" title="Cazare Jupiter">Cazare Jupiter 2013</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/saturn/" rel="nofollow"><img src="/images/index/litoral-saturn.jpg" alt="Cazare Saturn" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/saturn/" class="link-black" title="Cazare Saturn">Cazare Saturn 2013</a>
          </div>
          <br class="clear">
        </div></div>
        
        <br class="clear">
        
        <?php /*?><div class="chenar chn-color-red NEW-round8px"><div class="inner NEW-round6px clearfix">
          <h2 class="red float-left"><a href="/sejur-turcia/" title="Oferte sejut Turcia">Oferte Turcia</a></h2>
          <a href="/sejur-turcia/" class="float-right link-black" title="oferte Turcia">vezi toate ofertele de sejur din Turcia</a>
          <br class="clear">
          <div class="item">
            <a href="/sejur-turcia/antalya/"><img src="/images/index/charter-antalya.jpg" alt="Oferte Antalya" title="Oferte Antalya" class="NEW-round8px"></a>
            <a href="/sejur-turcia/antalya/" title="Oferte Antalya" class="link-black">Oferte Antalya</a>
          </div>
          <div class="item">
            <a href="/sejur-turcia/kusadasi/" title="Oferte Kusadasi"><img src="/images/index/charter-kusadasi.jpg" alt="Oferte Kusadasi" class="NEW-round8px"></a>
            <a href="/sejur-turcia/kusadasi/" class="link-black" title="Oferte Kusadasi">Oferte Kusadasi</a>
          </div>
          <div class="item">
            <a href="/sejur-turcia/bodrum/" title="Oferte Bodrum">
            <img src="/images/index/charter-bodrum.jpg" alt="Oferte Bodrum" class="NEW-round8px"></a>
            <a href="/sejur-turcia/bodrum/" class="link-black" title="Oferte Bodrum">Oferte Bodrum</a>
          </div>
          <div class="item">
            <a href="/sejur-turcia/marmaris/" ><img src="/images/index/charter-marmaris.jpg" alt="Oferte Marmaris" class="NEW-round8px" title="Oferte Marmaris"></a>
            <a href="/sejur-turcia/marmaris/" class="link-black" title="Oferte Marmaris">Oferte Marmaris</a>
          </div>
          <div class="item">
            <a href="/sejur-turcia/istanbul/" ><img src="/images/index/charter-istanbul.jpg" alt="Oferte Istanbul" class="NEW-round8px" title="Oferte Istanbul"></a>
            <a href="/sejur-turcia/istanbul/" class="link-black" title="Oferte Istanbul">Oferte Istanbul</a>
          </div>
          <br class="clear">
        </div></div>
        
        <br class="clear"><?php */?>
        
        <?php /*?><div class="chenar chn-color-blue NEW-round8px"><div class="inner NEW-round6px clearfix">
          <h2 class="blue float-left"><a href="/sejur-grecia/?optiuni=da&amp;transport=avion" title="Oferte charter Turcia">Charter Grecia</a></h2>
          <a href="/sejur-grecia/?optiuni=da&amp;transport=avion" class="float-right link-black" rel="nofollow">vezi toate ofertele Charter Grecia</a>
          <br class="clear">
          <div class="item">
            <a href="/sejur-grecia/creta/" rel="nofollow"><img src="/images/index/charter-creta.jpg" alt="Charter Creta" class="NEW-round8px"></a>
            <a href="/sejur-grecia/creta/" class="link-black">Charter Creta</a>
          </div>
          <div class="item">
            <a href="/sejur-grecia/santorini/" rel="nofollow"><img src="/images/index/charter-santorini.jpg" alt="Charter Santorini" class="NEW-round8px"></a>
            <a href="/sejur-grecia/santorini/" class="link-black">Charter Santorini</a>
          </div>
          <div class="item">
            <a href="/sejur-grecia/corfu/" rel="nofollow"><img src="/images/index/charter-corfu.jpg" alt="Charter Corfu" class="NEW-round8px"></a>
            <a href="/sejur-grecia/corfu/" class="link-black">Charter Corfu</a>
          </div>
          <div class="item">
            <a href="/sejur-grecia/insula-rodos/" rel="nofollow"><img src="/images/index/charter-rodos.jpg" alt="Charter Rodos" class="NEW-round8px"></a>
            <a href="/sejur-grecia/insula-rodos/" class="link-black">Charter Rodos</a>
          </div>
          <div class="item">
            <a href="/sejur-grecia/mykonos/" rel="nofollow"><img src="/images/index/charter-mykonos.jpg" alt="Charter Mykonos" class="NEW-round8px"></a>
            <a href="/sejur-grecia/mykonos/" class="link-black">Charter Mykonos</a>
          </div>
          <br class="clear">
        </div></div>
        
        <br class="clear"><?php */?>
        
<?php
$link_col1 = '/sejur-romania/statiuni-balneare/';

$sel_col1 = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND zone.id_zona = '1497'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_col1 = mysql_query($sel_col1) or die(mysql_error());

if(mysql_num_rows($que_col1)>0) {
?>
        <div class="chenar chn-color-orange NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h2 class="red"><a href="<?php echo $link_col1; ?>" title="Oferte Tratament Statiuni Balneare Romania">Statiuni Balneare Romania</a></h2>
          <a href="<?php echo $link_col1; ?>" rel="nofollow"><img src="/images/index/banner_statiuni-balneare-index_25-06-2013.jpg" alt="Statiuni Balneare Romania" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_col1 = mysql_fetch_array($que_col1)) {
        if($row_col1['oferte_moneda']=='EURO') $min_price_col1 = final_price_euro($row_col1['min_price']); else $min_price_col1 = $row_col1['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_col1.fa_link($row_col1['den_localitate']).'/'; ?>" title="Cazare <?php echo $row_col1['den_localitate']; ?> - Tratament <?php echo $row_col1['den_localitate']; ?>"><?php echo $row_col1['den_localitate']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_col1; ?></span> <?php echo substr($row_col1['moneda'], 0, 3); ?></span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="<?php echo $link_col1; ?>" rel="nofollow" class="link-black">vezi toate ofertele Statiuni Balneare Romania</a></div>
        </div></div>
<?php } ?>
        
<?php
$link_seniori = '/oferte-program-pentru-seniori/';

$sel_seniori = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND oferta_sejur_tip.id_tip_oferta IN ('66,72,74,75')
GROUP BY tari.denumire
ORDER BY numar DESC, tari.denumire ASC
LIMIT 0,6 ";
$que_seniori = mysql_query($sel_seniori) or die(mysql_error());

if(mysql_num_rows($que_seniori)>0) {
?>
        <div class="chenar chn-color-blue NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h2 class="blue"><a href="<?php echo $link_seniori; ?>" title="Oferte Programe Seniori">Programe Seniori</a></h2>
          <a href="<?php echo $link_seniori; ?>" rel="nofollow"><img src="/images/index/banner_seniori-index_04-01-2012.jpg" alt="Programe Seniori" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
<?php /*?><?php
$c2_link[1]='/oferte-program-pentru-seniori/spania/';
$c2_title[1]='Program Seniori Spania';
$c2_titlu[1]='Spania';
$c2_pret[1]='339';
$c2_moneda[1]='EUR';

for($i=1; $i<=6; $i++) {
?>
            <div class="item2 clearfix">
              <a href="<?php echo $c2_link[$i]; ?>" title="<?php echo $c2_title[$i]; ?>"><?php echo $c2_titlu[$i]; ?></a>
              <span class="tarif"><span class="value"><?php echo $c2_pret[$i]; ?></span> <?php echo $c2_moneda[$i]; ?></span>
            </div>
<?php } ?><?php */?>
	<?php while($row_seniori = mysql_fetch_array($que_seniori)) {
        if($row_seniori['oferte_moneda']=='EURO') $min_price_seniori = final_price_euro($row_seniori['min_price']); else $min_price_seniori = $row_seniori['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_seniori.fa_link($row_seniori['den_tara']).'/'; ?>" title="Program Seniori <?php echo $row_seniori['den_tara']; ?>"><?php echo $row_seniori['den_tara']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_seniori; ?></span> <?php echo substr($row_seniori['moneda'], 0, 3); ?></span>
            </div>
	<?php } ?>
          </div>
		  <div class="text-right"><a href="<?php echo $link_seniori; ?>" rel="nofollow" class="link-black">vezi toate ofertele Programe Seniori</a></div>
        </div></div>
<?php } ?>
       
<?php
$link_city_break = '/oferte-city-break/';

$sel_city_break = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND oferta_sejur_tip.id_tip_oferta IN ('14')
AND tari.denumire <> 'Romania'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_city_break = mysql_query($sel_city_break) or die(mysql_error());

if(mysql_num_rows($que_city_break)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h2 class="green"><a href="<?php echo $link_city_break; ?>" title="Oferte City Break">City Break</a></h2>
          <a href="<?php echo $link_city_break; ?>" rel="nofollow"><img src="/images/index/banner_city-break-index_04-01-2012.jpg" alt="City Break" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_city_break = mysql_fetch_array($que_city_break)) {
		if($row_city_break['oferte_moneda']=='EURO') $min_price_city_break = final_price_euro($row_city_break['min_price']); else $min_price_city_break = $row_city_break['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_city_break.fa_link($row_city_break['den_tara']).'/'.fa_link($row_city_break['den_zona']).'/'.fa_link($row_city_break['den_localitate']).'/'; ?>" title="City Break <?php echo $row_city_break['den_localitate']; ?>"><?php echo $row_city_break['den_localitate']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_city_break; ?></span> <?php echo substr($row_city_break['moneda'], 0, 3); ?></span>
            </div>
	<?php } ?>
          </div>
		  <div class="text-right"><a href="<?php echo $link_city_break; ?>" rel="nofollow" class="link-black">vezi toate ofertele City Break</a></div>
        </div></div>
<?php } ?>
        
        <br class="clear">
        
<?php
$sel_circuite = "SELECT
oferte.id_oferta,
oferte.denumire AS denumire_oferta,
oferte.denumire_scurta AS denumire_oferta_scurta,
oferte.exprimare_pret,
oferte.nr_zile,
oferte.nr_nopti,
oferte.masa,
oferte.pret_minim,
oferte.moneda,
hoteluri.id_hotel,
hoteluri.poza1,
hoteluri.nume AS denumire_hotel,
hoteluri.stele,
hoteluri.tip_unitate,
continente.nume_continent,
tari.denumire AS denumire_tara,
zone.denumire AS denumire_zona,
localitati.denumire AS denumire_localitate,
transport.denumire AS denumire_transport
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
INNER JOIN transport ON oferte.id_transport = transport.id_trans
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
GROUP BY oferte.id_hotel
ORDER BY rand()
LIMIT 0,6 ";
$que_circuite = mysql_query($sel_circuite) or die(mysql_error());

if(mysql_num_rows($que_circuite)>0) {
?>
        <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix">
          <h2 class="blue float-left"><a href="/circuite/" title="Circuite">Circuite</a></h2>
          <a href="/circuite/" class="float-right link-black" rel="nofollow">vezi toate Circuitele</a>
          <br class="clear">
	<?php while($row_circuite = mysql_fetch_array($que_circuite)) {
		$link_circuit = make_link_circuit($row_circuite['denumire_hotel'], $row_circuite['id_oferta']);
		?>
          <div class="of2cols NEW-round8px clearfix">
            <a href="<?php echo $link_circuit; ?>" rel="nofollow"><span class="image" style="background-image:url(<?php echo '/thumb_hotel/'.$row_circuite['poza1']; ?>);"></span></a>
            <div class="titlu"><a href="<?php echo $link_circuit; ?>" class="link-blue" title="<?php echo $row_circuite['denumire_oferta']; ?>"><?php echo $row_circuite['denumire_oferta']; ?></a></div>
            <div class="pret blue"><?php echo 'de la <span class="red">'.new_price($row_circuite['pret_minim']).' '.moneda($row_circuite['moneda']).'</span> '.$row_circuite['exprimare_pret']; ?></div>
            <?php if($row_circuite['denumire_transport']!='') { ?><div class="field"><p class="camp">Transport:</p><p class="valoare"><?php echo $row_circuite['denumire_transport']; ?></p></div><?php } ?>
            <?php if($row_circuite['nr_zile']!='') { ?><div class="field"><p class="camp">Durata:</p><p class="valoare"><?php echo $row_circuite['nr_zile'].' zile / '.$row_circuite['nr_nopti'].' nopti'; ?></p></div><?php } ?>
            <?php if($row_circuite['masa']!='') { ?><div class="field"><p class="camp">Masa:</p><p class="valoare"><?php echo $row_circuite['masa']; ?></p></div><?php } ?>
          </div>
	<?php } ?>
          <br class="clear">
        </div></div>
<?php } ?>

        <br class="clear">

        <?php /*?><div class="chenar chn-color-green NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h2 class="green"><a href="/early-booking/" title="Early Booking">Early Booking</a></h2>

          <div class="item4 clearfix">
            <a href="/sejur-romania/?optiuni=da&amp;early-booking=da" title="Early Booking Romania" class="stanga"><img src="/images/index/romania-logo.jpg" alt="Early Booking Romania"></a>
            <div class="dreapta big-procent blue">30%</div>
          </div>

          <div class="item4 clearfix">
            <a href="/sejur-bulgaria/?optiuni=da&amp;early-booking=da" title="Early Booking Bulgaria" class="stanga"><img src="/images/index/bulgaria-logo.jpg" alt="Early Booking Bulgaria"></a>
            <div class="dreapta big-procent blue">25%</div>
          </div>

          <div class="item4 clearfix">
            <a href="/sejur-turcia/?optiuni=da&amp;early-booking=da" title="Early Booking Turcia" class="stanga"><img src="/images/index/turcia-logo.jpg" alt="Early Booking Turcia"></a>
            <div class="dreapta big-procent blue">25%</div>
          </div>

          <div class="item4 clearfix">
            <a href="/sejur-grecia/?optiuni=da&amp;early-booking=da" title="Early Booking Grecia" class="stanga"><img src="/images/index/grecia-logo.jpg" alt="Early Booking Grecia"></a>
            <div class="dreapta big-procent blue">25%</div>
          </div>

        </div></div><?php */?>
        
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h2 class="green text-center underline">Charterele Verii</h2>

          <div class="item4-new clearfix">
            <a href="/sejur-turcia/?optiuni=da&transport=avion" title="Charter Turcia" class="stanga"><img src="/images/index/turcia-logo.jpg" alt="Charter Turcia"></a>
            <a href="/sejur-grecia/?optiuni=da&transport=avion" title="Charter Grecia" class="stanga"><img src="/images/index/grecia-logo.jpg" alt="Charter Grecia"></a>
          </div>

          <div class="item4-new clearfix">
            <a href="/sejur-spania/?optiuni=da&transport=avion" title="Charter Spania" class="stanga"><img src="/images/index/spania-logo.jpg" alt="Charter Spania"></a>
            <a href="/sejur-portugalia/?optiuni=da&transport=avion" title="Charter Portugalia" class="stanga"><img src="/images/index/portugalia-logo.jpg" alt="Charter Portugalia"></a>
          </div>

          <div class="item4-new clearfix">
            <a href="/sejur-italia/?optiuni=da&transport=avion" title="Charter Italia" class="stanga"><img src="/images/index/italia-logo.jpg" alt="Charter Italia"></a>
            <a href="/sejur-franta/?optiuni=da&transport=avion" title="Charter Franta" class="stanga"><img src="/images/index/franta-logo.jpg" alt="Charter Franta"></a>
          </div>

        </div></div>
        
        <div class="fb-like-box float-left NEW-round6px" style="width:318px; margin-right:10px; border:1px solid #CCC;" data-href="<?php echo $facebook_page; ?>" data-width="318" data-height="450" data-show-faces="true" data-stream="false" data-show-border="false" data-header="true"></div>
        
        <div class="float-left" style="width:320px;">

          <div class="chenar chn-color-blue NEW-round8px"><div class="inner NEW-round6px clearfix">
            <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_form.php"); ?>
          </div></div>

          <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix">
            <h2 class="black"><a href="http://blog.ocaziituristice.ro" target="_blank">Blog Ocaziituristice.ro</a></h2>
<?php 
require_once('magpierss/rss_fetch.inc');
$url = 'http://blog.ocaziituristice.ro/feed';
$rss = fetch_rss($url);
//echo "Site: ", $rss->channel['title'], "<br>";

$sf=sizeof($rss->items);
for($i=0;$i<$sf-8;$i++) {

    $item = $rss->items[$i];
	//print_r($item);
	$title = $item[title];
	$url = $item[link];
	$desc = $item[description];
	$date = date('d M Y, H:i',$item[date_timestamp]);
?>
<div class="item3">
  <a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="link-blue bold" target="_blank"><?php echo $title; ?></a>
  <div class="desc"><?php echo truncate_str($desc, 120).' [...]'; ?></div>
  <div class="date"><?php echo $date; ?></div>
</div>
<?php } ?>
          </div></div>

        </div>
        
        <br class="clear">
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script type="text/javascript">
$("#afis_filtru").load("/filtru.php");
</script>

<script src="/js/jflow.plus.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#myController").jFlow({
		controller: ".jFlowControl",
		slideWrapper: "#jFlowSlider",
		slides: "#mySlides",
		selectedWrapper: "jFlowSelected",
		effect: "flow",
		width: "720px",
		height: "250px",
		duration: 1,
		pause: 4000,
		prev: ".jFlowPrev",
		next: ".jFlowNext",
		auto: true
    });
});
</script>
</body>
</html>