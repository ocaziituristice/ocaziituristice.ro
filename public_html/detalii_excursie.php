<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/peste_tot.php');
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_excursie.php');
$id_excursie=$_REQUEST['id_excursie'];
$det = new DETALII_EXCURSIE;
$detalii=$det->select_det_excursie($id_excursie);
if($_GET['tara']<>fa_link($detalii['tara'])) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();	
} ?>

<div id="NEW-detaliiOferta" style="width:100%;">
  <span class="titlu_modala"><?php echo $detalii['denumire']; ?></span>

  <?php /*?><ul class="NEW-socials" style="float:right; margin:0;">
    <li><a href="http://www.facebook.com/sharer.php?u=<?php echo $sitepath.substr($_SERVER['REQUEST_URI'],1); ?>?ref=facebook" class="NEW-facebook-24" target="_blank">Facebook</a></li>
    <li><a href="http://twitter.com/home?status=<?php echo $sitepath.substr($_SERVER['REQUEST_URI'],1); ?>?ref=twitter" class="NEW-twitter-24" target="_blank">Twitter</a></li>
	<li style="width:70px;"><div class="g-plusone"></div></li>
    <li><div class="fb-like" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div></li>
  </ul><?php */?>
  
  
    
  <br class="clear" />
  
  <div class="NEW-gallery">
<?php
for($i=1;$i<=3;$i++) {
if($detalii['poza'.$i]) { ?>    
    <img src="<?php echo $sitepath_parinte; ?>img_prima_excursii/<?php echo $detalii['poza'.$i]; ?>" width="250" height="250" alt="" class="images" />
<?php }
} ?>
  </div>

  <br class="clear" />
  
  <div class="NEW-detalii-oferta">
    <?php afisare_frumos($detalii["descriere"], 'nl2p'); ?>
    <br class="clear" />
  </div>
  <?php if(sizeof($detalii['pret'])>0) { ?>
  <div style="font-size:1.15em;">Preturi disponibile pentru <strong class="blue"><em><?php echo $detalii['denumire']; ?></em></strong> din <strong class="blue"><em><?php echo $detalii['tara']; ?></em></strong></div>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabel-preturi">
      <thead>
      <tr>
       <th align="center" valign="top">Varsta</th>
       <th align="center" valign="top">Pret</th> 
      </tr>
      </thead>
    <tbody>
    <?php $nr_t=0; foreach($detalii['pret'] as $key_p=>$value_p) {
	if($nr_t%2==1) $class_tr='impar'; else $class_tr='par'; ?>
    <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
    <td align="center" valign="middle"><?php if($detalii['varsta_min'][$key_p] || $detalii['varsta_max'][$key_p]) { echo $detalii['varsta_min'][$key_p]; if($detalii['varsta_max'][$key_p]) echo ' - '.$detalii['varsta_max'][$key_p]; } else echo ' - '; ?></td>
    <td align="center" valign="middle" class="pret vtip last"><?php if($detalii['pret'][$key_p]) { echo $detalii['pret'][$key_p].' '; if($detalii['moneda'][$key_p]=='EURO') { echo '&euro;'; $valuta='da'; } elseif($detalii['moneda'][$key_p]=='USD') { echo '$'; $valuta='da'; } else echo  $detalii['moneda'][$key_p]; } ?></td>
    <?php } ?>
    </tbody>  
  </table>
  <?php } ?>    
</div>
<br class="clear" />
