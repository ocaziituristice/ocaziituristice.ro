<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip = "program pentru seniori";
$linktip = "program-pentru-seniori";
$dentip = "Seniori";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Programe Seniori - Senior Voyage 2016 | ocaziituristice.ro</title>
<meta name="description" content="Oferte programe pentru seniori, pachete senior voyage, sociale, oferta completa programe pentru seniori circuite si sejururi " />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte Seniori - pachete Seniori Voyage 2016</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear" />
		
        <div class="Hline"></div>
  
        <div class="pad5">
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Oferte <?php echo $dentip; ?>:</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
          
        </div>

        <?php if(date("Y-m-d")<"2014-07-01") { ?>
        <a href="/circuit/europa/croaziera-brizele-mediteranei-2013-all-inclusive-8281.html" title="Croaziera Seniori All Inclusive"><img src="/images/banner/banner_croaziera-brizele-mediteranei_27-03-2013.jpg" alt="Croaziera Seniori All Inclusive"></a>
        <br class="clear"><br>
        <?php } ?>
        
		<?php /*//afisare oferte speciale ?>  
		<?php
        $oferte_rec=new AFISARE_SEJUR_NORMAL();
        $oferte_rec->setAfisare(1, 4);
        $oferte_rec->setRecomandata('da');
        $oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_early_booking.tpl");
		$oferte_rec->setTipOferta($linktip);
        $nr_hoteluri=$oferte_rec->numar_oferte();
        if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
        ?>
        <div class="ofRec" style="padding:8px 5px 6px 5px;">
          <h3 class="blue">Oferte recomandate pentru Programe Seniori</h3>
		  <?php $oferte_rec->afiseaza();?>
          <br class="clear" />
        </div>
        <?php } ?>

        <br class="clear" /><br />
         
        <h3 class="blue">Destinatii recomandate pentru Programe Seniori</h3>
		<?php //afisare oferte speciale*/ ?> 
		
       <?php /*?> <a href="/circuite/israel/" title="Circuite Israel" class="cb-item" style="background:url(images/circuite-israel.jpg) left top no-repeat;"><span class="titlu"><img src="/images/icon_nou.png" alt="Nou" style="margin:-45px 5px 0 -35px;" />Circuite Israel</span></a><?php */?>
        
		<?php $sel_sen="SELECT
		COUNT(distinct(oferte.id_hotel)) AS numar,
		tari.denumire AS denumire_tara,
		tari.steag
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel=hoteluri.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id=localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona=zone.id_zona
		INNER JOIN tari ON zone.id_tara=tari.id_tara
		LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
		WHERE oferte.valabila='da'
		AND hoteluri.tip_unitate<>'Circuit'
		AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
		AND oferte.last_minute='nu'
		GROUP BY denumire_tara
		ORDER BY numar DESC, denumire_tara ASC ";
		$rez_sen=mysql_query($sel_sen) or die(mysql_error());
		while ($row_sen=mysql_fetch_array($rez_sen)) {
			$den_tara=$row_sen['denumire_tara'];
			$link='/oferte-'.$linktip.'/'.fa_link($den_tara).'/';
			if($den_tara=='Portugalia') {
				$den_lunga_tara='style="letter-spacing:-1.5pt;"';
			}
		?>
        <a href="<?php echo $link; ?>" title="Oferte Programe Seniori <?php echo $den_tara; ?>" class="cb-item" style="background:url(images/seniori-<?php echo fa_link($den_tara); ?>.jpg) left top no-repeat;"><span class="titlu" <?php echo $den_lunga_tara; ?>>Seniori <?php echo $den_tara; ?> <img src="/thumb_steag_tara/<?php echo $row_sen['steag']; ?>" alt="steag <?php echo $den_tara; ?>" /></span></a>
		<?php } @mysql_free_result($rez_sen); ?>
        
        <br class="clear"><br>
        
        <div class="pad10 newText">
          <img src="images/icon-programe-seniori.jpg" alt="Oferte Seniori" class="float-left" style="margin:3px 10px 0 0;" />
          <h2>Programe pentru Seniori în Europa</h2>
          <p class="text-justify">Sejururi atractive, la preţuri pentru pensionari, pentru relaxare după o viaţă de muncă. Seniorul poate fi însoţit de întreaga familie de juniori, la aceleaşi preţuri. Cele mai numeroase şi deosebit de bine organizate sunt ofertele pentru <a href="/oferte-program-pentru-seniori/spania/" title="Oferte Programe Seniori Spania" class="link-black"><strong>Spania</strong></a> (Mallorca, Costa del Sol, Costa Brava) si <a href="/oferte-program-pentru-seniori/cipru/" title="Oferte Programe Seniori Cipru" class="link-black"><strong>Cipru</strong></a> (Limassol, Larnaca). Sfatul nostru este: invitaţi un senior din familie şi însoţiţi-l într-o astfel de călătorie. Ofertele pentru programele turistice pentru seniori, au început să se răspândească în întreaga lume! Beneficiază acum de ofertele de seniori pentru Israel, şi vizitează locurile sfinte.</p>
          
          <br class="clear">
          
          <h3 class="blue pad0 mar0">Informaţii şi condiţii generale pentru Programele pentru Seniori</h3>
          <br>
          <p class="text-justify"><strong>Programul pentru Seniori</strong> se adresează cetăţenilor europeni de <strong>peste 55 ani</strong> (şi nu numai) prin care pot beneficia de un pachet complet de servicii turistice pentru a îşi petrece vacanţa cu <strong>reduceri substanţiale</strong> în perioada octombrie-mai, putând vizita <em>cele mai cunoscute locuri turistice şi istorice</em>.</p>
          <p class="text-justify">Prin această iniţiativă, se oferă tuturor posibilitatea de a descoperi lumea şi frumuseţile ei, aceste tipuri de programe încadrându-se în <em>&quot;turism social&quot;</em>.</p>
          <p class="text-justify">În ultima vreme s-au mai adus modificări asupra condiţiilor acestor programe, astfel că, pentru a putea beneficia de aceste super reduceri, <strong>cel puţin unul din cei 2 ocupanţi</strong> ai unei camere duble trebuie să aibă vârsta <strong>minimă de 55 ani</strong>.</p>
          <p class="text-justify">Există posibilitatea ca la unele oferte să achiziţionaţi <em>&quot;Senior Voyage Club Card&quot;</em>, astfel beneficiind de anumite extra oferte.</p>
        </div>
        
      </div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=<?php echo $id_tip_sejur; ?>");
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>