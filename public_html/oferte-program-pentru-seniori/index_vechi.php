<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip = "program pentru seniori";
$linktip = "program-pentru-seniori";
$dentip = "Seniori";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri'];
$link_new = '/oferte-program-pentru-seniori/';

$sel_dest = "SELECT

COUNT(oferte.id_oferta) AS numar,
zone.denumire as denumire_zona,
zone.descriere_scurta as descriere_scurta_zona,
zone.id_zona as id_zona,
tari.denumire as denumire_tara,
tari.id_tara,
tari.descriere_scurta
FROM oferte
INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
 and id_tip_oferta='66' and zone.id_zona!='1497'
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY zone.id_zona
ORDER BY numar DESC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['den_tara'][] = $row_dest['denumire_tara'];
	$destinatie['numar'][] = $row_dest['numar'];
	$destinatie['link'][] = '/oferte-program-pentru-seniori/'.fa_link($row_dest['denumire_tara']).'/'.fa_link($row_dest['denumire_zona']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$row_dest['id_tara'].'/';
	$destinatie['id_tara'][] = $row_dest['id_tara'];
	$destinatie['info_scurte'][] = nl2br($row_dest['descriere_scurta_zona']);
	//$destinatie['info_scurte'][] = "Seniori ".$row_dest['denumire_zona']." -".$row_dest['denumire_tara'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
} @mysql_free_result($rez_dest);
?>


<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Programe Seniori - Senior Voyage 2016-2017 | ocaziituristice.ro</title>
<meta name="description" content="Oferte programe pentru seniori, pachete senior voyage, sociale, oferta completa programe pentru seniori circuite si sejururi " />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte Seniori - pachete Seniori Voyage 2016-2017</h1>
     <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
        
        <br class="clear" />
		
        <div class="Hline"></div>
  
        <div class="pad5">
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Oferte <?php echo $dentip; ?>:</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
          
        </div>

        <?php if(date("Y-m-d")<"2014-07-01") { ?>
        <a href="/circuit/europa/croaziera-brizele-mediteranei-2013-all-inclusive-8281.html" title="Croaziera Seniori All Inclusive"><img src="/images/banner/banner_croaziera-brizele-mediteranei_27-03-2013.jpg" alt="Croaziera Seniori All Inclusive"></a>
        <br class="clear"><br>
        <?php } ?>
        
		<?php /*//afisare oferte speciale ?>  
		<?php
        $oferte_rec=new AFISARE_SEJUR_NORMAL();
        $oferte_rec->setAfisare(1, 4);
        $oferte_rec->setRecomandata('da');
        $oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_early_booking.tpl");
		$oferte_rec->setTipOferta($linktip);
        $nr_hoteluri=$oferte_rec->numar_oferte();
        if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
        ?>
        <div class="ofRec" style="padding:8px 5px 6px 5px;">
          <h3 class="blue">Oferte recomandate pentru Programe Seniori</h3>
		  <?php $oferte_rec->afiseaza();?>
          <br class="clear" />
        </div>
        <?php } ?>

        <br class="clear" /><br />
         
        <h3 class="blue">Destinatii recomandate pentru Programe Seniori</h3>
		<?php //afisare oferte speciale*/ ?> 
		
       <?php /*?> <a href="/circuite/israel/" title="Circuite Israel" class="cb-item" style="background:url(images/circuite-israel.jpg) left top no-repeat;"><span class="titlu"><img src="/images/icon_nou.png" alt="Nou" style="margin:-45px 5px 0 -35px;" />Circuite Israel</span></a><?php */?>


 <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <div id="destinations"  class="clearfix">
          <?php foreach($destinatie['den_zona'] as $key1 => $value1) { ?>
          <div class="destItem NEW-round4px clearfix" >
            <div class="topBox" onclick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
              <img src="/images/zone/<?php echo fa_link($destinatie['den_zona'][$key1]); ?>.jpg" alt="Seniori <?php echo $destinatie['den_zona'][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>
              <h2>Seniori <?php echo $destinatie['den_zona'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?>
            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?>
            </div><?php } ?>
            <div class="bottomBox clearfix">
              <div class="price" align="center"><span class="red"><span class="pret"><?php echo $destinatie['numar'][$key1]; ?></span> </span> Oferte Seniori <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte Seniori <?php echo $destinatie['den_zona'][$key1].' - '.$destinatie['den_tara'][$key1]; ?> ">
			  <?php echo $destinatie['den_zona'][$key1].' - '.$destinatie['den_tara'][$key1]; ?></a></span></span></div>
             
            </div>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        
	
        
        <br class="clear"><br>
        
        <div class="pad10 newText">
          <img src="images/icon-programe-seniori.jpg" alt="Oferte Seniori" class="float-left" style="margin:3px 10px 0 0;" />
          <h2>Programe pentru Seniori în Europa</h2>
          <p class="text-justify">Sejururi atractive, la preţuri pentru pensionari, pentru relaxare după o viaţă de muncă. Seniorul poate fi însoţit de întreaga familie de juniori, la aceleaşi preţuri. Cele mai numeroase şi deosebit de bine organizate sunt ofertele pentru <a href="/oferte-program-pentru-seniori/spania/" title="Oferte Programe Seniori Spania" class="link-black"><strong>Spania</strong></a> (Mallorca, Costa del Sol, Costa Brava) si <a href="/oferte-program-pentru-seniori/cipru/" title="Oferte Programe Seniori Cipru" class="link-black"><strong>Cipru</strong></a> (Limassol, Larnaca). Sfatul nostru este: invitaţi un senior din familie şi însoţiţi-l într-o astfel de călătorie. Ofertele pentru programele turistice pentru seniori, au început să se răspândească în întreaga lume! Beneficiază acum de ofertele de seniori pentru Israel, şi vizitează locurile sfinte.</p>
          
          <br class="clear">
          
          <h3 class="blue pad0 mar0">Informaţii şi condiţii generale pentru Programele pentru Seniori</h3>
          <br>
          <p class="text-justify"><strong>Programul pentru Seniori</strong> se adresează cetăţenilor europeni de <strong>peste 55 ani</strong> (şi nu numai) prin care pot beneficia de un pachet complet de servicii turistice pentru a îşi petrece vacanţa cu <strong>reduceri substanţiale</strong> în perioada octombrie-mai, putând vizita <em>cele mai cunoscute locuri turistice şi istorice</em>.</p>
          <p class="text-justify">Prin această iniţiativă, se oferă tuturor posibilitatea de a descoperi lumea şi frumuseţile ei, aceste tipuri de programe încadrându-se în <em>&quot;turism social&quot;</em>.</p>
          <p class="text-justify">În ultima vreme s-au mai adus modificări asupra condiţiilor acestor programe, astfel că, pentru a putea beneficia de aceste super reduceri, <strong>cel puţin unul din cei 2 ocupanţi</strong> ai unei camere duble trebuie să aibă vârsta <strong>minimă de 55 ani</strong>.</p>
          <p class="text-justify">Există posibilitatea ca la unele oferte să achiziţionaţi <em>&quot;Senior Voyage Club Card&quot;</em>, astfel beneficiind de anumite extra oferte.</p>
        </div>
        
      </div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=<?php echo $id_tip_sejur; ?>");
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>