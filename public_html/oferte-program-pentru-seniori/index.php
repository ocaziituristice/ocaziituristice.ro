<?php //error_reporting(E_ALL);
//ini_set('display_errors', 1);?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip = "program pentru seniori";
$linktip = "program-pentru-seniori";
$dentip = "Seniori";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri'];
$link_new = '/oferte-program-pentru-seniori/';

$sel_dest = "SELECT

COUNT(oferte.id_oferta) AS numar,
zone.denumire as denumire_zona,
zone.descriere_scurta as descriere_scurta_zona,
zone.id_zona as id_zona,
tari.denumire as denumire_tara,
tari.id_tara,
tari.descriere_scurta
FROM oferte
INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
 and id_tip_oferta='66' and zone.id_zona!='1497'
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY zone.id_zona
ORDER BY numar DESC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['den_tara'][] = $row_dest['denumire_tara'];
	$destinatie['numar'][] = $row_dest['numar'];
	$destinatie['link'][] = '/oferte-program-pentru-seniori/'.fa_link($row_dest['denumire_tara']).'/'.fa_link($row_dest['denumire_zona']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$row_dest['id_tara'].'/';
	$destinatie['id_tara'][] = $row_dest['id_tara'];
	$destinatie['info_scurte'][] = nl2br($row_dest['descriere_scurta_zona']);
	//$destinatie['info_scurte'][] = "Seniori ".$row_dest['denumire_zona']." -".$row_dest['denumire_tara'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
} @mysql_free_result($rez_dest);

?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte pachete pentru seniori <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Oferte pachete seniori in tara si strainatate, oferta completa de la toti touroperatorii" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/header/header_responsive.php"); ?>
</head>
<body>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header/admin_bar_responsive.php'); ?>

 <header>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/breadcrumb_responsive_intreg.php"); ?>
    </div>
<script src="<?php echo PATH_JS?>/jquery-ui.min.js"></script>

<div class="layout">

    <div id="middle">

        <div id="middleInner">

            <div class="NEW-column-full">

                <div id="NEW-destinatie" class="clearfix">

                    <h1 class="blue" style="float:left;">Oferte Seniori - pachete Seniori Voyage 2018 </h1>

                    <div class="float-right">

                        <?php /*?><div class="fb-share-button" data-href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/;src=sdkpreparse">Distribuie</a></div>
<?php */?>
                    </div>

                    <br class="clear" />



                        <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">

                                <div class="NEW-search-wide">

                                    <div class="chapter-title blue">Caută Oferte Seniori:</div>

                                    <div id="afis_filtru"></div>

                                </div>

                            </div></div>

                        <br class="clear"><br>

   <?php if(sizeof($destinatie['den_zona'])>0) { ?>
 <div id="destinations"  class="clearfix">
 
               <?php foreach($destinatie['den_zona'] as $key1 => $value1) { ?>

                        <div class="destItem NEW-round4px clearfix" >

                            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">

                                <img src="/images/zone/<?php echo fa_link($destinatie['den_zona'][$key1]); ?>.jpg" alt="Seniori <?php echo $destinatie['den_zona'][$key1]; ?>">

                                <span class="over"></span>

                                <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>

                                <h2>Seniori <?php echo $destinatie['den_zona'][$key1]; ?></h2>

                            </div>

                            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><div class="bottomBox clearfix">

                                <div class="price" align="center"><span class="red"><span class="pret"><?php echo $destinatie['numar'][$key1]?></span> </span> Oferte Seniori <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte Seniori <?php echo $destinatie['den_zona'][$key1].' - '.$destinatie['den_tara'][$key1]; ?>  ">

			 <?php echo $destinatie['den_zona'][$key1].' - '.$destinatie['den_tara'][$key1]; ?></a></span></span></div>

                            </div>

                        </div>
<?php }?>    

                    </div>

<?php }?>

                    <br class="clear"><br>

                    <div class="pad10 newText">

                        <img src="images/icon-programe-seniori.jpg" alt="Oferte Seniori" class="float-left" style="margin:3px 10px 0 0;" />

                        <h2>Programe pentru Seniori în Europa</h2>

                        <p class="text-justify">Sejururi atractive, la preţuri pentru pensionari, pentru relaxare după o viaţă de muncă. Seniorul poate fi însoţit de întreaga familie de juniori, la aceleaşi preţuri. Cele mai numeroase şi deosebit de bine organizate sunt ofertele pentru <a href="/oferte-program-pentru-seniori/spania/" title="Oferte Programe Seniori Spania" class="link-black"><strong>Spania</strong></a> (Mallorca, Costa del Sol, Costa Brava) si <a href="/oferte-program-pentru-seniori/cipru/" title="Oferte Programe Seniori Cipru" class="link-black"><strong>Cipru</strong></a> (Limassol, Larnaca). Sfatul nostru este: invitaţi un senior din familie şi însoţiţi-l într-o astfel de călătorie. Ofertele pentru programele turistice pentru seniori, au început să se răspândească în întreaga lume! Beneficiază acum de ofertele de seniori pentru Israel, şi vizitează locurile sfinte.</p>



                        <br class="clear">



                        <h3 class="blue pad0 mar0">Informaţii şi condiţii generale pentru Programele pentru Seniori</h3>

                        <br>

                        <p class="text-justify"><strong>Programul pentru Seniori</strong> se adresează cetăţenilor europeni de <strong>peste 55 ani</strong> (şi nu numai) prin care pot beneficia de un pachet complet de servicii turistice pentru a îşi petrece vacanţa cu <strong>reduceri substanţiale</strong> în perioada octombrie-mai, putând vizita <em>cele mai cunoscute locuri turistice şi istorice</em>.</p>

                        <p class="text-justify">Prin această iniţiativă, se oferă tuturor posibilitatea de a descoperi lumea şi frumuseţile ei, aceste tipuri de programe încadrându-se în <em>&quot;turism social&quot;</em>.</p>

                        <p class="text-justify">În ultima vreme s-au mai adus modificări asupra condiţiilor acestor programe, astfel că, pentru a putea beneficia de aceste super reduceri, <strong>cel puţin unul din cei 2 ocupanţi</strong> ai unei camere duble trebuie să aibă vârsta <strong>minimă de 55 ani</strong>.</p>

                        <p class="text-justify">Există posibilitatea ca la unele oferte să achiziţionaţi <em>&quot;Senior Voyage Club Card&quot;</em>, astfel beneficiind de anumite extra oferte.</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    $("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=66");

</script>



<script src="/js/ajaxpage.js"></script>

<script src="/js/jquery.rating.js"></script>

<script>

    $('input.rating').rating({

        callback: function(value) {

            var dataString = 'rating=' + value;

            $.ajax({

                type: "POST",

                url: "/voting/////",

                data:dataString,

                success: function(data) {

                    $('#rating').html(data);

                }

            })

        },required:'hidden'

    });

</script>



<script src="/js/ocazii_main.js"></script>

<script>document.oncopy = addLink;</script>











<script src="/js/jquery.masonry.min.js"></script>

<script>

    $(window).load(function(){

        $('#destinations').masonry({

            itemSelector: '.destItem'

        });

    });

</script>













<script>

    $(window).bind("load", function() {

        $.getScript('/js/ocazii_lateload.js', function() {});

    });

</script>

<!-- Google Code for Remarketing Tag -->

<!--------------------------------------------------

Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup

--------------------------------------------------->









<script type="text/javascript">


    $(document).ready(function() {

// Tooltip only Text

        $('.masterTooltip').hover(function(){

            // Hover over code

            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<p class="tooltip"></p>')

                .text(title)

                .appendTo('body')

                .fadeIn('slow');

        }, function() {

            // Hover out code

            $(this).attr('title', $(this).data('tipText'));

            $('.tooltip').remove();

        }).mousemove(function(e) {

            var mousex = e.pageX + 20; //Get X coordinates

            var mousey = e.pageY + 10; //Get Y coordinates

            $('.tooltip')

                .css({ top: mousey, left: mousex })

        });



    });

    const mq = window.matchMedia( "(max-width: 768px)" );



    if(mq){



        $('.topBox').each(function(){

            // Cache event

            var existing_event = this.onclick;



            // Remove the event from the link

            this.onclick = null;



            // Add a check in for the class disabled

//            $(this).click(function(e){

//                $(this).addClass('active_img');

//

//                if($(this).hasClass('active_img')){

//                    e.stopImmediatePropagation();

//                    e.preventDefault();

//                }

//

//            });



            // Reattach your original onclick, but now in the correct order

            // if it was set in the first place

            if(existing_event) $(this).click(existing_event);

        });

    }





</script>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php"); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php"); ?>

