<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Early Booking, reduceri, discounturi la oferte</title>
<meta name="description" content="Early Booking, reduceri substantiale la oferte prin programul de discounturi oferte early booking" />
<meta name="keywords" content="early booking, oferte early booking, reduceri oferte, discounturi oferte" />

<?php $link="http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI']; ?>
<meta name="Publisher" content="ocaziituristice.ro" />
<meta name="ROBOTS" content="INDEX,FOLLOW" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Early Booking - <span class="green">rezerva din timp vacanta ta!</span></h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear" />
        <div class="Hline"></div>
        
        <div style="float:left; width:490px; text-align:justify;">
          <div class="article bigger-11em pad10">
            <p>Rezervarea unui pachet turistic prin early booking, reprezinta un avantaj deoarece turistii au posibilitatea de a alege din timp atat destinatia dorita, cat si data plecarii in vacanta.</p>
            <p>Acest program se aplica pentru sejururi, circuite sau croziere.</p>
            <p>Tarifele acestor pachete early booking sunt foarte atractive. <strong class="black">Ofertele Early booking</strong> sunt oferte care iti dau posibilitatea de a-ti achizitiona o vacanta cu reduceri foarte mari. Aceste oferte sunt adresate celor care vor sa aiba o vacanta la cel mai avantajos pret, dar in anumite conditii.</p>
          </div>
          <br />
          <a href="/sejur-romania/?optiuni=da&early-booking=da" title="Early Booking Romania" class="cb-item" style="background:url(images/early-booking-litoral-romania.jpg) left top no-repeat;"><span class="titlu">Romania <img src="/thumb_steag_tara/4593romania1steag_tara.jpg" alt="steag Romania" /></span></a>
          <a href="/sejur-bulgaria/?optiuni=da&early-booking=da" title="Early Booking Bulgaria" class="cb-item" style="background:url(images/early-booking-litoral-bulgaria.jpg) left top no-repeat;"><span class="titlu">Bulgaria <img src="/thumb_steag_tara/1090bulgaria1steag_tara.jpg" alt="steag Bulgaria" /></span></a>
          <a href="/sejur-grecia/?optiuni=da&early-booking=da" title="Early Booking Grecia" class="cb-item" style="background:url(images/early-booking-litoral-grecia.jpg) left top no-repeat;"><span class="titlu">Grecia <img src="/thumb_steag_tara/5304grecia1steag_tara.jpg" alt="steag Grecia" /></span></a>
        </div>
        
        <div style="float:right; width:484px;">
          <img src="images/img-early-booking.gif" alt="Oferte Early Booking" /><br /><br /><br />

		<?php
        $oferte_rec=new AFISARE_SEJUR_NORMAL();
        $oferte_rec->setAfisare(1, 6);
        $oferte_rec->setRecomandata('da');
        $oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_early_booking.tpl");
        $oferte_rec->config_paginare('nou');
        $oferte_rec->setEarly('da');
        if($link_tara) $oferte_rec->setTari($link_tara);
        if($link_zona) $oferte_rec->setZone($link_zona);
        if($link_oras) $oferte_rec->setOrase($link_oras);
        if($id_tip) $oferte_rec->setTipOferta($_REQUEST['tip']);
        if($_REQUEST['ordonare']) {
            $tipO=explode('-',$_REQUEST['ordonare']);
            if($tipO[0]=='tip_pret') $oferte_rec->setOrdonarePret($tipO[1]);
                elseif($tipO[0]=='tip_numH') $oferte_rec->setOrdonareNumeH($tipO[1]);
        }
        if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $oferte_rec->setTransport($_REQUEST['transport']);
        if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') $oferte_rec->setStele($_REQUEST['stele']);
        if($_REQUEST['concept'] && $_REQUEST['concept']<>'toate') $oferte_rec->setConcept($_REQUEST['concept']);
        if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') $oferte_rec->setMasa($_REQUEST['masa']);
        if($id_loc_plecare_av) $oferte_rec->setPlecareAvion($id_loc_plecare_av);
        if($din_luna) $oferte_rec->setLunaPlecare($din_luna);
        if(sizeof($ds)==3) $oferte_rec->setDistanta($ds);
        $oferte_rec->initializeaza_pagini($link_p, 'pag-###/', $link);
        $nr_hoteluri=$oferte_rec->numar_oferte();
        
        if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
        ?>
          <div class="ofRec" style="background:none; margin:0; padding:0; border:none;">
          <h2>Oferte recomandate pentru Early Booking</h2>
          <?php $oferte_rec->afiseaza();
          if($nr_hoteluri>0) {
              $filtruOf=$oferte_rec->get_filtru_mare();
              $filtruActual=$oferte_rec->get_filtru();
          } ?>
            <br class="clear" />
          </div>
        <?php } ?>
        </div>
        
        <br class="clear" /><br />
        
        <div class="article bigger-11em pad10" align="justify">
          <p><strong class="black">Conditii obligatorii</strong> de comercializare a ofertei speciale - <strong class="black">Inscriere Timpurie</strong> - sau - <strong class="black">Early Booking</strong>:</p>
          <ul>
            <li>Rezervarile facute in perioada <em class="black">Early booking</em> nu permit modificari de nume, perioada sau anulari. Orice modificari aduse rezervarii initiale, duc la recalcularea pretului si implicit la pierderea acestei facilitati;</li>
            <li>Plata se va face <strong class="black">integral</strong> in 5 zile lucratoare de la primirea confirmarii;</li>
            <li>Reducerile se aplica doar la <strong class="black">pachetul de baza</strong> (tarif de cazare). Acestea nu se aplica la taxe sau la suplimente. Reducerile Early booking nu sunt valabile in cazul ofertelor speciale si nu se aplica nici copiilor care beneficiaza de gratuitate la cazare;</li>
            <li>Reducerile <strong class="black">nu se cumuleaza</strong> si <strong class="black">nu sunt transmisibile</strong>. In urma rezervarii si contractarii serviciilor turistice, aparitia unei oferte speciale nu modifica tariful deja stabilit;</li>
            <li>Fiecare destinatie beneficiaza de conditii diferite de early booking; pentru detalii, nu ezitati sa consultati ofertele noastre;</li>
            <li><strong class="red"><u>Anularea</u></strong> rezervarii pentru oferte marcate cu  "Inscrieri Timpurii" sau "Early Booking" se va solda cu <strong class="black">penalizare de 100%</strong> din valoarea sejurului, circuitului sau croazierei, fiind o conditie obligatorie de comercializare a acestei oferte, indiferent cu cate zile inainte de plecare faceti aceasta anulare (in cazul ofertelor early booking nu se aplica penalitatile cuprinse in contractul general incheiat intre agentie si turist), sau de motivul anularii (boala, probleme familiale, anularea concediului de odihna etc).</li>
            <li><strong class="black">Orice modificare</strong> a numelui persoanei se va considera o <strong class="black">anulare</strong> a rezervarii initiale si efectuarea unei noi rezervari. Noile rezervari se refac in functie de disponibilitati si se vor plati la tarif intreg, fara reducere.</li>
          </ul>
        </div>
        
      </div>

    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>