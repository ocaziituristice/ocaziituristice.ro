<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
$tip = "program pentru seniori";
$linktip = "program-pentru-seniori";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'"; if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Early Booking, reduceri, discounturi la oferte</title>
<meta name="description" content="Early Booking, reduceri substantiale la oferte prin programul de discounturi oferte early booking" />
<meta name="keywords" content="early booking, oferte early booking, reduceri oferte, discounturi oferte" />

<?php $link="http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI']; ?>
<meta name="Publisher" content="ocaziituristice.ro" />
<meta name="ROBOTS" content="INDEX,FOLLOW" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Early Booking - <span class="green">rezerva din timp vacanta ta!</span></h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="eb">
          <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/info_early_booking.php"); ?>
          
          <div class="right">
            <div class="eb-sej NEW-round8px">
              <h2>Early Booking Sejururi</h2>
              <div class="links">
                <div class="item clearfix"><a href="/sejur-romania/?optiuni=da&early-booking=da" class="link-blue" title="Early Booking Romania"><img src="/thumb_steag_tara/7941romania1steag_tara.jpg" alt="Flag Romania" /> Romania <span class="reducere green">20%</span></a></div>
                <div class="item clearfix"><a href="/sejur-bulgaria/?optiuni=da&early-booking=da" class="link-blue" title="Early Booking Bulgaria"><img src="/thumb_steag_tara/1090bulgaria1steag_tara.jpg" alt="Flag Bulgaria" /> Bulgaria <span class="reducere green">25%</span></a></div>
                <div class="item clearfix"><a href="/sejur-egipt/?optiuni=da&early-booking=da" class="link-blue" title="Early Booking Egipt"><img src="/thumb_steag_tara/4086egipt1steag_tara.jpg" alt="Flag Egipt" /> Egipt <span class="reducere green">15%</span></a></div>
                <div class="item clearfix"><a href="/sejur-emiratele-arabe-unite/dubai/?optiuni=da&early-booking=da" class="link-blue" title="Early Booking Dubai"><img src="/thumb_steag_tara/5856emiratele_arabe_unite1steag_tara.jpg" alt="Flag Dubai" /> Dubai <span class="reducere green">10%</span></a></div>
              </div>
              <a href="/sejur-romania/valea-prahovei/?optiuni=da&early-booking=da" title="Early Booking Valea Prahovei"><img src="images/banner-eb_valea-prahovei.jpg" alt="Early Booking Valea Prahovei" class="NEW-round6px" style="margin:2px 2px 6px 2px;" /></a>
              <a href="/oferte-revelion/" title="Revelion 2013"><img src="images/banner-eb_revelion-2013.jpg" alt="Revelion 2013" class="NEW-round6px" style="margin:2px 2px 6px 2px;" /></a>
              <a href="/sejur-bulgaria/munte/?optiuni=da&early-booking=da" title="Early Booking Bulgaria"><img src="images/banner-eb_bulgaria-munte.jpg" alt="Early Booking Bulgaria" class="NEW-round6px" style="margin:2px 2px 6px 2px;" /></a>
              <a href="/oferte-craciun/" title="Craciun 2012"><img src="images/banner-eb_craciun-2012.jpg" alt="Craciun 2012" class="NEW-round6px" style="margin:2px 2px 6px 2px;" /></a>
			  <?php /*?><a href="/sejur-turcia/antalya/?optiuni=da&early-booking=da" title="Early Booking Antalya"><img src="images/bannere_eb_sej_antalya.jpg" alt="Early Booking Antalya" class="NEW-round6px" style="margin:2px 2px 6px 2px;" /></a>
              <a href="/sejur-turcia/kusadasi/?optiuni=da&early-booking=da" title="Early Booking Kusadasi"><img src="images/bannere_eb_sej_kusadasi.jpg" alt="Early Booking Kusadasi" class="NEW-round6px" style="margin:2px 2px 6px 2px;" /></a>
              <a href="/sejur-turcia/bodrum/?optiuni=da&early-booking=da" title="Early Booking Bodrum"><img src="images/bannere_eb_sej_bodrum.jpg" alt="Early Booking Bodrum" class="NEW-round6px" style="margin:2px 2px 8px 2px;" /></a><?php */?>
            </div>
            
            <div class="eb-cir NEW-round8px">
              <h2><a href="/circuite/europa/" class="link-black" title="<?php /*?>Early Booking<?php */?> Circuite Europa"><?php /*?>Early Booking<?php */?> <span class="red">Circuite Europa</span></a></h2>
              <div class="continent">
                <img src="images/worldmap_europa.jpg" alt="Europa" />
                <div class="links clearfix">
                  <a href="/circuite/europa/austria/" class="item link-blue" title="Circuit Austria">Circuit Austria</a>
                  <a href="/circuite/europa/belgia/" class="item link-blue" title="Circuit Belgia">Circuit Belgia</a>
                  <a href="/circuite/europa/croatia/" class="item link-blue" title="Circuit Croatia">Circuit Croatia</a>
                  <a href="/circuite/europa/franta/" class="item link-blue" title="Circuit Franta">Circuit Franta</a>
                  <a href="/circuite/europa/grecia/" class="item link-blue" title="Circuit Grecia">Circuit Grecia</a>
                  <a href="/circuite/europa/italia/" class="item link-blue" title="Circuit Italia">Circuit Italia</a>
                  <a href="/circuite/europa/monaco/" class="item link-blue" title="Circuit Monaco">Circuit Monaco</a>
                  <a href="/circuite/europa/olanda/" class="item link-blue" title="Circuit Olanda">Circuit Olanda</a>
                  <a href="/circuite/europa/portugalia/" class="item link-blue" title="Circuit Portugalia">Circuit Portugalia</a>
                  <a href="/circuite/europa/spania/" class="item link-blue" title="Circuit Spania">Circuit Spania</a>
                  <a href="/circuite/europa/turcia/" class="item link-blue" title="Circuit Turcia">Circuit Turcia</a>
                  <a href="/circuite/europa/vatican/" class="item link-blue" title="Circuit Vatican">Circuit Vatican</a>
                </div>
              </div>
              <h2><a href="/circuite/africa/" class="link-black" title="<?php /*?>Early Booking<?php */?> Circuite Africa"><?php /*?>Early Booking<?php */?> <span class="red">Circuite Africa</span></a></h2>
              <div class="continent">
                <img src="images/worldmap_africa.jpg" alt="Africa" />
                <div class="links clearfix">
                  <a href="/circuite/africa/egipt/" class="item link-blue" title="Circuit Egipt">Circuit Egipt</a>
                  <a href="/circuite/africa/maroc/" class="item link-blue" title="Circuit Maroc">Circuit Maroc</a>
                  <a href="/circuite/africa/tunisia/" class="item link-blue" title="Circuit Tunisia">Circuit Tunisia</a>
                </div>
              </div>
              <h2><a href="/circuite/america/" class="link-black" title="<?php /*?>Early Booking<?php */?> Circuite America"><?php /*?>Early Booking<?php */?> <span class="red">Circuite America</span></a></h2>
              <div class="continent">
                <img src="images/worldmap_america.jpg" alt="America" />
                <div class="links clearfix">
                  <a href="/circuite/america/argentina/" class="item link-blue" title="Circuit Argentina">Circuit Argentina</a>
                  <a href="/circuite/america/brazilia/" class="item link-blue" title="Circuit Brazilia">Circuit Brazilia</a>
                  <a href="/circuite/america/chile/" class="item link-blue" title="Circuit Chile">Circuit Chile</a>
                  <a href="/circuite/america/paraguay/" class="item link-blue" title="Circuit Paraguay">Circuit Paraguay</a>
                  <a href="/circuite/america/uruguay/" class="item link-blue" title="Circuit Uruguay">Circuit Uruguay</a>
                </div>
              </div>
              <h2><a href="/circuite/asia/" class="link-black" title="<?php /*?>Early Booking<?php */?> Circuite Asia"><?php /*?>Early Booking<?php */?> <span class="red">Circuite Asia</span></a></h2>
              <div class="continent">
                <img src="images/worldmap_asia.jpg" alt="Asia" />
                <div class="links clearfix">
                  <a href="/circuite/asia/china/" class="item link-blue" title="Circuit China">Circuit China</a>
                  <a href="/circuite/asia/filipine/" class="item link-blue" title="Circuit Filipine">Circuit Filipine</a>
                  <a href="/circuite/asia/indonezia/" class="item link-blue" title="Circuit Indonezia">Circuit Indonezia</a>
                  <a href="/circuite/asia/iordania/" class="item link-blue" title="Circuit Iordania">Circuit Iordania</a>
                  <a href="/circuite/asia/israel/" class="item link-blue" title="Circuit Israel">Circuit Israel</a>
                  <a href="/circuite/asia/malaezia/" class="item link-blue" title="Circuit Malaezia">Circuit Malaezia</a>
                  <a href="/circuite/asia/singapore/" class="item link-blue" title="Circuit Singapore">Circuit Singapore</a>
                </div>
              </div>
            </div>
            
            <br class="clear" />
          </div>
          
          <br class="clear" />
        </div>
        
        <br class="clear" /><br />
        
      </div>

    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>