<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
$meta_index = "noindex,follow";
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Despre noi - <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Despre <?php echo $denumire_agentie; ?>, istoric, poveste, modalitati contact" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <h1 class="blue">Despre noi</h1>
        
        <div class="Hline"></div>
        
        <div class="text-justify pad10 article clearfix">
        
		  <div class="float-left w700">
			<p class="bigger-12em">Suntem mai mult decât o simplă Agenţie de Turism, suntem un portal destinat turiştilor care doresc să călătorească oriunde în lume şi să işi planifice vacanţele repede, fără să caute zile întregi pe zeci de site-uri. Am ţinut pasul cu tehnologia şi am reuşit să oferim vizitatorilor Ocaziituristice.ro pe loc preţul final al unei vacanţe (cu toate taxele incluse) şi disponibilitatea exactă în momentul în care se caută. Suntem pregătiţi pentru cele mai exigente cerinţe ale turiştilor, astfel, o rezervare se poate face direct online sau se poate apela la ajutorul nostru specializat oferit prin intermediul Call Center-ului nostru.</p>
            <p class="bigger-12em">Vă aştetăm cu drag să faceţi parte din familia <strong>Ocaziituristice.ro</strong> !</p>
            <a href="/files/cui.jpg" target="_blank" class="block mar5 float-left" title="Certificat Unic de Înregistrare"><img src="/files/icon_small_cui.jpg" alt="Certificat Unic de Inregistrare <?php echo $denumire_agentie; ?>"></a>
            <a href="/files/licenta_turism.jpg" target="_blank" class="block mar5 float-left" title="Licenţă de Turism"><img src="/files/icon_small_licenta_turism.jpg" alt="Licenta de Turism <?php echo $denumire_agentie; ?>"></a>
            <a href="/files/brevet_turism.jpg" target="_blank" class="block mar5 float-left" title="Brevet de Turism"><img src="/files/icon_small_brevet_turism.jpg" alt="Brevet de Turism <?php echo $denumire_agentie; ?>"></a>
            <a href="/files/asigurare.jpg" target="_blank" class="block mar5 float-left" title="Asigurare de Insolvenţă"><img src="/files/icon_small_asigurare.jpg" alt="Asigurare <?php echo $denumire_agentie; ?>"></a>
            <a href="/files/protectia_datelor.jpg" target="_blank" class="block mar5 float-left" title="Protecţia Datelor Personale"><img src="/files/icon_small_protectia_datelor.jpg" alt="Protectia Datelor <?php echo $denumire_agentie; ?>"></a>
          </div>
          
          <div class="float-right w200">
            <h3 class="black" style="margin-bottom:10px;">Suntem prezenţi şi pe</h3>
            <a href="http://www.facebook.com/OcaziiTuristice.ro" target="_blank"><img src="/images/label_facebook.jpg" alt="Facebook <?php echo $denumire_agentie; ?>"></a>
            <a href="https://plus.google.com/100372161073365189899?rel=author" target="_blank"><img src="/images/label_google_plus.jpg" alt="Google+ <?php echo $denumire_agentie; ?>"></a>
            <a href="https://twitter.com/ocaziituristice" target="_blank"><img src="/images/label_twitter.jpg" alt="Twitter <?php echo $denumire_agentie; ?>"></a>
            <a href="http://pinterest.com/ocaziituristice/" target="_blank"><img src="/images/label_pinterest.jpg" alt="Pinterest <?php echo $denumire_agentie; ?>"></a>
            <a href="http://blog.ocaziituristice.ro/" target="_blank"><img src="/images/label_blog.jpg" alt="Blog <?php echo $denumire_agentie; ?>"></a>
          </div>
                  
        </div>
        
        <br><br>
        
        <div class="article clearfix bigger-15em">
          <div class="w600 bigger-12em text-center pad20" style="margin-left:170px;">
            Povestea <strong><?php echo $denumire_agentie; ?></strong> a început cu multi ani in urma si am progresat în continuu alături de turiştii şi vizitatorii noştri fideli, încercând în permanenţă să adăugăm facilităţi noi.
          </div>
        </div>
        
        <br><br>
       

       
        
        <div id="timeline" class="clearfix">
          
           <div class="item float-right green" style="margin-top:0px;">
            <div class="text float-right" ><span class="bold underline bigger-13em">Am depasit 10 0000 de turisti la 5 ani de activitate:</span><br>
            
            - peste <span class="bold bigger-11em">30 de Tour Operatori</span> cu care colaborăm<br>
           
            </div>
            <div class="year year-left">2016</div>
          </div>
          
          
          
          
          <div class="item float-left blue" style="margin-top:60px;">
            <div class="text float-left"><span class="bold bigger-13em">Integrarea tuturor operatorilot din Romania</span> Preturile apar pe site in urma compararii live la Tour-Operatorii Romani <?php echo $denumire_agentie; ?></div>
            <div class="year year-right">2014</div>
          </div>
          
          <div class="item float-right green" style="margin-top:80px;">
            <div class="text float-right" ><span class="bold underline bigger-13em">Să vorbim în cifre:</span><br>
            - peste <span class="bold bigger-11em">1200 rezervări</span><br>
            - peste <span class="bold bigger-11em">1.000.000 de persoane</span> ne-au vizitat<br>
            - peste <span class="bold bigger-11em">30 de Tour Operatori</span> cu care colaborăm<br>
            - am majorat capitalul social la <span class="bold bigger-11em">50.000 Lei</span>
            </div>
            <div class="year year-left">2013</div>
          </div>
          
          <div class="item float-left red" style="margin-top:120px;">
            <div class="text float-left"><span class="bold underline">Proiectul avansează împreună cu tehnologia:</span><br>
            - implementare <span class="bold bigger-11em">XML-uri</span> de la Tour Operatori<br>
            - <span class="bold bigger-11em">răspuns în timp real</span> a tarifelor înainte de rezervare<br>
            - tot procesul de rezervare se poate desfăşura <span class="bold bigger-11em">ONLINE</span>, clientul nemaifiind nevoit să aştepte răspuns de la agenţie</div>
            <div class="year year-right">2012</div>
          </div>
          
          <div class="item float-right blue" style="margin-top:70px;">
            <div class="text float-right"><span class="bold underline bigger-13em">11 Octombrie</span><br>
            <?php echo $denumire_agentie; ?> devine un jucător important pe piaţa turismului din România, făcând parte din agenţia <strong>DREAM VOYAGE</strong></div>
            <div class="year year-left">2011</div>
          </div>
          
         
          
 
          
          
          

          
        </div>
        
        <br><br><br><br>
        
        <div class="pad20">
          <h2 class="text-center underline"><span class="bigger-15em">Ce spune lumea despre noi?</span></h2>
          <div class="clearfix pad20">
            <a href="https://www.facebook.com/OcaziiTuristice.ro#u_0_4d" target="_blank" onClick="ga('send', 'event', 'pagina despre noi', 'click', 'facebook reviews');"><img src="/images/facebook_reviews.jpg" alt="Facebook Reviews" class="float-left mar0-10"></a>
            <div class="float-right w420 mar0-10">
              <h3 class="blue bigger-15em"><span class="bigger-15em">Testimoniale</span></h3>
              <div class="short-testimoniale">
              <?php
              $sel_testimoniale="SELECT * FROM testimoniale ORDER BY rand() LIMIT 0,5";
              $que_testimoniale=mysql_query($sel_testimoniale) or die(mysql_error());
              while($row_testimoniale=mysql_fetch_array($que_testimoniale)) {
              ?>
                <div class="grey italic bold"><?php echo $row_testimoniale['nume'].' ('.$row_testimoniale['tip_turist'].')'; ?></div>
                <div class="content black">
                <?php echo truncate_str($row_testimoniale['comentariu'], 220); ?>..."
                </div>
                <hr>
              <?php } ?>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>