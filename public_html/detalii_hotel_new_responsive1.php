<?php ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
//error_reporting(E_ALL);?>

<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );

include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$nume_hotel = desfa_link( $_REQUEST['nume'] );
//echo $nume_hotel = strtr($nume_hotel,'_w_','and');
//desfa_link($_REQUEST['nume'])
$id_hotel = get_idhotel( $nume_hotel, desfa_link( $_REQUEST['localitate'] ) );
//echo $_REQUEST['localitate'];
$id_hotel      = 4247;
$det           = new DETALII_HOTEL();
$detalii_hotel = $det->select_camp_hotel( $id_hotel );
$det->hotel_vizitat( $id_hotel );
$tip_pagina = 'offerdetail';

$offers    = $det->get_oferte( $id_hotel );
$nr_oferte = count( $offers );

$id_oferta           = '';
$den_hotel           = ucwords( $detalii_hotel['denumire'] );
$den_tara            = ucwords( $detalii_hotel['tara'] );
$den_zona            = ucwords( $detalii_hotel['zona'] );
$den_localitate      = ucwords( $detalii_hotel['localitate'] );
$hoteluri_localitate = hoteluri_localitate( $detalii_hotel['id_localitate'], $id_hotel, $detalii_hotel['latitudine'], $detalii_hotel['longitudine'] );

/*if($_REQUEST['localitate']<>fa_link_oferta($detalii_hotel['localitate']) || !$detalii_hotel['denumire'] || $detalii_hotel['tip_unitate']=='Circuit') {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}*/

if ( $detalii_hotel['titlu_seo'] == "" ) {
	$metas_title = $detalii_hotel['denumire'] . ' din ' . $detalii_hotel['localitate'] . ', ' . $detalii_hotel['tara'];
} else {
	$metas_title = $detalii_hotel['titlu_seo'];
}
if ( $detalii_hotel['descriere_seo'] == "" ) {
	$metas_description = $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate'] . ' ' . $detalii_hotel['tara'] . ' - ' . $detalii_hotel['stele'] . ' stele . Oferte si sejururi de cazare la ' . $detalii_hotel['denumire'] . ' din ' . $detalii_hotel['zona'] . ', ' . $detalii_hotel['tara'];
} else {
	$metas_description = $detalii_hotel['descriere_seo'];
}

include( $_SERVER['DOCUMENT_ROOT'] . '/includes/hoteluri/reviews_top.php' ); ?>


<?php require_once( "includes/header/header_responsive.php" ); ?>
    <script src="/js/jquery_validate/jquery.validationEngine-ro.js"></script>
    <script src="/js/jquery_validate/jquery.validationEngine.js"></script>
    </head>
    <body>
	<?php
	//echo "<pre>";print_r($hoteluri_localitate);echo "</pre>";
	?>
    <header>
		<?php require( "includes/header/meniu_header_responsive.php" ); ?>
    </header>
    <div class="layout">
		<?php require( "includes/header/breadcrumb_responsive.php" ); ?>
		<?php require( "includes/header/search_responsive.php" ); ?>
    </div>
    <script>
        function myFunction(x) {
            x.classList.toggle("change");
        }
    </script>


    <div id="hotel_descriptions"></div>
    <div class="layout">

        <section class="hotel">

            <div class="clear">

                <div class="left-column">

                    <h1><?php echo $detalii_hotel['denumire'] ?> <span class="stars">
                    
           <?php for ( $stea = 1; $stea <= $detalii_hotel['stele']; $stea ++ ) {
	           $stars .= '<i class="icon-star"></i>';
           }
           echo $stars; ?>
                    </span></h1>


                    <p class="address" data-scroll="map"><i
                                class="icon-location"></i> <?php echo $detalii_hotel['adresa'] . ' ';
						if ( $detalii_hotel['nr'] ) {
							echo 'nr. ' . $detalii_hotel['nr'] . ', ';
						}
						echo $detalii_hotel['localitate'] . ', ';
						echo $detalii_hotel['tara'];
						if ( $detalii_hotel['distanta'] ) {
							echo ', <strong>' . $detalii_hotel['distanta'] . 'm de ' . $detalii_hotel['distanta_fata_de'] . '</strong>, ';
						}
						if ( $detalii_hotel['latitudine'] && $detalii_hotel['longitudine'] && $detalii_hotel['latitudine'] <> 0 && $detalii_hotel['longitudine'] <> 0 ) { ?>
                            <!--(<a href="#harta" id="button-harta" class="link"><strong>Arată harta</strong></a>)-->
						<?php } ?></p>

                </div>

                <div class="right-column">

                    <div class="recommended">

                        <i class="icon-thumbs-up-alt"></i>

                        <p>

                            Recomandat pentru:

                            <span>Familii cu copii</span>

                        </p>

                    </div>

                </div>

            </div>
            <div class="clear">

                <div class="left-column images">

                    <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-fit="cover"
                         data-loop="true" data-ratio="1.7">

						<?php for ( $i = 1; $i <= 20; $i ++ ) {
							if ( $detalii_hotel[ 'poza' . $i ] ) { ?>

                                <a href="https://www.ocaziituristice.ro/img_prima_hotel/<?php echo $detalii_hotel[ 'poza' . $i ]; ?>"
                                   data-thumb="https://www.ocaziituristice.ro/img_prima_hotel<?php echo $detalii_hotel[ 'poza' . $i ]; ?>"></a>

							<?php }
						} ?>


                    </div>

					<?php if ( ( sizeof( $detalii_online['early_time'] ) > 0 or $detalii_hotel['data_early_booking'] > date( 'Y-m-d' ) ) and $detalii_online['last_minute'] < 1 ) { ?>

						<?php if ( is_array( $detalii_online['early_disc'] ) ) {
							$reduce_eb = max( $detalii_online['early_disc'] );
						}
						if ( $reduce_eb < $detalii_hotel['comision_reducere'] ) {
							$reduce_eb = $detalii_hotel['comision_reducere'];
						}
						if ( $reduce_eb > 1 ) {
							echo '<div class="discount eb"> <strong>Reducere <br />Early Booking <span>' . $reduce_eb . '% </span></strong></div>';
						}
					} ?>
					<?php if ( $detalii['spo_titlu'] ) {
						echo '<div class="oferta-speciala"></div>';
					};
					if ( $detalii_online['last_minute'] > 0 ) {
						echo '<div class="last-minute"></div>';
					} ?>




					<?php //}?>

                </div>

                <div class="trust right-column">

                </div>


            </div>
			<?php include_once( "includes/price_form.php" ) ?>
			<?php include_once( "includes/tabel_preturi_responsive.php" ) ?>


            <h2 class="expand icon">
                Descriere <?php echo $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate']; ?></h2>
            <div id="fill_div_blank" class="expandable description">
                <div class="detHotelNew2 masonry-brick details">

					<?php afisare_frumos( schimba_caractere( $detalii_hotel["descriere_scurta"] ), 'nl2p' );
					afisare_frumos( schimba_caractere( $detalii_hotel["descriere"] ), 'nl2p' ); ?>


                </div>

                <div class="detHotelNew2 masonry-brick facilities">

                    <div>

                        <h3>General</h3>

                        Bar, si rstaurant (à la carte),<br>Saună, sală de fitness, masaj,<br>Teren de joacă pentru
                        copii,<br>Piscină în aer liber (deschisă în sezon),<br>Camere de familie, încălzire, aer
                        condiţionat,<br>Zonă pentru fumători

                    </div>

                    <div>

                        <h3>Servicii</h3>

                        Umbrele si sezlonguri la plaja si piscine; Prosoape plaja - pe baza de tichet primit la cazare;
                        Cosmetice la baie; Parc de joaca pentru copii pe categorii de varsta; Teatru de vara/Cinema;
                        Parcul indragostitilor (foisoare pentru seri romantice); Club animatie copii; Jocuri si
                        concursuri adulti si copii (la plaja, piscine, terasa); Piscine pentru adulti si copii cu
                        valuri/ tobogane (2 piscine cu apa incalzita), piscina interioara inclazita; animatori, muzica
                        live si program pentru copii seara la restaurant Kasttane; petreceri tematice, karaoke,
                        carnaval; Fitness / Aerobic / Sauna; Baza sportiva (tenis/baschet/fotbal/volei); Spalatorie
                        auto; Parcare supravegheata; Spalatorie haine cu autoservire; Televiziune interna proprie;
                        Internet WI-FI; Presa la receptie; Pat bebe la cerere; Business Center.

                    </div>

                </div>

                <div class="detHotelNew2 masonry-brick facilities-extra">

					<span>

						<h3>Internet</h3>

						Da, Internet wireless este disponibil în întregul hotel şi este gratuit.

					</span>

                    <span>

						<h3>Parcare</h3>

						Da, parcarea privată gratuit

					</span>

                    <span>

						<h3>Check-in / check-out</h3>

						incepind cu orele 16 / incepand cu ora 12:00

					</span>

                    <span>

						<h3>Animale companie</h3>

						Nu sunt acceptate

					</span>

                </div>

            </div>


            <h2 class="expand icon">Detalii
                camere <?php echo $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate']; ?></h2>

            <div class="expandable description">

                <ul class="room-details">

                    <li>

                        <h3>Camera dubla</h3>

                        Camera dubla cu pat matimonial, cuprinde un spatiu deschis cu o suprafata de 45 mp, zona pentru
                        dormit cu un pat matrimonial, canapea modulara, extensibila sau fixa, baie cu cada si balcon.

                    </li>

                    <li>

                        <h3>Family Suite de lux</h3>

                        Camerele co municante, este formata din doua camera duble, una matimoniala si una twin, insumand
                        un spatiu generos de 90 mp, cu doua bai cu cada.

                    </li>

                    <li>

                        <h3>Apartament cu 1 dormitor</h3>

                        Cuprinde un spatiu generos de 71 mp, format dintr-un living si un dormitor cu pat matrimonial, 2
                        bai, una de serviciu si una cu cada, balcon, cu vedere frontala fie spre mare, fie spre lacul
                        Siutghiol.

                    </li>

                </ul>

            </div>


            <h2 class="expand icon">Localizare pe
                harta <?php echo $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate']; ?></h2>

            <div class="map">

                <div class="expandable" id="map"></div>

                <div class="nearby">

                    <h3>Hoteluri din <?php echo $detalii_hotel['localitate'] ?> in apropiere</h3>


					<?php $hoteluri_localitate_1 = multid_sort( $hoteluri_localitate, 'distanta' );
					//echo '<pre>';print_r($hoteluri_localitate_1);echo '</pre>';?>
					<?php if ( sizeof( $hoteluri_localitate_1 ) > 3 ) { ?>


						<?php for ( $id = 1; $id <= 5; $id ++ ) {

							echo "<br /><a href=\"#\">

					<img src=\"" . $sitepath . "thumb_hotel/" . $hoteluri_localitate_1[ $id ]['poza1'] . "\" alt=\"" . $hoteluri_localitate_1[ $id ]['denumire'] . " " . $detalii_hotel['localitate'] . "\">

					<span class=\"title\">" . $hoteluri_localitate_1[ $id ]['denumire'] . "</span>

					<span class=\"stars\">" . afiseaza_stele( $hoteluri_localitate_1[ $id ]['stele'] ) . "</span>

					<span>" . $hoteluri_localitate_1[ $id ]['distanta'] . " metri</span>

				</a>";


						}
					} ?>


                </div>

            </div>
            <!--			<section class="comments"></section>
			-->
            <div class="row">
                <div class="col-sm-12">

                    <div class="heading">
                        <h2>Clienții care au văzut <span
                                    class="text-danger"><?php echo $detalii_hotel['denumire']; ?></span> au fost
                            interesați și de:</h2>
                    </div>

                    <div class="card-deck-wrapper circuit">
                        <div class="card-deck">

                            <div class="card block-view">
                                <div class="card-block">
                                    <h4 class="title">Hotel Olymp</h4>
                                    <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                class="fa fa-star"></i></span>
                                    <label class="description">
                                        <img src="https://www.ocaziituristice.ro/thumb_hotel/4678hotel_olymp1hotel.jpg"
                                             class="thumb" alt="">
                                        Cu toate ca este situat într-o zonă liniștită, hotelul este la doar 1,5 km de
                                        teleschiuri (unde se poate ajunge gratuit cu un autobuz de schi) și la 10 minute
                                        de mers pe jos de zona centrală a orasului. Facilităţile de spa ale...</label>
                                    <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                                </div>
                            </div>

                            <div class="card block-view">
                                <div class="card-block">
                                    <h4 class="title">Hotel Aseva House</h4>
                                    <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                    <label class="description">
                                        <img src="https://www.ocaziituristice.ro/thumb_hotel/7318hotel_aseva_house1hotel.jpg"
                                             class="thumb" alt="">
                                        Hotelul, situat la aproximativ situat la 300 m de gondola si 100 m de centrul
                                        statiunii, oferă .restaurantul principal, taverna cu semineu, internet in lobby,
                                        camera pentru depozitarea echipamentului pentru schi,parcare in
                                        limita...</label>
                                    <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                                </div>
                            </div>

                            <div class="card block-view">
                                <div class="card-block">
                                    <h4 class="title">Hotel Lina</h4>
                                    <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                class="fa fa-star"></i></span>
                                    <label class="description">
                                        <img src="https://www.ocaziituristice.ro/thumb_hotel/9008hotel_lina1hotel.jpg"
                                             class="thumb" alt="">
                                        Hotelul Lina, situat într-o zonă liniştită din Bansko, la 10 minute de centrul
                                        oraşului şi de principala staţie de telegondolă, oferă transport la staţie,
                                        camere spaţioase şi confortabile, cu acces gratuit la internet Wi-Fi.</label>
                                    <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                                </div>
                            </div>

                            <div class="card block-view">
                                <div class="card-block">
                                    <h4 class="title">Hotel Mount View Lodge</h4>
                                    <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                    <label class="description">
                                        <img src="https://www.ocaziituristice.ro/thumb_hotel/6344hotel_mount_view_lodge1hotel.jpg"
                                             class="thumb" alt="">
                                        Hotelul oferă studiouri spațioase de lux și apartamente cu un design interior
                                        deosebit, fiecare dintre acestea fiind dotate cu chicinetă complet echipata si
                                        semineu. Centrul de fitness si spa oferă saună, baie de aburi...</label>
                                    <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <section class="bottom second_mod"></section>


			<?php include_once( "includes/impresii_hotel.php" ) ?>

            <script>

                var locations = [
					<?php foreach ( $hoteluri_localitate_1 as $hoteluri_pe_harta ) {

					if ( $hoteluri_pe_harta['id_hotel'] == $id_hotel ) {
						echo "[\"" . $hoteluri_pe_harta['denumire'] . "- hotelul curent\"," . $hoteluri_pe_harta['latitudine'] . "," . $hoteluri_pe_harta['longitudine'] . ", \"https://www.ocaziituristice.ro/images/icon_maps_hotel.png\"],";


					} else {
						echo "[\"<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-tanne/'>" . $hoteluri_pe_harta['denumire'] . "</a>\"," . $hoteluri_pe_harta['latitudine'] . "," . $hoteluri_pe_harta['longitudine'] . ", \"https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png\"],";
					}
				}
					?>



                ];


                function initGoogleMap() {


                    var infowindow = new google.maps.InfoWindow();

                    var map = new google.maps.Map(document.getElementById('map'), {

                        zoom: 16,

                        center: new google.maps.LatLng(<?php  echo $detalii_hotel['latitudine']?>,   <?php echo $detalii_hotel['longitudine']?>),

                        mapTypeId: google.maps.MapTypeId.ROADMAP

                    });


                    function placeMarker(loc) {

                        var latLng = new google.maps.LatLng(loc[1], loc[2]);

                        var markerIcon = loc[3];

                        var marker = new google.maps.Marker({

                            position: latLng,

                            map: map,

                            icon: markerIcon

                        });

                        google.maps.event.addListener(marker, 'click', function () {

                            infowindow.close();

                            infowindow.setContent("<div id='infowindow'>" + loc[0] + "</div>");

                            infowindow.open(map, marker);

                        });

                    }


                    for (var i = 0; i < locations.length; i++) {

                        placeMarker(locations[i]);

                    }


                }


                if (isDevice) {

                    $("#map").html('<span class="static" style="background-image: url(\'https://maps.googleapis.com/maps/api/staticmap?center=<?php  echo $detalii_hotel['latitudine']?>,<?php  echo $detalii_hotel['longitudine']?>&zoom=16&size=1000x500&maptype=roadmap&markers=icon:https://www.ocaziituristice.ro/images/icon_maps_hotel.png|<?php  echo $detalii_hotel['latitudine']?>,<?php  echo $detalii_hotel['longitudine']?>=<?php echo $google_api_key_static ?>\'"></span>');

                } else {

                    google.maps.event.addDomListener(window, 'load', initGoogleMap);

                }

            </script>

            <script>
                $(function () {

                    $(".address").click(function (e) {

                        e.preventDefault();

                        goToByScroll($(this).data("scroll"), 'h2');

                    });
//					modalOpen('span.static', 'modals/map_hotel.php');

                });


                $(window).load(function () {
                    $(".visited ul li p").dotdotdot();
                });
                $(window).resize(function () {
                    $(".visited ul li p").dotdotdot();
                });
            </script>
            <script>

                //Show Age
                function displayAges(nVal) {
                    if (nVal > 0) {
                        $(".chd-ages").show();
                    } else {
                        $(".chd-ages").hide();
                    }

                    for (var i = 0; i < 3; i++) {
                        if (i < nVal) {
                            $(".varste-copii-" + i).css("display", "inline-block");
                        } else {
                            $(".varste-copii-" + i).css("display", "none");
                            $(".varste-copii-select-" + i).val(0);
                        }
                    }
                }

                //Form Validation
                //        function validateForm11853() {
                //            var date_field11853 = document.getElementById("calc-dataplec11853").value;
                //            if (date_field11853==null || !date_field11853) {
                //                alert("Data plecării trebuie selectată!");
                //                document.getElementById("calc-dataplec11853").focus();
                //                return false;
                //            }
                $('.link_card_mobile').click(function (e) {
                    e.preventDefault();
                    $(this).next('.card_mobile').toggle('slow');
                });


                //Include modal different media
                var mq = window.matchMedia("(min-width: 800px)");
                if (mq.matches) {
                    $(".trust").load("includes/trustme.php");
                } else {
                    $(".second_mod").load("includes/trustme.php");

                }
            </script>
        </section>
        <script src="/js/jquery.rating.js"></script>
        <script>
            $('input.rating').rating({
                callback: function (value) {
                    var dataString = 'rating=' + value;
                    $.ajax({
                        type: "POST",
                        url: "/voting/////",
                        data: dataString,
                        success: function (data) {
                            $('#rating').html(data);
                        }
                    })
                }, required: 'hidden'
            });
        </script>
        <script>
            $('#add-review').click(function () {
                $('#adauga-review').toggle('slow');
                return false;
            });
        </script>
        <script>
            $("#button-comentarii").click(function () {
                $('html, body').animate({
                    scrollTop: $("#comentarii").offset().top - 220
                });
            });
            $("#button-harta").click(function () {
                $('html, body').animate({
                    scrollTop: $("#harta").offset().top - 220
                });
            });
        </script>
        <script>
            jQuery(document).ready(function () {
//                jQuery("#adauga-review").validationEngine('attach', {promptPosition : "topRight"});
            });

            function checkHELLO(field, rules, i, options) {
                if (field.val() != "HELLO") {
                    return options.allrules.validate2fields.alertText;
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $('#fill_div_blank').masonry({
                    itemSelector: '.detHotelNew2'
                });
            });
        </script>
    </div>
	<?php require_once( "includes/newsletter_responsive.php" ); ?>

<?php require_once( "includes/footer/footer_responsive.php" ); ?>