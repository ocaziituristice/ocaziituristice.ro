<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');

$handle = fopen("../sitemaps/sitemap_hoteluri.xml", "w+");

$xml = '<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="'.$sitepath.'sitemaps/xml-sitemap.xsl"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'."\n\n";

$sel_hoteluri="SELECT
  tari.denumire as denumire_tara,
  zone.denumire as denumire_zona,
  localitati.denumire as denumire_localitate,
  hoteluri.nume as denumire_hotel,
  hoteluri.poza1 as poza_hotel,
 oferte.data_adaugarii as data_adaugarii,
oferte.ultima_modificare as ultima_modificare,
oferte.inactiva,
oferte.valabila,
  hoteluri.id_hotel
  FROM
  hoteluri
inner join oferte on hoteluri.id_hotel=oferte.id_hotel
  inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
  inner Join zone ON localitati.id_zona = zone.id_zona
  inner Join tari ON zone.id_tara = tari.id_tara
  left join continente on hoteluri.id_continent = continente.id_continent
  WHERE
oferte.valabila='da' and 
  hoteluri.tip_unitate <> 'Circuit'
  AND hoteluri.poza1 <> ''
  Group by hoteluri.id_hotel
  Order by hoteluri.nume
";
$que_hoteluri=mysql_query($sel_hoteluri) or die(mysql_error());
while($row_hoteluri=mysql_fetch_array($que_hoteluri)) {

$date_add=$row_hoteluri['data_adaugarii'];
$date_mod=$row_hoteluri['ultima_modificare'];

$link_hotel="http://www.ocaziituristice.ro".fa_link_hotel($row_hoteluri['denumire_localitate'], $row_hoteluri['denumire_hotel'], NULL, NULL);
$link_poza=$sitepath.'img_mediu_hotel/'.$row_hoteluri['poza_hotel'];
if(($date_mod!=NULL) and ($date_mod>$date_add)) {
	$ultima_modificare=date('Y-m-d', strtotime($date_mod));
} elseif($date_add!=NULL) {
	$ultima_modificare=date('Y-m-d', strtotime($date_add));
} else {
	$ultima_modificare=date('Y-m-d',strtotime(date('Y-m-d').' - 30 days'));
}

$xml .= '
	<url>
		<loc>'.$link_hotel.'</loc>
		<image:image>
			<image:loc>'.$link_poza.'</image:loc>
		</image:image>
		<lastmod>'.$ultima_modificare.'</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.5</priority>
	</url>
';

}

$xml .= '</urlset>';

if(fwrite($handle, $xml) == FALSE) {
	echo "Cannot write to file";
	exit;
}
fclose($handle); ?>

<p>Sitemap HOTELURI creat. <a href="../sitemaps/sitemap_hoteluri.xml">vezi sitemap</a></p>
