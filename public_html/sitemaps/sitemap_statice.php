<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');

$handle = fopen("../sitemaps/sitemap_statice.xml", "w+");

$xml = '<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="'.$sitepath.'sitemaps/xml-sitemap.xsl"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n\n";

$xml .= '
	<url>
		<loc>'.$sitepath.'</loc>
		<lastmod>'.date('Y-m-d').'</lastmod>
		<changefreq>daily</changefreq>
		<priority>1</priority>
	</url>
';

$xml .= '
	<url>
		<loc>'.$sitepath.'early-booking/</loc>
		<lastmod>'.date('Y-m-d').'</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.9</priority>
	</url>
';

$xml .= '
	<url>
		<loc>'.$sitepath.'last-minute/</loc>
		<lastmod>'.date('Y-m-d').'</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.9</priority>
	</url>
';

$xml .= '
	<url>
		<loc>'.$sitepath.'oferte-speciale/</loc>
		<lastmod>'.date('Y-m-d').'</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.9</priority>
	</url>
';

$xml .= '
	<url>
		<loc>'.$sitepath.'circuite/</loc>
		<lastmod>'.date('Y-m-d').'</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.9</priority>
	</url>
';

$sel_tem = "SELECT
tip_oferta.denumire_tip_oferta
FROM tip_oferta
LEFT JOIN oferta_sejur_tip ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
LEFT JOIN oferte ON oferte.id_oferta = oferta_sejur_tip.id_oferta
left join hoteluri on oferte.id_hotel = hoteluri.id_hotel
WHERE tip_oferta.activ = 'da'
AND oferte.valabila = 'da'
AND tip_oferta.tip <> 'oferte_speciale'
and tip_oferta.apare_site='da'
and tip_oferta.parinte is NULL
And  hoteluri.tip_unitate <> 'Circuit'
GROUP BY tip_oferta.denumire_tip_oferta
ORDER BY tip_oferta.denumire_tip_oferta
";
$que_tem = mysql_query($sel_tem) or die(mysql_error());
while($row_tem = mysql_fetch_array($que_tem)) {

$link_tem = $sitepath.'oferte-'.fa_link_oferta($row_tem['denumire_tip_oferta']).'/';
$date_mod = date('Y-m-d');

$xml .= '
	<url>
		<loc>'.$link_tem.'</loc>
		<lastmod>'.$date_mod.'</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.9</priority>
	</url>
';

}

$xml .= '</urlset>';

if(fwrite($handle, $xml) == FALSE) {
	echo "Cannot write to file";
	exit;
}
fclose($handle); ?>

<p>Sitemap PAGINI STATICE creat. <a href="../sitemaps/sitemap_statice.xml">vezi sitemap</a></p>
