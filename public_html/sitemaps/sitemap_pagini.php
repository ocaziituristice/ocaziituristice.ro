<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');

$handle = fopen("../sitemaps/sitemap_pagini.xml", "w+");

$siteurl = substr($sitepath,0,-1);

$xml = '<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="'.$sitepath.'sitemaps/xml-sitemap.xsl"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n\n";

function content($url) {
	$content = "<url>\n";
	$content .= "<loc>".$url."</loc>\n";
	$content .= "<lastmod>".date("Y-m-d")."</lastmod>\n";
	$content .= "<changefreq>daily</changefreq>\n";
	$content .= "<priority>0.5</priority>\n";
	$content .= "</url>\n\n";
	
	return $content;
}

//sejururi_fara_tip
$select_tari_sejururi = "SELECT
tari.denumire,
tari.id_tara
FROM oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
Inner Join tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY tari.id_tara
ORDER BY tari.denumire ASC ";
$result_tari_sejururi = mysql_query($select_tari_sejururi) or die(mysql_error());
while($row_oferte_tara = mysql_fetch_array($result_tari_sejururi)) {
	$id_tara = $row_oferte_tara['id_tara'];
	if($row_oferte_tara['id_tara']<>'1') {
		$xml .= content($siteurl.'/sejur-'.fa_link($row_oferte_tara['denumire']).'/');
	} else {
		$select_zone_sejururi = "SELECT
		zone.id_zona,
		zone.denumire
		FROM oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		WHERE oferte.valabila = 'da'
		AND tari.id_tara = '1'
		AND hoteluri.tip_unitate <> 'Circuit'
		GROUP BY zone.id_zona
		ORDER BY zone.denumire ASC ";
		$result_zone_sejururi = mysql_query($select_zone_sejururi) or die(mysql_error());
		while($row_zone_sejururi = mysql_fetch_array($result_zone_sejururi)) {
			$xml .= content($siteurl.'/sejur-romania/'.fa_link($row_zone_sejururi['denumire']).'/');
		} @mysql_free_result($result_zone_sejururi);
	}
	
	$select_oras = "SELECT
	localitati.denumire AS denumire_localitate,
	localitati.id_localitate,
	zone.denumire
	FROM oferte
	Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Inner Join zone ON localitati.id_zona = zone.id_zona
	Inner Join tari ON zone.id_tara = tari.id_tara
	WHERE oferte.valabila = 'da'
	AND tari.id_tara = '".$id_tara."'
	AND hoteluri.tip_unitate <> 'Circuit' AND oferte.cazare = 'nu'
	GROUP BY localitati.id_localitate
	ORDER BY localitati.denumire ASC ";
	$result_oras = mysql_query($select_oras) or die(mysql_error());
	while($row_oras = mysql_fetch_array($result_oras)) {
		$xml .= content($siteurl.'/cazare-'.fa_link($row_oras['denumire_localitate']).'/');
	} @mysql_free_result($result_oras);
} @mysql_free_result($result_tari_sejururi);

//sejururi_tip__
$tip = "SELECT
tip_oferta.id_tip_oferta,
tip_oferta.denumire_tip_oferta
FROM oferte
Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
Inner Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
Inner Join tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND tip_oferta.activ = 'da'
AND tip_oferta.apare_site = 'da'
AND tip_oferta.tip is not null
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY tip_oferta.denumire_tip_oferta
Order By tip_oferta.denumire_tip_oferta ";
$que_tip = mysql_query($tip) or die(mysql_error());
while($row_tip = mysql_fetch_array($que_tip)) {
	$id_tip = $row_tip['id_tip_oferta'];
	$den_tip = fa_link($row_tip['denumire_tip_oferta']);
	// echo content($siteurl.'/oferte-'.$den_tip.'/');
	/* mutat in fisierul sitemap_statice.php */
	
	//tara_tip__
	$select_tari_sejururi = "SELECT
	tari.denumire,
	tari.id_tara
	FROM oferte
	Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	Inner Join zone ON localitati.id_zona = zone.id_zona
	Inner Join tari ON zone.id_tara = tari.id_tara
	Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND oferta_sejur_tip.id_tip_oferta = '".$id_tip."'
	GROUP BY tari.id_tara
	ORDER BY tari.denumire ASC ";
	$result_tari_sejururi = mysql_query($select_tari_sejururi) or die(mysql_error());
	while($row_oferte_tara = mysql_fetch_array($result_tari_sejururi)) {
		$id_tara = $row_oferte_tara['id_tara'];
		$den_tara = fa_link($row_oferte_tara['denumire']);

		$xml .= content($siteurl.'/oferte-'.$den_tip.'/'.$den_tara.'/');
		
		//zona_tip__
		$select_zona_sejururi = "SELECT
		zone.denumire,
		zone.id_zona
		FROM oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		WHERE oferte.valabila = 'da'
		AND hoteluri.tip_unitate <> 'Circuit'
		AND oferta_sejur_tip.id_tip_oferta = '".$id_tip."'
		AND tari.id_tara = '".$id_tara."'
		GROUP BY zone.id_zona
		ORDER BY zone.denumire ASC ";
		$result_zona_sejururi = mysql_query($select_zona_sejururi) or die(mysql_error());
		while($row_oferte_zona = mysql_fetch_array($result_zona_sejururi)) {
			$id_zona = $row_oferte_zona['id_zona'];
			$den_zona = fa_link($row_oferte_zona['denumire']);
			
			$xml .= content($siteurl.'/oferte-'.$den_tip.'/'.$den_tara.'/'.$den_zona.'/');
			
			$select_oras = " SELECT
			localitati.denumire as denumire_localitate,
			localitati.id_localitate
			FROM oferte
			Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
			Inner Join zone ON localitati.id_zona = zone.id_zona
			Inner Join tari ON zone.id_tara = tari.id_tara
			Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND tari.id_tara = '".$id_tara."'
			AND zone.id_zona = '".$id_zona."'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND oferta_sejur_tip.id_tip_oferta = '".$id_tip."'
			GROUP BY localitati.id_localitate
			ORDER BY localitati.denumire ASC ";
			$result_oras = mysql_query($select_oras) or die(mysql_error());
			while($row_oras = mysql_fetch_array($result_oras)) {
				$xml .= content($siteurl.'/oferte-'.$den_tip.'/'.$den_tara.'/'.$den_zona.'/'.fa_link($row_oras['denumire_localitate']).'/');
			} @mysql_free_result($result_oras);
		} @mysql_free_result($result_zona_sejururi);
	} @mysql_free_result($result_tari_sejururi);
} @mysql_free_result($que_tip);

//circuite_____
$sel_cont = "SELECT
continente.nume_continent,
continente.id_continent
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN continente ON hoteluri.id_continent = continente.id_continent
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
GROUP BY continente.id_continent
ORDER BY continente.nume_continent ";
$que_cont = mysql_query($sel_cont) or die(mysql_error());
while($row_cont = mysql_fetch_array($que_cont)) {
	$id_continent = $row_cont['id_continent'];
	
	$sel_cont1 = "SELECT
	tari.denumire
	FROM oferte
	INNER JOIN hoteluri on oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
	INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara
	WHERE oferte.valabila = 'da'
	AND tari.id_continent = '".$id_continent."'
	AND hoteluri.tip_unitate = 'Circuit'
	GROUP BY tari.denumire
	ORDER BY tari.denumire ASC ";
	$que_cont1 = mysql_query($sel_cont1) or die(mysql_error());
	while($row_cont1 = mysql_fetch_array($que_cont1)) {
		$xml .= content($siteurl.'/circuite/'.fa_link($row_cont1['denumire']).'/');
	} @mysql_free_result($que_cont1);
} @mysql_free_result($que_cont);

$xml .= '</urlset>';

if(fwrite($handle, $xml) == FALSE) {
	echo "Cannot write to file";
	exit;
}
fclose($handle); ?>

<p>Sitemap PAGINI DINAMICE creat. <a href="../sitemaps/sitemap_pagini.xml">vezi sitemap</a></p>
