<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');

$handle = fopen("../sitemap.xml", "w+");

$xml = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>'.$sitepath.'sitemaps/sitemap_statice.xml</loc>
      <lastmod>'.date('Y-m-d').'T'.date('H:i:s').'+00:00</lastmod>
   </sitemap>
   <sitemap>
      <loc>'.$sitepath.'sitemaps/sitemap_pagini.xml</loc>
      <lastmod>'.date('Y-m-d').'T'.date('H:i:s').'+00:00</lastmod>
   </sitemap>
   <sitemap>
      <loc>'.$sitepath.'sitemaps/sitemap_hoteluri.xml</loc>
      <lastmod>'.date('Y-m-d').'T'.date('H:i:s').'+00:00</lastmod>
   </sitemap>
</sitemapindex>';

if(fwrite($handle, $xml) == FALSE) {
	echo "Cannot write to file";
	exit;
}
fclose($handle); ?>

<p>Sitemap INDEX creat. <a href="../sitemap.xml">vezi sitemap</a></p>
