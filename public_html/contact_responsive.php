<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Contact <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Pagina de contact a portalului Ocazii Turistice. Contactati-ne prin intermediul formularului, email sau telefoni pentru orice probleme." />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."includes/header/header_responsive.php"); ?>
</head>
<body>
 <header>
        <?php require("includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require("includes/header/breadcrumb_responsive.php"); ?>
    </div>
    
    <div class="layout contact_page" id="middle">

        <div id="middleInner">



            <div class="NEW-column-full">



                <div class="pad20">



                    <h1 style="clear:both;" class="blue">Contact Ocazii Turistice</h1>

                    <p style="clear:both;">Va punem la dispozitie numeroase metode de a ne contacta: telefonic, online, prin e-mail, facebook, twitter, blog, etc. Trebuie doar sa alegeti una din modalitatile descrise mai jos.</p>

                    <br />



                    <div class="contact-left">



                        <h2 class="green">Modalitati contact</h2>



                        <div class="contact-list reset-fntsize bkg-grey chn-grey">

                            <img src="https://www.ocaziituristice.ro/images/icon_contact_phone.png" width="32" height="32" alt="" class="image-left" />

                            <strong><?php echo $contact_telefon; ?></strong> ; <strong><?php echo $contact_mobil; ?></strong><br/>

                          Program: <?php echo $contact_program; ?>

                            <div class="clear"></div>

                        </div>



                        <div class="contact-list reset-fntsize bkg-grey chn-grey">
 							<img src="<?php echo $imgpath; ?>/icon_contact_email.png" width="32" height="32" alt="" class="image-left" />
 							<a href="mailto:<?php echo $contact_email; ?>" class="link"><?php echo $contact_email; ?></a><br />
                            Suport prin e-mail in cel mai scurt timp posibil
                            <div class="clear"></div>

                        </div>



                        <div class="contact-list reset-fntsize bkg-grey chn-grey">

                            <img src="<?php echo $imgpath; ?>/icon_contact_facebook.png" width="32" height="32" alt="" class="image-left" />
            <a href="http://www.facebook.com/OcaziiTuristice.ro" class="link" target="_blank">OcaziiTuristice</a><br />
                            Pagina de Facebook a portalului Ocazii Turistice
                            <div class="clear"></div>

                        </div>



                        <div class="contact-list reset-fntsize bkg-grey chn-grey">

                            <img src="<?php echo $imgpath; ?>/icon_contact_twitter.png" width="32" height="32" alt="" class="image-left" />
            				<a href="http://twitter.com/#!/ocaziituristice" class="link" target="_blank">@ocaziituristice</a><br />

                            Update-uri permanente despre oferte speciale si nu numai

                            <div class="clear"></div>

                        </div>



                        <div class="contact-list reset-fntsize bkg-grey chn-grey">

                            <img src="<?php echo $imgpath; ?>/icon_contact_blog.png" width="32" height="32" alt="" class="image-left" />
            <a href="http://blog.ocaziituristice.ro/" class="link" target="_blank">Blog Ocazii Turistice</a><br />

                            Informatii utile, discutii, recomandari si multe altele

                            <div class="clear"></div>

                        </div>



                        <div class="contact-list reset-fntsize bkg-grey chn-grey">

                            <img src="<?php echo $imgpath; ?>/icon_contact_maps.png" width="32" height="32" alt="" class="image-left" />
            <?php echo $contact_adresa; ?> <a href="#harta" class="link"><em>(click pentru harta)</em></a>

                            <div class="clear"></div>

                        </div>



                    </div>



                    <div class="contact-right">



                        <h2 class="green">Formular contact rapid</h2>
<?php if(isset($_POST['trimite']) and $_POST['trimite']=='trimite'){?>
<?php
$mesaj_email="";
$mesaj_email.="Nume: ".$_POST['nume']."\n";
$mesaj_email.="E-mail: ".$_POST['email']."\n";
$mesaj_email.="Telefon: ".$_POST['telefon']."\n";
$mesaj_email.="Mesaj: ".strip_tags($_POST['mesaj'])."\n

IP: ".$_SERVER['REMOTE_ADDR'];

$Name = "Contact Ocazii Turistice";
$email = "contact@ocaziituristice.ro";
$recipient = "contact@ocaziituristice.ro";

$subject = "Formular contact Ocazii Turistice";
$header = "From: ". $Name . " <" . $email . ">\r\n";

mail($recipient, $subject, $mesaj_email, $header);
?>
Cererea dumneavoastra a fost trimisa catre Echipa OcaziiTturistice.ro<br />
In cel mai scurt timp veti fi contactat in legatura cu subiectul trimis.
<br /><br />
Va multumim,<br />
Echipa OcaziiTuristice.ro
<?php } else { ?>


                        <div class="contact-form">

                            <form action="/contact.html" method="post" name="contactForm">

                                <div>

                                    <label for="nume">Nume (obligatoriu)</label>

                                    <input  class="width_inputs" name="nume" id="nume" type="text" value="" />

                                </div>

                                <div>

                                    <label for="email">E-mail (obligatoriu)</label>

                                    <input class="width_inputs" name="email" id="email" type="text" value=""/>

                                </div>

                                <div>

                                    <label for="telefon">Telefon (obligatoriu)</label>

                                    <input class="width_inputs" name="telefon" id="telefon" type="text" value="" />

                                </div>

                                <div>

                                    <label for="mesaj">Mesaj (obligatoriu)</label>

                                    <textarea name="mesaj" id="mesaj"></textarea>

                                </div>

                                <input name="trimite" type="hidden" value="trimite" />

                                <div align="center"><input style="background:#5cb85c;" type="submit" name="submit-comment" value=" Trimite " class="button-blue"></div>



                            </form>

                        </div>
<?php }?>


                    </div>



                    <br class="clear" /><br /><br />



                    <div class="contact-left">

                        <h2 class="blue" style="margin-left:0; margin-right:0;">Agentia de turism DREAM VOYAGE</h2>

                        <div class="contact-list bkg-grey chn-grey">

                               <strong>Licenta tur-operator:</strong> <?php echo $contact_licenta; ?> <a href="<?php echo $sitepath; ?>files/licenta_turism.jpg" title="Licenta tur-operator" target="_blank" rel="nofollow"><img src="<?php echo $imgpath; ?>/icon_external_link.png" alt="" /></a><br />
            <?php echo $contact_brevet; ?> <a href="<?php echo $sitepath; ?>files/brevet_turism.jpg" title="Brevet de Turism" target="_blank" rel="nofollow"><img src="<?php echo $imgpath; ?>/icon_external_link.png" alt="" /></a><br />
            <?php echo $contact_asigurare; ?> <a href="<?php echo $sitepath; ?>files/asigurare.jpg" title="Asigurare" target="_blank" rel="nofollow"><img src="<?php echo $imgpath; ?>/icon_external_link.png" alt="" /></a><br />
            <?php echo $contact_protectia_datelor; ?> <a href="<?php echo $sitepath; ?>files/protectia_datelor.jpg" title="Protectia Datelor" target="_blank" rel="nofollow"><img src="<?php echo $imgpath; ?>/icon_external_link.png" alt="" /></a><br />

                            <br />

                             <strong>Adresa punct de lucru:</strong> <?php echo $contact_adresa; ?><br />
            <strong>Tel:</strong> <?php echo $contact_telefon; ?><br />
            <strong>Fax:</strong> <?php echo $contact_fax; ?><br />
            <strong>Mobil:</strong> <?php echo $contact_mobil; ?><br />
            <strong>E-mail:</strong> <a href="mailto:<?php echo $contact_email; ?>" class="link"><?php echo $contact_email; ?></a><br />

                            <br />

                            <strong>Program:</strong><br />

                            <?php echo $contact_program; ?>  </div>



                        <br>



                        <h2 class="blue" style="margin-left:0; margin-right:0;">Compania <?php echo $contact_legal_den_firma; ?></h2>

                        <div class="contact-list bkg-grey chn-grey">

                            <strong>Nr. reg. com.:</strong> <?php echo $contact_legal_reg_com; ?><br />

                            <strong>Cod fiscal:</strong>  <?php echo $contact_legal_cui; ?> <a href="<?php echo $sitepath; ?>files/cui.jpg" title="Certificat Unic de Inregistrare" target="_blank" rel="nofollow"><img src="<?php echo $imgpath; ?>/icon_external_link.png" alt="" /></a><br />

                            <strong>Capital social:</strong>  <?php echo $contact_legal_capital_social; ?><br />

                            <strong>Adresa sediu social:</strong> <?php echo $contact_legal_adresa; ?><br />

                            <br />

                            <strong>Banca</strong> <?php echo $contact_legal_banca; ?><br />

                            <strong>RON:</strong><?php echo $contact_legal_cont_lei; ?><br />

                            <strong>EURO:</strong> <?php echo $contact_legal_cont_euro; ?></div>

                    </div>



                    <div class="contact-right">

                        <a name="harta"></a>

                        <h2 class="text-left blue" style="margin-left:0; margin-right:0;">Localizare pe harta</h2>

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d356.05071533472756!2d26.0979547287839!3d44.44535115849805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b1ff4e0892c3d7%3A0x9844827e355d353b!2sOcaziituristice.ro!5e0!3m2!1sro!2sro!4v1450342050629" width="100%" height="450" frameborder="0" style="border:0"></iframe>



                        <br class="clear"><br>



                        <h2 class="text-left blue">Documente</h2>

                        <ul>

                            <li><a href="/files/cui.jpg" title="Certificat Unic de Inregistrare" class="link-blue" target="_blank" rel="nofollow">Certificat Unic de Inregistrare</a></li>

                            <li><a href="/files/licenta_turism.jpg" title="Licenta tur-operator" class="link-blue" target="_blank" rel="nofollow">Licenta tur-operator</a></li>

                            <li><a href="/files/asigurare.jpg" title="Polita de asigurare" class="link-blue" target="_blank" rel="nofollow">Polita de asigurare</a></li>

                            <li><a href="/files/brevet_turism.jpg" title="Brevet de Turism" class="link-blue" target="_blank" rel="nofollow">Brevet de turism</a></li>

                            <li><a href="/files/protectia_datelor.jpg" title="Operator de date cu caracter personal" class="link-blue" target="_blank" rel="nofollow">Protectia datelor</a></li>

                        </ul>

                    </div>



                    <br class="clear" />



                </div>



            </div>

        </div>

    </div>



<?php require_once("includes/newsletter_responsive.php"); ?>

<?php require_once("includes/footer/footer_responsive.php"); ?>