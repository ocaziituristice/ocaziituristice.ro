<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body>
<div id="NEW-destinatie" class="clearfix">
<h1 class="green"><a href="<?php echo $sitepath; ?>contact.html" target="_blank">Contact</a></h1>

<hr />

<h2 class="blue" style="margin-left:0; margin-right:0;">Compania <?php echo $contact_legal_den_firma; ?></h2>
<strong>Nr. reg. com.:</strong> <?php echo $contact_legal_reg_com; ?><br />
<strong>Cod fiscal:</strong> <?php echo $contact_legal_cui; ?><br />
<strong>Adresa sediu social:</strong> <?php echo $contact_legal_adresa; ?><br />
<br />
<strong>Banca</strong> <?php echo $contact_legal_banca; ?><br />
<strong>RON:</strong> <?php echo $contact_legal_cont_lei; ?><br />
<strong>EURO:</strong> <?php echo $contact_legal_cont_euro; ?><br />
<br />
<h2 class="blue" style="margin-left:0; margin-right:0;"><?php echo $contact_den_agentie; ?></h2>
<strong>Licenta touroperator:</strong> <?php echo $contact_licenta; ?><br />
<?php echo $contact_brevet; ?><br />
<?php echo $contact_asigurare; ?><br />
<br />
<strong>Adresa punct de lucru:</strong> <?php echo $contact_adresa; ?><br />
<strong>Tel:</strong> <?php echo $contact_telefon; ?><br />
<strong>Fax:</strong> <?php echo $contact_fax; ?><br />
<strong>Mobil:</strong> <?php echo $contact_mobil; ?><br />
<strong>E-mail:</strong> <a href="mailto:<?php echo $contact_email; ?>" class="link"><?php echo $contact_email; ?></a><br />
<br />
<strong>Program:</strong><br />
<?php echo $contact_program; ?>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
