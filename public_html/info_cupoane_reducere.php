<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cupoane de Reducere | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Termeni si conditii Ocazii Turistice" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <h1 class="blue" style="float:left;">Cupoane de Reducere</h1>
        
        <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>

        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="text-justify pad20 article">
          <h2>Ce sunt Cupoanele de Reducere?</h2>
          <p>Cupoanele de Reducere sunt emise în format digital de către <?php echo $contact_den_agentie; ?> și pot fi utilizate pentru achiziția pachetelor turistice doar de pe site-ul <?php echo $denumire_agentie; ?>, doar la rezervarile efectuate on-line. Pentru rezervarile efectuate telefonic nu se acordă reducerea aferentă acestor cupoane.</p>
          <br>
          <h2>Cum obțin un Cupon de Reducere?</h2>
          <p>Cupoanele de Reducere se obțin în urma completării datelor dumneavoastră în formularul <strong>Înregistrează Cupon</strong>. Adresa către acest formular o veți primi pe email în urma unor campanii sau promoții efectuate de <?php echo $contact_den_agentie; ?>.</p>
          <p>După completarea corectă a acestui formular, veți primi un email pe adresa înregistrată ce va conține <strong>Codul Cuponului de Reducere</strong>, <strong>Valoarea reducerii aplicate</strong>, <strong>Destinația unde este valabil</strong>, <strong>Perioada de valabilitate a Cuponului</strong>, precum și alte informații ajutătoare.</p>
          <p>Va rugăm să păstrați acest email sau să il printați, întrucât veți avea nevoie de <strong>Codul Cuponului de Reducere</strong> pentru a obține reducerea în cadrul rezervării.</p>
          <br>
          <h2>Cum folosesc un Cupon de Reducere?</h2>
          <p>Cuponul de Reducere are un cod unic pentru fiecare client și <strong>poate fi folosit o singură dată</strong>.</p>
          <p>Când efectuați rezervarea on-line, în pasul în care introduceți numele turiștilor, datele de contact și de facturare, veți regăsi și o zonă special dedicată acestor Cupoane de Reducere - <strong>Reduceri</strong> (de obicei în partea de sus, deasupra câmpurilor pentru numele turiștilor). Adăugați <strong>CODUL Cuponului</strong> în câmpul indicat. Apoi în acea secțiune va apărea <strong>valoarea reducerii</strong>, având posibilitatea de selectare. Selectând această reducere, costul total se va reduce <strong>pe loc </strong>cu valoarea indicată. În acest pas aveți posibilitatea de a alege dacă doriți să folosiți sau nu Cuponul de Reducere prin bifarea/debifarea acestei casete.</p>
          <img src="/images/img_info_cupon_reducere_1.jpg" alt="cupoane reducere" width="460" class="float-left mar5">
          <img src="/images/img_info_cupon_reducere_2.jpg" alt="cupoane reducere" width="460" class="float-left mar5">
          <br class="clear"><br>
          <p><strong>ATENȚIE!</strong></p>
          <ul>
            <li>Cuponul de Reducere poate fi folosit doar de persoana care l-a inregistrat doar <strong>o singură dată</strong>.</li>
            <li>Cuponul de Reducere poate fi folosit doar la <strong>rezervările on-line</strong>.</li>
            <li>Cuponul de Reducere <strong>NU este transmisibil</strong>.</li>
            <li>Perioada de valabilitate a Cuponului de Reducere este înscrisă în email-ul primit la înregistrarea acestuia. Cuponul de Reducere nu poate fi folosit în afara acestei perioade. Valabilitatea cuponului se referă la perioada în care acesta se folosește la rezervare, nu perioada sejurului ales.</li>
          </ul>
        </div>
        
      </div>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>