<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');

$nume_hotel=desfa_link($_REQUEST['hotel']);
$id_hotel=get_idhotel($nume_hotel, desfa_link($_REQUEST['loc']));
$det = new DETALII_HOTEL();
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$id_oferta='';

$metas_title = '';
$metas_description = '';
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?></title>
<meta name="description" content="<?php echo $metas_description; ?>" />
<link rel="canonical" href="<?php echo $sitepath.'hoteluri-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full pad20">
      <h1 class="blue">Iti multumim pentru evaluare !</h1>
      <p class="pad10 bigger-13em bold">Comentariul tau a fost trimis catre un administrator pentru aprobare.</p>
      
      <br><br>
      
      <?php /*?><div class="fb-like-box" data-href="http://www.facebook.com/OcaziiTuristice.ro" data-width="292" data-show-faces="true" data-stream="false" data-header="false" style="float:right;"></div><?php */?>
      <h2 class="blue bigger-15em">Ce poti face acum?</h2>
      <h2 class="red">Spune-le si prietenilor tai despre acest hotel:</h2>
      <div class="float-left">
        <img src="<?php echo '/thumb_hotel/'.$detalii_hotel['poza1']; ?>" class="images" style="margin:10px;" />
        <h3 style="margin:10px 10px 0 10px;"><?php echo $metas_title; ?></h3>
        <div class="grey italic"><?php echo substr($sitepath,0,-1).make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL); ?></div>
        <div style="float:left; margin:30px 0 0 30px;">
          <a name="fb_share"></a>
		  <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
        </div>
        <br class="clear">
        <a href="<?php echo make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL); ?>" class="button-blue" style="display:inline-block; margin:10px 0 0 10px;">&laquo; Inapoi la <?php echo $detalii_hotel['denumire']; ?></a>
      </div>
      <br class="clear" />

      <br><br>
      
      <?php /*?><div class="bigger-15em bold"><img src="/images/icon_warning.png" style="vertical-align:middle; margin:-8px 5px 0 0;"> <span class="bigger-15em"><span class="red">STAI!</span> <span class="blue">Parerea ta conteaza!</span></span></div>
      <br>
      <div class="bigger-13em bold">Vrem sa imbunatatim experienta ta si a celorlalti vizitatori pe site-ul nostru. Astfel <span class="underline">avem nevoie de ajutorul tau!</span><br>
      Te rugam sa raspunzi la intrebarile de mai jos privind site-ul Ocazituristice.ro</div>
      
      <br><br><?php */?>
      
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
