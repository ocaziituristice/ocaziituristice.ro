<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/adm/class/rezervare.php"); 
$meta_index = "noindex,follow";
?>
<?php
	//  ------------------------------------------------------------------------------------------
	// BEGIN PlatiOnline.RO
	// First i must verify that the answer it is comming from PO servers
	// include the rsa files

	//includ fisierul cu functiile de criptare
	include("PlatiOnlineRo/RSALib.php");
	include("PlatiOnlineRo/clspo.php");

	$my_class = new PO3();

	$my_class->LoginID = $lid;
	$my_class->KeyEnc = $ke;
	$my_class->KeyMod = $km;

	//encript the F_MESSAGE_RELAY with RSA algoritm and then do the HMAC KEY and compare with F_CRYPT_MESSAGE_RELAY
	//in functie de metoda aleasa pui _GET sau _POST
	$vF_Message_Relay =			$_POST["F_MESSAGE_RELAY"];
	$vF_Crypt_Message_Relay = 	$_POST["F_CRYPT_MESSAGE_RELAY"];
	$vX_RESPONSE_CODE = 		$_POST["X_RESPONSE_CODE"];
	$vX_RESPONSE_REASON_TEXT = 	$_POST["X_RESPONSE_REASON_TEXT"];
	$vF_ORDER_NUMBER = 			$_POST["F_ORDER_NUMBER"];
	$vF_Amount = 				$_POST["F_AMOUNT"];
	$vF_Currency = 				strtoupper($_POST["F_CURRENCY"]);
	$vX_TRAN_ID =				$_POST["X_TRAN_ID"];
	$vMy_F_Message_Relay =		strtoupper($my_class->VerifyFRM(strval($vF_Message_Relay)));

	// make sure the response it is from PlatiOnline.ro servers
	if($vF_Crypt_Message_Relay!=$vMy_F_Message_Relay)
		die("<h3>Error!</h3><p>hacking attempt.[Relay Message]</p>");

	$vA = explode("^", $vF_Message_Relay);

	// if the curency do not match with the message currency decline the transaction
	$vCurrencyMessage = $vA[4];

	if($vCurrencyMessage != $vF_Currency)
		die("<h3>Error!</h3><p>Hacking attempt.[Currency Relay Message]</p>");

	// if the amount do not match with the message amount decline the transaction
	$vAmountMessage = $vA[3];
	if($vAmountMessage != round($vF_Amount, 2))  
		die("<h3>Error!</h3><p>Hacking attempt.[Amount Relay Message]</p>");
	// if the response code do not match with the message response code decline the transaction

	$vX_Response_Code_Message = $vA[5];

	if($vX_RESPONSE_CODE != $vX_Response_Code_Message)
		die("<h3>Error!</h3><p>Hacking attempt.[Response Code Relay Message]</p>");

	$vX_TRAN_ID_Message = $vA[7];

	if($vX_TRAN_ID != $vX_TRAN_ID_Message)
		die("<h3>Error!</h3><p>Hacking attempt.[Tranzaction ID Relay Message]</p>");

	// ATENTIE
	// validare ca nu e refresh
	// dupa tot F_Message sau doar dupa stampul din F_Message
	// daca e refresh atunci NU treb sa inregistrezi duplicat rezultatul
	$vReponseStamp = $vA[2];
	// END PlatiOnline.ro
	//  -------------

	switch($vX_RESPONSE_CODE) {
		case '2':
			//	aprobata - actualizati statusul comenzii in magazinul dvs.
			$mesaj_procesator="Plata Aprobata";
			$id_rezervare=$vF_ORDER_NUMBER ;
			$suma_platita=$vF_Amount;
			$moneda=$vF_Currency;
			$id_tranzactie_comerciant=$vX_TRAN_ID;
			
			break;
		case '13':
			//	on hold - actualizati statusul comenzii in magazinul dvs.
			$mesaj_procesator="in asteptare"; 
			$mesaj_procesator=$vX_RESPONSE_REASON_TEXT;
			$id_rezervare=$vF_ORDER_NUMBER ;
			break;
		case '8':
			$mesaj_procesator="plata refuzata - Mai incerca odata";
			$mesaj_procesator=$vX_RESPONSE_REASON_TEXT;
			$id_rezervare=$vF_ORDER_NUMBER ;
			//	refuzata - actualizati statusul comenzii in magazinul dvs.
			
			break;
		case '10':
			$mesaj_procesator=$vX_RESPONSE_REASON_TEXT." - Mai incearca odata plata";
			$id_rezervare=$vF_ORDER_NUMBER ;
			$mesaj=$vF_Crypt_Message_Relay;
			//	eroare - actualizati statusul comenzii in magazinul dvs.
			
			break;
	}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Confirmare plata -<?php echo $denumire_agentie; ?></title>
<meta name="description" content="Despre <?php echo $denumire_agentie; ?>, istoric, poveste, modalitati contact" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <h1 class="blue"><?php echo $mesaj_procesator?> </h1>
        
        <div class="Hline"></div>
        
        <div class="text-justify pad10 article clearfix">
        
		  <div class="float-left w700">
			<p class="bigger-12em">
            <?php
			//$id_rezervare=4422;
			
			$rezervare = new REZERVARE();
			$row_rez=$rezervare->date_rezervare($id_rezervare);
			
			?>
            Datele tranzactiei tale sunt: <br />
            Numarul de rezervare: <?php echo $id_rezervare;?><br />
			Suma Platita: <?php echo $row_rez['pret'].$row_rez['moneda']." - ". final_price_lei($row_rez['pret']." LEI", $row_rez['moneda'], $adaos=NULL)?><br />
			id tranzactiei de pe card = <?php echo $id_tranzactie_comerciant;?> <br/>
         
        <?php 
         if($vX_RESPONSE_CODE==2)
		 	{
				//operatie de adaugare plata in tranzactii
			 }
         
		 ?>
         
         
            <?php 
		if($vX_RESPONSE_CODE==10||$vX_RESPONSE_CODE==8)
		
		{
		echo $mesaj_procesator=$vX_RESPONSE_REASON_TEXT;
		$id_rezervare=$vF_ORDER_NUMBER ;
	
					
			if ($row_rez['disponibilitate']=="disponibil")	
		
		{
		$OrderNumber=$id_rezervare;	
		$suma_de_plata=final_price_lei($row_rez['pret'], $row_rez['moneda'], $adaos=NULL);
		$denumire_produs=$row_rez['oferta_denumire'];
		$descriere_produs="descriere _ ".$row_rez['oferta_denumire'];
		$prenume=$row_rez['prenume'];
		$nume=$row_rez['nume'];
		$adresa_facturare=$row_rez['adresa_facturare'];
		$oras_facturare=$row_rez['oras_facturare'];
		$telefon_facturare=$row_rez['telefon_facturare'];
		$email_facturare=$row_rez['email_facturare'];
		$denumire_facturare=$row_rez['denumire_facturare'];
		$reg_comert=$row_rez['reg_comert'];
		$cui_cnp=$row_rez['cui_cnp'];
		
		
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/plata_card.php");
		 }
		}
			?>
                   
            </p>
            
             <div class="NEW-orange NEW-round8px" style="border:1px solid #ffd0d0; padding:10px 5px;">
        <img src="/images/cerere_oferta.jpg" class="float-left" style="margin:10px;" />
        <div class="float-right" style="margin:0 10px 0 50px; width:260px;">
          <p>Pentru orice nelamuriri sau intrebari nu ezita sa ne contactezi:</p>
          <strong>Telefon:</strong> <?php echo $contact_telefon; ?><br />
          <strong>Mobil:</strong> <?php echo $contact_mobil; ?><br />
          <strong>Program:</strong> <?php echo $contact_program; ?>
        </div>
        <p style="padding-top:17px;" class="black">In scurt timp un agent de turism din echipa <?php echo $denumire_agentie; ?> te va contacta (prin email sau telefon) a discuta toate detaliile cum vei intra in posesia documentelor de calatorie</p>
        <p class="black">Iti multumim pentru interesul acordat!</p>
        <br class="clear" />
      </div>
      
            
          </div>
          
      
                  
        </div>
        
       
        
     
        
        <br><br>
        
  
        
    
     
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>


