<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );

$nume_hotel    = desfa_link( $_REQUEST['nume'] );
$id_hotel      = get_idhotel( $nume_hotel, desfa_link( $_REQUEST['localitate'] ) );
$id_hotel      = desfa_link( $_REQUEST['id_hotel'] );
$plecdata      = $_REQUEST['plecdata'];
$det           = new DETALII_HOTEL();
$detalii_hotel = $det->select_camp_hotel( $id_hotel );
$det->hotel_vizitat( $id_hotel );
$tip_pagina = 'offerdetail';



$offers    = $det->get_oferte( $id_hotel );
$nr_oferte = count( $offers );

$id_oferta           = '';
$den_hotel           = ucwords( $detalii_hotel['denumire'] );
$den_tara            = ucwords( $detalii_hotel['tara'] );
$den_zona            = ucwords( $detalii_hotel['zona'] );
$den_localitate      = ucwords( $detalii_hotel['localitate'] );
$hoteluri_localitate = hoteluri_localitate( $detalii_hotel['id_localitate'], $id_hotel, $detalii_hotel['latitudine'], $detalii_hotel['longitudine'] );

	$metas_title = $detalii_hotel['denumire'] . ' din ' . $detalii_hotel['localitate'] . ', ' . $detalii_hotel['tara'];


	$metas_description = $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate'] . ' ' . $detalii_hotel['tara'] . ' - ' . $detalii_hotel['stele'] . ' stele . Oferte si sejururi de cazare la ' . $detalii_hotel['denumire'] . ' din ' . $detalii_hotel['zona'] . ', ' . $detalii_hotel['tara'];

?>

<!DOCTYPE html>
<html lang="ro">
<head>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/header_charset.php" ); ?>
    <title><?php echo $metas_title; ?><?php if ( $nr_oferte < 1 )
			echo "0 oferte disponibile" ?></title>

    <meta name="description" content="<?php echo $metas_description; ?>"/>
    <link rel="canonical"
          href="<?php echo $sitepath . 'cazare-' . fa_link( $detalii_hotel['localitate'] ) . '/' . fa_link_oferta( $detalii_hotel['denumire'] ) . '/'; ?>"/>

    <meta property="og:title" content="<?php echo $metas_title; ?>"/>
    <meta property="og:description" content="<?php echo $metas_description; ?>"/>
    <meta property="og:type" content="hotel"/>
    <meta property="og:url"
          content="<?php echo $sitepath . 'hoteluri-' . fa_link( $detalii_hotel['localitate'] ) . '/' . fa_link_oferta( $detalii_hotel['denumire'] ) . '/'; ?>"/>
    <meta property="og:image" content="<?php echo $sitepath . 'img_mediu_hotel/' . $detalii_hotel['poza1']; ?>"/>
    <meta property="og:latitude" content="<?php echo $detalii_hotel['latitudine']; ?>"/>
    <meta property="og:longitude" content="<?php echo $detalii_hotel['longitudine']; ?>"/>
    <meta property="og:street-address" content="<?php echo $detalii_hotel['adresa'];
	if ( $detalii_hotel['nr'] ) {
		echo ' nr. ' . $detalii_hotel['nr'];
	} ?>"/>
    <meta property="og:locality" content="<?php echo $detalii_hotel['localitate']; ?>"/>
    <meta property="og:region" content="<?php echo $detalii_hotel['zona']; ?>"/>
    <meta property="og:postal-code" content="<?php echo $detalii_hotel['cod_postal']; ?>"/>
    <meta property="og:site_name" content="<?php echo $denumire_agentie; ?>"/>
    <meta property="fb:admins" content="100000435587231"/>
    <meta property="fb:app_id" content="314711018608962"/>
	<?php
	include( $_SERVER['DOCUMENT_ROOT'] . '/includes/hoteluri/reviews_top.php' ); ?>
	<?php require_once( "includes/header/header_responsive.php" ); ?>
    <script src="/js/jquery_validate/jquery.validationEngine-ro.js"></script>
    <script src="/js/jquery_validate/jquery.validationEngine.js"></script>
</head>
<body>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header/admin_bar_responsive.php'); ?>
<?php // Header ?>
<header>
	<?php require( $_SERVER['DOCUMENT_ROOT'] . "/includes/header/meniu_header_responsive.php" ); ?>
</header>

<?php // Breadcrumbs and general search ?>
<div class="layout">
	<?php require( $_SERVER['DOCUMENT_ROOT'] ."/includes/header/breadcrumb_responsive.php" ); ?>
	<?php require( $_SERVER['DOCUMENT_ROOT'] ."/includes/header/search_responsive.php" ); ?>
</div>


<?php // Hotel description ?>
<div id="hotel_descriptions"></div>

<?php // Hotel details content ?>
<div class="layout">
	<?php
	include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/hoteluri/detalii_generale_hotel.php" );
	?>
</div>

<?php // Newsletter ?>
<?php require_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/newsletter_responsive.php" ); ?>

<?php // Footer ?>
<?php require_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/footer/footer_responsive.php" ); ?>
<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom.php" ); ?>
</body>
</html>
