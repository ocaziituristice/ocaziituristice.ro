<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');

/*** check login admin ***/
$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

if(isset($_GET['data'])) {
	$codedData = $_GET['data'];
	//echo base64_decode($codedData);
	
	if(!isset($_COOKIE['cos_cumparaturi'])) {
		setcookie('cos_cumparaturi', $codedData, time()+2592000, '/', 'ocaziituristice.ro');
	} else {
		$cos_array = explode('+;++;+', $_COOKIE['cos_cumparaturi']);
		if(!in_array($codedData, $cos_array)) {
			$cookie_content = $_COOKIE['cos_cumparaturi'].'+;++;+'.$codedData;
			setcookie('cos_cumparaturi', $cookie_content, time()+2592000, '/', 'ocaziituristice.ro');
		}
	}
	
	
	$det_cos = explode('##', base64_decode($codedData));
	$id_oferta_cos = $det_cos[0]; // id oferta
	$cos_id_pret = $det_cos[1]; // id pret
	$cos_plecare = $det_cos[2]; // tip preturi (plecari sau perioade)
	$cos_data = $det_cos[3]; // data plecarii
	$cos_zile = $det_cos[4]; // nr nopti
	$cos_pret = $det_cos[5]; // pret
	$cos_id_cam = $det_cos[6]; // id_cam
	$cos_nr_adulti = $det_cos[7]; // nr adulti
	$cos_nr_copii = $det_cos[8]; // nr copii
	$cos_copil1 = $det_cos[9]; // copil1
	$cos_copil2 = $det_cos[10]; // copil2
	$cos_copil3 = $det_cos[11]; // copil3
	$cos_masa = $det_cos[12]; // tip masa
	$cos_den_camera = $det_cos[13]; // denumire camera
	$cos_data_early_booking=$det_cos[14]; //data early booking
	$cos_procent_early_booking=$det_cos[15]; //data early booking
	
	$ins_cos_cumparaturi = "INSERT INTO cos_cumparaturi SET 
	id_cos = '',
	cookie = '$codedData',
	id_client = NULL,
	id_oferta = '$id_oferta_cos',
	id_pret = '$cos_id_pret',
	plecari = '$cos_plecare',
	data_plecare = '$cos_data',
	nr_nopti = '$cos_zile',
	id_cam = '$cos_id_cam',
	den_cam = '$cos_den_camera',
	nr_adulti = '$cos_nr_adulti',
	nr_copii = '$cos_nr_copii',
	copil1 = '$cos_copil1',
	copil2 = '$cos_copil2',
	copil3 = '$cos_copil3',
	tip_masa = '$cos_masa',
	data_early_booking='$cos_data_early_booking',
	procent_early_booking='$cos_procent_early_booking',
	pret = '$cos_pret',
	data_adaugare = now(),
	data_stergere = NULL,
	tip_stergere = NULL,
	ip = '".$_SERVER['REMOTE_ADDR']."',
	browser_info = '".$_SERVER['HTTP_USER_AGENT']."'
	";
	$rez_cos_cumparaturi = mysql_query($ins_cos_cumparaturi) or die (mysql_error());
	
	$insert_id = mysql_insert_id();
?>
<span id="result<?php echo $insert_id; ?>"></span>
<script>
$("#result<?php echo $insert_id; ?>").html('<div class="black bigLoader"><div class="chenar">Se salvează în <span class="blue bold">Ofertele mele</span><br><img src="/images/loader3.gif" alt="loading"></div></div>').delay(1000).fadeOut("slow");
</script>
<?php
	//redirect_php('/ofertele-mele/');
	echo '<a href="/ofertele-mele/" class="smaller-09em link-blue" target="_blank" title="Deschide lista cu ofertele salvate"><i class="icon-heart green"></i> <span class="underline">Ofertă salvată</span></a>';
}
?>
