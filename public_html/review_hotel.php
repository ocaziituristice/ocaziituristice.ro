<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
$FM1=new SEND_EMAIL;
$id_hotel=$_REQUEST['hid'];
$det= new DETALII_HOTEL();
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$id_oferta='';

if($detalii_hotel['denumire']=='') {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();	
}

$metas_title='Parerile turistilor despre '.$detalii_hotel['denumire'];
$metas_description='Review '.$detalii_hotel['denumire'].', parerile turistilor despre '.$detalii_hotel['denumire'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="robots" content="noindex, nofollow">
<meta name="robots" content="noarchive">
<meta name="googlebot" content="nosnippet">
<meta name="googlebot" content="noodp">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?></title>
<meta name="description" content="<?php echo $metas_description; ?>" />
<link rel="canonical" href="<?php echo $sitepath.'hoteluri-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
    <?php if(!$pas=$_REQUEST['pas']) $pas=1;
	switch($pas) {
		case '1': include_once($_SERVER['DOCUMENT_ROOT']."/includes/review_hotel.php"); break;
		case '2': include_once($_SERVER['DOCUMENT_ROOT']."/includes/review_hotel_finalizare.php"); break;
		case '3': include_once($_SERVER['DOCUMENT_ROOT']."/includes/review_hotel_multumire.php"); break;
	} ?>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>