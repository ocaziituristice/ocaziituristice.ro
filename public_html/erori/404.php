<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include __DIR__ . '/../config/seteri.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>404 Pagina nu a putut fi gasita</title>
    <link href="/style404.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<div id="container">
    <div id="left">
        <a href="http://www.ocaziituristice.ro" title="Ocazii Turistice"><img src="/images/logo.jpg" alt="Ocazii Turistice"/></a>
        <br/><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.ocaziituristice.ro" title="Inapoi la prima pagina">&laquo; inapoi la Ocazii Turistice</a>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <h2 class="blue" align="right">Va recomandam alte sectiuni interesante:</h2>
    </div>

    <div id="right">
        <h1 class="blue">Pagina nu a putut fi gasita</h1>
        Ne cerem scuze pentru incovenienta, dar pagina pe care ati incercat sa o accesati nu exista in sistemul nostru.
        <br/><br/>
        Pagina s-ar putea sa fi fost mutata, sau adresa nu a fost corect introdusa. Daca problema persista, va rugam sa
        <a href="http://www.ocaziituristice.ro/contact.html" title="Contactati departamentul tehnic"><strong>contactati departamentul tehnic</strong></a>.
        <br/>
        <img src="/images/img_404page.jpg" alt="error 404"/>
        <br/><br/>
        &raquo; <a href="http://www.ocaziituristice.ro/sejur-bulgaria/" title="Oferte Bulgaria">Oferte sejur Bulgaria</a><br/>
        &raquo; <a href="http://www.ocaziituristice.ro/sejur-turcia/" title="Oferte Turcia, chartere si cazari">Oferte Turcia</a><br/>
        &raquo; <a href="http://www.ocaziituristice.ro/sejur-grecia/" title="Oferte Grecia">Oferte Grecia, chartere si cazare</a><br/>
        &raquo; <a href="http://www.ocaziituristice.ro/oferte-city-break/" title="Oferte City Break">Oferte City Break</a><br/>
        &raquo; <a href="http://www.ocaziituristice.ro/last-minute/" title="Oferte Last Minute">Oferte Last Minute</a><br/>
        &raquo; <a href="http://www.ocaziituristice.ro/early-booking/" title="Oferte Early Booking ">Oferte Early Booking </a><br/>
        <br/>
    </div>
</div>
<?php include_once __DIR__ . '/../includes/addins_bodybottom.php'; ?>
</body>
</html>
