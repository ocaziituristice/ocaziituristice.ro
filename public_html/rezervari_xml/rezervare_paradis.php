<?php
$RequestUser='dream.voyage';
$RequestPass='2013rezervare';
$TourOpCode='PRD';
$sUrl = 'http://rezervari.paradistours.ro/server_xml/server.php';
$cod_pat_suplimentar=60;
$arivdata = date('Y-m-d', strtotime($plecdata.' + '.$pleczile.' days'));
//if(!$err_logare_admin) { echo '<pre>';print_r($detalii);echo '</pre>'; }

$trip_Room_Chd_Age = '';
for($i=0; $i<$copii; $i++) {
	$trip_Room_Chd_Age .= '<Age>'.$copil_age[$i].'</Age>';
}

if($detalii['moneda']=='EURO') $trip_currency = 'EUR';
	else $trip_currency = $detalii['moneda'];

$sel_request = "SELECT
import_hoteluri.*,
localitati_corespondent.*,
import_hoteluri.nume_hotel,
import_hoteluri.stele_hotel,
localitati_corespondent.code_furnizor AS hotel_destCode
FROM import_hoteluri
INNER JOIN hoteluri ON hoteluri.id_hotel = import_hoteluri.id_hotel_corespondent
INNER JOIN localitati_corespondent ON localitati_corespondent.id_corespondent = hoteluri.locatie_id
WHERE import_hoteluri.id_hotel_corespondent = '".$id_hotel."'
AND import_hoteluri.id_furnizor = '".$id_furnizor."'
AND localitati_corespondent.id_furnizor = '".$id_furnizor."'
LIMIT 0,1 ";
$que_request = mysql_query($sel_request) or die(mysql_error());
$row_request = mysql_fetch_array($que_request);

$trip_CountryCode = $detalii_hotel['country_code'];
$trip_CityCode = $row_request['hotel_destCode'];
$trip_ProductName = $row_request['nume_hotel'];
$trip_ProductCategory = $row_request['stele_hotel'];
$trip_CurrencyCode = $trip_currency;


$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelPriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>'.$RequestUser.'</RequestUser>
    <RequestPass>'.$RequestPass.'</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
  </AuditInfo>
  <RequestDetails>
    <getHotelPriceRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
      <TourOpCode>'.$TourOpCode.'</TourOpCode>
      <ProductName>'.$trip_ProductName.'</ProductName>
      <ProductCategory>'.$trip_ProductCategory.'</ProductCategory>
      <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
      <Language>RO</Language>
      <OfferType>TOATE</OfferType>
      <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
      <Rooms>
        <Room Code="DB" NoAdults="'.$adulti.'" NoChildren="'.$copii.'">
          <Children>
            '.$trip_Room_Chd_Age.'
          </Children>
        </Room>
      </Rooms>
    </getHotelPriceRequest>
  </RequestDetails>
</Request>';


$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

$sResponse = curl_exec($ch);

$prices = new SimpleXMLElement($sResponse);
//if(!$err_logare_admin) { echo '<pre>';print_r($prices);echo '</pre>';  echo $sXml ; }
if($prices->ResponseDetails->getHotelPriceResponse != '') {
	if(count($rezcam)>0) $J = count($rezcam)+1; else $J=0;
	$product_code= (string)$prices->ResponseDetails->getHotelPriceResponse->Hotel->Product->ProductCode;
	foreach($prices->ResponseDetails->getHotelPriceResponse->Hotel->Offers->Offer as $key => $value) {
		$den_camera[$key] = (string) $value->BookingRoomTypes->Room;
	   
		$checkIn[$key] = (string) $value->PeriodOfStay->CheckIn;
		$checkOut[$key] = (string) $value->PeriodOfStay->CheckOut;
		$numar_de_nopti[$key] = dateDiff($checkOut[$key], $checkIn[$key])-1;
		//if($numar_de_nopti[$key]==$pleczile) {
		//if((string) $value->Availability['Code'] == 'IM') {
			$sel_roomtype = "SELECT tip_camera_corespondent.id_camera_corespondent, tip_camera.denumire
			FROM tip_camera_corespondent
			INNER JOIN tip_camera ON tip_camera.id_camera = tip_camera_corespondent.id_camera_corespondent
			WHERE tip_camera_corespondent.den_camera = '".$den_camera[$key]."' ";
			$que_roomtype = mysql_query($sel_roomtype) or die(mysql_error());
			$row_roomtype = mysql_fetch_array($que_roomtype);
		
			$rezcam[$J]['id_pret'] = 0;
			$rezcam[$J]['plecare'] = '1';
			$rezcam[$J]['id_camera'] = $row_roomtype['id_camera_corespondent'];
			if($row_roomtype['denumire']) $rezcam[$J]['denumire_camera'] = $row_roomtype['denumire']; else $rezcam[$J]['denumire_camera'] = schimba_caractere($den_camera[$key]);
			if((string)$value->ExtraBed==true) {
		$rezcam[$J]['pat_suplimentar']=true;
		$rezcam[$J]['denumire_camera']=$rezcam[$J]['denumire_camera'].' cu pat suplimentar';
		}
			$rezcam[$J]['pret'] = round((string) $value->Gross);
			$rezcam[$J]['comision'] = (string) $value->CommissionCed;
			$rezcam[$J]['moneda'] = $detalii['moneda'];
			$rezcam[$J]['nr_adulti'] = $adulti;
			$rezcam[$J]['nr_copii'] = $copii;
			$rezcam[$J]['copil1'] = $copil_age[0];
			$rezcam[$J]['copil2'] = $copil_age[1];
			$rezcam[$J]['copil3'] = $copil_age[2];
			if(isset($value->OfferDescription) and ((string) $value->Gross != (string) $value->PriceNoRedd)) {
				$rezcam[$J]['oferta'] = schimba_caractere((string) $value->OfferDescription);
				$rezcam[$J]['oferta_pret'] = round((string) $value->PriceNoRedd);
			} else {
				$rezcam[$J]['oferta'] = '';
				$rezcam[$J]['oferta_pret'] = '';
			}
			
			$rezcam[$J]['data_early_booking']=extrage_data($rezcam[$J]['oferta']);  
			$rezcam[$J]['comision_reducere']=extrage_procent( $rezcam[$J]['oferta']." ".$rezcam[$J]['grila_name']); 
			
			$rezcam[$J]['masa'] = '';
			if(sizeof($value->Meals->Meal)>0) {
				foreach($value->Meals->Meal as $k_meal => $v_meal) {
					$rezcam[$J]['masa'] .= schimba_caractere(change_meal((string) $v_meal)).', ';
					$rezcam[$J]['masa_neschimbata'].=(string) $v_meal.'*';
				}
				$rezcam[$J]['masa'] = substr($rezcam[$J]['masa'],0,-2);
			} else {
				$rezcam[$J]['masa'] .= 'Fără masă';
			}
			//if(((string) $value->Meals->Meal)=='') $rezcam[$J]['masa'] = 'Fara masa'; else $rezcam[$J]['masa'] = (string) $value->Meals->Meal;
			//$rezcam[$J]['series_name'] = (string) $value->SeriesName;
			$rezcam[$J]['PackageVariantId'] = (string) $value->PackageVariantId;
			$array_id_pachet=explode('_',$rezcam[$J]['PackageVariantId']);
			$rezcam[$J]['id_pachet']=$array_id_pachet['1'].'_'.$array_id_pachet['2'];
			//if($rezcam[$J]['PackageVariantId']==$id_oferta_furnizor)
			//{
		//$cod_camera[$key]=$value->BookingRoomTypes->Room;
		 //echo '<pre>';print_r($cod_camera[$key]);echo '</pre>';
		  foreach ($value->BookingRoomTypes->Room as $r_key => $r_valoare)
		  {
			 $rezcam[$J]['RoomCode']=(string)$r_valoare['Code'];
		  }
			//}
			$rezcam[$J]['grila_name'] = schimba_caractere((string) $value->GrilaName);
			$rezcam[$J]['data_early_booking']=extrage_data($rezcam[$J]['grila_name']);
			 
			$rezcam[$J]['period'] = date("d.m",strtotime($checkIn[$key])).' - '.date("d.m.Y",strtotime($checkOut[$key]));
			$rezcam[$J]['data_start'] = date("Y-m-d",strtotime($checkIn[$key]));
			if((string) $value->Availability['Code'] == 'IM') $rezcam[$J]['disponibilitate'] = 'disponibil';
				elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'request';
				//elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'disponibil';
				elseif((string) $value->Availability['Code'] == 'ST') $rezcam[$J]['disponibilitate'] = 'stopsales';
			$rezcam[$J]['nr_nights'] = $numar_de_nopti[$key];
			
			$rezcam[$J]['id_furnizor'] = $id_furnizor;
			$rezcam[$J]['furnizor_comision_procent'] = $furnizor['comision_procent'];
			
			if ($detalii['discount_tarif']>0) {$rezcam[$J]['furnizor_comision_procent']=$detalii['discount_tarif'];}
			
			$rezcam[$J]['id_oferta'] = $id_oferta;
			$rezcam[$J]['oferta_comision_procent'] =$detalii_online['discount_tarif'][$rezcam[$J]['id_oferta']];
			$J++;
		//}
	}
}
//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; }
//{ 
//echo $id_oferta_furnizor;
if($id_oferta_furnizor!='' and $cod_camera!='' and $pagina_curenta=='rezervare' ){
	for($a=1;$a<=$adulti;$a++) {
$pax_name.='<PaxName PaxType="adult">Adult '.$a.' </PaxName>'	;
}
for($i=0; $i<$copii; $i++) {
	$pax_name_copil.='<PaxName PaxType="child" ChildAge="'.$copil_age[$i].'">Copil '.$copil_age[$i].'</PaxName>';
}
 $sXml ='<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getItemFeesRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>'.$RequestUser.'</RequestUser>
    <RequestPass>'.$RequestPass.'</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
  </AuditInfo>
  <RequestDetails>
    <getItemFeesRequest CurrencyCode="RON">
      <BookingItems>
        <BookingItem ProductType="hotel">
          <TourOpCode>'.$TourOpCode.'</TourOpCode>
          <HotelItem>
           <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
             <ProductCode>'.$product_code.'</ProductCode>
            <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
            </PeriodOfStay>
            <VariantId>'.$id_oferta_furnizor.'</VariantId>
            <Rooms>
              <Room Code="'.$cod_camera.'" NoAdults="2">
                <PaxNames>"'.$pax_name.'"
           
                </PaxNames>
              </Room>
            </Rooms>
          </HotelItem>
        </BookingItem>
      </BookingItems>
    </getItemFeesRequest>
  </RequestDetails>
</Request>';


$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

$sResponse = curl_exec($ch);
//echo $sResponse;
$anulare = new SimpleXMLElement($sResponse);
//if(!$err_logare_admin) { echo '<pre>';print_r($anulare->ResponseDetails->getItemFeesResponse->ItemFees->ItemFee->Fees);echo '</pre>'; }

//$conditii_anulare = new SimpleXMLElement($anulare);


if($anulare->ResponseDetails->getItemFeesResponse->ItemFees->ItemFee->Fees != '') 
	{ 
		if(count($conditii_anulare)>0) $JA = count($anulare)+1; else $JA=0;
		foreach($anulare->ResponseDetails->getItemFeesResponse->ItemFees->ItemFee->Fees->Fee as $key => $value)
		{
			$conditii_anulare[$JA]['data_inceput']=(string)$value->FromDate;
			$conditii_anulare[$JA]['data_sfarsit']=(string)$value->ToDate;
			$conditii_anulare[$JA]['valoare']=(string)$value->Value;
			foreach ($value->Value as $v_key => $v_valoare)
		  {
			 if((string)$v_valoare['Procent'])$conditii_anulare[$JA]['tip_valoare_anulare']='procent';
		  }
			$JA++;
		}
		
	}
//echo '<pre>';print_r($conditii_anulare);echo '</pre>'; 


$array_id_pachet=explode('_',$id_oferta_furnizor);
$id_pachet=$array_id_pachet['1'].'_'.$array_id_pachet['2'];
//echo '<pre>';print_r($rezcam);echo '</pre>'; 
$selectat=find_items($rezcam,'id_pachet',$id_pachet,$selectat);
$array_masa=explode('*',$selectat['0']['masa_neschimbata']);

$servicii_supliemtare_Xml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelServiceTypesRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId> 
     <RequestUser>'.$RequestUser.'</RequestUser>
    <RequestPass>'.$RequestPass.'</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
  </AuditInfo>
  <RequestDetails>
    <getHotelServiceTypesRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
       <TourOpCode>'.$TourOpCode.'</TourOpCode>
      <ProductCode>'.$product_code.'</ProductCode>
      <VariantID>'.$id_oferta_furnizor.'</VariantID>    
      <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
  
    </getHotelServiceTypesRequest>
  </RequestDetails>
</Request>';
//if(!$err_logare_admin) echo "pppp". $servicii_supliemtare_Xml ;

$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $servicii_supliemtare_Xml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

$sResponse_servicii = curl_exec($ch);
$servicii_suplimetareXML = new SimpleXMLElement($sResponse_servicii);
//echo $servicii_suplimetareXML->ResponseDetails->getHotelServiceTypesResponse->Services->asXML();

$ss=0;
if($servicii_suplimetareXML->ResponseDetails->getHotelServiceTypesResponse != '') {
	foreach($servicii_suplimetareXML->ResponseDetails->getHotelServiceTypesResponse->Services->Service as $servicii_suplimentare_xml) {
		//echo (string)$servicii_suplimentare_xml->Service->HasPrice;
		if((string)$servicii_suplimentare_xml->HasPrice=='true') {
		if($selectat['0']['pat_suplimentar']==1 and (string)$servicii_suplimentare_xml->Code!=$cod_pat_suplimentar)	{
		$servicii_suplimentare[$ss]['id_serviciu']=(string)$servicii_suplimentare_xml->Code;	
		$servicii_suplimentare[$ss]['tip']=(string)$servicii_suplimentare_xml->Type;
		$servicii_suplimentare[$ss]['tip_denumire']=(string)$servicii_suplimentare_xml->TypeName;
		$servicii_suplimentare[$ss]['denumire']=(string)$servicii_suplimentare_xml->Name;
		++$ss;
		}
		
		if($selectat['0']['pat_suplimentar']!=1)	{
		$servicii_suplimentare[$ss]['id_serviciu']=(string)$servicii_suplimentare_xml->Code;	
		$servicii_suplimentare[$ss]['tip']=(string)$servicii_suplimentare_xml->Type;
		$servicii_suplimentare[$ss]['tip_denumire']=(string)$servicii_suplimentare_xml->TypeName;
		$servicii_suplimentare[$ss]['denumire']=(string)$servicii_suplimentare_xml->Name;
		++$ss;
		}
		
		
		}
	}
	}

// pretul la servicii suplimentare
$x=0; 



$pax_name='<PaxName PaxType="adult">Adult </PaxName>'	;

foreach($servicii_suplimentare as $servicii_suplimetare_pret){
if($servicii_suplimetare_pret['id_serviciu']!=$cod_pat_suplimentar) {
$servicii_suplimentare_pret_Xml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelServicePriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
      <RequestUser>'.$RequestUser.'</RequestUser>
    <RequestPass>'.$RequestPass.'</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
        <RequestLang>RO</RequestLang>
  </AuditInfo>
  <RequestDetails>
    <getHotelServicePriceRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
      <TourOpCode>'.$TourOpCode.'</TourOpCode>
      <ProductCode>'.$product_code.'</ProductCode>
       <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
     

      <VariantId>'.$id_oferta_furnizor.'</VariantId>    
      <Language>RO</Language>
<Services>      
<Service>
          <ServiceType>'.$servicii_suplimetare_pret['tip'].'</ServiceType>
          <ServiceCode>'.$servicii_suplimetare_pret['id_serviciu'].'</ServiceCode>

          
          
          <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
      <PaxNames>
                  
				'.$pax_name. ' 
                				  
     </PaxNames>
       </Service>
</Services>
  
  </getHotelServicePriceRequest>
</RequestDetails>
</Request>';
 //echo "cerere servicii pret <br/>". $servicii_suplimentare_pret_Xml ;


$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$servicii_suplimentare_pret_Xml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

$sResponse_servicii_pret = curl_exec($ch);
$servicii_suplimetareXML_pret = new SimpleXMLElement($sResponse_servicii_pret);
//echo $servicii_suplimetareXML_pret->asXML();

if($servicii_suplimetareXML_pret->ResponseDetails->getHotelServicePriceResponse != '') {

foreach($servicii_suplimetareXML_pret->ResponseDetails->getHotelServicePriceResponse->Services->Service as $servicii_suplimentare_xml){
	if($servicii_suplimentare_xml->Price>0) {
	

	if(!(in_array((string)$servicii_suplimentare_xml->Name, $array_masa))) {	
	for($a=1;$a<=$adulti;$a++) {	
	$extra_servicii_pret[$x]['denumire']=change_meal((string)$servicii_suplimentare_xml->Name);
	$extra_servicii_pret[$x]['pret']=(string)$servicii_suplimentare_xml->Gross;
	$extra_servicii_pret[$x]['tip_persoana']="Pentru 1 ADULT";
	$extra_servicii_pret[$x]['id']=(string)$servicii_suplimentare_xml->Code;
	$x++;
	}
	}

}
}
}
}
}
//pentru copil daca exista 


for($i=0; $i<$copii; $i++) {
	$pax_name_copil.='<PaxName PaxType="child" ChildAge="'.$copil_age[$i].'">Copil '.$copil_age[$i].'</PaxName>';

foreach($servicii_suplimentare as $servicii_suplimetare_pret){
	//echo '<pre>';print_r($selectat);echo '</pre>'; 

 $servicii_suplimentare_pret_Xml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelServicePriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
     <RequestUser>'.$RequestUser.'</RequestUser>
    <RequestPass>'.$RequestPass.'</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
        <RequestLang>RO</RequestLang>
  </AuditInfo>
  <RequestDetails>
    <getHotelServicePriceRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
      <TourOpCode>'.$TourOpCode.'</TourOpCode>
      <ProductCode>'.$product_code.'</ProductCode>
       <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
     

      <VariantId>'.$id_oferta_furnizor.'</VariantId>    
      <Language>RO</Language>
<Services>      
<Service>
          <ServiceType>'.$servicii_suplimetare_pret['tip'].'</ServiceType>
          <ServiceCode>'.$servicii_suplimetare_pret['id_serviciu'].'</ServiceCode>

          
          
          <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
      <PaxNames>
                  
				'.$pax_name. ' 
                				  
     </PaxNames>
       </Service>
</Services>
  
  </getHotelServicePriceRequest>
</RequestDetails>
</Request>';
 //echo "cerere servicii pret <br/>". $servicii_suplimentare_pret_Xml ;


$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$servicii_suplimentare_pret_Xml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);
$sResponse_servicii_pret = curl_exec($ch);
$servicii_suplimetareXML_pret = new SimpleXMLElement($sResponse_servicii_pret);
//echo $servicii_suplimetareXML_pret->asXML();

if($servicii_suplimetareXML_pret->ResponseDetails->getHotelServicePriceResponse != '') {

foreach($servicii_suplimetareXML_pret->ResponseDetails->getHotelServicePriceResponse->Services->Service as $servicii_suplimentare_xml){
	if($servicii_suplimentare_xml->Price>0) {
	
	if(!(in_array((string)$servicii_suplimentare_xml->Name, $array_masa))) {	
	$extra_servicii_pret[$x]['denumire']=change_meal((string)$servicii_suplimentare_xml->Name);
	$extra_servicii_pret[$x]['pret']=(string)$servicii_suplimentare_xml->Gross;
	$extra_servicii_pret[$x]['tip_persoana']="pentru 1 COPIL ".$copil_age[$i].' ani';
	$extra_servicii_pret[$x]['id']=(string)$servicii_suplimentare_xml->Code;
	$x++;
	}
	}
	}

}

}
}



        

//echo '<pre>';print_r($extra_servicii_pret);echo '</pre>'; 
}



$one_time_only = 'da';
?>