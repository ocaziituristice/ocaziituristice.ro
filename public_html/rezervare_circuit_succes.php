<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');

/*** check login admin ***/
$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

$id_oferta=$_GET['oferta'];
$id_hotel=$_GET['hotel'];
$id_rez=$_GET['rez'];
$id_user=$_GET['usr'];
$GLOBALS['make_vizualizata']='nu';
$det= new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');
$luna=array(1=>'Ianuarie', 2=>'Februarie', 3=>'Martie', 4=>'Aprilie', 5=>'Mai', 6=>'Iunie', 7=>'Iulie', 8=>'August', 9=>'Septembrie', 10=>'Octombrie', 11=>'Noiembrie', 12=>'Decembrie');
$link_oferta_return = substr($sitepath,0,-1).make_link_circuit($detalii_hotel['denumire'], $id_oferta);

$sel_rez="SELECT
cerere_rezervare.comision,
cerere_rezervare.comision_trimis_google,
cerere_rezervare_grad_ocupare.pret,
cerere_rezervare_grad_ocupare.moneda
FROM cerere_rezervare
LEFT JOIN cerere_rezervare_tip_camera ON cerere_rezervare_tip_camera.id_cerere = '".$id_rez."'
LEFT JOIN cerere_rezervare_grad_ocupare ON cerere_rezervare_grad_ocupare.id_camera = cerere_rezervare_tip_camera.id_camera
WHERE cerere_rezervare.id_cerere = '".$id_rez."' ";
$que_rez=mysql_query($sel_rez) or die(mysql_error());
$row_rez=mysql_fetch_array($que_rez);
@mysql_free_result($que_rez);

if($row_rez['comision_trimis_google']=='nu') {
	top_vanzari($id_hotel, '2');
	$ins_cerere_rez = "UPDATE cerere_rezervare SET comision_trimis_google = 'da' WHERE id_cerere = '".$id_rez."' ";
	$rez_cerere_rez = mysql_query($ins_cerere_rez) or die (mysql_error());
}

$comision_google = $row_rez['comision_trimis_google'];

$sel_bnr="SELECT * FROM curs_valutar ";
$que_bnr=mysql_query($sel_bnr) or die(mysql_error());
$row_bnr=mysql_fetch_array($que_bnr);
@mysql_free_result($que_bnr);

$pret_total = final_price_lei($row_rez['pret'], $row_rez['moneda']);
$pret_total_euro = final_price_euro(final_price_lei($row_rez['pret'], $row_rez['moneda']));

$comision = $row_rez['comision'];

$sel_usr="SELECT
cerere_rezervare_date_facturare.oras
FROM useri_fizice
LEFT JOIN cerere_rezervare_date_facturare ON cerere_rezervare_date_facturare.id_cerere = '$id_rez'
WHERE useri_fizice.id_useri_fizice = '$id_user' ";
$que_usr=mysql_query($sel_usr) or die(mysql_error());
$row_usr=mysql_fetch_array($que_usr);
@mysql_free_result($que_usr);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Rezervare circuit <?php echo $detalii['denumire']; ?></title>
<meta name="description" content="" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>

	<div class="NEW-column-full pad20" style="margin-bottom:10px;">
      <h2>Multumim pentru rezervarea efectuata! <span class="green">Care sunt urmatorii pasi?</span></h2>
      <div class="NEW-orange NEW-round8px" style="border:1px solid #ffd0d0; padding:10px 5px;">
        <img src="/images/cerere_oferta.jpg" class="float-left" style="margin:10px;" />
        <div class="float-right" style="margin:0 10px 0 50px; width:260px;">
          <p>Pentru orice nelamuriri sau intrebari nu ezita sa ne contactezi:</p>
          <strong>Telefon:</strong> <?php echo $contact_telefon; ?><br />
          <strong>Mobil:</strong> <?php echo $contact_mobil; ?><br />
          <strong>Program:</strong> <?php echo $contact_program; ?>
        </div>
        <p style="padding-top:17px;" class="black">In scurt timp un agent de turism din echipa <?php echo $denumire_agentie; ?> te va contacta (prin email sau telefon) pentru confirmarea rezervarii tale.</p>
        <p class="black">Iti multumim pentru interesul acordat!</p>
        <br class="clear" />
      </div>
      <?php /*?><div class="fb-like-box" data-href="http://www.facebook.com/OcaziiTuristice.ro" data-width="292" data-show-faces="true" data-stream="false" data-header="false" style="float:right;"></div><?php */?>
      <br /><br />
      <h2 class="red">Spune-le si prietenilor tai despre aceasta oferta</h2>
      <br />
      <img src="<?php echo '/thumb_hotel/'.$detalii_hotel['poza1']; ?>" class="images" style="margin:10px;" />
      <h3 style="margin:10px 10px 0 10px;"><span class="blue"><?php echo $detalii_hotel['denumire']; ?></span> <img src="/images/spacer.gif" class="stele-mari-<?php echo $detalii_hotel['stele']; ?>" alt="numar de stele"> <span class="smaller-09em"><?php echo $detalii['denumire_scurta']; ?></span></h3>
      <div class="grey italic"><?php echo $link_oferta_return; ?></div>
      <div style="float:left; margin:20px 0 0 30px;">
        <a name="fb_share" type="button_count" href="http://www.facebook.com/sharer.php?u=<?php echo $link_oferta_return; ?>" target="_blank" rel="noffolow"><img src="/images/social/facebook-24.png" alt="Facebook" /></a>
        &nbsp;
        <a href="http://twitter.com/home?status=<?php echo $detalii_hotel['denumire'].' '.$detalii['denumire_scurta']; ?>+<?php echo $link_oferta_return; ?>" target="_blank"><img src="/images/social/twitter-24.png" alt="Twitter" /></a>
      </div>
      <br class="clear" />
    </div>

    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
