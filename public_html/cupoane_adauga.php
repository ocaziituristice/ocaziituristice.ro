<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');

/*** check login admin ***/
$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/cupoane.php');
$cupoane = new CUPOANE();
$campaign = $cupoane->decode_link($_GET['hash']);


$coupon_valoare = new_price($campaign['valoare_campanie']).' '.moneda($campaign['moneda_campanie']);
if($campaign['moneda_campanie']=='Procent') $coupon_valoare=round($campaign['valoare_campanie'],0).'%';

$coupon_perioada_end = strtotime($campaign['data_inceput'].' + '.$campaign['nr_zile'].' days');
$coupon_perioada = date('j M Y', strtotime($campaign['data_inceput'])).' - '.date('j M Y', $coupon_perioada_end);

if($campaign['id_tara']==0 and $campaign['id_zona']==0 and $campaign['id_localitate']==0) {
	$coupon_destinatie = 'toate destinatiile';
} else if($campaign['id_localitate']!=0) {
	$coupon_destinatie = $campaign['den_localitate'].' ('.$campaign['den_tara'].')';
	$link_destinatie = '/sejur-'.fa_link($campaign['den_tara']).'/'.fa_link($campaign['den_zona']).'/'.fa_link($campaign['den_localitate']).'/';
} else if($campaign['id_zona']!=0) {
	$coupon_destinatie = $campaign['den_zona'].' ('.$campaign['den_tara'].')';
	$link_destinatie = '/sejur-'.fa_link($campaign['den_tara']).'/'.fa_link($campaign['den_zona']).'/';
} else if($campaign['id_tara']!=0) {
	$coupon_destinatie = $campaign['den_tara'];
	$link_destinatie = '/sejur-'.fa_link($campaign['den_tara']).'/';
}

if(isset($_POST['trimite'])) {
	$ins_cupon = $cupoane->insert_coupon($campaign['id_campanie'], $coupon_perioada_end, $_POST['sex'], $_POST['nume'], $_POST['prenume'], $_POST['email'], $_POST['data_nasterii'], $_POST['telefon']);
	if($ins_cupon['error']==1) {
		$link_succes = $_SERVER['REQUEST_URI'];
		echo '<script>alert("';
		foreach($ins_cupon['mesaje'] as $k_cupon => $v_cupon) {
			echo $v_cupon.'\n';
		}
		echo '"); document.location.href="'.$link_succes.'"; </script>';
	} else if($ins_cupon['error']==0) {
		if($_POST['send_email']=='da') {
			$sel_user = "SELECT useri_fizice.* FROM useri_fizice WHERE id_useri_fizice = '".$ins_cupon['cupon']['id_useri_fizice']."' ";
			$que_user = mysql_query($sel_user) or die(mysql_error());
			$row_user = mysql_fetch_array($que_user);
			
			$GLOBALS['nume_expeditor2'] = $row_user['prenume'].' '.$row_user['nume'];
			$GLOBALS['mail_expeditor'] = $row_user['email'];
			$GLOBALS['cod_cupon'] = $ins_cupon['cupon']['cod_cupon'];
			$GLOBALS['coupon_valoare'] = $coupon_valoare;
			$GLOBALS['coupon_perioada'] = $coupon_perioada;
			$GLOBALS['coupon_destinatie'] = $coupon_destinatie;
			$GLOBALS['coupon_link'] = $campaign['link_redirect'];
			$GLOBALS['sigla'] = $GLOBALS['path_sigla'];

			$send_m = new SEND_EMAIL;
			//email_client
			$param['templates'] = $_SERVER['DOCUMENT_ROOT'].'/mail/templates/inregistrare_cupon.htm';
			$param['subject'] = 'CUPON REDUCERE - '.$denumire_agentie;
			$param['from_email'] = $GLOBALS['email_rezervare'];
			$param['to_email'] = $mail_expeditor;
			$param['to_nume'] = $nume_expeditor2;
			$param['fr_email'] = $GLOBALS['email_rezervare'];
			$param['fr_nume'] = $GLOBALS['denumire_agentie'];
			$send_m->send_rezervari($param);
		}
		
		$coupon = $cupoane->decode_cupon($_COOKIE['cupon']);
		$link_succes = $coupon['link_redirect'];
		echo '<script>alert("Felicitari! Cuponul a fost generat!\nIn scurt timp veti primi pe adresa de email inregistrata un mesaj cu CODUL CUPONULUI!"); document.location.href="'. $campaign['link_redirect'].'"; </script>';
	}
}

?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cupoane Reducere | Ocaziituristice.ro</title>
<meta name="robots" content="noindex, nofollow">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
<link href="/js/jquery_validate/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie">
		<h1 class="blue">Completeaza formularul <br/>si profita de reducere in perioada <span class="red"><?php echo $coupon_perioada; ?></span></h1>
        
        <div class="pad20">
        
          <div class="coupon NEW-round4px">
            <div class="inner NEW-round6px clearfix">
              <div class="discount text-center">
			    <span class="titlu white">DISCOUNT</span>
				<span class="value white"><?php echo $coupon_valoare; ?></span>
              </div>
              <div class="comments black text-center bigger-11em">
                Cuponul de reducere cu valoarea de<br>
                <span class="bigger-12em bold"><?php echo $coupon_valoare; ?></span>
                este valabil in perioada<br>
                <span class="bigger-15em bold"><?php echo $coupon_perioada; ?></span><br>
                pentru ofertele din
                <span class="bigger-12em bold"><?php echo $coupon_destinatie; ?></span><br>
                de pe portalul <span class="blue"><?php echo $denumire_agentie; ?></span>
              </div>
            </div>
          </div>
          
          <div class="coupon-info">
            <div class="titlu bigger-12em">* <span class="underline">Informatii cupon de reducere</span></div>
            <p>Cuponul de reducere este valabil <strong>DOAR dupa completarea campurilor</strong> de mai jos cu datele dumneavoastra. Dupa ce veti completa campurile corect, veti primi un email cu <strong>CODUL CUPONULUI</strong>. Acest cod il puteti folosi <strong>DOAR</strong> la rezervarile on-line, <strong>DOAR</strong> in perioada mentionata.</p>
            <p><strong>ATENTIE!</strong> Valabilitatea cuponului se refera la perioada in care acesta se foloseste, nu perioada sejurului.</p>
            <p>Cuponul de reducere <strong>NU este transmisibil</strong>.</p>
          </div>
          
          <br class="clear"><br>
          
          <div class="NEW-calculeaza bkg-blue">
          <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="coupon-form">
            <div class="clearfix">
              <h2 class="blue underline">Inregistreaza Cupon</h2>

              <div class="item2 clearfix bigger-12em red bold" style="padding:5px 0;">
                <div class="left">&nbsp;</div>
                <div class="right"><label><input type="radio" name="sex" value="m" <?php if($_POST['sex']=='m') { ?> checked="checked" <?php } ?> data-validation-engine="validate[required]"> Domnul</label>
                &nbsp;&nbsp;&nbsp;<label><input type="radio" name="sex" value="f" <?php if($_POST['sex']=='f') { ?> checked="checked" <?php } ?> data-validation-engine="validate[required]"> Doamna</label></div>
              </div>
              
              <div class="float-left" style="width:440px;">
                <div class="item2 clearfix">
                  <div class="left"><label for="nume" class="titlu">* Nume</label></div>
                  <div class="right"><input name="nume" id="nume" type="text" value="<?php if($_POST['nume']) echo $_POST['nume']; ?>" data-validation-engine="validate[required]"></div>
                </div>
                
                <div class="item2 clearfix">
                  <div class="left"><label for="prenume" class="titlu">* Prenume</label></div>
                  <div class="right"><input name="prenume" id="prenume" type="text" value="<?php if($_POST['prenume']) echo $_POST['prenume']; ?>" data-validation-engine="validate[required]"></div>
                </div>
                
                <div class="item2 clearfix">
                  <div class="left"><label for="email" class="titlu">* E-mail</label></div>
                  <div class="right"><input name="email" id="email" type="text" value="<?php if($_POST['email']) echo $_POST['email']; ?>" data-validation-engine="validate[required,custom[email]]"></div>
                </div>
                
                <div class="item2 clearfix">
                  <div class="left"><label for="data_nasterii" class="titlu">* Data nasterii</label></div>
                  <div class="right"><input name="data_nasterii" id="data_nasterii" type="text" value="<?php if($_POST['data_nasterii']) echo $_POST['data_nasterii']; ?>" style="width:120px;"></div>
                </div>

                <div class="item2 clearfix">
                  <div class="left"><label for="telefon" class="titlu">* Telefon</label></div>
                  <div class="right"><input name="telefon" id="telefon" type="text" value="<?php if($_POST['telefon']) echo $_POST['telefon']; ?>" data-validation-engine="validate[required]"></div>
                </div>
                
                <?php if(!$err_logare_admin) { ?>
                <div class="item2 clearfix bkg-green">
                  <div class="left"><label for="send_email" class="titlu">* Trimite email?</label></div>
                  <div class="right"><select name="send_email" id="send_email">
                    <option value="da">DA</option>
                    <option value="nu">NU</option>
                  </select></div>
                </div>
                <?php } else { ?>
                <input name="send_email" id="send_email" type="hidden" value="da">
                <?php } ?>
              </div>
              
              <div class="float-right bigger-14em" style="width:440px;">
                <div class="item2 clearfix">
                  <div class="left2">Discount:</div>
                  <div class="right2 bold"><?php echo $coupon_valoare; ?></div>
                </div>
                <div class="item2 clearfix">
                  <div class="left2">Perioada valabilitate:</div>
                  <div class="right2 bold"><?php echo $coupon_perioada; ?></div>
                </div>
                <div class="item2 clearfix">
                  <div class="left2">Destinatie:</div>
                  <div class="right2 bold"><a href="<?php echo $link_destinatie; ?>" target="_blank" class="link-blue"><?php echo $coupon_destinatie; ?> <img src="/images/icon_external_link.png" alt=""></a></div>
                </div>
              </div>
              
              <br class="clear">
            
              <div class="bigger-11em bold" style="padding:10px 0 0 150px;">
                <label><input name="termeni_conditii" type="checkbox" value="da" <?php if ($_POST['termeni_conditii']=='da') echo 'checked="checked"'; ?> data-validation-engine="validate[required]"> * Sunt de acord cu <a href="<?php echo $sitepath; ?>termeni_si_conditii.html" target="_blank" class="infos2 link-blue" rel="nofollow">termenii si conditiile</a> de utilizare a site-ului.</label>
              </div>
                
              <div style="padding:10px 0 0 150px;">
                <input type="submit" name="trimite" value="Inregistreaza Cupon" class="button-red" onClick="ga('send', 'event', 'pagina cupon', 'inregistreaza', '');" />
              </div>
            </div>
          </form>
          </div>
        
		</div>

	  </div>
    
    <br>
    
    <div class="ofRec NEW-round6px" style="padding:10px 5px !important;">
      <h2 class="tit"><span class="black">Profita de cupon alegand oferte din</span> <a href="<?php echo $link_destinatie; ?>" target="_blank" class="link-blue"><?php echo $coupon_destinatie; ?> <img src="/images/icon_external_link.png" alt=""></a></h2>
    <?php
     $selOf = "SELECT
    oferte.denumire,
    oferte.denumire_scurta,
    oferte.id_oferta,
    oferte.nr_zile,
    oferte.nr_nopti,
    oferte.exprimare_pret,
    oferte.pret_minim,
    oferte.moneda,
    oferte.masa,
    transport.denumire AS denumire_transport,
    hoteluri.nume,
    hoteluri.stele,
    hoteluri.id_hotel,
    hoteluri.tip_unitate,
    hoteluri.poza1,
    zone.denumire as denumire_zona,
    localitati.denumire as denumire_localitate,
    tari.denumire as denumire_tara,
    continente.nume_continent
    FROM oferte
    INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
    INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
    INNER JOIN zone ON localitati.id_zona = zone.id_zona
    INNER JOIN tari ON zone.id_tara = tari.id_tara
    INNER JOIN transport ON transport.id_trans=oferte.id_transport
    LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
    WHERE oferte.valabila = 'da' ";
    if($campaign['id_tara']==0 and $campaign['id_zona']==0 and $campaign['id_localitate']==0) {
    } else if($campaign['id_localitate']!=0) {
        $selOf .= " AND localitati.id_localitate = '".$campaign['id_localitate']."' ";
    } else if($campaign['id_zona']!=0) {
        $selOf .= " AND zone.id_zona = '".$campaign['id_zona']."' ";
    } else if($campaign['id_tara']!=0) {
        $selOf .= " AND tari.id_tara = '".$campaign['id_tara']."' ";
    }
   $selOf .= " GROUP BY oferte.id_hotel ORDER BY oferte.recomandata ASC, hoteluri.oferta_speciala DESC, hoteluri.counter DESC, oferte.ultima_modificare DESC
    LIMIT 0,4 ";
   
    $queOf = mysql_query($selOf) or die(mysql_error());
    
    while($rowOf = mysql_fetch_array($queOf)) {
        if($rowOf['poza1']) $last_pic='/img_mediu_hotel/'.$rowOf['poza1'];
        else $last_pic='/images/no_photo.jpg';
        
        if($rowOf['tip_unitate']=='Circuit') {
            $last_denumire = $rowOf['nume'];
            $last_stele = '';
            $last_localizare = $rowOf['nume_continent'].' / '.$rowOf['nr_zile'].' zile';
            $last_link = '/circuit/'/*.fa_link($rowOf['nume_continent']).'/'*/.fa_link_oferta($last_denumire).'-'.$rowOf['id_oferta'].'.html';
        } else {
            $last_denumire = $rowOf['nume'];
            $last_stele = '<span class="stele-mici-'.$rowOf['stele'].'"></span>';
            $last_localizare = $rowOf['denumire_zona']; if($rowOf['denumire_localitate']<>$localizare) $localizare = $localizare.' / '.$rowOf['denumire_localitate'];
            $last_localizare = $last_localizare.' / '.$rowOf['denumire_tara'];
            $last_link = make_link_oferta($rowOf['denumire_localitate'], $rowOf['nume'], $rowOf['denumire_scurta'], $rowOf['id_oferta']);
        }
    ?>
        <div class="item">
          <a href="<?php echo $last_link; ?>" class="poza" rel="nofollow"><img src="<?php echo $last_pic; ?>" width="220" alt="<?php echo $last_denumire; ?>" /><span class="offer"><?php echo new_price($rowOf['pret_minim']).' '.$rowOf['moneda']; ?></span></a>
          <div class="underpic">
            <div class="titlu"><a href="<?php echo $last_link; ?>" class="link-blue"><?php echo $last_denumire; ?></a> <?php echo $last_stele; ?></div>
            <div class="text-left"><?php echo $rowOf['nr_nopti'].' nopti, '.$rowOf['masa'].', '.$rowOf['denumire_transport']; ?></div>
          </div>
        </div>
    <?php } @mysql_free_result($queOf); ?>
      <br class="clear">
    </div>

    <br><br>
	</div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>

<script src="/js/jquery_validate/jquery.validationEngine-ro.js"></script>
<script src="/js/jquery_validate/jquery.validationEngine.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery("#coupon-form").validationEngine('attach', {promptPosition : "topRight"});
});
function checkHELLO(field, rules, i, options){
	if (field.val() != "HELLO") {
		return options.allrules.validate2fields.alertText;
	}
}
</script>

<script src="/js/datepicker_ro.js"></script>
<script>
$(function() {
	$("#data_nasterii").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0',
		dateFormat: "yy-mm-dd",
		defaultDate: "-18y",
		showButtonPanel: true
	});
});
</script>
</body>
</html>
