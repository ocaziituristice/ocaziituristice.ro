<?php 
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/peste_tot.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );

?>
<?php
$link_article = $_REQUEST['article'];
$link_area = $_REQUEST['link_area'];

$sel_article = "SELECT articles.*
FROM articles
WHERE articles.activ = 'da'
AND articles.link = '".$link_article."'
AND articles.link_area = '".$link_area."'
";
$que_article = mysql_query($sel_article) or die(mysql_error());
$row_article = mysql_fetch_array($que_article);

$id_article = $row_article['id_article'];
$id_tara = $row_article['id_tara'];
$id_zona = $row_article['id_zona'];
$id_localitate = $row_article['id_localitate'];
$categorie = $row_article['categorie'];
$denumire = $row_article['denumire'];
$descriere_scurta = $row_article['descriere_scurta'];
$descriere = $row_article['descriere'];

$den_tara = get_den_tara($id_tara);
$link_tara = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';

if($id_localitate>0) {
	$den_destinatie = get_den_localitate($id_localitate);
	$link_destinatie = '/cazare-'.fa_link(get_den_localitate($id_localitate)).'/';
	
	//if(fa_link(get_den_zona($id_zona))!=fa_link(get_den_tara($id_tara))) $link_destinatie .= fa_link(get_den_zona($id_zona)).'/'.fa_link(get_den_localitate($id_localitate)).'/';
	//if(fa_link(get_den_zona($id_localitate))!=fa_link(get_den_tara($id_zona))) $link_destinatie .= fa_link(get_den_localitate($id_localitate)).'/';
	
	$den_loc = get_den_localitate($id_localitate);
	$check_weather = check_weather($id_tara, $id_zona);
	$link_weather = '/vremea-in-'.fa_link(get_den_zona($id_zona)).'_'.$id_tara.'.html';
} else if($id_zona>0) {
	$den_destinatie = get_den_zona($id_zona);
	$link_destinatie = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';
	if(fa_link(get_den_zona($id_zona))!=fa_link(get_den_tara($id_tara))) $link_destinatie .= fa_link(get_den_zona($id_zona)).'/';
	$den_zona = get_den_zona($id_zona);
	$check_weather = check_weather($id_tara, $id_zona);
	$link_weather = '/vremea-in-'.fa_link(get_den_zona($id_zona)).'_'.$id_tara.'.html';
} else if($id_tara>0) {
	$den_destinatie = get_den_tara($id_tara);
	$link_destinatie = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';
	$den_tara = get_den_tara($id_tara);
	$check_weather = check_weather($id_tara);
	$link_weather = '/vremea-in-'.fa_link(get_den_tara($id_tara)).'.html';
}
?>



<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
	
<title><?php echo $denumire; ?></title>
<?php
	
	require_once( "includes/header/header_responsive.php" );
?>
</head>

<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header/admin_bar_responsive.php'); ?>

	<?php // Header ?>
	<header>
		<?php require( "includes/header/meniu_header_responsive.php" ); ?>
	</header>

	<?php // Breadcrumbs and general search ?>
	<div class="layout">
    		<?php $watermark_cautare = 'Cauta hoteluri din ' . $den_tara;?>

		<?php //require( "includes/header/breadcrumb_responsive_intreg.php" ); ?>

		<?php require( "includes/header/breadcrumb_responsive.php" ); ?>
		<?php require( "includes/header/search_responsive.php" ); ?>    
    </div>

	<div class="layout">
		<div id="payment">
			<span class="titlu_modala"><h1><?php echo $denumire; ?> </h1></span>
		
        
			<div class="text-justify pad10 article">
	<!-- de aici  continutul-->			
  
           <div class="pad10 article bigger-12em text-justify">
            <?php echo htmlspecialchars_decode($descriere); ?>
          </div>

           <h2 class="blue">Alte informatii utile despre <?php echo $den_destinatie; ?></h2>   
       <div class="location-filters">
                <ul>      
         <?php if($den_destinatie!=$den_tara) { ?><li><a href="<?php echo $link_tara; ?>" title="Sejur <?php echo $den_tara; ?>">Sejur <?php echo $den_tara; ?></a></li><?php } ?>
              
                <li><a href="<?php echo $link_destinatie; ?>" title="Sejur <?php echo $den_destinatie; ?>">Sejur <?php echo $den_destinatie; ?></a></li>

                <?php if($check_weather==1) { ?><li><a href="<?php echo $link_weather; ?>" title="Vremea in <?php echo $den_destinatie; ?>">Vremea <?php echo $den_destinatie; ?></a></li><?php } ?>    
             
          </ul>
          </div>   
       
  
  <?php $sel_categorii = "SELECT articles.categorie
        FROM articles
        WHERE articles.activ = 'da'
		AND articles.id_article <> '".$id_article."' ";
        if($id_tara>0) $sel_categorii = $sel_categorii." AND articles.id_tara = '".$id_tara."' ";
        if($id_zona>0) $sel_categorii = $sel_categorii." AND articles.id_zona = '".$id_zona."' ";
        if($id_localitate>0) $sel_categorii = $sel_categorii." AND articles.id_localitate = '".$id_localitate."' ";
        $sel_categorii = $sel_categorii." GROUP BY articles.categorie ";
        $que_categorii = mysql_query($sel_categorii) or die(mysql_error());
        while($row_categorii = mysql_fetch_array($que_categorii)) { ?>
          <br>
          <div >
           
              <h2 class="blue"><?php echo $row_categorii['categorie'].' '.$den_destinatie; ?></h2>
              <div class="location-filters"><ul>
              <?php $sel_alte_articole = "SELECT articles.denumire, articles.link, articles.link_area
			  FROM articles
			  WHERE articles.activ = 'da'
			  AND articles.id_article <> '".$id_article."'
			  AND articles.categorie = '".$row_categorii['categorie']."' ";
			  if($id_tara>0) $sel_alte_articole = $sel_alte_articole." AND articles.id_tara = '".$id_tara."' ";
			  if($id_zona>0) $sel_alte_articole = $sel_alte_articole." AND articles.id_zona = '".$id_zona."' ";
			  if($id_localitate>0) $sel_alte_articole = $sel_alte_articole." AND articles.id_localitate = '".$id_localitate."' ";
			  $sel_alte_articole = $sel_alte_articole." ORDER BY articles.id_article DESC ";
			  $que_alte_articole = mysql_query($sel_alte_articole) or die(mysql_error());
			  while($row_alte_articole = mysql_fetch_array($que_alte_articole)) {
				  echo '<li><a href="/ghid-turistic-'.$row_alte_articole['link_area'].'/'.$row_alte_articole['link'].'.html" title="'.$row_alte_articole['denumire'].'">'.$row_alte_articole['denumire'].'</a></li>';
			  } ?>
            </ul> 
            </div>
          </div>
		<?php } ?>     
       
       
             
			</div>
		</div>
      <?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/oferte_vizualizate_responsive.php" ); ?>  
        
	</div>

	<?php // Footer ?>
    <?php require_once( "includes/newsletter_responsive.php" ); ?>

	<?php require_once( "includes/footer/footer_responsive.php" ); ?>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

</body>
</html>
