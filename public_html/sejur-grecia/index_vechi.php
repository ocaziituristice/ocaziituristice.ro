<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$den_tara = 'Grecia';
$id_tara = get_id_tara($den_tara);

$link_tara = fa_link_oferta($den_tara);

if(str_replace("/sejur-","",$_SERVER['REQUEST_URI'])==$link_tara."/") $img_path = 'images/';
	else $img_path = '/sejur-'.$link_tara.'/images/';

$sel_dest = "SELECT
MIN(oferte.pret_minim / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
COUNT(oferte.id_oferta) AS numar_hoteluri,
zone.id_zona,
zone.denumire AS denumire_zona,
zone.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '".$id_tara."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_zona
ORDER BY numar DESC, denumire_zona ASC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['numar'][] = $row_dest['numar'];
	$destinatie['link'][] = '/sejur-'.fa_link($den_tara).'/'.fa_link($row_dest['denumire_zona']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$row_dest['id_zona'].'/';
	$destinatie['id_zona'][] = $row_dest['id_zona'];
	$destinatie['info_scurte'][] = $row_dest['info_scurte'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
} @mysql_free_result($rez_dest);
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Sejur Grecia 2017,oferte cazare Grecia 2017 | Ocaziituristice.ro</title>
<meta name="description" content="Sejur Grecia 2017, oferte cazare Grecia, verifica disponibilitatea locurilor in timp real doar la  Ocaziituristice.ro" />
<link rel="canonical" href="<?php echo '/sejur-'.$link_tara.'/'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Sejur Grecia (cazare +transport avion), oferte cazare Grecia 2017</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?>

        <br class="clear">
        <?php /*?><div class="Hline"></div>
        
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
        
        <br class="clear"><?php */?>
        
		<?php $tipuri = get_tiputi_oferte('', '', '', $id_tara, '', '');
		$link_new = $link_tara.'/'; ?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/sejur-'.$link_new; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		if(sizeof($tipuri)>0) {
			foreach($tipuri as $key_t=>$value_t) {
				$i++;
				$valTip = fa_link($value_t['denumire']);
				if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire']; ?></a></li>
		<?php }
		} ?>
          
          
         
         
        </ul>

        <?php if(sizeof($destinatie['den_zona'])>0) { ?>
  
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie['den_zona'] as $key1 => $value1) { ?>
          <div class="destItem NEW-round4px clearfix" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
            <div class="topBox">
              <img src="<?php echo $img_path.fa_link($destinatie['den_zona'][$key1]); ?>.jpg" alt="<?php echo $destinatie['den_zona'][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie['den_zona'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?><div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><?php } ?>
            <div class="bottomBox clearfix">
              <div class="price" align="center"><span class="red"><span class="pret"><?php echo $destinatie['numar'][$key1]; ?> </span></span> oferte sejur <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="oferte sejur <?php echo $destinatie['den_zona'][$key1]; ?>"><?php echo $destinatie['den_zona'][$key1]; ?></a></span></span></div>
             
            </div>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        
        <br>
		
        <div class="pad10">
          <img src="<?php echo $img_path; ?>icon-sejur-grecia.jpg" alt="Sejur Grecia" class="float-left" style="margin:10px 10px 0 0;">
          <h2><strong>Sejur Grecia</strong> - pentru vacanțe de neuitat</h2>
          <p class="newText text-justify">Cei mai mulţi turişti care optează pentru un <em>sejur Grecia</em> se reîntorc an de an. Farmecul acestei zone, diversitatea culturală şi frumuseţea pitorească a peisajelor sunt greu de egalat de alte destinaţii turistice din Europa. Grecia este una dintre puţinele destinaţii de vacanţă cu o astfel de bogăţie şi frumuseţe ce pot fi vizitate inclusiv de persoanele pentru care vacanţa înseamnă întotdeauna buget restrâns şi mare atenţie la cheltuieli.</p>
          
          <br class="clear">
          
          <p class="newText text-justify">În materie de oferte, <strong>Grecia</strong> se numără printre cele mai economice destinaţii din Europa, iar pentru cei care aleg să îşi plănuiască vacanţa din timp, absolut totul, de la biletele de avion sau de autocar până la cazare se încadrează cu uşurinţă chiar şi în cele mai austere bugete de călătorie. Farmecul Greciei stă întocmai în diversitatea culturală a acestei zone şi în numeroasele opţiuni pe care le are de oferit pentru toate tipurile de turişti: numeroase obiective turistice din patrimoniul UNESCO, o bogată cultură locală, numeroase comori nedescoperite şi o diversitate de relief ce transformă un sejur Grecia în cea mai intensă şi bogată experienţă de vacanţă.</p>
          <p class="newText text-justify">Ospitalitatea grecească este bine cunoscută în toată lumea, iar acest detaliu uimeşte şi încântă turiştii încă de la primul contact. Indiferent că este vorba de personalul din hoteluri Grecia, de ospătarii de la restaurante sau de sătenii din pitoreştile zone rurale greceşti, aici turistul se simte mereu ca acasă, mereu bine venit, iar dorinţa de a te reîntoarce curând apare chiar imediat.</p>
          <p class="newText text-justify">În funcţie de buget şi preferinţe, turiştii pot opta pentru diverse tipuri de sejururi, de la cele în <a href="/sejur-grecia/?optiuni=da&masa=all-inclusive" class="link-blue" title="Grecia all inclusive">hoteluri all inclusive Grecia</a> la pensiunile mediteraneene tipice, de la călătorii la mare, la adevărate aventuri care rămân vii în amintire ani la rândul. Indiferent de tipul de pachet ales, o vacanţă în Grecia va însemna întotdeauna mâncare excelentă, peisaje de vis, vreme superbă şi o cultură locală care îşi lasă amprenta, ca o ştampilă pe paşaport, în inima fiecărui turist în parte.</p>
        </div>

        <br class="clear">
        
       <?php /*?> <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <h3 class="mar10"><?php echo $den_tara; ?> - sejururi și cazări disponibile</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php $zone_cazari = array("halkidiki", "paralia katerini", "riviera olimpului", "thassos");
		  foreach($destinatie['den_zona'] as $key2 => $value2) {
			  $value2 = str_replace("Insula ", "", $value2);
			  if(in_array(strtolower($value2), $zone_cazari)) $part_anchor = 'Cazare ';
			  	else $part_anchor = 'Sejur ';
		  ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie['link'][$key2]; ?>" title="<?php echo $part_anchor.$value2; ?>" class="link-blue block"><?php echo $part_anchor.$value2; ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?><?php */?>
        
        <br class="clear">
        
        <div class="pad10"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?></div>
        
        <br class="clear"><br>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>