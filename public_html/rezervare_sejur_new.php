<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');

/*** check login admin ***/
$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

$id_oferta=$_GET['oferta'];
$id_hotel=$_GET['hotel'];
$GLOBALS['make_vizualizata']='nu';
$de_afisat=array();
$det= new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');
$luna=array(1=>'Ianuarie', 2=>'Februarie', 3=>'Martie', 4=>'Aprilie', 5=>'Mai', 6=>'Iunie', 7=>'Iulie', 8=>'August', 9=>'Septembrie', 10=>'Octombrie', 11=>'Noiembrie', 12=>'Decembrie');

$tip_unitate = $detalii_hotel['tip_unitate'];
if($tip_unitate=='Circuit') {
	$link_oferta_return = $sitepath.'circuit/'/*.fa_link($detalii_hotel['nume_continent']).'/'*/.fa_link_oferta($detalii['denumire']).'-'.$id_oferta.'.html';
} else {
	$link_oferta_return = $sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii['denumire']).'-'.$id_oferta.'.html';
}
//$link_oferta_return = $sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii['denumire']).'-'.$id_oferta.'.html';
$hotel_denumire = $detalii_hotel['denumire'];
$hotel_stele = $detalii_hotel['stele'];

if(isset($_POST['trimite']) and ($_POST['trimite']=='pas1')) {
	$err=0;
	$err_nume='';
	if(strlen(trim($_POST['prenume']))<3) { ++$err; $err_prenume='Campul "Prenume" este obligatoriu!'; }
	if(strlen(trim($_POST['nume']))<3) { ++$err; $err_nume='Campul "Nume" este obligatoriu!'; }
	if(strlen(trim($_POST['email']))<5) { ++$err; $err_email='Campul "E-mail" este obligatoriu!'; }
	if(validate_email($_POST['email'])==false) { ++$err; $err_email='Adresa de email completata nu este corecta!'; }
	if(strlen(trim($_POST['telefon']))<4) { ++$err; $err_tel='Campul "Telefon" este obligatoriu!'; }
	if($err_logare_admin) { if((trim($_POST['ziua'])=='') or (trim($_POST['luna'])=='') or (trim($_POST['an'])=='')) { ++$err; $err_birth='"Data nasterii" nu este completata corect!'; } }
	if(strlen(trim($_POST['data_start1']))<10) { ++$err; $err_data='Campul "Data inceperii sejurului" este obligatoriu!'; }
	if(trim($_POST['nr_nopti'])=='') { ++$err; $err_nopti='Campul "Nr. nopti" este obligatoriu!'; }
	
	$data_adaugarii = date('Y-m-d H:i:s');
	$session_id = session_id();
	
	if(!$err) {
		$ins_useri_fizice = "INSERT INTO useri_fizice SET
		nume='".ucwords(strtolower($_POST['nume']))."',
		prenume='".ucwords(strtolower($_POST['prenume']))."',
		email='".$_POST['email']."',
		telefon='".$_POST['telefon']."',
		sex='".$_POST['sex']."',
		data_nasterii='".$_POST['an']."-".$_POST['luna']."-".$_POST['ziua']."',
		data_adaugarii='$data_adaugarii',
		session_id='$session_id'
		";
		$rez_ins_useri_fizice = mysql_query($ins_useri_fizice) or die (mysql_error());
		
		$sel_useri_fizice = "SELECT id_useri_fizice FROM useri_fizice WHERE session_id='$session_id' AND email='".$_POST['email']."' ORDER BY id_useri_fizice DESC";
		$que_useri_fizice = mysql_query($sel_useri_fizice) or die(mysql_error());
		$row_useri_fizice = mysql_fetch_array($que_useri_fizice);
		
		if(sizeof($detalii['early_time'])>0) $early_booking='da'; else $early_booking='nu';
		
		if($tip_unitate=='Circuit') {
			$hotel_tara = $detalii_hotel['nume_continent'];
			$hotel_localitate = '';
			$hotel_denumire = '';
		} else {
			$hotel_tara = $detalii_hotel['tara'];
			$hotel_localitate = $detalii_hotel['localitate'];
			$hotel_denumire = $detalii_hotel['denumire'];
		}
		
		if($detalii['nr_zile']='2') {
			$numar_zile = $_POST['nr_nopti']+1;
		} else {
			$numar_zile = $detalii['nr_zile'];
		}
		
		$ins_cerere_rezervare = "INSERT INTO cerere_rezervare SET
		id_oferta = '$id_oferta',
		id_useri_fizice = '".$row_useri_fizice['id_useri_fizice']."',
		data = '".$_POST['data_start1']."',
		nr_nopti = '".$_POST['nr_nopti']."',
		data_adaugarii = '$data_adaugarii',
		nr_zile = '".$numar_zile."',
		oferta_denumire = '".$detalii['denumire']."',
		hotel_denumire = '".$hotel_denumire."',
		hotel_categorie = '".$detalii_hotel['stele']."',
		hotel_localitate = '".$hotel_localitate."',
		hotel_tara = '".$hotel_tara."',
		tip_masa = '".$detalii['masa']."',
		transport = '".$detalii['transport']."',
		early_booking = '".$early_booking."',
		conditii_anulare = '".$detalii['conditii_anulare']."',
		stare = 'neterminata',
		tip = 'rezervare',
		adaugat_admin = '".$_SESSION['nume']."',
		session_id = '$session_id',
		ip = '".$_SERVER['REMOTE_ADDR']."'
		";
		$rez_cerere_rezervare = mysql_query($ins_cerere_rezervare) or die (mysql_error());
		
		$sel_cerere_rezervare = "SELECT id_cerere FROM cerere_rezervare WHERE session_id='$session_id' AND id_useri_fizice='".$row_useri_fizice['id_useri_fizice']."' ORDER BY id_useri_fizice DESC";
		$que_cerere_rezervare = mysql_query($sel_cerere_rezervare) or die(mysql_error());
		$row_cerere_rezervare = mysql_fetch_array($que_cerere_rezervare);
		
		if(!$err_logare_admin) {
			$actiune_log = $_SESSION['nume'].' a introdus rezervarea cu id-ul '.$row_cerere_rezervare['id_cerere'];
			$ins_logadmin = "INSERT INTO log_admin SET id_admin='".$_SESSION['id_user_adm']."', data=NOW(), actiune='$actiune_log'";
			$rez_logadmin = mysql_query($ins_logadmin) or die (mysql_error());
		}
		
		redirect_php($sitepath.'rezervare_sejur_new.php?hotel='.$id_hotel.'&oferta='.$id_oferta.'&usr='.$row_useri_fizice['id_useri_fizice'].'&rez='.$row_cerere_rezervare['id_cerere'].'&pas=2#pas2');
	}

} //if($_POST['trimite'])

if(isset($_POST['trimite']) and ($_POST['trimite']=='pas2')) {
	$err=0;
	$id_cerere = $_POST['id_cerere'];
	$id_usr = $_POST['id_usr'];
	//print_r($_POST['nr_camere']);

	$err_cam = 0;
	foreach($_POST['nr_camere'] as $key => $value) {
		if(!empty($value)) $err_cam = $err_cam+1;
	}
	if($err_cam=='0') { ++$err; $err_nr_camere='Va rugam selectati cel putin 1 camera!'; }
	
	//if(empty($_POST['nr_camere'])) { ++$err; $err_nr_camere='Va rugam selectati cel putin 1 camera!'; }
	
	if(!$err) {
		$nr_camere = $_POST['nr_camere'];
		$id_camere = $_POST['id_camere'];
		$denumire_camere = $_POST['denumire_camere'];
		
		for($i=0; $i<count($_POST['id_camere']); $i++) {
			$sel_cerere_rez_tip_cam = "SELECT id_camera FROM cerere_rezervare_tip_camera WHERE id_cerere='".$id_cerere."' AND id_camera_hotel='".$id_camere[$i]."'";
			$que_cerere_rez_tip_cam = mysql_query($sel_cerere_rez_tip_cam) or die(mysql_error());
			$row_cerere_rez_tip_cam = mysql_fetch_array($que_cerere_rez_tip_cam);
			$total_cerere_rez_tip_cam = mysql_num_rows($que_cerere_rez_tip_cam);
				
			if(($total_cerere_rez_tip_cam=='0') and ($nr_camere[$i]>0)) {
				$ins_cerere_rez_tip_cam = "INSERT INTO cerere_rezervare_tip_camera SET id_cerere='".$id_cerere."', id_camera_hotel='".$id_camere[$i]."', nume_camera='".$denumire_camere[$i]."', nr_camera='".$nr_camere[$i]."'";
				$rez_cerere_rez_tip_cam = mysql_query($ins_cerere_rez_tip_cam) or die (mysql_error());
			} else {
				if($nr_camere[$i]>0) {
					$upd_cerere_rez_tip_cam = "UPDATE cerere_rezervare_tip_camera SET nr_camera='".$nr_camere[$i]."' WHERE id_cerere='".$id_cerere."' AND id_camera_hotel='".$id_camere[$i]."'";
					$rez_cerere_rez_tip_cam = mysql_query($upd_cerere_rez_tip_cam) or die (mysql_error());
				} else {
					$del_cerere_rez_tip_cam = "DELETE FROM cerere_rezervare_tip_camera WHERE id_cerere='".$id_cerere."' AND id_camera_hotel='".$id_camere[$i]."'";
					$rez_cerere_rez_tip_cam = mysql_query($del_cerere_rez_tip_cam) or die (mysql_error());
				}
			}
			
			redirect_php($sitepath.'rezervare_sejur_new.php?hotel='.$id_hotel.'&oferta='.$id_oferta.'&usr='.$id_usr.'&rez='.$id_cerere.'&pas=3#pas3');
		}
	}
} //if($_POST['trimite'])

if(isset($_POST['trimite']) and ($_POST['trimite']=='pas3')) {
	$err=0;
	$id_cerere = $_POST['id_cerere'];
	$id_usr = $_POST['id_usr'];
	
	$sel1_cerere_rez_tip_cam = "SELECT * FROM cerere_rezervare_tip_camera WHERE id_cerere='".$id_cerere."'";
	$que1_cerere_rez_tip_cam = mysql_query($sel1_cerere_rez_tip_cam) or die(mysql_error());
	$j=0;
	while($row1_cerere_rez_tip_cam = mysql_fetch_array($que1_cerere_rez_tip_cam)) {
		$idu_camera = $row1_cerere_rez_tip_cam['id_camera_hotel'];
		for($i=0; $i<$row1_cerere_rez_tip_cam['nr_camera']; $i++) {
			$j++;
			if(trim($_POST['grad_ocupare_cam'.$j])=='') { ++$err; $err_grad_ocup[$j]='Va rugam alegeti gradul de ocupare in camera!'; }
		}
	}

	if(!$err) {
		$que1_cerere_rez_tip_cam = mysql_query($sel1_cerere_rez_tip_cam) or die(mysql_error());
		$i1=0;
		while($row1_cerere_rez_tip_cam = mysql_fetch_array($que1_cerere_rez_tip_cam)) {
			$idu_camera = $row1_cerere_rez_tip_cam['id_camera_hotel'];
			for($i2=0; $i2<$row1_cerere_rez_tip_cam['nr_camera']; $i2++) {
				$i1++;
				$grad_oc_cam = explode(';',$_POST['grad_ocupare_cam'.$i1]);
				$ins_cerere_rez_grad_ocup = "INSERT INTO cerere_rezervare_grad_ocupare (`id_grad_ocupare`,`id_camera`,`adulti`,`copii`,`copil1`,`copil2`,`copil3`,`pret`,`moneda`) VALUES ('','$grad_oc_cam[0]','$grad_oc_cam[1]','$grad_oc_cam[2]','$grad_oc_cam[3]','$grad_oc_cam[4]','$grad_oc_cam[5]','$grad_oc_cam[6]','$grad_oc_cam[7]')";
				$rez_cerere_rez_grad_ocup = mysql_query($ins_cerere_rez_grad_ocup) or die (mysql_error());
			}
		}
		
		redirect_php($sitepath.'rezervare_sejur_new.php?hotel='.$id_hotel.'&oferta='.$id_oferta.'&usr='.$id_usr.'&rez='.$id_cerere.'&pas=4#pas4');
	}
} //if($_POST['trimite'])

if(isset($_POST['trimite']) and ($_POST['trimite']=='pas4')) {
	$err=0;
	$id_cerere = $_POST['id_cerere'];
	$id_usr = $_POST['id_usr'];

	$sel_tip_cam = "SELECT * FROM cerere_rezervare_tip_camera WHERE id_cerere='".$id_cerere."'";
	$que_tip_cam = mysql_query($sel_tip_cam) or die(mysql_error());
	$i1=0;
	while($row_tip_cam = mysql_fetch_array($que_tip_cam)) {
		$id_cam = $row_tip_cam['id_camera'];
		$nume_cam = $row_tip_cam['nume_camera'];
		$nr_cam = $row_tip_cam['nr_camera'];
		
		$sel_grad_ocup = "SELECT * FROM cerere_rezervare_grad_ocupare WHERE id_camera='".$id_cam."'";
		$que_grad_ocup = mysql_query($sel_grad_ocup) or die(mysql_error());
		while($row_grad_ocup = mysql_fetch_array($que_grad_ocup)) {
			$i1++;
			for($i4=1; $i4<=$row_grad_ocup['adulti']; $i4++) {
				if(($_POST['nume_ad'.$i1.'_'.$i4]=='') or ($_POST['prenume_ad'.$i1.'_'.$i4]=='')) { ++$err; $err_pasageri='Toate campurile sunt obligatorii!'; }
			}
			for($i5=1; $i5<=$row_grad_ocup['copii']; $i5++) {
				if(($_POST['nume_copil'.$i1.'_'.$i5]=='') or ($_POST['prenume_copil'.$i1.'_'.$i5]=='') or ($_POST['data_copil'.$i1.'_'.$i5]=='')) { ++$err; $err_pasageri='Toate campurile sunt obligatorii!'; }
			}
		}
	}
	
	if($_POST['modalitate_plata']=='') { ++$err; $err_modalitate_plata='Va rugam alegeti o modalitate de plata!'; }
	if($_POST['termeni_conditii']=='') { ++$err; $err_termeni_conditii='Pentru a finaliza rezervarea trebuie sa fiti de acord cu termenii si conditiile site-ului!'; }
	
	if($_POST['facturare']=='persoana_fizica') {
		if($_POST['denumire']=='') { ++$err; $err_denumire='Campul "Prenume si Nume" este obligatoriu!'; }
		if($_POST['adresa']=='') { ++$err; $err_adresa='Campul "Adresa" este obligatoriu!'; }
		if($_POST['oras']=='') { ++$err; $err_oras='Campul "Oras" este obligatoriu!'; }
	}
	if($_POST['facturare']=='persoana_juridica') {
		if($_POST['denumire']=='') { ++$err; $err_denumire='Campul "Denumire societate" este obligatoriu!'; }
		if($_POST['cui_cnp']=='') { ++$err; $err_cui_cnp='Campul "CUI" este obligatoriu!'; }
		if($_POST['nr_reg_comert']=='') { ++$err; $err_nr_reg_comert='Campul "Nr. Reg. Comert" este obligatoriu!'; }
		if($_POST['adresa']=='') { ++$err; $err_adresa='Campul "Adresa" este obligatoriu!'; }
		if($_POST['oras']=='') { ++$err; $err_oras='Campul "Oras" este obligatoriu!'; }
	}
	
	if(!$err) {

		$sel_tip_cam = "SELECT * FROM cerere_rezervare_tip_camera WHERE id_cerere='".$id_cerere."'";
		$que_tip_cam = mysql_query($sel_tip_cam) or die(mysql_error());
		$i1=0;
		while($row_tip_cam = mysql_fetch_array($que_tip_cam)) {
			$id_cam = $row_tip_cam['id_camera'];
			$nume_cam = $row_tip_cam['nume_camera'];
			$nr_cam = $row_tip_cam['nr_camera'];
			
			$sel_grad_ocup = "SELECT * FROM cerere_rezervare_grad_ocupare WHERE id_camera='".$id_cam."'";
			$que_grad_ocup = mysql_query($sel_grad_ocup) or die(mysql_error());
			while($row_grad_ocup = mysql_fetch_array($que_grad_ocup)) {
				$i1++;
				for($i4=1; $i4<=$row_grad_ocup['adulti']; $i4++) {
					$sex_ad[$i4] = trim($_POST['sex_ad'.$i1.'_'.$i4]);
					$nume_ad[$i4] = ucwords(strtolower(trim($_POST['nume_ad'.$i1.'_'.$i4])));
					$prenume_ad[$i4] = ucwords(strtolower(trim($_POST['prenume_ad'.$i1.'_'.$i4])));
					$data_ad[$i4] = trim($_POST['data_ad'.$i1.'_'.$i4]);
					if(($data_ad[$i4]=='') or ($data_ad[$i4]=='0000-00-00')) $data_ad[$i4]=NULL;
					echo '<br />';
					
					$ins_pasageri1 = "INSERT INTO pasageri (`id_rezervare`,`id_grad_ocupare`,`sex`,`nume`,`prenume`,`data_nasterii`) VALUES ('".$id_cerere."','".$row_grad_ocup['id_grad_ocupare']."','$sex_ad[$i4]','$nume_ad[$i4]','$prenume_ad[$i4]','$data_ad[$i4]')";
					$res_pasageri1 = mysql_query($ins_pasageri1) or die (mysql_error());
				}
				for($i5=1; $i5<=$row_grad_ocup['copii']; $i5++) { 
					$sex_copil[$i5] = trim($_POST['sex_copil'.$i1.'_'.$i5]);
					$nume_copil[$i5] = ucwords(strtolower(trim($_POST['nume_copil'.$i1.'_'.$i5])));
					$prenume_copil[$i5] = ucwords(strtolower(trim($_POST['prenume_copil'.$i1.'_'.$i5])));
					$data_copil[$i5] = trim($_POST['data_copil'.$i1.'_'.$i5]);echo '<br />';
					
					$ins_pasageri2 = "INSERT INTO pasageri (`id_rezervare`,`id_grad_ocupare`,`sex`,`nume`,`prenume`,`data_nasterii`) VALUES ('".$id_cerere."','".$row_grad_ocup['id_grad_ocupare']."','$sex_copil[$i5]','$nume_copil[$i5]','$prenume_copil[$i5]','$data_copil[$i5]')";
					$res_pasageri2 = mysql_query($ins_pasageri2) or die (mysql_error());
				}
			}
		}
		
		$upd_rezervare = "UPDATE cerere_rezervare SET modalitate_plata='".$_POST['modalitate_plata']."', observatii='".$_POST['observatii']."', stare='noua' WHERE id_cerere='".$id_cerere."'";
		$res_rezervare = mysql_query($upd_rezervare) or die (mysql_error());

		if($_POST['facturare']=='persoana_fizica') {
			$facturare_tip_persoana = 'persoana_fizica';
			$facturare_denumire = $_POST['denumire'];
			$facturare_cui_cnp = $_POST['cui_cnp'];
			$facturare_nr_reg_comert = $_POST['nr_reg_comert'];
			$facturare_banca = $_POST['banca'];
			$facturare_iban = $_POST['iban'];
			$facturare_adresa = $_POST['adresa'];
			$facturare_oras = $_POST['oras'];
		} elseif($_POST['facturare']=='persoana_juridica') {
			$facturare_tip_persoana = 'persoana_juridica';
			$facturare_denumire = $_POST['denumire'];
			$facturare_cui_cnp = $_POST['cui_cnp'];
			$facturare_nr_reg_comert = $_POST['nr_reg_comert'];
			$facturare_banca = $_POST['banca'];
			$facturare_iban = $_POST['iban'];
			$facturare_adresa = $_POST['adresa'];
			$facturare_oras = $_POST['oras'];
		}
		
		$ins_facturare = "INSERT INTO cerere_rezervare_date_facturare (`id_cerere`,`tip_persoana`,`denumire`,`cui_cnp`,`nr_reg_comert`,`banca`,`iban`,`adresa`,`oras`) VALUES ('$id_cerere','$facturare_tip_persoana','$facturare_denumire','$facturare_cui_cnp','$facturare_nr_reg_comert','$facturare_banca','$facturare_iban','$facturare_adresa','$facturare_oras')";
		$res_facturare = mysql_query($ins_facturare) or die (mysql_error());

		//campuri email
		$sel_user = "SELECT useri_fizice.nume, useri_fizice.prenume, useri_fizice.email, useri_fizice.sex FROM useri_fizice INNER JOIN cerere_rezervare ON cerere_rezervare.id_useri_fizice=useri_fizice.id_useri_fizice WHERE cerere_rezervare.id_cerere='".$id_cerere."'";
		$que_user = mysql_query($sel_user) or die(mysql_error());
		$row_user = mysql_fetch_array($que_user);

		if($row_user['sex']=='m') $tit = 'Domnul '; elseif($row_user['sex']=='f') $tit = 'Doamna ';
		$GLOBALS['nume_expeditor2'] = $row_user['prenume'].' '.$row_user['nume'];
		$GLOBALS['nume_expeditor'] = $tit.$nume_expeditor2;
		$GLOBALS['mail_expeditor'] = $row_user['email'];
		$GLOBALS['oferta_denumire'] = $detalii['denumire'];
		$GLOBALS['oferta_id'] = $id_oferta;
		$GLOBALS['oferta_link'] = $link_oferta_return;
		$GLOBALS['rezervare_id'] = 'OCZ'.$id_cerere;
		$GLOBALS['sigla'] = $GLOBALS['path_sigla'];
		$GLOBALS['hotel_denumire'] = $hotel_denumire;
		$GLOBALS['hotel_stele'] = $hotel_stele;

		$send_m= new SEND_EMAIL;
		//email_agentie
		$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/rezervare_new.htm';
		$param['subject']='REZERVARE '.$oferta_denumire;
		$param['from_email']=$GLOBALS['email_rezervare'];
		$param['to_email']=$GLOBALS['email_rezervare'];
		$param['to_nume']=$GLOBALS['denumire_agentie'];
		$param['fr_email']=$mail_expeditor;
		$param['fr_nume']=$nume_expeditor2;
		$send_m->send_rezervari($param);
		
		if(!$err_logare_admin) {} else {
		//email_client
		$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/raspuns_automat_rezervare_new.htm';
		$param['subject']='REZERVARE '.$hotel_denumire.' '.$hotel_stele.'* - '.$GLOBALS['denumire_agentie'];
		$param['from_email']=$GLOBALS['email_rezervare'];
		$param['to_email']=$mail_expeditor;
		$param['to_nume']=$nume_expeditor2;
		$param['fr_email']=$GLOBALS['email_rezervare'];
		$param['fr_nume']=$GLOBALS['denumire_agentie'];
		$send_m->send_rezervari($param);
		}
		
		redirect_php($sitepath.'rezervare_sejur_succes.php?hotel='.$id_hotel.'&oferta='.$id_oferta.'&usr='.$id_usr.'&rez='.$id_cerere);
	}

} //if($_POST['trimite'])

/*if(!$err) {
	$date_time=date("Y-m-d G:i:s");
	$nume=$_POST['nume'];
	$email=$_POST['email'];
	$nr_adulti=$_POST['nr_adulti'];
	$nr_copii=$_POST['nr_copii'];
	$mr_tel=$_POST['telefon'];
	$camera=$_POST['camera'];
	$nr_nopti=$_POST['nr_nopti'];
	$perioada=$_POST['perioada'];
	$observatii=$_POST['observatii'];
	$prenume=$_POST['prenume'];
	$data_nasterii=$_POST['an'].'-'.$_POST['luna'].'-'.$_POST['ziua'];
	$data_nasterii_afisare=$_POST['ziua'].' '.$luna[$_POST['luna']].' '.$_POST['an'];
	if(sizeof($_POST['varsta'])>0) {
		foreach($_POST['varsta'] as $key=>$value) $varsta=$varsta.'Copil '.$key.' ani impliniti '.$value.'<br/>';  
	}
	foreach($_POST['asigurare'] as $key1=>$vale1) $asigurare=$asigurare.$vale1.',';
	
	$sol="INSERT INTO rezervari SET
	id_oferta = '".$id_oferta."',
	camera = '".$camera."',
	sex = '".$_POST['sex']."',
	nume = '".$nume."',
	prenume = '".$prenume."',
	email = '".$email."',
	telefon = '".$mr_tel."',
	nr_adulti = '".$nr_adulti."',
	nr_copii = '".$nr_copii."',
	nr_nopti = '".$nr_nopti."',
	asigurare = '".$asigurare."',
	perioada = '".$perioada."',
	observatii = '".$observatii."',
	data_adaugarii = '".$date_time."',
	ip = '".$_SERVER['REMOTE_ADDR']."',
	copii_varsta = '".$varsta."',
	data_nasterii = '".$data_nasterii."',
	data_nasterii_afisare = '".$data_nasterii_afisare."'
	";
	
	if($que=mysql_query($sol) or die(mysql_error())) {
		if($_POST['sex']=='m') $tit='Domnul '; else $tit='Doamna ';
		$GLOBALS['nume']=$tit.$nume.' '.$_POST['prenume'];
		$GLOBALS['email']=$email;
		$GLOBALS['data_ora']=$date_time;
		$GLOBALS['denumire_oferta']=$detalii['denumire'];
		$GLOBALS['data_nasterii_afisare']=$data_nasterii_afisare;
		$GLOBALS['link']=$sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link($detalii['denumire']).'-'.$id_oferta.'.html';
		$GLOBALS['perioada']=$perioada;
		$GLOBALS['nr_adulti']=$nr_adulti;
		if($nr_copii) {
			if($nr_copii>1) $t='copii'; else $t='copil';
			$GLOBALS['nr_copii']=' si <b>'.$nr_copii.'</b> '.$t.'<br/>'.$varsta;
		} else $GLOBALS['nr_copii']='';
		$GLOBALS['telefon']=$mr_tel;
		$GLOBALS['observatii']=$observatii;
		$GLOBALS['sigla']=$GLOBALS['path_sigla'];
		$send_m= new SEND_EMAIL;
		
		//raspuns_client
		$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/rezervare.htm';
		$param['subject']='Rezervare '.$detalii['denumire'];
		$param['to_email']=$email;
		$param['to_nume']=$nume.' '.$_POST['prenume'];
		$param['fr_email']=$GLOBALS['email_rezervare'];
		$param['fr_nume']=$GLOBALS['denumire_agentie'];
		$send_m->send($param);
		
		//email_agentie
		$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/mail_agentie_rezervare.htm';
		$param['subject']='Rezervare '.$detalii['denumire'];
		$param['to_email']=$GLOBALS['email_rezervare'];
		$param['to_nume']=$GLOBALS['denumire_agentie'];
		$param['fr_email']=$email;
		$param['fr_nume']=$nume.' '.$_POST['prenume'];
		$send_m->send($param);
	}//if($que=mysql_query($sol) or die(mysql_error()))
} //if(!$err)*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Rezervare <?php echo $detalii['denumire']; ?></title>
<meta name="description" content="Fa o rezervare pentru <?php echo $detalii['denumire']; ?> din <?php echo $detalii_hotel['localitate'].', '.$detalii_hotel['tara']; ?>" />
<link rel="canonical" href="<?php echo $sitepath.'hoteluri/'.fa_link_oferta($detalii_hotel['tara']).'/'.fa_link_oferta($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'-'.$id_hotel.'.html"'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">

      <div id="NEW-destinatie">
      
      <h1 class="red">Rezervare <?php echo $detalii['denumire']; ?></h1>
        
      <div class="NEW-column-left1">
      
        <div class="clearfix" style="padding:0 10px;">
          <img src="<?php echo $sitepath; ?>thumb_hotel/<?php echo $detalii_hotel['poza1']; ?>" width="100" alt="" class="images" style="margin-right:10px;" />
          <h2 class="blue"><?php echo $detalii_hotel['denumire']; ?> <img src="<?php echo $imgpath; ?>/spacer.gif" class="stele-mici-<?php echo $detalii_hotel['stele']; ?>" alt="" /></h2>
          <div class="float-right"><a href="<?php echo $link_oferta_return; ?>" class="link-blue"title="Inapoi la oferta">&laquo; inapoi la oferta</a></div>
		  <?php if($detalii_hotel['tip_unitate']!='Circuit') { ?><p><?php echo $detalii_hotel['tara']; ?> / <?php echo $detalii_hotel['zona']; ?> / <?php echo $detalii_hotel['localitate']; ?></p><?php } ?>
            <?php if($detalii['nr_zile']>2 || $detalii['nr_nopti']>1) { ?><p><strong>Durata <?php if(isset($detalii['nr_zile'])) { if($detalii['nr_zile']>1) echo $detalii['nr_zile'].' zile'; else echo $detalii['nr_zile'].' zi'; } ?> / <?php if(isset($detalii['nr_nopti'])) { if($detalii['nr_nopti']>1) echo $detalii['nr_nopti'].' nopti'; else echo $detalii['nr_nopti'].' noapte'; } ?></strong></p><?php } ?>
          <p><strong>Transport <?php echo $detalii['transport']; ?></strong></p>
          <div class="cod-oferta"><strong class="grey">Cod Oferta:</strong> <span class="blue"><?php echo 'OSE'.$id_oferta; ?></span></div>
          <p><strong>Masa <?php echo $detalii['masa']; ?></strong></p>
        </div>
        
        <br class="clear" /><br />
            
        <?php if(!isset($_GET['pas'])) {
            include_once($_SERVER['DOCUMENT_ROOT']."/includes/rezervare/rezervare_sejur_new.php");
        } elseif(isset($_GET['pas']) and ($_GET['pas']=='2')) {
            include_once($_SERVER['DOCUMENT_ROOT']."/includes/rezervare/rezervare_sejur_new_pas2.php");
        } elseif(isset($_GET['pas']) and ($_GET['pas']=='3')) {
            include_once($_SERVER['DOCUMENT_ROOT']."/includes/rezervare/rezervare_sejur_new_pas3.php");
        } elseif(isset($_GET['pas']) and ($_GET['pas']=='4')) {
            include_once($_SERVER['DOCUMENT_ROOT']."/includes/rezervare/rezervare_sejur_new_pas4.php");
        } elseif(isset($_GET['pas']) and ($_GET['pas']=='5')) {
            include_once($_SERVER['DOCUMENT_ROOT']."/includes/rezervare/rezervare_sejur_new_pas5.php");
        }
        ?>
  
      </div>
      
      <div class="NEW-column-right1">
      <?php if(isset($_GET['pas']) and ($_GET['pas']=='5')) {
		  include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/facebook.php");
	  } else { ?>
		<img src="<?php echo $imgpath; ?>/call_center_rezervari.jpg" alt="Call center Rezervari" />
        <?php /*?><p class="bigger-11em bold black">Te putem ajuta cu ceva? Dorești mai multe informații despre rezervare? Sună acum la:</p><?php */?>
		<p style="font-size:3.6em; line-height:1em; padding-left:5px;" class="red bold"><?php echo $contact_telefon2; ?></p>
        <div class="chenar-info NEW-orange NEW-round8px bigger-11em" style="margin-right:10px;"><strong>Atenție!</strong> Trimiterea acestei rezervări nu implică nici o obligație financiară din partea dumneavoastră!</div>
        <br />
        <div class="chn-green NEW-round8px bigger-11em black" style="margin-right:10px;">
          <p><strong class="red bigger-13em underline">Cum fac o rezervare?</strong></p>
          <p><strong class="blue bigger-12em underline">Pasul 1</strong> - Completezi datele tale de contact, data plecării și nr. de nopți</p>
          <p><strong class="blue bigger-12em underline">Pasul 2</strong> - Alegi câte camere dorești din fiecare tip disponibil</p>
          <p><strong class="blue bigger-12em underline">Pasul 3</strong> - Selectezi gradul de ocupare în fiecare cameră aleasă (nr. adulți, copii)</p>
          <p><strong class="blue bigger-12em underline">Pasul 4</strong> - Introduci datele fiecărui turist, date necesare pentru efectuarea rezervării la hotel,  alegi modalitatea de plată dorită, si introduci datele de facturare (necesare încheierii contractului și emiterii facturii)</p>
          <p><strong class="blue bigger-12em underline">Pasul 5</strong> - Cererea ta de rezervare s-a înregistrat și în scurt timp un operator OcaziiTuristice.ro te va contacta pentru <strong class="red">confirmarea disponibilității locurilor</strong> și pentru eventuale detalii suplimentare.</p>
          <p class="bold bigger-11em">Îți mulțumim pentru interesul acordat!</p>
        </div>
	  <?php } ?>
      <br />
      </div>
      
      <br class="clear" />
      
      </div>
      
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>

<?php if(!isset($_GET['pas'])) { ?>
<script type="text/javascript">
/*var availableDates = ["9-5-2012","14-5-2012","15-5-2012"];*/
var availableDates = [<?php echo $data_calendar; ?>];

function available(date) {
  dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
  if ($.inArray(dmy, availableDates) < 0) {
    return [false];
  } else {
    return [true];
  }
}

$('#data_start1').datepicker({
	numberOfMonths: 3,
	dateFormat: "yy-mm-dd",
	<?php if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') {} else { ?>
	beforeShowDay: available,
	<?php } ?>
	showButtonPanel: true,
	minDate: new Date(<?php echo $st[0].', '.$st[1].' - 1, '.$st[2]; ?>),
	maxDate: new Date(<?php echo $en[0].', '.$en[1].' - 1, '.$en[2]; ?>)
});
</script>
<?php } ?>

<?php if(isset($_GET['pas']) and ($_GET['pas']=='4')) { ?>
<script src="<?php echo $sitepath; ?>js/placeholder.js"></script>
<script type="text/javascript">
$(function(){
	$(':input[placeholder]').placeholder();
});
</script>

<script type="text/javascript">
<?php if(!isset($_POST['facturare'])) { ?>
$("#facturare").load("<?php echo $sitepath; ?>includes/rezervare/rezervare_sejur_new_facturare_1.php");
<?php } ?>
$(".persoana_fizica").click(function() {
	$("#facturare").load("<?php echo $sitepath; ?>includes/rezervare/rezervare_sejur_new_facturare_1.php");
});
$(".persoana_juridica").click(function() {
	$("#facturare").load("<?php echo $sitepath; ?>includes/rezervare/rezervare_sejur_new_facturare_2.php");
});
</script>

<script type="text/javascript" src="/js/datepicker_ro.js"></script>
<script type="text/javascript">
$(function() {
<?php for($i1_=1; $i1_<=$i1; $i1_++) { for($i4_=1; $i4_<=$i4; $i4_++) { ?>
  $("#data_ad<?php echo $i1_.'_'.$i4_; ?>").datepicker({
	  changeMonth: true,
	  changeYear: true,
	  yearRange: '-100:+0',
	  dateFormat: "yy-mm-dd",
	  defaultDate: "-18y",
	  showButtonPanel: true
  });
<?php } } ?>
<?php for($i1_=1; $i1_<=$i1; $i1_++) { for($i5_=1; $i5_<=$i5; $i5_++) { ?>
  $("#data_copil<?php echo $i1_.'_'.$i5_; ?>").datepicker({
	  changeMonth: true,
	  changeYear: true,
	  yearRange: '-14:+0',
	  dateFormat: "yy-mm-dd",
	  showButtonPanel: true
  });
<?php } } ?>
});
</script>
<?php } ?>
</body>
</html>
