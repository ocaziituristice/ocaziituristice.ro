<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');

/*** check login admin ***/
$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

$id_oferta=$_GET['oferta'];
$id_hotel=$_GET['hotel'];
$GLOBALS['make_vizualizata']='nu';
$det= new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');
$luna=array(1=>'Ianuarie', 2=>'Februarie', 3=>'Martie', 4=>'Aprilie', 5=>'Mai', 6=>'Iunie', 7=>'Iulie', 8=>'August', 9=>'Septembrie', 10=>'Octombrie', 11=>'Noiembrie', 12=>'Decembrie');

$tip_unitate = $detalii_hotel['tip_unitate'];
if($tip_unitate=='Circuit') {
	$link_oferta_return = $sitepath.'circuit/'/*.fa_link($detalii_hotel['nume_continent']).'/'*/.fa_link_oferta($detalii['denumire']).'-'.$id_oferta.'.html';
} else {
	$den_loc = $detalii_hotel['localitate'];
	$den_hotel = $detalii_hotel['denumire'];
	if($id_oferta=='') {
		$id_offer = NULL;
		$den_scurta_oferta = NULL;
		$ins_denumire = $detalii_hotel['denumire'];
	} else {
		$id_offer = $id_oferta;
		$den_scurta_oferta = $detalii['denumire_scurta'];
		$ins_denumire = $detalii['denumire'];
	}
	$link_oferta_return = make_link_oferta($den_loc, $den_hotel, $den_scurta_oferta, $id_offer);
}
//$link_oferta_return = $sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii['denumire']).'-'.$id_oferta.'.html';

if(isset($_POST['trimite']) and ($_POST['trimite']=='pas1')) {
	$err=0;
	$err_nume='';
	if(strlen(trim($_POST['prenume']))<3) { ++$err; $err_prenume='Campul "Prenume" este obligatoriu!'; }
	if(strlen(trim($_POST['nume']))<3) { ++$err; $err_nume='Campul "Nume" este obligatoriu!'; }
	if(strlen(trim($_POST['email']))<5) { ++$err; $err_email='Campul "E-mail" este obligatoriu!'; }
	if(validate_email($_POST['email'])==false) { ++$err; $err_email='Adresa de email completata nu este corecta!'; }
	if(!$err_logare_admin) {} else {
	if(strlen(trim($_POST['telefon']))<4) { ++$err; $err_tel='Campul "Telefon" este obligatoriu!'; }
	
		if((trim($_POST['ziua'])=='') or (trim($_POST['luna'])=='') or (trim($_POST['an'])=='')) { ++$err; $err_birth='"Data nasterii" nu este completata corect!'; }
	}
	if(strlen(trim($_POST['data_start1']))<10) { ++$err; $err_data='Campul "Data inceperii sejurului" este obligatoriu!'; }
	if(trim($_POST['nr_nopti'])=='') { ++$err; $err_nopti='Campul "Nr. nopti" este obligatoriu!'; }
	if(strlen(trim($_POST['nr_adulti']))<1) { ++$err; $err_adulti='Campul "Nr. adulti" este obligatoriu!'; }
	if(trim($_POST['scop_cerere'])=='') { ++$err; $err_scop_cerere='Va rugam alegeti scopul inregistrarii cererii!'; }
	if(($_POST['scop_cerere']=='Alte motive, pe care le completez in campul de mai jos') and (trim($_POST['observatii'])=='')) { ++$err; $err_observatii='Campul "Observatii" este obligatoriu!'; }
	
	$data_adaugarii = date('Y-m-d H:i:s');
	$session_id = session_id();
	if(sizeof($_POST['varsta'])>0) {
		foreach($_POST['varsta'] as $key=>$value) $varsta = $varsta.'Copil '.$key.', <strong>'.$value.' ani</strong><br/>';  
	}
	
	if(!$err) {
		$id_usr = insert_user($_POST['sex'], $_POST['nume'], $_POST['prenume'], $_POST['email'], $_POST['an']."-".$_POST['luna']."-".$_POST['ziua'], $_POST['telefon'], 'cerere detalii');
		
		$hoteluri_similare='';
		if($_POST['hotel_sim5']) $hoteluri_similare.=$_POST['hotel_sim5'].',';
		if($_POST['hotel_sim4']) $hoteluri_similare.=$_POST['hotel_sim4'].',';
		if($_POST['hotel_sim3']) $hoteluri_similare.=$_POST['hotel_sim3'].',';
		if($_POST['hotel_sim2']) $hoteluri_similare.=$_POST['hotel_sim2'].',';
		
		if(sizeof($detalii['early_time'])>0) $early_booking='da'; else $early_booking='nu';
		
		$ins_cerere_rezervare = "INSERT INTO cerere_rezervare SET
		id_oferta = '".$id_oferta."',
		id_furnizor = '".$detalii['furnizor']."',
		id_useri_fizice = '".$id_usr."',
		data = '".$_POST['data_start1']."',
		nr_nopti = '".$_POST['nr_nopti']."',
		nr_adulti = '".$_POST['nr_adulti']."',
		nr_copii = '".$_POST['nr_copii']."',
		varste_copii = '".$varsta."',
		data_adaugarii = '$data_adaugarii',
		scop_cerere = '".$_POST['scop_cerere']."',
		observatii = '".$_POST['observatii']."',
		hoteluri_similare = '".$hoteluri_similare."',
		oferta_denumire = '".$ins_denumire."',
		tip_masa = '".$detalii['masa']."',
		transport = '".$detalii['transport']."',
		early_booking = '".$early_booking."',
		stare = 'noua',
		tip = 'cerere_oferta',
		adaugat_admin = '".$_SESSION['nume']."',
		session_id = '$session_id',
		ip = '".$_SERVER['REMOTE_ADDR']."'
		";
		$rez_cerere_rezervare = mysql_query($ins_cerere_rezervare) or die (mysql_error());
		
		$sel_cerere_rezervare = "SELECT id_cerere FROM cerere_rezervare WHERE session_id = '".$session_id."' AND id_useri_fizice = '".$id_usr."' ORDER BY id_useri_fizice DESC";
		$que_cerere_rezervare = mysql_query($sel_cerere_rezervare) or die(mysql_error());
		$row_cerere_rezervare = mysql_fetch_array($que_cerere_rezervare);
		
		$id_cerere = $row_cerere_rezervare['id_cerere'];
		
		if(!$err_logare_admin) {
			$actiune_log = $_SESSION['nume'].' a introdus cererea cu id-ul '.$id_cerere;
			$ins_logadmin = "INSERT INTO log_admin SET id_admin='".$_SESSION['id_user_adm']."', data=NOW(), actiune='".$actiune_log."' ";
			$rez_logadmin = mysql_query($ins_logadmin) or die (mysql_error());
		}
		

		//campuri email
		if($_POST['sex']=='m') $tit = 'Domnul '; else $tit = 'Doamna ';
		$GLOBALS['nume_expeditor2'] = $_POST['prenume'].' '.$_POST['nume'];
		$GLOBALS['nume_expeditor'] = $tit.$nume_expeditor2;
		$GLOBALS['mail_expeditor'] = $_POST['email'];
		$GLOBALS['telefon_expeditor'] = $_POST['telefon'];
		$GLOBALS['data_nasterii_afisare'] = $_POST['an']."-".$_POST['luna']."-".$_POST['ziua'];
		$GLOBALS['hotel'] = $detalii_hotel['denumire'];
		if($tip_unitate=='Circuit') {
			$GLOBALS['localizare'] = $detalii_hotel['nume_continent'];
			$GLOBALS['numar_stele'] = '';
		} else {
			$GLOBALS['localizare'] = $detalii_hotel['tara'];
			if($detalii_hotel['zona']!=$detalii_hotel['tara']) $GLOBALS['localizare'] .= ' / '.$detalii_hotel['zona'];
			if($detalii_hotel['localizare']!=$detalii_hotel['zona']) $GLOBALS['localizare'] .= ' / '.$detalii_hotel['localitate'];
			$GLOBALS['numar_stele'] = '- '.$detalii_hotel['stele'].' stele';
		}
		$GLOBALS['data_plecarii'] = date('d.M.Y',strtotime($_POST['data_start1'])).' ('.$_POST['data_start1'].')';
		$GLOBALS['nr_nopti'] = $_POST['nr_nopti'];
		$GLOBALS['nr_adulti'] = $_POST['nr_adulti'];
		if($_POST['nr_copii']) {
			$GLOBALS['nr_copii'] = $_POST['nr_copii'];
			$GLOBALS['varsta'] = $varsta;
		} else {
			$GLOBALS['nr_copii'] = '-';
		}
		$GLOBALS['link'] = $link_oferta_return;
		$GLOBALS['sigla'] = $GLOBALS['path_sigla'];
		$GLOBALS['scop_cerere'] = $_POST['scop_cerere'];
		if($hoteluri_similare!='') $GLOBALS['hoteluri_sim'] = '<tr><td align="left" valign="top">Hoteluri similare:</td><td align="left" valign="top">'.$hoteluri_similare.'</td></tr>';
		if($_POST['observatii']) { $GLOBALS['observatii'] = nl2br($_POST['observatii']); } else { $GLOBALS['observatii'] = '-'; }
		//$GLOBALS['email_contact']='daniel@ocaziituristice.ro';
if(!$err_logare_admin) {} else {
		$send_m= new SEND_EMAIL;

		//email agentie
		$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/cere_detalii.htm';
		$param['subject']='CERERE OFERTA '.$detalii['denumire'];
		/*$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/verificare_disponibilitate.htm';
		$param['subject']='VERIFICARE DISPONIBILITATE '.$detalii['denumire'];*/
		$param['to_email']=$GLOBALS['email_rezervare'];
		$param['to_nume']=$GLOBALS['denumire_agentie'];
		$param['fr_email']=$_POST['email'];
		$param['fr_nume']=$nume_expeditor2;
		$send_m->send($param);
}
		if($detalii_hotel['tip_unitate']=='Circuit') {
			$link_succes = make_link_circuit($detalii_hotel['denumire'], $id_oferta, 'da').'cerere-succes-'.$id_hotel.'_'.$id_oferta.'_'.$id_usr.'_'.$id_cerere;
		} else {
			if($id_oferta=='') $id_oferta = '0';
			$link_succes = make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'cerere-succes-'.$id_hotel.'_'.$id_oferta.'_'.$id_usr.'_'.$id_cerere;
		}
		redirect_php($link_succes);
	}

/*if(!$err) {
	$date_time=date("Y-m-d G:i:s");
	$nume=$_POST['nume'];
	$email=$_POST['email'];
	$nr_adulti=$_POST['nr_adulti'];
	$nr_copii=$_POST['nr_copii'];
	$mr_tel=$_POST['telefon'];
	$camera=$_POST['camera'];
	$nr_nopti=$_POST['nr_nopti'];
	$perioada=$_POST['perioada'];
	$observatii=$_POST['observatii'];
	$prenume=$_POST['prenume'];
	$data_nasterii=$_POST['an'].'-'.$_POST['luna'].'-'.$_POST['ziua'];
	$data_nasterii_afisare=$_POST['ziua'].' '.$luna[$_POST['luna']].' '.$_POST['an'];
	if(sizeof($_POST['varsta'])>0) {
		foreach($_POST['varsta'] as $key=>$value) $varsta=$varsta.'Copil '.$key.' ani impliniti '.$value.'<br/>';  
	}
	foreach($_POST['asigurare'] as $key1=>$vale1) $asigurare=$asigurare.$vale1.',';
	
	$sol="INSERT INTO rezervari SET
	id_oferta = '".$id_oferta."',
	camera = '".$camera."',
	sex = '".$_POST['sex']."',
	nume = '".$nume."',
	prenume = '".$prenume."',
	email = '".$email."',
	telefon = '".$mr_tel."',
	nr_adulti = '".$nr_adulti."',
	nr_copii = '".$nr_copii."',
	nr_nopti = '".$nr_nopti."',
	asigurare = '".$asigurare."',
	perioada = '".$perioada."',
	observatii = '".$observatii."',
	data_adaugarii = '".$date_time."',
	ip = '".$_SERVER['REMOTE_ADDR']."',
	copii_varsta = '".$varsta."',
	data_nasterii = '".$data_nasterii."',
	data_nasterii_afisare = '".$data_nasterii_afisare."'
	";
	
	if($que=mysql_query($sol) or die(mysql_error())) {
		if($_POST['sex']=='m') $tit='Domnul '; else $tit='Doamna ';
		$GLOBALS['nume']=$tit.$nume.' '.$_POST['prenume'];
		$GLOBALS['email']=$email;
		$GLOBALS['data_ora']=$date_time;
		$GLOBALS['denumire_oferta']=$detalii['denumire'];
		$GLOBALS['data_nasterii_afisare']=$data_nasterii_afisare;
		$GLOBALS['link']=$sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link($detalii['denumire']).'-'.$id_oferta.'.html';
		$GLOBALS['perioada']=$perioada;
		$GLOBALS['nr_adulti']=$nr_adulti;
		if($nr_copii) {
			if($nr_copii>1) $t='copii'; else $t='copil';
			$GLOBALS['nr_copii']=' si <b>'.$nr_copii.'</b> '.$t.'<br/>'.$varsta;
		} else $GLOBALS['nr_copii']='';
		$GLOBALS['telefon']=$mr_tel;
		$GLOBALS['observatii']=$observatii;
		$GLOBALS['sigla']=$GLOBALS['path_sigla'];
		$send_m= new SEND_EMAIL;
		
		//raspuns_client
		$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/rezervare.htm';
		$param['subject']='Rezervare '.$detalii['denumire'];
		$param['to_email']=$email;
		$param['to_nume']=$nume.' '.$_POST['prenume'];
		$param['fr_email']=$GLOBALS['email_rezervare'];
		$param['fr_nume']=$GLOBALS['denumire_agentie'];
		$send_m->send($param);
		
		//email_agentie
		$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/mail_agentie_rezervare.htm';
		$param['subject']='Rezervare '.$detalii['denumire'];
		$param['to_email']=$GLOBALS['email_rezervare'];
		$param['to_nume']=$GLOBALS['denumire_agentie'];
		$param['fr_email']=$email;
		$param['fr_nume']=$nume.' '.$_POST['prenume'];
		$send_m->send($param);
	}//if($que=mysql_query($sol) or die(mysql_error()))
} //if(!$err)*/

} //if($_POST['trimite'])

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cerere detalii pentru <?php echo $detalii['denumire']; ?></title>
<meta name="description" content="Inregistreaza o cerere pentru mai multe detalii pentru <?php echo $detalii['denumire']; ?> din <?php echo $detalii_hotel['localitate'].', '.$detalii_hotel['tara']; ?>" />
<link rel="canonical" href="<?php echo $sitepath.'hoteluri/'.fa_link_oferta($detalii_hotel['tara']).'/'.fa_link_oferta($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'-'.$id_hotel.'.html"'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">


<div id="NEW-destinatie">

<h1 class="red"><span class="black">Cerere detalii pentru</span> <?php echo str_replace($detalii_hotel['denumire'],'<span class="blue">'.$detalii_hotel['denumire'].'</span>',$detalii["denumire"]); ?></h1>
  
<div class="NEW-column-left1">

  <div class="clearfix" style="padding:0 10px;">
    <img src="<?php echo $sitepath; ?>thumb_hotel/<?php echo $detalii_hotel['poza1']; ?>" width="100" alt="" class="images" style="margin-right:10px;" />
    <h2 class="blue"><?php echo $detalii_hotel['denumire']; ?> <img src="<?php echo $imgpath; ?>/spacer.gif" class="stele-mici-<?php echo $detalii_hotel['stele']; ?>" alt="" /></h2>
    <div class="float-right pad5-0"><a href="<?php echo $link_oferta_return; ?>" class="link-blue" title="Inapoi la oferta">&laquo; inapoi la oferta</a></div>
    <?php if($detalii_hotel['tip_unitate']!='Circuit') { ?><p class="pad5-0"><?php echo $detalii_hotel['tara']; ?> / <?php echo $detalii_hotel['zona']; ?> / <?php echo $detalii_hotel['localitate']; ?></p><?php } ?>
    <?php if($id_oferta!='') { ?>
		<?php if($detalii['nr_nopti']>1) { ?><p class="pad5-0"><strong>Durata <?php if(isset($detalii['nr_nopti'])) { if($detalii['nr_nopti']>1) echo $detalii['nr_nopti'].' nopti'; else echo $detalii['nr_nopti'].' noapte'; } ?></strong></p><?php } ?>
	    <p class="pad5-0"><strong>Transport <?php echo $detalii['transport']; ?></strong></p>
		<div class="cod-oferta"><strong class="grey">Cod Oferta:</strong> <span class="blue"><?php echo 'OSE'.$id_oferta; ?></span></div>
		<p class="pad5-0"><strong>Masa <?php echo $detalii['masa']; ?></strong></p>
	<?php } else { ?>
    	<br />
        <div class="cod-oferta"><strong class="grey">Cod Hotel:</strong> <span class="blue"><?php echo 'OHT'.$id_hotel; ?></span></div>
    <?php } ?>
  </div>
  
  <br class="clear" /><br />
  
  <form action="" method="post" name="contactForm">
  <h2 class="black">Inregistreaza o cerere pentru mai multe detalii la <span class="green"><?php echo $detalii_hotel['denumire']; ?></span></h2>
  
  <div style="padding-left:15px;">Toate campurile marcate cu <strong class="red">*</strong> sunt obligatorii!</div>

  <div class="NEW-rezervare NEW-round8px clearfix">
    <a href="<?php echo $link_oferta_return; ?>" title="Inapoi la oferta"><span class="cancel float-right"></span></a>
    <h3 class="green">Datele dumneavoastra de contact</h3>
    <div class="item clearfix">
      <div class="left">&nbsp;</div>
      <div class="right"><label><input type="radio" name="sex" value="m" <?php if(!$_POST['sex'] || $_POST['sex']=='m') { ?> checked="checked" <?php } ?>/> <strong class="red">Domnul</strong></label>
      &nbsp;&nbsp;&nbsp;<label><input type="radio" name="sex" value="f" <?php if($_POST['sex']=='f') { ?> checked="checked" <?php } ?> /> <strong class="red">Doamna</strong></label></div>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="prenume" class="titlu">* Prenume</label></div>
      <div class="right"><input name="prenume" id="prenume" type="text" value="<?php if($_POST['prenume']) echo $_POST['prenume']; ?>" class="big" />
      <?php if($err_prenume) { ?><label class="error"><?php echo $err_prenume; ?></label><?php } ?></div>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="nume" class="titlu">* Nume</label></div>
      <div class="right"><input name="nume" id="nume" type="text" value="<?php if($_POST['nume']) echo $_POST['nume']; ?>" class="big" />
      <?php if($err_nume) { ?><label class="error"><?php echo $err_nume; ?></label><?php } ?></div>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="email" class="titlu">* E-mail</label></div>
      <div class="right"><input name="email" id="email" type="text" value="<?php if($_POST['email']) echo $_POST['email']; ?>" class="big" />
      <?php if($err_email) { ?><label class="error"><?php echo $err_email; ?></label><?php } ?></div>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="telefon" class="titlu">* Telefon</label></div>
      <div class="right"><input name="telefon" id="telefon" type="text" value="<?php if($_POST['telefon']) echo $_POST['telefon']; ?>" class="big" />
      <?php if($err_tel) { ?><label class="error"><?php echo $err_tel; ?></label><?php } ?></div>
    </div>
    <div class="item clearfix">
      <div class="left"><label class="titlu">* Data nasterii</label></div>
      <div class="right">
        &nbsp;&nbsp; Zi <select name="ziua"><option value="" selected="selected">--</option><?php for($i=1;$i<=31;$i++) { echo "<option value='$i'"; if($_POST['ziua']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
        Luna <select name="luna"><option value="" selected="selected">--</option><?php for($i=1;$i<=12;$i++) { echo "<option value='$i'"; if($_POST['luna']==$i) echo "selected='selected'"; echo ">".$luna[$i]."</option>"; } ?></select>
        An <select name="an"><option value="" selected="selected">--</option><?php for($i=date("Y")-18;$i>=1930;$i--) { echo "<option value='$i'"; if($_POST['an']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
        <?php if($err_birth) { ?><br /><label class="error"><?php echo $err_birth; ?></label><?php } ?>
      </div>
    </div>
    <h3 class="green">Informatii suplimentare</h3>
    <?php if(!$err_logare_admin) { ?>
    <div class="item clearfix bold">
      <div class="left"><label for="data_start1" class="titlu">Hoteluri similare</label></div>
      <div class="right">
        <label><input type="checkbox" name="hotel_sim5" value="de 5*" /> de 5*</label>
        <label><input type="checkbox" name="hotel_sim4" value="de 4*" /> de 4*</label>
        <label><input type="checkbox" name="hotel_sim3" value="de 3*" /> de 3*</label>
        <label><input type="checkbox" name="hotel_sim2" value="de 2*" /> de 2*</label>
      </div>
    </div>
    <?php } ?>
    <div class="item clearfix">
      <div class="left">
        <label for="data_start1" class="titlu">* Data inceperii sejurului</label></div>
      <div class="right">
<?php
if($detalii['valabila']=='da') {
	$data_calendar='';
	foreach($preturi['data_start_normal'] as $key_d=>$data_start) {
	  $data_calendar .= '"'.date('j-n-Y',strtotime($data_start)).'",';
	}
	$data_calendar=substr($data_calendar,0,-1);
	$st = explode('-', $preturi['min_start']);
	$en = explode('-', $preturi['max_end']);
}
?>
        <input type="text" id="data_start1" name="data_start1" value="<?php if($_POST['data_start1']) echo $_POST['data_start1']; ?>" readonly="readonly" />
       <?php if($err_data) { ?><br /><label class="error"><?php echo $err_data; ?></label><?php } ?>
      </div>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="nr_nopti" class="titlu">* Nr. nopti</label></div>
      <div class="right">
        <?php if($detalii['nr_nopti']>1) { ?>
        <input type="text" name="nr_nopti" id="nr_nopti" class="small" value="<?php echo $detalii['nr_nopti']; ?>" readonly="readonly" />
        <?php } else { ?>
        <input type="text" name="nr_nopti" id="nr_nopti" class="small" value="<?php if($_POST['nr_nopti']) echo $_POST['nr_nopti']; ?>" />
        <?php } ?>
        <?php if($err_nopti) { ?><br /><label class="error"><?php echo $err_nopti; ?></label><?php } ?>
      </div>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="nr_adulti" class="titlu">* Nr. adulti</label></div>
      <div class="right">
        <select name="nr_adulti" id="nr_adulti"><?php for($i=1;$i<=10;$i++) { ?><option value="<?php echo $i; ?>" <?php if($_POST['nr_adulti']==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option><?php } ?></select>
        <?php if($err_adulti) { ?><br /><label class="error"><?php echo $err_adulti; ?></label><?php } ?>
      </div>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="nr_copii" class="titlu">Nr. copii</label></div>
      <div class="right">
        <select name="nr_copii" id="nr_copii" onchange="if(this.value>0) { var cont=''; for(var i=1; i<=this.value; i++) { cont=cont+'<p>Copil '+i+', ani impliniti: <select name=\'varsta['+i+']\'>'; for(var j=1; j<=14; j++) cont=cont+'<option value=\''+j+'\'>'+j+'</option>'; cont=cont+'</select></p>'; } document.getElementById('copii').innerHTML=cont; }"><?php for($i=0;$i<=10;$i++) { ?><option value="<?php echo $i; ?>" <?php if($_POST['nr_copii']==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option><?php } ?></select>
        <div id="copii"><?php if(sizeof($_POST['varsta'])>0) {
foreach($_POST['varsta'] as $key=>$value) {  echo '<p>Copil '.$key.', ani impliniti: <select name="varsta['.$key.']">';
for($j=1;$j<=14;$j++) { echo '<option value="'.$j.'"'; if($j==$value) echo 'selected="selected"'; echo '>'.$j.'</option>'; } echo '</select></p>'; }
 } ?></div>
      </div>
    </div>
    <h3 class="red">Aceasta cerere de oferta o inregistrez in scopul:</h3>
    <div class="item clearfix" style="padding:0 10px 10px 10px;">
      <?php /*?><label class="black"><input type="radio" name="scop_cerere" value="Calcularii pretului total al sejurului" <?php if($_POST['scop_cerere']=='Calcularii pretului total al sejurului') echo 'checked="checked"'; ?> /> <strong>Calcularii pretului total al sejurului</strong></label><br /><?php */?>
      <label class="black"><input type="radio" name="scop_cerere" value="Verificarii disponibilitatii locurilor in perioada aleasa" <?php if($_POST['scop_cerere']=='Verificarii disponibilitatii locurilor in perioada aleasa') echo 'checked="checked"'; ?> /> <strong>Verificarii disponibilitatii locurilor in perioada aleasa</strong></label><br />
      <label class="black"><input type="radio" name="scop_cerere" value="Alte motive, pe care le completez in campul de mai jos" <?php if($_POST['scop_cerere']=='Alte motive, pe care le completez in campul de mai jos') echo 'checked="checked"'; ?> /> <strong>Alte motive, pe care le completez in campul de mai jos</strong></label>
      <?php if($err_scop_cerere) { ?><br /><label class="error"><?php echo $err_scop_cerere; ?></label><?php } ?>
    </div>
    <div class="item clearfix">
      <div class="left"><label for="observatii" class="titlu">Observatii</label></div>
      <div class="right">
        <textarea name="observatii" id="observatii" class="big"><?php if($_POST['observatii']) echo $_POST['observatii']; ?></textarea>
        <?php if($err_observatii) { ?><br /><label class="error"><?php echo $err_observatii; ?></label><?php } ?>
      </div>
    </div>
  </div>
  
  <input name="trimite" type="hidden" value="pas1" />
  <p style="padding-left:180px;"><input type="submit" value="Inregistreaza cererea" class="button-blue" /></p>
  <br class="clear" /><br />
  </form>

</div>

<div class="NEW-column-right1">
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta_new.php"); ?>
</div>

<br class="clear" />

</div>


    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script type="text/javascript">
/*var availableDates = ["9-5-2012","14-5-2012","15-5-2012"];*/
var availableDates = [<?php echo $data_calendar; ?>];

function available(date) {
  dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
  if ($.inArray(dmy, availableDates) < 0) {
    return [false];
  } else {
    return [true];
  }
}

$('#data_start1').datepicker({
	<?php if($detalii['valabila']=='da') { ?>
	numberOfMonths: 3,
	dateFormat: "yy-mm-dd",
	<?php if(!$err_logare_admin) {} else { if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') {} else { ?>
	beforeShowDay: available,
	<?php } } ?>
	showButtonPanel: true,
	minDate: new Date(<?php echo $st[0].', '.$st[1].' - 1, '.$st[2]; ?>),
	maxDate: new Date(<?php echo $en[0].', '.$en[1].' - 1, '.$en[2]; ?>)
	<?php } else { ?>
	numberOfMonths: 2,
	dateFormat: "yy-mm-dd"
	<?php } ?>
});
</script>
</body>
</html>
