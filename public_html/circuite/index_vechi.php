<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$sel_dest = "SELECT
MIN(oferte.pret_minim) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
tari.denumire,
tari.id_tara,
tari.info_scurte,
continente.nume_continent
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara
INNER JOIN continente ON continente.id_continent = tari.id_continent
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
AND traseu_circuit.tara_principala = 'da'
GROUP BY tari.denumire
ORDER BY numar DESC, tari.denumire ASC ";
$que_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($que_dest)) {
	$destinatie['den_tara'][] = $row_dest['denumire'];
	$destinatie['link'][] = '/circuite/'.fa_link($row_dest['denumire']).'/';
	$destinatie['link_noindex'][] = '/circuit/'.$row_dest['id_tara'].'/';
	$destinatie['id_tara'][] = $row_dest['id_tara'];
	$destinatie['info_scurte'][] = $row_dest['info_scurte'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Circuite <?php echo date("Y"); ?>, Oferte circuite | Ocaziituristice.ro</title>
<meta name="description" content="Circuite 2014 in Europa, Asia, America, Africa cu avionul sau autocarul. Vezi oferta Ocaziituristice.ro pentru circuite <?php echo date("Y"); ?>." />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Circuite <?php echo date("Y"); ?></h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>

        <br class="clear">
        <div class="Hline"></div>
        
		<?php $tipuri = get_tiputi_circuite('', '', '', '', '', '');
		if(sizeof($tipuri)>0) {
			$link_new = '/circuite-';
		?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/circuite/'; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		foreach($tipuri as $key_t=>$value_t) {
			$i++;
			$valTip = fa_link($value_t['denumire']);
			if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo $link_new.$valTip.'/'; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire']; ?></a></li>
		<?php } ?>
        </ul>
		<?php } ?>

        <?php if(sizeof($destinatie['den_tara'])>0) { ?>
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie['den_tara'] as $key1 => $value1) { ?>
          <div class="destItem NEW-round4px clearfix" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
            <div class="topBox">
              <img src="images/<?php echo fa_link($destinatie['den_tara'][$key1]); ?>.jpg" alt="<?php echo $destinatie['den_tara'][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie['den_tara'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?><div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><?php } ?>
            <div class="bottomBox clearfix">
              <div class="price"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Circuite <?php echo $destinatie['den_tara'][$key1]?>">Circuite <?php echo $destinatie['den_tara'][$key1]?></a>&nbsp;<br/>de la<br><span class="red"><span class="pret"><?php echo $destinatie['pret_minim'][$key1]; ?></span> <span class="bold bigger-15em">&euro;</span></span> /pers</div>
              <div class="details"><a href="<?php echo $destinatie['link'][$key1]; ?>"><img src="/images/but_nou_detalii.png" alt="detalii" width="90"></a></div>
            </div>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        
        <br>
		
        <div class="pad10">
          <?php /*?><img src="images/icon-circuite-turistice.jpg" alt="Circuite turistice" class="float-left" style="margin:10px 10px 0 0;"><?php */?>
          <h2><strong>Circuite turistice</strong> - vacanţe de vis cu autocarul sau avionul</h2>
          <p class="newText text-justify">Dorinţa de aventură şi explorare a cât mai multe oraşe, regiuni sau ţări într-o singură călătorie devine tot mai accesibilă apelând la circuite turistice oferite de agenţiile de turism.</p>
          
          <br class="clear">
          
          <p class="newText text-justify">Există destinaţii cu preţuri moderate, dar şi destinaţii de top unde turiştii se pot bucura de condiţii luxoase, totul depinde de bugetul alocat de fiecare.</p>
          <p class="newText text-justify">În Europa, Asia sau America, poate fi o singură ţară ca destinaţie, sau mai multe, unde cele mai importante atracţii sunt vizitate după un itinerar bine stabilit cu întoarcerea la punctul de plecare.</p>
          <p class="newText text-justify">Cele mai ieftine <em>circuite turistice</em> sunt cele cu autocarul. Pentru circuite în destinaţii foarte îndepărtate, transportul cu avionul care se impune în aceste cazuri, măreşte preţul vacanţei, dar, atunci când se ajunge la destinaţie cu avionul, vizitele, excursiile şi deplasările între obiective se realizează tot folosind autocarul, fiind incluse în preţ.</p>
          <p class="newText text-justify">Ghidul ce însoţeşte turiştii în călătorie are grijă să nu lase plictiseala şi monotonia să se strecoare în timpul vacanţei. Organizarea de mici concursuri, prezentarea obiectivelor ce urmează a fi vizitate, proiecţia de documentare informative, reprezintă câteva din modalităţile prin care se doreşte ca aceste circuite turistice să se transforme într-un mare avantaj pentru turişti.</p>
          <p class="newText text-justify">Circuitele se adresează în special persoanelor care vor să viziteze mai multe locuri sau locaţii pe perioada vacanţei şi să îşi petreacă timpul liber în cel mai util mod, să viziteze obiective importante din punct de vedere turistic sau istoric, să cunoască tradiţiile şi obiceiurile locurilor respective.</p>
          <p class="newText text-justify">Pentru <strong>oferte circuite turistice</strong>, alegerea operatorului de turism este deosebit de importantă. Astfel de vacanţe implică o planificare foarte detaliată între agenţie şi reprezentanţii turistici locali.</p>
          <p class="newText text-justify">Avantajul unor astfel de circuite turistice o reprezintă faptul că într-o săptămână, maxim zece zile, se pot vizita obiectivele dorite la un preţ mult mai mic decât atunci când se plătesc separat cazările, mesele, intrările la obiective turistice, taxele de drum, etc.</p>
          <p class="newText text-justify">Deşi par obositoare, o bună organizare a timpului din partea agenţiei de turism face din aceste circuite o luptă permanentă de asimilare a tuturor informaţiilor, indiferent de starea de spirit a turiştilor.</p>
          <p class="newText text-justify">Profesionalismul şi serviciile de calitate oferite de agenţia de turism, de care turiştii se despart cu greu, reprezintă reuşita şi satisfacţia unei vacanţe de vis!</p>
        </div>

        <br class="clear">
        
        <?php /*?><?php if(sizeof($destinatie['den_tara'])>0) { ?>
        <h3 class="mar10">Circuite <?php echo date("Y"); ?></h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie['den_tara'] as $key2 => $value2) {
			  $part_anchor = 'Circuite ';
		  ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie['link'][$key2]; ?>" title="<?php echo $part_anchor.$destinatie['den_tara'][$key2]; ?>" class="link-blue block"><?php echo $part_anchor.$destinatie['den_tara'][$key2]; ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?><?php */?>
        
        <br class="clear"><br>
        
      </div>

    </div>
    <br><br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>