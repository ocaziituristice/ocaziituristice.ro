<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Agentie de turism, sejururi, circuite, cazari, early booking | Ocaziituristice.ro</title>
<meta name="description" content="Oferte sejururi, cazari, program pentru seniori ale agentiei de turism Dream Voyage, posibilitatea de a face rezervari online" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner" style="padding:5px 0;">
    <div class="NEW-column-full">
      <div id="index" class="clearfix">
      
        <div class="half-right">
          <div class="tematici">
            
            <?php /*?><div class="list clearfix">
              <h2><a href="<?php echo $sitepath; ?>oferte-revelion/" title="Oferte Revelion 2012">Oferte Revelion 2012</a></h2>
              <img src="<?php echo $imgpath; ?>/tematici/tematica-revelion.png" alt="Revelion 2012" class="image" onclick="location.href='<?php echo $sitepath; ?>oferte-revelion/';" />
              <div class="items">
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>';" title="Revelion Borovets - Hotel Yanakiev">
                  <div class="pret">de la<br /><span>123 &euro;</span></div>
                  <a href="<?php echo $sitepath; ?>" class="link-black" title="Revelion Borovets - Hotel Yanakiev">Hotel Yanakiev</a><br />
                  <span class="stele-mici-4"></span>
                </div>
                <div class="readmore">vezi toate Ofertele Sejur Ski &raquo;</div>
              </div>
            </div><?php */?>
            
            <div class="list clearfix">
              <h2><a href="<?php echo $sitepath; ?>oferte-paste/" title="Oferte sejur paste">Paste 2012</a></h2>
              <a href="<?php echo $sitepath; ?>oferte-paste/" title="Oferte sejur paste 2012"><img src="<?php echo $imgpath; ?>/tematici/tematica-paste.png" alt="Sarbatoare Paste 2012" class="image" /></a>
              <div class="items">
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-paste/romania/';" title="Sejur paste Romania">
                  <a href="<?php echo $sitepath; ?>oferte-paste/romania/" class="denumire link-red" title="sejur paste Romania">Romania</a>
                  <span>Obiceiuri Romanesti</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-paste/bulgaria/';" title="oferte sejur paste Bulgaria">
                  <a href="<?php echo $sitepath; ?>oferte-paste/bulgaria/" class="denumire link-red" title="Oferte paste Bulgaria">Bulgaria</a>
                  <span>Rasfat la pret bun</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-paste/italia/';" title="Oferte sejur paste Italia">
                  <a href="<?php echo $sitepath; ?>oferte-paste/italia/" class="denumire link-red" title="paste Italia">Italia</a>
                  <span>Inima catolicismului</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-paste/spania/';" title="Oferte sejur paste Spania">
                  <a href="<?php echo $sitepath; ?>oferte-paste/spania/" class="denumire link-red" title="Oferte sejur paste Spania">Spania</a>
                  <span>Primele urme de bronz</span>
                </div>
                <div class="readmore">vezi toate <a href="<?php echo $sitepath; ?>oferte-paste/" class="link-green"> Ofertele de Paste &raquo;</a></div>
              </div>
            </div>
            
            <div class="list clearfix">
              <h2>Vara - Litoral Plaja 2012</h2>
              <img src="<?php echo $imgpath; ?>/tematici/tematica-litoral.png" alt="Craciun" class="image" />
              <div class="items">
               <div class="item">
                  <a href="<?php echo $sitepath; ?>sejur-romania/litoral/" class="denumire link-red" title="Oferte sejur, cazare Litoral">Litoral Romania</a>
                </div>
                <div class="item">
                <a href="<?php echo $sitepath; ?>sejur-bulgaria/" class="denumire link-red" title="Sejur Bulgaria">Bulgaria</a>
                  <span>All Inclusive</span>
                </div>
                <div class="item">
                <a href="<?php echo $sitepath; ?>sejur-grecia/" class="denumire link-red" title="Oferte Sejur Grecia">Grecia</a>
                  <span>Preturi pentru toti</span>
                </div>
                <div class="item">
                <a href="<?php echo $sitepath; ?>sejur-turcia/" class="denumire link-red" title="Sejur Turcia">Turcia</a>
                  <span>Eleganta maxima</span>
                </div>
                <?php /*?><div class="readmore"><a href="<?php echo $sitepath; ?>oferte-craciun/" class="link-green">vezi toate Ofertele de Craciun &raquo;</a></div><?php */?>
              </div>
            </div>
            
            <div class="list clearfix">
              <h2><a href="<?php echo $sitepath; ?>oferte-sejur-ski/" title="Oferte Sejur Ski">Sejur Ski</a></h2>
              <a href="<?php echo $sitepath; ?>oferte-sejur-ski/" title="Oferte Sejur Ski"><img src="<?php echo $imgpath; ?>/tematici/tematica-sejur-ski.png" alt="Sejur Ski" class="image" /></a>
              <div class="items">
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-sejur-ski/bulgaria/';" title="Sejur Ski Bulgaria">
                  <a href="<?php echo $sitepath; ?>oferte-sejur-ski/bulgaria/" class="denumire link-red" title="Sejur Ski Bulgaria">Bulgaria</a>
                  <span>SKI la pret bun</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-sejur-ski/austria/';" title="Sejur Ski Austria">
                  <a href="<?php echo $sitepath; ?>oferte-sejur-ski/austria/" class="denumire link-red" title="Sejur Ski Austria">Austria</a>
                  <span>Zapada adevarata</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-sejur-ski/romania/';" title="Sejur Ski Romania">
                  <a href="<?php echo $sitepath; ?>oferte-sejur-ski/romania/" class="denumire link-red" title="Sejur Ski Romania">Romania</a>
                  <span>Ski acasa</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-sejur-ski/italia/';" title="Sejur Ski Italia">
                  <a href="<?php echo $sitepath; ?>oferte-sejur-ski/italia/" class="denumire link-red" title="Sejur Ski Italia">Italia</a>
                  <span>Eleganta ski-ului</span>
                </div>
                <div class="readmore">vezi toate <a href="<?php echo $sitepath; ?>oferte-sejur-ski/" class="link-green">Ofertele de Ski &raquo;</a></div>
              </div>
            </div>
            
            <div class="list clearfix">
              <h2><a href="<?php echo $sitepath; ?>oferte-program-pentru-seniori/" title="Oferte Program pentru Seniori">Program pentru Seniori</a></h2>
              <a href="<?php echo $sitepath; ?>oferte-program-pentru-seniori/" title="Oferte Program pentru Seniori"><img src="<?php echo $imgpath; ?>/tematici/tematica-program-pentru-seniori.png" alt="Program pentru Seniori" class="image" /></a>
              <div class="items">
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-program-pentru-seniori/spania/';" title="Program pentru Seniori Spania">
                  <a href="<?php echo $sitepath; ?>oferte-program-pentru-seniori/spania/" class="denumire link-red" title="Program pentru Seniori Spania">Spania</a><span>Plaja si distractie</span></div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-program-pentru-seniori/italia/';" title="Program pentru Seniori Italia">
                  <a href="<?php echo $sitepath; ?>oferte-program-pentru-seniori/italia/" class="denumire link-red" title="Program pentru Seniori Italia">Italia</a>
                  <span>Istoria noastra</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-program-pentru-seniori/cipru/';" title="Program pentru Seniori Cipru">
                  <a href="<?php echo $sitepath; ?>oferte-program-pentru-seniori/cipru/" class="denumire link-red" title="Program pentru Seniori Cipru">Cipru</a>
                  <span>Caldura si prietenie</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-program-pentru-seniori/israel/';" title="Program pentru Seniori Israel">
                  <a href="<?php echo $sitepath; ?>oferte-program-pentru-seniori/israel/" class="denumire link-red" title="Program pentru Seniori Israel">Israel</a>
                  <span>Locuri sfinte</span>
                </div>
                <div class="readmore">vezi toate <a href="<?php echo $sitepath; ?>oferte-program-pentru-seniori/" class="link-green">Ofertele pentru Seniori &raquo;</a></div>
              </div>
            </div>
            
            <div class="list clearfix">
              <h2><a href="<?php echo $sitepath; ?>oferte-city-break/" title="Oferte City Break">City Break</a></h2>
              <a href="<?php echo $sitepath; ?>oferte-city-break/" title="Oferte City Break"><img src="<?php echo $imgpath; ?>/tematici/tematica-city-break.png" alt="City Break" class="image" /></a>
              <div class="items">
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-city-break/ungaria/budapesta/';" title="City Break Budapesta">
                  <a href="<?php echo $sitepath; ?>oferte-city-break/ungaria/budapesta/" class="denumire link-red" title="City Break Budapesta">Budapesta</a>
                  <span>Üdvözöljük</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-city-break/franta/paris/';" title="City Break Paris">
                  <a href="<?php echo $sitepath; ?>oferte-city-break/franta/paris/" class="denumire link-red" title="City Break Paris">Paris</a>
                  <span>Bienvenue</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-city-break/italia/roma/';" title="City Break Roma">
                  <a href="<?php echo $sitepath; ?>oferte-city-break/italia/roma/" class="denumire link-red" title="City Break Roma">Roma</a>
                  <span>Benvenuto</span>
                </div>
                <div class="item clearfix vtip" onclick="location.href='<?php echo $sitepath; ?>oferte-city-break/spania/catalonia/barcelona/';" title="City Break Barcelona">
                  <a href="<?php echo $sitepath; ?>oferte-city-break/spania/catalonia/barcelona/" class="denumire link-red" title="City Break Barcelona">Barcelona</a>
                  <span>Bienvenida</span>
                </div>
                <div class="readmore">vezi toate <a href="<?php echo $sitepath; ?>oferte-city-break/" class="link-green">Ofertele de City Break &raquo;</a></div>
              </div>
            </div>
            
          </div>
        </div>
        
		<div class="half-left clearfix">
          <h1 class="green">Oferte sejururi Litoral Vara 2012</h1>
          
          <div class="destinatie">
            <a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/" title="oferte bulgaria litoral"><img src="<?php echo $imgpath; ?>/index/bulgaria-logo.jpg" alt="Bulgaria logo" class="image" /></a>
            <h2><a href="<?php echo $sitepath; ?>sejur-bulgaria/munte/bansko/" title="Bansko">Bulgaria</a></h2>
    <!--        <a href="<?php echo $sitepath; ?>sejur-bulgaria/" title="Bugaria" class="link-green"><img src="<?php echo $imgpath; ?>/flags/bulgaria.small.gif" alt="Steag Bulgaria" class="flag" /> Bulgaria</a>-->
            <div class="linkuri">
              <a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/albena/" title="oferte albena">Albena</a>
              <a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/nisipurile-de-aur/" title="oferte nisipurile de aur">Nisipirile de Aur</a>
              <a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/sunny-beach//" title="oferte Sunny Beach">Sunny Beach</a>
            </div>
          </div>
          
          <div class="destinatie">
            <a href="<?php echo $sitepath; ?>sejur-turcia/" title="Oferte turcia"><img src="<?php echo $imgpath; ?>/index/turcia-logo.jpg" alt="Turcia" class="image" /></a>
            <h2><a href="<?php echo $sitepath; ?>sejur-turcia/" title="Oferte Turcia">Turcia</a></h2>
                        <div class="linkuri">
              <a href="<?php echo $sitepath; ?>sejur-turcia/antalya/" title="oferte Antalya">Antalya</a>
              <a href="<?php echo $sitepath; ?>sejur-turcia/kusadasi/" title="oferte Kusadasi">Kusadasi</a>
              <a href="<?php echo $sitepath; ?>sejur-turcia/bodrum/" title="Sejur Bodrum">Bodrum</a>
            </div>
          </div>
          
          <div class="destinatie">
            <a href="<?php echo $sitepath; ?>sejur-grecia/" title="oferte Grecia"><img src="<?php echo $imgpath; ?>/index/grecia-logo.jpg" alt="Grecia logo" class="image" /></a>
            <h2><a href="<?php echo $sitepath; ?>sejur-grecia/" title="oferte Grecia">Grecia</a></h2>
            
            <div class="linkuri">
              <a href="<?php echo $sitepath; ?>sejur-grecia/halkidiki-sithonia//" title="Oferte Halkidiki">Halkidiki</a>
              <a href="<?php echo $sitepath; ?>sejur-grecia/paralia-katerini/" title="Oferte Paralia katerni">Paralia Katerini</a>
              <a href="<?php echo $sitepath; ?>sejur-grecia/corfu/" title="Oferte Corfu">Corfu</a>
               <a href="<?php echo $sitepath; ?>sejur-grecia/creta/" title="Oferte Creta">Creta</a>
              
            </div>
          </div>
          
          <div class="destinatie">
            <a href="<?php echo $sitepath; ?>sejur-spania/" title="oferte croatia"><img src="<?php echo $imgpath; ?>/index/spania-logo.jpg" alt="Spania" class="image" /></a>
            <h2><a href="<?php echo $sitepath; ?>sejur-spania/" title="Oferte Spania">Spania</a></h2>
            
            <div class="linkuri">
              <a href="<?php echo $sitepath; ?>sejur-spania/mallorca/" title="Oferte Mallorca">Mallorca</a>
              <a href="<?php echo $sitepath; ?>sejur-spania/costa-brava/" title="Oferte Costa Brava">Costa Brava</a>
              <a href="<?php echo $sitepath; ?>sejur-spania/costa-del-sol/" title="Oferte Costa del Sol">Costa del Sol</a>
             
            </div>
          </div>
         
          <div class="clear"></div>
          
          <div class="romania">
            <div class="titlu clearfix">
              <img src="<?php echo $imgpath; ?>/flags/romania.small.gif" alt="Steag Romania" class="flag" />
              <h2><a href="<?php echo $sitepath; ?>sejur-romania/litoral/" title="Oferte Litoral - Romania" class="link-red" >Oferte Litoral Romania</a></h2>
            </div>
            <div class="clearfix">
              <a href="<?php echo $sitepath; ?>oferte-turism-intern/" title="Oferte Turism Intern - Romania"><img src="<?php echo $imgpath; ?>/index/romania.png" alt="Romania" class="image" /></a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/mamaia/" title="Oferte Mamaia" class="dest">Mamaia</a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/neptun/" title="Oferte Neptun" class="dest">Neptun</a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/eforie-nord/" title="Oferte Eforie Nord" class="dest">Eforie Nord</a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/eforie-sud/" title="Oferte Eforie Sud" class="dest">Eforie Sud</a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/costinesti/" title="Oferte Costinesti" class="dest">Costinesti</a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/venus/" title="Oferte Venus" class="dest">Venus</a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/jupiter/" title="Oferte Jupiter" class="dest">Jupiter</a>
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/mangalia/" title="Oferte Mangalia" class="dest">Mangalia</a>                                          
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/olimp/" title="Olimp" class="dest">Olimp</a>            
              <a href="<?php echo $sitepath; ?>sejur-romania/litoral/saturn/" title="Saturn" class="dest">Saturn</a>       
            </div>
            <div class="readmore"><a href="<?php echo $sitepath; ?>oferte-turism-intern/" class="link-black">vezi toate ofertele din Romania &raquo;</a></div>
          </div>
          
          <div class="eb">
            <h2><a href="<?php echo $siteapth; ?>early-booking/" title="Oferte Early Booking, Inscrieri Timpurii">Early Booking - Inscrieri Timpurii</a></h2>
            <br />
            <div class="item clearfix" onclick="location.href='<?php echo $sitepath; ?>early-booking/turcia/';">
              <div class="titlu"><a href="<?php echo $sitepath; ?>early-booking/turcia/" title="Early Booking Turcia">Turcia</a></div>
              <span class="pret red">de la <strong>405</strong> &euro; &nbsp; pers / noapte</span>
              <span class="rezerva">rezerva &raquo;</span>
            </div>
            <div class="item clearfix" onclick="location.href='<?php echo $sitepath; ?>early-booking/bulgaria/munte/bansko/';">
              <div class="titlu"><a href="<?php echo $sitepath; ?>early-booking/spania/" title="Early Booking Spania">Spania</a></div>
              <span class="pret red">de la <strong>407</strong> &euro; &nbsp; pers / noapte</span>
              <span class="rezerva">rezerva &raquo;</span>
            </div>
            <div class="item clearfix" onclick="location.href='<?php echo $sitepath; ?>early-booking/muntenegru/';">
              <div class="titlu"><a href="<?php echo $sitepath; ?>early-booking/muntenegru/" title="Early Booking Muntenegru">Muntenegru</a></div>
              <span class="pret red">de la <strong>119</strong> &euro; &nbsp; pers / noapte</span>
              <span class="rezerva">rezerva &raquo;</span>
            </div>
           
            
            <div class="item clearfix" onclick="location.href='<?php echo $sitepath; ?>early-booking/bulgaria/';">
              <div class="titlu"><a href="<?php echo $sitepath; ?>early-booking/bulgaria/" title="Early Booking Litoral Bulgaria">Litoral Bulgaria</a></div>
              <span class="pret red">de la <strong>13</strong> &euro; &nbsp; pers / noapte</span>
              <span class="rezerva">rezerva &raquo;</span>
            </div>
            <div class="item clearfix" onclick="location.href='<?php echo $sitepath; ?>early-booking/grecia/';">
              <div class="titlu"><a href="<?php echo $sitepath; ?>early-booking/grecia/" title="Early Booking Litoral Grecia">Litoral Grecia</a></div>
              <span class="pret red">de la <strong>13</strong> &euro; &nbsp; pers / noapte</span>
              <span class="rezerva">rezerva &raquo;</span>
            </div>
            <div class="item clearfix" onclick="location.href='<?php echo $sitepath; ?>early-booking/romania/';">
              <div class="titlu"><a href="<?php echo $sitepath; ?>early-booking/romania/" title="Early Booking Litoral Romania">Litoral Romania</a></div>
              <span class="pret red">de la <strong>13</strong> &euro; &nbsp; pers / noapte</span>
              <span class="rezerva">rezerva &raquo;</span>
            </div>
          </div>
          
        </div>
        
        <div class="clear"></div>
        
        <?php /*?><div class="third">
          <a href="http://bileteavion.ocaziituristice.ro/" target="_blank" title="Bilete Avion Online"><img src="<?php echo $imgpath; ?>/banner/banner-bilete-avion.gif" alt="Bilete Avion Online" /></a>
        </div>
        
        <div class="third">
          <div class="circuite">
            <img src="<?php echo $imgpath; ?>/banner/banner-circuite.png" alt="Circuite" />
            <div class="titles">
              <a href="<?php echo $sitepath; ?>circuite/europa/" title="Circuite Europa">Circuite Europa</a>
              <a href="<?php echo $sitepath; ?>circuite/africa/" title="Circuite Africa">Circuite Africa</a>
              <a href="<?php echo $sitepath; ?>circuite/america/" title="Circuite America">Circuite America</a>
              <a href="<?php echo $sitepath; ?>circuite/asia/" title="Circuite Asia">Circuite Asia</a>
            </div>
            <div class="globuri">
              <span onclick="location.href='<?php echo $sitepath; ?>circuite/europa/';"></span>
              <span onclick="location.href='<?php echo $sitepath; ?>circuite/africa/';"></span>
              <span onclick="location.href='<?php echo $sitepath; ?>circuite/america/';"></span>
              <span onclick="location.href='<?php echo $sitepath; ?>circuite/asia/';"></span>
            </div>
          </div>
        </div>
        
        <div class="clear"></div><?php */?>
        
        <div class="circuite">
          <div class="item">
            <a href="<?php echo $sitepath; ?>circuite/europa/" title="Circuite Europa" class="titlu link-red">
              <img src="<?php echo $imgpath; ?>/index/circuit-europa.png" alt="Circuite Europa" />
              <span>Circuite Europa</span>
            </a>
          </div>
          <div class="item">
            <a href="<?php echo $sitepath; ?>circuite/africa/" title="Circuite Africa" class="titlu link-red">
              <img src="<?php echo $imgpath; ?>/index/circuit-africa.png" alt="Circuite Africa" />
              <span>Circuite Africa</span>
            </a>
          </div>
          <div class="item">
            <a href="<?php echo $sitepath; ?>circuite/america/" title="Circuite America" class="titlu link-red">
              <img src="<?php echo $imgpath; ?>/index/circuit-america.png" alt="Circuite America" />
              <span>Circuite America</span>
            </a>
          </div>
          <div class="item">
            <a href="<?php echo $sitepath; ?>circuite/asia/" title="Circuite Asia" class="titlu link-red">
              <img src="<?php echo $imgpath; ?>/index/circuit-asia.png" alt="Circuite Asia" />
              <span>Circuite Asia</span>
            </a>
          </div>
        </div>
        
        <div class="clear"></div>
        
        <div class="third-1col">
          
          <div class="rss-blog">
            <h2 class="green">Ultimele articole de pe blog</h2>
            <br />
<?php 
include_once($_SERVER['DOCUMENT_ROOT']."/js/magpierss/rss_fetch.inc");
$url = 'http://blog.ocaziituristice.ro/feed';
$rss = fetch_rss($url);

$sf=sizeof($rss->items);
for($i=0;$i<$sf-6;$i++) {

    $item = $rss->items[$i];
	$title = $item[title];
	$url   = $item[link];
	$desc = $item[description];
	$readmore = '<a href="'.$url.'" class="red" target="_blank">[...] citeste mai mult</a>';
?>
<div class="item">
  <a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="link" target="_blank"><strong class="blue"><?php echo $title; ?></strong></a><br />
  <span class="description"><?php echo substr($desc, 0, 120); ?></span>
</div>
<?php } ?>
		  </div>
          
        </div>
        
        <div class="third-2col">
          <div class="ultof">
            <h2>Vacante Inedite</h2>
            <br />
            
            <div class="lastof clearfix">
              <a href="<?php echo $sitepath; ?>circuit/asia/pelerinaj-de-boboteaza-la-raul-iordan-in-israel-839.html"><img src="<?php echo $sitepath; ?>thumb_hotel/1666pelerinaj_de_boboteaza_la_raul_iordan_in_israel3hotel.jpg" alt="" width="80" class="image" /></a>
              <div class="titlu"><a href="<?php echo $sitepath; ?>circuit/asia/pelerinaj-de-boboteaza-la-raul-iordan-in-israel-839.html" class="link-blue" title="Pelerinaj de Boboteaza la raul Iordan in Israel">Pelerinaj de Boboteaza la raul Iordan in Israel</a></div>
              <div class="pret red">de la <span>567 &euro;</span> sejur/pers</div>
              <div class="field"><p class="camp">Transport:</p><p class="valoare">Avion</p></div>
              <div class="field"><p class="camp">Durata:</p><p class="valoare">7 zile / 6 nopti</p></div>
            </div>
            
            <div class="lastof clearfix">
              <a href="<?php echo $sitepath; ?>spania/can-pastilla/program-seniori-can-pastilla-_d_-hotel-las-513.html"><img src="<?php echo $sitepath; ?>thumb_hotel/1596hotel_las_arenas2hotel.jpg" alt="" width="80" class="image" /></a>
              <div class="titlu"><a href="<?php echo $sitepath; ?>spania/can-pastilla/program-seniori-can-pastilla-_d_-hotel-las-513.html" class="link-blue" title="Program Seniori Can Pastilla - Hotel Las Arenas">Program Seniori Can Pastilla - Hotel Las Arenas</a> <span class="stele-mici-4"></span></div>
              <div class="pret red">de la <span>355 &euro;</span> sejur/pers</div>
              <div class="field"><p class="camp">Transport:</p><p class="valoare">Avion</p></div>
              <div class="field"><p class="camp">Durata:</p><p class="valoare">8 zile / 7 nopti</p></div>
              <div class="field"><p class="camp">Masa:</p><p class="valoare">Demipensiune</p></div>
            </div>
            
            <div class="lastof clearfix">
              <a href="<?php echo $sitepath; ?>emiratele-arabe-unite/dubai/revelion-dubai-_d_-hotel-emirates--483.html"><img src="<?php echo $sitepath; ?>thumb_hotel/4045hotel_emirates_grand1hotel.jpg" alt="" width="80" class="image" /></a>
              <div class="titlu"><a href="<?php echo $sitepath; ?>emiratele-arabe-unite/dubai/revelion-dubai-_d_-hotel-emirates--483.html" class="link-blue" title="Revelion Dubai - Hotel Emirates Grand">Revelion Dubai - Hotel Emirates Grand</a> <span class="stele-mici-4"></span></div>
              <div class="pret red">de la <span>815 &euro;</span> sejur/pers</div>
              <div class="field"><p class="camp">Transport:</p><p class="valoare">Avion</p></div>
              <div class="field"><p class="camp">Durata:</p><p class="valoare">9 zile / 8 nopti</p></div>
              <div class="field"><p class="camp">Masa:</p><p class="valoare">Mic Dejun</p></div>
            </div>
            
          </div>
        </div>
        
        <div class="third-3col">
          <a href="http://bileteavion.ocaziituristice.ro/" title="Bilete Avion" target="_blank"><img src="<?php echo $imgpath; ?>/index/bileteavion.jpg" alt="" /></a>
        </div>
        
        <div class="clear"></div>
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>