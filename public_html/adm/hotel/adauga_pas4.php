<script type="text/javascript">
//<![CDATA[
function validare() {
var err='';
var tara=document.getElementById('tara').value;
var zona=document.getElementById('zona').value;
var localitate=document.getElementById('localitate').value;
var denumire=document.getElementById('denumire').value;
if(tara=='') err='Nu ati selectat tara! \n';
if(zona=='') err=err+'Nu ati selectat zona! \n';
if(localitate=='') err=err+'Nu ati selectat localitatea! \n';
var dim=denumire.length;
if(dim<3) err=err+'Nu ati completat denumirea localitatii';
if(err!='') { alert(err); return false;} else return true;	
} 
//]]>
</script>
<?php
if($_POST['param']) $param=$_POST['param']; else $param=array();
if($_POST['circuit']) $circuit=$_POST['circuit']; else $circuit=array();
$id_localitate=$param['localitate'];
$id_tara=$param['tara'];
$id_zona=$param['zona'];
if($_POST['adauga']) {
	if(strlen(trim($param['denumire']))>3) {
		if($param['tara']) {
			if($param['zona']) {
				$den=trim($param['denumire']);
				if(verif_hotel($den, $id_localitate)==0) {
					include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'class/hotel.php');
					include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'class/adaugare_poza.php');
					$ad_poza=new ADAUGARE_POZA;
					$nume=fa_link_vechi($den);
					if($_FILES['attachments']) {
						$poz_h=$_FILES['attachments'];
						$param['img']=$ad_poza->adauga($poz_h, 'hotel', $nume);
					}
					$add_z=new HOTEL;
					$add_z->adaugare($param, $circuit);
				} else $mesaj='Zona exista deja in baza de date!';
			} else $mesaj='Nu ati selectat tara!';
		} else $mesaj='Nu ati selectat tara!';
	}else $mesaj='Denumirea este prea scurta!';
} ?>
<form name="adauga_hotel" method="post" action="" enctype="multipart/form-data" onsubmit="return validare();">
<?php $valut_but_submit='Adauga'; include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'hotel/tabel.php'); ?>
</form>