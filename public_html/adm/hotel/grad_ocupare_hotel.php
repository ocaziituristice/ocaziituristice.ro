<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="/adm/reset.css" rel="stylesheet" type="text/css" />
<link href="/adm/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/adm/js/grad_ocupare.js"></script>
<script src="/js/ajaxpage.js"></script>
</head>

<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
$id_hotel=$_REQUEST['id_hotel'];
$ocup=array();
if($_POST['salveaza']) {
	$ocup['nr_adulti']=$_POST['nr_adulti'];
	$ocup['nr_copii']=$_POST['nr_copii'];
	$ocup['varsta_min']=$_POST['varsta_min'];
	$ocup['varsta_max']=$_POST['varsta_max'];
	if(sizeof($ocup['nr_adulti'])>0) {
		$del="DELETE FROM grad_ocupare_camera WHERE id_hotel = '".$id_hotel."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);
		foreach($ocup['nr_adulti'] as $ke=>$va) {
			$id_cam=$ke;
			foreach($va as $ke1=>$val) {
				$j=0;
				$nr_ad=$val;
				$nr_cop=$ocup['nr_copii'][$id_cam][$ke1];
				$cop=array();
				if(sizeof($ocup['varsta_min'][$id_cam][$ke1])>0) {
					foreach($ocup['varsta_min'][$id_cam][$ke1] as $ke2=>$val2) {
						$cop[]=$val2;
					}
				}
				$j++;
				
				$ins="INSERT INTO grad_ocupare_camera SET id_hotel = '".$id_hotel."', id_camera = '".$id_cam."', nr_adulti = '".$nr_ad."', nr_copii = '".$nr_cop."', copil1 = '".$cop[0]."', copil2 = '".$cop[1]."', copil3 = '".$cop[2]."', copil4 = '".$cop[3]."', ordine = '".$j."' ";
				$que=mysql_query($ins) or die(mysql_error());
				@mysql_free_result($que);
			}
		}
	} echo '<h2><font class="red">Datele au fost adaugate!</font></h2><br/><br/>';
} else {
	$cam='';
	
	$selCam="select * from grad_ocupare_camera where id_hotel = '".$id_hotel."' Group by id_camera, nr_adulti, nr_copii, copil1, copil2, copil3 Order by id_camera ";
	$queCam=mysql_query($selCam) or die(mysql_error());
	while($rowCam=mysql_fetch_array($queCam)) {
		if($rowCam['id_camera']<>$cam) {
			$ind=0;
			$cam=$rowCam['id_camera'];
		}
		$ocup['nr_adulti'][$rowCam['id_camera']][$ind]=$rowCam['nr_adulti'];
		$ocup['nr_copii'][$rowCam['id_camera']][$ind]=$rowCam['nr_copii'];
		if($rowCam['nr_copii']>0) {
			for($i=1; $i<=3; $i++) {
				if($rowCam['copil'.$i]) {
					$ocup['varsta_min'][$rowCam['id_camera']][$ind][$i]=$rowCam['copil'.$i];
				}
			}
		}
		$ind++;
	} @mysql_free_result($queCam);
}

$selCH="select tip_camera.denumire, tip_camera.id_camera from tip_camera inner join camere_hotel on tip_camera.id_camera = camere_hotel.id_camera where camere_hotel.id_hotel = '".$id_hotel."' Group by tip_camera.id_camera Order by tip_camera.denumire ";
$queCH=mysql_query($selCH) or die(mysql_error());
?>
<form name="ad" action="" method="post">
  <table width="97%" border="1" cellspacing="2" cellpadding="3" id="grad_ocupare" align="left" class="tabel-inside">
    <tr>
      <th align="center" valign="top">Camera</th>
      <th align="center" valign="top" width="100">Nr. adulti</th>
      <th align="center" valign="top">Nr. copii</th>
      <th align="center" valign="top" width="50">&nbsp;</th>
    </tr>
    <?php while($rowCH=mysql_fetch_array($queCH)) { $nrL=1; ?>
    <tr>
      <td align="center" valign="top"><?php echo $rowCH['denumire']; ?></td>
      <td align="left" valign="top" colspan="3"><table width="100%" cellspacing="0" cellpadding="0" id="ocup_<?php echo $rowCH['id_camera']; ?>">
          <tr id="randGrad<?php echo $rowCH['id_camera']; ?>_0">
            <td align="left" valign="top" width="100"><?php $nrL=1; ?>
              <select name="nr_adulti[<?php echo $rowCH['id_camera']; ?>][0]">
                <?php for($i=1;$i<=10;$i++) { ?>
                <option value="<?php echo $i; ?>" <?php if($ocup['nr_adulti'][$rowCH['id_camera']][0]==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                <?php } ?>
              </select></td>
            <td align="left" valign="top"><select name="nr_copii[<?php echo $rowCH['id_camera']; ?>][0]" onchange="if(this.value>0) { var cont=''; for(var i=1; i<=this.value; i++) { cont=cont+'Copil '+i+' interval: <input type=\'text\' name=\'varsta_min[<?php echo $rowCH['id_camera']; ?>][0]['+i+']\' class=\'mic\' /> <br/><br/>'; } document.getElementById('copii<?php echo $rowCH['id_camera']; ?>_0').innerHTML=cont; }">
                <?php for($i=0;$i<=3;$i++) { ?>
                <option value="<?php echo $i; ?>" <?php if($ocup['nr_copii'][$rowCH['id_camera']][0]==$i || (!$ocup['nr_copii'][$rowCH['id_camera']][0] && $i==0)) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                <?php } ?>
              </select>
              <div id="copii<?php echo $rowCH['id_camera']; ?>_0">
                <?php if(sizeof($ocup['varsta_min'][$rowCH['id_camera']][0])>0) {
foreach($ocup['varsta_min'][$rowCH['id_camera']][0] as $key1=>$value1) { echo 'Copil '.$key1.' interval: <input type="text" name="varsta_min['.$rowCH['id_camera'].'][0]['.$key1.']" value="'.$value1.'" class="mic" /><br/><br/>'; }
  } else echo '&nbsp;'; ?>
              </div></td>
            <td align="center" valign="top" width="50"><a href="javascript: ;" onclick="adauga_linia_GO(<?php echo $rowCH['id_camera']; ?>);" class="buttons">+</a></td>
          </tr>
          <?php if(sizeof($ocup['nr_adulti'][$rowCH['id_camera']])>0) {
  foreach($ocup['nr_adulti'][$rowCH['id_camera']] as $key=>$value) {
  if($key<>0) { $nrL++; ?>
          <tr id="randGrad<?php echo $rowCH['id_camera'].'_'.$nrL; ?>">
            <td align="left" valign="top" width="100"><select name="nr_adulti[<?php echo $rowCH['id_camera']; ?>][<?php echo $nrL; ?>]">
                <?php for($i=1;$i<=10;$i++) { ?>
                <option value="<?php echo $i; ?>" <?php if($ocup['nr_adulti'][$rowCH['id_camera']][$key]==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                <?php } ?>
              </select></td>
            <td align="left" valign="top"><select name="nr_copii[<?php echo $rowCH['id_camera']; ?>][<?php echo $nrL; ?>]" onchange="if(this.value>0) { var cont=''; for(var i=1; i<=this.value; i++) { cont=cont+'Copil '+i+' interval: <input type=\'text\' name=\'varsta_min[<?php echo $rowCH['id_camera']; ?>][<?php echo $nrL; ?>]['+i+']\' class=\'mic\' /><br/><br/>'; } document.getElementById('copii<?php echo $rowCH['id_camera'].'_'.$nrL; ?>').innerHTML=cont; }">
                <?php for($i=0;$i<=3;$i++) { ?>
                <option value="<?php echo $i; ?>" <?php if($ocup['nr_copii'][$rowCH['id_camera']][$key]==$i || (!$ocup['nr_copii'][$rowCH['id_camera']][0] && $i==0)) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                <?php } ?>
              </select>
              <div id="copii<?php echo $rowCH['id_camera'].'_'.$nrL; ?>">
                <?php if(sizeof($ocup['varsta_min'][$rowCH['id_camera']][$key])>0) {
foreach($ocup['varsta_min'][$rowCH['id_camera']][$key] as $key1=>$value1) { echo 'Copil '.$key1.' interval: <input type="text" name="varsta_min['.$rowCH['id_camera'].']['.$nrL.']['.$key1.']" value="'.$value1.'" class="mic" /><br/><br/>'; }
  } else echo '&nbsp;'; ?>
              </div></td>
            <td align="center" valign="top" width="50"><a href="javascript: ;" onclick="sterge_linia_GO(<?php echo $key; ?>, <?php echo $rowCH['id_camera']; ?>);" class="buttons">X</a></td>
          </tr>
          <?php }
    }
  } ?>
        </table>
        <input type="hidden" name="nr_lin<?php echo $rowCH['id_camera']; ?>" id="nr_lin<?php echo $rowCH['id_camera']; ?>" value="<?php echo ++$nrL; ?>" /></td>
    </tr>
    <?php } @mysql_free_result($queCH); ?>
    <tr>
      <td align="center" valign="top" colspan="4"><input type="submit" name="salveaza" value="Salveaza" class="buttons" /></td>
    </tr>
  </table>
</form>
</body>
</html>