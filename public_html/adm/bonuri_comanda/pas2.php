<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body { font-family:Arial; color:#000; margin:0; padding:0; }
#main { width:198mm; margin:0; position:relative; }
.pozitie1 { position:relative; height:89mm; margin:0; padding:0 0 4mm 0; border-bottom:0.7mm dashed #000; }
.pozitie2 { position:relative; height:89mm; margin:4mm 0 0 0; padding:0 0 4mm 0; border-bottom:0.7mm dashed #000; }
.pozitie3 { position:relative; height:89mm; margin:4mm 0 0 0; padding:0; }
.date-firma { position:absolute; top:0; left:0; width:66mm; height:23.5mm; font-size:7pt; line-height:8pt; overflow:hidden; }
.date-firma .titlu { font-size:9pt; line-height:11pt; font-weight:bold; }
.date-agentie { position:absolute; top:0; right:0; width:94mm; height:23.5mm; text-align:center; font-size:9pt; font-weight:bold; line-height:11pt; overflow:hidden; }
.date-agentie img { margin:-4px 0; }
.title { position:absolute; top:24mm; left:0; width:198mm; height:5mm; text-align:center; font-size:18pt; line-height:5mm; font-weight:bold; overflow:hidden; }
fieldset { position:absolute; border:0.5mm solid #000; padding:1mm 2mm; margin:0; overflow:hidden; }
legend { font-size:10pt; line-height:10pt; font-weight:bold; }
.Fnume { top:30mm; left:0; width:93mm; height:18mm; font-size:9pt; line-height:10.5pt; }
.Fpersoane { top:30mm; left:100mm; width:36mm; height:18mm; font-size:9.5pt; line-height:11pt; text-align:center; }
.Fdestinatie { top:30mm; right:0; width:50.3mm; height:18mm; font-size:9.5pt; line-height:11pt; }
.Fcazare { top:51mm; left:0; width:136mm; height:11mm; font-size:9.5pt; line-height:11.5pt; }
.Fcheckin { top:51mm; right:0; width:50.3mm; height:11mm; font-size:9.5pt; line-height:20pt; text-align:center; }
.Fservicii { top:65mm; left:0; width:136mm; height:10mm; font-size:8.5pt; line-height:9.5pt; }
.Fconfirmare { top:78mm; left:0; width:93mm; height:8mm; font-size:9pt; line-height:12pt; }
.Fagent { top:78mm; left:100mm; width:36mm; height:8mm; font-size:9pt; line-height:12pt; }
.Fsemnatura { top:65mm; right:0; width:50.3mm; height:21mm; font-size:9.5pt; line-height:17pt; text-align:center; }
</style>
</head>

<body>
<div id="main">

<?php
$idv = $_GET['idv'];
?>

  <div class="pozitie3">
  <?php include "bonuri_comanda/continut_bon.php"; ?>
  </div>
  
  <br /><br />
  <a href="vouchere/pas3.php?idv=<?php echo $idv; ?>" class="buttons" target="_blank">Printeaza Voucher</a>
  
  <br /><br /><br />
  <div class="infos">
    <strong>Setari OBLIGATORII pentru printare!</strong><br />
    Imprimanta: <em>HP LaserJet P2015 DN</em><br />
    Browser: <em>Firefox (testat pe v. 9 si 10)</em><br />
    Print Preview > Page Setup > Format &amp; Options: <em>Portrait, Scale 100%, FARA shrink, FARA background</em><br />
  Print Preview > Page Setup > Margins &amp; Header/Footer: <em>Margins (1,1,1,1), Headers &amp; Footers (blank la toate)</em></div>

</div>
</body>
</html>
