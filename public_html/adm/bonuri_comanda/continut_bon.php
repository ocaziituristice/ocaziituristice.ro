<?php session_start(); include_once('../restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body { font-family:Calibri, "Trebuchet MS", Arial; font-size:10pt; line-height:130%; color:#000; margin:0; padding:0; }
.clear { content:"."; display:block; height:0; font-size:0; clear:both; visibility:hidden; }
.date-agentie { float:left; width:80mm; }
.date-agentie .titlu { font-weight:bold; text-transform:uppercase; }
.date-client { float:right; width:70mm; }
.date-client .titlu { font-weight:bold; margin-top:50px; }
h1 { font-size:24pt; margin:10px; padding:0; }
h2 { font-size:14pt; margin:10px; padding:0; }
table { border-collapse:collapse; border-color:#000; }
table td, table th { vertical-align:middle; text-align:left; padding:5px; }
table th { font-weight:bold; }
p { text-indent:3em; margin:0; padding:0 0 1.5em 0; text-align:justify; }
.semnatura-agentie { float:left; width:100mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
.semnatura-client { float:right; width:70mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
</style>
<?php include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>

<div class="date-agentie">
  <img src="/images/logo.png" /><br />
  <div class="titlu"><?php echo $contact_legal_den_firma; ?></div>
  Nr. reg. com.: <?php echo $contact_legal_reg_com; ?><br />
  Cod fiscal: <?php echo $contact_legal_cui; ?><br />
  Adresa sediu social: <?php echo $contact_legal_adresa; ?><br />
  Banca <?php echo $contact_legal_banca; ?><br />
  RON: <?php echo $contact_legal_cont_lei; ?><br />
  EURO: <?php echo $contact_legal_cont_euro; ?><br />
  Email: <?php echo $contact_email; ?><br />
  Tel: <?php echo $contact_telefon; ?>
</div>

<div class="date-client">
  <div class="titlu">Zamfir Gheorghe</div><br />
  Str. Stefanestii de Jos, com. Stefanestii de Jos<br />
  Jud. Ilfov<br /><br />
  Telefon: 0762 172 184<br />
  Email: rafy_boss2@yahoo.com
</div>

<div class="clear"></div>

<br />

<h1 align="center">BON DE COMANDA</h1>
<h2 align="center">Nr. 177 din data de 10.07.2012</h2>

<br />

<strong>Nr. persoane:</strong> 3 adulti + 1 copil<br />

<br />

<table width="100%" border="1">
  <tr>
    <th width="60%" style="text-align:center;">Categorii si servicii comandate</th>
    <th width="25%" style="text-align:center;">Observatii</th>
    <th width="15%" style="text-align:center;">Pret</th>
  </tr>
  <tr>
    <td>Pachet turistic Hotel Marvel 4*, Sunny Beach, perioada 01.08.2012 - 06.08.2012</td>
    <td></td>
    <td style="text-align:right;">670.00 EUR</td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Destinatie:</strong> Sunny Beach, Bulgaria<br />
      <strong>Data incepere sejur / Data terminare sejur:</strong> 01.08.2012 / 06.08.2012<br />
      <strong>Durata:</strong> 5 nopti / 6 zile
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Unitatea de cazare, categorie:</strong> Hotel Marvel, 4 stele<br />
      <strong>Tipul camerei:</strong> Camera Dubla<br />
      <strong>Serviciile de masa:</strong> All inclusive
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Persoanele pentru care se comanda serviciile (Numele, varsta, act identitate):</strong><br />
      Zamfir Gheorghe, data nasterii: 29.11.1984<br />
      Zamfir Emilia, data nasterii: 24.06.1985<br />
      Zamfir Mario-Rafael, data nasterii: 20.06.2006<br />
      Moisii Adriana, data nasterii: 11.10.1985
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Mijlocul de transport:</strong> -<br />
      <strong>Ruta:</strong> -<br />
      <strong>Transfer aeroport-hotel-aeroport:</strong> -
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Alte servicii:</strong> -<br />
      <strong>Excursii optionale:</strong> -
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Nr. minim persoane necesar pentru efectuarea programului:</strong> -<br />
      <strong>Data limita de anuntare a anularii:</strong> Conform contract<br />
      <strong>Regimul pasapoartelor, vizelor, conditii speciale de intrare si asistenta in tara destinatie:</strong> Carte de identitate / Pasaport<br />
      <strong>Asigurari medicale obligatorii/facultative etc:</strong> Nu sunt incluse
    </td>
  </tr>
</table>

<?php /*?><div style="page-break-after:always;"></div><?php */?>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Termenul pentru achitarea avansului:</strong> avans minim 30%<br />
      <strong>Termenul pentru achitarea restului de plata:</strong> 12.07.2012<br />
      <strong>Modalitatea de plata:</strong> Transfer bancar/Cash
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Alte mentiuni si observatii suplimentare / alte solicitari speciale:</strong><br />
      <br />
      <strong>Observatii:</strong> -
    </td>
  </tr>
</table>

<br />

<p>Turistul reprezentant, Zamfir Gheorghe, declar pe propria raspundere faptul ca reprezint, in conditiile legii, toti turistii enumerati in prezentul bon de comanda, si semnez prezentul act atat in numele meu, si in numele acestora. Ma oblig la plata eventualelor penalitati, in caz de renuntare. Am primit un exemplar al bonului.</p>
<p>Plata serviciilor al caror tarif este exprimat in alta moneda decat LEI se va efectua la cursul BNR din ziua platii, plus 2% comision de risc valutar.</p>

<br />
<br />
<br />
<br />

<div class="semnatura-agentie">
  Agentie,<br />
  <?php echo $contact_legal_den_firma; ?>
</div>

<div class="semnatura-client">
  Turist,<br />
  Zamfir Gheorghe
</div>

<div class="clear"></div>

</body>
</html>
