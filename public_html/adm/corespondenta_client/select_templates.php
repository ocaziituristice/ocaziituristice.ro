<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
?>
<script type="text/javascript" src="/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="/js/tinymce/editor_load.js"></script>
<?php
$template = $_REQUEST['template']; 
$idf=$_REQUEST['idf'];
$idc=$_REQUEST['idc'];

$sel_continut = "SELECT * FROM corespondenta_templates WHERE id_template='$template' ";
$que_continut = mysql_query($sel_continut) or die(mysql_error());
$row_continut = mysql_fetch_array($que_continut);

$sel_useri_fizice = "SELECT * FROM useri_fizice WHERE id_useri_fizice='$idf' ";
$que_useri_fizice = mysql_query($sel_useri_fizice) or die(mysql_error());
$row_useri_fizice = mysql_fetch_array($que_useri_fizice);

$turist_nume = $row_useri_fizice['prenume'].' '.$row_useri_fizice['nume'];

$sel_oferta = "SELECT
oferte.id_oferta,
oferte.denumire,
oferte.denumire_scurta,
oferte.descriere_oferta,
oferte.nota,
oferte.conditii_plata,
oferte.conditii_anulare,
hoteluri.nume AS denumire_hotel,
hoteluri.stele,
hoteluri.descriere,
hoteluri.descriere_scurta,
localitati.denumire AS denumire_localitate,
zone.denumire AS denumire_zona,
tari.denumire AS denumire_tara,
cerere_rezervare.early_booking,
cerere_rezervare.bon_comanda,
cerere_rezervare.plata_avans,
cerere_rezervare.plata_rest,
cerere_rezervare.procent_avans,
cerere_rezervare.pret,
cerere_rezervare.moneda,
cerere_rezervare.data AS cerere_data,
cerere_rezervare.nr_nopti AS cerere_nopti,
cerere_rezervare.nr_adulti AS cerere_nr_adulti,
cerere_rezervare.nr_copii AS cerere_nr_copii,
cerere_rezervare.data_adaugarii,
cerere_rezervare.oferta_denumire AS cerere_oferta_den,
cerere_rezervare.hotel_denumire AS cerere_hotel_den,
cerere_rezervare.hotel_categorie AS cerere_hotel_stele,
cerere_rezervare.transport AS cerere_transport,
cerere_rezervare.tip_masa AS cerere_tip_masa,
early_booking.end_date AS eb_data,
early_booking.discount AS eb_disc,
furnizori.denumire_agentie as furnizor_denumire_agentie,
furnizori.denumire_societate as furnizor_denumire_societate,
cerere_rezervare_date_facturare.denumire as denumire_date_facturare
FROM cerere_rezervare
LEFT JOIN oferte ON oferte.id_oferta=cerere_rezervare.id_oferta
LEFT JOIN hoteluri ON hoteluri.id_hotel=oferte.id_hotel
LEFT JOIN localitati ON localitati.id_localitate=hoteluri.locatie_id
LEFT JOIN zone ON zone.id_zona=localitati.id_zona
LEFT JOIN tari ON tari.id_tara=zone.id_tara
Left Join furnizori on cerere_rezervare.id_furnizor=furnizori.id_furnizor
LEFT JOIN early_booking ON early_booking.id_oferta = oferte.id_oferta
Inner Join cerere_rezervare_date_facturare ON cerere_rezervare.id_cerere = cerere_rezervare_date_facturare.id_cerere
WHERE cerere_rezervare.id_cerere='$idc'
";
$que_oferta = mysql_query($sel_oferta) or die(mysql_error());
$row_oferta = mysql_fetch_array($que_oferta);

$sel_financiar="SELECT
facturi.id_client,
facturi.id_rezervare,
facturi.valoare,
facturi.valuta,
facturi.status_tranzactie,
facturi.tip_tranzactie
FROM
facturi
WHERE
facturi.tip_operatie='client' and
facturi.id_rezervare =  '$idc'";
$que_financiar = mysql_query($sel_financiar) or die(mysql_error());
$intrari=0;
$iesiri=0;
while ($row_financiar = mysql_fetch_array($que_financiar)){
if($row_financiar['tip_tranzactie']=='intrare'){$intrari=$intrari+$row_financiar['valoare'];}
 if($row_financiar['tip_tranzactie']=='iesire'){
$iesiri=$iesiri+$row_financiar['valoare']; }
$valuta=$row_financiar['valuta'];
}

$rest_plata=$iesiri-$intrari;
$rest_plata=$rest_plata.' '.$valuta;

$id_oferta = $row_oferta['id_oferta'];
$hotel_denumire = $row_oferta['denumire_hotel'];
$hotel_stele = $row_oferta['stele'];
$hotel_descriere = nl2p($row_oferta['descriere_scurta']).nl2p($row_oferta['descriere']);
$oferta_denumire = $row_oferta['denumire'];
$oferta_denumire_scurta = $row_oferta['denumire_scurta'];
$tara_denumire = $row_oferta['denumire_tara'];
$zona_denumire = $row_oferta['denumire_zona'];
$localitate_denumire = $row_oferta['denumire_localitate'];
$furnizor_denumire_agentie=$row_oferta['furnizor_denumire_agentie'];
$furnizor_denumire_societate=$row_oferta['furnizor_denumire_societate'];

//$oferta_link = $sitepath.fa_link($tara_denumire).'/'.fa_link($localitate_denumire).'/'.fa_link_oferta($oferta_denumire).'-'.$row_oferta['id_oferta'].'.html';
$oferta_link = substr($sitepath,0,-1).make_link_oferta($localitate_denumire, $hotel_denumire, $oferta_denumire_scurta, $id_oferta);
$comentarii_link = substr($sitepath,0,-1).make_link_oferta($localitate_denumire, $hotel_denumire, NULL, NULL).'#comentarii';
$hoteluri_aceeasi_stea = $sitepath.'sejur-'.fa_link($tara_denumire).'/'.fa_link($zona_denumire).'/'.fa_link($localitate_denumire).'/'.'?optiuni=da&amp;stele='.$hotel_stele;
$descriere_oferta = nl2br($row_oferta['descriere_oferta']);
$nota = nl2br($row_oferta['nota']);
$conditii_plata = nl2br($row_oferta['conditii_plata']);
$conditii_anulare = nl2br($row_oferta['conditii_anulare']);
$plata_avans = nl2br($row_oferta['plata_avans']);
$plata_rest = nl2br($row_oferta['plata_rest']);
if(isset($row_oferta['procent_avans'])) $procent_avans = $row_oferta['procent_avans'] / 100;
$cerere_pret = $row_oferta['pret'];
$cerere_bon_comanda = $row_oferta['bon_comanda'];
$cerere_moneda = $row_oferta['moneda'];
$cerere_data = $row_oferta['cerere_data'];
$cerere_nopti = $row_oferta['cerere_nopti'];
$cerere_nr_adulti = $row_oferta['cerere_nr_adulti'];
$cerere_nr_copii = $row_oferta['cerere_nr_copii'];
$cerere_oferta_den = $row_oferta['cerere_oferta_den'];
$cerere_hotel_den = $row_oferta['cerere_hotel_den'];
$cerere_hotel_stele = $row_oferta['cerere_hotel_stele'];
$cerere_transport = $row_oferta['cerere_transport'];
$cerere_tip_masa = $row_oferta['cerere_tip_masa'];
$early_booking = $row_oferta['early_booking'];
$early_booking_discount = $row_oferta['eb_disc'];
$early_booking_data = date('d.m.Y',strtotime($row_oferta['eb_data']));
$data_adaugarii = strtotime($row_oferta['data_adaugarii']);

$sel_servicii_incluse = "SELECT denumire FROM oferte_servicii WHERE id_oferta='$id_oferta' AND tip_serv='Servicii incluse' ORDER BY ordonare";
$que_servicii_incluse = mysql_query($sel_servicii_incluse) or die(mysql_error());
$servicii_incluse = '';
while($row_servicii_incluse = mysql_fetch_array($que_servicii_incluse)) {
	$servicii_incluse .= '- '.$row_servicii_incluse['denumire'].'<br>';
}

$sel_servicii_neincluse = "SELECT denumire, value, moneda, obligatoriu FROM oferte_servicii WHERE id_oferta='$id_oferta' AND tip_serv='Servicii neincluse' ORDER BY ordonare";
$que_servicii_neincluse = mysql_query($sel_servicii_neincluse) or die(mysql_error());
$servicii_neincluse = '';
while($row_servicii_neincluse = mysql_fetch_array($que_servicii_neincluse)) {
	$servicii_neincluse .= '- '.$row_servicii_neincluse['denumire'];
	if($row_servicii_neincluse['value']) {
		$servicii_neincluse .= ' - <strong>'.$row_servicii_neincluse['value'].' '.moneda($row_servicii_neincluse['moneda']).'</strong>';
	}
	if($row_servicii_neincluse['obligatoriu']=='da') {
		$servicii_neincluse .= ' - <em class="red bold">Obligatoriu</em>';
	} else {
		$servicii_neincluse .= ' - <em class="blue">Optional</em>';
	}
	$servicii_neincluse .= '<br>';
}

$subiect = $row_continut['subiect'].' '.$hotel_denumire;
if($row_continut['nume_template']=='Trimite voucher' ||$row_continut['nume_template']=='Trimite acte' || $row_continut['nume_template']=='Confirmare incasare avans') $subiect = $subiect." - ". $row_oferta['denumire_date_facturare'];
if($row_continut['nume_template']=='Drum bun') $subiect = $row_continut['subiect'];

?>
<table cellpadding="0" cellspacing="0" width="100%" class="default">
  <tr>
    <th align="right" valign="middle"><label for="subiect">Subiect:</label></th>
    <td align="left" valign="middle"><input type="text" id="subiect" name="subiect" value="<?php echo $subiect; ?>" class="mare" /></td>
  </tr>
  <tr>
    <th align="right" valign="middle" width="130"><label for="continut">Continut:</label></th>
    <td align="left" valign="middle" style="padding-bottom:10px;"><textarea id="continut" name="continut" class="tinymce" style="width:600px; height:600px;"><?php if($row_continut['continut']) include_once($_SERVER['DOCUMENT_ROOT'].'/mail/templates/cereri/'.$row_continut['continut']); ?></textarea></td>
  </tr>
</table>
<?php 
mysql_free_result ( $que_oferta );
mysql_free_result ( $que_financiar );
?>