<?php session_start(); include_once('../restrictionare.php');
$id_cerere=$_GET['idc']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body { font-family:Calibri, "Trebuchet MS", Arial; font-size:10.5pt; line-height:130%; color:#000; margin:5px; padding:15px; padding-bottom:25px; padding-top:25px }
.clear { content:"."; display:block; height:0; font-size:0; clear:both; visibility:hidden; }
h1 { font-size:18pt; margin:10px; padding:0; line-height:1.2em; }
.semnatura-agentie { float:left; width:100mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
.semnatura-client { float:right; width:70mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
</style>
<?php include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>
<?php
$sel_boncom = "SELECT * FROM cerere_rezervare
LEFT JOIN cerere_rezervare_date_facturare ON cerere_rezervare_date_facturare.id_cerere = cerere_rezervare.id_cerere
WHERE cerere_rezervare.id_cerere = '$id_cerere'";
$que_boncom = mysql_query($sel_boncom) or die(mysql_error());
$row_boncom = mysql_fetch_array($que_boncom);
?>

<div align="justify">

<h1 align="center">CONTRACT<br />DE COMERCIALIZARE A PACHETULUI DE SERVICII TURISTICE<br />
  Nr.rezervare OCZ<?php echo $row_boncom['id_cerere']; ?> din <?php echo date('d.m.Y',strtotime($row_boncom['data_documente'])); ?></h1>
<br />
<strong>Părțile contractante:</strong><br />
<br />
<strong><?php echo $contact_legal_den_firma; ?></strong> - Agenție Tour Operator, cu sediul social situat în <?php echo $contact_legal_adresa; ?>, înregistrata la O.R.C. de pe lângă Tribunalul București cu nr. <?php echo $contact_legal_reg_com; ?>, CUI <?php echo $contact_legal_cui; ?>, cont nr. <?php echo $contact_legal_cont_lei; ?> - LEI și <?php echo $contact_legal_cont_euro; ?> - EURO deschis la banca <?php echo $contact_legal_banca; ?>, titulară a agenției de turism <?php echo $contact_legal_den_agentie; ?> cu punct de lucru în <?php echo $contact_adresa; ?>, tel: <?php echo $contact_telefon; ?>, e-mail <?php echo $contact_email; ?>,  cu licența de turism tur-operator: 6250 / 21.12.2011, Brevet de Manager în Activitatea de Turism nr. 19004/31.08.2011, reprezentată prin <?php echo $contact_legal_reprezentant; ?> în calitate de <?php echo $contact_legal_calitate; ?>, denumită în continuare <strong>Agenție</strong><br />
<br />
și <strong>Turist</strong><br />
<br />
<strong><?php echo $row_boncom['denumire']; ?></strong>, 
<?php if($row_boncom['tip_persoana']=='persoana_fizica') {
	echo 'cu domiciliul in '.$row_boncom['adresa'].', '.$row_boncom['oras'].', ';
} elseif($row_boncom['tip_persoana']=='persoana_juridica') {
	echo 'cu sediul social situat in '.$row_boncom['adresa'].', '.$row_boncom['oras'].', avand Cod Unic de Inregistrare '.$row_boncom['cui_cnp'].', Numar de ordine in Registrul Comertului '.$row_boncom['nr_reg_comert'].', IBAN '.$row_boncom['iban'].', deschis la '.$row_boncom['banca'].', ';
} ?>
email: <?php echo $row_boncom['email']; ?>, telefon: <?php echo $row_boncom['telefon']; ?> în calitate de contractant al pachetului de servicii turistice, denumit în continuare Turist, a convenit la încheierea prezentului contract, în conformitate cu prevederile O.G. nr. 107/1999 privind activitatea de comercializare a pachetelor de servicii turistice, aprobată cu modificari și completări prin Legea nr. 631/2001.<br />
<br />
<strong>I. Dispoziţii generale</strong><br />
1. Agenţia comercializează pachete de servicii turistice în următoarele variante:<br />
a) servicii turistice la cerere, care se referă la programele organizate în mod special, ca urmare a solicitărilor turiştilor, denumite în continuare servicii la cerere;<br />
b) servicii turistice preorganizate de agenţia de turism şi puse la dispoziţia clientului prin materiale de prezentare de tipul: cataloage, broşuri, pliante şi alte oferte letrice, denumite în continuare servicii din oferta standard;<br />
c) servicii turistice interne, comercializate pe bază de vouchere de servicii turistice interne sau bilete de odihnă şi/sau de tratament, denumite în continuare bilete de odihnă şi/sau de tratament. Aceste servicii reprezintă un caz particular al serviciilor turistice din oferta standard.<br />
Prezentul contract deţine prevederi comune celor trei variante de servicii turistice, precum şi prevederi specifice fiecăreia dintre ele.<br />
2. Bonul de comandă, confirmarea de servicii, biletul de odihnă şi/sau de tratament, precum şi oferta standard a agenţiei de turism expusă în materialele de prezentare fac parte integrantă din prezentul contract. Modelul bonului de comandă este prevăzut în anexă la prezentul contract.<br />
<br />
<strong>II. Apariţia raporturilor contractuale</strong><br />
Prezentul contract se consideră încheiat în momentul semnării lui de către Agenţie şi de către Turist.<br />
<br />
<strong>III. Obiectul contractului</strong><br />
1. Obiectul contractului îl constituie vânzarea de către Agenţie a uneia dintre variantele de servicii turistice prevăzute la cap. I, în schimbul plăţii preţului.<br />
<strong>Servicii solicitate si confirmate:</strong><br />
- Pachet turistic conform BON DE COMANDA rezervarea nr. OCZ<?php echo $row_boncom['id_cerere']; ?> din data <?php echo date('d.m.Y',strtotime($row_boncom['data_documente'])); ?>
<?php /*?>- Pachet turistic: <?php echo $row_boncom['oferta_denumire']; ?>, <?php echo $row_boncom['hotel_localitate']; if($row_boncom['hotel_localitate']!='Circuit') echo ', '.$row_boncom['hotel_tara']; ?>, <?php echo date('d.m.Y',strtotime($row_boncom['data'])); ?> / <?php echo date('d.m.Y',strtotime($row_boncom['data'].'+'.($row_boncom['nr_zile']-1).' days')); ?><br />
<?php if($row_boncom['early_booking']=='da') echo '- Discount tip: Early Booking<br />'; ?>
- Destinatie: <?php echo $row_boncom['hotel_localitate']; ?>, <?php echo $row_boncom['hotel_tara']; ?><br />
- Unitatea de cazare: <?php echo $row_boncom['hotel_denumire']; if($row_boncom['hotel_localitate']!='Circuit') echo $row_boncom['hotel_categorie'].' stele'; ?><br />
- Tip camera: <?php echo $row_boncom['nume_camera']; ?><br />
- Data incepere sejur / Data terminare sejur: <?php echo date('d.m.Y',strtotime($row_boncom['data'])); ?> / <?php echo date('d.m.Y',strtotime($row_boncom['data'].'+'.($row_boncom['nr_zile']-1).' days')); ?><br />
- Durata: <?php echo $row_boncom['nr_nopti']; ?> nopti / <?php echo $row_boncom['nr_zile']; ?> zile<br />
- Servicii masa: <?php echo $row_boncom['tip_masa']; ?><br />
- Mijloc de transport: <?php echo $row_boncom['transport']; ?><br />
- Ruta: <?php echo $row_boncom['ruta']; ?><br />
- Transfer aeroport-hotel-aeroport: <?php echo $row_boncom['transfer']; ?><br />
- Alte servicii: <?php echo nl2br($row_boncom['alte_servicii']); ?><br />
- Excursii optionale: <?php echo nl2br($row_boncom['excursii_optionale']); ?><br /><?php */?>
<br />
2. Caracteristicile serviciilor turistice la care se obligă Agenţia se află descrise în materialele de prezentare (cataloage, broşuri, pliante, oferte letrice), în cazul ofertei standard şi a biletelor de odihnă şi/sau de tratament, sau în confirmarea de servicii, în cazul serviciilor la cerere.<br />
3. Orice alte servicii turistice neînscrise în materialele de prezentare sau în confirmarea de servicii nu fac obiectul prezentului contract şi nu atrag răspunderea Agenţiei.<br />
<br />
<strong>IV. Condiții speciale - early booking, rezervări timpurii, last minute</strong><br />
1. În cazul renunțării la pachetele de servicii turistice care includ bilete de avion (dacă renunțarea se face după ce biletele de avion au fost emise, indiferent de numărul de zile rămase până la momentul plecării), restituirea contravalorii biletelor de avion și a taxelor de aeroport se face în funcție de condițiile de renunțare / penalizare ale companiei aeriene.<br />
2. În cazul în care turistul renunţă din vina sa la pachetul de servicii turistice la care a beneficiat de reducerea de Early Booking (rezervare din timp) sau last minute, atunci se aplică condiții speciale de renunțare, și anume penalizare 100% în caz de anulare, indiferent de momentul anulării rezervării. Pentru anumite oferte termenele de plată si de renunțare/penalizare pot fi diferite - caz în care acestea vor fi menționate pe oferte. Prin semnarea contractului, turistul recunoaște că a fost informat și acceptă termenele de renunțare/penalizare speciale aferente pachetului de servicii turistice achiziționat.<br />
3. Neconfirmarea cerințelor speciale (fără plată suplimentară) și cu titlu informativ nu poate fi considerat motiv pentru anularea rezervărilor din partea Turistului.<br />
4. Nu se acceptă modificări la rezervarile cu early booking (modificări de nume, structură cameră, perioadă, pachet).<br />
<br />
<strong>V. Preţul</strong><br />
1. Preţul contractului este de <strong><?php echo $row_boncom['pret']; ?> <?php echo $row_boncom['moneda']; ?></strong> pentru <?php echo $row_boncom['nr_adulti']; ?> adulți și <?php echo $row_boncom['nr_copii']; ?> copii (în cazul plăţilor în RON, se calculează cursul BNR) şi cuprinde costul serviciilor turistice efective, comisionul Agenţiei şi T.V.A.<br />
2. Preţul este publicat în cadrul ofertei standard a Agenţiei pentru serviciile din oferta standard şi pentru biletele de odihnă şi/sau de tratament sau este înscris în confirmarea de servicii, în cazul serviciilor la cerere.<br />
3. Preţul contractului poate fi modificat, în sensul majorării sau micşorării, după caz, numai dacă modificarea are loc ca urmare a urmatoarelor variaţii:<br />
- costurilor de transport, inclusiv costul carburantului;<br />
- redevenţelor şi taxelor aferente serviciilor de aterizare, debarcare/îmbarcare în porturi şi aeroporturi şi ale taxelor de turist;<br />
- cursurilor de schimb valutar aferente pachetului de servicii turistice contractat.<br />
4. În cazul în care preţurile stabilite în contract sunt majorate cu peste 10%, indiferent de motivele majorării, turistul poate rezilia contractul fără nici o obligaţie faţă de Agenţie, aceasta din urmă având obligaţia de a rambursa imediat turistului toate sumele aferente călătoriei turistice plătite de acesta, inclusiv comisionul.<br />
<br />
<strong>VI. Modalităţi de plată</strong><br />
Modalităţile de plată a preţului contractului sunt:<br />
1. Avansul se achită la înscriere şi reprezintă minim <strong> <?php echo $row_boncom['procent_avans']; ?>% din preţul contractului</strong> pana la date de <?php echo date("d.m.Y", strtotime($row_boncom['plata_avans'])); ?>.<br />
<?php /*?>2. Plata finală a pachetului de servicii se face cu minim 16 zile înainte de data începerii programului/pachetului de servicii turistice.<br /><?php */?>
2. Plata finală a pachetului de servicii se face până la data de <?php echo date("d.m.Y", strtotime($row_boncom['plata_rest'])); ?>.<br />
3. Agenţia poate aplica sistemul de vânzare în rate, în baza acordului cu turistul, cu condiţia ca turistul să respecte termenele de plată a avansului şi a plăţii finale.<br />
<br />
<strong>VII. Drepturile şi obligaţiile Agenţiei</strong><br />
1. În cazul în care Agenţia este nevoită să modifice una dintre prevederile esenţiale ale contractului, are obligaţia să informeze turistul cu cel puţin 7 zile înainte de data plecării.<br />
2. În cazul în care o parte importantă din serviciile turistice prevăzute în contract nu este realizată sau agenţia de turism organizatoare constată că nu le va putea realiza, Agenţia este obligată:<br />
- să ofere turistului alternative corespunzătoare în vederea continuării călătoriei turistice, fără majorarea preţului, respectiv serviciile turistice oferite să fie de aceeaşi calitate şi cantitate;<br />
- să restituie turistului sumele ce reprezintă diferenţa dintre serviciile turistice achitate şi cele efectiv prestate în timpul călătoriei turistice;<br />
- în cazul în care nu pot fi oferite turistului alternative corespunzătoare sau acesta nu le acceptă din motive întemeiate, să asigure fără costuri suplimentare transportul retur al turistului la locul de plecare ori în alt loc agreat de acesta.<br />
3. Agenţia este răspunzătoare pentru buna executare a obligaţiilor asumate prin contract, inclusiv în situaţia în care aceste obligaţii trebuiau îndeplinite de către o altă agenţie de turism sau de către alţi prestatori de servicii, cu excepţia următoarelor cazuri:<br />
- când neîndeplinirea sau îndeplinirea defectuoasă a obligaţiilor asumate prin contract se datorează turistului;<br />
- când neîndeplinirea obligaţiilor se datorează unor cauze de forţă majoră.<br />
În situaţia în care neexecutarea obligaţiilor contractuale se datorează agenţiei de turism organizatoare, Agenţia are dreptul să cheme în garanţie agenţia de turism organizatoare pentru plata despăgubirilor datorate turistului.<br />
4. Agenţia are obligaţia să furnizeze turistului, în termen de 2 zile înainte de data plecării, următoarele informaţii:<br />
- orarele, locurile escalelor şi legăturile, precum şi, după caz, locul ce urmează să fie ocupat de turist în fiecare dintre mijloacele de transport incluse în contract;<br />
- denumirea, sediul/adresa, numerele de telefon şi de fax ale reprezentanţei locale a organizatorului şi/sau a detailistului ori, în lipsa acesteia, cele ale autorităţilor locale care pot ajuta turistul în caz de nevoie; în cazul în care aceste reprezentanţe sau autorităţi locale nu există, turistul trebuie să dispună de un număr de apel de urgenţă sau de orice altă informaţie care să îi permită contactarea organizatorului şi/sau a detailistului;<br />
5. Excursiile opţionale se efectuează la faţa locului cu agenţii locale. Sumele încasate pentru aceste excursii nu se fac în numele şi pentru Agenţia <?php echo $contact_legal_den_agentie; ?>. Preţurile la excursiile opţionale pot fi mai mari decât cele cumpărate de la recepţia hotelurilor aceasta datorându-se faptului că persoanele participante vor avea la dispoziţie un mijloc de transport care îi va duce şi îi va aduce la hotelul respectiv, ghidul excursiei şi după caz ghid local.<br />
- pentru călătoriile minorilor neînsoţiţi de părinţi, informaţii care să permită stabilirea unui contact direct cu copilul sau cu responsabilul de la locul cazării copilului;<br />
- posibilitatea semnării unui contract de asigurare facultativă, care să acopere taxele de transfer ale turistului, sau a unui contract de asistenţă, care să acopere taxele de repatriere în caz de accidentare ori de boală.<br />
<br />
<strong>VIII. Drepturile şi obligaţiile turistului</strong><br />
1. Dacă turistul nu poate să participe la călătoria turistică, acesta poate să cesioneze contractul unei terţe persoane (cu excepţia rezervărilor Early Booking, Last Minute) care îndeplineşte toate condiţiile prevăzute în pachetul de servicii turistice contractat, cu obligaţia de a anunţa Agenţia cu minim 5 zile înaintea datei de plecare. Turistul care cedează pachetul său de servicii, precum şi cesionarul sunt responsabili în mod solidar la plata preţului călătoriei şi a costurilor suplimentare posibile (taxă de viză, bilet de avion, etc.) apărute cu ocazia acestei cedări.<br />
Pentru modificarea de nume într-o rezervare se percepe o taxă de 25 Euro/persoană.<br />
2. Dacă preţurile stabilite în contract sunt majorate cu peste 10%, turistul poate rezilia contractul, având dreptul la rambursarea de către Agenţie a sumelor plătite.<br />
3. Turistul este obligat să comunice Agenţiei, în termen de 3 zile calendaristice de la primirea înştiinţării prevăzute la cap. VI pct. 1, hotărârea sa de a opta pentru:<br />
a) rezilierea contractului fără plata penalităţilor;<br />
sau<br />
b) acceptarea noilor condiţii ale contractului.<br />
4. Dacă Agenţia anulează călătoria înaintea datei de plecare, turistul are dreptul:<br />
a) să accepte la acelaşi preţ un alt pachet de servicii turistice de calitate echivalentă sau superioară,<br />
b) să accepte un pachet de servicii turistice de calitate inferioară, cu rambursarea diferenţei de preţ;<br />
c) să i se ramburseze toate sumele achitate în virtutea contractului.<br />
5. În toate cazurile menţionate la cap. VII pct. 4. turistul are dreptul să solicite Agenţiei şi o despăgubire pentru neîndeplinirea prevederilor contractului iniţial, cu excepţia cazurilor în care:<br />
a) anularea s-a făcut datorită nerealizării numărului minim de persoane care să asigure coeficientul de umplere a mijlocului de transport peste limita cheltuielilor, iar Agenţia a informat turistul în termenul menţionat în contract, termen care nu poate fi mai mic de 7 zile calendaristice premergătoare datei plecării;<br />
b) anularea s-a datorat unui caz de forţă majoră (circumstanţe imprevizibile, independente de voinţa celui care le invocă şi ale căror consecinţe nu au putut fi evitate);<br />
c) anularea s-a făcut din vina turistului - caz în care se aplică prevederile cap. IX din prezentul contract.<br />
6. Turistul are dreptul să rezilieze contractul în orice moment iar în cazul în care rezilierea îi este imputabilă, este obligat să despăgubească Agenţia pentru prejudiciul creat acesteia, conform prevederilor cap. IX din prezentul contract.<br />
7. Dacă turistul, după confirmarea serviciilor, solicită schimbarea datei de plecare, schimbarea hotelului sau a oricărora dintre servicii, aceasta echivalează cu rezilierea contractului, cu aplicarea penalităţilor legale la momentul respectiv şi încheierea unui nou contract.<br />
8. Dacă după confirmarea serviciilor se solicită modificarea/completarea numelui/prenumelui turistului sau schimbarea structurii camerelor, se percepe o taxă de 25 euro/modificare.<br />
9. În cazul în care turistul care a intrat pe teritoriul statului în care se realizează pachetul de servicii turistice refuză să se mai întoarcă în România şi autorităţile din ţara respectivă fac cheltuieli de orice natură cu acesta, turistul respectiv are obligaţia de a suporta toate aceste cheltuieli.<br />
10. Turistul are obligaţia de a respecta programul de acordare a serviciilor turistice, respectiv: cazarea se face începând cu ora de check-in a zilei de sosire şi se termină la ora de check-out a ultimei zilei înscrise pe voucher / biletul de odihnă şi/sau de tratament. Sosirea la cazare mai devreme de ora respectivă sau plecarea mai târziu de ora respectivă nu îndreptăţeşte utilizarea camerelor timp mai îndelungat.<br />
11. Turistul este obligat să achite la recepţia unităţii hoteliere taxa de staţiune, taxa de salubrizare, precum şi alte taxe locale, fără a putea pretinde despăgubiri sau returnarea sumelor de la Agenţie.<br />
12. Turistul este obligat să prezinte la recepţia unităţii hoteliere actele sale de identitate, precum şi documentul de călătorie eliberat (voucher, bilet de odihnă şi/sau de tratament) în vederea acordării serviciilor turistice, să păstreze integritatea bunurilor din unităţile de cazare, alimentaţie publică, mijloace de transport, în caz contrar urmând să suporte contravaloarea pagubelor produse.<br />
13. În cazul rezervărilor Rent-a-car, titularul rezervării are obligația să dețină un card de credit internațional pentru plata unei garanții solicitate de companie în momentul ridicării autoturismului. În cazul în care compania de Rent-a-car refuză predarea autoturismului către client din motive legate de cardul de credit sau permis de conducere, Agenția nu poate fi facută răspunzătoare.<br />
14. Turistul se obligă să respecte toate regulile impuse de compania aeriană pe durata zborului. În cazul în care compania aeriană sau turoperatorul nu stabilește alte limite, greutatea maximă a bagajelor este de 20 kg + 5 kg bagaj de mână la companiile de linie. Pentru companiile Low Cost greutatea bagajelor este contra cost și va fi in funcție de compania aeriană și preferințele turistului.<br />
15. Dacă pentru efectuarea călătoriei este necesară îndeplinirea de către turist a unor formalități suplimentare (de ex. călătorie împreună cu minori, situația în care numele turistului este schimbat ca urmare a căsătoriei, desfacerii, etc.) acesta va îndeplini toate cerințele legale. Turistul este obligat să se informeze și să respecte regulile de părăsire a teritoriului țării, impuse de Poliția de Frontieră (www.politiadefrontiera.ro).<br />
16. În cazul în care o singură persoană angajează servicii pentru un număr mai mare de turiști, condițiile contractului se extind în mod automat asupra întregului grup pentru care s-au achitat serviciile. Prin semnarea contractului sau prin rezervarea pachetului turistic, turistul declară faptul, că a luat la cunoștință și acceptă toate condițiile de călătorie prevăzute în ofertă.<br />
<br />
<strong>IX. Asigurări</strong><br />
Turistul este asigurat pentru riscul de insolvabilitate şi/sau de faliment al Agenţiei <?php echo $contact_legal_den_agentie; ?> la Societatea de asigurări <?php echo $contact_legal_asigurator; ?> cu poliță <?php echo $contact_legal_asigurare; ?>. Condiţiile de despăgubire de către societatea de asigurare sunt:<br />
1. În cazul în care Agenţia nu efectuează repatrierea turistului, acesta are obligaţia de a anunţa imediat societatea de asigurare prin telefon, fax sau e-mail. În această situaţie societatea de asigurare nu are obligaţia de a achita imediat contravaloarea cheltuielilor de repatriere, ci de a o rambursa după întoarcerea turistului în România, în condiţiile poliţei de asigurare încheiate între Agenţie şi societatea de asigurare.<br />
2. În cazul în care turistul solicită de la Agenţie contravaloarea sumelor achitate şi/sau a cheltuielilor de repatriere, acesta trebuie să trimită documentele justificative către Agenţie prin scrisoare recomandată cu confirmare de primire. Turistul are obligaţia să păstreze fotocopii de pe respectivele documente justificative. Turistul poate solicita Agenţiei rambursarea sumelor achitate şi/sau a cheltuielilor de repatriere, în termen de 15 (cincisprezece) zile calendaristice de la data încheierii derulării pachetului de servicii turistice sau de la data repatrierii.<br />
3. Turistul are obligaţia de a notifica societăţii de asigurare, prin scrisoare recomandată cu confirmare de primire, în legătură cu solicitarea adresată Agenţiei privind rambursarea sumelor achitate şi/sau a cheltuielilor de repatriere, în termen de 5 (cinci) zile calendaristice de la data confirmării de primire prevăzute la cap. X pct. 2.<br />
4. În cazul în care, în termen de 15 zile calendaristice de la data confirmării de primire a documentelor justificative de către Agenţie turistul nu a primit sumele solicitate de la aceasta, are loc evenimentul asigurat.<br />
5. În termen de 10 (zece) zile calendaristice de la data producerii evenimentului asigurat, turistul are obligaţia de a transmite societăţii de asigurare, prin scrisoare recomandată cu confirmare de primire, cererea de despăgubire însoţită de documentele justificative.<br />
6. Documentele justificative constau în:<br />
a) contractul de comercializare a pachetului de servicii turistice;<br />
b) confirmările de primire precizate la pct. 2, 3 şi 5 din prezentul capitol;<br />
c) fotocopiile de pe documentele de plată a avansului (chitanţe, ordine de plată etc.), în cazul cererilor de rambursare a sumelor achitate de turist;<br />
d) fotocopiile de pe documentele de transport şi cazare, în cazul cererilor de rambursare a cheltuielilor de repatriere.<br />
Societatea de asigurare are dreptul să solicite turistului şi alte documente justificative.<br />
7. Despăgubirea nu poate depăşi suma achitată de turist în contractul de comercializare a pachetului de servicii turistice, precum şi sumele necesare repatrierii acestuia, cu respectarea prevederilor cap. IX pct. 2.<br />
8. Din despăgubire se scade franşiza menţionată pe poliţa de asigurare.<br />
9. Despăgubirea va fi plătită în termen de 30 (treizeci) de zile calendaristice de la data primirii de către societatea de asigurare a documentelor justificative de la turist.<br />
10. În cazul în care după plata despăgubirii Agenţia plăteşte debitul către turist, acesta are obligaţia de a restitui asigurătorului despăgubirea primită, în termen de 5 (cinci) zile lucrătoare de la data primirii de la Agenţie a sumelor reprezentând debitul.<br />
<br />
<strong>X. Renunţări, penalizări, despăgubiri</strong><br />
1. În cazul în care turistul renunţă din vina sa la pachetul de servicii turistice care face obiectul prezentului contract, în cazul serviciilor la cerere sau al ofertei standard, el datorează Agenţiei penalizări după cum urmează:<br />
a) comisionul agenției, taxa de rezervare și toate cheltuielile legate de rezervare (emitere bilete de avion, plata serviciilor la sol, taxa de viză, asigurarea medicală de călătorie, etc.), dar nu mai putin de <strong>30% din prețul pachetului</strong> de servicii, dacă renunţarea se face cu mai mult de 40 zile calendaristice înainte de plecare;<br />
b) <strong>penalizare 50% din preţul pachetului</strong> de servicii, dacă renunţarea se face cu 16-40 zile înainte de plecare;<br />
c) <strong>penalizare 80% din preţul pachetului</strong> de servicii, dacă renunţarea se face în intervalul de 16 zile înainte de plecare, cu condiția rezervării pentru pachetele de catalog, neincluzând condițiile speciale (early booking, rezervări timpurii)<br />
d) <strong>penalizare 100% din preţul pachetului</strong> de servicii, dacă:<br />
- renunţarea se face într-un interval mai mic de 15 zile înainte de plecare<br />
- pentru neprezentarea turistului la program sau întârziere<br />
- dacă turistul nu este lăsat să treaca la una din frontiere din motive care sunt legate exclusiv de persoana sa sau din motive care nu pot fi imputate Agenției<br />
- dacă turistul nu respectă condițiile de vânzare ale Agenției, dacă prezintă acte incomplete sau false, nu are pașaport sau nu achită excursia în termenele stabilite<br />
e) <strong>penalizare 100% din prețul pachetului</strong> de servicii dacă rezervarea este în regim Early Booking (rezervări din timp). Aceeași penalizare se aplică și în cazul oricărei modificări (nume, dată, hotel, etc.) în regim Early Booking.<br />
Aceste penalizări se aplică în toate cazurile, cu excepția celor în care programul valorificat / confirmat are propriile reguli de achitare, anulare, penalizare (ex. programe de Paște, Crăciun, Revelion, rezervări individuale, croaziere, destinații exotice). În acest caz se aplică regulile speciale ale fiecarui program în parte, astfel:<br />
- pentru cursele charter - condiții speciale fiecărei destinații, speciale fiecărei destinații , specificate în Programul Turistic și broșură (dacă este cazul);<br />
- pentru excursii - condiții speciale fiecărei destinații, specificate în Programul Turistic;<br />
- pentru rezervările efectuate la agențiile organizatoare străine (germane, austriece, olandeze, etc.) și croziere - condițiile specifice fiecărui sistem de rezervări on-line (Der Tour, Amadeus-condițiile IATA, Schmetterling, etc.);<br />
- pentru acțiuni speciale (campionate sportive, evenimente culturale, etc) - condiții specifice menționate în Programul Turistic furnizat și broșura (dacă este cazul)<br />
<strong>Penalizările de mai sus NU se aplică în cazul în care mai jos sunt trecute condiții speciale, caz în care se vor respecta acestea.</strong><br />
<?php echo nl2br($row_boncom['conditii_anulare']); ?><br />
2. În cazul în care o ambasadă refuză să acorde viza de intrare pentru efectuarea pachetului de servicii, turistului i se vor reţine toate taxele achitate de Agenţie prestatorilor direcţi, precum şi cheltuielile de operare proprii acesteia.<br />
3. Penalizările echivalente cu preţul contractului se aplică şi în cazul în care turistul nu ajunge la timp la aeroport sau la locul de plecare, dacă nu poate pleca în călătorie pentru că nu are actele în regulă sau dacă este întors de la graniţă de către poliţia de frontieră.<br />
4. Turistul trebuie să depună în scris cererea de renunţare la pachetul de servicii turistice, cu număr de înregistrare la Agenţia la care a achitat serviciile. În caz contrar cererea de renunţare nu este luată în considerare.<br />
5. Agenţia va acorda despăgubiri în funcţie de gradul de nerespectare a obligaţiilor din contract, despăgubiri ce nu pot depăşi preţul pachetului de servicii turistice prevăzut în contract.<br />
<br />
<strong>XI. Reclamaţii</strong><br />
1. În cazul în care turistul este nemulţumit de serviciile turistice primite, acesta are obligaţia de a întocmi la faţa locului o sesizare în scris, clar şi explicit cu privire la deficienţele constatate la faţa locului, legate de realizarea pachetului de servicii turistice contractat, ce se va transmite prompt atât prestatorului de servicii (conducerii hotelului, restaurantului etc), cât şi Agenţiei. Pentru a fi luată în considerare, reclamaţia trebuie să fie semnată de reprezentantul prestatorului de servicii (hotel, restaurant etc.), care va constata realitatea şi veridicitatea reclamaţiei.<br />
2. Atât Agenţia cât şi prestatorul de servicii vor acţiona imediat pentru soluţionarea sesizării. În cazul în care sesizarea nu este soluţionată sau este soluţionată parţial, turistul va depune la sediul Agenţiei o reclamaţie în scris, în termen de maximum 3 zile calendaristice de la încheierea călătoriei, Agenţia urmând ca în termen de 30 zile calendaristice să comunice turistului răspunsul la reclamație, în condiţiile prezentului contract.<br />
În cazul în care turistul nu respecta termenul și condițiile prevăzute pentru depunerea reclamației scrise, Agenția este exonerată de orice obligație față de turist.<br />
<br />
<strong>XII. Dispoziţii finale:</strong><br />
<?php if($row_boncom['observatii_contract']) echo nl2br($row_boncom['observatii_contract']).'<br />'; ?>
Prezentul contract a fost încheiat în două exemplare, câte unul pentru fiecare parte.<br />
<strong>Turistul: Declar pe proprie răspundere că am luat la cunoștință toate clauzele prevăzute în Anexa la Contract (Bonul de Comandă) și sunt de acord cu ele.</strong><br />
<br />
<br />
<br />

<div class="semnatura-agentie">
  Agenție,<br />
  <?php echo $contact_legal_den_firma; ?><br />
  <?php if($_GET['stampila']=='da') echo '<img src="/images/stampila.png">'; ?>
</div>

<div class="semnatura-client">
  Turist,<br />
  <?php echo $row_boncom['denumire']; ?>
</div>

<div class="clear"></div>

</div>
</body>
</html>
