<?php session_start(); include_once('../restrictionare.php');
$id_cerere=$_GET['idc']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body { font-family:Calibri, "Trebuchet MS", Arial; font-size:10pt; line-height:130%; color:#000; margin:0; padding:0; }
.clear { content:"."; display:block; height:0; font-size:0; clear:both; visibility:hidden; }
.date-agentie { float:left; width:80mm; }
.date-agentie .titlu { font-weight:bold; text-transform:uppercase; }
.date-client { float:right; width:70mm; }
.date-client .titlu { font-weight:bold; margin-top:50px; }
h1 { font-size:24pt; margin:10px; padding:0; }
h2 { font-size:14pt; margin:10px; padding:0; }
table { border-collapse:collapse; border-color:#000; }
table td, table th { vertical-align:middle; text-align:left; padding:5px; }
table th { font-weight:bold; }
p { text-indent:3em; margin:0; padding:0 0 1.5em 0; text-align:justify; }
.larger { font-size:16px; font-weight:bold; }
.pasageri { padding:5px 0 0 20px; line-height:22px; font-weight:bold; }
.pasageri span { font-size:12pt; }
.semnatura-agentie { float:left; width:100mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
.semnatura-client { float:right; width:70mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
</style>
<?php include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>

<?php
$sel_boncom = "SELECT * FROM cerere_rezervare
LEFT JOIN cerere_rezervare_date_facturare ON cerere_rezervare_date_facturare.id_cerere = cerere_rezervare.id_cerere
WHERE cerere_rezervare.id_cerere = '$id_cerere'
";
$que_boncom = mysql_query($sel_boncom) or die(mysql_error());
$row_boncom = mysql_fetch_array($que_boncom);

?>

<div class="date-agentie">
  <img src="/images/logo.png" /><br />
  <div class="titlu"><?php echo $contact_legal_den_firma; ?></div>
  Nr. reg. com.: <?php echo $contact_legal_reg_com; ?><br />
  Cod fiscal: <?php echo $contact_legal_cui; ?><br />
  <strong>Capital social:</strong> <strong style="font-size:1.1em;"><?php echo $contact_legal_capital_social; ?></strong><br />
  Adresa sediu social: <?php echo $contact_legal_adresa; ?><br />
  Banca <?php echo $contact_legal_banca; ?><br />
  RON: <?php echo $contact_legal_cont_lei; ?><br />
  EURO: <?php echo $contact_legal_cont_euro; ?><br />
  Email: <?php echo $contact_email; ?><br />
  Tel: <?php echo $contact_telefon; ?>
</div>

<div class="date-client">
  <div class="titlu">
  	  <?php
      $sel_pasageri = "SELECT * FROM pasageri WHERE id_rezervare = '$id_cerere'";
      $que_pasageri = mysql_query($sel_pasageri) or die(mysql_error());
      
      while($row_pasageri = mysql_fetch_array($que_pasageri)) {
          $turist.='<span>'.$row_pasageri['nume'].' '.$row_pasageri['prenume'].'</span> <br />';
          }
echo $turist;
      ?>
   
  
</div><br />

  <?php if($row_boncom['oras']) { ?>Oras: <?php echo $row_boncom['oras']; ?><br /><?php } ?>

</div>

<div class="clear"></div>

<br />

<h1 align="center">BON DE COMANDA</h1>
<h2 align="center">Nr. <?php echo $row_boncom['bon_comanda']; ?> din data de <?php echo date('d.m.Y',strtotime($row_boncom['data_documente'])); ?></h2>

<br />

<strong>Nr. persoane:</strong> <?php echo $row_boncom['nr_adulti']; ?> adulti <?php if($row_boncom['nr_copii']) { ?>+ <?php echo $row_boncom['nr_copii'].' '; if($row_boncom['nr_copii']=='1') echo 'copil'; else echo 'copii'; } ?><br />

<br />

<table width="100%" border="1">
  <tr>
    <th width="85%" style="text-align:center;">Categorii si servicii comandate</th>
    <th width="15%" style="text-align:center;">Pret</th>
  </tr>
  <tr>
    <td><?php echo $row_boncom['oferta_denumire']; ?>, <?php echo $row_boncom['hotel_localitate']; ?>, <?php if($row_boncom['hotel_localitate']!='Circuit') echo $row_boncom['hotel_tara'].','; ?> <?php echo date('d.m.Y',strtotime($row_boncom['data'])); ?> / <?php echo date('d.m.Y',strtotime($row_boncom['data'].'+'.($row_boncom['nr_zile']-1).' days')); ?></td>
    <td style="text-align:right;"><?php echo $row_boncom['pret']; ?> <?php echo $row_boncom['moneda']; ?></td>
  </tr>
</table>

<?php if($row_boncom['puncte_fidelitate']) { ?>
<br />
<strong>Puncte de fidelitate obtinute:</strong> <span class="larger"><?php echo $row_boncom['puncte_fidelitate']; ?> puncte</span>
<br />
<?php } ?>
<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Destinatie:</strong> <?php echo $row_boncom['hotel_localitate']; ?>, <?php echo $row_boncom['hotel_tara']; ?><br />
      <strong>Data incepere sejur / Data terminare sejur:</strong> <?php echo date('d.m.Y',strtotime($row_boncom['data'])); ?> / <?php echo date('d.m.Y',strtotime($row_boncom['data'].'+'.($row_boncom['nr_zile']-1).' days')); ?><br />
      <strong>Durata:</strong> <?php echo $row_boncom['nr_nopti']; ?> nopti / <?php echo $row_boncom['nr_zile']; ?> zile
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Unitatea de cazare, categorie:</strong> <?php echo $row_boncom['hotel_denumire']; ?>, <?php if($row_boncom['hotel_categorie']!='0') echo $row_boncom['hotel_categorie'].' stele'; ?><br />
      <strong>Tipul camerei:</strong> <?php echo $row_boncom['nume_camera']; ?><br />
      <strong>Serviciile de masa:</strong> <?php echo $row_boncom['tip_masa']; ?>
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Persoanele pentru care se comanda serviciile (Numele, varsta, act identitate):</strong><br />
      <div class="pasageri">
	  <?php
      $sel_pasageri = "SELECT * FROM pasageri WHERE id_rezervare = '$id_cerere'";
      $que_pasageri = mysql_query($sel_pasageri) or die(mysql_error());
      
      while($row_pasageri = mysql_fetch_array($que_pasageri)) {
          echo '<span>'.$row_pasageri['nume'].' '.$row_pasageri['prenume'].'</span>';
          if(($row_pasageri['data_nasterii']!='') and ($row_pasageri['data_nasterii']!='0000-00-00')) echo ', data nasterii: '.date('d.m.Y',strtotime($row_pasageri['data_nasterii']));
          if($row_pasageri['buletin']) echo ', '.$row_pasageri['buletin'];
          echo '<br />';
      }
      ?>
      </div>
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Mijlocul de transport:</strong> <?php echo $row_boncom['transport']; ?><br />
      <strong>Ruta:</strong> <?php echo $row_boncom['ruta']; ?><br />
      <strong>Transfer aeroport-hotel-aeroport:</strong> <?php echo $row_boncom['transfer']; ?>
    </td>
  </tr>
</table>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Servicii incluse:</strong> <?php echo nl2br($row_boncom['servicii_incluse']); ?><br />
      <strong>Excursii optionale:</strong> <?php echo nl2br($row_boncom['excursii_optionale']); ?>
    </td>
  </tr>
</table>

<?php if($row_boncom['page_break_pos']==1) { ?><div style="page-break-after:always;"></div><?php } ?>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Nr. minim de persoane necesar pentru efectuarea programului:</strong> <?php echo $row_boncom['nr_minim_pers']; ?><br />
      <strong>Data limita de anuntare a anularii:</strong> <u><?php echo denLuniRo(date('j F Y',strtotime($row_boncom['data_anulare']))); ?></u><br />
      <strong>Regimul pasapoartelor, vizelor, conditii speciale de intrare si asistenta in tara destinatie:</strong> <?php echo $row_boncom['regim_documente']; ?><br />
      <strong>Asigurari medicale obligatorii/facultative etc:</strong> <?php echo $row_boncom['asigurari']; ?>
    </td>
  </tr>
</table>

<?php if($row_boncom['page_break_pos']==2) { ?><div style="page-break-after:always;"></div><?php } ?>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Achitarea avansului:</strong> <u><?php echo $row_boncom['procent_avans']; ?> %</u> pana la <u><?php echo denLuniRo(date('j F Y',strtotime($row_boncom['plata_avans']))); ?></u><br />
      <strong>Termenul pentru achitarea restului de plata:</strong> <u><?php echo denLuniRo(date('j F Y',strtotime($row_boncom['plata_rest']))); ?></u><br />
      <strong>Modalitatea de plata:</strong>
      <?php if($row_boncom['modalitate_plata']=="plata_sediu") echo 'Plata la sediul agentiei'; ?>
      <?php if($row_boncom['modalitate_plata']=="depunere_in_cont") echo 'Depunere Banca Transilvania'; ?>
      <?php if($row_boncom['modalitate_plata']=="transfer_bancar") echo 'Transfer bancar'; ?>
      <?php if($row_boncom['modalitate_plata']=="card_online") echo 'Cu cardul online'; ?>
      <?php if($row_boncom['modalitate_plata']=="numerar") echo 'Numerar la sediul agentiei'; ?>
      <?php if($row_boncom['modalitate_plata']=="card_sediu") echo 'Cu cardul la sediul agentiei'; ?>
      <?php if($row_boncom['modalitate_plata']=="la_hotel") echo 'La receptia hotelului'; ?>
    </td>
  </tr>
</table>

<?php if($row_boncom['page_break_pos']==3) { ?><div style="page-break-after:always;"></div><?php } ?>

<br />

<table width="100%" border="1">
  <tr>
    <td>
      <strong>Alte mentiuni si observatii suplimentare / alte solicitari speciale:</strong><br />
      <br />
      <strong>Observatii:</strong> <?php echo nl2br($row_boncom['alte_observatii']); ?><br />
    </td>
  </tr>
</table>

<br />

<p>Plata serviciilor al caror tarif este exprimat in alta moneda decat LEI se va efectua la cursul BNR din ziua platii, plus 2% comision de risc valutar.</p>

<br />
<br />
<br />
<br />

<div class="semnatura-agentie">
  Agentie,<br />
  <?php echo $contact_legal_den_firma; ?><br />
  <?php if($_GET['stampila']=='da') echo '<img src="/images/stampila.png">'; ?>
</div>

<div class="semnatura-client">
  Turisti,<br />
  <?php echo $turist; ?>
</div>

<div class="clear"></div>

</body>
</html>
