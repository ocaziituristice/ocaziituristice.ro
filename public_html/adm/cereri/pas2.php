<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/adm/cofig_adm.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
<script>
		function showEdit(editableObj) {
			$(editableObj).css("background","#FFF", "width","100px");
		} 
		

function saveToDatabase(editableObj,column,id) {
	$(editableObj).css("background","#FFF url(/images/loader.gif) no-repeat right");
	$.ajax({
		url: "/adm/cereri/save_edit.php",
		type: "POST",
		data:'editval='+editableObj.innerHTML+'&id_cerere='+id,
		success: function(data){
			$(editableObj).css("background","#FDFDFD");
		}        
   });
}

		</script>
        
        
<div style="padding:6px 10px 12px 10px; border-bottom:4px solid #CCC;">

<?php
$sel_rez = "SELECT
cerere_rezervare.*,
useri_fizice.nume AS usrfiz_nume,
useri_fizice.prenume AS usrfiz_prenume,
useri_fizice.email AS usrfiz_email,
useri_fizice.telefon AS usrfiz_telefon,
useri_fizice.sex AS usrfiz_sex,
useri_fizice.data_nasterii AS usrfiz_data_nasterii,
useri_fizice.newsletter AS usrfiz_newsletter,
oferte.id_oferta,
oferte.denumire_scurta,
oferte.new_layout,
hoteluri.nume,
localitati.denumire AS denumire_localitate,
tari.denumire AS denumire_tara,
continente.nume_continent,
furnizori.denumire AS denumire_furnizor,
furnizori.id_furnizor,
hoteluri.tip_unitate
FROM cerere_rezervare
INNER JOIN useri_fizice ON cerere_rezervare.id_useri_fizice = useri_fizice.id_useri_fizice
LEFT JOIN oferte ON cerere_rezervare.id_oferta = oferte.id_oferta
LEFT JOIN hoteluri
	ON (hoteluri.id_hotel = oferte.id_hotel AND (cerere_rezervare.id_hotel = '0' OR cerere_rezervare.id_hotel IS NULL))
	OR (hoteluri.id_hotel = cerere_rezervare.id_hotel AND (cerere_rezervare.id_oferta = '0' OR cerere_rezervare.id_oferta IS NULL))
LEFT JOIN localitati ON localitati.id_localitate = hoteluri.locatie_id
LEFT JOIN zone ON zone.id_zona = localitati.id_zona
LEFT JOIN tari ON tari.id_tara = zone.id_tara
LEFT JOIN continente ON continente.id_continent = hoteluri.id_continent
LEFT JOIN furnizori ON furnizori.id_furnizor = cerere_rezervare.id_furnizor
WHERE cerere_rezervare.id_cerere='".$_GET['idc']."'
";
$que_rez = mysql_query($sel_rez) or die(mysql_error());
$row_rez = mysql_fetch_array($que_rez);

$tip_unitate = $row_rez['tip_unitate'];

$sel_tipcam = "SELECT * FROM cerere_rezervare_tip_camera WHERE id_cerere='".$_GET['idc']."'";
$que_tipcam = mysql_query($sel_tipcam) or die(mysql_error());
$row_tipcam = mysql_fetch_array($que_tipcam);

$sel_gradocup = "SELECT * FROM cerere_rezervare_grad_ocupare WHERE id_camera='".$row_tipcam['id_camera']."'";
$que_gradocup = mysql_query($sel_gradocup) or die(mysql_error());
$row_gradocup = mysql_fetch_array($que_gradocup);

if($tip_unitate=='Circuit') {
	$link_oferta = make_link_circuit($row_rez['nume'], $row_rez['id_oferta']);
	
	$link_oferta = $link_oferta . '?plecdata='.$row_rez['data'].'&pleczile='.$row_rez['nr_nopti'].'&adulti='.$row_rez['nr_adulti'].'&copii='.$row_rez['nr_copii'].'&age[0]='.$row_gradocup['copil1'].'&age[1]='.$row_gradocup['copil2'].'&age[2]='.$row_gradocup['copil3'].'#calc';
} else {
	$den_loc = $row_rez['denumire_localitate'];
	$den_hotel = $row_rez['nume'];
	if($row_rez['id_oferta']=='' or $row_rez['id_oferta']=='0') {
		$den_scurta_oferta = NULL;
		$id_oferta = NULL;
	} else {
		$den_scurta_oferta = $row_rez['denumire_scurta'];
		$id_oferta = $row_rez['id_oferta'];
	}
	
	$link_oferta = make_link_oferta($den_loc, $den_hotel, $den_scurta_oferta, $id_oferta);
	
	if($row_rez['new_layout']=='da' or $id_oferta==NULL) {
		$plecdata = date('d.m.Y',strtotime($row_rez['data']));
	} else {
		$plecdata = $row_rez['data'];
	}
	
	$link_oferta = $link_oferta . '?plecdata='.$plecdata.'&pleczile='.$row_rez['nr_nopti'].'&adulti='.$row_rez['nr_adulti'].'&copii='.$row_rez['nr_copii'].'&age[0]='.$row_gradocup['copil1'].'&age[1]='.$row_gradocup['copil2'].'&age[2]='.$row_gradocup['copil3'].'#preturi';
}
?>
  <table cellpadding="0" cellspacing="0" border="0" width="95%">
    <tr>
      <td style="text-align:left; vertical-align:top; width:45%;">
        <strong class="black">DETALII OFERTA:</strong>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableRez">
          <tr>
            <td align="right" valign="middle" width="120"><?php if($id_oferta==NULL) { ?>Hotel<?php } else { ?>Oferta<?php } ?>:</td>
            <td align="left" valign="middle"><a href="<?php echo $link_oferta; ?>" target="_blank"><strong class="bigger-11em red"><?php if($id_oferta==NULL) echo $row_rez['nume']; else echo $row_rez['oferta_denumire']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;" /></strong></a></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Data adaugare:</td>
            <td align="left" valign="middle"><?php echo $row_rez['data_adaugarii']; ?></td>
          </tr>
          <?php if($id_oferta!=NULL) { ?>
          <tr>
            <td align="right" valign="middle">Furnizor:</td>
            <td align="left" valign="middle" style="padding:5px 0 !important;"><strong class="bigger-11em black"><?php echo ucwords($row_rez['denumire_furnizor']); ?></strong> &nbsp; &raquo; &nbsp; <a href="/adm/corespondenta_furnizor.php?pas=trimite_mail&idc=<?php echo $_GET['idc']; ?>&idf=<?php echo $row_rez['id_furnizor']; ?>" target="_blank"><strong>Trimite email FURNIZOR <img src="/images/link_extern.png" style="vertical-align:middle;" /></strong></a></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Disponibilitate:</td>
            <td align="left" valign="middle"><span class="bold green"><?php echo $row_rez['disponibilitate']; ?></span></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Rezervare furnizor</td>
          
			
<td align="left" valign="middle" class="bigger-12em green" >						
		<div class="bigger-12em green" style="width:200px; border:thin #666" contenteditable="true" onBlur="saveToDatabase(this,'id_cerere','<?php echo $row_rez['id_cerere']; ?>')" onClick="showEdit(this);"><?php echo $row_rez['id_rezervare_furnizor']; 
		 if(strlen($row_rez['id_rezervare_furnizor'])<1) echo "---";?></div></td>
            
            
          </tr>
          <tr>
            <td align="right" valign="middle" width="80">ID oferta:</td>
            <td align="left" valign="middle"><a href="/adm/editare_sejur.php?pas=2&oferta=<?php echo $row_rez['id_oferta']; ?>" target="_blank"><strong class="bigger-12em blue"><?php echo $row_rez['id_oferta']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;" /></strong></a></td>
          </tr>
          <?php } ?>
          <tr>
            <td align="right" valign="middle">Data inceput:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo date('d.m.Y',strtotime($row_rez['data'])); ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Durata:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_rez['nr_nopti']; ?> nopti</strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Transport:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_rez['transport']; ?></strong><?php if($row_rez['oras_plecare']>0) echo ', plecare din <strong class="bigger-11em black">'.get_den_localitate($row_rez['oras_plecare']).'</strong>'; ?></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Tip masa:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_rez['tip_masa']; ?></strong></td>
          </tr>
<?php if($row_rez['tip']=='rezervare') {
$sel_tipcam = "SELECT * FROM cerere_rezervare_tip_camera WHERE id_cerere='".$_GET['idc']."'";
$que_tipcam = mysql_query($sel_tipcam) or die(mysql_error());
while($row_tipcam = mysql_fetch_array($que_tipcam)) {
?>
          <tr>
            <td align="right" valign="middle">Camera:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_tipcam['nr_camera'].'x '.$row_tipcam['nume_camera']; ?></strong></td>
          </tr>
	<?php
    $tarif_total = '';
	
	$sel_gradocup = "SELECT * FROM cerere_rezervare_grad_ocupare WHERE id_camera='".$row_tipcam['id_camera']."'";
    $que_gradocup = mysql_query($sel_gradocup) or die(mysql_error());
    while($row_gradocup = mysql_fetch_array($que_gradocup)) {
		$tarif_total = $tarif_total + $row_gradocup['pret'];
		$moneda = $row_gradocup['moneda'];
    ?>
          <tr>
            <td align="right" valign="middle">Grad ocupare:</td>
            <td align="left" valign="middle" style="text-indent:1em;">
              <?php
              echo '<strong class="bigger-11em black">';
			  if($row_gradocup['adulti']) {
				  echo '- '.$row_gradocup['adulti'].' adult';
				  if($row_gradocup['adulti']>1) echo 'i';
			  }
			  if($row_gradocup['copil1']) {
				  echo ' + 1 copil ('.$row_gradocup['copil1'].')';
			  }
			  if($row_gradocup['copil2']) {
				  echo ' + 1 copil ('.$row_gradocup['copil2'].')';
			  }
			  if($row_gradocup['copil3']) {
				  echo ' + 1 copil ('.$row_gradocup['copil3'].')';
			  }
			  echo '</strong>';
			  echo ' - <strong class="bigger-12em green">'.$row_gradocup['pret'].' '.moneda($row_gradocup['moneda']).'</strong>';
			  ?>
            </td>
          </tr>
	<?php } ?>
<?php } ?>
          <tr>
            <td align="right" valign="middle">Tip plata:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo str_replace('_',' ',$row_rez['modalitate_plata']); ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle"><strong class="black">Tarif total:</strong></td>
            <td align="left" valign="middle"><strong class="bigger-13em green" id="tarif_total"><?php echo $tarif_total.' '.moneda($moneda); ?></strong></td>
          </tr>
<?php if($row_rez['id_cupon']) {
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/cupoane.php');
$cupoane = new CUPOANE();
$coupon = $cupoane->decode_cupon(md5(md5($row_rez['id_cupon'])), 'nu');
$tarif_initial=$tarif_total + $coupon['valoare_campanie'];
if($coupon['valoare_campanie']>0 and $coupon['tip_valoare']!='procent') {
	if($coupon['moneda_campanie']==$detalii['moneda']) {
		$coupon['valoare_campanie'] = $coupon['valoare_campanie'];
		
	} else {
		$coupon['valoare_campanie'] = round(final_price_eur($coupon['valoare_campanie'], $coupon['moneda_campanie']));
		$coupon['moneda_campanie'] = 'EURO';
	}
	
} else {
	 
	$tarif_initial=round($tarif_total*100/(100-$coupon['valoare_campanie']),0);}

//$tarif_initial=$tarif_total + $coupon['valoare_campanie'];
echo "<script>$('#tarif_total').html('<span style=\"text-decoration:line-through;\" class=\"black\">".($tarif_initial)." ".moneda($moneda)."</span> ".$tarif_total." ".moneda($moneda)."');</script>";
?>

		  <tr>
			<td align="right" valign="middle"><strong class="bigger-12em yellow">CUPON</strong></td>
            <td align="left" valign="middle"><span class="bigger-12em yellow">valoare <strong class="bigger-11em">
			<?php echo $coupon['valoare_campanie'];
			if($coupon['moneda_campanie']!=='Procent')
			{echo moneda($coupon['moneda_campanie']);}
			else
			{echo " %";} ?></strong></span></td>
          </tr>
<?php } ?>
<?php } else { ?>
          <tr>
            <td align="right" valign="middle">Grad ocupare:</td>
            <td align="left" valign="middle" style="text-indent:1em;">
              <?php
              echo '<strong class="bigger-11em black">';
			  if($row_rez['nr_adulti']>0) {
				  echo '- '.$row_rez['nr_adulti'].' adult';
				  if($row_rez['nr_adulti']>1) echo 'i';
			  }
			  if($row_rez['nr_copii']>0) {
				  echo ' + '.$row_rez['nr_copii'].' copi';
				  if($row_rez['nr_copii']>1) echo 'i'; else echo 'l';
			  }
			  if($row_rez['varste_copii']) {
				  echo '<br />'.$row_rez['varste_copii'];
			  }
			  echo '</strong>';
			  ?>
            </td>
          </tr>
<?php } ?>
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="left" valign="middle">&nbsp;</td>
          </tr>
          <?php if($row_rez['scop_cerere']) { ?>
          <tr>
            <td align="right" valign="middle">In scopul:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_rez['scop_cerere']; ?></strong></td>
          </tr>
          <?php } ?>
          <?php if($row_rez['observatii']) { ?>
          <tr>
            <td align="right" valign="middle">Observatii:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo nl2br($row_rez['observatii']); ?></strong></td>
          </tr>
          <?php } ?>
          <?php if($row_rez['servicii_incluse']) { ?>
          <tr>
            <td align="right" valign="middle" nowrap style="vertical-align:top !important;"><strong class="black">Servicii incluse:</strong></td>
            <td align="left" valign="middle"><?php echo nl2br($row_rez['servicii_incluse']); ?></td>
          </tr>
          <?php } ?>
          <?php if($row_rez['hoteluri_similare']) { ?>
          <tr>
            <td align="right" valign="middle">Hoteluri similare:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_rez['hoteluri_similare']; ?></strong></td>
          </tr>
          <?php } ?>
        </table>
      </td>
      <td style="text-align:left; vertical-align:top; width:30%;">
        <strong class="black">CLIENT:</strong>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableRez">
          <tr>
            <td align="right" valign="middle" width="75">Nume:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo ucwords(strtolower($row_rez['usrfiz_nume'])); ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Prenume:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo ucwords(strtolower($row_rez['usrfiz_prenume'])); ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Email:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo strtolower($row_rez['usrfiz_email']); ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Telefon:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_rez['usrfiz_telefon']; ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Sex:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo ucwords($row_rez['usrfiz_sex']); ?></strong></td>
          </tr>
          <?php if($row_rez['usrfiz_data_nasterii']!='0000-00-00') { ?>
          <tr>
            <td align="right" valign="middle">Data nasterii:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo date('d.m.Y',strtotime($row_rez['usrfiz_data_nasterii'])); ?></strong></td>
          </tr>
          <?php } ?>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right" valign="middle"><strong class="blue">NEWSLETTER:</strong></td>
            <td align="left" valign="middle">
<script type="text/javascript">
$(document).ready(function() {
	$('#newsletter<?php echo $row_rez['id_useri_fizice']; ?>').editable("/adm/cereri/editinplace_update_newsletter.php", {
		loadurl : '/adm/cereri/load_stare_newsletter.php',
		type   : "select",
		submit : "OK",
		style  : "inherit" 
	});
});
</script>
			  <div id="newsletter<?php echo $row_rez['id_useri_fizice']; ?>" class="bold red"><?php echo $row_rez['usrfiz_newsletter']; ?></div>
            </td>
          </tr>
        </table>
      </td>
      <?php if($row_rez['tip']=='rezervare_nou') { ?>
      <td rowspan="2" style="text-align:left; vertical-align:top; background:#fff1f3;">
        <strong class="black">STARE FURNIZOR:</strong>
		<div class="black" style="padding-left:10px;" id="stareFurnizor<?php echo $_GET['idc']; ?>"></div>
		<script>
        $("#stareFurnizor<?php echo $_GET['idc']; ?>").empty().html('<img src="/images/loader.gif" alt="" />');
        $("#stareFurnizor<?php echo $_GET['idc']; ?>").load("/adm/cereri/stare_furnizor.php?idc=<?php echo $_GET['idc']; ?>");
        
        function submitForm() {
            $.ajax({
                type: 'POST',
                url: '/adm/cereri/stare_furnizor.php?idc=<?php echo $_GET['idc']; ?>',
                data: $('#stare_furnizor<?php echo $_GET['idc']; ?>').serialize(),
                dataType: 'html',
                beforeSend:function(){
                    $('#stareFurnizor<?php echo $_GET['idc']; ?>').html('<img src="/images/loader.gif" alt="" />');
                },
                success: function(response) {
                    $('#stareFurnizor<?php echo $_GET['idc']; ?>').html(response);
                }
            });
            return false;
        }
        </script>
      </td>
      <?php } ?>
    </tr>
    <?php if($row_rez['tip']=='rezervare') { ?>
    <tr>
      <td style="text-align:left; vertical-align:top;">
        <br><strong class="black">PASAGERI:</strong>
<?php
$sel_go2 = "SELECT
cerere_rezervare_tip_camera.nume_camera,
cerere_rezervare_grad_ocupare.*
FROM cerere_rezervare_grad_ocupare
INNER JOIN cerere_rezervare_tip_camera ON cerere_rezervare_tip_camera.id_camera = cerere_rezervare_grad_ocupare.id_camera
WHERE cerere_rezervare_tip_camera.id_cerere = '".$_GET['idc']."'
";
$que_go2 = mysql_query($sel_go2) or die(mysql_error());
while($row_go2 = mysql_fetch_array($que_go2)) {
?>
		<br><strong class="bigger-11em blue"><?php
        echo $row_go2['nume_camera'];
		if($row_go2['adulti']) {
			echo ' - '.$row_go2['adulti'].' adult';
			if($row_go2['adulti']>1) echo 'i';
		}
		if($row_go2['copil1']) {
			echo ' + 1 copil ('.$row_go2['copil1'].')';
		}
		if($row_go2['copil2']) {
			echo ' + 1 copil ('.$row_go2['copil2'].')';
		}
		if($row_go2['copil3']) {
			echo ' + 1 copil ('.$row_go2['copil3'].')';
		}
		?></strong>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableRez">
	<?php
    $sel_pasageri = "SELECT * FROM pasageri WHERE id_grad_ocupare='".$row_go2['id_grad_ocupare']."' AND id_rezervare='".$_GET['idc']."'";
    $que_pasageri = mysql_query($sel_pasageri) or die(mysql_error());
    $i=0;
    while($row_pasageri = mysql_fetch_array($que_pasageri)) {
        $i++;
    ?>
          <tr>
            <td align="right" valign="middle" width="75">Pasager <?php echo $i; ?>:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo ucwords(strtolower($row_pasageri['nume'])).' '.ucwords(strtolower($row_pasageri['prenume'])).', '.$row_pasageri['sex']; if(($row_pasageri['data_nasterii']!='') and ($row_pasageri['data_nasterii']!='0000-00-00')) { echo ', '.date('d.m.Y',strtotime($row_pasageri['data_nasterii']) ); ?></strong> 
            <?php 
			
$datetime1 = new DateTime($row_pasageri['data_nasterii']);
$datetime2 = new DateTime($row_rez['data']);
$interval = $datetime1->diff($datetime2);
echo " <strong class=\"blue\" >- ".$interval->format('%y ani')."</strong>" ;
			}
?>
			
			
            
            
            </td>
          </tr>
	<?php } ?>
        </table>
<?php } ?>
      </td>
      <td style="text-align:left; vertical-align:top;">
        <br><strong class="black">DATE FACTURARE:</strong>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableRez">
<?php
$sel_facturare = "SELECT * FROM cerere_rezervare_date_facturare WHERE id_cerere='".$_GET['idc']."'";
$que_facturare = mysql_query($sel_facturare) or die(mysql_error());
while($row_facturare = mysql_fetch_array($que_facturare)) {
?>
          <tr>
            <td align="right" valign="middle" width="75">Denumire:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_facturare['denumire']; ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">CUI / CNP:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_facturare['cui_cnp']; ?></strong></td>
          </tr>
          <?php if($row_facturare['tip_persoana']=='persoana_juridica') { ?>
          <tr>
            <td align="right" valign="middle">Nr. Reg. Comert:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_facturare['nr_reg_comert']; ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Banca:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_facturare['banca']; ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">IBAN:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_facturare['iban']; ?></strong></td>
          </tr>
          <?php } ?>
          <tr>
            <td align="right" valign="middle">Adresa:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_facturare['adresa']; ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Oras:</td>
            <td align="left" valign="middle"><strong class="bigger-11em black"><?php echo $row_facturare['oras']; ?></strong></td>
          </tr>
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="left" valign="middle">&nbsp;</td>
          </tr>
<?php } ?>
        </table>
      </td>
    </tr>
    <?php } ?>
    <tr>
      <td style="text-align:left; vertical-align:top;" colspan="3">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td style="text-align:left; vertical-align:top; width:35%; padding-right:15px;">
              <strong class="black underline" style="display:block;">ACTIUNI:</strong><br />
              <div>
                <a href="/adm/corespondenta_client.php?pas=trimite_mail&idc=<?php echo $_GET['idc']; ?>" class="buttons" target="_blank">Trimite raspuns client</a>
                <a href="/adm/corespondenta_client.php?pas=trimite_sms&idc=<?php echo $_GET['idc']; ?>" class="buttons-brown" target="_blank">Trimite SMS client</a>
              </div><br>
              <?php if($row_rez['tip']=='rezervare' and $row_rez['stare']=='anulat') {
				  if($row_rez['anulare_google']=='nu') { ?>
              <div><a href="#" class="buttons-red" onclick="window.open('/adm/anuleaza_google_analytics.php?idc=<?php echo $_GET['idc']; ?>','Anuleaza Google Analytics','width=400,height=200')">ANULEAZA Google Analytics</a></div><br>
              <?php } else echo '<div class="red bold">Anulare efectuata deja</div><br>';
			  } ?>
            </td>
            <td style="text-align:left; vertical-align:top; width:65%; padding-left:15px;">
              <strong class="red underline" style="display:block;">OBSERVATII AGENTIE:</strong>
              <div id="observatii_agentie<?php echo $_GET['idc']; ?>"></div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="text-align:left; vertical-align:top;" colspan="3">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td style="text-align:left; vertical-align:top; padding:5px 15px 5px 5px; width:48%;">
              <strong class="green underline" style="display:block;">CORESPONDENTA CLIENT: &nbsp;&nbsp; <a href="/adm/corespondenta_client.php?pas=trimite_mail&idc=<?php echo $_GET['idc']; ?>" style="font-size:1.3em;" target="_blank">&laquo; Trimite email &raquo;</a></strong>
              <ol class="corespondenta">
              <?php
              $sel_mail_client="SELECT corespondenta_client.*, useri.nume
              FROM corespondenta_client
              LEFT JOIN useri ON useri.id_user=corespondenta_client.id_admin
              WHERE id_cerere='".$_GET['idc']."' and id_furnizor is NULL
              ORDER BY data_trimitere ASC ";
              $que_mail_client=mysql_query($sel_mail_client) or die(mysql_error());
              while($row_mail_client=mysql_fetch_array($que_mail_client)) {
                  echo '<li class="clear"><a class="expandable emailC'.$row_mail_client['id_mail'].'">'.$row_mail_client['subiect'].'</a> - <strong>'.date('d.m.Y H:i:s',strtotime($row_mail_client['data_trimitere'])).'</strong> - '.$row_mail_client['nume'].'<div id="emailContentC'.$row_mail_client['id_mail'].'" class="clear black" style="background:#FFF;">'.$row_mail_client['continut'];
			echo '<strong>ATASAMENTE</strong>:<br />'.$row_mail_client['atasamente'];	  
			if(strlen($row_mail_client['send_id'])>5) {  
				 echo '<br />Citirea mail'.'<br />';
			$sel_tranzactii="SELECT * FROM tranzactii_email WHERE id_send='".$row_mail_client['send_id']."' ORDER BY data_adaugare ASC ";
              $que_tranzactii=mysql_query($sel_tranzactii) or die(mysql_error());
              while($row_mail_tranzactii=mysql_fetch_array($que_tranzactii)) {	
			  
			  echo  "<br><strong>".$row_mail_tranzactii['actiune'].'</strong>-'.$row_mail_tranzactii['ip_host'].'-'.$row_mail_tranzactii['url'].'-'.date("d-m-Y | h:i:sa",$row_mail_tranzactii['timestamp'])."<br />";
			  }
				}
				
				 echo  '</div></li>';
              ?>
<script type="text/javascript">
$("#emailContentC<?php echo $row_mail_client['id_mail']; ?>").hide();
$(".emailC<?php echo $row_mail_client['id_mail']; ?>").show();
$(".emailC<?php echo $row_mail_client['id_mail']; ?>").click(function() {
	$("#emailContentC<?php echo $row_mail_client['id_mail']; ?>").slideToggle('fast');
	$(this).toggleClass("expandableON");
});
</script>
			  <?php } ?>
              </ol>
            </td>
            <td style="text-align:left; vertical-align:top; padding:5px 5px 5px 15px; width:48%; background:#fff1f3;">
              <strong class="red underline" style="display:block;">CORESPONDENTA FURNIZOR: &nbsp;&nbsp; <a href="/adm/corespondenta_furnizor.php?pas=trimite_mail&idc=<?php echo $_GET['idc']; ?>&idf=<?php echo $row_rez['id_furnizor']; ?>" style="font-size:1.3em;" target="_blank">&laquo; Trimite email &raquo;</a></strong>
             
              <?php /*?><?php
			   <ol class="corespondenta">
              $sel_mail_furnizor="SELECT corespondenta_furnizor.*, furnizori.denumire
              FROM corespondenta_furnizor
              LEFT JOIN furnizori ON furnizori.id_furnizor=corespondenta_furnizor.id_furnizor
              WHERE id_cerere='".$_GET['idc']."'
              ORDER BY data_trimitere ASC ";
              $que_mail_furnizor=mysql_query($sel_mail_furnizor) or die(mysql_error());
              while($row_mail_furnizor=mysql_fetch_array($que_mail_furnizor)) {
                  echo '<li class="clear"><a class="expandable emailF'.$row_mail_furnizor['id_mail'].'">'.$row_mail_furnizor['subiect'].'</a> - <strong>'.date('d.m.Y H:i:s',strtotime($row_mail_furnizor['data_trimitere'])).'</strong> - '.$row_mail_furnizor['denumire'].'<div id="emailContentF'.$row_mail_furnizor['id_mail'].'" class="clear black" style="background:#FFF;">'.$row_mail_furnizor['continut'].'</div></li>';
              ?>
<script type="text/javascript">
$("#emailContentF<?php echo $row_mail_furnizor['id_mail']; ?>").hide();
$(".emailF<?php echo $row_mail_furnizor['id_mail']; ?>").show();
$(".emailF<?php echo $row_mail_furnizor['id_mail']; ?>").click(function() {
	$("#emailContentF<?php echo $row_mail_furnizor['id_mail']; ?>").slideToggle('fast');
	$(this).toggleClass("expandableON");
});
</script>
			  <?php } ?>
              </ol><?php */?>
     
     <ol class="corespondenta">
              <?php
              $sel_mail_client="SELECT corespondenta_client.*, useri.nume
              FROM corespondenta_client
              LEFT JOIN useri ON useri.id_user=corespondenta_client.id_admin
              WHERE id_cerere='".$_GET['idc']."' and id_furnizor>0
              ORDER BY data_trimitere ASC ";
              $que_mail_client=mysql_query($sel_mail_client) or die(mysql_error());
              while($row_mail_client=mysql_fetch_array($que_mail_client)) {
                  echo '<li class="clear">
				  <a class="expandable emailF'.$row_mail_client['id_mail'].'">'.$row_mail_client['subiect'].'</a> - <strong>'.date('d.m.Y H:i:s',strtotime($row_mail_client['data_trimitere'])).'</strong> - '.$row_mail_client['nume'].'<div id="emailContentF'.$row_mail_client['id_mail'].'" class="clear black" style="background:#FFF;">'.$row_mail_client['continut'];
			echo '<strong>ATASAMENTE</strong>:<br />'.$row_mail_client['atasamente'];	  
			if(strlen($row_mail_client['send_id'])>5) {  
				 echo '<br />Citirea mail'.'<br />';
			$sel_tranzactii="SELECT * FROM tranzactii_email WHERE id_send='".$row_mail_client['send_id']."' ORDER BY data_adaugare ASC ";
              $que_tranzactii=mysql_query($sel_tranzactii) or die(mysql_error());
              while($row_mail_tranzactii=mysql_fetch_array($que_tranzactii)) {	
			  
			  echo  "<br><strong>".$row_mail_tranzactii['actiune'].'</strong>-'.$row_mail_tranzactii['ip_host'].'-'.$row_mail_tranzactii['url'].'-'.date("d-m-Y | h:i:sa",$row_mail_tranzactii['timestamp'])."<br />";
			  }
				}
				
				 echo  '</div></li>';
              ?>
<script type="text/javascript">
$("#emailContentF<?php echo $row_mail_client['id_mail']; ?>").hide();
$(".emailF<?php echo $row_mail_client['id_mail']; ?>").show();
$(".emailF<?php echo $row_mail_client['id_mail']; ?>").click(function() {
	$("#emailContentF<?php echo $row_mail_client['id_mail']; ?>").slideToggle('fast');
	$(this).toggleClass("expandableON");
});
</script>
			  <?php } ?>
              </ol>         
              
              
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="text-align:left; vertical-align:top; background:#FFFFE8;" colspan="3">
		<?php if($row_rez['tip']=='rezervare') { ?>
        <br><strong class="black underline" style="display:block; font-size:1.3em;">DOCUMENTE:</strong>
        <br />
        <table cellspacing="0" cellpadding="5" border="0">
          <tr>
		<?php
        $sel_boncom = "SELECT genereaza_bon, genereaza_contract, genereaza_voucher, id_voucher,proforma_principala FROM cerere_rezervare WHERE id_cerere='".$_GET['idc']."'";
        $que_boncom = mysql_query($sel_boncom) or die(mysql_error());
        $row_boncom = mysql_fetch_array($que_boncom);
        
        if($row_boncom['genereaza_bon']=='n') {
		?>
            <td><a href="cereri/bon_comanda.php?idc=<?php echo $_GET['idc']; ?>" target="_blank" class="buttons">Genereaza Bon de Comanda</a></td>
        <?php } else { ?>
        	<td><strong class="bigger-13em black">Bon de Comanda</strong><br />
            <a href="cereri/bon_comanda.php?idc=<?php echo $_GET['idc']; ?>" target="_blank"><strong>Editeaza</strong></a>
            &nbsp; | &nbsp;
            <a href="cereri/bon_comanda_print.php?idc=<?php echo $_GET['idc']; ?>" target="_blank"><strong>Printeaza</strong></a>
            &nbsp; | &nbsp;
            <a href="cereri/bon_comanda_print.php?idc=<?php echo $_GET['idc']; ?>&stampila=da" target="_blank"><strong>+ Stampila</strong></a>
            </td>
            <?php if($row_boncom['genereaza_contract']=='n') { ?>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><a href="cereri/contract.php?idc=<?php echo $_GET['idc']; ?>" target="_blank" class="buttons">Genereaza Contract</a></td>
            <?php } else { ?>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><strong class="bigger-13em black">Contract</strong><br />
                <a href="cereri/contract.php?idc=<?php echo $_GET['idc']; ?>" target="_blank"><strong>Editeaza</strong></a>
                &nbsp; | &nbsp;
                <a href="cereri/contract_print.php?idc=<?php echo $_GET['idc']; ?>" target="_blank"><strong>Printeaza</strong></a>
                &nbsp; | &nbsp;
                <a href="cereri/contract_print.php?idc=<?php echo $_GET['idc']; ?>&stampila=da" target="_blank"><strong>+ Stampila</strong></a>
                </td>
            <?php } ?>
            
        <?php if(strlen($row_boncom['proforma_principala'])>4) { ?>
                
          
           <td>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><strong class="bigger-13em black">Proforma</strong><br />
                <a href="<?php echo link_document($row_boncom['proforma_principala'],'proforma',$key_criptare)?>" target="_blank"><strong>Vizualizare proforma</strong></a>
                </td>
            <?php } else { 
      if($row_boncom['genereaza_bon']=='y') { ?>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><a href="cereri.php?pas=proforma&idc=<?php echo $_GET['idc']; ?>" target="_blank" class="buttons">Genereaza proforma</a></td>     
            <?php }  }?>
               
        
            
            
            <?php if($row_boncom['genereaza_voucher']=='n') { ?>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</td>
                
                <td><a href="vouchere.php?pas=genereaza&idc=<?php echo $_GET['idc']; ?>" target="_blank" class="buttons">Genereaza Voucher</a></td>
            <?php } else { ?>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><strong class="bigger-13em black">Voucher</strong><br />
                <a href="vouchere.php?pas=2&idv=<?php echo $row_boncom['id_voucher']; ?>" target="_blank"><strong>Editeaza</strong></a>
                &nbsp; | &nbsp;
                <a href="vouchere/pas3.php?idv=<?php echo $row_boncom['id_voucher']; ?>" target="_blank"><strong>Printeaza</strong></a>
                &nbsp; | &nbsp;
                <a href="vouchere/pas3.php?idv=<?php echo $row_boncom['id_voucher']; ?>&stampila=da" target="_blank"><strong>+ Stampila</strong></a>
                </td>
            <?php } ?>
        <?php } ?>
          </tr>
        </table>
        <?php } ?>
        <table cellspacing="10" cellpadding="5" border="0" width="98%">
          <tr valign="top">
            <td align="left" valign="top" width="50%">
              <strong class="black underline" style="display:block; font-size:1.1em;">Gestionare fisiere uploadate manuale: <a href="cereri.php?pas=upload_files&idc=<?php echo $_GET['idc']; ?>" target="_blank">click aici &raquo;</a></strong>
			  <ul class="corespondenta">
			  <?php
              $sel_files="SELECT * FROM cerere_rezervare_files WHERE id_cerere='".$_GET['idc']."' ";
              $que_files=mysql_query($sel_files) or die(mysql_error());
              while($row_files=mysql_fetch_array($que_files)) {
                  echo '<li class="clear">';
				  echo '<a href="uploads/cereri/'.$row_files['fisier'].'" target="_blank" class="blue"><strong>Download</strong></a>';
                  if($row_files['denumire']) echo ' - <strong>'.$row_files['denumire'].'</strong>';
                  echo ' - '.$row_files['fisier'];
				  echo '</li>';
              }
              ?>
              </ul>
            </td>
           
           <td style="text-align:left; vertical-align:top; padding:5px 5px 5px 15px; width:48%; background:#fff1f3;">
              <strong class="black underline" style="display:block; font-size:1.1em;">Gestionare fisiere generate automat: </strong>
			  <ul class="corespondenta">
			  <?php
              $sel_files="SELECT * FROM facturi WHERE id_rezervare='".$_GET['idc']."' and tip_operatie='client' and id_rezervare>5000 ";
              $que_files=mysql_query($sel_files) or die(mysql_error());
              while($row_files=mysql_fetch_array($que_files)) {
              if($row_files['tip_document']=='proforma' and preg_match("/(OCZP)([0-9]{4})/",$row_files['numar_document'],$documente)) {
		  $link_document=link_document($row_files['numar_document'],'proforma');	
			 
			  echo '<li class="clear">';
	
	echo '<a href="'.$link_document.'" target="_blank" class="blue"><strong>Vezi '.$row_files['tip_document'].' - '.$row_files['numar_document'] .' </strong></a>';
                 
				  echo '</li>'; }
	// facturile scoase din financiar
if(preg_match("/(OCZWEB)([0-9]{4})/",$row_files['observatii'],$documente)) 
{ 
$link_document=link_document($documente[0],'factura');	

echo '<li class="clear">';
	
	echo '<a href="'.$link_document.'" target="_blank" class="blue"><strong>Vezi factura  - '.$documente[0] .' </strong></a>';
                 
				  echo '</li>';
	
	}				  
              }
              ?>
              </ul>
            </td>
           
           
            
            
          </tr>
        </table>
      </td>
    </tr>
	<?php if($row_rez['tip']=='rezervare') { ?>
    <tr>
      <td style="text-align:left; vertical-align:top;" colspan="3">
        <div id="financiar<?php echo $_GET['idc']; ?>"></div>
		<script>
        $("#financiar<?php echo $_GET['idc']; ?>").empty().html('<img src="/images/loader.gif" alt="" />');
        $("#financiar<?php echo $_GET['idc']; ?>").load("/adm/cereri/financiar.php?idc=<?php echo $_GET['idc']; ?>");
        
        function submitFinanciar() {
            $.ajax({
                type: 'POST',
                url: '/adm/cereri/financiar.php?idc=<?php echo $_GET['idc']; ?>',
                data: $('#form_financiar<?php echo $_GET['idc']; ?>').serialize(),
                dataType: 'html',
                beforeSend:function(){
                    $('#financiar<?php echo $_GET['idc']; ?>').html('<img src="/images/loader.gif" alt="" />');
                },
                success: function(response) {
                    $('#financiar<?php echo $_GET['idc']; ?>').html(response);
                }
            });
            return false;
        }
        </script>
      </td>
    </tr>
    <?php } ?>
  </table>
  
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#observatii_agentie<?php echo $_GET['idc']; ?>").load("/adm/cereri/observatii.php?idc=<?php echo $_GET['idc']; ?>");
});
</script>
