<?php session_start(); include_once('../restrictionare.php');
$id_cerere=$_GET['idc']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body { font-family:Calibri, "Trebuchet MS", Arial; font-size:10.5pt; line-height:130%; color:#000; margin:5px; padding:15px; padding-bottom:25px; padding-top:25px }
.clear { content:"."; display:block; height:0; font-size:0; clear:both; visibility:hidden; }
h1 { font-size:18pt; margin:10px; padding:0; line-height:1.2em; }
.semnatura-agentie { float:left; width:100mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
.semnatura-client { float:right; width:70mm; font-size:13pt; line-height:1.3em; font-weight:bold; }
</style>
<?php include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>
<?php
$sel_boncom = "SELECT * FROM cerere_rezervare
LEFT JOIN cerere_rezervare_date_facturare ON cerere_rezervare_date_facturare.id_cerere = cerere_rezervare.id_cerere
WHERE cerere_rezervare.id_cerere = '$id_cerere'";
$que_boncom = mysql_query($sel_boncom) or die(mysql_error());
$row_boncom = mysql_fetch_array($que_boncom);
?>

<div align="justify" style="padding-left:60px">

<h1 align="center">CONTRACT<br />DE COMERCIALIZARE A PACHETULUI DE SERVICII TURISTICE<br />
  Nr. rezervare OCZ<?php echo $row_boncom['id_cerere']; ?> din <?php echo date('d.m.Y',strtotime($row_boncom['data_documente'])); ?></h1>
<br />
<strong>Părțile contractante:</strong><br />
<br />
<strong><?php echo $contact_legal_den_firma; ?></strong> - Agenție Tour Operator, cu sediul social situat în <?php echo $contact_legal_adresa; ?>, înregistrata la O.R.C. de pe lângă Tribunalul București cu nr. <?php echo $contact_legal_reg_com; ?>, CUI <?php echo $contact_legal_cui; ?>, cont nr. <?php echo $contact_legal_cont_lei; ?> - LEI și <?php echo $contact_legal_cont_euro; ?> - EURO deschis la banca <?php echo $contact_legal_banca; ?>, titulară a agenției de turism <?php echo $contact_legal_den_agentie; ?> cu punct de lucru în <?php echo $contact_adresa; ?>, tel: <?php echo $contact_telefon; ?>, e-mail <?php echo $contact_email; ?>, cu licența de turism tur-operator: 6250 / 21.12.2011, Brevet de Manager în Activitatea de Turism nr. 19004/31.08.2011, reprezentată prin <?php echo $contact_legal_reprezentant; ?> în calitate de <?php echo $contact_legal_calitate; ?>, denumită în continuare <strong>Agenție</strong><br />
<br />
și <strong>Turist</strong><br />
<br />
<strong><?php echo $row_boncom['denumire']; ?></strong>, 
<?php if($row_boncom['tip_persoana']=='persoana_fizica') {
	echo 'cu domiciliul in '.$row_boncom['adresa'].', '.$row_boncom['oras'].', ';
} elseif($row_boncom['tip_persoana']=='persoana_juridica') {
	echo 'cu sediul social situat in '.$row_boncom['adresa'].', '.$row_boncom['oras'].', avand Cod Unic de Inregistrare '.$row_boncom['cui_cnp'].', Numar de ordine in Registrul Comertului '.$row_boncom['nr_reg_comert'].', IBAN '.$row_boncom['iban'].', deschis la '.$row_boncom['banca'].', ';
} ?>
email: <?php echo $row_boncom['email']; ?>, telefon: <?php echo $row_boncom['telefon']; ?> în calitate de contractant al pachetului de servicii turistice, denumit în continuare Turist, a convenit la încheierea prezentului contract, în conformitate cu prevederile O.G. nr. 107/1999 privind activitatea de comercializare a pachetelor de servicii turistice, aprobată cu modificari și completări prin Legea nr. 631/2001.<br />
<br />
<strong> I. Obiectul contractului </strong><br />
<stong>1.1.</stong> Obiectul contractului îl constituie vânzarea de către Agenţie a pachetului de servicii turistice înscris în BONUL DE COMANDĂ la rezervarea numarul OCZ<?php echo $row_boncom['id_cerere']; ?> sau alt înscris anexat prezentului contract şi eliberarea documentelor de plată şi călătorie. <br />
<br />
<strong>II. Încheierea contractului</strong> <br />
<strong>2.1.</strong> Contractul se încheie, după caz, în oricare din următoarele situaţii: <br />
  <strong> a)</strong> în momentul semnării lui de către turist sau prin acceptarea condiţiilor contractuale de servicii turistice, inclusiv în cazul celor achiziţionate la distanţă prin mijloace electronice; <br />
<strong>b) </strong>în momentul în care turistul primeşte confirmarea scrisă a rezervării de la Agenţie, în cel mult 60 de zile calendaristice de la data întocmirii bonului de comandă. Este responsabilitatea agenţiei de turism de a informa turistul prin orice mijloace convenite în scris cu acesta (telefon, mail, fax etc.) dacă rezervarea pe care a solicitat-o s-a confirmat. Pentru procesarea unei rezervări de servicii, Agenţia poate solicita un avans de până la  <?php echo $row_boncom['procent_avans']?>% din preţul pachetului sau plata integrală a contravalorii pachetului, în funcţie de data la care turistul solicită serviciile. <br />


   <strong>1.</strong> În cazul în care conţinutul bonului de comandă nu diferă de conţinutul confirmării călătoriei turistice şi confirmarea s-a efectuat în termenul de 60 de zile calendaristice de la data semnării bonului de comandă, turistul care decide să renunţe la călătoria turistică datorează Agenţiei penalităţi conform cap. VI. <br />
   <strong>2.</strong> În cazul în care conţinutul bonului de comandă diferă de conţinutul confirmării călătoriei turistice primite în scris de la Agenţie sau dacă această confirmare nu s-a făcut în termenul de 60 de zile calendaristice de la data semnării bonului de comandă, turistul poate considera că voiajul nu a fost rezervat şi are dreptul la rambursarea imediată a tuturor sumelor deja plătite; <br />
  <strong> c)</strong> în momentul eliberării documentelor de călătorie (voucher, bilet de odihnă şi/sau tratament, bilet de excursie etc.), inclusiv în format electronic, în cazul în care pachetele de servicii turistice fac parte din oferta standard a agenţiei de turism sau există deja confirmarea de rezervare din partea altor prestatori. <br />

   <strong>2.2.</strong> În cazul în care prezentul contract este pus integral la dispoziţia turistului sub formă de catalog, pliant, alte înscrisuri, site-ul Agenţiei sau alte mijloace de comunicare electronică (e- mail, fax etc.), obligaţia de informare a turistului este considerată îndeplinită prin înscrierea acestei informaţii în oricare dintre documentele de călătorie sau documentele fiscale furnizate de Agenţie, nefiind necesară încheierea în formă scrisă a contractului de comercializare a pachetelor de servicii turistice, respectiv dacă prezentul contract prezentat turistului în modalităţile prevăzute mai sus conţine clauzele prevăzute la art. 12 alin. (2) din Ordonanţa Guvernului nr. 107/1999 privind activitatea de comercializare a pachetelor de servicii turistice, republicată.  <br />
   
     <strong> 2.3.</strong> Contractul încetează de drept odată cu finalizarea prestării efective a pachetului de servicii turistice înscris în documentele de călătorie. <br />
 <br />
  <strong>  III. Preţul contractului şi modalităţi de plată  </strong> <br />
   

 <strong>  3.1. </strong>Preţul contractului este de <strong><?php echo $row_boncom['pret']; ?> <?php echo $row_boncom['moneda']; ?></strong> şi se compune din costul serviciilor turistice efective, comisionul Agenţiei şi TVA. Dacă preţul nu este specificat în prezentul contract, atunci acesta este specificat în bonul de comandă (dacă există), alte documente de călătorie, cataloage, pliante, alte înscrisuri puse la dispoziţie turistului, site-ul Agenţiei, alte mijloace de comunicare electronică şi/sau pe factură. <br />
  <stong> 3.2.</strong> Modalităţi de plată: <br />
  <strong> 3.2.1.</strong> La încheierea contractului se percepe un avans de <?php echo $row_boncom['procent_avans']; ?>% din preţul stabilit sau, după caz, plata integrală a contravalorii pachetului de servicii turistice. 
 <strong>  3.2.2. </strong>În cazul în care la încheierea contractului s-a perceput un avans, plata finală se va face conform termenelor de plată comunicate de Agenţie în scris prin bonul de comandă sau alte mijloace de comunicare; <br />
<strong>3.3.</strong> Plata serviciilor turistice externe aferente contractului se poate efectua într-o singură monedă, în valuta specificată în contract sau în RON la cursul de referinţă al BNR din ziua emiterii facturii. <br />
<br />

   <strong>IV. Drepturile şi obligaţiile Agenţiei </strong><br  />
   <strong>4.1. </strong>Agenţia se obligă să furnizeze turistului un bon de comandă, numai în situaţia solicitării unor pachete de servicii turistice care nu fac parte din oferta proprie/standard a agenţiei de turism şi care necesită confirmarea rezervării din partea altor prestatori. În cazul solicitării unor pachete de servicii turistice care se regăsesc în oferta proprie a agenţiei de turism sau în cazul în care există deja confirmare asupra rezervării din partea altor prestatori, Agenţia poate furniza turistului un bon de comandă, dacă consideră necesar. <br />
   <strong>4.2.</strong> În cazul modificării uneia dintre prevederile esenţiale ale contractului, cum ar fi: serviciile incluse în pachet, datele de călătorie, modificarea categoriei unităţii de cazare, Agenţia are obligaţia de a informa turistul cu cel puţin 15 zile înainte de data începerii călătoriei.<br /> 
    În cazurile prevăzute la pct. 4.7 lit. b) şi c), informarea se va face în timp util pentru a permite turistului să decidă începerea călătoriei. <br />
  <strong> 4.3.</strong> În cazul achiziţionării unui pachet de servicii turistice având în componenţă şi asigurarea transportului pe cale aeriană, transportatorul aerian, fără a cere acordul agenţiei de turism care derulează programul turistic, are dreptul de a modifica orele de zbor. Prin urmare, Agenţia nu este răspunzătoare pentru decolarea/aterizarea avioanelor la o altă oră decât cea înscrisă în programul turistic. Pentru aceste întârzieri, compania aeriană este obligată să asiste turiştii conform Regulamentului (CE) nr. 261/2004 al Parlamentului European şi al Consiliului din 11 februarie 2004 de stabilire a unor norme comune în materie de compensare şi de asistenţă a pasagerilor în eventualitatea refuzului la îmbarcare şi anulării sau întârzierii prelungite a zborurilor şi de abrogare a Regulamentului (CEE) nr. 295/91, implementat prin Hotărârea Guvernului nr. 1.912/2006. Orice problemă privind operarea zborului şi acţiunile adiacente acestuia intră în competenţa şi responsabilitatea transportatorului, biletul de avion reprezentând contractul între pasager şi transportator. În cazul acestor pachete de servicii turistice, ziua de plecare şi ziua de sosire nu sunt considerate zile turistice, acestea fiind destinate transportului.<br /> 
   <strong>4.4.</strong> În situaţia achiziţionării unui produs de tip croazieră, linia de croazieră poate modifica, din motive ce ţin de siguranţa navigării, numărul cabinei şi locaţia acesteia pe punţi (o punte inferioară, una superioară, mai spre pupa sau spre prova etc.), doar cu o cabină de aceeaşi categorie ca cea rezervată iniţial, caz în care turistul nu va fi despăgubit în niciun mod având în vedere că este vorba de acelaşi tip de cabină ca şi cel rezervat. <br />
   <strong>4.5. </strong>Agenţia poate modifica preţul contractului în sensul majorării sau micşorării, după caz, arătând totodată modul de calcul al majorărilor sau micşorărilor de preţ ale contractului şi numai dacă modificarea are loc ca urmare a variaţiilor costurilor de transport, a redevenţelor şi a taxelor aferente serviciilor de aterizare, debarcare/îmbarcare în porturi şi aeroporturi, a taxelor de turist ori a cursurilor de schimb valutar aferente pachetului de servicii turistice contractat. Preţurile stabilite în contract nu pot fi majorate, în niciun caz, în cursul celor 20 de zile calendaristice care preced data plecării. <br />
  <strong> 4.6.</strong> În cazul în care, după începerea călătoriei turistice, o parte importantă din serviciile turistice prevăzute în contract nu este realizată sau Agenţia constată că nu le va putea realiza, aceasta este obligată: <br />
  <strong> a) </strong>să ofere turistului alternative corespunzătoare în vederea continuării călătoriei turistice fără majorarea preţului, respectiv serviciile turistice oferite să fie de aceeaşi calitate şi cantitate; <br />
  <strong> b)</strong> să restituie turistului sumele ce reprezintă diferenţa dintre serviciile turistice achitate şi cele efectiv prestate în timpul călătoriei turistice; 
   <strong>c) </strong>în cazul în care nu pot fi oferite turistului alternative corespunzătoare sau acesta nu le acceptă din motive întemeiate, să asigure fără costuri suplimentare transportul retur al turistului la locul de plecare ori în alt loc agreat de acesta şi, după caz, despăgubirea pentru serviciile neprestate. 
 <strong>  4.7.</strong> Agenţia este răspunzătoare pentru buna executare a obligaţiilor asumate prin contract, cu excepţia următoarelor cazuri: <br />
  <strong> a)</strong> când neîndeplinirea sau îndeplinirea defectuoasă a obligaţiilor asumate prin contract se datorează turistului; <br />
  <strong> b) </strong> când neîndeplinirea obligaţiilor se datorează unor cauze de forţă majoră sau unor împrejurări pe care nici Agenţia, nici prestatorii de servicii nu le puteau prevedea sau evita, inclusiv, dar fără a se limita la: modificarea companiei aeriene, de orar sau de itinerar, întârzieri în traficul mijloacelor de transport, defectarea mijlocului de transport, accident, blocaje, lucrări sau reparaţii pe drumurile publice), neîndeplinirea grupului minim ca urmare a unor renunţări de ultimă oră sau a neobţinerii vizelor pentru toţi participanţii. Agenţia nu este răspunzătoare pentru prejudiciile cauzate turistului ca urmare a întârzierilor curselor (inclusiv charter), a pierderii de bagaje şi a altor împrejurări care revin exclusiv în sarcina transportatorului în temeiul actelor normative specifice; <br />
  <strong> c)</strong> când neîndeplinirea obligaţiilor se datorează unui terţ care nu are legătură cu furnizarea serviciilor prevăzute în contract, iar cauzele care au determinat neîndeplinirea obligaţiilor au un caracter imprevizibil şi inevitabil. <br />
   <strong>4.8.</strong> Agenţia are obligaţia să furnizeze în scris turistului sau prin orice mijloace de comunicare electronice convenite în scris cu turistul (e-mail, fax, sms etc.), cu 3 zile, înainte de data plecării, următoarele informaţii: < br />
 <strong>  a)</strong> orarele, locurile escalelor şi legăturile, precum şi, după caz, locul ce urmează să fie ocupat de turist în fiecare dintre mijloacele de transport incluse; <br/ >
<strong>   b) </strong>denumirea, sediul/adresa, numerele de telefon şi de fax, adresele de e-mail ale reprezentanţei locale a organizatorului şi/sau a detailistului ori, în lipsa acestora, un număr de apel de urgenţă care să îi permită contactarea organizatorului şi/sau a detailistului; <br />
  <strong> c) </strong>pentru călătoriile minorilor neînsoţiţi de părinţi, informaţii care să permită părinţilor stabilirea unui contact direct cu copilul sau cu responsabilul de la locul cazării copilului; <br />
   <strong>d) </strong>obligaţiile turistului prevăzute la pct. 5.10, 5.11 şi 5.13. <br />
  <strong> 4.9.</strong> Agenţia parte în contract este obligată să acorde prompt asistenţă turistului aflat în dificultate, în cazul situaţiilor de forţă majoră sau al unui eveniment pe care nici agenţia de turism, nici furnizorul sau prestatorul de servicii, cu tot efortul depus, nu îl puteau prevedea sau evita. <br />
<br />

  <strong> V. Drepturile şi obligaţiile turistului </strong><br />
  <strong> 5.1.</strong> În cazul în care turistul nu poate să participe la călătoria turistică independent de motivele care stau la baza imposibilităţii de participare, acesta poate să cesioneze contractul unei terţe persoane care îndeplineşte toate condiţiile aplicabile pachetului de servicii turistice contractat, cu obligaţia de a anunţa în scris Agenţia cu cel puţin 5 zile înaintea datei de plecare. În acest caz, între turistul care nu poate participa la călătoria turistică contractată (cedentul), terţa persoană (cesionarul) şi agenţia de turism (debitor cedat) urmează a se încheia un contract de cesiune cu privire la pachetul de servicii turistice contractate şi cesionate. Responsabilitatea încheierii contractului de cesiune revine, după caz, fie cedentului, fie cesionarului, şi niciodată agenţiei de turism (debitorului cedat). Turistul care cedează pachetul său de servicii, precum şi cesionarul sunt responsabili în mod solidar la plata preţului călătoriei şi a eventualelor costuri suplimentare apărute cu ocazia acestei cedări. <br />
 <strong>  5.2.</strong> În cazul sejururilor de odihnă şi/sau de tratament cu locul de desfăşurare în România, turistul are obligaţia să respecte următorul program de acordare a serviciilor: cazarea se face, de regulă, la ora 18,00 a zilei de intrare şi se termină, de regulă, la ora 12,00 a zilei de ieşire înscrise pe documentele de călătorie (voucher, bilet de odihnă şi/sau tratament, bilet de excursie etc.). Eventualele costuri suplimentare generate de neeliberarea spaţiilor de cazare până cel târziu la orele specificate mai sus cad în sarcina exclusivă a turistului. <br />
  <strong> 5.3. </strong>În cazul în care preţurile stabilite în contract sunt majorate cu peste 10%, indiferent de motivele majorării, turistul poate rezilia/denunţa unilateral contractul fără nicio obligaţie faţă de Agenţie, acesta având dreptul la rambursarea imediată de către Agenţie a sumelor plătite, inclusiv comisionul. <br />
   <strong>5.4.</strong> Turistul este obligat să comunice Agenţiei în termen de 5 zile calendaristice de la primirea înştiinţării prevăzute la cap. IV pct. 4.2 modificarea prevederilor esenţiale ale contractului sau în timp util înainte de începerea călătoriei, iar în cazul în care se aplică clauzele prevăzute în cap. IV pct. 4.7 lit. b) şi c), hotărârea sa de a opta pentru: <br />
   <strong>a) </strong>rezilierea/denunţarea unilaterală a contractului fără plata penalităţilor; sau <br />
 <strong>  b) </strong>acceptarea noilor condiţii ale contractului. 
   <strong>5.5.</strong> În cazul în care turistul decide să participe la călătoria asupra căreia s-au operat modificări în condiţiile cap. IV pct. 4.2 se consideră că toate modificările au fost acceptate şi turistul nu poate solicita despăgubiri ulterioare datorate modificărilor de acest tip. <br />
<strong>   5.6.</strong> În cazul în care turistul reziliază/denunţă unilateral contractul în temeiul pct. 5.4 sau Agenţia anulează călătoria turistică înaintea datei de plecare, turistul are dreptul: <br />
 <strong>  a)</strong> să accepte la acelaşi preţ un alt pachet de servicii turistice de calitate echivalentă sau superioară, propus de Agenţie; <br />
  <strong> b)</strong> să accepte un pachet de servicii turistice de calitate inferioară propus de Agenţie, cu rambursarea imediată a diferenţei de preţ, în sensul rambursării diferenţei de preţ dintre cele două pachete turistice, la momentul încheierii noului contract de prestare de servicii; <br />
  <strong> c) </strong>să i se ramburseze imediat toate sumele achitate în virtutea contractului. <br />
   <strong>5.7.</strong> În toate cazurile menţionate la pct. 5.6, turistul are dreptul să solicite Agenţiei şi o despăgubire, al cărei cuantum poate fi stabilit prin acordul comun al părţilor sau în baza unei hotărâri a instanţei de judecată, pentru neîndeplinirea prevederilor contractului iniţial, cu excepţia cazurilor în care: <br />
  <strong> a)</strong> anularea s-a făcut datorită nerealizării numărului minim de persoane menţionat în contract, iar Agenţia a informat în scris turistul cu cel puţin 15 zile calendaristice premergătoare datei plecării; <br />
 <strong>  b) </strong>anularea s-a datorat unui caz de forţă majoră (circumstanţe imprevizibile, independente de voinţa celui care le invocă şi ale căror consecinţe nu au putut fi evitate în ciuda oricăror eforturi depuse, în aceasta nefiind incluse suprarezervările, caz în care responsabilitatea revine companiei aeriene sau unităţii de cazare) sau unor cauze dintre cele prevăzute la cap. IV pct. 4.7 lit. b); 
   c) anularea s-a făcut din vina turistului.<br /> 
   <strong>5.8.</strong> Turistul are dreptul să rezilieze/denunţe unilateral în orice moment, în tot sau în parte, contractul, iar în cazul în care rezilierea/denunţarea unilaterală îi este imputabilă este obligat sa despăgubească Agenţia pentru prejudiciul creat acesteia, conform prevederilor cap. VI, cu excepţia cazurilor de forţă majoră definite conform legii. Despăgubirea se poate ridica la maximul preţului pachetului de servicii turistice contractat. <br />
<strong>   5.9.</strong> În cazul în care turistul alege să se mute la un alt hotel decât cel contractat iniţial şi achitat, responsabilitatea financiară a renunţării îi aparţine. Agenţia va rezolva cerinţele turistului în limita posibilităţilor, eventualele diferenţe de preţ urmând a fi suportate de către turist. <br />
    Dacă turistul solicită nemotivat schimbarea hotelului, structurii camerelor sau a oricărora dintre servicii, aceasta echivalează cu rezilierea/denunţarea unilaterală a contractului, cu aplicarea penalităţilor prevăzute la cap. VI la momentul respectiv şi încheierea unui nou contract. <br  />
  <strong> 5.10.</strong> Turistul este obligat să achite la recepţia unităţii hoteliere taxa de staţiune, taxa de salubritate, precum şi alte taxe locale, fără a putea pretinde despăgubiri sau returnarea sumelor de la Agenţie. <br />
  <strong> 5.11.</strong> Turistul este obligat să prezinte la recepţia unităţii hoteliere actele sale de identitate, precum şi documentul de călătorie eliberat de Agenţie (voucher, bilet de odihnă şi/sau de tratament etc.), în vederea acordării serviciilor turistice. În cazul în care turistul beneficiază de bilete de odihnă şi tratament, este obligat să prezinte la recepţia unităţii hoteliere bilet de trimitere de la medicul de familie şi dovada plăţii contribuţiilor pentru asigurări sociale, la zi. <br />
   <strong>5.12. </strong>Turistul ia la cunoştinţă că serviciile pe care le achiziţionează fără ca acestea să facă parte din contractul cu agenţia de turism sunt în stricta responsabilitate a prestatorului local, sunt guvernate de legislaţia ţării de destinaţie, iar agenţia de turism nu are nicio responsabilitate asupra prestaţiilor în cauză. <br />
   <strong>5.13.</strong> Dacă pentru efectuarea călătoriei este necesară îndeplinirea de către turist a unor formalităţi suplimentare (de exemplu, călătoria împreună cu minori, situaţia în care numele turistului este schimbat ca urmare a căsătoriei/desfacerii ei etc.), acesta are obligaţia de a îndeplini toate cerinţele legale. Pentru o informare optimă, Agenţia recomandă şi consultarea site-ului Poliţiei de Frontieră. În cazul în care turistul nu îşi respectă obligaţia de a se informa cu privire la formalităţile suplimentare necesare în vederea efectuării călătoriei care nu sunt în sarcina Agenţiei (de exemplu, în cazul călătoriei cu minori, împuternicire din partea părintelui sau reprezentantului legal ce nu îl însoţeşte sau orice alte documente suplimentare - enumerarea fiind exemplificativă), Agenţia este exonerată de orice răspundere în cazul imposibilităţii efectuării călătoriei. <br />
  <strong> 5.14.</strong> Agenţia de turism recomandă turiştilor contactarea acesteia cu 24 de ore înainte de plecare pentru reconfirmarea detaliilor de îmbarcare (orar de zbor, loc de îmbarcare etc.). <br />
   <strong>5.15.</strong> În cazul în care o singură persoană angajează servicii pentru un număr mai mare de turişti, condiţiile contractuale se extind în mod automat asupra întregului grup pentru care au fost achitate serviciile. <br />
 <strong>  5.16.</strong> Turistul este obligat să folosească mijloacele de transport, camera de hotel şi bunurile din dotarea acesteia ca un bun proprietar şi potrivit destinaţiei lor. Agenţia nu se face vinovată de eventualele pagube produse sau vătămări suferite de turist ca urmare a nerespectării acestui alineat. <br />
  <strong> 5.17.</strong> Turistul are obligaţia să respecte locul, data şi ora plecării atât la dus, cât şi la întors, precum şi locurile, datele şi orele stabilite pe parcursul programului turistic contractat. Toate cheltuielile şi daunele produse ca urmare a nerespectării de către turist a prevederilor privind locurile de întâlnire şi orarele vor fi suportate de către acesta. <br />
  <strong> 5.18.</strong> În cazul în care turistul care a intrat pe teritoriul statului în care se realizează pachetul de servicii turistice refuză să se mai întoarcă în România şi autorităţile din ţara respectivă fac cheltuieli de orice natură cu acesta, turistul respectiv are obligaţia de a suporta toate aceste cheltuieli.<br /> <br />
 <strong>  VI. Renunţări, penalizări, despăgubiri </strong><br />
   <strong>6.1.</strong> În cazul în care turistul renunţă din vina sa la pachetul de servicii turistice care face obiectul prezentului contract, el datorează Agenţiei penalizări după cum urmează: <br />
<strong>a)</strong>	30% din preţul pachetului de servicii, dacă renunţarea se face cu mai mult de 40 zile calendaristice înainte de data plecării;<br />
<strong>b)</strong>	50% din preţul pachetului de servicii, dacă renunţarea se face în intervalul 16-40 zile calendaristice înainte de data plecării;<br />
<strong>c)</strong>	80% din preţul pachetului de servicii, dacă renunţarea se face într-un interval mai mic de 16 zile calendaristice înainte de plecare cu condiția rezervării pentru pachetele de catalog, neincluzând condițiile speciale (early booking, rezervări timpurii); <br />
   <strong>d) </strong>100% din preţul pachetului de servicii, dacă:<br />
- renunţarea se face într-un interval mai mic de 15 zile înainte de plecare<br />
- turistul nu este lăsat să treaca la una din frontiere din motive care sunt legate exclusiv de persoana sa sau din motive care nu pot fi imputate Agenției<br />
- turistul nu respectă condițiile de vânzare ale Agenției, dacă prezintă acte incomplete sau false, nu are pașaport sau nu achită excursia în termenele stabilite<br />
- turistul a achiziţionat pachetul de servicii turistice din cadrul programului special (de exemplu, Early Booking etc.), indiferent de data la care turistul solicită renunţarea. <br />
 	Aceste penalizări se aplică în toate cazurile, cu excepția celor în care programul valorificat / confirmat are propriile reguli de achitare, anulare, penalizare (ex. programe de Paște, Crăciun, Revelion, rezervări individuale, croaziere, destinații exotice). În acest caz se aplică regulile speciale ale fiecarui program în parte, astfel:<br />
- pentru cursele charter - condiții speciale fiecărei destinații, specificate în Programul Turistic și broșură (dacă este cazul);<br />
- pentru excursii - condiții speciale fiecărei destinații, specificate în Programul Turistic;<br />
- pentru rezervările efectuate la agențiile organizatoare străine (germane, austriece, olandeze, etc.) și croziere - condițiile specifice fiecărui sistem de rezervări on-line (Der Tour, Amadeus-condițiile IATA, Schmetterling, etc.);<br />
- pentru acțiuni speciale (campionate sportive, evenimente culturale, etc) - condiții specifice menționate în Programul Turistic furnizat și broșura (dacă este cazul)
<br /><strong>Penalizările de mai sus NU se aplică în cazul în care mai jos sunt trecute condiții speciale, caz în care se vor respecta acestea.</strong><br />
<?php echo nl2br($row_boncom['conditii_anulare']); ?><br />

<strong>6.2.</strong> În cazul în care turistul care a contractat un pachet de servicii turistice cu Agenţia şi a achitat un avans nu se prezintă în termenul specificat în bonul de comandă sau în termenul comunicat în scris, pentru a achita ratele aferente sau restul de plată, contractul se consideră reziliat de drept, iar Agenţia are dreptul de a anula rezervările efectuate în beneficiul turistului cu reţinerea penalizărilor prevăzute la pct. 6.1. <br />
  <strong> 6.3.</strong> Pentru biletele de odihnă şi/sau de tratament cumpărate prin organizaţii sindicale, Agenţia va face restituiri numai în baza cererilor de renunţare contrasemnate şi ştampilate de reprezentantul organizaţiei sindicale. <br />
  <strong> 6.4. </strong>În cazul în care o ambasadă refuză să acorde viza de intrare pentru efectuarea pachetului de servicii, turistului i se vor reţine toate taxele datorate de Agenţie prestatorilor direcţi, precum şi cheltuielile de operare proprii acesteia. <br />
  <strong> 6.5.</strong> Penalizările echivalente cu cele indicate la pct. 6.1 lit. d), se aplică şi în cazul în care turistul nu ajunge la timp la aeroport sau la locul de plecare/destinaţie, dacă nu poate pleca în călătorie pentru că documentele personale necesare în vederea efectuării călătoriei nu sunt conforme normelor legale sau este în imposibilitatea de a părăsi teritoriului ţării, din alte motive ce ţin de persoana acestuia. <br />
  <strong> 6.6.</strong> Turistul trebuie să depună în scris cererea de renunţare la pachetul de servicii turistice la Agenţia la care a achitat serviciile. În caz contrar, cererea de renunţare nu este luată în considerare.<br /> 
  <strong> 6.7.</strong> Agenţia va acorda despăgubiri în funcţie de gradul de nerespectare a obligaţiilor din contract. <br />
  <strong> 6.8.</strong> Agenţia nu răspunde în situaţii de grevă, conflicte politice şi de război, catastrofe, de pericol public, atac terorist, embargou internaţional, precum şi în cazul în care companiile aeriene stabilesc limite de răspundere. Toate aceste situaţii care nu sunt imputabile niciunei părţi se consideră situaţii de forţă majoră şi exonerează de răspundere Agenţia. <br />
  <strong> 6.9.</strong> Toate sumele menţionate la pct. 6.1, 6.2, 6.4 şi 6.5 se vor reţine de către Agenţie din avansul sau preţul total al pachetului de servicii turistice achitat de turist, fără a fi necesară intervenţia instanţelor de judecată. <br />
<br />
<strong>VII. Reclamaţii </strong><br />
   <strong>7.1. </strong>În cazul în care turistul este nemulţumit de serviciile turistice primite, acesta are obligaţia de a întocmi o sesizare în scris la faţa locului, clar şi explicit, cu privire la deficienţele constatate, legate de realizarea pachetului de servicii turistice contractat, ce se va transmite prompt atât reprezentantului Agenţiei, cât şi prestatorului de servicii turistice (conducerii hotelului, restaurantului, reprezentanţilor locali ai turoperatorului). <br />
    Datele de contact ale Agenţiei: <br />
    Telefon:<?php echo $contact_telefon; ?><br />
    E-mail: <?php echo $contact_email; ?> <br />
  <strong> 7.2.</strong> Atât Agenţia, cât şi prestatorul de servicii turistice vor acţiona imediat pentru soluţionarea sesizării. În cazul în care sesizarea nu este soluţionată sau este soluţionată parţial, turistul va depune la sediul Agenţiei o reclamaţie în scris, în termen de maximum 5 zile calendaristice de la încheierea călătoriei, Agenţia urmând ca, în termen de 30 de zile calendaristice, să comunice turistului despăgubirile care i se cuvin, după caz. <br />
  <br />
 <strong> VIII. Asigurări </strong><br />
   <strong>8.1.</strong> Turistul este asigurat pentru riscul de insolvabilitate şi/sau de faliment al Agenţiei <?php echo $contact_legal_den_agentie; ?> la Societatea de asigurări <?php echo $contact_legal_asigurator; ?> cu poliță <?php echo $contact_legal_asigurare; ?> este afişată pe pagina web a agenţiei de turism www.ocaziituristice.ro  <br />
 <strong>  8.2.</strong> Condiţiile în care turistul va fi despăgubit de către societatea de asigurare sunt:<br /> 
  <strong> 8.2.1.</strong> În cazul în care Agenţia nu efectuează repatrierea turistului, acesta are obligaţia de a anunţa imediat societatea de asigurare prin telefon, fax sau e-mail. În această situaţie societatea de asigurare nu are obligaţia de a achita imediat contravaloarea cheltuielilor de repatriere, ci de a le rambursa după întoarcerea turistului în România, în condiţiile poliţei de asigurare încheiate între Agenţie şi societatea de asigurare. <br />
   <strong>8.2.2.</strong> În cazul în care turistul solicită Agenţiei contravaloarea sumelor achitate şi/sau a cheltuielilor de repatriere, acesta trebuie să trimită documentele justificative către Agenţie prin scrisoare recomandată cu confirmare de primire. Turistul are obligaţia să păstreze fotocopii de pe respectivele documente justificative. Turistul poate solicita Agenţiei rambursarea sumelor achitate şi/sau a cheltuielilor de repatriere în termen de 15 (cincisprezece) zile calendaristice de la data încheierii derulării pachetului de servicii turistice sau de la data repatrierii.<br /> 
  <strong> 8.2.3.</strong> Turistul are obligaţia de a notifica societăţii de asigurare, prin scrisoare recomandată cu confirmare de primire, în legătură cu solicitarea adresată Agenţiei privind rambursarea sumelor achitate şi/sau a cheltuielilor de repatriere, în termen de 5 (cinci) zile calendaristice de la data confirmării de primire prevăzute la pct. 8.2.2. <br />
   <strong>8.2.4.</strong> În cazul în care, în termen de 15 (cincisprezece) zile calendaristice de la data confirmării de primire a documentelor justificative de către Agenţie, turistul nu a primit sumele solicitate de la aceasta, are loc evenimentul asigurat. <br />
   <strong>8.2.5.</strong> În termen de 10 (zece) zile calendaristice de la data producerii evenimentului asigurat, turistul are obligaţia de a transmite societăţii de asigurare, prin scrisoare recomandată cu confirmare de primire, cererea de despăgubire însoţită de documentele justificative. <br />
   <strong>8.2.6.</strong> Documentele justificative constau în principal în: <br />
   a) contractul de comercializare a pachetului de servicii turistice; <br />
   b) confirmările de primire precizate la pct. 8.2.2, 8.2.3 şi 8.2.5; <br />
   c) fotocopiile de pe documentele de plată a avansului (chitanţe, ordine de plată etc.), în cazul cererilor de rambursare a sumelor achitate de turist; <br />
   d) fotocopiile de pe documentele de transport şi cazare, în cazul cererilor de rambursare a cheltuielilor de repatriere. Societatea de asigurare are dreptul să solicite turistului şi alte documente justificative. <br />
  <strong> 8.2.7. </strong>Despăgubirea nu poate depăşi suma achitată de turist în contractul de comercializare a pachetului de servicii turistice şi nici sumele necesare repatrierii acestuia, cu respectarea prevederilor legale în vigoare. <br />
  <strong> 8.2.8. </strong>Despăgubirea va fi plătită în termen de 30 (treizeci) de zile calendaristice de la data primirii de către societatea de asigurare a documentelor justificative de la turist. <br />
  <strong> 8.2.9.</strong> În cazul în care, după plata despăgubirii, Agenţia plăteşte debitul către turist, acesta are obligaţia de a restitui asiguratorului despăgubirea primită, în termen de 5 (cinci) zile lucrătoare de la data primirii de la Agenţie a sumelor reprezentând debitul. <br />
  <strong> 8.2.10. </strong>Facultativ, turistul are posibilitatea încheierii unui contract de asigurare, care să acopere taxele de transfer, sau a unui contract de asistenţă care să acopere taxele de repatriere în caz de accidente, de boală sau deces, a unui contract de asigurare pentru bagaje, a unui contract de asigurare pentru servicii medicale la destinaţie ori a unei asigurări storno sau altor tipuri de asigurări de călătorie. Agenţia recomandă încheierea unei asigurări storno pentru acoperirea eventualelor penalităţi de anulare. Turistul se poate informa în agenţii despre cazurile acoperite de asigurarea storno, aceasta putând fi încheiată în agenţia de turism de unde acesta a achiziţionat pachetul de servicii turistice, dacă Agenţia oferă acest tip de serviciu. <br />
   <strong>8.3.</strong> Agenţia nu se face vinovată de eventuala nerespectare a obligaţiilor stipulate în poliţele de asigurare contractate prin intermediul Agenţiei, deoarece aceasta este doar intermediar între turist şi asigurator. <br />
  <br />
  <strong> IX. Documentele contractului se constituie ca anexă la acesta şi sunt următoarele: </strong><br />
  <strong> a) </strong>voucherul, biletul de odihnă/tratament, biletul de excursie, bonul de comandă, după caz; <br />
  <strong> b) </strong>programul turistic, în cazul acţiunilor turistice; <br />
   <strong>c) </strong>cataloage/pliante/oferte/alte înscrisuri/etc. ale Agenţiei puse la dispoziţia turistului, în format tipărit sau pe suport electronic. <br />
  <br />
  <strong> X. Dispoziţii finale </strong><br />
   <strong>10.1. </strong>Prezentul contract a fost încheiat în două exemplare, câte unul pentru fiecare parte. <br />
  <strong> 10.2.</strong> Comercializarea pachetelor de servicii turistice se va face în conformitate cu prevederile prezentului contract şi cu respectarea prevederilor Ordonanţei Guvernului nr. 107/1999, republicată. <br />
 <strong>  10.3. </strong>Toate unităţile de cazare, precum şi mijloacele de transport sunt clasificate de către organismele abilitate ale ţărilor de destinaţie, conform procedurilor interne şi normativelor locale, acolo unde acestea există, care diferă de la o ţară la alta şi de la un tip de destinaţie la altul. <br />
 <strong>  10.4.</strong> Turistul declară că Agenţia de turism l-a informat complet cu privire la condiţiile de comercializare a pachetelor de servicii turistice în conformitate cu prevederile Ordonanţei Guvernului nr. 107/1999, republicată. Prin semnarea acestui contract sau prin acceptarea pachetelor de servicii turistice, inclusiv în cazul celor achiziţionate la distanţă prin mijloace electronice, turistul îşi exprimă acordul şi luarea la cunoştinţă cu privire la condiţiile generale de comercializare a pachetelor de servicii turistice, în conformitate cu oferta agenţiei de turism. <br />
 <strong>  10.5.</strong> Litigiile apărute între părţi se rezolvă pe cale amiabilă, în caz contrar părţile înţeleg să se adreseze instanţelor de judecată competente. <br />
   <strong>10.6.</strong> Contractul va fi interpretat conform legilor din România. <br />
<br />
<br />
<br />

<div class="semnatura-agentie">
  Agenție,<br />
  <?php echo $contact_legal_den_firma; ?><br />
  <?php if($_GET['stampila']=='da') echo '<img src="/images/stampila.png">'; ?>
</div>

<div class="semnatura-client">
  Turist,<br />
  <?php echo $row_boncom['denumire']; ?>
</div>

<div class="clear"></div>

</div>
</body>
</html>
