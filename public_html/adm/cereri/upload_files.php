<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');

$idc=$_GET['idc'];

if($_POST['adauga']) {
	if(basename($_FILES['fisier']['name'])!='') {
		$target_path='uploads/cereri/';
		$upfile='id'.$idc.'_'.date('His').'_'.basename($_FILES['fisier']['name']);
		$target_path=$target_path.$upfile; 
		
		if(move_uploaded_file($_FILES['fisier']['tmp_name'], $target_path)) {
			echo "The file ".basename($_FILES['fisier']['name'])." has been uploaded";
		} else {
			echo "There was an error uploading the file, please try again!";
		}
		
		$ins_upload="INSERT INTO cerere_rezervare_files SET
		id_cerere='".$idc."',
		fisier='".$upfile."',
		denumire='".trim($_POST['upload_denumire'])."',
		data_adaugare=NOW()
		";
		$que_upload=mysql_query($ins_upload) or die(mysql_error());
	}
}

if($_GET['delete']) {
	$sel_upload="SELECT * FROM cerere_rezervare_files WHERE id_upload='".$_GET['delete']."' ";
	$que_upload=mysql_query($sel_upload) or die(mysql_error());
	$row_upload=mysql_fetch_array($que_upload);
	
	unlink('uploads/cereri/'.$row_upload['fisier']);

	$del_upload="DELETE FROM cerere_rezervare_files WHERE id_upload='".$_GET['delete']."' ";
	$que_upload=mysql_query($del_upload) or die(mysql_error());
	
	echo "<script> alert('Fisierul a fost sters!'); document.location.href='cereri.php?pas=upload_files&idc=".$idc."'; </script>";
}

$sel_usr="SELECT
useri_fizice.nume,
useri_fizice.prenume,
oferte.denumire,
cerere_rezervare_date_facturare.denumire as denumire_date_facturare
FROM
cerere_rezervare
Left Join useri_fizice ON useri_fizice.id_useri_fizice = cerere_rezervare.id_useri_fizice
Left Join oferte ON oferte.id_oferta = cerere_rezervare.id_oferta
Inner Join cerere_rezervare_date_facturare ON cerere_rezervare.id_cerere = cerere_rezervare_date_facturare.id_cerere
WHERE cerere_rezervare.id_cerere='".$idc."'
";
$que_usr=mysql_query($sel_usr) or die(mysql_error());
$row_usr=mysql_fetch_array($que_usr);
?>
<h1>Gestioneaza upload fisiere pentru <strong>ID <?php echo $idc; ?></strong><br />
<?php echo '<strong class="blue">'.$row_usr['denumire_date_facturare'].' / '.$row_usr['prenume'].' '.$row_usr['nume'].'</strong> - <span class="red">'.$row_usr['denumire'].'</span>'; ?></h1>
<form action="" method="post" enctype="multipart/form-data">
  <table width="100%" border="0" cellspacing="2" cellpadding="4" class="default">
    <tr>
      <th valign="top" align="right" width="160">Denumire:</th>
      <td><input type="text" name="upload_denumire" value="" class="mare" /></td>
    </tr>
    <tr>
      <th valign="top" align="right" width="160">Fisier:</th>
      <td><input type="file" name="fisier" value="" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="left"><input type="submit" name="adauga" value=" Adauga fisier " class="buttons" /></td>
    </tr>
  </table>
</form>
<br><br>
<div style="font-size:1.2em; line-height:1.5em;">
<?php
$sel_files="SELECT * FROM cerere_rezervare_files WHERE id_cerere='".$idc."' ";
$que_files=mysql_query($sel_files) or die(mysql_error());
while($row_files=mysql_fetch_array($que_files)) {
	echo '<a href="uploads/cereri/'.$row_files['fisier'].'" target="_blank" class="blue"><strong>Download</strong></a>';
	if($row_files['denumire']) echo ' - <strong>'.$row_files['denumire'].'</strong>';
	echo ' - '.$row_files['fisier'];
	echo ' - <a href="cereri.php?pas=upload_files&idc='.$idc.'&delete='.$row_files['id_upload'].'" class="red" onClick="confirm(\'Esti sigur ca doresti sa stergi fisierul: '.$row_files['fisier'].' ?\')">Sterge &raquo;</a><br />';
}
?>
</div>
