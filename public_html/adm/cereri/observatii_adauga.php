<?php /*?><script src="/js/jquery_ajax_submit/runonload.js"></script>
<script src="/js/jquery_ajax_submit/tutorial.js"></script><?php */?>
<script type="text/javascript">
$(function() {
	$('.error<?php echo $_GET['idc']; ?>').hide();
	
	$(".button<?php echo $_GET['idc']; ?>").click(function() {
		$('.error<?php echo $_GET['idc']; ?>').hide();
		
		//var name = $("input#name").val();
		var name = document.getElementById("name<?php echo $_GET['idc']; ?>").value;
		if (name == "") {
			$("label#name<?php echo $_GET['idc']; ?>_error").show();
			$("input#name<?php echo $_GET['idc']; ?>").focus();
			return false;
		}
		var id_cerere = document.getElementById("id_cerere<?php echo $_GET['idc']; ?>").value;
		
		var dataString = 'name=' + name + '&id_cerere=' + id_cerere;
		//alert (dataString);return false;
		
		$.ajax({
			type: "POST",
			url: "/adm/cereri/observatii_proces.php",
			data: dataString,
			success: function() {
				$('#observatii_form<?php echo $_GET['idc']; ?>').html("<div id='message<?php echo $_GET['idc']; ?>'></div>");
				$('#message<?php echo $_GET['idc']; ?>').html("<strong class='red'>Observatia a fost adaugata!</strong>");
			}
		});
		return false;
	});
});
</script>
<div id="observatii_form<?php echo $_GET['idc']; ?>">
  <form name="observatii<?php echo $_GET['idc']; ?>" method="post" action="">
    <textarea name="name<?php echo $_GET['idc']; ?>" id="name<?php echo $_GET['idc']; ?>" class="text-input" style="width:98%; height:45px;"></textarea>
    <input type="hidden" name="id_cerere<?php echo $_GET['idc']; ?>" id="id_cerere<?php echo $_GET['idc']; ?>" value="<?php echo $_GET['idc']; ?>" />
    <label class="error<?php echo $_GET['idc']; ?> red" for="name<?php echo $_GET['idc']; ?>" id="name<?php echo $_GET['idc']; ?>_error">Camp obligatoriu.</label><br />
    <input type="submit" name="submit" class="button<?php echo $_GET['idc']; ?> buttons" id="submit_btn" value="Adauga observatie" />
  </form>
</div>
