<?php class ADAUGARE_POZA {
	function adauga($attachments, $tip, $nume, $poza_booking=NULL) {
		ini_set('memory_limit', '1024M');
		$error = array();
		include_once $_SERVER['DOCUMENT_ROOT']."/config/SimpleImage.php";
		include_once $_SERVER['DOCUMENT_ROOT']."/config/snapshot.class.php";
		include $_SERVER['DOCUMENT_ROOT'].'/config/config_poze.php';
		
		if(sizeof($attachments)>'0') {
			foreach($attachments['name'] as $file_id => $file) {
				if($file) {
					$i = $file_id;
					if($file != NULL && (strstr(strtolower($file), 'jpg') || strstr(strtolower($file), 'jpeg') ) ) {
						$ext=substr($file, strripos($file, '.'));
						$foto = rand(1000, 9999).$nume.$i.$tip.$ext;
						$url = $_SERVER['DOCUMENT_ROOT'].'/img_'.$tip.'/'.$foto;
						if(copy($attachments['tmp_name'][$file_id], $url)) {
							list($width, $height, $type, $attr) = getimagesize($url);
							if(($width>=$dim_poze[$tip]['thumb']['width'] && $height>=$dim_poze[$tip]['thumb']['height']) && ($width<=$dim_poze['maxim']['width'] && $height<=$dim_poze['maxim']['height'])) {
								$images['poza'][$i] = $foto;
								if($width>$dim_poze[$tip]['prima']['width'] || $height>$dim_poze[$tip]['prima']['height']) {
									$maxwidth = $dim_poze[$tip]['prima']['width'];
									$maxHeight = $dim_poze[$tip]['prima']['height'];
									$image = new SimpleImage();
									$image->load($url);
									if($width<=$height) {
										$image->resizeToHeight($maxHeight);
									} else {
										$image->resizeToWidth($maxwidth);
									}
									//$image->scale(100);
									//$image->resize($maxwidth,$maxHeight);
									$image->save($_SERVER['DOCUMENT_ROOT'].'/img_prima_'.$tip.'/'.$foto);
									unset($image);
								} else {
									$url2 = $_SERVER['DOCUMENT_ROOT'].'/img_prima_'.$tip.'/'.$foto;
									copy($url, $url2);
								}
								
								//thumb___
								$maxwidth = $dim_poze[$tip]['thumb']['width'];
								$maxHeight = $dim_poze[$tip]['thumb']['height'];
								$myimage = new ImageSnapshot;
								//If loading from an uploaded file:
								$myimage->ImageField = $url;
								//OR if loading from a variable containing the image contents:
								//$myimage->ImageContents = $my_image_var;
								$myimage->Width = $maxwidth;
								$myimage->Height = $maxHeight;
								$myimage->Resize = 'true'; //if false, snapshot takes a portion from the unsized image.
								$myimage->ResizeScale = '100';
								$myimage->Position = 'center';
								$myimage->Compression = $quality[$tip];
								$dest=$_SERVER['DOCUMENT_ROOT'].'/thumb_'.$tip.'/'.$foto;
								$myimage->SaveImageAs($dest);
								
								if($tip=='hotel' || $tip=='circuit') {
									//mediu___
									$maxwidth = $dim_poze[$tip]['mediu']['width'];
									$maxHeight = $dim_poze[$tip]['mediu']['height'];
									$myimage = new ImageSnapshot;
									//If loading from an uploaded file:
									$myimage->ImageField = $url;
									//OR if loading from a variable containing the image contents:
									//$myimage->ImageContents = $my_image_var;
									$myimage->Width = $maxwidth;
									$myimage->Height = $maxHeight;
									$myimage->Resize = 'true'; //if false, snapshot takes a portion from the unsized image.
									$myimage->ResizeScale = '100';
									$myimage->Position = 'center';
									$myimage->Compression = $quality[$tip];
									$dest=$_SERVER['DOCUMENT_ROOT'].'/img_mediu_'.$tip.'/'.$foto;
									$myimage->SaveImageAs($dest);
								}
								
								if($tip=='foto_general') {
									//mediu___
									$maxwidth = $dim_poze[$tip]['mediu']['width'];
									$maxHeight = $dim_poze[$tip]['mediu']['height'];
									$myimage = new ImageSnapshot;
									//If loading from an uploaded file:
									$myimage->ImageField = $url;
									//OR if loading from a variable containing the image contents:
									//$myimage->ImageContents = $my_image_var;
									$myimage->Width = $maxwidth;
									$myimage->Height = $maxHeight;
									$myimage->Resize = 'true'; //if false, snapshot takes a portion from the unsized image.
									$myimage->ResizeScale = '100';
									$myimage->Position = 'center';
									$myimage->Compression = $quality[$tip];
									$dest=$_SERVER['DOCUMENT_ROOT'].'/images/foto-zone/'.$foto;
									$myimage->SaveImageAs($dest);
								}
								
								
							} else {
								unlink($url);
								$images['error'][$i]="Poza $i nu se incadreaza in dimensiunile minime sau maxime.<br>Va rugam uploadati poze mai mari de ".$dim_poze[$tip]['thumb']['width']." pixeli si mai mici de ".$dim_poze['maxim']['width']." pixeli.";
							}
						} else $images['error'][$i]='Poza '.$i.' nu s-a putut uploada!';
					} else $images['error'][$i]='Poza '.$i.' nu este jpg!';
				}
			}
		}

		if(sizeof($poza_booking)>'0') {
			foreach($poza_booking as $file_id => $file) {
				if($file) {
					$i = $file_id;
					if($file != NULL) {
						$nume_poza = explode("/",str_replace("https://","",$file));
						$filename = $nume_poza[5];
						$folder = $nume_poza[4];
						$ext=substr($filename, strripos($filename, '.'));
						$foto = rand(1000, 9999).$nume.$i.$tip.$ext;
						$url = $_SERVER['DOCUMENT_ROOT'].'/img_'.$tip.'/'.$foto;
						if(copy("https://t-ec.bstatic.com/images/hotel/max1024x768/".$folder."/".$filename, $url)) {
						//if(copy("http://r-ec.bstatic.com/images/hotel/max1024x768/".$folder."/".$filename, $url)) {
							list($width, $height, $type, $attr) = getimagesize($url);
							if(($width>=$dim_poze[$tip]['thumb']['width'] && $height>=$dim_poze[$tip]['thumb']['height']) && ($width<=$dim_poze['maxim']['width'] && $height<=$dim_poze['maxim']['height'])) {
								$images['poza'][$i] = $foto;
								if($width>$dim_poze[$tip]['prima']['width'] || $height>$dim_poze[$tip]['prima']['height']) {
									$maxwidth = $dim_poze[$tip]['prima']['width'];
									$maxHeight = $dim_poze[$tip]['prima']['height'];
									$image = new SimpleImage();
									$image->load($url);
									if($width<=$height) {
										$image->resizeToHeight($maxHeight);
									} else {
										$image->resizeToWidth($maxwidth);
									}
									//$image->scale(100);
									//$image->resize($maxwidth,$maxHeight);
									$image->save($_SERVER['DOCUMENT_ROOT'].'/img_prima_'.$tip.'/'.$foto);
									unset($image);
								} else {
									$url2 = $_SERVER['DOCUMENT_ROOT'].'/img_prima_'.$tip.'/'.$foto;
									copy($url, $url2);
								}
								
								//thumb___
								$maxwidth = $dim_poze[$tip]['thumb']['width'];
								$maxHeight = $dim_poze[$tip]['thumb']['height'];
								$myimage = new ImageSnapshot;
								//If loading from an uploaded file:
								$myimage->ImageField = $url;
								//OR if loading from a variable containing the image contents:
								//$myimage->ImageContents = $my_image_var;
								$myimage->Width = $maxwidth;
								$myimage->Height = $maxHeight;
								$myimage->Resize = 'true'; //if false, snapshot takes a portion from the unsized image.
								$myimage->ResizeScale = '100';
								$myimage->Position = 'center';
								$myimage->Compression = $quality[$tip];
								$dest=$_SERVER['DOCUMENT_ROOT'].'/thumb_'.$tip.'/'.$foto;
								$myimage->SaveImageAs($dest);
								
								if($tip=='hotel' || $tip=='circuit') {
									//mediu___
									$maxwidth = $dim_poze[$tip]['mediu']['width'];
									$maxHeight = $dim_poze[$tip]['mediu']['height'];
									$myimage = new ImageSnapshot;
									//If loading from an uploaded file:
									$myimage->ImageField = $url;
									//OR if loading from a variable containing the image contents:
									//$myimage->ImageContents = $my_image_var;
									$myimage->Width = $maxwidth;
									$myimage->Height = $maxHeight;
									$myimage->Resize = 'true'; //if false, snapshot takes a portion from the unsized image.
									$myimage->ResizeScale = '100';
									$myimage->Position = 'center';
									$myimage->Compression = $quality[$tip];
									$dest=$_SERVER['DOCUMENT_ROOT'].'/img_mediu_'.$tip.'/'.$foto;
									$myimage->SaveImageAs($dest);
								}
							} else {
								unlink($url);
								$images['error'][$i]="Poza $i nu se incadreaza in dimensiunile minime sau maxime.<br>Va rugam uploadati poze mai mari de ".$dim_poze[$tip]['thumb']['width']." pixeli si mai mici de ".$dim_poze['maxim']['width']." pixeli.";
							}
						} else $images['error'][$i]='Poza '.$i.' nu s-a putut uploada!';
					} //else $images['error'][$i]='Poza '.$i.' nu este jpg!';
				}
			}
		}
		return $images;
	}
}
?>