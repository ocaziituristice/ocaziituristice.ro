<?php
class CIRCUIT {
	function copieza($id_oferta, $param, $hotel, $tip) {
		$ins='INSERT INTO hoteluri (
			SELECT
			NULL AS id_hotel,
			locatie_id,
			"'.$param['denumire'].'" as nume,
			stele,
			descriere,
			descriere_scurta,
			poza1,
			poza2,
			poza3,
			poza4,
			poza5,
			poza6,
			poza7,
			poza8,
			observatii,
			"'.$_SESSION['id_user_adm'].'" as proprietar,
			descriere_seo,
			latitudine,
			longitudine,
			facilitati,
			vot_ok,
			vot_neok,
			"nu" as verificat,
			acceptat,
			titlu_seo,
			cuvinte_cheie_seo,
			adresa,
			nr,
			cod_postal,
			email,
			website,
			"Circuit" as tip_unitate,
			concept,
			distanta_fata_de,
			distanta,
			general,
			servicii,
			internet,
			parcare,
			check_in,
			check_out,
			anulare_plata,
			info_copii,
			accepta_animale,
			carduri,
			garantam,
			SYSDATE() as data_adaugarii,
			null as proprietar_modificare,
			null as ultima_modificare,
			telefon,
			stare,
			observatii_stare,
			obiectice_turistice,
			id_continent,
			apare_grad,
			corespodent_teztour,
			cod_remarketing
			FROM hoteluri
			WHERE id_hotel = "'.$hotel.'"
		)';
		$que=mysql_query($ins) or die(mysql_error());
		$id_hotel=mysql_insert_id();
		@mysql_free_result($que);
		
		$ins='insert into traseu_circuit(select "'.$id_hotel.'" as id_hotel_parinte, id_tara, id_localitate, id_hotel, detalii, tara_principala, ordine from traseu_circuit where id_hotel_parinte = "'.$hotel.'")';
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		$ins='insert into camere_hotel (select "'.$id_hotel.'" as id_hotel, id_camera, detalii, ordine from camere_hotel where id_hotel = "'.$hotel.'")';
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		$last_minute='da';
		$parinte=$id_oferta;
		
		$ins='insert into oferte (select null as id_oferta, valabila, "'.$param['denumire'].'" as denumire, denumire_scurta, exprimare_pret, start_date, end_date, "'.$id_hotel.'" as id_hotel,  nr_zile,  nr_nopti, id_transport, descriere_oferta, null as ultima_modificare, null as proprietar_modificare, SYSDATE() as data_adaugarii, nota, observatii, "'.$_SESSION['id_user_adm'].'" as proprietar, masa, excursii, furnizor, observatii_furnizor, discount_tarif, pret_minim, moneda, click, pret_minim_lei, cazare, comision_fix, moneda_comision, procent_comision, obsercatii_comision, tip_preturi, "da" as last_minute, "'.$parinte.'" as id_parinte, inactiva, recomandata, detalii_recomandata, pret_recomandata, oferta_parinte_early, id_oferta_pivot, nr_formula_pivot, calculeaza_pret, id_spo, apare_grad, rezervare_online, new_layout, fisier_upload, online_prices, oferta_saptamanii, oferta_speciala from oferte where id_oferta = "'.$id_oferta.'" )';
		$que=mysql_query($ins) or die(mysql_error());
		$id_oferta_nou=mysql_insert_id();
		@mysql_free_result($que);
		
		$insE="insert into early_booking (select '".$id_oferta_nou."' as id_oferta, tip, end_date, discount, early_inclus, text_early from early_booking where id_oferta = '".$id_oferta."' and tip = 'sejur') ";
		$queE=mysql_query($insE) or die(mysql_error());
		@mysql_free_result($queE);
		
		$insE="insert into oferta_sejur_tip (select '".$id_oferta_nou."' as id_oferta, id_tip_oferta from oferta_sejur_tip where id_oferta = '".$id_oferta."') ";
		$queE=mysql_query($insE) or die(mysql_error());
		@mysql_free_result($queE);
		
		$insS="insert into oferte_servicii (select '".$id_oferta_nou."' as id_oferta, denumire, value, data_start, data_end, moneda, semn, tip_serv,  tip_plata, obligatoriu, tip_supliment, ordonare from oferte_servicii where id_oferta = '".$id_oferta."') ";
		$queS=mysql_query($insS) or die(mysql_error());
		@mysql_free_result($queS);
		
		$insPr="insert into data_pret_oferta (select '".$id_oferta_nou."' as id_oferta, data_start, data_end, tip_camera, pret, moneda, id_hotel, of_logictour, pret_pivot from data_pret_oferta where id_oferta = '".$id_oferta."')";
		$quePr=mysql_query($insPr) or die(mysql_error());
		@mysql_free_result($quePr);
		
		$insN="insert into reduceri_speciale (select '".$id_oferta_nou."' as id_oferta, data_start, data_end, zile_deaplicare, zile_aplicare, ordonare from reduceri_speciale where id_oferta = '".$id_oferta."') ";
		$queN=mysql_query($insN) or die(mysql_error());
		@mysql_free_result($queN);
		
		$insN="insert into nr_nopti_minim (select '".$id_oferta_nou."' as id_oferta, data_start, data_end, nr_min, ordonare from nr_nopti_minim where id_oferta = '".$id_oferta."') ";
		$queN=mysql_query($insN) or die(mysql_error());
		@mysql_free_result($queN);
		
		$insN="insert into zile_anulare (select '".$id_oferta_nou."' as id_oferta, data_start, data_end, zile_anulare, ordonare from zile_anulare where id_oferta = '".$id_oferta."') ";
		$queN=mysql_query($insN) or die(mysql_error());
		@mysql_free_result($queN);
		
		$insN="insert into zile_plata (select '".$id_oferta_nou."' as id_oferta, data_start, data_end, zile_plata, ordonare from zile_plata where id_oferta = '".$id_oferta."') ";
		$queN=mysql_query($insN) or die(mysql_error());
		@mysql_free_result($queN);
		
		$insN="insert into sold_out (select '".$id_oferta_nou."' as id_oferta, data_start, data_end, camera, ordonare from sold_out where id_oferta = '".$id_oferta."') ";
		$queN=mysql_query($insN) or die(mysql_error());
		@mysql_free_result($queN);
		
		$ins="insert into oferte_transport_avion (select '".$id_oferta_nou."' as id_oferta, aeroport_plecare, aeroport_sosire, companie, ora_plecare, ora_sosire, pret, moneda, tip, ordonare from oferte_transport_avion where id_oferta = '".$id_oferta."') ";
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		$ins="insert into oferte_transport_autocar (select '".$id_oferta_nou."' as id_oferta, id_localitate, ora, detalii, pret, moneda, ordonare from oferte_transport_autocar where id_oferta = '".$id_oferta."') ";
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		$insN="insert into oferte_excursii (select '".$id_oferta_nou."' as id_oferta, id_excursie, tip from oferte_excursii where id_oferta = '".$id_oferta."') ";
		$queN=mysql_query($insN) or die(mysql_error());
		@mysql_free_result($queN);
		
		echo '<script> alert(\'Circuitul a fost copiat\'); document.location.href=\'editare_sejur.php?pas=2&oferta='.$id_oferta_nou.'\'; </script>';
	}
	
	function adaugare($param, $circuit) {
		$img=array();
		$error_poza=array();
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		$id_localitate=1;
		$distanta=$param['fata_de'].' '.$param['dist'];
		
		$ins="INSERT INTO hoteluri SET
		locatie_id = '".$id_localitate."',
		nume='".trim(inserare_frumos($param['denumire']))."',
		descriere='".inserare_frumos($param['descriere'])."',
		descriere_scurta='".inserare_frumos($param['descriere_scurta'])."',
		descriere_seo='".inserare_frumos($param['descriere_seo'])."',
		titlu_seo='".inserare_frumos($param['titlu_seo'])."',
		cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."',
		poza1 = '$img[1]',
		poza2 = '$img[2]',
		poza3 = '$img[3]',
		poza4 = '$img[4]',
		poza5 = '$img[5]',
		poza6 = '$img[6]',
		poza7 = '$img[7]',
		poza8 = '$img[8]',
		stele = '".$param['stele']."',
		latitudine = '".$param['latitudine']."',
		longitudine = '".$param['longitudine']."',
		adresa = '".inserare_frumos($param['adresa'])."',
		cod_postal = '".$param['cod_postal']."',
		email = '".inserare_frumos($param['email'])."',
		website = '".$param['website']."',
		tip_unitate = '".$param['tip_unitate']."',
		concept = '".$param['concept']."',
		distanta = '$distanta',
		general = '".inserare_frumos($param['general'])."',
		servicii = '".inserare_frumos($param['servicii'])."',
		internet = '".inserare_frumos($param['internet'])."',
		parcare = '".inserare_frumos($param['parcare'])."',
		check_in = '".$param['check_in']."',
		check_out = '".$param['check_out']."',
		anulare_plata = '".$param['anulare_plata']."',
		info_copii = '".inserare_frumos($param['info_copii'])."',
		accepta_animale = '".$param['accepta_animale']."',
		carduri = '".inserare_frumos($param['carduri'])."',
		nr = '".$param['nr']."',
		observatii = '".inserare_frumos($param['observatii'])."',
		garantam = '".inserare_frumos($param['garantam'])."',
		proprietar = '".$_SESSION['id_user_adm']."',
		data_adaugarii = SYSDATE(),
		telefon = '".$_POST['telefon']."',
		stare = '".$_POST['stare']."',
		observatii_stare = '".inserare_frumos($param['observatii_stare'])."',
		obiectice_turistice = '".inserare_frumos($param['obiectice_turistice'])."',
		id_continent = '".$param['id_continent']."'
		";
		$que=mysql_query($ins) or die(mysql_error());
		$id_hotel=mysql_insert_id();
		@mysql_free_result($que);
		
		if(sizeof($param['camere_hotel'])>'0') {
			foreach($param['camere_hotel'] as $key=>$value) {
				$insC="insert into camere_hotel set id_hotel = '".$id_hotel."', id_camera = '".$value."', ordine = '".$key."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		//traseu_circ
		if(sizeof($circuit['tara'])>0 && $param['tip_unitate']=='Circuit') {
			foreach($circuit['tara'] as $key_t=>$value_t) {
				$localitate=$circuit['localitate'][$key_t];
				$hotel=$circuit['hotel'][$key_t];
				$detalii=inserare_frumos($circuit['detalii'][$key_t]);
				$tara_principala=$circuit['tara_principala'][$key_t];
				$insC="insert into traseu_circuit set id_hotel_parinte = '".$id_hotel."', id_tara = '".$value_t."', id_localitate = '".$localitate."', id_hotel = '".$hotel."', detalii = '".$detalii."', ordine = '".$key_t."', tara_principala = '".$tara_principala."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		if(sizeof($error_poza)>0) { ?>
        <form name="pas_urm" method="post" action="editare_circuit.php?pas=2&circuit=<?php echo $id_hotel; ?>#poze" />
			<?php if(sizeof($error_poza)>0) {
			foreach($error_poza as $key_1=>$error1) { ?>
            <input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
			<?php }
			} ?>
			<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit(); </script>
        </form>
		<?php } else echo '<script>alert(\'Circuit-ul a fost adaugata!\n Adaugati preturile circuitului!\'); document.location.href=\'/adm/adauga_sejur.php?hotel='.$id_hotel.'\'; </script>';
	}
	
	function editeaza($param, $id_hotel, $url, $circuit) {
		$img=array();
		$error_poza=array();
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		if($param['poza1']) $img[1]=$param['poza1'];
		if($param['poza2']) $img[2]=$param['poza2'];
		if($param['poza3']) $img[3]=$param['poza3'];
		if($param['poza4']) $img[4]=$param['poza4'];
		if($param['poza5']) $img[5]=$param['poza5'];
		if($param['poza6']) $img[6]=$param['poza6'];
		if($param['poza7']) $img[7]=$param['poza7'];
		if($param['poza8']) $img[8]=$param['poza8'];
		$id_localitate=1;
		$distanta=$param['fata_de'].' '.$param['dist'];
		
		$ins="UPDATE hoteluri SET
		locatie_id = '".$id_localitate."',
		nume='".trim(inserare_frumos($param['denumire']))."',
		descriere='".inserare_frumos($param['descriere'])."',
		descriere_scurta='".inserare_frumos($param['descriere_scurta'])."',
		descriere_seo='".inserare_frumos($param['descriere_seo'])."',
		titlu_seo='".inserare_frumos($param['titlu_seo'])."',
		cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."',
		poza1 = '$img[1]',
		poza2 = '$img[2]',
		poza3 = '$img[3]',
		poza4 = '$img[4]',
		poza5 = '$img[5]',
		poza6 = '$img[6]',
		poza7 = '$img[7]',
		poza8 = '$img[8]',
		stele = '".$param['stele']."',
		latitudine = '".$param['latitudine']."',
		longitudine = '".$param['longitudine']."',
		adresa = '".inserare_frumos($param['adresa'])."',
		cod_postal = '".$param['cod_postal']."',
		email = '".inserare_frumos($param['email'])."',
		website = '".$param['website']."',
		tip_unitate = '".$param['tip_unitate']."',
		concept = '".$param['concept']."',
		distanta = '$distanta',
		general = '".inserare_frumos($param['general'])."',
		servicii = '".inserare_frumos($param['servicii'])."',
		internet = '".inserare_frumos($param['internet'])."',
		parcare = '".inserare_frumos($param['parcare'])."',
		check_in = '".$param['check_in']."',
		check_out = '".$param['check_out']."',
		anulare_plata = '".inserare_frumos($param['anulare_plata'])."',
		info_copii = '".inserare_frumos($param['info_copii'])."',
		accepta_animale = '".$param['accepta_animale']."',
		carduri = '".inserare_frumos($param['carduri'])."',
		nr = '".$param['nr']."',
		observatii = '".inserare_frumos($param['observatii'])."',
		garantam = '".inserare_frumos($param['garantam'])."',
		proprietar_modificare = '".$_SESSION['id_user_adm']."',
		ultima_modificare = SYSDATE(), telefon = '".$param['telefon']."',
		observatii_stare = '".inserare_frumos($param['observatii_stare'])."',
		stare = '".$_POST['stare']."',
		obiectice_turistice = '".inserare_frumos($param['obiectice_turistice'])."',
		id_continent = '".$param['id_continent']."',
		cod_remarketing = '".$param['cod_remarketing']."'
		WHERE id_hotel= '".$id_hotel."'
		";
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		$del="delete from camere_hotel where id_hotel = '".$id_hotel."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);
		
		if(sizeof($param['camere_hotel'])>'0') {
			foreach($param['camere_hotel'] as $key=>$value) {
				$insC="insert into camere_hotel set id_hotel = '".$id_hotel."', id_camera = '".$value."', ordine = '".$key."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		$del="delete from traseu_circuit where id_hotel_parinte = '".$id_hotel."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);
		
		if(sizeof($circuit['tara'])>0 && $param['tip_unitate']=='Circuit') {
			foreach($circuit['tara'] as $key_t=>$value_t) {
				$localitate=$circuit['localitate'][$key_t];
				$hotel=$circuit['hotel'][$key_t];
				$tara_principala=$circuit['tara_principala'][$key_t];
				$detalii=inserare_frumos($circuit['detalii'][$key_t]);
				
				$insC="insert into traseu_circuit set id_hotel_parinte = '".$id_hotel."', id_tara = '".$value_t."', id_localitate = '".$localitate."', id_hotel = '".$hotel."', detalii = '".$detalii."', ordine = '".$key_t."', tara_principala = '".$tara_principala."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		if(sizeof($error_poza)>0) { ?>
        <form name="pas_urm" method="post" action="editare_circuit.php?pas=2&circuit=<?php echo $id_hotel; ?>#poze" />
			<?php if(sizeof($error_poza)>0) {
			foreach($error_poza as $key_1=>$error1) { ?>
            <input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
			<?php }
			} ?>
			<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit();</script>
		</form>
		<?php } else echo '<script>alert(\'Datele au fost modificate!\'); document.location.href=\''.$url.'\'; </script>';
	}
	
	function select_camp_hotel($id_hotel) {
		$selT="SELECT
		hoteluri.locatie_id as localitate,
		hoteluri.nume as denumire,
		hoteluri.descriere,
		hoteluri.descriere_scurta,
		hoteluri.descriere_seo,
		hoteluri.titlu_seo,
		hoteluri.cuvinte_cheie_seo as cuvinte_seo,
		hoteluri.poza1,
		hoteluri.poza2,
		hoteluri.poza3,
		hoteluri.poza4,
		hoteluri.poza5,
		hoteluri.poza6,
		hoteluri.poza7,
		hoteluri.poza8,
		hoteluri.latitudine,
		hoteluri.longitudine,
		hoteluri.adresa,
		hoteluri.stele,
		hoteluri.cod_postal,
		hoteluri.email,
		hoteluri.website,
		hoteluri.tip_unitate,
		hoteluri.concept,
		hoteluri.distanta,
		hoteluri.general,
		hoteluri.servicii,
		hoteluri.internet,
		hoteluri.parcare,
		hoteluri.check_in,
		hoteluri.check_out,
		hoteluri.anulare_plata,
		hoteluri.info_copii,
		hoteluri.accepta_animale,
		hoteluri.carduri,
		hoteluri.nr,
		hoteluri.observatii,
		hoteluri.garantam,
		hoteluri.stare,
		hoteluri.telefon,
		hoteluri.observatii_stare,
		hoteluri.id_continent,
		hoteluri.obiectice_turistice,
		hoteluri.cod_remarketing,
		localitati.denumire as denumire_localitate,
		zone.id_zona as zona,
		zone.denumire as denumire_zona,
		tari.id_tara as tara,
		tari.denumire as denumire_tara,
		continente.nume_continent
		FROM hoteluri
		left join localitati on hoteluri.locatie_id = localitati.id_localitate
		left join zone on localitati.id_zona = zone.id_zona
		left join tari on zone.id_tara = tari.id_tara
		left join continente on hoteluri.id_continent = continente.id_continent
		WHERE hoteluri.id_hotel = '".$id_hotel."' ";
		$queT=mysql_query($selT) or die(mysql_error());
		$param=mysql_fetch_array($queT);
		@mysql_free_result($queT);
		return $param;
	}
	
	function select_traseu_circ($id_hotel) {
		$selT="select * from traseu_circuit where id_hotel_parinte = '".$id_hotel."' Group by ordine Order by ordine ";
		$queT=mysql_query($selT) or die(mysql_error());
		while($rowT=mysql_fetch_array($queT)) {
			$circuit['tara'][]=$rowT['id_tara'];
			$circuit['localitate'][]=$rowT['id_localitate'];
			$circuit['hotel'][]=$rowT['id_hotel'];
			$circuit['detalii'][]=$rowT['detalii'];
			$circuit['tara_principala'][]=$rowT['tara_principala'];
		} @mysql_free_result($queT);
		return $circuit;
	}
} ?>