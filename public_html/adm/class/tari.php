<?php class TARI {
	function adaugare($param) {
		$img=array();
		$error_poza=array();
		$steag='';
		$harta='';
		$error_steag='';
		$error_harta='';
		
		if(sizeof($param['steag'])>0) {
			if(sizeof($param['steag']['error'][1])>0) $error_steag=$param['steag']['error'][1];
			else $steag=$param['steag']['poza'][1];
		}
		
		if(sizeof($param['harta'])>0) {
			if(sizeof($param['harta']['error'][1])>0) $error_harta=$param['harta']['error'][1];
			else $harta=$param['harta']['poza'][1];
		}
		
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		
		$ins="insert into tari SET denumire='".trim(inserare_frumos($param['denumire']))."', descriere='".inserare_frumos($param['descriere'])."', descriere_scurta='".inserare_frumos($param['descriere_scurta'])."', info_scurte='".$param['info_scurte']."', descriere_seo='".inserare_frumos($param['descriere_seo'])."', titlu_seo='".inserare_frumos($param['titlu_seo'])."', cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."', id_continent = '".$param['continent']."', poza1 = '$img[1]', poza2 = '$img[2]', poza3 = '$img[3]', harta = '$harta', steag = '$steag', latitudine = '".$param['latitudine']."', longitudine = '".$param['longitudine']."', proprietar = '".$_SESSION['id_user_adm']."', data_adaugarii = SYSDATE() ";
		$que=mysql_query($ins) or die(mysql_error());
		$id_tara=mysql_insert_id();
		@mysql_free_result($que);
		$caracteristica=$param['caracteristica'];
		
		if(sizeof($caracteristica)>'0') {
			foreach($caracteristica as $key => $value) {
				if($value) {
					$carac=remove_special_characters($value);
					$ins_caract="insert into tari_caracteristici (id_tara, id_caracteristica, value) VALUES ('".$id_tara."', '$key', '$carac') ";
					$que_caract=mysql_query($ins_caract) or die(mysql_error());
					@mysql_free_result($que_caract);
				}
			}
		}
		
		if(sizeof($param['tip_oferta'])>'0') {
			foreach($param['tip_oferta'] as $key=>$value) {
				if($value) {
					$insE="insert into tari_tip_sejur set id_tara = '".$id_tara."', id_tip_oferta = '".$value."' ";
					$queE=mysql_query($insE) or die(mysql_error());
					@mysql_free_result($queE);
				}
			}
		}
		
		if($error_steag || $error_harta || sizeof($error_poza)>0) { ?>
        <form name="pas_urm" method="post" action="editare_tara.php?pas=2&tara=<?php echo $id_tara; ?>#poze" />
        <input type="hidden" name="error_steag" value="<?php echo $error_steag; ?>" />
        <input type="hidden" name="error_harta" value="<?php echo $error_harta; ?>" />
		<?php if(sizeof($error_poza)>0) {
			foreach($error_poza as $key_1=>$error1) { ?>
            <input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
			<?php }
		} ?>
		<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit(); </script>
        </form>
		<?php } else echo '<script>alert(\'Tara a fost adaugata!\'); document.location.href=\'editare_tara.php?pas=3&tara='.$id_tara.'\'; </script>';
	}
	
	function editeaza($param, $id_tara) {
		$img=array();
		$error_poza=array();
		$steag='';
		$harta='';
		$error_steag='';
		$error_harta='';
		
		if(sizeof($param['steag'])>0) {
			if(sizeof($param['steag']['error'][1])>0) $error_steag=$param['steag']['error'][1];
			else $steag=$param['steag']['poza'][1];
		}
		
		if(sizeof($param['harta'])>0) {
			if(sizeof($param['harta']['error'][1])>0) $error_harta=$param['harta']['error'][1];
			else $harta=$param['harta']['poza'][1];
		}
		
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		
		if($param['p_steag']) $steag=$param['p_steag'];
		if($param['p_harta']) $harta=$param['p_harta'];
		if($param['poza1']) $img[1]=$param['poza1'];
		if($param['poza2']) $img[2]=$param['poza2'];
		if($param['poza3']) $img[3]=$param['poza3'];
		
		$ins="update tari SET denumire='".inserare_frumos(trim($param['denumire']))."', descriere='".inserare_frumos($param['descriere'])."', descriere_scurta='".inserare_frumos($param['descriere_scurta'])."', info_scurte='".$param['info_scurte']."', descriere_seo='".inserare_frumos($param['descriere_seo'])."', titlu_seo='".inserare_frumos($param['titlu_seo'])."', cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."', id_continent = '".$param['continent']."', poza1 = '$img[1]', poza2 = '$img[2]', poza3 = '$img[3]', harta = '$harta', steag = '$steag', latitudine = '".$param['latitudine']."', longitudine = '".$param['longitudine']."', proprietar_modificare = '".$_SESSION['id_user_adm']."', ultima_modificare = SYSDATE() where id_tara = '".$id_tara."' ";
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		$del="delete from tari_caracteristici where id_tara = '".$id_tara."' ";
		$que_del=mysql_query($del) or die(mysql_error());
		@mysql_free_result($que_del);
		$caracteristica=$param['caracteristica'];
		
		if(sizeof($caracteristica)>'0') {
			foreach($caracteristica as $key => $value) {
				if($value) {
					$carac=remove_special_characters($value);
					$ins_caract="insert into tari_caracteristici (id_tara, id_caracteristica, value) VALUES ('".$id_tara."', '$key', '$carac') ";
					$que_caract=mysql_query($ins_caract) or die(mysql_error());
					@mysql_free_result($que_caract);
				}
			}
		}
		
		$del = "DELETE FROM clima WHERE id_tara = '".$id_tara."' AND id_zona = '0' AND id_localitate = '0' ";
		$que_del = mysql_query($del) or die(mysql_error());
		@mysql_free_result($que_del);
		
		if(sizeof($param['temp']['max'])>'0') {
			$ins_temp_max = '';
			foreach($param['temp']['max'] as $key=>$value) {
				$ins_temp_max .= $value.';';
			}
			$ins_temp_max = substr(str_replace(";;","",$ins_temp_max), 0, -1);
			
			$ins_temp_min = '';
			foreach($param['temp']['min'] as $key=>$value) {
				$ins_temp_min .= $value.';';
			}
			$ins_temp_min = substr(str_replace(";;","",$ins_temp_min), 0, -1);
			
			$ins_temp_apa = '';
			foreach($param['temp']['apa'] as $key=>$value) {
				$ins_temp_apa .= $value.';';
			}
			$ins_temp_apa = substr(str_replace(";;","",$ins_temp_apa), 0, -1);
			
			$insE = "INSERT INTO clima SET id_tara = '".$id_tara."', temp_max = '".$ins_temp_max."', temp_min = '".$ins_temp_min."', temp_apa = '".$ins_temp_apa."', link_vreme = '".$param['link_vreme']."' ";
			$queE = mysql_query($insE) or die(mysql_error());
			@mysql_free_result($queE);
		}
		
		$del="delete from tari_tip_sejur where id_tara = '".$id_tara."' ";
		$que_del=mysql_query($del) or die(mysql_error());
		@mysql_free_result($que_del);
		
		if(sizeof($param['tip_oferta'])>'0') {
			foreach($param['tip_oferta'] as $key=>$value) {
				if($value) {
					$insE="insert into tari_tip_sejur set id_tara = '".$id_tara."', id_tip_oferta = '".$value."' ";
					$queE=mysql_query($insE) or die(mysql_error());
					@mysql_free_result($queE);
				}
			}
		}
		
		if($error_steag || $error_harta || sizeof($error_poza)>0) { ?>
        <form name="pas_urm" method="post" action="editare_tara.php?pas=2&tara=<?php echo $id_tara; ?>#poze" />
        <input type="hidden" name="error_steag" value="<?php echo $error_steag; ?>" />
        <input type="hidden" name="error_harta" value="<?php echo $error_harta; ?>" />
		<?php if(sizeof($error_poza)>0) {
			foreach($error_poza as $key_1=>$error1) { ?>
            <input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
			<?php }
		} ?>
		<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit(); </script>
        </form>
		<?php } else echo '<script>alert(\'Datele au fost modificate!\'); document.location.href=\'editare_tara.php?pas=3&tara='.$id_tara.'\'; </script>';
	}
	
	function select_camp_tara($id_tara) {
		$selT="select denumire, descriere, descriere_scurta, info_scurte, descriere_seo, titlu_seo, cuvinte_cheie_seo as cuvinte_seo, id_continent as continent, poza1, poza2, poza3, harta, steag, latitudine, longitudine from tari where id_tara = '".$id_tara."' ";
		$queT=mysql_query($selT) or die(mysql_error());
		$param=mysql_fetch_array($queT);
		@mysql_free_result($queT);
		
		$selC="select * from tari_caracteristici where id_tara = '".$id_tara."' ";
		$queC=mysql_query($selC) or die(mysql_error());
		while($rowC=mysql_fetch_array($queC)) {
			$param['caracteristica'][$rowC['id_caracteristica']]=$rowC['value'];
		} @mysql_free_result($queC);
		
		$sel_temp = "SELECT * FROM clima WHERE id_tara = '".$id_tara."' AND id_zona = '0' AND id_localitate = '0' ";
		$que_temp = mysql_query($sel_temp) or die(mysql_error());
		$row_temp = mysql_fetch_array($que_temp);
		$temp_max = explode(";", $row_temp['temp_max']);
		$temp_min = explode(";", $row_temp['temp_min']);
		$temp_apa = explode(";", $row_temp['temp_apa']);
		for($i=0; $i<12; $i++) {
			$param['temp']['max'][$i] = $temp_max[$i];
			$param['temp']['min'][$i] = $temp_min[$i];
			$param['temp']['apa'][$i] = $temp_apa[$i];
		}
		$param['link_vreme'] = $row_temp['link_vreme'];
		@mysql_free_result($queC);
		
		return $param;
	}
} ?> 