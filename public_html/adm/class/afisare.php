<?php
class AFISARE {
	function __construct($link, $filtru) {
		$this->filtru=$filtru;
		$this->filtruF='';
		$this->filtruFS='';
		if(sizeof($this->filtru)>0) {
			foreach($this->filtru as $key=>$value) {
				$this->filtruF=$this->filtruF.'&filtru['.$key.']='.$value;
				$this->filtruFS=$this->filtruFS.'&f['.$key.']='.$value;
			}
		}
		$this->link=$link;
	}
	
	function set_from($nr_pe_pagina, $pagina_curenta) {
		$this->numar_articol_pagina=$nr_pe_pagina;
		$this->nr_pagina=$pagina_curenta;
	}
	
	function numar_total() {
		$this->sql=select($this->link,$this->filtru);
		$result=mysql_query($this->sql) or die(mysql_error());
		$numar_articole=mysql_num_rows($result);
		return $numar_articole;
	}
	
	function paginare($numar_total) {
		$this->nr_pagini=0;
		if($numar_total) {
			$this->nr_pagini=floor($numar_total/$this->numar_articol_pagina);
			
			if($numar_total%$this->numar_articol_pagina!=0) $this->nr_pagini++;
			
			if(($this->start*$this->numar_articol_pagina)>$numar_total) {
				$produse_afisate=($this->start+1)."-".($this->start+1)*$this->numar_articol_pagina;
			} else {
				$produse_afisate=($this->start+1)."-".$numar_total;
			}
			
			$begin_ct=1;
			$end_ct=$this->nr_pagini;
			$this->pagini='';
			if($this->nr_pagina>=9) {
				$begin_ct = $this->nr_pagina-5;
			}
			
			if($this->nr_pagina>1) $this->pagini.="<a href=\"#\" onClick=\"clic('1',".$this->numar_articol_pagina.",".$numar_total.",'".$this->link."','".$this->filtruF."');\">Start</a>&nbsp" ;
			else $this->pagini.="<span class=\"selNav\">Start</span>&nbsp;";
			
			$afis=$this->nr_pagina-1;
			if($this->nr_pagina>1) $this->pagini.="<a href=\"#\" onClick=\"clic(".$afis.",".$this->numar_articol_pagina.",".$numar_total.",'".$this->link."','".$this->filtruF."');\">Prev</a>&nbsp;";
			else $this->pagini.="<span class=\"selNav\">Prev</span>&nbsp;";
			
			if($this->nr_pagini>20) {
				if($this->nr_pagina<9) $end_ct=10;
				else $end_ct=$this->nr_pagina+5;
				
				if($end_ct>$this->nr_pagini) $end_ct=$this->nr_pagini;
			}
			
			for($i=$begin_ct;$i<=$end_ct;$i++) {
				if($i==$this->nr_pagina) {
					$this->pagini.="<span class=\"sel\">".$i."</span>&nbsp";
				} else {
					$this->pagini.="<a href=\"#\" onClick=\"clic(".$i.",".$this->numar_articol_pagina.",".$numar_total.",'".$this->link."','".$this->filtruF."');\">".$i."</a>&nbsp;";
				}
			}
			
			$afis=$this->nr_pagina+1;
			if($this->nr_pagina<$this->nr_pagini) $this->pagini.="<a href=\"#\" onClick=\"clic(".$afis.",".$this->numar_articol_pagina.",".$numar_total.",'".$this->link."','".$this->filtruF."');\">Next</a>&nbsp;";
			else $this->pagini.="<span class=\"selNav\">Next</span>&nbsp;";
			
			if($this->nr_pagina<$this->nr_pagini) $this->pagini.="<a href=\"#\" onClick=\"clic(".$this->nr_pagini.",".$this->numar_articol_pagina.",".$numar_total.",'".$this->link."','".$this->filtruF."');\">End</a>";
			else $this->pagini.="<span class=\"selNav\">End</span>&nbsp;";
		}
	}
	
	function afisare($numar_total) {
		$this->start=$this->numar_articol_pagina*($this->nr_pagina-1);
		$this->paginare($numar_total); ?>
        <div class="paginatie">
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="170" align="left" valign="middle">Afisare <?php $st=$this->numar_articol_pagina*($this->nr_pagina-1)+1; echo $st; $sf=$this->numar_articol_pagina*$this->nr_pagina; echo "-".$sf; ?> din <?php echo $numar_total; ?></td>
              <td align="center" valign="middle"><?php echo $this->pagini; ?></td>
              <td width="160" align="right" valign="middle">Pagina <?php echo $this->nr_pagina; ?> din <?php if($this->nr_pagini) echo $this->nr_pagini; else echo '1'; ?></td>
            </tr>
          </table>
        </div>
		
		<?php $this->sql=select($this->link,$this->filtru);
		$this->sql=$this->sql." LIMIT ".$this->start.", ".$this->numar_articol_pagina;
		$afisare_pr=mysql_query($this->sql) or die(mysql_error());
		include($_SERVER['DOCUMENT_ROOT'].$GLOBALS['path_adm'].$this->link.'/tabel_'.$this->link.'.php'); ?>
        <div class="paginatie">
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="170" align="left" valign="middle">Afisare <?php $st=$this->numar_articol_pagina*($this->nr_pagina-1)+1; echo $st; $sf=$this->numar_articol_pagina*$this->nr_pagina; echo "-".$sf; ?> din <?php echo $numar_total; ?></td>
              <td align="center" valign="middle"><?php echo $this->pagini; ?></td>
              <td width="160" align="right" valign="middle">Pagina <?php echo $this->nr_pagina; ?> din <?php if($this->nr_pagini) echo $this->nr_pagini; else echo '1'; ?></td>
            </tr>
          </table>
        </div>
		<script>
		function clic(pag, nr_pe_pag, nr_total, linku, sql) {
			$("#afisare").load("<?php echo $GLOBALS['path_adm']; ?>class/afis.php?nr_pagina="+pag+"&nr_pe_pag="+nr_pe_pag+"&nr_total="+nr_total+"&linku="+linku+sql);
		}
        </script>
	<?php }
}//end class ?>