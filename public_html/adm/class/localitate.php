<?php class LOCALITATE {
	function adaugare($param) {
		$img=array();
		$error_poza=array();
		$harta='';
		$error_harta='';
		
		if(sizeof($param['harta'])>0) {
			if(sizeof($param['harta']['error'][1])>0) $error_harta=$param['harta']['error'][1];
			else $harta=$param['harta']['poza'][1];
		}
		
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		
		$id_tara=$param['tara'];
		$id_zona=$param['zona'];
		if(!$apare_dreapta=$param['apare_dreapta']) $apare_dreapta='nu';
		
		$ins="insert into localitati SET id_zona = '".$id_zona."', denumire='".trim(inserare_frumos($param['denumire']))."', descriere='".$param['descriere']."', descriere_scurta='".$param['descriere_scurta']."', info_scurte='".$param['info_scurte']."', descriere_seo='".inserare_frumos($param['descriere_seo'])."', titlu_seo='".inserare_frumos($param['titlu_seo'])."', cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."', poza1 = '$img[1]', poza2 = '$img[2]', poza3 = '$img[3]', harta = '$harta', oras_plecare = '".$param['oras_plecare']."', port = '".$param['port']."', statiune = '".$param['statiune']."', latitudine = '".$param['latitudine']."', longitudine = '".$param['longitudine']."', id_judet = '".$param['id_judet']."', id_parinte = '".$param['parinte']."', aeroport = '".$param['aeroport']."', proprietar = '".$_SESSION['id_user_adm']."', data_adaugarii = SYSDATE(), apare_dreapta = '".$apare_dreapta."' ";
		$que=mysql_query($ins) or die(mysql_error());
		$id_localitate=mysql_insert_id();
		@mysql_free_result($que);
		$caracteristica=$param['caracteristica'];
		
		if(sizeof($caracteristica)>'0') {
			foreach($caracteristica as $key => $value) {
				if($value) {
					$carac=remove_special_characters($value);
					$ins_caract="insert into localitati_caracteristici (id_localitate, id_caracteristica, value) VALUES ('".$id_localitate."', '$key', '$carac') ";
					$que_caract=mysql_query($ins_caract) or die(mysql_error());
					@mysql_free_result($que_caract);
				}
			}
		}
		
		if($error_steag || $error_harta || sizeof($error_poza)>0) { ?>
        <form name="pas_urm" method="post" action="editare_localitate.php?pas=2&localitate=<?php echo $id_localitate; ?>#poze" />
        <input type="hidden" name="error_harta" value="<?php echo $error_harta; ?>" />
		<?php if(sizeof($error_poza)>0) {
			foreach($error_poza as $key_1=>$error1) { ?>
            <input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
			<?php }
		} ?>
		<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit(); </script>
        </form>
		<?php } else echo '<script>alert(\'Localitatea a fost adaugata!\'); document.location.href=\'editare_localitate.php?pas=3&localitate='.$id_localitate.'\'; </script>';
	}
	
	function editeaza($param, $id_localitate) {
		$img=array();
		$error_poza=array();
		$harta='';
		$error_steag='';
		$error_harta='';
		
		if(sizeof($param['harta'])>0) {
			if(sizeof($param['harta']['error'][1])>0) $error_harta=$param['harta']['error'][1];
			else $harta=$param['harta']['poza'][1];
		}
		
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		
		if($param['p_harta']) $harta=$param['p_harta'];
		if($param['poza1']) $img[1]=$param['poza1'];
		if($param['poza2']) $img[2]=$param['poza2'];
		if($param['poza3']) $img[3]=$param['poza3'];
		$id_tara=$param['tara'];
		$id_zona=$param['zona'];
		if(!$apare_dreapta=$param['apare_dreapta']) $apare_dreapta='nu';
		
		$ins="update localitati SET id_zona = '".$id_zona."', denumire='".trim(inserare_frumos($param['denumire']))."', descriere='".$param['descriere']."', descriere_scurta='".inserare_frumos($param['descriere_scurta'])."', info_scurte='".$param['info_scurte']."', descriere_seo='".inserare_frumos($param['descriere_seo'])."', titlu_seo='".inserare_frumos($param['titlu_seo'])."', cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."', poza1 = '$img[1]', poza2 = '$img[2]', poza3 = '$img[3]', harta = '$harta', oras_plecare = '".$param['oras_plecare']."', port = '".$param['port']."', statiune = '".$param['statiune']."', latitudine = '".$param['latitudine']."', longitudine = '".$param['longitudine']."', id_parinte = '".$param['parinte']."', aeroport = '".$param['aeroport']."', proprietar_modificare = '".$_SESSION['id_user_adm']."', ultima_modificare = SYSDATE(), apare_dreapta = '".$apare_dreapta."' where id_localitate = '".$id_localitate."' ";
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		$del="delete from localitati_caracteristici where id_localitate = '".$id_localitate."' ";
		$que_del=mysql_query($del) or die(mysql_error());
		@mysql_free_result($que_del);
		$caracteristica=$param['caracteristica'];
		
		if(sizeof($caracteristica)>'0') {
			foreach($caracteristica as $key => $value) {
				if($value) {
					$carac=remove_special_characters($value);
					$ins_caract="insert into localitati_caracteristici (id_localitate, id_caracteristica, value) VALUES ('".$id_localitate."', '$key', '$carac') ";
					$que_caract=mysql_query($ins_caract) or die(mysql_error());
					@mysql_free_result($que_caract);
				}
			}
		}
		
		$del = "DELETE FROM clima WHERE id_tara = '".$id_tara."' AND id_zona = '".$id_zona."' AND id_localitate = '".$id_localitate."' ";
		$que_del = mysql_query($del) or die(mysql_error());
		@mysql_free_result($que_del);
		
		if(sizeof($param['temp']['max'])>'0') {
			$ins_temp_max = '';
			foreach($param['temp']['max'] as $key=>$value) {
				$ins_temp_max .= $value.';';
			}
			$ins_temp_max = substr($ins_temp_max, 0, -1);
			
			$ins_temp_min = '';
			foreach($param['temp']['min'] as $key=>$value) {
				$ins_temp_min .= $value.';';
			}
			$ins_temp_min = substr($ins_temp_min, 0, -1);
			
			$ins_temp_apa = '';
			foreach($param['temp']['apa'] as $key=>$value) {
				$ins_temp_apa .= $value.';';
			}
			$ins_temp_apa = substr($ins_temp_apa, 0, -1);
			
			$insE = "INSERT INTO clima SET id_tara = '".$id_tara."', id_zona = '".$id_zona."', id_localitate = '".$id_localitate."', temp_max = '".$ins_temp_max."', temp_min = '".$ins_temp_min."', temp_apa = '".$ins_temp_apa."', link_vreme = '".$param['link_vreme']."' ";
			$queE = mysql_query($insE) or die(mysql_error());
			@mysql_free_result($queE);
		}
		
		if($error_steag || $error_harta || sizeof($error_poza)>0) { ?>
        <form name="pas_urm" method="post" action="editare_localitate.php?pas=2&localitate=<?php echo $id_localitate; ?>#poze" />
        <input type="hidden" name="error_harta" value="<?php echo $error_harta; ?>" />
		<?php if(sizeof($error_poza)>0) {
			foreach($error_poza as $key_1=>$error1) { ?>
            <input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
			<?php }
		} ?>
		<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit(); </script>
        </form>
		<?php } else echo '<script>alert(\'Datele au fost modificate!\'); document.location.href=\'editare_localitate.php?pas=3&localitate='.$id_localitate.'\'; </script>';
	}
	
	function select_camp_localitate($id_localitate) {
		$selT="select localitati.id_zona as zona, localitati.id_judet,  localitati.denumire, localitati.descriere, localitati.descriere_scurta, localitati.info_scurte, localitati.descriere_seo, localitati.titlu_seo, localitati.cuvinte_cheie_seo as cuvinte_seo, localitati.poza1, localitati.poza2, localitati.poza3, localitati.harta, localitati.latitudine, localitati.longitudine, localitati.id_parinte as parinte, localitati.aeroport, localitati.oras_plecare, localitati.port, localitati.statiune, localitati.apare_dreapta, tari.denumire as denumire_tara, zone.denumire as denumire_zona, tari.id_tara as tara from localitati left join zone on localitati.id_zona = zone.id_zona left join tari on zone.id_tara = tari.id_tara where localitati.id_localitate = '".$id_localitate."' ";
		$queT=mysql_query($selT) or die(mysql_error());
		$param=mysql_fetch_array($queT);
		@mysql_free_result($queT);
		
		$selC="select * from localitati_caracteristici where id_localitate = '".$id_localitate."' ";
		$queC=mysql_query($selC) or die(mysql_error());
		while($rowC=mysql_fetch_array($queC)) {
			$param['caracteristica'][$rowC['id_caracteristica']]=$rowC['value'];
		} @mysql_free_result($queC);
				
		$sel_temp = "SELECT * FROM clima WHERE id_tara = '".$param['tara']."' AND id_zona = '".$param['zona']."' AND id_localitate = '".$id_localitate."' ";
		$que_temp = mysql_query($sel_temp) or die(mysql_error());
		$row_temp = mysql_fetch_array($que_temp);
		$temp_max = explode(";", $row_temp['temp_max']);
		$temp_min = explode(";", $row_temp['temp_min']);
		$temp_apa = explode(";", $row_temp['temp_apa']);
		for($i=0; $i<12; $i++) {
			$param['temp']['max'][$i] = $temp_max[$i];
			$param['temp']['min'][$i] = $temp_min[$i];
			$param['temp']['apa'][$i] = $temp_apa[$i];
		}
		$param['link_vreme'] = $row_temp['link_vreme'];
		@mysql_free_result($queC);
		
		return $param;
	}
} ?> 