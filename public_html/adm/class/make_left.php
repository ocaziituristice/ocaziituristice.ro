<?php class ACCES {
  function __construct($acces) {
	$this->acces=$acces;  
  }
  function mk_left() {
  
  $left='<div class="titlu">Tools</div>
   <ul>
    <li><a href="/adm/home.php" title="Pagina de inceput"><strong>Dashboard</strong></a></li>';
	if(eregi('hotel', $this->acces)) $left=$left.'<li><a href="/adm/cron_list.php" title="Pagina de cronuri"><strong>Cron list</strong></a></li>';
	if(eregi('creare_cont', $this->acces)) $left=$left.'<li><a href="'.$sitepath_adm.'creare_cont.php" title="Creeaza un cont nou">Creaza cont</a></li>';
	if(eregi('hotel', $this->acces)) $left=$left.'<li><a href="'.$sitepath_adm.'suplimente.php" title="Tip suplimente">Tip suplimente</a></li>';
    if(eregi('erori_disponibilitate', $this->acces)) $left=$left.'<li><a href="'.$sitepath_adm.'erori_disponibilitate.php" title="Disponibilitati care nu au intors rezultate">Erori disponibilitate</a></li>';
    $left=$left.'<li><a href="'.$sitepath_adm.'adauga_articol.php" title="Adauga un Articol nou" class="add">+</a><a href="'.$sitepath_adm.'editare_articol.php" title="Editare Articole">Editare Articole</a></li>
	</ul>';
	if(eregi('localizare', $this->acces)) $left=$left.'<div class="titlu">Localizare</div>
    <ul>
      <!--<li><a href="'.$sitepath_adm.'capitole.php" title="Administreaza capitolele">Capitole</a></li>-->
      <li><a href="'.$sitepath_adm.'adauga_tara.php" title="Adauga o Tara noua" class="add">+</a><a href="'.$sitepath_adm.'editare_tara.php" title="Editare Tari">Editare Tari</a></li>
      <li><a href="'.$sitepath_adm.'adauga_zona.php" title="Adauga o Zona noua" class="add">+</a><a href="'.$sitepath_adm.'editare_zona.php" title="Editare Zone">Editare Zone</a></li>
      <li><a href="'.$sitepath_adm.'adauga_localitate.php" title="Adauga o Localitate noua" class="add">+</a><a href="'.$sitepath_adm.'editare_localitate.php" title="Editare Localitati">Editare Localitati</a></li>
	  <li><a href="'.$sitepath_adm.'adauga_aeroport.php" title="Adauga Aeroport" class="add">+</a><a href="'.$sitepath_adm.'editare_aeroport.php" title="Editare Aeroporturi">Editare Aeroport</a></li>
	</ul>';
	if(eregi('hotel', $this->acces)) $left=$left.'<div class="titlu">Oferte</div>
	<ul>
	  <li><a href="'.$sitepath_adm.'adauga_hotel.php" title="Adauga un Hotel nou" class="add">+</a><a href="'.$sitepath_adm.'editare_hotel.php" title="Editare Hoteluri">Editare Hoteluri</a></li>
	  <li><a href="'.$sitepath_adm.'editare_sejur.php" title="Afiseaza toate sejururile">Lista Sejururi</a></li>
	  <li><a href="'.$sitepath_adm.'oferte_speciale.php" title="Afiseaza Ofertele Speciale">Oferte Speciale</a></li>
	  <li><a href="'.$sitepath_adm.'adauga_circuit.php" title="Adauga circuit nou" class="add">+</a><a href="'.$sitepath_adm.'editare_circuit.php" title="Editare Circuite">Editare Circuite</a></li>
	  <li><a href="'.$sitepath_adm.'adauga_excursie.php" title="Adauga Excursie" class="add">+</a><a href="'.$sitepath_adm.'editare_excursie.php" title="Editare Excursii">Editare Excursii</a></li>
	</ul>';
	if(eregi('rezervari', $this->acces)) $left=$left.'<div class="titlu">Rezervari</div>
	<ul>
	  <li><a href="'.$sitepath_adm.'cereri.php" title="Editare Cereri">Cereri</a></li>
	  <li><a href="'.$sitepath_adm.'vouchere.php" title="Editare Vouchere">Editare Vouchere</a></li>
	  <!--<li><a href="'.$sitepath_adm.'oferte_generare_email.php" title="Genereaza text model pentru o oferta">Oferte - Generare Email</a></li>-->
	  <li><a href="'.$sitepath_adm.'useri.php" title="Editare Clienti">Editare Clienti</a></li>
	    <li><a href="'.$sitepath_adm.'newsman_sincronizare.php" title="Newsman sincronizare">Newsman Sincronizare</a></li>
	</ul>
	<div class="titlu">Cupoane</div>
	<ul>
	  <li><a href="'.$sitepath_adm.'editare_cupoane.php" title="Editare Cupoane">Editare Cupoane</a></li>
	  <li><a href="'.$sitepath_adm.'adauga_cupon_campanie.php" title="Adauga o Campanie noua" class="add">+</a><a href="'.$sitepath_adm.'editare_cupoane_campanii.php" title="Editare Cupoane Campanii">Editare Campanii</a></li>
	</ul>';
	if(eregi('furnizori', $this->acces)) $left=$left.'<div class="titlu">Furnizori</div>
	<ul>
	  <li><a href="'.$sitepath_adm.'adauga_furnizor.php" title="Adauga un Furnizor nou" class="add">+</a><a href="'.$sitepath_adm.'editare_furnizor.php" title="Editare Furnizori">Editare Furnizori</a></li>
	  <li><a href="'.$sitepath_adm.'plati_furnizori.php" title="Plati Furnizori">Plati</a></li>
	</ul>';
	if(eregi('financiar', $this->acces)) $left=$left.'<div class="titlu">Financiar</div>
	<ul>
	  <li><a href="'.$sitepath_adm.'adauga_tranzactie.php" title="Adauga o tranzactie noua" class="add">+</a><a href="'.$sitepath_adm.'editare_tranzactii.php" title="Editare Tranzactii">Editare Tranzactii</a></li>
	  <li><a href="'.$sitepath_adm.'raport_vanzare.php" title="Raport Vanzare">Raport_vanzare</a></li>
	    <li><a href="'.$sitepath_adm.'raport_plati_furnizor.php" title="Raport Vanzare">Raport plati furnizor</a></li>
	  
	  
	</ul>';
	if(eregi('furnizori', $this->acces)) $left=$left.'<div class="titlu">Importuri</div>
	<ul>
	  <li class="title">ROMANIA:</li>
	  <li><a href="'.$sitepath_adm.'import_accent.php?id_furnizor=15" title="Import Accent Travel">Accent Travel</a></li>
	   <li><a href="'.$sitepath_adm.'import_eximtur.php?id_furnizor=471" title="Import Exim Tur">Eximtur</a></li>
	  <li><a href="'.$sitepath_adm.'import_bibi_touring.php?id_furnizor=398" title="Import BIBI TOURING">BIBI Touring</a></li>
	  <li><a href="'.$sitepath_adm.'import_paradis.php?id_furnizor=330" title="Import Paradis Vacante de vis">Paradis Vacante</a></li>
	  <li><a href="'.$sitepath_adm.'import_transilvania.php?id_furnizor=218" title="Import Transilvania Travel">Transilvania</a></li>
	  <li class="title">EXTERN:</li>
	  <li><a href="'.$sitepath_adm.'import_aerotravel.php?id_furnizor=397" title="Import Aerotravel">Aerotravel</a></li>
	  <li><a href="'.$sitepath_adm.'import_christian.php?id_furnizor=6" title="Import Christian Tour">Christian Tour</a></li>
	  <li><a href="'.$sitepath_adm.'import_solvex.php?id_furnizor=32" title="Import Solvex Bulgaria">Solvex</a></li>
	  <li><a href="'.$sitepath_adm.'import_cocktails.php?id_furnizor=22" title="Import Cocktail Holidays">Cocktail Holidays</a></li>
	  <li><a href="'.$sitepath_adm.'import_fibula.php?id_furnizor=474" title="Import Fibula">Fibula</a></li>
	  <li><a href="'.$sitepath_adm.'import_laguna.php?id_furnizor=486" title="Import Laguna Tour">Laguna Tour</a></li>
	  <li><a href="'.$sitepath_adm.'import_malta.php?id_furnizor=52" title="Import Malta Travel">Malta Travel</a></li>
	  <li><a href="'.$sitepath_adm.'import_melitours.php?id_furnizor=216" title="Import Melitours">Melitours</a></li>
	  <li><a href="'.$sitepath_adm.'import_mouzenidis.php?id_furnizor=491" title="Import Mouzenidis">Mouzenidis</a></li>
	  <li><a href="'.$sitepath_adm.'import_nova.php?id_furnizor=463" title="Import Nova Travel (TUI Travel Center)">Nova Travel (TUI)</a></li>
	  <li><a href="'.$sitepath_adm.'import_paralela45.php?id_furnizor=2" title="Import Paralela 45">Paralela 45</a></li>
	  <li><a href="'.$sitepath_adm.'import_peninsula.php?id_furnizor=490" title="Import Peninsula (Adagio Tour)">Peninsula (Adagio Tour)</a></li>
	  <li><a href="'.$sitepath_adm.'import_trip_corporation.php?id_furnizor=493" title="Import Trip Corporation">Trip Corporation</a></li>
	  <li><a href="'.$sitepath_adm.'import_prestige.php?id_furnizor=225" title="Import Prestige Tours">Prestige</a></li>
	  <li><a href="'.$sitepath_adm.'import_romadria.php?id_furnizor=480" title="Import Romadria">Romadria</a></li>
	  <li><a href="'.$sitepath_adm.'import_tez.php?id_furnizor=1" title="Import TezTour">TezTour</a></li>
	  <li><a href="'.$sitepath_adm.'import_holiday_office.php?id_furnizor=509" title="Holiday Office"> Holiday Office</a></li>
	  <li><a href="'.$sitepath_adm.'import_karpaten.php?id_furnizor=349" title="Karpaten"> Karpaten</a></li>
	  	  <li><a href="'.$sitepath_adm.'import_rezeda.php?id_furnizor=562" title="Rezeda">Rezeda</a></li>
		  
		  	  <li><a href="'.$sitepath_adm.'import_calypso_prestige.php?id_furnizor=527" title="Calypso Prestige">Calypso Prestige</a></li>
  
		  
	  <li><a href="'.$sitepath_adm.'import_goglobal.php?id_furnizor=226" title="Go Global"> Go Global</a></li>
	   <li><a href="'.$sitepath_adm.'import_filos.php?id_furnizor=557" title="Filos Grecia"> Filos Grecia</a></li>
	     <li><a href="'.$sitepath_adm.'import_zeus.php?id_furnizor=563" title="Zeus Grecia"> Zeus Grecia</a></li>
	</ul>';
	  /*<li class="title">TEZ TOUR:</li>
	  <li><a href="'.$sitepath_adm.'import_teztour.php" title="Import Teztour">Import Teztour</a></li>
	  <li><a href="'.$sitepath_adm.'import_teztour.php?pas=3" title="Adauga preturi spo">Adauga preturi spo</a></li>
	  <li><a href="'.$sitepath_adm.'/adm/preturi_teztour.php" title="Adauga preturi spo">Importa preturi</a></li>
	  <li><a href="/cron/autoupdate_preturi_teztour.php" onclick="return confirm(\'Incepe updatarea preturilor de la ofertele active Tez Tour.\nATENTIE, DUREAZA!!!\nEsti sigur da?\')" title="" target="_blank">Autoupdate preturi oferte</a></li>*/
	
	/*if($_SESSION['mail']=='daniel@ocaziituristice.ro') {
		$left = '<div class="titlu">Import Preturi</div>
		<ul>
		  <li><a href="'.$sitepath_adm.'import_cocktails.php?id_furnizor=22" title="Import Hoteluri Cocktail Holidays">Hoteluri Cocktail Holidays</a></li>
		</ul>';
	}*/
	
	return $left;
	}
}
$CL_acces= new ACCES($acces[$_SESSION['tip']]);
?>