<?php class SEJUR {
function copieza( $id_oferta, $param, $hotel, $tip ) {
	if ( $tip == 'last' ) {
		$last_minute = 'da';
		$parinte = $id_oferta;
	} else {
		$parinte = '';
		$last_minute = 'nu';
	}
	$ins = 'INSERT INTO oferte
	(SELECT null AS id_oferta,
		valabila,
		"' . $param['denumire'] . '" AS denumire,
		denumire_scurta,
		exprimare_pret,
		SYSDATE() AS start_date,
		end_date,
		"' . $param['id_hotel'] . '" AS id_hotel,
		nr_zile,
		nr_nopti,
		id_transport,
		descriere_oferta,
		null AS ultima_modificare,
		null AS proprietar_modificare,
		SYSDATE() AS data_adaugarii,
		nota,
		observatii,
		"' . $_SESSION['id_user_adm'] . '" AS proprietar,
		masa, excursii,
		furnizor,
		observatii_furnizor,
		conditii_plata,
		conditii_anulare,
		pret_minim,
		moneda,
		click,
		pret_minim_lei,
		cazare,
		comision_fix,
		moneda_comision,
		procent_comision,
		obsercatii_comision,
		tip_preturi, "' . $last_minute . '" AS last_minute,
		"' . $parinte . '" AS id_parinte,
		inactiva,
		recomandata,
		detalii_recomandata,
		oferta_parinte_early,
		id_oferta_pivot,
		nr_formula_pivot,
		calculeaza_pret,
		id_spo,
		taxa_aeroport,
		apare_grad,
		rezervare_online,
		fisier_upload,
		online_prices,
		oferta_saptamanii,
		oferta_speciala
		FROM oferte
		WHERE id_oferta = "' . $id_oferta . '"
	)';
	$que = mysql_query( $ins ) or die( mysql_error() );
	$id_oferta_nou = mysql_insert_id();
	@mysql_free_result( $que );

	$insE = "INSERT INTO early_booking (SELECT '" . $id_oferta_nou . "' AS id_oferta, tip, end_date, discount, early_inclus, text_early FROM early_booking WHERE id_oferta = '" . $id_oferta . "' AND tip = 'sejur') ";
	$queE = mysql_query( $insE ) or die( mysql_error() );
	@mysql_free_result( $queE );

	$insE = "INSERT INTO oferta_sejur_tip (SELECT '" . $id_oferta_nou . "' AS id_oferta, id_tip_oferta FROM oferta_sejur_tip WHERE id_oferta = '" . $id_oferta . "') ";
	$queE = mysql_query( $insE ) or die( mysql_error() );
	@mysql_free_result( $queE );

	$insS = "INSERT INTO oferte_servicii (SELECT '" . $id_oferta_nou . "' AS id_oferta, denumire, value, data_start, data_end, moneda, semn, tip_serv, tip_plata, obligatoriu, tip_supliment, ordonare FROM oferte_servicii WHERE id_oferta = '" . $id_oferta . "') ";
	$queS = mysql_query( $insS ) or die( mysql_error() );
	@mysql_free_result( $queS );
	
	if ( $hotel == $param['id_hotel'] ) {
		$insPr = "INSERT INTO data_pret_oferta (SELECT '', '" . $id_oferta_nou . "' AS id_oferta, data_start, data_end, tip_camera, pret, moneda, id_hotel, of_logictour, pret_pivot, id_masa FROM data_pret_oferta WHERE id_oferta = '" . $id_oferta . "')";
		$quePr = mysql_query( $insPr ) or die( mysql_error() );
		@mysql_free_result( $quePr );
	} else {
		$insPr = "INSERT INTO data_pret_oferta (SELECT '', '" . $id_oferta_nou . "' AS id_oferta, data_start, data_end, null AS tip_camera, '1' AS pret, 'EURO' AS moneda, id_hotel, of_logictour, pret_pivot, id_masa FROM data_pret_oferta WHERE id_oferta = '" . $id_oferta . "')";
		$quePr = mysql_query( $insPr ) or die( mysql_error() );
		@mysql_free_result( $quePr );
	}
	
	$insN = "INSERT INTO reduceri_speciale (SELECT '" . $id_oferta_nou . "' AS id_oferta, data_start, data_end, zile_deaplicare, zile_aplicare, ordonare FROM reduceri_speciale WHERE id_oferta = '" . $id_oferta . "') ";
	$queN = mysql_query( $insN ) or die( mysql_error() );
	@mysql_free_result( $queN );

	$insN = "INSERT INTO nr_nopti_minim (SELECT '" . $id_oferta_nou . "' AS id_oferta, data_start, data_end, nr_min, ordonare FROM nr_nopti_minim WHERE id_oferta = '" . $id_oferta . "') ";
	$queN = mysql_query( $insN ) or die( mysql_error() );
	@mysql_free_result( $queN );

	$insN = "INSERT INTO zile_anulare (SELECT '" . $id_oferta_nou . "' AS id_oferta, data_start, data_end, zile_anulare, ordonare FROM zile_anulare WHERE id_oferta = '" . $id_oferta . "') ";
	$queN = mysql_query( $insN ) or die( mysql_error() );
	@mysql_free_result( $queN );

	$insN = "INSERT INTO zile_plata (SELECT '" . $id_oferta_nou . "' AS id_oferta, data_start, data_end, zile_plata, ordonare FROM zile_plata WHERE id_oferta = '" . $id_oferta . "') ";
	$queN = mysql_query( $insN ) or die( mysql_error() );
	@mysql_free_result( $queN );

	$insN = "INSERT INTO sold_out (SELECT '" . $id_oferta_nou . "' AS id_oferta, data_start, data_end, camera, ordonare FROM sold_out WHERE id_oferta = '" . $id_oferta . "') ";
	$queN = mysql_query( $insN ) or die( mysql_error() );
	@mysql_free_result( $queN );

	$ins = "insert into oferte_transport_avion (select '" . $id_oferta_nou . "' as id_oferta, aeroport_plecare, aeroport_sosire, companie, ora_plecare, ora_sosire, pret, moneda, tip, ordonare from oferte_transport_avion where id_oferta = '" . $id_oferta . "') ";
	$que = mysql_query( $ins ) or die( mysql_error() );
	@mysql_free_result( $que );

	$ins = "insert into oferte_transport_autocar (select '" . $id_oferta_nou . "' as id_oferta, id_localitate, ora, detalii, pret, moneda, ordonare from oferte_transport_autocar where id_oferta = '" . $id_oferta . "') ";
	$que = mysql_query( $ins ) or die( mysql_error() );
	@mysql_free_result( $que );

	$insN = "insert into oferte_excursii (select '" . $id_oferta_nou . "' as id_oferta, id_excursie, tip from oferte_excursii where id_oferta = '" . $id_oferta . "') ";
	$queN = mysql_query( $insN ) or die( mysql_error() );
	@mysql_free_result( $queN );

	echo '<script> alert(\'Sejurul a fost copiat\'); document.location.href=\'editare_sejur.php?pas=2&oferta=' . $id_oferta_nou . '\'; </script>';
}




function adaugare( $param, $preturi, $aeroport, $autocar, $tip ) {
	if ( $param['cazare'] ) $cazare = 'da'; else $cazare = 'nu';
	if ( $param['inactiva'] ) $inactiva = 'da'; else $inactiva = 'nu';
	if ( $param['recomandata'] ) $recomandata = 'da'; else $recomandata = 'nu';
	if ( !$calculeaza_pret = $param['calculeaza_pret'] ) $calculeaza_pret = 'nu';
	if ( !$apare_grad = $param['apare_grad'] ) $apare_grad = 'nu';
	if ( !$rezervare_online = $param['rezervare_online'] ) $rezervare_online = 'nu';
	if ( !$taxa_aeroport = $param['taxa_aeroport'] ) $taxa_aeroport = 'nu';

	$ins = 'INSERT INTO oferte SET
		id_hotel = "' . $param['id_hotel'] . '",
		denumire = "' . str_replace('  ',' ', trim( inserare_frumos( str_replace('&',' and ',$param["denumire"]) ) ) ) . '",
		nr_zile = "' . $param['nr_zile'] . '",
		nr_nopti = "' . $param['nr_nopti'] . '",
		id_transport = "' . $param['id_transport'] . '",
		descriere_oferta = "' . inserare_frumos( $param["descriere_oferta"] ) . '",
		data_adaugarii = SYSDATE(),
		nota = "' . inserare_frumos( $param["nota"] ) . '",
		proprietar = "' . $_SESSION['id_user_adm'] . '",
		masa = "' . $param['masa'] . '",
		exprimare_pret = "' . $param['exprimare_pret'] . '",
		furnizor = "' . $param['furnizor'] . '",
		observatii_furnizor = "' . inserare_frumos( $param['observatii_furnizor'] ) . '",
		conditii_plata="'.$param['conditii_plata'].'",
		conditii_anulare="'.$param['conditii_anulare'].'",
		cazare = "' . $cazare . '",
		comision_fix = "' . $param['comision_fix'] . '",
		moneda_comision = "' . $param['moneda_comision'] . '",
		procent_comision = "' . $param['procent_comision'] . '",
		obsercatii_comision = "' . inserare_frumos( $param['obsercatii_comision'] ) . '",
		tip_preturi = "' . $param['tip_preturi'] . '",
		last_minute = "' . $param['last_minute'] . '",
		inactiva = "' . $inactiva . '",
		recomandata = "' . $recomandata . '",
		denumire_scurta = "' . str_replace('  ',' ', trim( inserare_frumos( str_replace('&',' and ',$param['denumire_scurta']) ) ) ) . '",
		detalii_recomandata = "' . trim( inserare_frumos( $param['detalii_recomandata'] ) ) . '",
		oferta_parinte_early = "' . $param['oferta_parinte_early'] . '",
		calculeaza_pret = "' . $calculeaza_pret . '",
		taxa_aeroport = "' . $taxa_aeroport . '",
		apare_grad = "' . $apare_grad . '",
		rezervare_online = "'.$rezervare_online.'"
	';
	$que = mysql_query( $ins ) or die( mysql_error() );
	$id_oferta = mysql_insert_id();
	@mysql_free_result( $que );

	$end_early_per = '0001-01-01';

	if ( sizeof( $param['early_time'] ) > '0' ) {
		foreach( $param['early_time'] as $key => $value ) {
			$incl = $param['early_inclus'][$key];
			if ( $incl == 'da' ) {
				if ( $value > $end_early_per ) $end_early_per = $value;
			}
			$insE = "insert into early_booking set id_oferta = '" . $id_oferta . "', tip = 'sejur', end_date = '" . $value . "', discount = '" . $param['early_disc'][$key] . "', early_inclus = '" . $param['early_inclus'][$key] . "', text_early = '" . $param['text_early'][$key] . "' ";
			$queE = mysql_query( $insE ) or die( mysql_error() );
			@mysql_free_result( $queE );
		}
	}

	if ( sizeof( $param['evenimente'] ) > '0' ) {
		foreach( $param['evenimente'] as $key => $value ) {
			if ( $value ) {
				$insE = "insert into oferta_sejur_tip set id_oferta = '" . $id_oferta . "', id_tip_oferta = '" . $value . "' ";
				$queE = mysql_query( $insE ) or die( mysql_error() );
				@mysql_free_result( $queE );
			}
		}
	}

	if ( $param['tematici'] > 0 ) {
		$insE = "insert into oferta_sejur_tip set id_oferta = '" . $id_oferta . "', id_tip_oferta = '" . $param['tematici'] . "' ";
		$queE = mysql_query( $insE ) or die( mysql_error() );
		@mysql_free_result( $queE );
	}

	if ( $param['programe'] > 0 ) {
		$insE = "insert into oferta_sejur_tip set id_oferta = '" . $id_oferta . "', id_tip_oferta = '" . $param['programe'] . "' ";
		$queE = mysql_query( $insE ) or die( mysql_error() );
		@mysql_free_result( $queE );
	}

	if ( sizeof( $param['denumire_v1'] ) > '0' ) {
		foreach( $param['denumire_v1'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v1'];
				$moneda = $param['moneda_v1'];
				$tip_serv = 'Servicii incluse';
				if ( $param['obligatoriu_v1'] ) $obligatoriu = $param['obligatoriu_v1'];
				else $obligatoriu = 'nu';
				$tip_supliment = $param['tip_supliment_v1'];
				
				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', obligatoriu = '" . $obligatoriu[$key_s] . "', ordonare = '" . $key_s . "', tip_supliment = '" . $tip_supliment[$key_s] . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	if ( sizeof( $param['denumire_v2'] ) > '0' ) {
		foreach( $param['denumire_v2'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v2'];
				$moneda = $param['moneda_v2'];
				$tip_serv = 'Servicii neincluse';
				if ( $param['obligatoriu_v2'][$key_s] ) $obligatoriu = 'da';
				else $obligatoriu = 'nu';
				$tip_supliment = $param['tip_supliment_v2'];

				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', obligatoriu = '" . $obligatoriu . "', ordonare = '" . $key_s . "', tip_supliment = '" . $tip_supliment[$key_s] . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	if ( sizeof( $param['denumire_v3'] ) > '0' ) {
		foreach( $param['denumire_v3'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v3'];
				$moneda = $param['moneda_v3'];
				$tip_serv = 'Suplimente';
				$tip_supliment = $param['tip_supliment_v3'];
				if ( $param['obligatoriu_v3'][$key_s] ) $obligatoriu = 'da';
				else $obligatoriu = 'nu';
				$data_start_supliment = $param['data_start_supliment'][$key_s];
				$data_end_supliment = $param['data_end_supliment'][$key_s];
				
				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', obligatoriu = '" . $obligatoriu . "', tip_supliment = '" . $tip_supliment[$key_s] . "', data_start = '" . $data_start_supliment . "', data_end = '" . $data_end_supliment . "', ordonare = '" . $key_s . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	if ( sizeof( $param['denumire_v4'] ) > '0' ) {
		foreach( $param['denumire_v4'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v4'];
				$moneda = $param['moneda_v4'];
				$tip_serv = 'Reduceri';

				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', ordonare = '" . $key_s . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	if ( sizeof( $preturi['data_start'] ) > '0' ) {
		$camera = $_POST['camera'];
		foreach( $preturi['data_start'] as $key_p => $value_p ) {
			if ( $value_p && $value_p <> '0000-00-00' ) {
				if ( $key_p == '0' ) {
					$max_s = $value_p;
					$max_e = $preturi['data_end'][$key_p];
				} else {
					if ( $max_s < $value_p ) $max_s = $value_p;
					if ( $max_e < $preturi['data_end'][$key_p] ) $max_e = $preturi['data_end'][$key_p];
				}
				$data_end = $preturi['data_end'][$key_p];
				$pret = $preturi['pret'][$key_p];
				$moneda = $preturi['moneda'][$key_p];
				$tip_masa = $preturi['tip_masa'][$key_p];
				if ( sizeof( $pret ) > '0' ) {
					foreach( $pret as $key_p1 => $value_p1 ) {
						if ( $value_p1 || $tip == 'adauga_preturi' ) {
							if ( $value_p1 ) {
								if ( is_float( $value_p1 ) ) {
									$valuare_pret = round( $value_p1 );
									if ( $valuare_pret < $value_p1 ) $valuare_pret++;
								}
								else $valuare_pret = $value_p1;
							}
							else $valuare_pret = 1;

							$insPr = "insert into data_pret_oferta set id_oferta = '" . $id_oferta . "', data_start = '" . $value_p . "', data_end = '" . $data_end . "', pret = '" . $valuare_pret . "', moneda = '" . $moneda[$key_p1] . "', tip_camera = '" . $key_p1 . "', id_masa = '".$tip_masa[$key_p1]."' ";
							$quePr = mysql_query( $insPr ) or die( mysql_error() );
							@mysql_free_result( $quePr );
						}
					}
				}
			}
		}
	}

	if ( sizeof( $param['data_start_reduceri_speciale'] ) > 0 ) {
		foreach( $param['data_start_reduceri_speciale'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_nr_min = $param['data_end_reduceri_speciale'];
				$zile_deaplicare = $param['zile_deaplicare'];
				$zile_aplicare = $param['zile_aplicare'];

				$insN = "insert into reduceri_speciale set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_nr_min[$key_nr_min] . "', zile_deaplicare = '" . $param['zile_deaplicare'][$key_nr_min] . "', zile_aplicare = '" . $zile_aplicare[$key_nr_min] . "', ordonare = '" . $key_nr_mi . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	if ( sizeof( $param['data_start_nr_min'] ) > 0 ) {
		foreach( $param['data_start_nr_min'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_nr_min = $param['data_end_nr_min'];
				$nr_min = $param['nr_min'];

				$insN = "insert into nr_nopti_minim set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_nr_min[$key_nr_min] . "', nr_min = '" . $nr_min[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	if ( sizeof( $param['data_start_zile_anulare'] ) > 0 ) {
		foreach( $param['data_start_zile_anulare'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_zile_anulare = $param['data_end_zile_anulare'];
				$zile_anulare = $param['zile_anulare'];

				$insN = "insert into zile_anulare set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_zile_anulare[$key_nr_min] . "', zile_anulare = '" . $zile_anulare[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	if ( sizeof( $param['data_start_zile_plata'] ) > 0 ) {
		foreach( $param['data_start_zile_plata'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_zile_plata = $param['data_end_zile_plata'];
				$zile_plata = $param['zile_plata'];

				$insN = "insert into zile_plata set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_zile_plata[$key_nr_min] . "', zile_plata = '" . $zile_plata[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	if ( sizeof( $param['data_start_sold_out'] ) > 0 ) {
		foreach( $param['data_start_sold_out'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_sold_out = $param['data_end_sold_out'];
				$cam_sold_out = $param['cam_sold_out'];

				$insN = "insert into sold_out set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_sold_out[$key_nr_min] . "', camera = '" . $cam_sold_out[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	if ( sizeof( $aeroport['aeroport_plecare'] ) > 0 ) {
		foreach( $aeroport['aeroport_plecare'] as $key_a => $value_a ) {
			if ( $value_a ) {
				$ora_plecare = $aeroport['ora_plecare'][$key_a];
				$aeroport_sosire = $aeroport['aeroport_sosire'][$key_a];
				$ora_sosire = $aeroport['ora_sosire'][$key_a];
				$companie = $aeroport['companie'][$key_a];
				$pret = $aeroport['pret'][$key_a];
				$moneda = $aeroport['moneda'][$key_a];
				$tip = $aeroport['tip'][$key_a];

				$ins = "insert into oferte_transport_avion set id_oferta = '" . $id_oferta . "', aeroport_plecare = '" . $value_a . "', aeroport_sosire = '" . $aeroport_sosire . "', companie = '" . $companie . "', ora_plecare = '" . $ora_plecare . "', ora_sosire = '" . $ora_sosire . "', pret = '" . $pret . "', moneda = '" . $moneda . "', tip = '" . $tip . "', ordonare = '" . $key_a . "' ";
				$que = mysql_query( $ins ) or die( mysql_error() );
				@mysql_free_result( $que );
			}
		}
	}

	if ( sizeof( $autocar['id_localitate'] ) > 0 ) {
		foreach( $autocar['id_localitate'] as $key_a => $value_a ) {
			if ( $value_a ) {
				$ora = $autocar['ora'][$key_a];
				$detalii = inserare_frumos( $autocar['detalii'][$key_a] );
				$pret = $autocar['pret'][$key_a];
				$moneda = $autocar['moneda'][$key_a];

				$ins = "insert into oferte_transport_autocar set id_oferta = '" . $id_oferta . "', id_localitate = '" . $value_a . "', ora = '" . $ora . "', detalii = '" . $detalii . "', pret = '" . $pret . "', moneda = '" . $moneda . "', ordonare = '" . $key_a . "' ";
				$que = mysql_query( $ins ) or die( mysql_error() );
				@mysql_free_result( $que );
			}
		}
	}

	if ( sizeof( $param['excursii'] ) > 0 ) {
		foreach( $param['excursii'] as $key_nr_min => $excursii ) {
			if ( $excursii ) {
				$insN = "insert into oferte_excursii set id_oferta = '" . $id_oferta . "', id_excursie = '" . $excursii . "', tip = '" . $param['tip_excursie'][$excursii] . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	if ( !$param['inactiva'] ) {
		if ( $param['tip_preturi'] <> 'plecari' ) $end_date = $max_e;
		else $end_date = $max_s;
		$start_date = date( 'Y-m-d' );
		if ( $end_early_per <> '0001-01-01' ) $end_date = $end_early_per;
		if ( $end_date >= $start_date ) $valabil = 'da';
		else $valabil = 'nu';
	} else {
		$valabil = 'nu';
		$end_date = date( 'Y-m-d', mktime( 0, 0, 0, date( 'm' ), date( 'd' ) - 1, date( 'Y' ) ) );
		$start_date = $end_date;
	}

	if ( $param['oferta_parinte_early'] && $end_early_per <> '0001-01-01' ) {
		$valabil = 'nu';

		$updOP = "update oferte set start_date = '" . $end_early_per . "', valabila = 'nu' where id_oferta = '" . $param['oferta_parinte_early'] . "' ";
		$queOP = mysql_query( $updOP ) or die( mysql_error() );
		@mysql_free_result( $queOP );
	}

	$upd = "update oferte set start_date = '" . $start_date . "', end_date = '" . $end_date . "', valabila = '" . $valabil . "' where id_oferta = '" . $id_oferta . "' ";
	$queU = mysql_query( $upd ) or die( mysql_error() );
	@mysql_free_result( $queU );

	if ( $tip == 'adauga_preturi' ) echo '<script> alert(\'Sejurul a fost adaugat\'); document.location.href=\'import_xml.php?id_oferta=' . $id_oferta . '&id_hotel=' . $param['id_hotel'] . '\'; </script>';
	else echo '<script> alert(\'Sejurul a fost adaugat\'); document.location.href=\'editare_sejur.php?pas=2&oferta=' . $id_oferta . '\'; </script>';
}




function editare( $param, $preturi, $id_oferta, $url, $aeroport, $autocar, $tip ) {
	$del = "delete from data_pret_oferta where id_oferta = '" . $id_oferta . "' ";
	//$queD = mysql_query( $del ) or die( mysql_error() );
	//@mysql_free_result( $queD );
	
	//echo '<pre>'; print_r($preturi); echo '</pre>';
	//echo '<pre>'; print_r($_POST['preturi']); echo '</pre>';
	$preturi_noi=$_POST['preturi'];
	echo '<pre>'; print_r($preturi_noi); echo '</pre>';
	foreach( $preturi_noi as $key_nou => $value_nou )
	{
		//echo "data_start=".$preturi_noi['data_start'][$value_nou]."</br>";
		foreach($preturi_noi[$key_nou] as $key_1=>$value_1)
		{
		//echo '<pre>'; print_r($preturi_noi[$key_nou][$key_1]); echo '</pre>';
		
		echo 	"data_start=".$preturi_noi[$key_nou][$key_1]['data_start']."<br>";
		echo 	"data_end=".$preturi_noi[$key_nou][$key_1]['data_end']."<br>";
		echo 	"pret=".$preturi_noi[$key_nou][$key_1]['pret']."<br>";
		echo 	"moneda=".$preturi_noi[$key_nou][$key_1]['moneda']."<br>";
		echo 	"id_camera=".$preturi_noi[$key_nou][$key_1]['id_camera']."<br>";
		echo 	"id_masa=".$preturi_noi[$key_nou][$key_1]['id_masa']."<br>";
		echo "________________________<br>";
		}
		//{
			
		//}
	//print_r($preturi_noi['data_start'][$key_nou])."<br>";
	//echo '<pre>'; print_r($preturi_noi[$key_nou]); echo '</pre>';
	}
	echo "nnn".sizeof( $preturi['data_start'] );
	if ( sizeof( $preturi['data_start'] ) > '0' ) {
		foreach( $preturi['data_start'] as $key_p => $value_p ) {
			if ( $value_p && $value_p <> '0000-00-00' ) {
				if ( $key_p == '0' ) {
					$max_s = $value_p;
					$max_e = $preturi['data_end'][$key_p];
				} else {
					if ( $max_s < $value_p ) $max_s = $value_p;
					if ( $max_e < $preturi['data_end'][$key_p] ) $max_e = $preturi['data_end'][$key_p];
				}
				$data_end = $preturi['data_end'][$key_p];
				
				
				foreach($preturi['tip_masa'] as $key_m => $value_m) {
					$pret = $preturi['pret'][$key_m][$key_p];
					$moneda = $preturi['moneda'][$key_m][$key_p];
					$tip_masa = $value_m;
					if ( sizeof( $pret ) > '0' ) {
						foreach( $pret as $key_p1 => $value_p1 ) {
							if ( $value_p1 || $tip == 'importa' ) {
								if ( $value_p1 ) {
									if ( is_float( $value_p1 ) ) {
										$valuare_pret = round( $value_p1 );
										if ( $valuare_pret < $value_p1 ) $valuare_pret++;
									}
									else $valuare_pret = $value_p1;
								}
								else $valuare_pret = 1;
								
							echo	$insPr = "insert into data_pret_oferta set id_oferta = '" . $id_oferta . "', data_start = '" . $value_p . "', data_end = '" . $data_end . "', pret = '" . $valuare_pret . "', moneda = '" . $moneda[$key_p1] . "', tip_camera = '" . $key_p1 . "', id_hotel = '" . $preturi['id_hotel'] . "', of_logictour = '" . $preturi['of_logictour'] . "', id_masa = '".$tip_masa."' ";
								$quePr = mysql_query( $insPr ) or die( mysql_error() );
								@mysql_free_result( $quePr );
							}
						}
					}
				}
			}
		}
	}

	$del = "delete from early_booking where id_oferta = '" . $id_oferta . "' and tip = 'sejur' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$end_early_per = '0001-01-01';

	if ( sizeof( $param['early_time'] ) > '0' ) {
		foreach( $param['early_time'] as $key => $value ) {
			$incl = $param['early_inclus'][$key];
			if ( $incl == 'da' ) {
				if ( $value > $end_early_per ) $end_early_per = $value;
			}

			$insE = "insert into early_booking set id_oferta = '" . $id_oferta . "', tip = 'sejur', end_date = '" . $value . "', discount = '" . $param['early_disc'][$key] . "', early_inclus = '" . $param['early_inclus'][$key] . "', text_early = '" . $param['text_early'][$key] . "' ";
			$queE = mysql_query( $insE ) or die( mysql_error() );
			@mysql_free_result( $queE );
		}
	}

	if ( !$param['inactiva'] ) {
		if ( $param['tip_preturi'] <> 'plecari' ) $end_date = $max_e; else $end_date = $max_s;
		$start_date = $_POST['start_date'];
		$inactiva = 'nu';
		if ( $end_early_per <> '0001-01-01' ) $end_date = $end_early_per;
		if ( $end_date >= $start_date ) $valabil = 'da'; else $valabil = 'nu';
	} else {
		$inactiva = 'da';
		$valabil = 'nu';
		$end_date = date( 'Y-m-d', mktime( 0, 0, 0, date( 'm' ), date( 'd' ) - 1, date( 'Y' ) ) );
	}

	if ( $param['cazare'] ) $cazare = 'da'; else $cazare = 'nu';
	if ( $param['recomandata'] ) $recomandata = 'da'; else $recomandata = 'nu';
	if ( !$calculeaza_pret = $param['calculeaza_pret'] ) $calculeaza_pret = 'nu';
	if ( !$apare_grad = $param['apare_grad'] ) $apare_grad = 'nu';
	if ( !$rezervare_online = $param['rezervare_online'] ) $rezervare_online = 'nu';
	if ( !$taxa_aeroport = $param['taxa_aeroport'] ) $taxa_aeroport = 'nu';

	$ins = 'UPDATE oferte SET
		id_hotel = "' . $param['id_hotel'] . '",
		denumire = "' . str_replace('  ',' ', trim( inserare_frumos( str_replace('&',' and ',$param["denumire"]) ) ) ) . '",
		nr_zile = "' . $param['nr_zile'] . '",
		nr_nopti = "' . $param['nr_nopti'] . '",
		id_transport = "' . $param['id_transport'] . '",
		descriere_oferta = "' . inserare_frumos( $param["descriere_oferta"] ) . '",
		ultima_modificare = SYSDATE(),
		nota = "' . inserare_frumos( $param["nota"] ) . '",
		proprietar_modificare = "' . $_SESSION['id_user_adm'] . '",
		masa = "' . $param['masa'] . '",
		exprimare_pret = "' . $param['exprimare_pret'] . '",
		end_date = "' . $end_date . '",
		start_date = "' . $start_date . '",
		furnizor = "' . $param['furnizor'] . '",
		conditii_plata="'.$param['conditii_plata'].'",
		conditii_anulare="'.$param['conditii_anulare'].'",
		observatii_furnizor = "' . inserare_frumos( $param['observatii_furnizor'] ) . '",
		cazare = "' . $cazare . '",
		comision_fix = "' . $param['comision_fix'] . '",
		moneda_comision = "' . $param['moneda_comision'] . '",
		procent_comision = "' . $param['procent_comision'] . '",
		obsercatii_comision = "' . inserare_frumos( $param['obsercatii_comision'] ) . '",
		tip_preturi = "' . $param['tip_preturi'] . '",
		last_minute = "' . $param['last_minute'] . '",
		inactiva = "' . $inactiva . '",
		recomandata = "' . $recomandata . '",
		denumire_scurta = "' . str_replace('  ',' ', trim( inserare_frumos( str_replace('&',' and ',$param['denumire_scurta']) ) ) ) . '",
		detalii_recomandata = "' . trim( inserare_frumos( $param['detalii_recomandata'] ) ) . '",
		oferta_parinte_early = "' . $param['oferta_parinte_early'] . '",
		calculeaza_pret = "' . $calculeaza_pret . '",
		apare_grad = "' . $apare_grad . '",
		rezervare_online = "' . $rezervare_online . '",
		taxa_aeroport = "' . $taxa_aeroport . '"
		WHERE id_oferta = "' . $id_oferta . '"
	';
	$que = mysql_query( $ins ) or die( mysql_error() );
	@mysql_free_result( $que );

	if(basename($_FILES['fisier']['name'])!='') {
		$target_path='../uploads/oferte/';
		$upfile='id'.$id_furnizor.'_'.date('YmdHis').'_'.basename($_FILES['fisier']['name']);
		$target_path=$target_path.$upfile; 
		
		if(move_uploaded_file($_FILES['fisier']['tmp_name'], $target_path)) {
			echo "The file ".basename($_FILES['fisier']['name'])." has been uploaded";
		} else {
			echo "There was an error uploading the file, please try again!";
		}
		
		$ins_upload="UPDATE oferte SET
		fisier_upload='".$upfile."'
		WHERE id_oferta = '".$id_oferta."'
		";
		$que_upload=mysql_query($ins_upload) or die(mysql_error());
	}

	if ( $param['oferta_parinte_early'] && $end_early_per <> '0001-01-01' ) {
		$valabil = 'nu';
		$updOP = "update oferte set start_date = '" . $end_early_per . "', valabila = 'nu' where id_oferta = '" . $param['oferta_parinte_early'] . "' ";
		$queOP = mysql_query( $updOP ) or die( mysql_error() );
		@mysql_free_result( $queOP );
	}

	$del = "delete from oferta_sejur_tip where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['evenimente'] ) > '0' ) {
		foreach( $param['evenimente'] as $key => $value ) {
			$insE = "insert into oferta_sejur_tip set id_oferta = '" . $id_oferta . "', id_tip_oferta = '" . $value . "' ";
			$queE = mysql_query( $insE ) or die( mysql_error() );
			@mysql_free_result( $queE );
		}
	}

	if ( $param['tematici'] > 0 ) {
		$insE = "insert into oferta_sejur_tip set id_oferta = '" . $id_oferta . "', id_tip_oferta = '" . $param['tematici'] . "' ";
		$queE = mysql_query( $insE ) or die( mysql_error() );
		@mysql_free_result( $queE );
	}

	if ( $param['programe'] > 0 ) {
		$insE = "insert into oferta_sejur_tip set id_oferta = '" . $id_oferta . "', id_tip_oferta = '" . $param['programe'] . "' ";
		$queE = mysql_query( $insE ) or die( mysql_error() );
		@mysql_free_result( $queE );
	}

	$del = "delete from nr_nopti_minim where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['data_start_nr_min'] ) > 0 ) {
		foreach( $param['data_start_nr_min'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_nr_min = $param['data_end_nr_min'];
				$nr_min = $param['nr_min'];
				$insN = "insert into nr_nopti_minim set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_nr_min[$key_nr_min] . "', nr_min = '" . $nr_min[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	$del = "delete from oferte_servicii where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['denumire_v1'] ) > '0' ) {
		foreach( $param['denumire_v1'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v1'];
				$moneda = $param['moneda_v1'];
				$tip_serv = 'Servicii incluse';
				$tip_supliment = $param['tip_supliment_v1'];
				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', ordonare = '" . $key_s . "', tip_supliment = '" . $tip_supliment[$key_s] . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	if ( sizeof( $param['denumire_v2'] ) > '0' ) {
		foreach( $param['denumire_v2'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v2'];
				$moneda = $param['moneda_v2'];
				$tip_serv = 'Servicii neincluse';
				if ( $param['obligatoriu_v2'][$key_s] ) $obligatoriu = 'da'; else $obligatoriu = 'nu';
				$tip_supliment = $param['tip_supliment_v2'];

				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', obligatoriu = '" . $obligatoriu . "', ordonare = '" . $key_s . "', tip_supliment = '" . $tip_supliment[$key_s] . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	if ( sizeof( $param['denumire_v3'] ) > '0' ) {
		foreach( $param['denumire_v3'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v3'];
				$moneda = $param['moneda_v3'];
				$tip_serv = 'Suplimente';
				$tip_supliment = $param['tip_supliment_v3'];
				if ( $param['obligatoriu_v3'][$key_s] ) $obligatoriu = 'da'; else $obligatoriu = 'nu';
				$data_start_supliment = $param['data_start_supliment'][$key_s];
				$data_end_supliment = $param['data_end_supliment'][$key_s];

				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', obligatoriu = '" . $obligatoriu . "', tip_supliment = '" . $tip_supliment[$key_s] . "', data_start = '" . $data_start_supliment . "', data_end = '" . $data_end_supliment . "', ordonare = '" . $key_s . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	if ( sizeof( $param['denumire_v4'] ) > '0' ) {
		foreach( $param['denumire_v4'] as $key_s => $value_s ) {
			if ( $value_s ) {
				$value = $param['value_v4'];
				$moneda = $param['moneda_v4'];
				$tip_serv = 'Reduceri';

				$insS = "insert into oferte_servicii set id_oferta = '" . $id_oferta . "', denumire = '" . inserare_frumos( $value_s ) . "', value = '" . $value[$key_s] . "', moneda = '" . $moneda[$key_s] . "', tip_serv = '" . $tip_serv . "', ordonare = '" . $key_s . "' ";
				$queS = mysql_query( $insS ) or die( mysql_error() );
				@mysql_free_result( $queS );
			}
		}
	}

	$del = "delete from reduceri_speciale where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['data_start_reduceri_speciale'] ) > 0 ) {
		foreach( $param['data_start_reduceri_speciale'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_nr_min = $param['data_end_reduceri_speciale'];
				$zile_deaplicare = $param['zile_deaplicare'];
				$zile_aplicare = $param['zile_aplicare'];

				$insN = "insert into reduceri_speciale set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_nr_min[$key_nr_min] . "', zile_deaplicare = '" . $param['zile_deaplicare'][$key_nr_min] . "', zile_aplicare = '" . $zile_aplicare[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	$del = "delete from sold_out where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['data_start_sold_out'] ) > 0 ) {
		foreach( $param['data_start_sold_out'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_sold_out = $param['data_end_sold_out'];
				$cam_sold_out = $param['cam_sold_out'];

				$insN = "insert into sold_out set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_sold_out[$key_nr_min] . "', camera = '" . $cam_sold_out[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	$del = "delete from zile_anulare where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['data_start_zile_anulare'] ) > 0 ) {
		foreach( $param['data_start_zile_anulare'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_zile_anulare = $param['data_end_zile_anulare'];
				$zile_anulare = $param['zile_anulare'];

				$insN = "insert into zile_anulare set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_zile_anulare[$key_nr_min] . "', zile_anulare = '" . $zile_anulare[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	$del = "delete from zile_plata where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['data_start_zile_plata'] ) > 0 ) {
		foreach( $param['data_start_zile_plata'] as $key_nr_min => $value_nr_min ) {
			if ( $value_nr_min && $value_nr_min <> '0000-00-00' ) {
				$data_end_zile_plata = $param['data_end_zile_plata'];
				$zile_plata = $param['zile_plata'];

				$insN = "insert into zile_plata set id_oferta = '" . $id_oferta . "', data_start = '" . $value_nr_min . "', data_end = '" . $data_end_zile_plata[$key_nr_min] . "', zile_plata = '" . $zile_plata[$key_nr_min] . "', ordonare = '" . $key_nr_min . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	$del1 = "delete from oferte_transport_avion where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del1 ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $aeroport['aeroport_plecare'] ) > 0 ) {
		foreach( $aeroport['aeroport_plecare'] as $key_a => $value_a ) {
			if ( $value_a ) {
				$ora_plecare = $aeroport['ora_plecare'][$key_a];
				$aeroport_sosire = $aeroport['aeroport_sosire'][$key_a];
				$ora_sosire = $aeroport['ora_sosire'][$key_a];
				$companie = $aeroport['companie'][$key_a];
				$pret = $aeroport['pret'][$key_a];
				$moneda = $aeroport['moneda'][$key_a];
				$tip = $aeroport['tip'][$key_a];

				$ins = "insert into oferte_transport_avion set id_oferta = '" . $id_oferta . "', aeroport_plecare = '" . $value_a . "', aeroport_sosire = '" . $aeroport_sosire . "', companie = '" . $companie . "', ora_plecare = '" . $ora_plecare . "', ora_sosire = '" . $ora_sosire . "', pret = '" . $pret . "', moneda = '" . $moneda . "', tip = '" . $tip . "', ordonare = '" . $key_a . "' ";
				$que = mysql_query( $ins ) or die( mysql_error() );
				@mysql_free_result( $que );
			}
		}
	}

	$del = "delete from oferte_transport_autocar where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $autocar['id_localitate'] ) > 0 ) {
		foreach( $autocar['id_localitate'] as $key_a => $value_a ) {
			if ( $value_a ) {
				$ora = $autocar['ora'][$key_a];
				$detalii = $autocar['detalii'][$key_a];
				$pret = $autocar['pret'][$key_a];
				$moneda = $autocar['moneda'][$key_a];

				$ins = "insert into oferte_transport_autocar set id_oferta = '" . $id_oferta . "', id_localitate = '" . $value_a . "', ora = '" . $ora . "', detalii = '" . inserare_frumos( $detalii ) . "', pret = '" . $pret . "', moneda = '" . $moneda . "', ordonare = '" . $key_a . "' ";
				$que = mysql_query( $ins ) or die( mysql_error() );
				@mysql_free_result( $que );
			}
		}
	}

	$del = "delete from oferte_excursii where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	if ( sizeof( $param['excursii'] ) > 0 ) {
		foreach( $param['excursii'] as $key_nr_min => $excursii ) {
			if ( $excursii ) {
				$insN = "insert into oferte_excursii set id_oferta = '" . $id_oferta . "', id_excursie = '" . $excursii . "', tip = '" . $param['tip_excursie'][$excursii] . "' ";
				$queN = mysql_query( $insN ) or die( mysql_error() );
				@mysql_free_result( $queN );
			}
		}
	}

	/*echo '<script> alert(\'Datele au fost modificate\'); document.location.href=\'' . $url . '\'; </script>';*/
}




function sterge_sejur( $id_oferta, $filtruF ) {
	$del = "delete from oferte where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from data_pret_oferta where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from early_booking where id_oferta = '" . $id_oferta . "' and tip = 'sejur' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from oferta_sejur_tip where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from nr_nopti_minim where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from oferte_servicii where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from reduceri_speciale where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from sold_out where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from zile_anulare where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from zile_plata where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from oferte_excursii where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from oferte_transport_autocar where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$del = "delete from oferte_transport_avion where id_oferta = '" . $id_oferta . "' ";
	$queD = mysql_query( $del ) or die( mysql_error() );
	@mysql_free_result( $queD );

	$link = 'editare_hotel.php';
	if ( $filtruF ) $link = $link . '?filtrare=da' . $filtruF;
	echo '<script> alert(\'Sejurul a fost sters\'); document.location.href=\'' . $link . '\'; </script>';
}




function select_camp_sejur( $id_oferta ) {
	$sel = "select
	oferte.*,
	localitati.id_localitate,
	hoteluri.nume as denumire_hotel,
	hoteluri.tip_unitate,
	continente.nume_continent,
	tari.id_tara,
	tari.denumire as denumire_tara,
	localitati.denumire as denumire_localitate,
	zone.denumire as denumire_zona
	from oferte
	left join hoteluri on oferte.id_hotel = hoteluri.id_hotel
	left join localitati on hoteluri.locatie_id = localitati.id_localitate
	left join zone on localitati.id_zona = zone.id_zona
	left join tari on zone.id_tara = tari.id_tara
	left join continente on hoteluri.id_continent = continente.id_continent
	where oferte.id_oferta = '" . $id_oferta . "' ";
	$que = mysql_query( $sel ) or die( mysql_error() );
	$param = mysql_fetch_array( $que );
	@mysql_free_result( $que );

	$selE = "select data_start, data_end, nr_min from nr_nopti_minim where id_oferta = '" . $id_oferta . "' Group by data_start, data_end, nr_min, ordonare Order by ordonare ";
	$queE = mysql_query( $selE ) or die( mysql_error() );
	while ( $rowE = mysql_fetch_array( $queE ) ) {
		$param['data_start_nr_min'][] = $rowE['data_start'];
		$param['data_end_nr_min'][] = $rowE['data_end'];
		$param['nr_min'][] = $rowE['nr_min'];
	}
	@mysql_free_result( $queE );

	$selE = "select end_date, discount, early_inclus, text_early from early_booking where id_oferta = '" . $id_oferta . "' and tip = 'sejur' ";
	$queE = mysql_query( $selE ) or die( mysql_error() );
	while ( $rowE = mysql_fetch_array( $queE ) ) {
		$param['early_time'][] = $rowE['end_date'];
		$param['early_disc'][] = $rowE['discount'];
		$param['early_inclus'][] = $rowE['early_inclus'];
		$param['text_early'][] = $rowE['text_early'];
	}
	@mysql_free_result( $queE );

	$selE = "select * from sold_out where id_oferta = '" . $id_oferta . "' Group by data_start, data_end, ordonare Order by ordonare ";
	$queE = mysql_query( $selE ) or die( mysql_error() );
	while ( $rowE = mysql_fetch_array( $queE ) ) {
		$param['data_start_sold_out'][] = $rowE['data_start'];
		$param['data_end_sold_out'][] = $rowE['data_end'];
		$param['cam_sold_out'][] = $rowE['camera'];
	}
	@mysql_free_result( $queE );

	$selE = "select * from reduceri_speciale where id_oferta = '" . $id_oferta . "' Group by data_start, data_end, zile_deaplicare, zile_aplicare, ordonare Order by ordonare  ";
	$queE = mysql_query( $selE ) or die( mysql_error() );
	while ( $rowE = mysql_fetch_array( $queE ) ) {
		$param['data_start_reduceri_speciale'][] = $rowE['data_start'];
		$param['data_end_reduceri_speciale'][] = $rowE['data_end'];
		$param['zile_deaplicare'][] = $rowE['zile_deaplicare'];
		$param['zile_aplicare'][] = $rowE['zile_aplicare'];
	}
	@mysql_free_result( $queE );

	$selE = "select * from zile_anulare where id_oferta = '" . $id_oferta . "' Group by data_start, data_end, ordonare Order by ordonare ";
	$queE = mysql_query( $selE ) or die( mysql_error() );
	while ( $rowE = mysql_fetch_array( $queE ) ) {
		$param['data_start_zile_anulare'][] = $rowE['data_start'];
		$param['data_end_zile_anulare'][] = $rowE['data_end'];
		$param['zile_anulare'][] = $rowE['zile_anulare'];
	}
	@mysql_free_result( $queE );

	$selE = "select * from zile_plata where id_oferta = '" . $id_oferta . "' Group by data_start, data_end, ordonare Order by ordonare ";
	$queE = mysql_query( $selE ) or die( mysql_error() );
	while ( $rowE = mysql_fetch_array( $queE ) ) {
		$param['data_start_zile_plata'][] = $rowE['data_start'];
		$param['data_end_zile_plata'][] = $rowE['data_end'];
		$param['zile_plata'][] = $rowE['zile_plata'];
	}
	@mysql_free_result( $queE );

	$selS = "select * from oferte_servicii where id_oferta = '" . $id_oferta . "' Group by denumire, value, moneda, tip_serv, obligatoriu, ordonare Order by ordonare ";
	$queS = mysql_query( $selS ) or die( mysql_error() );
	$v1 = 0;
	$v2 = 0;
	$v3 = 0;
	$v4 = 0;
	while ( $rowS = mysql_fetch_array( $queS ) ) {
		if ( $rowS['tip_serv'] == 'Servicii incluse' ) {
			$v1++;
			$param['denumire_v1'][$v1] = $rowS['denumire'];
			$param['value_v1'][$v1] = $rowS['value'];
			$param['moneda_v1'][$v1] = $rowS['moneda'];
			$param['obligatoriu_v1'][$v1] = $rowS['obligatoriu'];
			$param['tip_supliment_v1'][$v1] = $rowS['tip_supliment'];
		} elseif ( $rowS['tip_serv'] == 'Servicii neincluse' ) {
			$v2++;
			$param['denumire_v2'][$v2] = $rowS['denumire'];
			$param['value_v2'][$v2] = $rowS['value'];
			$param['moneda_v2'][$v2] = $rowS['moneda'];
			$param['obligatoriu_v2'][$v2] = $rowS['obligatoriu'];
			$param['tip_supliment_v2'][$v2] = $rowS['tip_supliment'];
		} elseif ( $rowS['tip_serv'] == 'Suplimente' ) {
			$v3++;
			$param['denumire_v3'][$v3] = $rowS['denumire'];
			$param['value_v3'][$v3] = $rowS['value'];
			$param['moneda_v3'][$v3] = $rowS['moneda'];
			$param['obligatoriu_v3'][$v3] = $rowS['obligatoriu'];
			$param['tip_supliment_v3'][$v3] = $rowS['tip_supliment'];
			$param['data_start_supliment'][$v3] = $rowS['data_start'];
			$param['data_end_supliment'][$v3] = $rowS['data_end'];
		} elseif ( $rowS['tip_serv'] == 'Reduceri' ) {
			$v4++;
			$param['denumire_v4'][$v4] = $rowS['denumire'];
			$param['value_v4'][$v4] = $rowS['value'];
			$param['moneda_v4'][$v4] = $rowS['moneda'];
		}
	}
	return $param;
}




function select_preturi_sejur( $id_oferta ) {
	$preturi = array();
echo	$selP = "SELECT data_pret_oferta.*
	FROM data_pret_oferta
	LEFT JOIN tip_camera ON data_pret_oferta.tip_camera = tip_camera.id_camera
	WHERE data_pret_oferta.id_oferta = '" . $id_oferta . "'
	GROUP BY data_pret_oferta.data_start, data_pret_oferta.data_end, data_pret_oferta.pret, data_pret_oferta.moneda, data_pret_oferta.tip_camera
	ORDER BY data_pret_oferta.data_start, data_pret_oferta.data_end, tip_camera.denumire
	";
	$queP = mysql_query( $selP ) or die( mysql_error() );
	while ( $rowP = mysql_fetch_array( $queP ) ) {
		$id_pret=$rowP['id_pret'];
		$preturi[$id_pret]['id_pret'] =$id_pret;
		$preturi[$id_pret]['id_hotel'] = $rowP['id_hotel'];
		$preturi[$id_pret]['of_logictour'] = $rowP['of_logictour'];
		$preturi[$id_pret]['id_camera']= $rowP['tip_camera'];
		$preturi[$id_pret]['id_masa']=$rowP['id_masa'];
		$preturi[$id_pret]['data_start']= $rowP['data_start'];
		$preturi[$id_pret]['data_end']=$rowP['data_end'];
		$preturi[$id_pret]['pret']=$rowP['pret'];
		$preturi[$id_pret]['moneda']=$rowP['moneda'];
		}
//echo '<pre>'; print_r($preturi['tip_masa']); echo '</pre>';
		
		
	//}
	@mysql_free_result( $queP );
	
	
	echo '<pre>'; print_r($preturi); echo '</pre>';
	
	//echo "------".sizeof($preturi);
	
	return $preturi;
}



}
?>