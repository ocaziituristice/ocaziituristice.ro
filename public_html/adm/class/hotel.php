<?php
class HOTEL {
	function adaugare($param, $circuit) {
		$img=array();
		$error_poza=array();
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		$id_tara=$param['tara'];
		$id_zona=$param['zona'];
		$id_localitate=$param['localitate'];
		$distanta=$param['distanta'];
		if(!$apare_grad=$param['apare_grad']) $apare_grad='nu';
		$denumire_hotel=str_replace('  ',' ',str_replace('   ',' ',str_replace('-',' ',trim(inserare_frumos(str_replace('&',' and ',$param['denumire']))))));
		
		$ins="INSERT INTO hoteluri SET
			locatie_id = '".$id_localitate."',
			nume='".$denumire_hotel."',
			
			descriere='".inserare_frumos($param['descriere'])."',
			descriere_scurta='".inserare_frumos($param['descriere_scurta'])."',
			descriere_seo='".inserare_frumos($param['descriere_seo'])."',
			titlu_seo='".inserare_frumos($param['titlu_seo'])."',
			cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."',
			new_descriere='".inserare_frumos($param['new_descriere'])."',
			new_camera='".inserare_frumos($param['new_camera'])."',
			new_teritoriu='".inserare_frumos($param['new_teritoriu'])."',
			new_relaxare='".inserare_frumos($param['new_relaxare'])."',
			new_pentru_copii='".inserare_frumos($param['new_pentru_copii'])."',
			new_plaja='".inserare_frumos($param['new_plaja'])."',
			poza1 = '$img[1]',
			poza2 = '$img[2]',
			poza3 = '$img[3]',
			poza4 = '$img[4]',
			poza5 = '$img[5]',
			poza6 = '$img[6]',
			poza7 = '$img[7]',
			poza8 = '$img[8]',
			poza9 = '$img[9]',
			poza10 = '$img[10]',
			poza11 = '$img[11]',
			poza12 = '$img[12]',
			poza13 = '$img[13]',
			poza14 = '$img[14]',
			poza15 = '$img[15]',
			poza16 = '$img[16]',
			poza17 = '$img[17]',
			poza18 = '$img[18]',
			poza19 = '$img[19]',
			poza20 = '$img[20]',
			stele = '".$param['stele']."',
			latitudine = '".$param['latitudine']."',
			longitudine = '".$param['longitudine']."',
			adresa = '".inserare_frumos($param['adresa'])."',
			cod_postal = '".$param['cod_postal']."',
			email = '".inserare_frumos($param['email'])."',
			website = '".$param['website']."',
			tip_unitate = '".$param['tip_unitate']."',
			concept = '".$param['concept']."',
			detalii_concept = '".$param['detalii_concept']."',
			distanta = '$distanta',
			distanta_fata_de = '".$param['distanta_fata_de']."',
			general = '".inserare_frumos($param['general'])."',
			servicii = '".inserare_frumos($param['servicii'])."',
			internet = '".inserare_frumos($param['internet'])."',
			parcare = '".inserare_frumos($param['parcare'])."',
			plaja = '".inserare_frumos($param['plaja'])."',
			allinclusive = '".inserare_frumos($param['allinclusive'])."',
			recomandat = '".$param['recomandat']."',
			check_in = '".$param['check_in']."',
			check_out = '".$param['check_out']."',
			anulare_plata = '".$param['anulare_plata']."',
			info_copii = '".inserare_frumos($param['info_copii'])."',
			accepta_animale = '".$param['accepta_animale']."',
			carduri = '".inserare_frumos($param['carduri'])."',
			nr = '".$param['nr']."',
			observatii = '".inserare_frumos($param['observatii'])."',
			garantam = '".inserare_frumos($param['garantam'])."',
			proprietar = '".$_SESSION['id_user_adm']."',
			data_adaugarii = SYSDATE(),
			telefon = '".$_POST['telefon']."',
			stare = '".$_POST['stare']."',
			observatii_stare = '".inserare_frumos($param['observatii_stare'])."',
			obiectice_turistice = '".inserare_frumos($_POST['obiectice_turistice'])."',
			fisier_upload = '".$_POST['fisier_upload']."',
			apare_grad = '".$apare_grad."',
			cautare_live = '".$param['cautare_live']."',
			tur_virtual_3d = '".trim($param['tur_virtual_3d'])."',
			video_youtube = '".trim($param['video_youtube'])."'
		";
		$que=mysql_query($ins) or die(mysql_error());
		$id_hotel=mysql_insert_id();
		@mysql_free_result($que);
		if(sizeof($param['camere_hotel'])>'0') {
			foreach($param['camere_hotel'] as $key=>$value) {
				$detalii_camera=inserare_frumos($param['detalii_camera'][$key]);
				$ordine_camera=$param['ordine_camera'][$key];
				
				$insC="insert into camere_hotel set id_hotel = '".$id_hotel."', id_camera = '".$value."', detalii = '".$detalii_camera."', ordine = '".$ordine_camera."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		if(sizeof($param['legaturi']['url'])>'0') {
			foreach($param['legaturi']['url'] as $key_leg => $value_leg) {
				$ins_leg = "INSERT INTO hoteluri_legaturi SET url = '".$value_leg."', id_hotel = '".$id_hotel."', legatura = '".$key_leg."' ";
				$que_leg = mysql_query($ins_leg) or die(mysql_error());
				@mysql_free_result($que_leg);
			}
		}
		
		//traseu_circ
		if(sizeof($circuit['tara'])>0 && $param['tip_unitate']=='Circuit') {
			foreach($circuit['tara'] as $key_t=>$value_t) {
				$localitate=$circuit['localitate'][$key_t];
				$hotel=$circuit['hotel'][$key_t];
				$detalii=inserare_frumos($circuit['detalii'][$key_t]);
				
				$insC="insert into traseu_circuit set id_hotel_parinte = '".$id_hotel."', id_tara = '".$value_t."', id_localitate = '".$localitate."', id_hotel = '".$hotel."', detalii = '".$detalii."', ordine = '".$key_t."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		if(sizeof($param['hotel_caracteristici'])>0) {
			foreach($param['hotel_caracteristici'] as $id=>$value) {
				if($value=='da') {
					$observatii=$param['hotel_caracteristici_detaliii'][$id];
					
					$ins="insert into hotel_caracteristici set id_hotel = '".$id_hotel."', id = '".$id."', observatii = '".$observatii."' ";
					$que=mysql_query($ins) or die(mysql_error());
					@mysql_free_result($que);
				}
			}
		}
		
		if(sizeof($param['tip_masa'])>0) {
			foreach($param['tip_masa'] as $km=>$vm) {
				$ins="INSERT INTO hotel_meal SET id_hotel = '".$id_hotel."', masa = '".$vm."', id_masa = '".$km."', insert_manual = 'da' ";
				$que=mysql_query($ins) or die(mysql_error());
				@mysql_free_result($que);
			}
		}
		
		if(sizeof($error_poza)>0) { ?>
        	<form name="pas_urm" method="post" action="editare_hotel.php?pas=2&hotel=<?php echo $id_hotel; ?>#poze" />
			<?php if(sizeof($error_poza)>0) {
				foreach($error_poza as $key_1=>$error1) { ?>
                	<input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
				<?php }
			} ?>
			<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit(); </script>
            </form>
		<?php } else echo '<script>alert(\'Hotelul a fost adaugata!\'); document.location.href=\'editare_hotel.php?pas=3&hotel='.$id_hotel.'\'; </script>';
	}
	
	
	function editeaza($param, $id_hotel, $url, $circuit) {
		$img=array();
		$error_poza=array();
		if(sizeof($param['img'])>0) {
			if(sizeof($param['img']['error'])>0) {
				foreach($param['img']['error'] as $key_ar=>$err) $error_poza[$key_ar]=$err;
			}
			if(sizeof($param['img']['poza'])>0) {
				foreach($param['img']['poza'] as $key_p=>$poza) $img[$key_p]=$poza;
			}
		}
		if($param['poza1']) $img[1]=$param['poza1'];
		if($param['poza2']) $img[2]=$param['poza2'];
		if($param['poza3']) $img[3]=$param['poza3'];
		if($param['poza4']) $img[4]=$param['poza4'];
		if($param['poza5']) $img[5]=$param['poza5'];
		if($param['poza6']) $img[6]=$param['poza6'];
		if($param['poza7']) $img[7]=$param['poza7'];
		if($param['poza8']) $img[8]=$param['poza8'];
		if($param['poza9']) $img[9]=$param['poza9'];
		if($param['poza10']) $img[10]=$param['poza10'];
		if($param['poza11']) $img[11]=$param['poza11'];
		if($param['poza12']) $img[12]=$param['poza12'];
		if($param['poza13']) $img[13]=$param['poza13'];
		if($param['poza14']) $img[14]=$param['poza14'];
		if($param['poza15']) $img[15]=$param['poza15'];
		if($param['poza16']) $img[16]=$param['poza16'];
		if($param['poza17']) $img[17]=$param['poza17'];
		if($param['poza18']) $img[18]=$param['poza18'];
		if($param['poza19']) $img[19]=$param['poza19'];
		if($param['poza20']) $img[20]=$param['poza20'];
		$id_tara=$param['tara'];
		$id_zona=$param['zona'];
		$id_localitate=$param['localitate'];
		$distanta=$param['distanta'];
		if(!$apare_grad=$param['apare_grad']) $apare_grad='nu';
		$denumire_hotel=str_replace('  ',' ',str_replace('   ',' ',str_replace('-',' ',trim(inserare_frumos(str_replace('&',' and ',$param['denumire']))))));
		if($param['apare_adwords']!=1)$param['apare_adwords']=0;
		$ins="UPDATE hoteluri SET
			locatie_id = '".$id_localitate."',
			nume='".$denumire_hotel."',
			link_seo='".$param['link_seo']."',
			descriere='".inserare_frumos($param['descriere'])."',
			descriere_scurta='".inserare_frumos($param['descriere_scurta'])."',
			descriere_seo='".inserare_frumos($param['descriere_seo'])."',
			titlu_seo='".inserare_frumos($param['titlu_seo'])."',
			cuvinte_cheie_seo='".inserare_frumos($param['cuvinte_seo'])."',
			new_descriere='".inserare_frumos($param['new_descriere'])."',
			new_camera='".inserare_frumos($param['new_camera'])."',
			new_teritoriu='".inserare_frumos($param['new_teritoriu'])."',
			new_relaxare='".inserare_frumos($param['new_relaxare'])."',
			new_pentru_copii='".inserare_frumos($param['new_pentru_copii'])."',
			new_plaja='".inserare_frumos($param['new_plaja'])."',
			poza1 = '$img[1]',
			poza2 = '$img[2]',
			poza3 = '$img[3]',
			poza4 = '$img[4]',
			poza5 = '$img[5]',
			poza6 = '$img[6]',
			poza7 = '$img[7]',
			poza8 = '$img[8]',
			poza9 = '$img[9]',
			poza10 = '$img[10]',
			poza11 = '$img[11]',
			poza12 = '$img[12]',
			poza13 = '$img[13]',
			poza14 = '$img[14]',
			poza15 = '$img[15]',
			poza16 = '$img[16]',
			poza17 = '$img[17]',
			poza18 = '$img[18]',
			poza19 = '$img[19]',
			poza20 = '$img[20]',
			stele = '".$param['stele']."',
			latitudine = '".$param['latitudine']."',
			longitudine = '".$param['longitudine']."',
			adresa = '".inserare_frumos($param['adresa'])."',
			cod_postal = '".$param['cod_postal']."',
			email = '".inserare_frumos($param['email'])."',
			website = '".$param['website']."',
			tip_unitate = '".$param['tip_unitate']."',
			concept = '".$param['concept']."',
			detalii_concept = '".$param['detalii_concept']."',
			distanta = '$distanta',
			distanta_fata_de = '".$param['distanta_fata_de']."',
			general = '".inserare_frumos($param['general'])."',
			servicii = '".inserare_frumos($param['servicii'])."',
			internet = '".inserare_frumos($param['internet'])."',
			parcare = '".inserare_frumos($param['parcare'])."',
			plaja = '".inserare_frumos($param['plaja'])."',
			allinclusive = '".inserare_frumos($param['allinclusive'])."',
			recomandat = '".$param['recomandat']."',
			check_in = '".$param['check_in']."',
			check_out = '".$param['check_out']."',
			anulare_plata = '".inserare_frumos($param['anulare_plata'])."',
			info_copii = '".inserare_frumos($param['info_copii'])."',
			accepta_animale = '".$param['accepta_animale']."',
			carduri = '".inserare_frumos($param['carduri'])."',
			nr = '".$param['nr']."',
			observatii = '".inserare_frumos($param['observatii'])."',
			garantam = '".inserare_frumos($param['garantam'])."',
			proprietar_modificare = '".$_SESSION['id_user_adm']."',
			ultima_modificare = SYSDATE(),
			telefon = '".$param['telefon']."',
			apare_adwords = '".$param['apare_adwords']."',
			observatii_stare = '".inserare_frumos($param['observatii_stare'])."',
			stare = '".$_POST['stare']."',
			obiectice_turistice = '".inserare_frumos($_POST['obiectice_turistice'])."',
			apare_grad = '".$apare_grad."',
			tripadvisor = '".$param['tripadvisor']."',
			cod_remarketing = '".$param['cod_remarketing']."',
			cautare_live = '".$param['cautare_live']."',
			tur_virtual_3d = '".trim($param['tur_virtual_3d'])."',
			video_youtube = '".trim($param['video_youtube'])."'
			WHERE id_hotel= '".$id_hotel."'
		";
		$que=mysql_query($ins) or die(mysql_error());
		@mysql_free_result($que);
		
		if(basename($_FILES['fisier']['name'])!='') {
			$target_path='../uploads/hotel/';
			$upfile='id'.$id_hotel.'_'.date('YmdHis').'_'.basename($_FILES['fisier']['name']);
			$target_path=$target_path.$upfile; 
			
			if(move_uploaded_file($_FILES['fisier']['tmp_name'], $target_path)) {
				echo "The file ".basename($_FILES['fisier']['name'])." has been uploaded";
			} else {
				echo "There was an error uploading the file, please try again!";
			}
			
			$ins_upload="UPDATE hoteluri SET
			fisier_upload='".$upfile."'
			WHERE id_hotel = '".$id_hotel."'
			";
			$que_upload=mysql_query($ins_upload) or die(mysql_error());
		}
	
		if(sizeof($param['legaturi']['url'])>'0') {
			foreach($param['legaturi']['url'] as $key_leg => $value_leg) {
				$sel_legs = "SELECT * FROM hoteluri_legaturi WHERE id_hotel = '".$id_hotel."' AND legatura = '".$key_leg."' ";
				$que_legs = mysql_query($sel_legs) or die(mysql_error());
				if(mysql_num_rows($que_legs)>0) {
					$upd_leg = "UPDATE hoteluri_legaturi SET url = '".$value_leg."' WHERE id_hotel = '".$id_hotel."' AND legatura = '".$key_leg."' ";
					$que_leg = mysql_query($upd_leg) or die(mysql_error());
					@mysql_free_result($que_leg);
				} else {
					$ins_leg = "INSERT INTO hoteluri_legaturi SET url = '".$value_leg."', id_hotel = '".$id_hotel."', legatura = '".$key_leg."' ";
					$que_leg = mysql_query($ins_leg) or die(mysql_error());
					@mysql_free_result($que_leg);
				}
			}
		}
		
		$del="DELETE FROM camere_hotel WHERE id_hotel = '".$id_hotel."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);
		
		if(sizeof($param['camere_hotel'])>'0') {
			foreach($param['camere_hotel'] as $key=>$value) {
				$detalii_camera=inserare_frumos($param['detalii_camera'][$key]);
				$ordine_camera=$param['ordine_camera'][$key];
				
				$insC="INSERT INTO camere_hotel SET id_hotel = '".$id_hotel."', id_camera = '".$value."', detalii = '".$detalii_camera."', ordine = '".$ordine_camera."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		$del="DELETE FROM traseu_circuit WHERE id_hotel_parinte = '".$id_hotel."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);
		
		if(sizeof($circuit['tara'])>0 && $param['tip_unitate']=='Circuit') {
			foreach($circuit['tara'] as $key_t=>$value_t) {
				$localitate=$circuit['localitate'][$key_t];
				$hotel=$circuit['hotel'][$key_t];
				$detalii=inserare_frumos($circuit['detalii'][$key_t]);
				
				$insC="INSERT INTO traseu_circuit SET id_hotel_parinte = '".$id_hotel."', id_tara = '".$value_t."', id_localitate = '".$localitate."', id_hotel = '".$hotel."', detalii = '".$detalii."', ordine = '".$key_t."' ";
				$queC=mysql_query($insC) or die(mysql_error());
				@mysql_free_result($queC);
			}
		}
		
		$del="DELETE FROM hotel_caracteristici WHERE id_hotel = '".$id_hotel."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);
		
		if(sizeof($param['hotel_caracteristici'])>0) {
			foreach($param['hotel_caracteristici'] as $id=>$value) {
				if($value=='da') {
					$observatii=$param['hotel_caracteristici_detalii'][$id];
					
					$ins="INSERT INTO hotel_caracteristici SET id_hotel = '".$id_hotel."', id = '".$id."', observatii = '".$observatii."' ";
					$que=mysql_query($ins) or die(mysql_error());
					@mysql_free_result($que);
				}
			}
		}
		
		$del_masa = "DELETE FROM hotel_meal WHERE id_hotel = '".$id_hotel."' ";
		//$del_masa = "DELETE FROM hotel_meal WHERE insert_manual = 'da' ";
		$que_masa = mysql_query($del_masa) or(mysql_error());
		@mysql_free_result($que_masa);
		
		if(sizeof($param['tip_masa'])>0) {
			foreach($param['tip_masa'] as $km=>$vm) {
				$ins="INSERT INTO hotel_meal SET id_hotel = '".$id_hotel."', masa = '".$vm."', id_masa = '".$km."', insert_manual = 'da' ";
				$que=mysql_query($ins) or die(mysql_error());
				@mysql_free_result($que);
			}
		}
		
		if(sizeof($error_poza)>0) { ?>
        	<form name="pas_urm" method="post" action="editare_hotel.php?pas=2&hotel=<?php echo $id_hotel; ?>#poze" />
			<?php if(sizeof($error_poza)>0) {
				foreach($error_poza as $key_1=>$error1) { ?>
                	<input type="hidden" name="error_poza[<?php echo $key_1; ?>]" value="<?php echo $error1; ?>" />
				<?php }
			} ?>
			<script>alert('S-a produs o eroare la adaugare poze!'); document.pas_urm.submit(); </script>
            </form>
		<?php } else echo '<script>alert(\'Datele au fost modificate!\'); document.location.href=\''.$url.'\'; </script>';
	}
	
	
	function select_camp_hotel($id_hotel) {
		 $selT="SELECT
			hoteluri.locatie_id AS localitate,
			hoteluri.nume AS denumire,
			hoteluri.link_seo,
			hoteluri.descriere,
			hoteluri.descriere_scurta,
			hoteluri.descriere_seo,
			hoteluri.titlu_seo,
			hoteluri.cuvinte_cheie_seo AS cuvinte_seo,
			hoteluri.new_descriere,
			hoteluri.new_camera,
			hoteluri.new_teritoriu,
			hoteluri.new_relaxare,
			hoteluri.new_pentru_copii,
			hoteluri.new_plaja,
			hoteluri.poza1,
			hoteluri.poza2,
			hoteluri.poza3,
			hoteluri.poza4,
			hoteluri.poza5,
			hoteluri.poza6,
			hoteluri.poza7,
			hoteluri.poza8,
			hoteluri.poza9,
			hoteluri.poza10,
			hoteluri.poza11,
			hoteluri.poza12,
			hoteluri.poza13,
			hoteluri.poza14,
			hoteluri.poza15,
			hoteluri.poza16,
			hoteluri.poza17,
			hoteluri.poza18,
			hoteluri.poza19,
			hoteluri.poza20,
			hoteluri.latitudine,
			hoteluri.longitudine,
			hoteluri.adresa,
			hoteluri.stele,
			hoteluri.cod_postal,
			hoteluri.email,
			hoteluri.website,
			hoteluri.tip_unitate,
			hoteluri.concept,
			hoteluri.detalii_concept,
			hoteluri.distanta,
			hoteluri.distanta_fata_de,
			hoteluri.general,
			hoteluri.servicii,
			hoteluri.internet,
			hoteluri.parcare,
			hoteluri.plaja,
			hoteluri.allinclusive,
			hoteluri.recomandat,
			hoteluri.check_in,
			hoteluri.check_out,
			hoteluri.anulare_plata,
			hoteluri.info_copii,
			hoteluri.accepta_animale,
			hoteluri.carduri,
			hoteluri.nr,
			hoteluri.observatii,
			hoteluri.garantam,
			hoteluri.stare,
			hoteluri.telefon,
			hoteluri.observatii_stare,
			hoteluri.apare_grad,
			hoteluri.corespodent_teztour,
			hoteluri.tripadvisor,
			hoteluri.fisier_upload,
			hoteluri.cod_remarketing,
			hoteluri.cautare_live,
			hoteluri.apare_adwords,
			hoteluri.tur_virtual_3d,
			hoteluri.video_youtube,
			localitati.denumire AS denumire_localitate,
			zone.id_zona AS zona,
			zone.denumire AS denumire_zona,
			tari.id_tara AS tara,
			tari.denumire AS denumire_tara,
			tari.country_code
			FROM hoteluri
			LEFT JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			LEFT JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN tari ON zone.id_tara = tari.id_tara
			WHERE hoteluri.id_hotel = '".$id_hotel."' ";
		$queT=mysql_query($selT) or die(mysql_error());
		$param=mysql_fetch_array($queT);
		@mysql_free_result($queT);
		
		$sel_mese = "SELECT * FROM hotel_meal WHERE id_hotel = '".$id_hotel."' ";
		$que_mese = mysql_query($sel_mese) or die(mysql_error());
		while($row_mese = mysql_fetch_array($que_mese)) {
			$param['tip_masa'][$row_mese['id_masa']] = $row_mese['masa'];
		}
		@mysql_free_result($que_mese);
		
		$sel_leg = "SELECT * FROM hoteluri_legaturi WHERE id_hotel = '".$id_hotel."' ";
		$que_leg = mysql_query($sel_leg) or die(mysql_error());
		while($row_leg = mysql_fetch_array($que_leg)) {
			$param['legaturi']['url'][$row_leg['legatura']] = $row_leg['url'];
		}
		@mysql_free_result($que_mese);
		
		return $param;
	}
	
	
	function select_traseu_circ($id_hotel) {
		$selT="SELECT * FROM traseu_circuit WHERE id_hotel_parinte = '".$id_hotel."' GROUP BY ordine ORDER BY ordine ";
		$queT=mysql_query($selT) or die(mysql_error());
		while($rowT=mysql_fetch_array($queT)) {
			$circuit['tara'][]=$rowT['id_tara'];
			$circuit['localitate'][]=$rowT['id_localitate'];
			$circuit['hotel'][]=$rowT['id_hotel'];
			$circuit['detalii'][]=$rowT['detalii'];
		} @mysql_free_result($queT);
		return $circuit;
	}
}
?> 