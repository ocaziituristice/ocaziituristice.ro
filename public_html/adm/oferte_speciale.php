<?php session_start(); include_once('restrictionare.php'); ?>
<?php
if($_REQUEST['scadenta']) $scadenta = $_REQUEST['scadenta']; else $scadenta = 5;
$scadenta_end = date("Y-m-d", strtotime('+'.$scadenta.' days'));
$scadenta_start = date("Y-m-d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
  
<h1>Oferte Speciale <a href="adauga_oferte_speciale.php" class="buttons">Adauga Oferte Speciale</a></h1>

<br />

<?php
$sel_tipof = "SELECT * FROM tip_oferta WHERE tip = 'oferte_speciale' AND CURDATE() BETWEEN data_inceput_eveniment AND data_sfarsit_eveniment ORDER BY ordine DESC ";
$que_tipof = mysql_query($sel_tipof) or die(mysql_error());
while($row_tipof = mysql_fetch_array($que_tipof)) {
?>
  <br />
  <div class="clearfix" style="font-size:1.3em;">
	<h3 style="display:inline-block;"><?php echo $row_tipof['denumire_tip_oferta']; ?></h3> | &nbsp;&nbsp;&nbsp; <?php echo date("d.m.Y", strtotime($row_tipof['data_inceput_eveniment'])).' - '.date("d.m.Y", strtotime($row_tipof['data_sfarsit_eveniment'])); if($row_tipof['reducere']) echo ' &nbsp;&nbsp; | &nbsp;&nbsp; '.$row_tipof['reducere']; ?>
  </div>
  
  <table cellpadding="2" cellspacing="2" border="1" width="100%" align="center" class="tabel">
    <tr>
      <th width="30" align="center" valign="top">#</th>
      <th align="center" valign="top" width="100">Tara</th>
      <th align="center" valign="top" width="150">Zona</th>
      <th align="center" valign="top" colspan="2">Oferta</th>
      <th align="center" valign="top">Hotel</th>
      <th align="center" valign="top" width="80">Transport</th>
      <th align="center" valign="top" width="80">Nr. nopti</th>
      <th align="center" valign="top" width="100">Masa</th>
      <th align="center" valign="top" width="100">Valabila</th>
    </tr>
    <?php
	$sel_oferte = "SELECT
	oferte.id_oferta,
	oferte.denumire_scurta,
	oferte.nr_nopti,
	oferte.masa,
	oferte.valabila,
	transport.denumire AS den_transport,
	hoteluri.id_hotel,
	hoteluri.nume,
	hoteluri.stele,
	tari.denumire AS den_tara,
	zone.denumire AS den_zona
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara
	INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	INNER JOIN transport ON oferte.id_transport = transport.id_trans
	WHERE oferta_sejur_tip.id_tip_oferta = '".$row_tipof['id_tip_oferta']."'
	ORDER BY tari.denumire ASC, zone.denumire ASC, hoteluri.nume ASC, oferte.denumire ASC
	";
	$que_oferte = mysql_query($sel_oferte) or die(mysql_error());
	$i=0;
	while($row_oferte = mysql_fetch_array($que_oferte)) {
		$i++;
	?>
    <tr class="normal" onmouseout="this.className='normal'" onmouseover="this.className='hover'">
      <td align="right" valign="top"><?php echo $i; ?></td>
      <td align="center" valign="top"><?php echo $row_oferte['den_tara']; ?></td>
      <td align="left" valign="top"><?php echo $row_oferte['den_zona']; ?></td>
      <td align="center" valign="top" width="60"><?php echo $row_oferte['id_oferta']; ?></td>
      <td align="left" valign="top"><a href="<?php echo "editare_sejur.php?pas=2&oferta=".$row_oferte['id_oferta']; ?>" class="titluArticol" target="_blank"><?php echo $row_oferte['denumire_scurta']; ?></a></td>
      <td align="left" valign="top"><a href="editare_hotel.php?pas=2&hotel=<?php echo $row_oferte['id_hotel']; ?>" target="_blank"><?php echo $row_oferte['nume'].' <img src="'.$sitepath.'images/spacer.gif" class="stele-mici-'.$row_oferte['stele'].'" />'; ?></a></td>
	  <td align="left" valign="top"><?php echo $row_oferte['den_transport']; ?></td>
      <td align="left" valign="top"><?php echo $row_oferte['nr_nopti'].' nopti'; ?></td>
      <td align="left" valign="top"><?php echo $row_oferte['masa']; ?></td>
      <td align="center" valign="top"><?php if($row_oferte['valabila']=='da') echo '<img src="images/yes.png"> <span class="green bold">Valabila</span>'; else echo '<img src="images/no.png"> <span class="red bold">Nevalabila</span>'; ?></td>
    </tr>
	<?php } ?>
  </table>
<?php } ?>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>