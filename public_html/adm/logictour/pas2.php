<h1>Adaugati perioada si preturile ofertei</h1>
<?php $id_hotel=$_REQUEST['id_hotel'];
$camere=$_POST['camere']; ?>
<form name="pas_urm" action="/adm/logictour.php?id_hotel=<?php echo $id_hotel; ?>&pas=3" method="post">
  <table width="90%" border="0" cellspacing="2" cellpadding="3" id="grad_ocupare" align="left" class="tabel-inside">
    <tr>
      <td align="left" valign="top">Perioada:<br/>
        <textarea name="data" id="data" class="mic"></textarea>
        <div class="infos">Data trebuie sa fie de forma <strong>zz.ll.aa</strong></div></td>
    </tr>
    <tr>
      <td align="left" valign="top"><h2>Camere:
          <select name="moneda" class="type2">
            <option value="EURO" selected="selected">EURO</option>
            <option value="USD">USD</option>
            <option value="RON">RON</option>
          </select>
        </h2>
        <br/>
        <?php if(sizeof($camere)>0) {
			foreach($camere as $key=>$val) { ?>
        <input type="hidden" name="camere[<?php echo $key; ?>]" value="<?php echo $val; ?>" />
        <?php echo $val.'<br/>'; ?>
        <textarea name="preturi[<?php echo $key; ?>]" class="mic float-left"></textarea>
        <div class="infos float-left">Se introduce <strong>TOATA</strong> linia in care scrie <strong>PP</strong> (de obicei prima si colorata)</div>
        <br class="clear">
        Grad ocupare <?php echo $val; ?>:<br/>
        <textarea name="preturi_gr[<?php echo $key; ?>]" class="mic float-left"></textarea>
        <div class="infos float-left">Se introduc <strong>toate liniile</strong> urmatoare celei cu Pret Pivot (PP), inclusiv cele cu Single, Double, Triple, etc.</div>
        <br class="clear">
        <br/>
        <?php }
		} ?></td>
    </tr>
    <tr>
      <td align="center" valign="top"><input type="submit" name="urm" value="Adauga preturile" onClick="if(document.getElementById('data').value!='') return true; else { alert('Nu ati inserat perioadele ofertei!'); return false; }" class="buttons" /></td>
    </tr>
  </table>
</form>
