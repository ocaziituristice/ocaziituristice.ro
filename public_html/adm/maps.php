<?php echo $param['denumire']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../adm/reset.css" rel="stylesheet" type="text/css" />
<link href="../adm/style.css" rel="stylesheet" type="text/css" />
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
      	<title>Find latitude and longitude with Google Maps</title>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAv7av7utOHi2WAbBdT0_ERxQMpSnk_PJ2h26IKWwDzHChhPamvBQdU68jeITShQF-H9vHkF9hQ7nirQ"
      type="text/javascript"></script>
<script type="text/javascript">
 function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        var center = new GLatLng(48.89364, 2.33739);
        map.setCenter(center, 15);
        geocoder = new GClientGeocoder();
        var marker = new GMarker(center, {draggable: true});  
        map.addOverlay(marker);
		document.getElementById("lat").value = center.lat().toFixed(5);
        document.getElementById("lng").value = center.lng().toFixed(5);

	  GEvent.addListener(marker, "dragend", function() {
       var point = marker.getPoint();
	      map.panTo(point);
	   document.getElementById("lat").value = point.lat().toFixed(5);
       document.getElementById("lng").value = point.lng().toFixed(5);
       
        });


	 GEvent.addListener(map, "moveend", function() {
		  map.clearOverlays();
    var center = map.getCenter();
		  var marker = new GMarker(center, {draggable: true});
		  map.addOverlay(marker);
		  document.getElementById("lat").value = center.lat().toFixed(5);
	   document.getElementById("lng").value = center.lng().toFixed(5);


	 GEvent.addListener(marker, "dragend", function() {
      var point =marker.getPoint();
	     map.panTo(point);
       document.getElementById("lat").value = point.lat().toFixed(5);
	   document.getElementById("lng").value = point.lng().toFixed(5);

        });
 
        });

      }
    }

	   function showAddress(address) {
	   var map = new GMap2(document.getElementById("map"));
       map.addControl(new GSmallMapControl());
       map.addControl(new GMapTypeControl());
       if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
              alert(address + " not found");
            } else {
		document.getElementById("lat").value = point.lat().toFixed(5);
	   document.getElementById("lng").value = point.lng().toFixed(5);
		 map.clearOverlays()
			map.setCenter(point, 14);
   var marker = new GMarker(point, {draggable: true});  
		 map.addOverlay(marker);

		GEvent.addListener(marker, "dragend", function() {
      var pt = marker.getPoint();
	     map.panTo(pt);
      document.getElementById("lat").value = pt.lat().toFixed(5);
	     document.getElementById("lng").value = pt.lng().toFixed(5);
        });


	 GEvent.addListener(map, "moveend", function() {
		  map.clearOverlays();
    var center = map.getCenter();
		  var marker = new GMarker(center, {draggable: true});
		  map.addOverlay(marker);
		  document.getElementById("lat").value = center.lat().toFixed(5);
	   document.getElementById("lng").value = center.lng().toFixed(5);

	 GEvent.addListener(marker, "dragend", function() {
     var pt = marker.getPoint();
	    map.panTo(pt);
    document.getElementById("lat").value = pt.lat().toFixed(5);
	   document.getElementById("lng").value = pt.lng().toFixed(5);
        });
 
        });

            }
          }
        );
      }
    }
 </script>    
</head>

  
<body onload="load()" onunload="GUnload()" >
 <table width="100%" border="0" cellspacing="2" cellpadding="2">
 <tr>
   <td colspan="2" align="left"><h2>Coordonate:</h2></td>
 </tr>
  <tr>
    <td align="right" valign="top" width="160">Latitudine:</td>
    <td align="left" valign="top"><input type="text" name="param[latitudine]" id="lat" class="mediu" /></td>
  </tr>
  <tr>
    <td align="right" valign="top">Longitudine:</td>
    <td align="left" valign="top"><input type="text" name="param[longitudine]" id="lng" class="mediu" /></td>
  </tr>
  <tr>
    <td colspan="2"><input type="text" name="search" id="search" class="mare" /> <input type="button" name="Cauta" value="Cauta" class="buttons" onclick="showAddress(document.getElementById('search').value);" /></td>
  </tr>
 <tr>
  <td colspan="2"><div align="center" id="map" style="width: 600px; height: 400px"><br/></div></td>
 </tr>
</table>


  <script type="text/javascript">
//<![CDATA[
if (typeof _gstat != "undefined") _gstat.audience('','pagesperso-orange.fr');
//]]>
</script>
</body>

</html>