<table cellpadding="2" cellspacing="2" border="1" width="100%" align="center" class="tabel">
  <tr>
    <th width="20" align="center" valign="top">#</th>
    <th align="center" valign="top">Denumire</th>
    <th align="center" valign="top">Valoare</th>
    <th align="center" valign="top">Scadenta</th>
    <th align="center" valign="top">Destinatar</th>
    <th align="center" valign="top">Furnizor</th>
    <th align="center" valign="top">Client</th>
    <th align="center" valign="top">Rezervare</th>
    <th align="center" valign="top" nowrap>Data document</th>
    <th width="60" align="center" valign="top">ID</th>
  </tr>
<?php $alternate = false; $i=$st-1;
while($row=mysql_fetch_array($afisare_pr)) { $i++; ?>
  <tr class="normal" onmouseout="this.className='normal'; if(document.getElementById('tools<?php echo $row['id_factura']; ?>').style.visibility == 'visible'){ document.getElementById('tools<?php echo $row['id_factura']; ?>').style.visibility = 'hidden'; }" onmouseover="this.className='hover'; if(document.getElementById('tools<?php echo $row['id_factura']; ?>').style.visibility == 'hidden'){ document.getElementById('tools<?php echo $row['id_factura']; ?>').style.visibility = 'visible'; }else{ document.getElementById('tools<?php echo $row['id_factura']; ?>').style.display = 'hidden'; }">
    <td align="right" valign="top"><?php echo $i; ?></td>
    <td align="left" valign="top">
      <a href="editare_tranzactii.php?pas=2&tranzactie=<?php echo $row['id_factura']; ?>" class="titluArticol"><?php echo $row['tip_document'].' '.$row['numar_document']; ?></a>
      <div style="visibility:hidden;" id="tools<?php echo $row['id_factura']; ?>" class="tools">
        <a href="editare_tranzactii.php?pas=2&tranzactie=<?php echo $row['id_factura']; ?>">Editeaza</a> |
        <a href="javascript: if(confirm('Sunteti sigur ca doriti sa stergeti tranzactia <?php echo $row['id_factura']; ?>?')) { window.location.href='editare_tranzactii.php?pas=1&sterge=<?php echo $row['id_factura']; ?>'; } ">Sterge</a></div>
    </td>
    <td align="center" valign="top" class="bold" nowrap><?php if($row['tip_tranzactie']=='iesire') echo '<span class="red bigger-12em">- '; else echo '<span class="green bigger-12em">'; echo $row['valoare'].' </span>'.moneda($row['valuta']); ?></td>
    <td align="center" valign="top"><?php if($row['scadenta']!='0000-00-00') echo denLuniRo(date('j F Y',strtotime($row['scadenta']))); ?></td>
    <td align="center" valign="top"><?php echo $row['tip_operatie']; ?></td>
    <td align="center" valign="top"><?php if($row['id_furnizor']) { ?><a href="editare_furnizor.php?pas=2&furnizor=<?php echo $row['id_furnizor']; ?>" target="_blank"><?php echo $row['den_furnizor']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a><?php } ?></td>
    <td align="center" valign="top"><?php if($row['id_client']) { ?><a href="useri.php?id_user=<?php echo $row['id_useri_fizice']; ?>" target="_blank"><?php echo $row['den_client']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a><?php } ?></td>
    <td align="center" valign="top"><?php if($row['id_rezervare']) { ?><a href="cereri.php?id_cerere=<?php echo $row['id_rezervare']; ?>" target="_blank"><?php echo $row['id_rezervare']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a><?php } ?></td>
    <td align="center" valign="top"><?php echo $row['data_document']; ?></td>
    <td align="center" valign="top"><?php echo $row['id_factura']; ?></td>
  </tr>
<?php } ?>
</table>