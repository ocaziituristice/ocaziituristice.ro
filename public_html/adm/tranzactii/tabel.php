<table width="100%" border="0" cellspacing="2" cellpadding="4" class="default">
  <?php if($mesaj) { ?>
  <tr>
    <td colspan="2"><font class="red"><?php echo $mesaj; ?></font></td>
  </tr>
  <?php } ?>
  <tr>
    <th valign="top" align="right" width="160">Furnizor:</th>
    <td><select name="param[id_furnizor]" onChange="document.getElementById('tip_operatie').selectedIndex='1';">
      <option value="">--</option>
	  <?php $sel_furnizor = "SELECT * FROM furnizori ORDER BY denumire ASC ";
      $que_furnizor = mysql_query($sel_furnizor) or die(mysql_error());
      $row_furnizor = mysql_fetch_array($que_furnizor);
      while($row_furnizor = mysql_fetch_array($que_furnizor)) { ?>
	  <option value="<?php echo $row_furnizor['id_furnizor']; ?>" <?php if($param['id_furnizor']==$row_furnizor['id_furnizor'] and $param['id_furnizor']!='') echo 'selected'; ?>><?php echo $row_furnizor['denumire']; ?></option>
	  <?php } @mysql_free_result($que_furnizor); ?>
    </select></td>
  </tr>
  <tr>
    <th valign="top" align="right">Tip operatie:</th>
    <td><select name="param[tip_operatie]" id="tip_operatie">
      <option value="">--</option>
      <option value="furnizor" <?php if($param['tip_operatie']=='furnizor') echo 'selected'; ?>>Furnizor</option>
      <option value="client" <?php if($param['tip_operatie']=='client') echo 'selected'; ?>>Client</option>
    </select></td>
  </tr>
  <tr>
    <th valign="top" align="right">Tip document:</th>
    <td><select name="param[tip_document]">
      <option value="">--</option>
	  <?php $sel_tipdoc = "SELECT COLUMN_TYPE FROM information_schema.columns WHERE table_name = 'facturi' AND column_name = 'tip_document' ";
      $que_tipdoc = mysql_query($sel_tipdoc) or die(mysql_error());
      $row_tipdoc = mysql_fetch_array($que_tipdoc);
      $tipdoc = explode(",", str_replace("'", "", substr($row_tipdoc['COLUMN_TYPE'], 5, (strlen($row_tipdoc['COLUMN_TYPE'])-6))));
      foreach($tipdoc as $value_tipdoc) { ?>
	  <option value="<?php echo $value_tipdoc; ?>" <?php if($param['tip_document']==$value_tipdoc and $param['tip_document']!='') echo 'selected'; ?>><?php echo $value_tipdoc; ?></option>
	  <?php } @mysql_free_result($que_tipdoc); ?>
    </select></td>
  </tr>
  <tr>
    <th valign="top" align="right">Numar:</th>
    <td><input type="text" name="param[numar_document]" class="mediu" value="<?php echo $param['numar_document']; ?>"></td>
  </tr>
  <tr>
    <th valign="top" align="right">Data:</th>
    <td><input type="date" name="param[data_document]" class="mediu" value="<?php echo $param['data_document']; ?>"></td>
  </tr>
  <tr>
    <th align="right" valign="top">Valoare:</th>
    <td>
      <select name="param[tip_tranzactie]">
        <option value="intrare" <?php if($param['tip_tranzactie']=='intrare') echo 'selected'; ?>>+</option>
        <option value="iesire" <?php if($param['tip_tranzactie']=='iesire') echo 'selected'; ?>>-</option>
      </select>
      <input type="text" name="param[valoare]" class="date" value="<?php echo $param['valoare']; ?>">
      <select name="param[valuta]">
		<option value="">--</option>
		<option value="RON" <?php if($param['valuta']=='RON') echo 'selected'; ?>>Lei</option>
		<option value="EUR" <?php if($param['valuta']=='EUR') echo 'selected'; ?>>Euro</option>
	  </select>
    </td>
  </tr>
  <tr>
    <th valign="top" align="right">Scadenta:</th>
    <td><input type="date" name="param[scadenta]" class="mediu" value="<?php echo $param['scadenta']; ?>"></td>
  </tr>
  <tr>
    <th valign="top" align="right">Observatii:</th>
    <td><input type="text" name="param[observatii]" class="mare" value="<?php echo $param['observatii']; ?>"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><input type="submit" name="adauga" value="<?php echo $valut_but_submit; ?>" class="buttons" /></td>
  </tr>
</table>
