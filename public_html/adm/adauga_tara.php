<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
$lat='44.437711';
$long='26.097366'; ?>
<input type="hidden" name="cord_lat" id="cord_lat" value="<?php echo $lat; ?>" />
<input type="hidden" name="cord_lon" id="cord_lon" value="<?php echo $long; ?>" />
<script type="text/javascript" src="js/map/main.js"></script>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
<?php //centru_______________________________________ ?>
<h1>Adauga Tara</h1>
<br /><br />
<?php if($_POST['adauga']) {
$param=$_POST['param'];

if(strlen(trim($param['denumire']))>3) {
$den=trim($param['denumire']);
if(get_id_tara($den)==0) {
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'class/tari.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'class/adaugare_poza.php');
$ad_poza=new ADAUGARE_POZA;
$nume=fa_link($den);
if($_FILES['steag']) { $steag=$_FILES['steag']; $param['steag']=$ad_poza->adauga($steag, 'steag_tara', $nume);  }
if($_FILES['harta']) { $harta=$_FILES['harta']; $param['harta']=$ad_poza->adauga($harta, 'harta_tara', $nume);  }
if($_FILES['attachments']) { $tara=$_FILES['attachments']; $param['img']=$ad_poza->adauga($tara, 'tara', $nume);  }
$add_t=new TARI;
$add_t->adaugare($param); 
  } else $mesaj='Tara exista deja in baza de date!';
 } else $mesaj='Denumirea este prea scurta!';
} ?>
<form name="adauga" enctype="multipart/form-data"  method="post" action="" >
<?php $valut_but_submit='Adauga'; include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'tari/tabel.php'); ?>
</form>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>