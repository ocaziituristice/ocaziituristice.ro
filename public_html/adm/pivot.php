<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
<script type="text/javascript" src="js/tabel_preturi_adm.js"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/datepicker_ro.js"></script>
<script type="text/javascript">
//<![CDATA[
$(function() {
		$("#data_start").datepicker({
			numberOfMonths: 3,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true
		});
		
		$("#data_end").datepicker({
			numberOfMonths: 3,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true
		});
	});
	
function pick_calendar(den) {
 $("#"+den).datepicker({
	numberOfMonths: 3,
	dateFormat: "yy-mm-dd",
	showButtonPanel: true
 });
}
//]]>
</script>	
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
<?php //centru_______________________________________ 

$sel_hot = "SELECT nume FROM hoteluri WHERE id_hotel = '".$_GET['id_hotel']."' ";
$que_hot=mysql_query($sel_hot) or die(mysql_error());
$row_hot=mysql_fetch_array($que_hot);
$nume_hotel = $row_hot['nume'];

if(!$pas=$_REQUEST['pas']) $pas=1;
switch($pas)
{
	case "1": include_once "pivot/preturi.php"; break;
	case "2": include_once "pivot/add.php"; break;
	case "3": include_once "pivot/add2.php"; break;
	case "select": include_once "pivot/select.php"; break;
	case "copiaza": include_once "pivot/copiaza_preturi.php"; break;
	case "update_idof": include_once "pivot/modifica_idof.php"; break;
}
?>
  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>