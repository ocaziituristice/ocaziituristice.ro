<?php if(!$_SESSION['id_user_adm']) {
	header(sprintf("Location: %/adm", $_SERVER['HTTP_HOST']));
	exit;
} else {
	include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/send_mail.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/login.php');
	include('cofig_adm.php');
	$log=new LOGIN($user_db);
	$err=$log->verifica_user(); if($err) {
		session_destroy();
		header(sprintf("Location: %/adm", $_SERVER['HTTP_HOST']));		
		exit;
	}
	unset($log);
	include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
	include('cofig_adm.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/adm/class/make_left.php');
} ?>
