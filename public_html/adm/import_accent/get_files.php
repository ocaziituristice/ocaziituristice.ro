<?php
// Catalog file
$sXml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://aida.dcsplus.net/ws/2010">
	<soapenv:Header>
		<ns:AuthHeader>
			<ns:Username>dream.voyage</ns:Username>
			<ns:Password>dreamvoyage</ns:Password>
			<ns:ResellerCode>DREA</ns:ResellerCode>
		</ns:AuthHeader>
	</soapenv:Header>
	<soapenv:Body>
		<ns:AIDA_PackCatalogFileRQ>
			<ns:ReturnURL>http://www.ocaziituristice.ro/includes/import_aida_accent/receivePackCatalogFile.php</ns:ReturnURL>
		</ns:AIDA_PackCatalogFileRQ>
	</soapenv:Body>
</soapenv:Envelope>';

$sUrl = 'http://ec2-54-217-165-82.eu-west-1.compute.amazonaws.com/aida/tourOperator/ws/';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);
echo $sResponse;

/*echo '<script>alert("Cererea a fost trimisa. Va rugam sa asteptati dupa ora 00:00 sa se primeasca fisierele!"); document.location.href="/adm/import_accent.php";</script>';*/

?>