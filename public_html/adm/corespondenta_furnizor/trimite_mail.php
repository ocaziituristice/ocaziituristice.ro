<?php
$idc=$_GET['idc'];
$idf=$_GET['idf'];

$sel_cerere="SELECT
cerere_rezervare.data,
cerere_rezervare.nr_nopti,
hoteluri.nume AS hotel_nume,
hoteluri.stele AS hotel_stele,
localitati.denumire AS localitate_nume,
tari.denumire AS tara_nume
FROM cerere_rezervare
INNER JOIN oferte ON oferte.id_oferta=cerere_rezervare.id_oferta
INNER JOIN hoteluri ON hoteluri.id_hotel=oferte.id_hotel
INNER JOIN localitati ON localitati.id_localitate=hoteluri.locatie_id
INNER JOIN zone ON zone.id_zona=localitati.id_zona
INNER JOIN tari ON tari.id_tara=zone.id_tara
WHERE id_cerere='$idc'
";
$que_cerere=mysql_query($sel_cerere) or die(mysql_error());
$row_cerere=mysql_fetch_array($que_cerere);

$sel_furnizor="SELECT email, denumire FROM furnizori WHERE id_furnizor='$idf' ";
$que_furnizor=mysql_query($sel_furnizor) or die(mysql_error());
$row_furnizor=mysql_fetch_array($que_furnizor);

$hotel_nume=$row_cerere['hotel_nume'];
$hotel_stele=$row_cerere['hotel_stele'];
$localitate_nume=$row_cerere['localitate_nume'];
$tara_nume=$row_cerere['tara_nume'];
?>
<script language="javascript">
function confirmSubmit() {
	var agree=confirm("Esti sigur ca e totul in ordine si ca vrei sa trimiti email-ul ?");
	if(agree) return true; else return false;
}
</script>
<h1>Trimite email furnizorului <strong><?php echo $row_furnizor['denumire']; ?></strong></h1>
<br>
<form action="/adm/corespondenta_furnizor.php?pas=trimite_mail_action&idc=<?php echo $idc; ?>&idf=<?php echo $idf; ?>" method="post">
  <table cellpadding="0" cellspacing="0" border="0" class="default">
    <tr>
      <th align="right" valign="middle"><label for="email_furnizor">Email furnizor:</label></th>
      <td align="left" valign="middle"><input type="text" id="email_furnizor" name="email_furnizor" value="<?php echo trim($row_furnizor['email']); ?>" class="mare" /></td>
    </tr>
    <tr>
      <th align="right" valign="middle"><label for="template">Tip template:</label></th>
      <td align="left" valign="middle">
      <select name="template" id="template">
		<option value="" disabled="disabled" selected="selected">Selecteaza</option>
	  <?php
	  $sel_template="SELECT * FROM corespondenta_templates WHERE activ='y' AND destinatar='furnizor' AND (tip='toate' OR tip='".$row_cerere['tip']."') ORDER BY ordine ASC ";
	  $que_template=mysql_query($sel_template) or die(mysql_error());
	  while($row_template=mysql_fetch_array($que_template)) { ?>
		<option value="<?php echo $row_template['id_template']; ?>"><?php echo $row_template['nume_template']; ?></option>
      <?php } ?>
      </select>
      </td>
    </tr>
    <tr>
      <th align="right" valign="middle"><label for="subiect">Subiect:</label></th>
      <td align="left" valign="middle"><div id="subject"><input type="text" id="subiect" name="subiect" value="<?php echo $subiect; ?>" class="mare" /></div></td>
    </tr>
    <tr>
      <td align="right" colspan="2" id="tpl_content"></td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr>
      <th align="right" valign="middle"><label>Semnatura</label></th>
      <td align="left" valign="middle"><?php echo 'Va multumesc,<br>'.$_SESSION['nume'].'<br><br>'.$sigla_txt.'<br>'.$signature; ?></td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr>
      <th align="right" valign="middle"><label>Attachment:</label></th>
      <td align="left" valign="middle">
      <?php
	  $sel_attachment="SELECT * FROM cerere_rezervare_files WHERE id_cerere='$idc' ORDER BY id_upload ";
	  $que_attachment=mysql_query($sel_attachment) or die(mysql_error());
	  while($row_attachment=mysql_fetch_array($que_attachment)) {
	  ?>
        <label><input type="checkbox" id="attachment[]" name="attachment[]" value="<?php echo $row_attachment['id_upload']; ?>" /> <?php if($row_attachment['denumire']){ echo $row_attachment['denumire'].' - '; } echo $row_attachment['fisier']; ?></label><br />
      <?php } ?>
        <label><input type="checkbox" id="conditii_asigurare" name="conditii_asigurare" value="conditii_asigurare.pdf" /> <strong>Conditii de Asigurare</strong> - conditii_asigurare.pdf</label><br />
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle" width="130">&nbsp;</td>
      <td align="left" valign="middle"><input type="submit" class="submit" name="modifica" value="  Trimite cererea furnizorului  " onClick="return confirmSubmit()" /></td>
    </tr>
  </table>
</form>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#template").change(function(){
		var optionValue = jQuery("#template").val();
		jQuery("#tpl_content").load('/adm/corespondenta_furnizor/select_templates.php', "template="+optionValue+"&idc=<?php echo $idc; ?>&idf=<?php echo $idf; ?>", function(response){
			if(response) {
				jQuery("#tpl_content").css('display', '');
			} else {
				jQuery("#tpl_content").css('display', 'none');
			}
		});
		jQuery("#subject").load('/adm/corespondenta_furnizor/subiect.php', "template="+optionValue+"&idc=<?php echo $idc; ?>&idf=<?php echo $idf; ?>", function(response){
			if(response) {
				jQuery("#subject").css('display', '');
			} else {
				jQuery("#subject").css('display', 'none');
			}
		});
	});
});
</script>
