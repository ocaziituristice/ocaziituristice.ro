<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
?>
<script type="text/javascript" src="/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="/js/tinymce/editor_load.js"></script>
<?php
$template = $_REQUEST['template']; 
$idf = $_REQUEST['idf'];
$idc = $_REQUEST['idc'];

$sel_continut = "SELECT * FROM corespondenta_templates WHERE id_template='$template' ";
$que_continut = mysql_query($sel_continut) or die(mysql_error());
$row_continut = mysql_fetch_array($que_continut);

$sel_cerere = "SELECT
cerere_rezervare.*,
oferte.denumire AS oferta_nume,
hoteluri.nume AS hotel_nume,
hoteluri.stele AS hotel_stele,
hoteluri.tip_unitate,
localitati.denumire AS localitate_nume,
tari.denumire AS tara_nume
FROM cerere_rezervare
INNER JOIN oferte ON oferte.id_oferta=cerere_rezervare.id_oferta
INNER JOIN hoteluri ON hoteluri.id_hotel=oferte.id_hotel
INNER JOIN localitati ON localitati.id_localitate=hoteluri.locatie_id
INNER JOIN zone ON zone.id_zona=localitati.id_zona
INNER JOIN tari ON tari.id_tara=zone.id_tara
WHERE id_cerere='$idc'
";
$que_cerere = mysql_query($sel_cerere) or die(mysql_error());
$row_cerere = mysql_fetch_array($que_cerere);

$data_inceput = date('d.m.Y',strtotime($row_cerere['data']));
$nr_nopti = $row_cerere['nr_nopti'];
$nr_adulti = $row_cerere['nr_adulti'];
$nr_copii = $row_cerere['nr_copii'];
$tip_masa = $row_cerere['tip_masa'];
$data_sfarsit = date('d.m.Y',strtotime($row_cerere['data'].'+'.$nr_nopti.' days'));
$oferta_nume = $row_cerere['oferta_nume'];
$hotel_nume = $row_cerere['hotel_nume'];
$hotel_stele = $row_cerere['hotel_stele'];
$localitate_nume = $row_cerere['localitate_nume'];
$tara_nume = $row_cerere['tara_nume'];
$early_booking = $row_cerere['early_booking'];
$tip_unitate = $row_cerere['tip_unitate'];

$sel_pasageri = "SELECT
pasageri.sex,
pasageri.nume,
pasageri.prenume,
pasageri.data_nasterii,
pasageri.buletin
FROM pasageri
INNER JOIN cerere_rezervare_grad_ocupare ON cerere_rezervare_grad_ocupare.id_grad_ocupare = pasageri.id_grad_ocupare
INNER JOIN cerere_rezervare_tip_camera ON cerere_rezervare_tip_camera.id_camera = cerere_rezervare_grad_ocupare.id_camera
WHERE cerere_rezervare_tip_camera.id_cerere = '$idc'
";
$que_pasageri = mysql_query($sel_pasageri) or die(mysql_error());

?>
<table cellpadding="0" cellspacing="0" width="100%" class="default">
  <tr>
    <th align="right" valign="middle" width="130"><label for="continut">Continut:</label></th>
    <td align="left" valign="middle" style="padding-bottom:10px;"><textarea id="continut" name="continut" class="tinymce" style="width:600px; height:600px;"><?php if($row_continut['continut']) include_once($_SERVER['DOCUMENT_ROOT'].'/mail/templates/cereri/'.$row_continut['continut']); ?></textarea></td>
  </tr>
</table>
