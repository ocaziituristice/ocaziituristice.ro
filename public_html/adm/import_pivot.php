<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
   <h1>Metode import preturi pivot pentru <?php echo $nume_hotel; ?></h1>

  <ul>
    <li><a href="/adm/metode_import.php?metoda=1&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link">Metoda 1</a></li>
    <li><a href="/adm/metode_import.php?metoda=2&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link">Metoda 2 - <strong>Kusadasi</strong> (excel)</a></li>
    <li><a href="/adm/metode_import.php?metoda=3&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link"><em>Metoda 3 - <strong>Cocktail Holidays</strong></em> - <strong class="red">NEVERIFICATA</strong></a></li>
    <li><a href="/adm/metode_import.php?metoda=4&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link">Metoda 4 - <strong>Cocktail Holidays</strong> (excel)</a></li>
    <li><a href="/adm/metode_import.php?metoda=5&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link">Metoda 5 - <strong>Prestige Tours</strong> (website)</a></li>
    <li><a href="/adm/metode_import.php?metoda=7&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link">Metoda 7 - <strong>Prestige Tours</strong> (excel)</a></li>
    <li><a href="/adm/metode_import.php?metoda=6&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link">Metoda 6 - <strong>TezTour</strong> (excel)</a></li>
    <li><a href="/adm/metode_import.php?metoda=8&id_hotel=<?php echo $_GET['id_hotel'].'&id_oferta='.$_GET['id_oferta']; ?>" class="link">Metoda 8 - <strong>Christian Tour</strong> (excel)</a></li>
  </ul>
  <br />
  <br />
  <?php $id_hotel=$_REQUEST['id_hotel'];
$selCam="select * from grad_ocupare_camera where id_hotel = '".$id_hotel."' Group by id_camera, nr_adulti, nr_copii Order by id_camera ";
$queCam=mysql_query($selCam) or die(mysql_error());
while($rowCam=mysql_fetch_array($queCam)) {
if($rowCam['id_camera']<>$cam) { $ind=0; $cam=$rowCam['id_camera']; }
$ocup['nr_adulti'][$rowCam['id_camera']][$ind]=$rowCam['nr_adulti'];
$ocup['nr_copii'][$rowCam['id_camera']][$ind]=$rowCam['nr_copii'];
if($rowCam['nr_copii']>0) {
 $selCam1="select * from grad_ocupare_camera where id_hotel = '".$id_hotel."' and id_camera = '".$rowCam['id_camera']."' and nr_adulti = '".$rowCam['nr_adulti']."' and nr_copii = '".$rowCam['nr_copii']."' Group by ordine Order by ordine ";
 $queCam1=mysql_query($selCam1) or die(mysql_error()); $z=0;
 while($rowCam1=mysql_fetch_array($queCam1)) { $z++;
 $ocup['varsta_min'][$rowCam['id_camera']][$ind][$z]=$rowCam1['varsta_min'];
 $ocup['varsta_max'][$rowCam['id_camera']][$ind][$z]=$rowCam1['varsta_max'];
 }@mysql_free_result($queCam1);
 
}
$ind++;
} @mysql_free_result($queCam);

$selCH="select tip_camera.denumire, tip_camera.id_camera from tip_camera inner join camere_hotel on tip_camera.id_camera = camere_hotel.id_camera where camere_hotel.id_hotel = '".$id_hotel."' Group by tip_camera.id_camera Order by tip_camera.denumire ";
$queCH=mysql_query($selCH) or die(mysql_error()); ?>
<h1>Camere hotel <a href="/adm/editare_hotel.php?pas=2&hotel=<?php echo $_GET['id_hotel']; ?>#camere" class="buttons">Adauga camere</a></h1><br/><br/>

<table width="90%" border="1" cellspacing="2" cellpadding="3" id="grad_ocupare" align="left" class="tabel-inside">
<tr>
  <th align="center" valign="top">Camera</th>
  <th align="center" valign="top" width="150">Nr. max adulti</th>
  <th align="center" valign="top">Nr. max copii</th>
</tr>
<?php $z=0; while($rowCH=mysql_fetch_array($queCH)) { $z++; ?>
<tr>
  <td align="left" valign="top"><?php echo $rowCH['denumire']; ?></td>
  <td align="left" valign="top" colspan="3">
  <table width="100%" cellspacing="0" cellpadding="0" id="ocup_<?php echo $rowCH['id_camera']; ?>">
  <tr id="randGrad<?php echo $rowCH['id_camera']; ?>_0">
    <td align="left" valign="top" width="150"><?php $nrL=1; echo
    $ocup['nr_adulti'][$rowCH['id_camera']][0]; ?></td>
    <td align="left" valign="top"><?php echo $ocup['nr_copii'][$rowCH['id_camera']][0]; ?>
  <div id="copii<?php echo $rowCH['id_camera']; ?>_0"><?php if(sizeof($ocup['varsta_min'][$rowCH['id_camera']][0])>0) {
foreach($ocup['varsta_min'][$rowCH['id_camera']][0] as $key1=>$value1) { echo 'Copil '.$key1.' interval: '.$value1.' '.$ocup['varsta_max'][$rowCH['id_camera']][0][$key1].'<br/>'; }
  } else echo '&nbsp;'; ?></div></td>
  </tr>
  <?php if(sizeof($ocup['nr_adulti'][$rowCH['id_camera']])>0) {
  foreach($ocup['nr_adulti'][$rowCH['id_camera']] as $key=>$value) {
  if($key<>0) { $nrL++; ?>
  <tr id="randGrad<?php echo $rowCH['id_camera'].'_'.$nrL; ?>">
    <td align="left" valign="top" width="150"><?php echo $ocup['nr_adulti'][$rowCH['id_camera']][$key]; ?></td>
    <td align="left" valign="top"><?php echo $ocup['nr_copii'][$rowCH['id_camera']][$key]; ?>
  <div id="copii<?php echo $rowCH['id_camera'].'_'.$nrL; ?>"><?php if(sizeof($ocup['varsta_min'][$rowCH['id_camera']][$key])>0) {
foreach($ocup['varsta_min'][$rowCH['id_camera']][$key] as $key1=>$value1) { echo 'Copil '.$key1.' interval: '.$value1.' '.$ocup['varsta_max'][$rowCH['id_camera']][$key][$key1].'<br/>'; }
  } else echo '&nbsp;'; ?></div></td>
  </tr>
  <?php }
    }
  } ?>
  </table>
  </td>
</tr>
<?php } @mysql_free_result($queCH); ?>
</table>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>
