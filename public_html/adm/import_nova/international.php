<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/adm/class/hotel.php');	
$id_furnizor = '463';
$id_hotel = $_REQUEST['hotel'];
$add_t = new HOTEL;
$param = $add_t->select_camp_hotel($id_hotel);

$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getCityRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>DREAM_RAZVAN</RequestUser>
    <RequestPass>13573R</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
  </AuditInfo>
  <RequestDetails>
    <getCityRequest CountryCode="'.$param['country_code'].'" />
  </RequestDetails>
</Request>';

$sUrl = 'http://agentii.eurosite.ro/server_xml/server.php';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);

//echo '<pre>';print_r($_POST);echo '</pre>';

if(isset($_POST['connect_hotel'])) {
	$id_oferta = $_POST['oferta'];
	$hotel_name = $_POST['hotel_name'];
	$city_code = $_POST['city_code'];
	
	$del_autoupd = "DELETE FROM preturi_autoupdate WHERE id_oferta = '".$id_oferta."' ";
	$que_autoupd = mysql_query($del_autoupd) or die(mysql_error());
	@mysql_free_result($que_autoupd);
			
	$ins_autoupdate = "INSERT INTO preturi_autoupdate (id_oferta, id_furnizor, hotel_code, transport, id_pachet, autoupdate) VALUES ('".$id_oferta."', '".$id_furnizor."', '".$hotel_name."', '".$city_code."', 'international', 'nu') ";
	$que_autoupdate = mysql_query($ins_autoupdate) or die(mysql_error());
	@mysql_free_result($que_autoupdate);
	
	$upd_offer = "UPDATE oferte SET online_prices = 'da' WHERE id_oferta = '".$id_oferta."' ";
	$que_offer = mysql_query($upd_offer) or die(mysql_error());
	@mysql_free_result($que_offer);
	
	echo '<script>alert("Hotelul a fost conectat!"); document.location.href="/adm/editare_sejur.php?pas=2&oferta='.$id_oferta.'";</script>';
}

?>
<h1><?php echo $param['denumire']; ?> - Import Nova Travel (INTERNATIONAL)</h1>

<br>
<h2>Creere corespondenta intre Hoteluri</h2>
<br>

<form action="" method="post">
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="default">
  <tr>
    <th valign="top" align="right" width="160">Hotel Ocaziituristice.ro:</th>
    <td><strong><?php echo $param['denumire'].' '.$param['stele'].'* / '.$param['denumire_localitate'].' / '.$param['denumire_zona'].' / '.$param['denumire_tara']; ?></strong></td>
  </tr>
  <tr>
    <th valign="top" align="right">Destinatie Nova:</th>
    <td><select name="city_code" class="select_simple">
		<option value="">-- Selecteaza destinatia --</option>
		<?php
        $cities = new SimpleXMLElement($sResponse);
        foreach($cities->ResponseDetails->getCityResponse->City as $key => $value) {
            echo '<option value="'.(string) $value->CityCode.'">'.(string) $value->CityName.'</option>';
        }
        ?>
    </select></td>
  </tr>
  <tr>
	<th valign="top" align="right">Denumire Hotel Nova:</th>
	<td>
      <input type="text" value="" name="hotel_name" class="mare">
      <div class="infos">* Denumirea hotelului se va lua din sistem. ATENTIE la denumiri intrucat pot fi mai multe combinatii! Trebuie luata denumirea cea mai generala ca sa cuprinda toate variantele dar si cea mai unica in acelasi timp pentru hotelul respectiv.</div>
    </td>
  </tr>
  <tr>
	<td>
	  <label for="oferta" class="bigger-12em red"><strong>Selectati oferta:</strong></label>
	</td>
	<td>
	<select name="oferta" id="oferta">
    <option value="">--</option>
    <?php $selALO = "SELECT denumire, id_oferta, nr_nopti, furnizor FROM oferte WHERE id_hotel = '".$id_hotel."' GROUP BY id_oferta ORDER BY denumire ";
	$queALO = mysql_query($selALO) or die(mysql_error());
	while($rowALO = mysql_fetch_array($queALO)) {
		$detalii_furnizor = get_detalii_furnizor($rowALO['furnizor']);
		echo '<option value="'.$rowALO['id_oferta'].'">'.$rowALO['denumire'].', '.$rowALO['nr_nopti'].' nopti - '.$rowALO['id_oferta'].' | '.$detalii_furnizor['denumire'].'</option>';
	} @mysql_free_result($queALO); ?>
	</select>
	</td>
  </tr>
</table>
<br><br>
  
<div style="padding:5px;"><input type="submit" name="connect_hotel" class="submit" value="Conecteaza oferta noastra cu furnizorul"></div>
</form>
