<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/adm/cofig_adm.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>

<?php
$user_info = getUserInfo($_GET['idu']);
$email_user = $user_info['email'];
$nr_cereri = $user_info['nr_cereri'];
$nr_reviews = $user_info['nr_reviews'];
?>

<a href="/adm/corespondenta_client.php?pas=trimite_sms&idu=<?php echo $_GET['idu']; ?>" class="buttons" target="_blank">Trimite SMS client</a>

<ol class="corespondenta">
<?php
$sel_sms_client = "SELECT corespondenta_client.*, useri.nume
FROM corespondenta_client
LEFT JOIN useri ON useri.id_user = corespondenta_client.id_admin
WHERE id_useri_fizice = '".$_GET['idu']."'
AND corespondenta_client.subiect LIKE 'SMS%'
ORDER BY data_trimitere ASC ";
$que_sms_client = mysql_query($sel_sms_client) or die(mysql_error());
if(mysql_num_rows($que_sms_client)>0) {
	while($row_sms_client=mysql_fetch_array($que_sms_client)) {
    	echo '<li class="clear"><a class="expandable emailC'.$row_sms_client['id_mail'].'">'.$row_sms_client['subiect'].'</a> - <strong>'.date('d.m.Y H:i:s',strtotime($row_sms_client['data_trimitere'])).'</strong> - '.$row_sms_client['nume'].'<div id="emailContentC'.$row_sms_client['id_mail'].'" class="clear black" style="background:#FFF;">'.$row_sms_client['continut'].'</div></li>';
?>
	<script type="text/javascript">
    $("#emailContentC<?php echo $row_sms_client['id_mail']; ?>").hide();
    $(".emailC<?php echo $row_sms_client['id_mail']; ?>").show();
    $(".emailC<?php echo $row_sms_client['id_mail']; ?>").click(function() {
    $("#emailContentC<?php echo $row_sms_client['id_mail']; ?>").slideToggle('fast');
    $(this).toggleClass("expandableON");
    });
    </script>
<?php }
} ?>
</ol>

<?php if($nr_cereri>0) { ?>
<div style="padding:6px 10px 12px 10px; border-bottom:4px solid #CCC;">
<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-collapse:collapse; color:#333;">
  <tr>
    <th align="center" valign="top" width="20">#</th>
    <th align="center" valign="top">Oferta</th>
    <th align="center" valign="top" width="130">Data adaugarii</th>
    <th align="center" valign="top" width="130">Tip cerere</th>
    <th align="center" valign="top">Stare</th>
    <th align="center" valign="top" width="60">ID cerere</th>
  </tr>
<?php
$sel_rez = "SELECT
cerere_rezervare.*,
oferte.id_oferta,
localitati.denumire AS denumire_localitate,
tari.denumire AS denumire_tara,
continente.nume_continent,
furnizori.denumire AS denumire_furnizor,
furnizori.id_furnizor,
hoteluri.tip_unitate
FROM cerere_rezervare
INNER JOIN useri_fizice ON cerere_rezervare.id_useri_fizice = useri_fizice.id_useri_fizice
LEFT JOIN oferte ON cerere_rezervare.id_oferta = oferte.id_oferta
LEFT JOIN hoteluri ON hoteluri.id_hotel = oferte.id_hotel
LEFT JOIN localitati ON localitati.id_localitate = hoteluri.locatie_id
LEFT JOIN zone ON zone.id_zona = localitati.id_zona
LEFT JOIN tari ON tari.id_tara = zone.id_tara
LEFT JOIN continente ON continente.id_continent = hoteluri.id_continent
LEFT JOIN furnizori ON furnizori.id_furnizor = cerere_rezervare.id_furnizor
WHERE useri_fizice.email = '".$email_user."'
ORDER BY cerere_rezervare.id_cerere DESC
";
$que_rez = mysql_query($sel_rez) or die(mysql_error());
$i=0;
while($row_rez = mysql_fetch_array($que_rez)) {
	$i++;
	$tip_unitate = $row_rez['tip_unitate'];
	if($tip_unitate=='Circuit') {
		$link_oferta = $sitepath.'circuit/'.fa_link($row_rez['nume_continent']).'/'.fa_link_oferta($row_rez['oferta_denumire']).'-'.$row_rez['id_oferta'].'.html';
	} else {
		$link_oferta = $sitepath.fa_link($row_rez['denumire_tara']).'/'.fa_link($row_rez['denumire_localitate']).'/'.fa_link_oferta($row_rez['oferta_denumire']).'-'.$row_rez['id_oferta'].'.html';
	}
?>
  <tr>
    <td align="right" valign="top"><?php echo $i; ?></td>
    <td align="left" valign="top"><a href="cereri.php?id_cerere=<?php echo $row_rez['id_cerere']; ?>" target="_blank"><strong class="bigger-11em red"><?php echo $row_rez['oferta_denumire']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;" /></strong></a></td>
    <td align="center" valign="top"><?php echo $row_rez['data_adaugarii']; ?></td>
    <td align="center" valign="top"><?php echo $row_rez['tip']; ?></td>
    <td align="center" valign="top"><?php echo $row_rez['stare']; ?></td>
    <td align="center" valign="top"><?php echo $row_rez['id_cerere']; ?></td>
  </tr>
<?php } ?>
</table>
</div>
<?php } ?>
