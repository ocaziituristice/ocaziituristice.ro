<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/adm/class/hotel.php');	
$id_hotel = $_REQUEST['hotel'];
$add_t = new HOTEL;
$param = $add_t->select_camp_hotel($id_hotel);
?>
<h1>Lista Pachete - Christian Tour</h1>

<div class="filtrare">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td align="left" valign="middle">
      <form name="filtru" method="post" action="import_christian.php?pas=lista_pachete">
      <?php $sel_orase1 = "SELECT
	  		import_pachete.id_plecare,
			localitati_corespondent.nume_furnizor AS departure
	  		FROM import_pachete
			INNER JOIN localitati_corespondent ON (import_pachete.id_plecare = localitati_corespondent.code_furnizor AND localitati_corespondent.id_furnizor = '".$id_furnizor."')
			WHERE import_pachete.id_furnizor = '".$id_furnizor."'
			GROUP BY import_pachete.id_plecare
			ORDER BY localitati_corespondent.nume_furnizor ASC
			";
            $rez_orase1 = mysql_query($sel_orase1);
			while($row_orase1 = mysql_fetch_array($rez_orase1,MYSQL_ASSOC)) {
				$departure[$row_orase1['id_plecare']] = $row_orase1['departure'];
			}
      		$sel_orase2 = "SELECT
	  		import_pachete.id_destinatie,
			localitati_corespondent.nume_furnizor AS destination
	  		FROM import_pachete
			INNER JOIN localitati_corespondent ON (import_pachete.id_destinatie = localitati_corespondent.code_furnizor AND localitati_corespondent.id_furnizor = '".$id_furnizor."')
			WHERE import_pachete.id_furnizor = '".$id_furnizor."'
			GROUP BY import_pachete.id_destinatie
			ORDER BY localitati_corespondent.nume_furnizor ASC
			";
            $rez_orase2 = mysql_query($sel_orase2);
			while($row_orase2 = mysql_fetch_array($rez_orase2,MYSQL_ASSOC)) {
				$destination[$row_orase2['id_destinatie']] = $row_orase2['destination'];
			}
	  ?>
		Plecare: <select name="plecare">
          <option value="">Selecteaza</option>
		  <?php foreach($departure as $k_dep => $v_dep) { ?>
		  <option value="<?php echo $k_dep; ?>" <?php if($_POST['plecare']==$k_dep) echo 'selected'; ?>><?php echo $v_dep; ?></option>
		  <?php } ?>
		</select>
		&nbsp;&nbsp; Destinatie: <select name="destinatie">
          <option value="">Selecteaza</option>
		  <?php foreach($destination as $k_des => $v_des) { ?>
		  <option value="<?php echo $k_des; ?>" <?php if($_POST['destinatie']==$k_des) echo 'selected'; ?>><?php echo $v_des; ?></option>
		  <?php } ?>
		</select>
        <input type="hidden" name="filtrare" value="da" />
        <button type="submit">Go</button>
      </form>
      <form name="filtru" method="post" action="import_christian.php?pas=lista_pachete">
        <button type="submit">Reset</button>
      </form>
      </td>
    </tr>
  </table>
</div>   

<?php if(!isset($_REQUEST['filtrare'])) { exit; } else { ?>

<br><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
$sel_localitati = "SELECT * FROM localitati_corespondent LEFT JOIN localitati ON localitati.id_localitate = localitati_corespondent.id_corespondent WHERE id_furnizor = '".$id_furnizor."' ";
$que_localitati = mysql_query($sel_localitati);
while($row_localitati = mysql_fetch_array($que_localitati)) {
	if($row_localitati['denumire']!='') {
		$den_loc = $row_localitati['denumire'];
	} else {
		$den_loc = $row_localitati['nume_furnizor'];
	}
	$localitati[$row_localitati['code_furnizor']] = $den_loc;
}

$sel_pack = "SELECT 
import_pachete.*,
import_hoteluri.*
FROM import_pachete
LEFT JOIN import_hoteluri ON import_hoteluri.id_hotel = import_pachete.id_hotel
WHERE import_pachete.id_furnizor = '".$id_furnizor."'
AND import_hoteluri.id_furnizor = '".$id_furnizor."' ";
if(isset($_POST['plecare']) and $_POST['plecare']!='') $sel_pack .= " AND import_pachete.id_plecare = '".$_POST['plecare']."' ";
if(isset($_POST['destinatie']) and $_POST['destinatie']!='') $sel_pack .= " AND import_pachete.id_destinatie = '".$_POST['destinatie']."' ";
$que_pack = mysql_query($sel_pack);
while($row_pack = mysql_fetch_array($que_pack)) {
	
	$hotel_legat = '';
	$sel_autoupdate = "SELECT * FROM preturi_autoupdate WHERE id_furnizor = '".$id_furnizor."' AND autoupdate = 'nu' AND hotel_code = '".$row_pack['id_hotel']."' AND plecare LIKE '%".$row_pack['id_plecare']."%' AND plecare LIKE '%".$row_pack['id_destinatie']."%' GROUP BY id_oferta ";
	$que_autoupdate = mysql_query($sel_autoupdate);
	if(mysql_num_rows($que_autoupdate)>0) {
		$hotel_legat .= '<img src="images/yes.png"> <strong class="green">Legat</strong>:';
		while($row_autoupdate = mysql_fetch_array($que_autoupdate)) {
			$hotel_legat .= ' <a href="editare_sejur.php?pas=2&oferta='.$row_autoupdate['id_oferta'].'" target="_blank" class="link"><strong>'.$row_autoupdate['id_oferta'].'</strong></a>,';
		}
		$hotel_legat = substr($hotel_legat, 0, -1);
	}
?>
  <tr class="blue">
	<td valign="top" align="left"><h3><?php echo $row_pack['id_pachet']; ?></h3></td>
	<td valign="top" align="left">&nbsp;</td>
    <td valign="top" align="left"><h3><?php echo $row_pack['denumire']; ?></h3></td>
    <td valign="top" align="left" width="85"><h3><?php echo $row_pack['nr_nopti'].' nopti'; ?></h3></td>
    <td valign="top" align="left"><h3><?php echo $row_pack['nume_hotel'].' '.$row_pack['stele_hotel'].' *'; ?></h3></td>
    <td valign="top" align="left" nowrap><h3><?php echo $localitati[$row_pack['id_plecare']].' <span class="red">>>></span> '.$localitati[$row_pack['id_destinatie']]; ?></h3></td>
    <td valign="middle" align="left" style="background:#EFEFEF;"><?php echo $hotel_legat; ?></td>
  </tr>
  <tr>
    <td colspan="7">
      <table width="100%" cellspacing="0" cellpadding="5" border="1" style="border-collapse:collapse; border-color:#666;" class="default">
		<?php
        $sel_packOF = "SELECT * FROM import_pachete_oferte WHERE import_pachete_oferte.id_furnizor = '".$id_furnizor."' AND import_pachete_oferte.id_pachet = '".$row_pack['id_pachet']."' ";
        $que_packOF = mysql_query($sel_packOF);
        while($row_packOF = mysql_fetch_array($que_packOF)) {
			
			$sel_preturi = "SELECT * FROM preturi_christian WHERE id_oferta_pachet = '".$row_packOF['id_oferta_pachet']."' AND id_pachet = '".$row_pack['id_pachet']."' ";
			$que_preturi = mysql_query($sel_preturi);
			if(mysql_num_rows($que_preturi)>0) {
				$prices = '<strong class="green">Exista preturi</strong>';
			} else {
				$prices = '<strong class="red">NU exista preturi</strong>';
			}
			
			$sel_preturi = "SELECT * FROM preturi_autoupdate WHERE id_furnizor = '".$id_furnizor."' AND hotel_code = '".$row_pack['id_hotel']."' AND id_rooms LIKE '%".$row_packOF['id_oferta_pachet']."%' AND id_rooms LIKE '%".$row_pack['id_pachet']."%' GROUP BY id_oferta ";
			$que_preturi = mysql_query($sel_preturi);
			if(mysql_num_rows($que_preturi)>0) {
				$importat = '<strong class="green">Importat:</strong> <img src="images/yes.png">';
			} else {
				$importat = '<strong class="red">Importat:</strong> <img src="images/no.png">';
			}
        ?>
        <tr class="normal">
          <td valign="top" align="left" width="60"><?php echo $row_packOF['id_oferta_pachet']; ?></td>
          <td valign="top" align="left"><?php echo $row_packOF['Label'].' - '.$row_packOF['Description']; ?></td>
          <td valign="top" align="left" width="230"><?php echo '<strong>Valabil intre:</strong> '.$row_packOF['ValidFrom'].' - '.$row_packOF['ValidTo']; ?></td>
          <td valign="top" align="left" width="280"><?php if($row_packOF['TravelFrom']!=NULL or $row_packOF['TravelFrom']!='0000-00-00') echo '<strong>Excursia are loc intre:</strong> '.$row_packOF['TravelFrom'].' - '.$row_packOF['TravelTo']; ?></td>
          <td valign="top" align="left" width="100"><?php echo $prices; ?></td>
          <td valign="top" align="left" width="70"><?php echo $importat; ?></td>
          <td valign="top" align="left" width="110">Oferta: 
            <?php while($row_preturi = mysql_fetch_array($que_preturi)) {
				$sel_avail = "SELECT valabila FROM oferte WHERE id_oferta = '".$row_preturi['id_oferta']."' ";
				$que_avail = mysql_query($sel_avail);
				$row_avail = mysql_fetch_array($que_avail);
				if($row_avail['valabila']=='da') {
					$avail = 'yes';
					$title = 'title="Oferta activa"';
				} elseif($row_avail['valabila']=='nu') {
					$avail = 'no';
					$title = 'title="Oferta Inactiva"';
				}
			?>
            <a href="editare_sejur.php?pas=2&oferta=<?php echo $row_preturi['id_oferta']; ?>" target="_blank" class="link" <?php echo $title; ?>><strong><?php echo $row_preturi['id_oferta']; ?></strong> <img src="images/<?php echo $avail; ?>.png"></a><br>
            <?php } ?>
        </tr>
        <?php } ?>
      </table>
      <br><br>
    </td>
  </tr>
<?php } ?>
</table>

<?php } ?>