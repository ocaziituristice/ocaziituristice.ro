<tr>
  <td align="left" colspan="2"><h2>Clima (medii anuale)</h2></td>
</tr>
<tr>
  <th valign="top" align="left">Link vreme:</th>
  <td><input type="text" name="param[link_vreme]" class="mare" value="<?php echo $param['link_vreme']; ?>"></td>
</tr>
<tr>
  <td align="left" colspan="2">
    <table width="1000" cellpadding="2" cellspacing="0" border="1" bordercolor="#999" bgcolor="#FFF" style="border-collapse:collapse;">
      <tr>
        <th>Luna:</th>
        <th>Ian</th>
        <th>Feb</th>
        <th>Mar</th>
        <th>Apr</th>
        <th>Mai</th>
        <th>Iun</th>
        <th>Iul</th>
        <th>Aug</th>
        <th>Sep</th>
        <th>Oct</th>
        <th>Noi</th>
        <th>Dec</th>
      </tr>
      <tr>
        <td><strong>Temperatura Maxima (&deg;C)</strong></td>
        <?php for($i=0; $i<12; $i++) { ?>
        <td><input type="text" name="param[temp][max][<?php echo $i; ?>]" value="<?php echo $param['temp']['max'][$i]; ?>" class="mic"></td>
        <?php } ?>
      </tr>
      <tr>
        <td><strong>Temperatura Minima (&deg;C)</strong></td>
        <?php for($i=0; $i<12; $i++) { ?>
        <td><input type="text" name="param[temp][min][<?php echo $i; ?>]" value="<?php echo $param['temp']['min'][$i]; ?>" class="mic"></td>
        <?php } ?>
      </tr>
      <tr>
        <td><strong>Temperatura Apei (&deg;C)</strong></td>
        <?php for($i=0; $i<12; $i++) { ?>
        <td><input type="text" name="param[temp][apa][<?php echo $i; ?>]" value="<?php echo $param['temp']['apa'][$i]; ?>" class="mic"></td>
        <?php } ?>
      </tr>
      <tr>
        <td><strong>Precipitatii (nr. zile)</strong></td>
        <?php for($i=0; $i<12; $i++) { ?>
        <td><input type="text" name="param[temp][precipitatii][<?php echo $i; ?>]" value="<?php echo $param['temp']['precipitatii'][$i]; ?>" class="mic"></td>
        <?php } ?>
      </tr>
    </table>
  </td>
</tr>
