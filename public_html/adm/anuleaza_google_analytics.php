<?php session_start(); include_once('restrictionare.php'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>
<body>

<?php
$id_rez = $_GET['idc'];

echo $sel_anulare = "SELECT
cerere_rezervare.id_oferta,
cerere_rezervare.comision,
cerere_rezervare_grad_ocupare.pret,
cerere_rezervare_grad_ocupare.moneda,
cerere_rezervare_date_facturare.oras,
oferte.id_hotel as id_hotel,
oferte.denumire,
oferte.denumire_scurta
FROM cerere_rezervare
INNER JOIN oferte ON oferte.id_oferta = cerere_rezervare.id_oferta
INNER JOIN cerere_rezervare_tip_camera ON cerere_rezervare_tip_camera.id_cerere = cerere_rezervare.id_cerere
INNER JOIN cerere_rezervare_grad_ocupare ON cerere_rezervare_grad_ocupare.id_camera = cerere_rezervare_tip_camera.id_camera
INNER JOIN cerere_rezervare_date_facturare ON cerere_rezervare_date_facturare.id_cerere = cerere_rezervare.id_cerere
WHERE cerere_rezervare.tip = 'rezervare'
AND cerere_rezervare.id_cerere = '".$id_rez."'
 ";
$que_anulare = mysql_query($sel_anulare);
$row_anulare = mysql_fetch_array($que_anulare);

if(mysql_num_rows($que_anulare)) {
	$pret_total_euro = final_price_euro(final_price_lei($row_anulare['pret'], $row_anulare['moneda']));
	$comision = $row_anulare['comision'];
	$oras = $row_anulare['oras'];
	$id_oferta = $row_anulare['id_oferta'];
	$id_hotel = $row_anulare['id_hotel'];
	$denumire = $row_anulare['denumire'];
	$denumire_scurta = $row_anulare['denumire_scurta'];
	
	$upd_extra = "UPDATE cerere_rezervare SET anulare_google = 'da' WHERE id_cerere = '".$id_rez."' ";
	$que_extra = mysql_query($upd_extra) or die(mysql_error());
?>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27001687-1', 'auto');
  ga('send', 'pageview');
  ga('require', 'displayfeatures');
     ga('set','dimension6','Admin');
	
//ga('require', 'ec');


// Refund an entire transaction.
<?php /*?>ga('ecommerce:setAction', 'refund', {
  'id':'<?php echo  $id_rez; ?>'    // Transaction ID is only required field for full refund.
});<?php */?>
ga('send', 'pageview');

</script>

<script>	
 ga('require', 'ecommerce', 'ecommerce.js');
  ga('ecommerce:addTransaction', {
	'id': '<?php echo  $id_rez; ?>', // Transaction ID. Required
	'affiliation': 'ocaziituristice.ro', // Affiliation or store name
	'revenue': '-<?php echo $comision; ?>', // Total general
	'shipping': '', // Expediere
	'tax': '' // Taxă
  });
  ga('ecommerce:addItem', {
	'id': '<?php echo $id_rez; ?>', // Codul tranzacției. Obligatoriu
	'name': '<?php echo $denumire; ?>', // Nume de produs. Obligatoriu
	'sku': '<?php echo $id_hotel; ?>', // UGS/cod
	'category': '<?php echo $denumire_scurta; ?>', // Categorie sau variantă
	'price': '<?php echo $comision; ?>', // Preț unitate
	'quantity': '-1' // Cantitate
  });
  ga('ecommerce:send');
  </script>

<?php } ?>

<div style="text-align:center">
  <h2>Anularea a fost trimisa si la Google Analytics</h2>
  <br>
  <input type="button" value="Inchide fereastra" onClick="window.close()" class="buttons">
</div>

</body>
</html>