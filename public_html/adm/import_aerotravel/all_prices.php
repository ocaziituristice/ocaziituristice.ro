<h2>Alegeti un fisier pentru a efectua uploadul</h2>
<br>
<form action="" method="post">
<?php
$files = glob($_SERVER['DOCUMENT_ROOT']."/includes/import_aida_aerotravel/*.xml", 1);
//echo '<pre>';print_r($files);echo '</pre>';
foreach($files as $v_files) {
	echo '<label style="display:block; padding:5px;"><input type="radio" name="file" value="'.basename($v_files).'"> '.basename($v_files).' - <strong class="red">'.round((filesize($v_files)/(1024*1024)),2).' MB</strong>';
	if($_SESSION['import_aerotravel']['prices'][basename($v_files)]) echo ' - <strong>Data import: '.$_SESSION['import_aerotravel']['prices'][basename($v_files)].'</strong>';
	echo '</label>';
}
?>
	<br>
	<input type="submit" value="Importa" class="buttons">
</form>
<?php
if($_REQUEST['file']) {
	$_SESSION['import_aerotravel']['prices'][$_REQUEST['file']] = date("d.m.Y H:i:s");

	$sUrl = $sitepath.'includes/import_aida_aerotravel/'.$_REQUEST['file'];
	//$sUrl = $sitepath.'test/aerotravel2.xml';
	
	$prices = new XMLReader();
	$prices->open($sUrl);
	
	/*mysql_query("TRUNCATE TABLE preturi_aerotravel") or die(mysql_error());*/
	
	$err = 0;
	
	while($prices->read()) {
		if($prices->nodeType == XMLReader::ELEMENT && $prices->name == 'Package') {
			$doc = new DOMDocument('1.0', 'UTF-8');
			$xml = simplexml_import_dom($doc->importNode($prices->expand(),true));
			
			$Package_ID = (string) $xml['ID'];
			$Package_Type = (string) $xml['Type'];
			$Package_Name = (string) $xml['Name'];
			$Package_Currency = (string) $xml['Currency'];
			$Package_Code = (string) $xml['Code'];
			$Project_ID = (string) $xml->Project['ID'];
			$Project_Name = (string) $xml->Project['Name'];
			
			if(sizeof($xml->PackEntries)>0) {
				foreach($xml->PackEntries->PackEntry as $k_pack => $v_pack) {
					$PackEntry_ID = (string) $v_pack['ID'];
					$PackEntry_DateStart = (string) $v_pack['DateStart'];
					$PackEntry_DateEnd = (string) $v_pack['DateEnd'];
					$PackEntry_nopti = (string) $v_pack->Duration;
					
					/*if(sizeof($v_pack->ExtraServices)>0) {*/
						
						if(sizeof($v_pack->PackConfigurations)>0) {
							$ins_sql_prices[$k_pack] = array();
							
							/*$del_extra = "DELETE FROM preturi_aerotravel WHERE Package_ID = '".$Package_ID."' AND PackEntry_ID = '".$PackEntry_ID."' ";
							$que_del_extra = mysql_query($del_extra) or die(mysql_error());
							@mysql_free_result($que_del_extra);*/
							
							foreach($v_pack->PackConfigurations->Configuration as $k_combo => $v_combo) {
								
								$nr_adulti = 0;
								$nr_copii = 0;
								$copil1 = '';
								$copil2 = '';
								$copil3 = '';
								$ages = '';
								foreach($v_combo->Occupancy->GuestCount as $k_occ => $v_occ) {
									if((string) $v_occ['AgeQualifyingCode']=='a') {
										$nr_adulti += (string) $v_occ['Count'];
									} else if(((string) $v_occ['AgeQualifyingCode']=='c') or ((string) $v_occ['AgeQualifyingCode']=='i')) {
										$nr_copii += (string) $v_occ['Count'];
										for($i=0; $i<(string) $v_occ['Count']; $i++) {
											$ages .= (string) $v_occ['MinAge'].'-'.(string) $v_occ['MaxAge'].';';
										}
									}
								}
								$varste = substr($ages, 0, -1);
								$varste = explode(';', $varste);
								$copil1 = $varste[0];
								$copil2 = $varste[1];
								$copil3 = $varste[2];
								//print "<pre>";print_r($v_combo);print "</pre>";

								
								//$key_pret = array_search("spo", $preturi[$k_combo]['type']);
								if($key_pret !== NULL) {
									$pret = $preturi[$k_combo]['pret'][$key_pret];
									$Rate_ID = $preturi[$k_combo]['rateID'][$key_pret];
								} else {
									$pret = $preturi[$k_combo]['pret'][0];
									$Rate_ID = $preturi[$k_combo]['rateID'][0];
								}
								//$pret = $v_combo->Price['Ammount'];
								$moneda = (string) $v_combo->Price['CurrencyCode'];
								
								if((string) $v_combo->Transportation['FlightNo'] != '') {
									$transport = 'avion';
									$plecare = (string) $v_combo->Transportation[0]->Departure->City['Name'];
									$Accommodation_Name = (string) $v_combo->Accommodation['Name'];
								} else if(preg_match("/\bcharter\b/i", (string) $v_combo->Transportation['Name']) or preg_match("/\bavion\b/i", (string) $v_combo->Transportation['Name'])) {
									$transport = 'avion';
									$plecare = (string) $v_combo->Transportation[0]->Departure->City['Name'];
									$Accommodation_Name = (string) $v_combo->Accommodation['Name'];
								} else {
									$plecare = '';
									$hName = explode(" - ", (string) $v_combo->Accommodation['Name']);
									$transport = $hName[1];
									$Accommodation_Name = $hName[0];
								}
								
								$Accommodation_UnitID = (string) $v_combo->Accommodation['UnitID'];
								$Accommodation_Stars = (string) $v_combo->Accommodation['Stars'];
								$Accommodation_RoomID = (string) $v_combo->Accommodation['RoomID'];
								$Accommodation_RoomType_Code = (string) $v_combo->Accommodation->RoomType['Code'];
								
								$roomFeature = explode("/", (string) $v_combo->Accommodation->RoomFeature);
								$Accommodation_RoomFeature = (string) $v_combo->Accommodation->RoomType." - ".(string) $v_combo->Accommodation->RoomTypeCategory;
								$masa = trim($roomFeature[1]);
	
								$Accommodation_City_Name = (string) $v_combo->Accommodation->City['Name'];
								$Accommodation_City_ID = (string) $v_combo->Accommodation->City['ID'];
								
								$preturi[$k_combo] = array();
								foreach($v_combo->Rates->Rate as $k_rate => $v_rate) {
									if((string) $v_rate['DateStart']<=date("Y-m-d") and (string) $v_rate['DateEnd']>=date("Y-m-d")) {
										$preturi[$k_combo]['type'][] = (string) $v_rate['Type'];
										$preturi[$k_combo]['pret'] = (string) $v_rate->Price['Ammount'];
										$preturi[$k_combo]['rateID'] = (string) $v_rate['ID'];
								
								
								$ins_sql_prices[$k_pack] = '("'.$id_furnizor.'", "'.$Package_ID.'", "'.$Package_Type.'", "'.$Package_Name.'", "'.$Package_Code.'", "'.$Project_ID.'", "'.$Project_Name.'", "'.$PackEntry_ID.'", "'.$PackEntry_DateStart.'", "'.$PackEntry_DateEnd.'", "'.$PackEntry_nopti.'", "'.$plecare.'", "'.$transport.'", "'.$masa.'", "'.$nr_adulti.'", "'.$nr_copii.'", "'.$copil1.'", "'.$copil2.'", "'.$copil3.'", "'.$preturi[$k_combo]['pret'].'", "'.$moneda.'", "'.$Accommodation_UnitID.'", "'.$Accommodation_Name.'", "'.$Accommodation_Stars.'", "'.$Accommodation_RoomID.'", "'.$Accommodation_RoomType_Code.'", "'.$Accommodation_RoomFeature.'", "'.$Accommodation_City_Name.'", "'.$Accommodation_City_ID.'", "'.$preturi[$k_combo]['rateID'].'")';
								$ins_prices = "INSERT INTO import_aidatemp_preturi (id_furnizor, Package_ID, Package_Type, Package_Name, Package_Code, Project_ID, Project_Name, PackEntry_ID, data_start, data_end, nr_nopti, plecare, transport, masa, nr_adulti, nr_copii, copil1, copil2, copil3, pret, moneda, Accommodation_UnitID, Accommodation_Name, Accommodation_Stars, Accommodation_RoomID, Accommodation_RoomType_Code, Accommodation_RoomFeature, Accommodation_City_Name, Accommodation_City_ID, Rate_ID) VALUES ".$ins_sql_prices[$k_pack]." ";
								$que_prices = mysql_query($ins_prices) or die(mysql_error());
								@mysql_free_result($que_prices);	
								}
								}
							}
						}
						
					/*} else {
						echo '<p>Nu exista <strong>ExtraServices</strong>. Pachetul '.$PackEntry_ID.' nu a fost introdus!</p>';
					}*/
				}
			}
			$err += 1;
		}
	}
	
	if($err>0) {
		echo '<script>alert("Preturile au fost updatate cu succes."); document.location.href="/adm/import_aerotravel.php?pas=all_prices";</script>';
	} else {
		echo '<br><br><h1>Nu sunt preturi disponibile!</h1>';
		echo '<h2>Se va reveni la aceasta pagina automat in <span id="seconds" class="red">3</span></h2>';
		echo "<script>var seconds = 3; setInterval(function(){if (seconds <= 1) { window.location = '/adm/import_aerotravel.php?pas=all_prices'; } else { document.getElementById('seconds').innerHTML = --seconds; } }, 1000);</script>";
	}
}
?>
