<?php

$apiData = array(
    'username'          => $_SESSION['mail'],
    'token'             => $_SESSION['api_smart_bill'],
);
$serie_document['proforma']='OCZP';
$serie_document['factura']='OCZWEB';

$serie_document['intern']['factura']='OCZ-ROM';
$serie_document['extern']['factura']='OCZ-EXT';

$serie_document['chitanta']='OCZAG';
include_once 'class.Array2XML.php';

/**
 * BETA !!!
 */
class SmartBillCloud
{
    const VERSION               = '1.0.0';

    const SSL_CERT_URL          = 'ssl://ws.smartbill.ro:8182';
    const CERT_FILENAME         = 'dwl-ws-test.crt';
    const PUBLIC_KEY_FILENAME   = 'sbc_public_key.pem';
    const DOMAIN_VALID          = 'cloud.smartbill.ro';

    const INVOICE_URL           = 'https://ws.smartbill.ro:8183/SBORO/api/invoice';
    const PROFORMA_URL          = 'https://ws.smartbill.ro:8183/SBORO/api/estimate';
    const INVOICE_PDF_URL 		= 'https://ws.smartbill.ro:8183/SBORO/api/invoice/pdf?cif=%s&seriesname=%s&number=%s';
    const PROFORMA_PDF_URL 		= 'https://ws.smartbill.ro:8183/SBORO/api/estimate/pdf?cif=%s&seriesname=%s&number=%s';
	const PAYMENT_URL 			='https://ws.smartbill.ro:8183/SBORO/api/payment';

    const DEFAULT_LOGIN_ERROR   = 'Autentificare esuata. Va rugam verificati datele si incercati din nou.';
    const DEFAULT_DOCUMENT_ERROR= 'Crearea documentului a esuat. Va rugam verificati structura de date si incercati din nou.';
    const DEFAULT_PDF_ERROR 	= 'Obtinerea PDF-ului asocial documentului a esuat.';
    const DEFAULT_COMM_ERROR    = 'A intervenit o eroare la comunicarea cu Smart Bill Cloud. Va rugam verificati datele de conectare / reincercati o noua autentificare cu datele existente.';

    var $user   = '';
    var $token  = '';
    var $doc    = null;

    /**
      *  @param string $user
      *  @param string $token
      */
    function __construct($user, $token) {
        // set user
        $this->user = $user;
        // set token
        $this->token = $token;
    }

    public function createNewDocument($data) {
        try {
            foreach ($data as $key => &$value) {
               //echo $key;
			    switch ($key) {
					
					
                    case 'client':
                        $this->_sanitizeClient($value);
                        break;
                    
                    case 'products':
                        $this->_sanitizeProducts($value);
                        break;
                }
            }
            $this->doc = $data;
            $this->_sanitizeDocument();
        } catch(Exception $e) {
            return false;
        }

        return true;
    }
    private function _sanitizeDocument() {
        if (empty($this->doc['isDraft'])) {
            try {
            	$this->doc['number'] = '';
            	//$this->doc['estimate']['number'] = '';
            } catch(Exception $e) {}        	
        }
    }
    private function _sanitizeClient(&$data) {
        try {
            $data['vatCode'] = trim($data['vatCode']);
            if (empty($data['vatCode'])) {
                $data['vatCode'] = '-';
            }
        } catch(Exception $e) {}
    }
    private function _sanitizeProducts(&$data) {
        // if (!is_array($data)) return false;

        // foreach ($data as $key => $value) {
        // }
    }
	
	 private function _sanitizePayment(&$data) {
        // if (!is_array($data)) return false;

        // foreach ($data as $key => $value) {
        // }
    }


    // TODO: test (DO NOT USE)
    public function updateDocument($data) {
        try {
            $this->_updateObject($this->doc, $data);
        } catch(Exception $e) {
            return false;
        }

        return true;
    }

    // TODO: test (DO NOT USE)
    public function updateDocumentRecord($data, $recordID) {
        try {
            $this->doc['products'][$recordID] = $data;
        } catch(Exception $e) {
            return false;
        }

        return true;
    }

    // TODO: test (DO NOT USE)
    public function removeDocumentRecord($recordID) {
        try {
            unset($this->doc['products'][$recordID]);
            $this->doc['products'] = array_values($this->doc['products']);
        } catch(Exception $e) {
            return false;
        }

        return true;
    }

    // TODO: finish implementation
    public function updateDocumentTransport($data) {

    }

    /**
     * Send the document to the server
     */
    public function sendDocument() {
     $apiURL 	= $this->_getDocumentAPIurl();
     $docType	= $this->_getDocumentType();
    $xmlData  	= self::_createXML($this->doc, $docType);
    	$response 	= self::_curl($apiURL, $xmlData, array("Content-Type: application/xml", "Authorization: Basic ".$this->getAuthorization()));
    $document 	= self::_decodeDocumentResponse($response);
       //print_r($document) ;	
        return $document;
    }    
    private function _getDocumentType() {
        switch ($this->doc['type']) {
             case 'Chitanta':
                $type = 'payment';
                break;
			case 'Ordin plata':
                $type = 'payment';
                break;
			case 'n':
                $type = 'invoice';
                break;
            
            default:
                $type = 'estimate';
                break;
        }

        return $type;    	
    }
    private function _getDocumentAPIurl() {
        switch ($this->doc['type']) {
            case 'Ordin plata':
                $apiURL = self::PAYMENT_URL;
                break;
            
		    case 'n':
                $apiURL = self::INVOICE_URL;
                break;
            
            default:
                $apiURL = self::PROFORMA_URL;
                break;
        }

        return $apiURL;    	
    }
    private function _createXML($data, $rootNode) {
        /*
        $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><$rootNode></$rootNode>");
        array_walk_recursive($data, array($xml, 'addChild'));
        
        return $xml->asXML();
        */
        $xml = Array2XML::createXML($rootNode, $data);
        $xml = str_replace(array('<products>', '</products>'), '', $xml->saveXML());

        return $xml;
    }    
    private function _decodeDocumentResponse($response) {
    	$docNumber = false;

    	$response = json_decode($response);
    	if (!empty($response)) {
    		$docNumber = $response->number;
    	}    	
$docNumber;
    	 return $response;
    }

    /**
     * Returns the PDF for the existing or specified document
     */
    public function getDocumentPDF($docNumber, $companyVAT='', $docSeriesName='',$tip_document) {
    	// sanitize
    	$docNumber	= urlencode(trim($docNumber));
    	$VATno 		= empty($this->doc['companyVatCode']) ? $companyVAT : $this->doc['companyVatCode'];
    	$VATno		= urlencode(trim($VATno));
    	$docSeries 	= empty($this->doc['seriesName']) ? $docSeriesName : $this->doc['seriesName'];
    	$docSeries	= urlencode(trim($docSeries));
    	$type=trim($tip_document);
		$apiURL 	= $this->_getPDFDocumentAPIurl($VATno, $docSeries, $docNumber,$type);
    	
		$response 	= self::_curl($apiURL, null, array("Content-Type: application/xml","Accept:application/octet-stream","Authorization: Basic ".$this->getAuthorization()));

		return $response;
    }
    private function _getPDFDocumentAPIurl($VATno, $docSeries, $docNumber,$tip_document) {
        
	if($tip_document=='factura'){echo $doc_type='n';}
		switch ($doc_type) {
 			case 'n':
                $apiURL = sprintf(self::INVOICE_PDF_URL, $VATno, $docSeries, $docNumber);
                break;
            
            default:
                $apiURL = sprintf(self::PROFORMA_PDF_URL, $VATno, $docSeries, $docNumber);
                break;
        }

        return $apiURL;    	
    }
    private function _curl($url, $data=null, $httpHeaders=null) {
        if (empty($url))   return FALSE;

        // $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        //curl_setopt($ch, CURLOPT_MUTE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml"));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            // $tempFilePath = self::_saveDataToTempFile($data);
            // if (!empty($tempFilePath)) {
            // 	curl_setopt($ch, CURLOPT_POSTFIELDS, '@'.$tempFilePath);
            // } else {
            // 	return FALSE;
            // }
        }

        if (!empty($httpHeaders)
         && is_array($httpHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        }

        $return = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // @unlink($tempFilePath);
//echo $return;
        if ($http_status!=200) {
            $errorMessage = $return;
			
//echo $url;
//echo $data;
print_r($errorMessage);die();

            try {
                $returnArray = self::_convertResponse($return);
                $errorMessage = $returnArray['errorText'];
            } catch (Exception $e) {}

            $http_status = empty($http_status) ? '' : $http_status;
            $errorMessage = empty($errorMessage) ? self::DEFAULT_COMM_ERROR : $errorMessage;
            throw new Exception($errorMessage);

            // empty response
            $return = '';
        }

        return $return;
    }
    /**
     * Take XML content and convert
     * if to a PHP array.
     * @param string $xml Raw XML data.
     * @param string $main_heading If there is a primary heading within the XML that you only want the array for.
     * @return array XML data in array format.
     */
    private function _convertResponse($xml, $main_heading = '') {
        $deXml = simplexml_load_string($xml);
        $deJson = json_encode($deXml);
        $xml_array = json_decode($deJson,TRUE);
        if (! empty($main_heading)) {
            $returned = $xml_array[$main_heading];
            return $returned;
        } else {
            return $xml_array;
        }
    }    
   	private function getAuthorization() {
        return base64_encode($this->user.':'.$this->token);
    }
	// private function _saveDataToTempFile($data) {
	// 	$filePath = tempnam(dirname(__FILE__).DIRECTORY_SEPARATOR, 'xml_');
	// 	if (!empty($filePath)) {
	// 		file_put_contents($filePath, $data);
	// 	}

	// 	return $filePath;
	// }        
}