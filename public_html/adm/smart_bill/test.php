<?php

include_once 'class.SmartBillCloud.SDK.php';

// API credentials

$sbc = new SmartBillCloud($apiData['username'], $apiData['token']);

// create document
$clientData = array(
	'vatCode'    => '',
	'name'       => 'client name',
	'code'       => '',
	'address'    => 'client address',
	'regCom'     => '',
	'isTaxPayer' => false,
	'contact'    => 'contact person name',
	'phone'      => '0720123456',
	'city'       => 'city',
	'county'     => 'county',
	'country'    => 'RO',
	'email'      => 'email@company.com',
	'bank'       => '',
	'iban'       => '',
	'saveToDb'   => false, // or true, if the client should be saved in Smart Bill Cloud
);
$productData = array(
    'code'                      => 'codd',
    'currency'                  => 'RON', // or any "symbol" from $settings->companies[0]->currencies
    'exchangeRate'              => 1,
    'isDiscount'                => false,
    'discountPercentage'        => (float)0,
    'discountValue'             => -(float)0,
    'discountType'              => 1, // 1 - for value; 2 - for percent 
    'isTaxIncluded'             => true,
    'measuringUnitName'         => 'buc', // or any "symbol" from $settings->companies[0]->measureUnits
    'translatedMeasuringUnit'   => '',
    'name'                      => 'product name',
    'translatedName'            => '',
    'price'                     => (float)124,
    'quantity'                  => 1,
    'saveToDb'                  => false, // or true, if the client should be saved in Smart Bill Cloud
    'taxName'                   => '', // or empty ???
    'taxPercentage'             => (float)24,
    'translatedMeasuringUnit'   => '',
    'translatedName'            => '',
);

$productData2 = $productData;
$productData2['isDiscount'] = true;
$productData2['name']       = 'discount "product name"';
$productData2['price']      = (float)0;
$productData2['discountValue'] = -(float)10;

$productData3 = $productData;
$productData3['code']       = 'SKU00002';
$productData3['name']       = 'product name 2';
$productData3['price']      = (float)200;

$transportData = $productData;
$transportData['code']      = 'shipping';
$transportData['name']      = 'Transport';
$transportData['quantity']  = 1;
$transportData['price']     = 10;
$transportData['measuringUnitName']  = 'buc';

$products = array(
    $productData,
    // $productData2,
    // $productData3,
    // $transportData,
);
$companyID = 1;
$documentData = array(
    'companyVatCode'       => '29259993',
    'client'               => $clientData,
    'isDraft'              => false, 										// false for the real document to be generated
    'issueDate'            => date('Y-m-d'), 								// or set a differend date but keep the format: YYYY-MM-DD
    'seriesName'           => 'OCZP',     // for proforma use "estimateSeries" property
    'number'               => '1',
    'type'                 => '', 											// for proforma use "" (empty)
    'currency'             => 'RON', 										// or any "symbol" from $settings->companies[0]->currencies
    'exchangeRate'         => 1,
    'language'             => 'RO',
    'precision'            => 2,
    'issuerName'           => '',
    'issuerCnp'            => '',
    'aviz'                 => '',
    'dueDate'              => '',
    'mentions'             => 'se pune jos la info',                                           // document comments
    'observations'         => '',											// document observations
    'delegateAuto'         => '',
    'delegateIdentityCard' => '',
    'delegateName'         => '',
    'deliveryDate'         => '',
    'paymentDate'          => '',
    'usePaymentTax'        => (bool)true,
    'paymentBase'          => 1,
    'colectedTax'          => 0,
    'orderNumber'          => '',			                        // order ID
    'trackingNumber'       => '', 											// AWB number
    'estimate'             => array(
        'seriesName' => '',
        'number'     => '',
    ),
    'products'             => array(
        'product' => $products,
    ),
    'paymentTotal'         => (float)124,
);

try {
    $sbc->createNewDocument($documentData);
	//echo 'document data1: ',print_r($sbc->doc, true);
} catch(Exception $e) {
    die($e->getMessage());
}


// separator
// echo '<br>',"\r\n";
// echo '<br>',"\r\n";

// create invoice
/*try {
    $docNumber = $sbc->sendDocument();
    if (empty($docNumber)) {
        die($sbc::DEFAULT_DOCUMENT_ERROR);
    } else {
        echo 'document factura SBC: #',$docNumber;
    }
} catch(Exception $e) {
    die($e->getMessage());
}
// separator
echo '<br>',"\r\n";
// get PDF and save to disk
try {
    $pdfData = $sbc->getDocumentPDF($docNumber);
    if (empty($pdfData)) {
        die($sbc::DEFAULT_PDF_ERROR);
    } else {
        $pdfFile = $documentData['seriesName'].$docNumber.'.pdf';
        @file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.$pdfFile, $pdfData);
        echo 'document PDF: ',$pdfFile;
    }
} catch(Exception $e) {
    die($e->getMessage());
}*/

// separator
echo '<br>',"\r\n";

$documentData['type'] = '';
$documentData['seriesName'] = 'OCZP';
try {
    $sbc->createNewDocument($documentData);
	//echo 'document data1: ',print_r($sbc->doc, true);
} catch(Exception $e) {
    die($e->getMessage());
}
 // echo 'document data1: ',print_r($sbc->doc, true);
// create proforma

try {
    $docNumber = $sbc->sendDocument();
	//echo 'document data1: ',print_r($sbc->doc, true);
    if (empty($docNumber)) {
        die($sbc::DEFAULT_DOCUMENT_ERROR);
    } else {
        //echo 'document proforma SBC: #',$docNumber;
		$nr_document=$docNumber;
    }
} 
catch(Exception $e) {
    die($e->getMessage());
}
// separator
echo '<br>',"\r\n";
// get PDF and save to disk
try {
    $pdfData = $sbc->getDocumentPDF($docNumber);
    if (empty($pdfData)) {
        die($sbc::DEFAULT_PDF_ERROR);
    } else {
        $pdfFile = $documentData['seriesName'].$docNumber.'.pdf';
        @file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.$pdfFile, $pdfData);
        echo 'document PDF: ',$pdfFile;
    }
} catch(Exception $e) {
    die($e->getMessage());
}