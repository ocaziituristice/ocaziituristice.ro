<?php
$url='https://ws.smartbill.ro:8183/SBORO/api/estimate'; 

 $user='razvan@ocaziituritice.ro';
    $token='93f8e2d001367867f8fcc74c98049904';

	
	echo $autentificare=base64_encode($user.':'.$token);
	
	$httpHeaders=array("Content-Type: application/xml", "Authorization: Basic ".$autentificare);	
$data='<estimate>
  <companyVatCode>29259993</companyVatCode>
  <client>
    <name>Intelligent IT</name>
    <vatCode>RO12345678</vatCode>
    <address>str. Sperantei, nr. 5</address>
    <isTaxPayer>false</isTaxPayer>
    <city>Sibiu</city>
    <county>Sibiu</county>
    <country>Romania</country>
    <saveToDb>false</saveToDb>
  </client>
  <isDraft>false</isDraft>
  <issueDate>2016-03-10</issueDate>
  <seriesName>PRF</seriesName>
  <currency>RON</currency>
  <language>RO</language>
  <precision>2</precision>
  <dueDate>2016-03-28</dueDate>
  <product>
    <name>Mapa A4</name>
    <code>ccd1</code>
    <productDescription>produse de papetarie</productDescription>
    <isDiscount>false</isDiscount>
    <measuringUnitName>buc</measuringUnitName>
    <currency>RON</currency>
    <quantity>2</quantity>
    <price>40</price>
    <isTaxIncluded>true</isTaxIncluded>
    <taxName>Normala</taxName>
    <taxPercentage>20</taxPercentage>
    <saveToDb>false</saveToDb>
    <isService>false</isService>
  </product>
  <product>
    <name>Biblioraft Plastifiat</name>
    <code>ccd2</code>
    <productDescription>produse de papetarie</productDescription>
    <isDiscount>false</isDiscount>
    <measuringUnitName>buc</measuringUnitName>
    <currency>RON</currency>
    <quantity>3</quantity>
    <price>60</price>
    <isTaxIncluded>true</isTaxIncluded>
    <taxName>Normala</taxName>
    <taxPercentage>20</taxPercentage>
    <saveToDb>false</saveToDb>
    <isService>false</isService>
  </product>
  <product>
    <name>Discount valoric pe produsul 2</name>
    <isDiscount>true</isDiscount>
    <numberOfItems>1</numberOfItems>
    <measuringUnitName>buc</measuringUnitName>
    <currency>RON</currency>
    <isTaxIncluded>true</isTaxIncluded>
    <taxName>Normala</taxName>
    <taxPercentage>20</taxPercentage>
    <discountType>1</discountType>
    <discountValue>-15</discountValue>
  </product>
  <product>
    <name>Discount procentual pe produsul 1 si 2</name>
    <isDiscount>true</isDiscount>
    <numberOfItems>2</numberOfItems>
    <measuringUnitName>buc</measuringUnitName>
    <currency>RON</currency>
    <isTaxIncluded>true</isTaxIncluded>
    <taxName>Normala</taxName>
    <taxPercentage>20</taxPercentage>
    <discountType>2</discountType>
    <discountPercentage>10</discountPercentage>
  </product>
</estimate>';
 $ch = curl_init($url);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
 curl_setopt($ch, CURLOPT_HEADER, 0);
 curl_setopt($ch, CURLOPT_VERBOSE, 0);
 curl_setopt($ch, CURLOPT_TIMEOUT, 30);
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 //curl_setopt($ch, CURLOPT_CAINFO, $this->getCertificatePath());
 //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml"));
 curl_setopt($ch, CURLOPT_POST, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
 

       echo $return = curl_exec($ch);
       echo $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
            // $tempFilePath = self::_saveDataToTempFile($data)
?>