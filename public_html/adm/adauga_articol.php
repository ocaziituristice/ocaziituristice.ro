<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php'); ?>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/editor_load.js"></script>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
<?php //centru_______________________________________ ?>
<h1>Adauga Articol</h1>
<br /><br />
<?php if($_POST['adauga']) {
	$param = $_POST['param'];
	
	if(strlen(trim($param['denumire']))>3) {
		$den = trim($param['denumire']);
		include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'class/articole.php');
		$nume = fa_link($den);
		$add_t = new ARTICOLE;
		$add_t->adaugare($param);
	} else $mesaj = 'Denumirea este prea scurta!';
} ?>
<form name="adauga" enctype="multipart/form-data"  method="post" action="" >
<?php $valut_but_submit='Adauga'; include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'articole/tabel.php'); ?>
</form>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>