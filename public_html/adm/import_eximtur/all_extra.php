<h2>Alegeti un fisier pentru a efectua uploadul de la eximtur</h2>
<br>
<form action="" method="post">
<?php
$files = glob($_SERVER['DOCUMENT_ROOT']."/includes/import_aida_eximtur/*.xml", 1);
//echo '<pre>';print_r($files);echo '</pre>';
foreach($files as $v_files) {
	echo '<label style="display:block; padding:5px;"><input type="radio" name="file" value="'.basename($v_files).'"> '.basename($v_files).' - <strong class="red">'.round((filesize($v_files)/(1024*1024)),2).' MB</strong>';
	if($_SESSION['import_eximtur']['extra'][basename($v_files)]) echo ' - <strong>Data import: '.$_SESSION['import_eximtur']['extra'][basename($v_files)].'</strong>';
	echo '</label>';
}
?>
	<br>
	<input type="submit" value="Importa" class="buttons">
</form>
<?php
if($_REQUEST['file']) {
	$_SESSION['import_eximtur']['extra'][$_REQUEST['file']] = date("d.m.Y H:i:s");

	$sUrl = $sitepath.'includes/import_aida_eximtur/'.$_REQUEST['file'];
	//$sUrl = $sitepath.'test/transilvania.xml';
	
	$prices = new XMLReader();
	$prices->open($sUrl);
	
	/*mysql_query("TRUNCATE TABLE import_transilvania_extra") or die(mysql_error());*/
	
	$err = 0;
	
	while($prices->read()) {
		if($prices->nodeType == XMLReader::ELEMENT && $prices->name == 'Package') {
			$doc = new DOMDocument('1.0', 'UTF-8');
			$xml = simplexml_import_dom($doc->importNode($prices->expand(),true));

			$Package_ID = (string) $xml['ID'];
			$Package_Type = (string) $xml['Type'];
			$Package_Name = (string) $xml['Name'];
			$Package_Currency = (string) $xml['Currency'];
			$Package_Code = (string) $xml['Code'];

			if(sizeof($xml->PackEntries)>0) {
				foreach($xml->PackEntries->PackEntry as $k_pack => $v_pack) {
					$PackEntry_ID = (string) $v_pack['ID'];
					$PackEntry_DateStart = (string) $v_pack['DateStart'];
					$PackEntry_DateEnd = (string) $v_pack['DateEnd'];
					$PackEntry_nopti = (string) $v_pack->Duration;
					
					if(sizeof($v_pack->ExtraServices)>0) {
						/*$del_extra = "DELETE FROM import_transilvania_extra WHERE PackEntry_ID = '".$PackEntry_ID."' ";
						$que_del_extra = mysql_query($del_extra) or die(mysql_error());
						@mysql_free_result($que_del_extra);*/
						
						$ins_sql[$k_pack] = array();
						foreach($v_pack->ExtraServices->ExtraService as $k_extra => $v_extra) {
							$ExtraService_ID = (string) $v_extra['ID'];
							$ExtraService_Name = (string) $v_extra['Name'];
							$ExtraService_Type = (string) $v_extra['Type'];
							$ExtraService_Mandatory = (string) $v_extra['Mandatory'];
							
							if($ExtraService_Mandatory=='true') {
								$ins_sql[$k_pack][] = '("'.$id_furnizor.'", "'.$PackEntry_ID.'", "'.$ExtraService_ID.'", "'.$ExtraService_Name.'", "'.$ExtraService_Type.'", "'.$ExtraService_Mandatory.'")';
							}
						}
	
						if(sizeof($ins_sql[$k_pack])>0) {
							$ins_extra = "INSERT INTO import_aidatemp_extra (id_furnizor, PackEntry_ID, ExtraService_ID, ExtraService_Name, ExtraService_Type, ExtraService_Mandatory) VALUES ".implode(',', $ins_sql[$k_pack])." ";
							$que_extra = mysql_query($ins_extra) or die(mysql_error());
							@mysql_free_result($que_extra);
						}
											
					} else {
						echo '<p>Nu exista <strong>ExtraServices</strong>. Pachetul '.$PackEntry_ID.' nu a fost introdus!</p>';
					}
				}
			}
			$err += 1;
		}
	}
	
	if($err>0) {
		echo '<script>alert("ExtraServices au fost updatate cu succes."); document.location.href="/adm/import_eximtur.php?pas=all_extra";</script>';
	} else {
		echo '<br><br><h1>Nu sunt preturi disponibile!</h1>';
		echo '<h2>Se va reveni la aceasta pagina automat in <span id="seconds" class="red">3</span></h2>';
		echo "<script>var seconds = 3; setInterval(function(){if (seconds <= 1) { window.location = '/adm/import_eximtur.php?pas=all_extra'; } else { document.getElementById('seconds').innerHTML = --seconds; } }, 1000);</script>";
	}
}
?>
