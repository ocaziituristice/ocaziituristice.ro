<table width="100%" border="0" cellspacing="2" cellpadding="4" class="default">
  <?php if($mesaj) { ?>
  <tr>
    <td colspan="2"><font class="red"><?php echo $mesaj; ?></font></td>
  </tr>
  <?php } ?>
  <tr>
    <th valign="top" align="right" width="160">Denumire:</th>
    <td><input type="text" name="param[denumire]" class="mare" value="<?php echo $param['denumire']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Denumire Societate</th>
    <td><input type="text" name="param[denumire_societate]" class="mare" value="<?php echo $param['denumire_societate']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Denumire Agentie/Hotel</th>
    <td><input type="text" name="param[denumire_agentie]" class="mare" value="<?php echo $param['denumire_agentie']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Cod Fiscal</th>
    <td><input type="text" name="param[cod_fiscal]" class="mare" value="<?php echo $param['cod_fiscal']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Registrul Comertului</th>
    <td><input type="text" name="param[registrul_comertului]" class="mare" value="<?php echo $param['registrul_comertului']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Localitate</th>
    <td><input type="text" name="param[localitate]" class="mare" value="<?php echo $param['localitate']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Adresa</th>
    <td><textarea name="param[adresa]" class="mic"><?php echo $param['adresa']; ?></textarea></td>
  </tr>
  <tr>
    <th valign="top" align="right">Iban Lei</th>
    <td><input type="text" name="param[iban_lei]" class="mare" value="<?php echo $param['iban_lei']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Iban Euro</th>
    <td><input type="text" name="param[iban_euro]" class="mare" value="<?php echo $param['iban_euro']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Email:</th>
    <td><input type="text" name="param[email]" class="mare" value="<?php echo $param['email']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Telefon:</th>
    <td><input type="text" name="param[telefon]" class="mare" value="<?php echo $param['telefon']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Link administrare:</th>
    <td><input type="text" name="param[link_admin]" class="mare" value="<?php echo $param['link_admin']; ?>" /></td>
  </tr>
  <tr>
    <th align="right" valign="top">Observatii: <br /></th>
    <td><textarea name="param[observatii]" class="mic"><?php echo $param['observatii']; ?></textarea></td>
  </tr>
  <tr>
    <th valign="top" align="right">Adaos sau Discount:</th>
    <td><input type="text" name="param[comision_procent]" class="mic" value="<?php echo $param['comision_procent']; ?>" placeholder="1.07" /> %</td>
  </tr>
  <?php if($_SERVER['PHP_SELF']=='/adm/editare_furnizor.php') { ?>
  <tr>
    <th valign="top" align="right">Upload:</th>
    <td><input type="file" name="fisier" value="" /> <input type="text" name="param[upload_denumire]" value="<?php echo $param['upload_denumire']; ?>" class="mare" placeholder="Denumire fisier" /></td>
  </tr>
  <tr>
    <th>&nbsp;</th>
    <td>
    <?php
	$sel_files="SELECT * FROM furnizori_files WHERE id_furnizor='".$_GET['furnizor']."' ";
	$que_files=mysql_query($sel_files) or die(mysql_error());
	while($row_files=mysql_fetch_array($que_files)) {
		echo '<a href="uploads/furnizori/'.$row_files['fisier'].'" target="_blank" class="blue"><strong>Download</strong></a>';
		if($row_files['denumire']) echo ' - <strong>'.$row_files['denumire'].'</strong>';
		echo ' - '.$row_files['fisier'];
		echo ' - <a href="editare_furnizor.php?pas=2&furnizor='.$_GET['furnizor'].'&delete='.$row_files['id_upload'].'" class="red" onClick="confirm(\'Esti sigur ca doresti sa stergi fisierul: '.$row_files['fisier'].' ?\')">Sterge &raquo;</a><br />';
	}
	?>
    </td>
  </tr>
  <?php } ?>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><input type="submit" name="adauga" value="<?php echo $valut_but_submit; ?>" class="buttons" /></td>
  </tr>
</table>
