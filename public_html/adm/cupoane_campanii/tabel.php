<table width="100%" border="0" cellspacing="2" cellpadding="4" class="default">
  <?php if($mesaj) { ?>
  <tr>
    <td colspan="2"><font class="red"><?php echo $mesaj; ?></font></td>
  </tr>
  <?php } ?>
  <tr>
    <th valign="top" align="right" width="160">Denumire:</th>
    <td><input type="text" name="param[denumire_campanie]" class="mare" value="<?php echo $param['denumire_campanie']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Valoare:</th>
    <td><input type="text" placeholder="00.00" name="param[valoare_campanie]" class="mic" value="<?php echo $param['valoare_campanie']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Moneda:</th>
    <td><select name="param[moneda_campanie]" class="mic">
      <option value="RON" <?php if($param['moneda_campanie']=='RON') echo 'selected'; ?>>RON</option>
      <option value="EURO" <?php if($param['moneda_campanie']=='EURO') echo 'selected'; ?>>EURO</option>
      <option value="USD" <?php if($param['moneda_campanie']=='USD') echo 'selected'; ?>>USD</option>
  
    <option value="Procent" <?php if($param['moneda_campanie']=='USD') echo 'selected'; ?>>Procent</option>
    </select>
    </td>
  </tr>
  <tr>
    <th valign="top" align="right">Data inceput:</th>
    <td><input type="date" name="param[data_inceput]" class="mediu" value="<?php echo $param['data_inceput']; ?>" /></td>
  </tr>
  <tr>
    <th align="right" valign="top">Nr zile:</th>
    <td><input type="number" name="param[nr_zile]" class="mic" value="<?php echo $param['nr_zile']; ?>" /></td>
  </tr>
  <tr>
    <th valign="top" align="right">Tara:</th>
    <td><?php $sel_tari="SELECT id_tara, denumire FROM tari ORDER BY denumire ASC";
	$rez_tari=mysql_query($sel_tari) or die(mysql_error()); ?>
    <select name="param[tara]" id="tara" class="mediu" onchange="ajaxpage('hotel/select_zona.php?zona=<?php echo $param['zona']; ?>&tara='+this.value,'zon');">
      <option value="">--</option>
	  <?php while($row_tari=mysql_fetch_array($rez_tari)) { ?>
      <option value="<?php echo $row_tari['id_tara']; ?>" <?php if($row_tari['id_tara']==$id_tara) { ?> selected="selected" <?php } ?>><?php echo $row_tari['denumire']; ?></option>
	  <?php } @mysql_free_result($rez_tari); ?>
    </select></td>
  </tr>
  <tr>
    <td colspan="2" align="left" id="zon">
      <table width="100%" border="0" cellspacing="2" cellpadding="2" align="left">
        <tr>
          <th align="right" valign="top" width="160">Zona:</th>
          <td>
          <?php $sel_zon="select id_zona, denumire from zone where id_tara = '".$id_tara."' group by id_zona order by denumire ";
          $que_zon=mysql_query($sel_zon) or die(mysql_error()); ?>
          <select name="param[zone]" id="zona" class="mediu" onchange="ajaxpage('hotel/select_localitate.php?id_tara=<?php echo $param['tara']; ?>&zona='+this.value,'loc2');">
            <option value="">--</option>
            <?php while($row_zon=mysql_fetch_array($que_zon)) { ?>
            <option value="<?php echo $row_zon['id_zona']; ?>" <?php if($row_zon['id_zona']==$id_zona) { ?> selected="selected" <?php } ?>><?php echo $row_zon['denumire']; ?></option>
            <?php } @mysql_free_result($que_zon); ?>
          </select></td>
        </tr>
        <tr>
          <th align="right" valign="top">Localitatea:</th>
          <td id="loc2">
          <?php $sel_loc="select id_localitate, denumire from localitati where id_zona = '".$id_zona."' group by id_localitate order by denumire ";
          $que_loc=mysql_query($sel_loc) or die(mysql_error()); ?>
          <select name="param[localitate]" id="localitate" class="mediu">
            <option value="">--</option>
            <?php while($row_loc=mysql_fetch_array($que_loc)) { ?>
            <option value="<?php echo $row_loc['id_localitate']; ?>" <?php if($row_loc['id_localitate']==$id_localitate) { ?> selected="selected" <?php } ?>><?php echo $row_loc['denumire']; ?></option>
            <?php } @mysql_free_result($que_loc); ?>
          </select></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th valign="top" align="right" width="160">Link redirect:</th>
    <td><input type="text" name="param[link_redirect]" class="mare" value="<?php if(!isset($param['link_redirect'])) echo $sitepath; else echo $param['link_redirect']; ?>" /></td>
  </tr>
  <tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><input type="submit" name="adauga" value="<?php echo $valut_but_submit; ?>" class="buttons" /></td>
  </tr>
</table>
