<?php

require __DIR__ . '/../../phalcon/app/legacy.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$requestService = request();

if ($requestService->isPost()) {
    if (strlen($requestService->getPost('username')) > 3 && strlen($requestService->getPost('parola')) > 3) {
        $link_ref = $sitepath_adm . 'home.php';

        if (!empty($_REQUEST['hp'])) {
            $link_ref = $_REQUEST['hp'];
        }

        include_once(__DIR__ . '/../config/includes/class/mysql.php');
        include_once(__DIR__ . '/../config/includes/class/send_mail.php');
        include_once(__DIR__ . '/../config/includes/class/login.php');
        include_once(__DIR__ . '/cofig_adm.php');

        $log = new LOGIN($user_db);

        $err = $log->logare($requestService->getPost('username'), $requestService->getPost('parola'), $link_ref);

        if ($err) {
            echo '<span style="color: #FF4848">' . $err . '</span>';
        }
    } else {
        $err = 'Nu ati completat parola';
    }
}

include_once(__DIR__ . '/login.php');

