<h1>Copiaza oferta</h1>
<?php $id_oferta = $_GET['oferta'];
include_once( $_SERVER['DOCUMENT_ROOT'] . $sitepath_adm . 'class/sejur.php' );
$add_t = new SEJUR;
if ( $_POST['copiaza'] ) {
	$hotel = $_POST['hotel'];
	$add_t->copieza( $id_oferta, $_POST['param'], $hotel, 'normal' );
}

$sel="SELECT
zone.id_zona,
zone.id_tara,
oferte.id_hotel,
oferte.denumire,
hoteluri.nume AS den_hotel
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.id_oferta = '".$id_oferta."' ";
$que=mysql_query($sel) or die(mysql_error());
$row=mysql_fetch_array($que);
@mysql_free_result($que);

$denumire = $row['denumire'];
$den_first = str_replace($row['den_hotel'], "", $denumire);

$hotels = '';
$sel_hotel = "SELECT
hoteluri.id_hotel,
hoteluri.nume,
TRIM(REPLACE(REPLACE(hoteluri.nume, 'Hotel ', ''), 'HOTEL ', '')) AS nume_hotel,
hoteluri.stele,
localitati.denumire AS den_loc,
zone.denumire AS den_zona
FROM hoteluri
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
ORDER BY nume_hotel ASC
";
$que_hotel = mysql_query($sel_hotel) or die(mysql_error());
while($row_hotel = mysql_fetch_array($que_hotel)) {
	$hotels .= '{value:"'.$row_hotel['id_hotel'].'", label:"'.$row_hotel['nume'].' '.$row_hotel['stele'].'* / '.$row_hotel['den_loc'].' / '.$row_hotel['den_zona'].'", replacement:"'.$row_hotel['nume'].'"},'."\n";
}
$hotels = substr($hotels, 0, -1);
?>

<script>
$(function() {
	var hoteluri = [<?php echo $hotels; ?>];
	$("#hotel_new").autocomplete({
		minLength: 3,
		source: hoteluri,
		focus: function( event, ui ) {
			$( "#hotel_new" ).val( ui.item.label );
			return false;
		},
		select: function( event, ui ) {
			$( "#hotel_new" ).val( ui.item.label );
			$( "#id_hotel_new" ).val( ui.item.value );
			$( "#denumire_new" ).val( "<?php echo $den_first; ?>" + ui.item.replacement );
			return false;
		}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li>" )
		.append( "<a>" + item.label + "</a>" )
		.appendTo( ul );
	};
});
</script>

<form name="adauga" enctype="multipart/form-data"  method="post" action="">
  <input type="hidden" name="hotel" value="<?php echo $row['id_hotel']; ?>">
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td valign="top" align="right" width="160">Hotel:</td>
      <td>
        <input type="text" id="hotel_new" name="param[nume_hotel]" class="mare">
        <div class="infos">Scrieti in campul de mai sus denumirea hotelului sau o parte din ea (minim 3 caractere).</div>
        <input type="hidden" id="id_hotel_new" name="param[id_hotel]">
      </td>
    </tr>
    <tr>
      <td valign="top" align="right">Denumire oferta:</td>
      <td><input type="text" id="denumire_new" name="param[denumire]" class="mare" value="<?php echo $denumire; ?>"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="left"><input type="submit" name="copiaza" value="Copiaza" class="buttons"></td>
    </tr>
  </table>
</form>

<?php /*?><script>
function displayResult(selTag) {
	var den_hotel = selTag.options[selTag.selectedIndex].text;
	var den_hotel = den_hotel.replace(/\s\s/g, "");
	var den_hotel = den_hotel.substring(0, den_hotel.length - 3);
	var denumire = "<?php echo $den_first; ?>" + den_hotel;
	document.getElementById('denumire').value = denumire;
}
</script>

<form name="adauga" enctype="multipart/form-data"  method="post" action="" >
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td valign="top" align="right" width="160">Hotel:</td>
      <td>
        <select name="param[id_hotel]" id="id_hotel" onChange="displayResult(this)">
          <option value="" disabled="disabled" <?php if ( !$param["id_hotel"] ) { ?> selected="selected" <?php } ?>>Selecteaza</option>
          <?php
		  $sel_zone = "SELECT zone.denumire, zone.id_zona
		  FROM zone
		  INNER JOIN localitati ON localitati.id_zona = zone.id_zona
		  INNER JOIN hoteluri ON hoteluri.locatie_id = localitati.id_localitate
		  WHERE zone.id_tara = '".$row['id_tara']."'
		  GROUP BY zone.denumire
		  ORDER BY zone.denumire ASC ";
		  $que_zone = mysql_query($sel_zone) or die(mysql_error());
		  while($row_zone = mysql_fetch_array($que_zone)) {
			  echo '<optgroup label="'.$row_zone['denumire'].'">';
			  
			  $sel_loc = "SELECT localitati.denumire, localitati.id_localitate
			  FROM localitati
			  INNER JOIN hoteluri ON hoteluri.locatie_id = localitati.id_localitate
			  WHERE localitati.id_zona = '".$row_zone['id_zona']."'
			  GROUP BY localitati.denumire
			  ORDER BY localitati.denumire ASC ";
			  $que_loc = mysql_query($sel_loc) or die(mysql_error());
			  while($row_loc = mysql_fetch_array($que_loc)) {
				  echo '<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;'.$row_loc['denumire'].'" style="padding-left:20px;">';
				  
				  $sel_hotel = "SELECT hoteluri.id_hotel, TRIM(REPLACE(REPLACE(hoteluri.nume, 'Hotel ', ''), 'HOTEL ', '')) AS nume_hotel, hoteluri.stele
				  FROM hoteluri
				  WHERE hoteluri.locatie_id = '".$row_loc['id_localitate']."'
				  ORDER BY nume_hotel ASC ";
				  $que_hotel = mysql_query($sel_hotel) or die(mysql_error());
				  while($row_hotel = mysql_fetch_array($que_hotel)) {
					  echo '<option value="'.$row_hotel["id_hotel"].'"';
					  if($row['id_hotel'] == $row_hotel["id_hotel"]) echo 'selected';
					  echo '>&nbsp;&nbsp;&nbsp;&nbsp;'.$row_hotel['nume_hotel'].' '.$row_hotel['stele'].'*</option>';
				  } @mysql_free_result($que_hotel);
			  
				  echo '</optgroup>';
			  } @mysql_free_result($que_loc);
			  
			  echo '</optgroup>';
		  } @mysql_free_result($que_zone); ?>
        </select>
        <input type="hidden" name="hotel" value="<?php echo $row['id_hotel']; ?>" /></td>
    </tr>
    <tr>
      <td valign="top" align="right" width="160">Denumire oferta:</td>
      <td><input type="text" name="param[denumire]" id="denumire" class="mare" value="<?php echo $denumire; ?>" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="left"><input type="submit" name="copiaza" value="Copiaza" class="buttons" /></td>
    </tr>
  </table>
</form><?php */?>
