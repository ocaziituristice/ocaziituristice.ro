<?php session_start(); include_once('restrictionare.php'); ?>
<?php
if(isset($_POST['iduri'])) {
	$iduri = explode(",",$_POST['iduri']);
	foreach($iduri as $id_oferta) {
		if($id_oferta>0) {
			$ins = "INSERT INTO oferta_sejur_tip SET
			id_tip_oferta = '".trim($_POST['tip_oferte'])."',
			id_oferta = '".trim($id_oferta)."'
			";
			$que = mysql_query($ins) or die(mysql_error());
		}
	}
	echo "<script> alert('Ofertele au fost adaugate!'); document.location.href='oferte_speciale.php'; </script>";
}
//echo '<pre>';print_r($_POST);echo '</pre>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
  
<h1>Adauga Oferte Speciale</h1>

<br />
<br />

<div class="infos">Introduceti cate <strong>1 ID de oferta</strong> si apoi dati <strong>ADD</strong>. Dupa ce ati introdus tot, apasati <strong>Adauga</strong> pentru salvarea ofertelor.</div>

<br />

<form name="add_ofsp" action="" method="post">
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="default">
  <tr>
    <th width="100">Tip oferta</th>
    <td><select name="tip_oferte">
    	<option value="" disabled selected>--</option>
    <?php
    $sel_tipof = "SELECT * FROM tip_oferta WHERE tip = 'oferte_speciale' ORDER BY ordine DESC ";
    $que_tipof = mysql_query($sel_tipof) or die(mysql_error());
    while($row_tipof = mysql_fetch_array($que_tipof)) {
    ?>
        <option value="<?php echo $row_tipof['id_tip_oferta']; ?>"><?php echo $row_tipof['denumire_tip_oferta'].' &nbsp; - &nbsp; '.date("d.m.Y",strtotime($row_tipof['data_inceput_eveniment'])).' - '.date("d.m.Y",strtotime($row_tipof['data_sfarsit_eveniment'])); ?></option>
    <?php } ?>
    </select></td>
  </tr>
  <?php /*?><tr>
    <th>ID-uri oferte</th>
    <td><textarea name="iduri" class="mic2"></textarea></td>
  </tr><?php */?>
  <tr>
    <th>ID-uri oferte</th>
    <td>
      <input type="number" name="ids[0]" id="id_new" value="" class="mediu">
	  <a class="buttons" id="add_offer" style="cursor:pointer;">ADD</a>
    </td>
  </tr>
  <tr>
    <td colspan="2" id="oferte" style="padding:15px 5px;"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><input type="submit" name="adauga" value="Adauga" class="buttons"></td>
  </tr>
</table>
</form>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

<script>
$("#add_offer").click(function () {
	var ids_old = $("#iduri").val();
	var id_new = $("#id_new").val();
	var load_offers = "oferte_speciale_view.php?oferte="+id_new;
	if(ids_old) var load_offers = load_offers + "," + ids_old;
	$("#oferte").load(load_offers);
	document.getElementById('id_new').value = '';
});
</script>

</body>
</html>