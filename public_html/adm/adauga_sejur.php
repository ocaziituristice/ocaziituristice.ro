<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php'); ?>
<script type="text/javascript" src="js/tabel_preturi_adm.js"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/datepicker_ro.js"></script>
<script type="text/javascript">
//<![CDATA[
$(function() {
		$("#data_start").datepicker({
			numberOfMonths: 3,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true
		});
		
		$("#data_end").datepicker({
			numberOfMonths: 3,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true
		});
		
		$("#data_start_nr_min").datepicker({
			numberOfMonths: 3,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true
		});
		
		$("#data_end_nr_min").datepicker({
			numberOfMonths: 3,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true
		});
		
		$("#early_time1").datepicker({
			numberOfMonths: 3,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true
		});
	});
	
function pick_calendar(den) {
 $("#"+den).datepicker({
	numberOfMonths: 3,
	dateFormat: "yy-mm-dd",
	showButtonPanel: true
 });
}
//]]>

</script>
<script type="text/javascript" src="js/detalii_avion.js"></script>
<script type="text/javascript" src="js/detalii_autocar.js"></script>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
<?php //centru_______________________________________ ?>
<h1>Adauga Sejur</h1>
<br /><br />
<?php if($_POST['param']) $param=$_POST['param']; else $param=array();
if($_POST['preturi']) $preturi=$_POST['preturi']; else $preturi=array();
if($_POST['aeroport']) $aeroport=$_POST['aeroport']; else $aeroport=array();
if($_POST['autocar']) $autocar=$_POST['autocar']; else $autocar=array();
if($_POST['adauga'] || $_POST['adauga_preturi']) {
include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'class/sejur.php');
if(strlen(trim($param['denumire']))>3) {
$add_t=new SEJUR;
if($_POST['adauga_preturi']) $tip='adauga_preturi';
if($_POST['adauga']) $tip='adauga';
echo $tip;
$add_t->adaugare($param, $preturi, $aeroport, $autocar, $tip); 
 } else $mesaj='Denumirea este prea scurta!';
}
if(!$param['id_hotel']) $id_hotel=$_GET['hotel']; else $id_hotel=$param['id_hotel'];
$selH="select localitati.id_localitate, hoteluri.id_hotel, tari.id_tara, hoteluri.tip_unitate, tari.denumire as denumire_tara from hoteluri left join localitati on hoteluri.locatie_id = localitati.id_localitate left join zone on localitati.id_zona = zone.id_zona left join tari on zone.id_tara = tari.id_tara where hoteluri.id_hotel = '".$id_hotel."' ";
$queH=mysql_query($selH) or die(mysql_error());
$param1=mysql_fetch_array($queH);
@mysql_free_result($queH);
if(!$_POST['param']['id_hotel']) $param['id_hotel']=$param1['id_hotel'];
$param['id_localitate']=$param1['id_localitate'];
$param['tip_unitate']=$param1['tip_unitate']; ?>
<form name="adauga" enctype="multipart/form-data"  method="post" action="" >
<?php $valut_but_submit='Finalizeaza'; include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'sejur/tabel.php'); ?>
</form>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>