function adauga_early() {
	var nume='early';
	var early1=document.getElementById('early_time1').value;
	var disc1=document.getElementById('early_disc1').value;
	var inclus1=document.getElementById('early_inclus1').value;
	var text_early1=document.getElementById('text_early1').value;
	
	if(early1!='') {
		var table = document.getElementById(nume);
		var rowCount = (table.rows.length);
		var index=parseInt(rowCount,10)+parseInt(1,10);
		var x=document.getElementById(nume).insertRow(rowCount);
		var v=x.insertCell(0);
		v.align="right";
		v.innerHTML=index+'. Pana in data / discount:';
		var v2=x.insertCell(1);
		v2.align="left";
		var sel='<select name="param[early_inclus][]" id="early_inclus'+rowCount+'"><option value="nu"';
		if(inclus1=='nu') sel=sel+'selected="selected"'; sel=sel+'>Nu</option> <option value="da"';
		if(inclus1=='da') sel=sel+'selected="selected"'; sel=sel+'>Da</option> </select>';
		v2.innerHTML='<input type="text" name="param[early_time][]" value="'+early1+'" class="date" id="early_time'+rowCount+'" onclick="pick_pick_calendar(\'early_time'+rowCount+'\');" /> / <input type="text" name="param[early_disc][]" style="width:30px;" value="'+disc1+'" />% Inclus in pret: '+sel;
		var v3=x.insertCell(2);
		v3.align="left";
		v3.innerHTML='Detalii:<br/><textarea name="param[text_early][]" id="text_early'+rowCount+'" class="mic">'+text_early1+'</textarea>';
		var v4=x.insertCell(3);
		v4.align="left";
		v4.innerHTML='<a href="javascript: ;" onclick="delete_early('+rowCount+');" class="buttons">X</a>';
		document.getElementById('early_time1').value='';
		document.getElementById('early_disc1').value='';
		document.getElementById('text_early1').value='';
	} else alert('Nu ati completat datele pentru early booking!');
}


function delete_early(rand) {
	var de_la=parseInt(rand,10)+parseInt(1,10);
	nume='early';
	var table = document.getElementById(nume);
	var rowCount = (table.rows.length-1);
	if(de_la<=rowCount) {
		for(var i=de_la; i<=rowCount; i++) {
			var index=i-1;
			table.rows[i].cells[0].innerHTML=i+'. Pana in data / discount:';
			table.rows[i].cells[3].innerHTML='<a href="javascript: ;" onclick="delete_early('+index+');" class="buttons">X</a>';
		}
	}
	table.deleteRow(rand);
}


function adauga_linie_serv(nume, den, value, moneda, oblicatoriu, tip) {
	if(den!='') {
		var table = document.getElementById(nume);
		var rowCount = (table.rows.length);
		
		<!--select_moneda-->
		var selectat=document.getElementById('moneda_'+tip).selectedIndex;
		sele="<select name='param[moneda_"+tip+"]["+rowCount+"]' style='width:100px'>";
		for (var z=0; z<document.getElementById('moneda_'+tip).length; z++) {
			if(document.getElementById('moneda_'+tip)[selectat].value!=document.getElementById('moneda_'+tip)[z].value) sele=sele+"<option value='"+document.getElementById('moneda_'+tip)[z].value+"'>" + document.getElementById('moneda_'+tip)[z].text+"</option>";
			else sele=sele+"<option value='"+document.getElementById('moneda_'+tip)[z].value+"' selected='selected'>" + document.getElementById('moneda_'+tip)[z].text+"</option>";
		}
		sele=sele+"</select>";

		<!--select_tip-->
		if(tip!='v4') {
			var selectatT=document.getElementById('tip_supliment_'+tip).selectedIndex;
			seleT="&nbsp;&nbsp;&nbsp;<select name='param[tip_supliment_"+tip+"]["+rowCount+"]'>";
			for (var z=0; z<document.getElementById('tip_supliment_'+tip).length; z++) {
				if(document.getElementById('tip_supliment_'+tip)[selectatT].value!=document.getElementById('tip_supliment_'+tip)[z].value) seleT=seleT+"<option value='"+document.getElementById('tip_supliment_'+tip)[z].value+"'>" + document.getElementById('tip_supliment_'+tip)[z].text+"</option>";
					else seleT=seleT+"<option value='"+document.getElementById('tip_supliment_'+tip)[z].value+"' selected='selected'>" + document.getElementById('tip_supliment_'+tip)[z].text+"</option>";
				if(tip=='v3') {
					var data_start_supliment=document.getElementById('data_start_supliment').value;
					var data_end_supliment=document.getElementById('data_end_supliment').value;
				}
			}
			seleT=seleT+"</select>";
		} else seleT='';
		
		<!--select_exprim-->
		if(tip=='v2' || tip=='v3') {
			var selectatE=document.getElementById('exprim_'+tip).selectedIndex;
			seleE="&nbsp;&nbsp; Exprimare: <select name='param[exprim_"+tip+"]["+rowCount+"]'>";
			for (var z=0; z<document.getElementById('exprim_'+tip).length; z++) {
				if(document.getElementById('exprim_'+tip)[selectatE].value!=document.getElementById('exprim_'+tip)[z].value) seleE=seleE+"<option value='"+document.getElementById('exprim_'+tip)[z].value+"'>" + document.getElementById('exprim_'+tip)[z].text+"</option>";
				else seleE=seleE+"<option value='"+document.getElementById('exprim_'+tip)[z].value+"' selected='selected'>" + document.getElementById('exprim_'+tip)[z].text+"</option>";
			}
			seleE=seleE+"</select>";
		} else seleE='';

		<!--select_pasager-->
		if(tip=='v2' || tip=='v3') {
			var selectatP=document.getElementById('pasager_'+tip).selectedIndex;
			seleP="<select name='param[pasager_"+tip+"]["+rowCount+"]'>";
			for (var z=0; z<document.getElementById('pasager_'+tip).length; z++) {
				if(document.getElementById('pasager_'+tip)[selectatP].value!=document.getElementById('pasager_'+tip)[z].value) seleP=seleP+"<option value='"+document.getElementById('pasager_'+tip)[z].value+"'>" + document.getElementById('pasager_'+tip)[z].text+"</option>";
				else seleP=seleP+"<option value='"+document.getElementById('pasager_'+tip)[z].value+"' selected='selected'>" + document.getElementById('pasager_'+tip)[z].text+"</option>";
			}
			seleP=seleP+"</select>";
		} else seleP='';

		<!--obligatoriu-->
		if(tip!='v4' && tip!='v1') {
			if(document.getElementById('obligatoriu_'+tip).checked) var obl='&nbsp;&nbsp;&nbsp; <label>Obligatoriu <input type="checkbox" name="param[obligatoriu_'+tip+']['+rowCount+']" value="da" checked="checked" /></label>';
			else var obl='&nbsp;&nbsp;&nbsp; <label>Obligatoriu <input type="checkbox" name="param[obligatoriu_'+tip+']['+rowCount+']" value="da" /></label>';
		} else var obl='';
		
		var x=document.getElementById(nume).insertRow(rowCount);
		
		var v=x.insertCell(0);
		v.align="center";
		v.innerHTML=rowCount;
		
		var v2=x.insertCell(1);
		v2.align="left";
		v2.innerHTML='<input type="text" name="param[denumire_'+tip+']['+rowCount+']" value="'+den+'" class="mare" />';
		if(tip=='v3') v2.innerHTML=v2.innerHTML+'<br/>Perioada: <input type="text" name="param[data_start_supliment]['+rowCount+']" id="data_start_supliment'+rowCount+'" class="date" value="'+data_start_supliment+'" /> <input type="text" name="param[data_end_supliment]['+rowCount+']" id="data_end_supliment'+rowCount+'" class="date" value="'+data_end_supliment+'" />';
		
		var v3=x.insertCell(2);
		v3.align="left";
		v3.innerHTML='<input type="text" name="param[value_'+tip+']['+rowCount+']" value="'+value+'" class="mediu" /> '+sele+' '+obl+' '+seleT+' '+seleE+' '+seleP;
		
		var v4=x.insertCell(3);
		v4.align="center";
		v4.innerHTML='<a href="javascript: ;" onclick="delete_linie_serv(\''+nume+'\', '+rowCount+');" class="buttons">X</a>';
		
		document.getElementById('den_'+tip).value='';
		document.getElementById('value_'+tip).value='';
		
	} else alert('Nu ati completat datele pentru supliment!');
}


function delete_linie_serv(nume, rand) {
	var de_la=parseInt(rand,10)+parseInt(1,10);
	var table = document.getElementById(nume);
	var rowCount = (table.rows.length-1);
	if(de_la<=rowCount) {
		for(var i=de_la; i<=rowCount; i++) {
			var index=i-1;
			table.rows[i].cells[0].innerHTML=index;
			table.rows[i].cells[3].innerHTML='<a href="javascript: ;" onclick="delete_linie_serv(\''+nume+'\', '+index+');" class="buttons">X</a>';
		}
	}
	table.deleteRow(rand);
}


function adauga_rand_pret() {
	var nume='t_preturi';
	var table = document.getElementById(nume);
	var id_hotel=document.getElementById('id_hotel').value;
	var nr_cam=document.getElementById('nr_cam').value;
	var id_c=parseInt(nr_cam,10)+parseInt(2,10);
	var rowCount = (table.rows.length);
	var nr_randuri=document.getElementById('nr_randuri').value;
	var x=table.insertRow(rowCount);
	var v=x.insertCell(0);
	v.colSpan=id_c;
	v.innerHTML='<div id="rand_'+rowCount+'" align="left"> </div>';
	var l='&data_start='+document.getElementById('data_start').value+'&data_end='+document.getElementById('data_end').value;
	for(var i=1; i<=nr_cam; i++) {
		l=l+'&pret['+i+']='+document.getElementById('pret'+i).value;
		l=l+'&moneda['+i+']='+document.getElementById('moneda'+i).value;
		
	}
	nr_randuri1=parseInt(nr_randuri,10)+parseInt(1,10);
	document.getElementById('nr_randuri').value=nr_randuri1;
	ajaxpage('/adm/sejur/linie_pret.php?id='+rowCount+'&nr_randuri='+nr_randuri+'&id_hotel='+id_hotel+l,'rand_'+rowCount);
	document.getElementById('data_start').value='';
	document.getElementById('data_end').value='';
}


function delete_row(id) {
	var nume='t_preturi';
	var table = document.getElementById(nume);
	var id_hotel=document.getElementById('id_hotel').value;
	var nr_cam=document.getElementById('nr_cam').value;
	var id_c=parseInt(nr_cam,10)+parseInt(1,10);
	var rowCount =(table.rows.length-1);
	var from = parseInt(id,10)+parseInt(1,10);
	for(var i=from; i<=rowCount; i++) {
		var index=i-1;
		var div=document.getElementById('rand_'+i);
		div.id='rand_'+index;
		var table1 = document.getElementById('table_in'+i);
		table1.rows[0].cells[id_c].innerHTML='<a href="javascript: ;" onclick="delete_row('+index+');" class="buttons">X</a>';
		table1.id='table_in'+index;
	}
	table.deleteRow(id);
}

function adauga_rand_nr_min() {
var nume='t_min_n';
var data_start=document.getElementById('data_start_nr_min').value;
var data_end=document.getElementById('data_end_nr_min').value;
var nr_min1=document.getElementById('nr_min1').value;

if(data_start!='' && data_end!='' && nr_min1!='') {
var table = document.getElementById(nume);
var rowCount = (table.rows.length);
var x=document.getElementById(nume).insertRow(rowCount);
var v=x.insertCell(0);
v.align="left";
v.innerHTML='Inceput: <input type="text" name="param[data_start_nr_min][]" class="date" value="'+data_start+'" id="data_start_nr_min'+rowCount+'" onclick="pick_calendar(\'data_start_nr_min'+rowCount+'\');" /><br/>Sfarsit: &nbsp; <input type="text" name="param[data_end_nr_min][]" class="date" value="'+data_end+'" id="data_end_nr_min'+rowCount+'" onclick="pick_calendar(\'data_end_nr_min'+rowCount+'\');" />';
var v2=x.insertCell(1);
v2.align="left";
v2.innerHTML='Numar minim: <input type="text" name="param[nr_min][]" value="'+nr_min1+'" class="mic" />';
var v3=x.insertCell(2);
v3.align="left";
v3.innerHTML='<a href="javascript: ;" onclick="delete_nr_min('+rowCount+');" class="buttons">X</a>';
document.getElementById('data_start_nr_min').value='';
document.getElementById('data_end_nr_min').value='';
document.getElementById('nr_min1').value='';
} else alert('Nu ati completat datele pentru numarul minim de nopti!');
}

function delete_nr_min(rand) {
var de_la=parseInt(rand,10)+parseInt(1,10);
nume='t_min_n';
var table = document.getElementById(nume);
var rowCount = (table.rows.length-1);
 if(de_la<=rowCount) {
  for(var i=de_la; i<=rowCount; i++) {
	var index=i-1;
	table.rows[i].cells[2].innerHTML='<a href="javascript: ;" onclick="delete_nr_min('+index+');" class="buttons">X</a>';
  }
 }
table.deleteRow(rand); 
}

function include_tari(eu, continator) {
var selectare = document.getElementById(eu);
selectat=selectare.selectedIndex;
id=selectare[selectat].value;
var outputArea = document.getElementById(continator);
pagina="fa_orase.php?id="+id;
ajaxpage(pagina,continator);
}

function scoate_oras(nume1, nume2) {	
var lista1=document.getElementById(nume1);
var lista2=document.getElementById(nume2);
lung1=lista1.length;
lung2=lista2.length;
selectat=lista1.selectedIndex;
oras_nume=lista1[selectat].text;
oras_id=lista1[selectat].value;
oras_nume=oras_nume.slice(0, oras_nume.search(">")-1);
oras_id=oras_id.slice(0, oras_id.search(">")-1);
lista1.remove(selectat);
appendOptionLast(oras_nume, oras_id,  lista2);
}

function baga_oras(nume1, nume2, textul) { 
var lista1=document.getElementById(nume1);
var lista2=document.getElementById(nume2);
var text1=document.getElementById(textul);
lung1=lista1.length;
lung2=lista2.length; 
text2=text1.value;
selectat=lista1.selectedIndex;
oras_nume=lista1[selectat].text;
oras_id=lista1[selectat].value;		
lista1.remove(selectat);
appendOptionLast(oras_nume+"->"+ text1.value, oras_id +"->"+ text1.value,  lista2);
text1.value="";
}

function appendOptionLast(text, val,  lista) {
var elOptNew = document.createElement('option');
elOptNew.text =text;
elOptNew.value = val;
var elSel = lista
try {
elSel.add(elOptNew, null); // asta nu merge in Internet Explorer
}
catch(ex) {
elSel.add(elOptNew); // In schimb asta merge doar in Internet Explorer
}
}

function selectAllOptions(selStr)
{
  var selObj = document.getElementById(selStr);
  for (var i=0; i<selObj.length; i++) {
    selObj[i].selected = true;
  }
}


