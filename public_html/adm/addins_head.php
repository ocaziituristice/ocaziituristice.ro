<link href="/adm/reset.css" rel="stylesheet" type="text/css" />
<link href="/adm/style.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="/adm/style_ie6.css" rel="stylesheet" type='text/css' />
<![endif]-->
<!--[if lte IE 7]>
<link href="/adm/style_ie7.css" rel="stylesheet" type='text/css' />
<![endif]-->
<script src="/js/ajaxpage.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

<link href="/js/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	$("a[rel='gallery']").colorbox({});
	$("a[rel='gallery1']").colorbox({});
	$("a[rel='grad_ocup']").colorbox({width:"850px", height:"600px", iframe:true, overlayClose:false});
	});
</script>
