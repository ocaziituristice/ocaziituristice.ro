<?php
$xml = simplexml_load_file($sUrl);

if(sizeof($xml)>0) {
	
	$trip = array();
	
	$i=-1;
	foreach($xml->trip as $k_xml => $v_xml) {
		$i++;
		$trip[$i]['date'] = trim($v_xml->attributes()->{'date'});
		$trip[$i]['period'] = trim($v_xml->attributes()->{'period'});
		$trip[$i]['hotel_code'] = trim($v_xml->hotel->attributes()->{'code'});
		$trip[$i]['hotel_name'] = trim($v_xml->hotel->attributes()->{'name'});
		$trip[$i]['hotel_category'] = trim($v_xml->hotel->attributes()->{'category'});
		$trip[$i]['hotel_destCode'] = trim($v_xml->hotel->attributes()->{'destCode'});
		$trip[$i]['hotel_city'] = trim($v_xml->hotel->attributes()->{'city'});
		$trip[$i]['hotel_region'] = trim($v_xml->hotel->attributes()->{'region'});
		$trip[$i]['hotel_country'] = trim($v_xml->hotel->attributes()->{'country'});
		$trip[$i]['hotel_room'] = trim($v_xml->hotel->attributes()->{'room'});
		$trip[$i]['hotel_roomDescription'] = trim($v_xml->hotel->attributes()->{'roomDescription'});
		$trip[$i]['hotel_maintenance'] = trim($v_xml->hotel->attributes()->{'maintenance'});
		$trip[$i]['hotel_maintenanceDescription'] = trim($v_xml->hotel->attributes()->{'maintenanceDescription'});
		$trip[$i]['price_adult'] = trim($v_xml->price->attributes()->{'adult'});
		$trip[$i]['price_currency'] = trim($v_xml->price->attributes()->{'currency'});
		if($v_xml->flight!='') {
			$trip[$i]['trans_airlineCode'] = trim($v_xml->flight->attributes()->{'airlineCode'});
			$trip[$i]['trans_code'] = trim($v_xml->flight->attributes()->{'flightNr'});
			$trip[$i]['trans_dep_city'] = trim($v_xml->flight->departure->attributes()->{'city'});
			$trip[$i]['trans_dep_cityDesc'] = trim($v_xml->flight->departure->attributes()->{'cityDesc'});
			$trip[$i]['trans_dep_depDate'] = trim($v_xml->flight->departure->attributes()->{'depDate'}).' '.trim($v_xml->flight->departure->attributes()->{'depTime'}).':00';;
			$trip[$i]['trans_dep_arrDate'] = trim($v_xml->flight->departure->attributes()->{'arrDate'}).' '.trim($v_xml->flight->departure->attributes()->{'arrTime'}).':00';
			$trip[$i]['trans_des_city'] = trim($v_xml->flight->destination->attributes()->{'city'});
			$trip[$i]['trans_des_cityDesc'] = trim($v_xml->flight->destination->attributes()->{'cityDesc'});
			$trip[$i]['trans_des_depDate'] = trim($v_xml->flight->destination->attributes()->{'depDate'}).' '.trim($v_xml->flight->destination->attributes()->{'depTime'}).':00';
			$trip[$i]['trans_des_arrDate'] = trim($v_xml->flight->destination->attributes()->{'arrDate'}).' '.trim($v_xml->flight->destination->attributes()->{'arrTime'}).':00';
		}
		
		/*if($i==10) break;*/
	}
	
	if(sizeof($trip)>0) {
		mysql_query("TRUNCATE TABLE preturi_laguna") or die(mysql_error());
		
		foreach($trip as $key => $value) {
			$ins_hotel = 'INSERT INTO preturi_laguna (date, period, hotel_code, hotel_name, hotel_category, hotel_destCode, hotel_city, hotel_region, hotel_country, hotel_room, hotel_roomDescription, hotel_maintenance, hotel_maintenanceDescription, price_adult, price_currency, trans_airlineCode, trans_code, trans_dep_city, trans_dep_cityDesc, trans_dep_depDate, trans_dep_arrDate, trans_des_city, trans_des_cityDesc, trans_des_depDate, trans_des_arrDate, data_update) VALUES ("'.$value['date'].'", "'.$value['period'].'", "'.$value['hotel_code'].'", "'.$value['hotel_name'].'", "'.$value['hotel_category'].'", "'.$value['hotel_destCode'].'", "'.$value['hotel_city'].'", "'.$value['hotel_region'].'", "'.$value['hotel_country'].'", "'.$value['hotel_room'].'", "'.$value['hotel_roomDescription'].'", "'.$value['hotel_maintenance'].'", "'.$value['hotel_maintenanceDescription'].'", "'.$value['price_adult'].'", "'.$value['price_currency'].'", "'.$value['trans_airlineCode'].'", "'.$value['trans_code'].'", "'.$value['trans_dep_city'].'", "'.$value['trans_dep_cityDesc'].'", "'.$value['trans_dep_depDate'].'", "'.$value['trans_dep_arrDate'].'", "'.$value['trans_des_city'].'", "'.$value['trans_des_cityDesc'].'", "'.$value['trans_des_depDate'].'", "'.$value['trans_des_arrDate'].'", NOW() ) ';
			$que_hotel = mysql_query($ins_hotel) or die(mysql_error());
			@mysql_free_result($que_hotel);
		}
		mysql_query("OPTIMIZE TABLE preturi_laguna") or die(mysql_error());
		
		echo '<script>alert("Preturile au fost introduse cu succes."); document.location.href="/adm/import_laguna.php";</script>';
	}
} else {
	echo '<script>alert("EROARE!!! Va rugam incercati din nou mai tarziu!"); document.location.href="/adm/import_laguna.php";</script>';
}

?>