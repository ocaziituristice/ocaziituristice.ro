<script type="text/javascript">
	function UpdateTableHeaders() {
		$("div.divTableWithFloatingHeader").each(function() {
			var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
			var floatingHeaderRow = $(".tableFloatingHeader", this);
			var offset = $(this).offset();
			var scrollTop = $(window).scrollTop();
			if ((scrollTop > offset.top) && (scrollTop < offset.top + $(this).height())) {
				floatingHeaderRow.css("visibility", "visible");
				floatingHeaderRow.css("top", Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()) + "px");

				// Copy cell widths from original header
				$("th", floatingHeaderRow).each(function(index) {
					var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
					$(this).css('width', cellWidth);
				});

				// Copy row width from whole table
				floatingHeaderRow.css("width", $(this).css("width"));
			}
			else {
				floatingHeaderRow.css("visibility", "hidden");
				floatingHeaderRow.css("top", "0px");
			}
		});
	}

	$(document).ready(function() {
		$("table.tableWithFloatingHeader").each(function() {
			$(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

			var originalHeaderRow = $("tr:first", this)
			originalHeaderRow.before(originalHeaderRow.clone());
			var clonedHeaderRow = $("tr:first", this)

			clonedHeaderRow.addClass("tableFloatingHeader");
			clonedHeaderRow.css("position", "absolute");
			clonedHeaderRow.css("top", "0px");
			clonedHeaderRow.css("left", $(this).css("margin-left"));
			clonedHeaderRow.css("visibility", "hidden");

			originalHeaderRow.addClass("tableFloatingHeaderOriginal");
		});
		UpdateTableHeaders();
		$(window).scroll(UpdateTableHeaders);
		$(window).resize(UpdateTableHeaders);
	});
</script>

<script type="text/javascript" src="/js/jquery.jeditable.mini.js"></script>

<?php
if(isset($_POST['update_hotels'])) {

$sXml = '<HotelListRQ>';
$sXml .= $login_data;	
$sXml .= '</HotelListRQ>';

$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "requestXML=".$sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);

$hotels = new SimpleXMLElement($sResponse);

//echo '<ol>';
$ins_sql = array();
foreach($hotels->Hotels[0]->Hotel as $k_hotels => $v_hotels) {
	$hotel_nume = trim($v_hotels->HotelName);
	$hotel_id = trim($v_hotels->attributes()->code);
	if(count($v_hotels->Address->City)>0) $hotel_city = trim($v_hotels->Address->City); else $hotel_city = '';
	if(count($v_hotels->Address->Country)>0) $hotel_country = trim($v_hotels->Address->Country->attributes()->code); else $hotel_country = '';
	//echo '<li>'.$hotel_nume.' '.$hotel_id.' '.$hotel_city.' '.$hotel_country.'</li>';
	
	$sel_hotels = 'SELECT * FROM import_hoteluri WHERE id_hotel = "'.$hotel_id.'" AND nume_hotel = "'.$hotel_nume.'" ';
	$que_hotels = mysql_query($sel_hotels);
	$row_hotels = mysql_fetch_array($que_hotels);
	$totalRows_hotels = mysql_num_rows($que_hotels);
	if($totalRows_hotels=='0') {
		$ins_sql[] = '("'.$hotel_id.'", "'.$hotel_nume.'", "'.$hotel_city.'", "'.$hotel_country.'", "'.$_SESSION['id_furnizor'].'")';
	}
	@mysql_free_result($que_hotels);
}
//echo '</ol>';

if(sizeof($ins_sql)>0) {
	$ins_hotel = "INSERT INTO import_hoteluri (id_hotel, nume_hotel, city, country, id_furnizor) VALUES ".implode(',', $ins_sql)." ";
	$que_hotel = mysql_query($ins_hotel) or die(mysql_error());
	@mysql_free_result($que_hotel);
}


	echo '<script>alert("Hotelurile au fost updatate cu succes.");</script>';
	/*echo '<script>alert("Hotelurile au fost updatate cu succes."); document.location.href=\'/adm/import_melitours.php?pas=availability\'; </script>';*/

} else {
?>
<form action="" method="post">
  <br>
  <h1>Apasand pe butonul de mai jos se va incarca lista cu toate hotelurile! <strong class="red">Aveti rabdare, dureaza!</strong></h1>
  <h1 class="red">Asteptati mesajul de incarcare finalizata !</h1>
  <br>
  <input type="submit" name="update_hotels" value="Updateaza Hotelurile" class="buttons">
</form>
<?php } ?>
<?php
$sel_hoteluri = 'SELECT * FROM import_hoteluri WHERE id_furnizor = "'.$_SESSION['id_furnizor'].'" ORDER BY preturi_disponibile DESC, country ASC, city ASC, nume_hotel ASC ';
$que_hoteluri = mysql_query($sel_hoteluri);
?>
<br><br>
<table cellpadding="2" cellspacing="2" border="1" width="100%" align="center" class="tabel tableWithFloatingHeader">
  <thead>
  <tr>
    <th>#</th>
    <th>Nume hotel</th>
    <th>ID hotel</th>
    <th>Oras</th>
    <th>Tara</th>
    <th>Corespondent</th>
    <th>Data update</th>
    <th>Preturi</th>
    <th></th>
  </tr>
  </thead>
  <tbody>
<?php
$i=0;
while($row_hoteluri = mysql_fetch_array($que_hoteluri)) {
	$i++;
	if($row_hoteluri['preturi_disponibile']=='1') $preturi='<img src="images/yes.png">'; elseif($row_hoteluri['preturi_disponibile']=='0') $preturi='<img src="images/no.png">'; else $preturi = '';
	if(isset($row_hoteluri['data_update'])) $data_update = date('d.m.Y H:i:s',strtotime($row_hoteluri['data_update'])); else $data_update = '';
	
	echo '<tr class="normal" onmouseout="this.className=\'normal\';" onmouseover="this.className=\'hover\';">';
	echo '<td><div style="position:relative; top:-40px;" id="'.$row_hoteluri['id_hotel'].'"></div>'.$i.'</td>';
	echo '<td><strong>'.$row_hoteluri['nume_hotel'].'</strong></td>';
	echo '<td>'.$row_hoteluri['id_hotel'].'</td>';
	echo '<td>'.$row_hoteluri['city'].'</td>';
	echo '<td>'.$row_hoteluri['country'].'</td>';
	echo '<td>';?>
<script type="text/javascript">
$(document).ready(function() {
	$('#hotel<?php echo $row_hoteluri['id']; ?>').editable("/adm/import_melitours/editinplace_update.php", {
		loadurl : '/adm/import_melitours/load.php',
		type   : "select",
		submit : "OK",
		style  : "inherit" 
	});
});
</script>
<div id="hotel<?php echo $row_hoteluri['id']; ?>" style="cursor:pointer;" class="bold black"><?php if($row_hoteluri['id_hotel_corespondent']) echo $row_hoteluri['id_hotel_corespondent']; else echo 'Click pentru corespondent'; ?></div>

    <?php /*foreach($hoteluri_ocazii as $key_hoc => $value_hoc) {
    	echo '<option value="'.$key_hoc.'"';
		if($row_hoteluri['id_corespondent']==$key_hoc) echo ' selected';
		echo'>'.$value_hoc.'</option>';
	}*/
    echo '</td>';
    echo '<td align="center">'.$data_update.'</td>';
	echo '<td align="center">'.$preturi.'</td>';
	echo '<td align="center"><a href="import_melitours.php?pas=1&idf='.$_SESSION['id_furnizor'].'&id_hot_import='.$row_hoteluri['id_hotel'].'" target="_blank" class="buttons">Importa preturi</a></td>';
	echo '</tr>';
}
?>
  </tbody>
</table>
<script>alert("Incarcare completa!")</script>