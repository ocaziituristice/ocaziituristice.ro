<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/adm/class/hotel.php');	
$id_furnizor = '216';
$id_hotel = $_REQUEST['hotel'];
$add_t = new HOTEL;
$param = $add_t->select_camp_hotel($id_hotel);

//echo '<pre>';print_r($_POST);echo '</pre>';

if(isset($_POST['adauga_coresp_hotel'])) {
	$upd_hot = "UPDATE import_hoteluri SET id_hotel_corespondent = '".$id_hotel."', data_update = NOW() WHERE id = '".$_POST['id_hot_other']."' ";
	$que_hot = mysql_query($upd_hot) or die(mysql_error());
	@mysql_free_result($que_hot);

	$sel_local = "SELECT localitati_corespondent.id
	FROM import_hoteluri
	INNER JOIN localitati_corespondent ON localitati_corespondent.id = import_hoteluri.city
	WHERE import_hoteluri.id = '".$_POST['id_hot_other']."' ";
	$que_local = mysql_query($sel_local);
	$row_local = mysql_fetch_array($que_local);
	
	if($row_local['id_corespondent']=='') {
		$upd_loc = "UPDATE localitati_corespondent SET id_corespondent = '".$param['localitate']."' WHERE id = '".$row_local['id']."' ";
		$que_loc = mysql_query($upd_loc) or die(mysql_error());
		@mysql_free_result($que_loc);
	}
}

$sel_hotel = "SELECT * FROM import_hoteluri WHERE id_furnizor = '".$id_furnizor."' AND id_hotel_corespondent = '".$id_hotel."' ";
$que_hotel = mysql_query($sel_hotel);
if(mysql_num_rows($que_hotel)>1) {
	while($row_hotel = mysql_fetch_array($que_hotel)) {
		$hotel_codes[] = $row_hotel['id_hotel'];
	}
} else {
	$row_hotel = mysql_fetch_array($que_hotel);
	$hotel_code = $row_hotel['id_hotel'];
}

if(isset($_POST['codul_hotelului'])) {
	$hotel_code = $_POST['codul_hotelului'];
	$_SESSION['hotel_code'] = $_POST['codul_hotelului'];
} else if(isset($_SESSION['hotel_code'])) {
	$hotel_code = $_SESSION['hotel_code'];
}

if(isset($_POST['perioada_preturi'])) {
	$start_date = $_POST['start_date'];
	$end_date = $_POST['end_date'];
	$_SESSION['start_date'] = $_POST['start_date'];
	$_SESSION['end_date'] = $_POST['end_date'];
} else {
	if(isset($_SESSION['start_date'])) {
		$start_date = $_SESSION['start_date'];
	}
	if(isset($_SESSION['end_date'])) {
		$end_date = $_SESSION['end_date'];
	}
}

$furnizor = get_detalii_furnizor($id_furnizor);
if(isset($_POST['comision_procent'])) {
	$comision_procent = $_POST['comision_procent'];
	$_SESSION['comision_procent'] = $_POST['comision_procent'];
} else if(isset($_SESSION['comision_procent'])) {
	$comision_procent = $_SESSION['comision_procent'];
} else {
	$comision_procent = $furnizor['comision_procent'];
}

if(isset($start_date) and isset($end_date)) {
	$sXml = '<pricePeriodRQ>';
	$sXml .= $login_data;	
	$sXml .= '<Language>EN</Language>
	<pricePeriodInfo>
	<hotel>'.$hotel_code.'</hotel>
	<roomType></roomType>
	<fromd>'.$start_date.'</fromd>
	<tod>'.$end_date.'</tod>
	</pricePeriodInfo>
	</pricePeriodRQ>';
	
	$ch = curl_init($sUrl);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "requestXML=".$sXml);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$sResponse = curl_exec($ch);
	
	$hotel_prices = new SimpleXMLElement($sResponse);
	
	if(($hotel_prices->{'header'}->status=='200') and ($hotel_prices->{'header'}->msg=='successfull')) {
		$upd_hotels = "UPDATE import_hoteluri SET data_update = NOW(), preturi_disponibile = '1' WHERE id_furnizor = '".$id_furnizor."' AND id_hotel = '".$hotel_code."' ";
		$que_hotels = mysql_query($upd_hotels) or die(mysql_error());
		@mysql_free_result($que_hotels);
		
		foreach($hotel_prices->exports[0]->service->periods->item as $k_data_start => $v_data_start) {
			$data_start[$k_data_start] = (string) $v_data_start->{'begin'};
			$preturi['data_start'][] = date('Y-m-d',strtotime(str_replace('/','.',$data_start[$k_data_start])));
		}
		
		foreach($hotel_prices->exports[0]->service->periods->item as $k_data_end => $v_data_end) {
			$data_end[$k_data_end] = (string) $v_data_end->{'end'};
			$preturi['data_end'][] = date('Y-m-d',strtotime(str_replace('/','.',$data_end[$k_data_end])));
		}
		
		foreach($hotel_prices->exports[0]->service->rooms->item as $k_row => $v_row) {
			if((string) $v_row->{'comb'} == '2+0+0+0') {
				$preturi['meals'][] = (string) $v_row->{'meal'};
				$j=0;
				foreach($v_row->prices->{'p'} as $k_pret => $v_pret) {
					$preturi['rooms'][(string) $v_row->{'meal'}][(string) $v_row->{'name'}][$j] = (string) $v_pret->{'price'};
					$j++;
				}
			}
		}
		$preturi['meals'] = array_unique($preturi['meals']);
		
		foreach($hotel_prices->exports[0]->service->policies->item as $k_policy => $v_policy) {
			$preturi['informatii'][(string) $v_policy->{'name'}] = (string) $v_policy->{'descr'};		
		}
		
		$_SESSION['preturi'] = $preturi;
	}
}

if(isset($_POST['adauga_coresp_camere'])) {
	foreach($_POST['tip_cam'] as $k_tc => $v_tc) {
		$tc[$k_tc] = explode("===", $v_tc);
		
		if($v_tc) {
			$sel_tip_camera = "SELECT * FROM tip_camera_corespondent WHERE den_camera = '".$tc[$k_tc][0]."' ";
			$que_tip_camera = mysql_query($sel_tip_camera) or die(mysql_error());
			if(mysql_num_rows($que_tip_camera)==0) {
				$ins_tip_cam = "INSERT INTO tip_camera_corespondent SET den_camera = '".$tc[$k_tc][0]."', id_camera_corespondent = '".$tc[$k_tc][1]."' ";
				$que_tip_cam = mysql_query($ins_tip_cam) or die(mysql_error());
				@mysql_free_result($ins_tip_cam);
			}
		}
	}
}

if(isset($_POST['insert_prices']) or isset($_POST['connect_hotel'])) {
	
  if(isset($_POST['insert_prices'])) {
	$perr = explode(';', $_POST['tip_masa']);
	$ii = $perr[1];
	$tip_masa = $perr[0];
	$hotel_code = $_POST['hotel_code'];
	$id_oferta = $_POST['oferta'];
	
	if(isset($_POST['delete_prices']) and $_POST['delete_prices']=='da') {
		$del_preturi = "DELETE FROM data_pret_oferta WHERE id_oferta = '".$id_oferta."' ";
		$que_preturi = mysql_query($del_preturi) or(mysql_error());
		@mysql_free_result($que_preturi);
	}
	
	/*$del_autoupd = "DELETE FROM preturi_autoupdate WHERE id_oferta = '".$id_oferta."' ";
	$que_autoupd = mysql_query($del_autoupd) or die(mysql_error());
	@mysql_free_result($que_autoupd);*/
			
	foreach($_POST['id_rooms'][$ii] as $k_rooms => $v_rooms) {
		$sel_rooms1 = "SELECT MAX(ordine) AS max_ordine FROM camere_hotel WHERE id_hotel = '".$id_hotel."' ";
		$que_rooms1 = mysql_query($sel_rooms1);
		$row_rooms1 = mysql_fetch_array($que_rooms1);

		$sel_rooms2 = "SELECT id_camera FROM camere_hotel WHERE id_hotel = '".$id_hotel."' AND id_camera = '".$v_rooms."' ";
		$que_rooms2 = mysql_query($sel_rooms2);
		if(mysql_num_rows($que_rooms2)==0) {
			$ins_room = "INSERT INTO camere_hotel (id_hotel, id_camera, ordine) VALUES ('".$id_hotel."', '".$v_rooms."', '".($row_rooms1['max_ordine']+1)."') ";
			$que_room = mysql_query($ins_room) or die(mysql_error());
			@mysql_free_result($que_room);
		}
		
		$sel_tipcamera = "SELECT den_camera FROM tip_camera_corespondent INNER JOIN camere_hotel ON camere_hotel.id_camera = tip_camera_corespondent.id_camera_corespondent WHERE camere_hotel.id_camera = '".$v_rooms."' AND camere_hotel.id_hotel = '".$id_hotel."' ";
		$que_tipcamera = mysql_query($sel_tipcamera);
		$row_tipcamera = mysql_fetch_array($que_tipcamera);

		if(isset($_POST['oferta'])) {
			foreach($_SESSION['preturi']['rooms'][$tip_masa][$_SESSION['camere'][$v_rooms]] as $k_price => $v_price) {
				$price_moneda = 'EURO';
				$price_adult = ($v_price/2) * $comision_procent;
				
				$ins_prices = "INSERT INTO data_pret_oferta (id_oferta, data_start, data_end, tip_camera, pret, moneda, id_hotel) VALUES ('".$id_oferta."', '".$_SESSION['preturi']['data_start'][$k_price]."', '".$_SESSION['preturi']['data_end'][$k_price]."', '".$v_rooms."', '".$price_adult."', '".$price_moneda."', '".$id_hotel."') ";
				$que_prices = mysql_query($ins_prices) or die(mysql_error());
				@mysql_free_result($que_prices);
				//echo $ins_prices.'<br>';
			}
			
			/*$ins_autoupdate = "INSERT INTO preturi_autoupdate (id_oferta, id_furnizor, perioada, tip_masa, hotel_code, plecare, transport, id_rooms, camere, id_pachet) VALUES ('".$id_oferta."', '".$id_furnizor."', '".$perioada."', '".$tip_masa."', '".$hotel_code."', '".$plecare."', '".$transport."', '".$v_rooms."', '".$_SESSION['camere'][$v_rooms]."', '".$pachet."') ";
			$que_autoupdate = mysql_query($ins_autoupdate) or die(mysql_error());
			@mysql_free_result($que_autoupdate);*/
			$ins_autoupdate = "INSERT INTO preturi_autoupdate (id_oferta, id_furnizor, hotel_code, autoupdate) VALUES ('".$id_oferta."', '".$id_furnizor."', '".$hotel_code."', 'nu') ";
			$que_autoupdate = mysql_query($ins_autoupdate) or die(mysql_error());
			@mysql_free_result($que_autoupdate);
	
		}
	}
  } else if(isset($_POST['connect_hotel'])) {
	
	$id_oferta = $_POST['oferta'];
	$hotel_code = $_POST['hotel_code'];
	
	$del_autoupd = "DELETE FROM preturi_autoupdate WHERE id_oferta = '".$id_oferta."' ";
	$que_autoupd = mysql_query($del_autoupd) or die(mysql_error());
	@mysql_free_result($que_autoupd);
			
	$ins_autoupdate = "INSERT INTO preturi_autoupdate (id_oferta, id_furnizor, hotel_code, autoupdate) VALUES ('".$id_oferta."', '".$id_furnizor."', '".$hotel_code."', 'nu') ";
	$que_autoupdate = mysql_query($ins_autoupdate) or die(mysql_error());
	@mysql_free_result($que_autoupdate);
	
  }
	
	$upd_offer = "UPDATE oferte SET online_prices = 'da' WHERE id_oferta = '".$id_oferta."' ";
	$que_offer = mysql_query($upd_offer) or die(mysql_error());
	@mysql_free_result($que_offer);
			
	unset($_SESSION['preturi']);
	unset($_SESSION['comision_procent']);
	unset($_SESSION['camere']);
	unset($_SESSION['hotel_code']);
	
	echo '<script>alert("Preturile au fost introduse!"); document.location.href="/adm/editare_sejur.php?pas=2&oferta='.$id_oferta.'";</script>';
}
//echo '<pre>';print_r($hotel_prices);echo '</pre>';

?>
<h1><?php echo $param['denumire']; ?> - Import Melitours</h1>

<br>
<h2>Creere corespondenta intre Hoteluri</h2>
<br>

<form action="" method="post">
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="default">
  <tr>
    <th valign="top" align="right" width="160">Hotel Ocaziituristice.ro:</th>
    <td><strong><?php echo $param['denumire'].' '.$param['stele'].'* / '.$param['denumire_localitate'].' / '.$param['denumire_zona'].' / '.$param['denumire_tara']; ?></strong></td>
  </tr>
  <tr>
    <th valign="top" align="right">Destinatie Melitours:</th>
    <td><select class="select_simple" onChange="window.location='<?php echo $_SERVER['PHP_SELF'].'?pas=oferte&hotel='.$_GET['hotel']; ?>'+this.value">
      <option value="">-- Selecteaza destinatia --</option>
    <?php
	$sel_destinatie = "SELECT localitati_corespondent.id, localitati_corespondent.nume_furnizor
	FROM import_hoteluri
	INNER JOIN localitati_corespondent ON localitati_corespondent.id = import_hoteluri.city
	WHERE localitati_corespondent.id_furnizor = '".$id_furnizor."'
	GROUP BY localitati_corespondent.nume_furnizor
	ORDER BY localitati_corespondent.nume_furnizor ";
	$que_destinatie = mysql_query($sel_destinatie);
	while($row_destinatie = mysql_fetch_array($que_destinatie)) {
		$destinatie = '&idL='.$row_destinatie['id'];
		echo '<option value="'.$destinatie.'"';
		if($row_destinatie['id']==$_GET['idL']) echo ' selected';
		echo '>'.ucwords(strtolower($row_destinatie['nume_furnizor'])).'</option>';
	} ?>
    </select></td>
  </tr>
  <tr>
	<th valign="top" align="right">Hotel Melitours:</th>
	<td><select class="select_simple" name="id_hot_other">
	<?php
	$sel_hotels = "SELECT
	import_hoteluri.id,
	import_hoteluri.id_hotel,
	import_hoteluri.nume_hotel,
	import_hoteluri.stele_hotel,
	import_hoteluri.id_hotel_corespondent,
	localitati_corespondent.nume_furnizor AS den_loc
	FROM import_hoteluri
	Inner Join localitati_corespondent ON localitati_corespondent.id = import_hoteluri.city
	WHERE import_hoteluri.id_furnizor = '".$id_furnizor."' ";
	if($_GET['idL']) $sel_hotels .= "AND localitati_corespondent.id = '".$_GET['idL']."' ";
	$sel_hotels .= "ORDER BY import_hoteluri.nume_hotel ASC
	";
	$que_hotels = mysql_query($sel_hotels);
	while($row_hotels = mysql_fetch_array($que_hotels)) {
		echo '<option value="'.$row_hotels['id'].'"';
		if($row_hotels['id_hotel_corespondent']==$id_hotel) {
			echo ' selected';
		}
		echo '>'.$row_hotels['nume_hotel'].' '.$row_hotels['stele_hotel'].'* - '.$row_hotels['den_loc'].'</option>';
	}
	?>
	</select></td>
  </tr>
  <tr>
    <th>&nbsp;</th>
    <td><input type="submit" name="adauga_coresp_hotel" value="Salveaza corespondenta" class="submit" /></td>
  </tr>
</table>
</form>

<?php if(sizeof($hotel_codes)>0 and !isset($_SESSION['hotel_code'])) { ?>
<br>
<div class="red">
<h1 class="red">ATENTIE! Exista mai multe corespondente!</h1>
<h3 class="red">Va rugam alegeti unul din hotelurile de mai jos pentru care doriti sa introduceti preturile.</h3>
<form action="" method="post">
<?php foreach($hotel_codes as $k_hc => $v_hc) {
	$sel_hotelul = "SELECT * FROM import_hoteluri WHERE id_furnizor = '".$id_furnizor."' AND id_hotel = '".$v_hc."' ";
	$que_hotelul = mysql_query($sel_hotelul);
	$row_hotelul = mysql_fetch_array($que_hotelul);
	
	echo '<label class="black bold""><input type="radio" value="'.$v_hc.'" name="codul_hotelului"> '.$row_hotelul['nume_hotel'].' '.$row_hotelul['stele_hotel'].'*</label><br>';
} ?>
  <input type="submit" name="alege_hotelul" value="Alege Hotelul" class="submit" />
</form>
</div>
<?php } ?>

<?php
if(mysql_num_rows($que_hotel)<1) {
	echo '<h3 class="red">Va rugam asignati inainte hotelul! (vezi mai sus)</h3>';
} else {
?>

<br>
<h2 id="clickConnect1" style="cursor:pointer;" title="Click pentru a deschide"><span class="bold red">&darr;</span> Preturi disponibile la <?php echo $param['denumire']; ?> <span class="float-right smaller-08em red bold">click pentru a deschide</span></h2>
<br>
<div id="connect1" style="display:<?php if(isset($_SESSION['preturi'])) echo 'block'; else echo 'none'; ?>;">

<h2>Alegere perioada import preturi</h2>
<br>
<script type="text/javascript">
$(function() {
	$("#start_date").datepicker({
		numberOfMonths: 3,
		dateFormat: "yy-mm-dd",
		showButtonPanel: true
	});
	$("#end_date").datepicker({
		numberOfMonths: 3,
		dateFormat: "yy-mm-dd",
		showButtonPanel: true
	});
});
</script>
<form action="" method="post">
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="default">
  <tr>
    <th valign="top" align="right"><label for="start_date">Data start:</label></th>
    <td><input type="text" name="start_date" id="start_date" class="date" value="<?php echo $start_date; ?>"></td>
  </tr>
  <tr>
    <th valign="top" align="right"><label for="end_date">Data end:</label></th>
    <td><input type="text" name="end_date" id="end_date" class="date" value="<?php echo $end_date; ?>"></td>
  </tr>
  <tr>
    <th valign="top" align="right"><label for="comision_procent">Comision procent:</label></th>
    <td><input type="text" name="comision_procent" id="comision_procent" class="mic" value="<?php echo $comision_procent; ?>"> % (se completeaza cu zecimale cu punct adaosul agentiei - se vor include si eventualele reduceri, ca la preturi pivot)</td>
  </tr>
  <tr>
    <th width="160">&nbsp;</th>
    <td><input type="submit" name="perioada_preturi" value="Cauta preturi in perioada aleasa" class="submit" /></td>
  </tr>
</table>
</form>

<?php if(isset($_SESSION['preturi'])) { ?>
<br>
<h2>Creere corespondenta intre tipul camerelor</h2>
<br>
<form action="" method="post">
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="default">
<?php
foreach($_SESSION['preturi']['rooms'][$_SESSION['preturi']['meals'][0]] as $key_room => $value_room) {
?>
  <tr>
    <th valign="top" align="right"><?php echo $key_room; ?>:</th>
    <td>
    <?php
    $sel_tipCam = "SELECT * FROM tip_camera_corespondent LEFT JOIN tip_camera ON tip_camera.id_camera = tip_camera_corespondent.id_camera_corespondent WHERE den_camera = '".$key_room."' ";
    $que_tipCam = mysql_query($sel_tipCam);
    $row_tipCam = mysql_fetch_array($que_tipCam);
    if(mysql_num_rows($que_tipCam)>0) {
        $err += 0;
        /*echo '<strong class="red">'.$row_tipCam['denumire'].'</strong>';*/
        $key_camere = $row_tipCam['id_camera_corespondent'];
        $_SESSION['camere'][$key_camere] = $key_room;
    } else {
        $err += 1;
    }
    echo '<select name="tip_cam[]"><option value="" selected>--</option>';
    $sel_tipcam = "SELECT * FROM tip_camera ORDER BY denumire ";
    $que_tipcam = mysql_query($sel_tipcam);
    while($row_tipcam = mysql_fetch_array($que_tipcam)) {
        echo '<option value="'.$key_room.'==='.$row_tipcam['id_camera'].'"';
        if($row_tipCam['id_camera']==$row_tipcam['id_camera']) echo ' selected';
        echo '>'.$row_tipcam['denumire'].'</option>';
    }
    echo '</select>';
    ?></td>
  </tr>
<?php } ?>
  <tr>
    <th width="160">&nbsp;</th>
    <td><input type="submit" name="adauga_coresp_camere" value="Salveaza corespondenta" class="submit" /></td>
  </tr>
</table>
</form>

<?php if(isset($_SESSION['camere'])) { ?>
<br>
<h2>Preturi disponibile la <?php echo $param['denumire']; ?></h2>
<br>

<form action="" method="post">
  <input type="hidden" name="hotel_code" value="<?php echo $hotel_code; ?>">
  <label for="oferta" class="bigger-12em red"><strong>Selectati oferta:</strong></label>
  <select name="oferta" id="oferta">
    <option value="">--</option>
    <?php $selALO = "SELECT denumire, id_oferta, nr_nopti FROM oferte WHERE id_hotel = '".$id_hotel."' GROUP BY id_oferta ORDER BY denumire ";
	$queALO = mysql_query($selALO) or die(mysql_error());
	while($rowALO = mysql_fetch_array($queALO)) {
		echo '<option value="'.$rowALO['id_oferta'].'">'.$rowALO['denumire'].', '.$rowALO['nr_nopti'].' nopti - '.$rowALO['id_oferta'].'</option>';
	} @mysql_free_result($queALO); ?>
  </select>
  <br>
  <label class="bold bigger-11em">Sterge preturile curente de la oferta de mai sus? <input type="checkbox" name="delete_prices" checked value="da"></label>
  <br><br>
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
    <tr>
<?php $ic=0;
foreach($_SESSION['preturi']['meals'] as $key_meals => $value_meals) {
	echo '<td valign="top" align="left">';
	echo '<label><input type="radio" name="tip_masa" value="'.$value_meals.';'.$ic.'">  <strong class="bigger-13em">'.$value_meals.'</strong> (tipul de masa)</label>';
	
	foreach($_SESSION['camere'] as $key_rooms => $value_rooms) {
		echo '<div style="padding:10px 5px 5px 30px;">';
		echo '<label><input type="checkbox" name="id_rooms['.$ic.'][]" value="'.get_id_camera($value_rooms).'"> <strong>'.$value_rooms.'</strong> ('.sizeof($_SESSION['preturi']['rooms'][$value_meals][$value_rooms]).' perioade)</label><br>';
		echo '</div>';
	}

	echo '<div style="padding:5px; line-height:1.4em;"><strong class="red">Bifati camerele pentru care doriti sa introduceti preturi</strong><br>(dupa ce ati selectat optiunile de mai sus)</div>';
	echo '</td>';
	$ic++;
}
?>
	</tr>
<?php
if(sizeof($_SESSION['preturi']['informatii'])>0) {
	echo '<tr>';
	echo '<td colspan="'.sizeof($_SESSION['preturi']['meals']).'">';
	foreach($_SESSION['preturi']['informatii'] as $key_informatii => $value_informatii) {
		echo '<div class="infos bigger-11em"><strong>'.$key_informatii.'</strong><br>'.$value_informatii.'</div><br>';
	}
	echo '</td>';
	echo '</tr>';
}
?>
  </table>
  <div style="padding:5px;"><input type="submit" name="insert_prices" class="submit" value="Insereaza preturile pentru camerele selectate"></div>
</form>
<?php } ?>

<?php } ?>

<?php } ?>
</div>

<br>
<h2 id="clickConnect2" style="cursor:pointer;" title="Click pentru a deschide"><span class="bold red">&darr;</span> Conecteaza <?php echo $param['denumire']; ?> cu furnizorul <span class="float-right smaller-08em red bold">click pentru a deschide</span></h2>
<br>

<form action="" method="post" id="connect2" style="display:none;">
  <input type="hidden" name="hotel_code" value="<?php echo $hotel_code; ?>">
  
  <table border="0" cellspacing="2" cellpadding="2">
    <tr>
	  <td>
  <label for="oferta" class="bigger-12em red"><strong>Selectati oferta:</strong></label>
	  </td>
      <td>
  <select name="oferta" id="oferta">
    <option value="">--</option>
    <?php $selALO = "SELECT denumire, id_oferta, nr_nopti FROM oferte WHERE id_hotel = '".$id_hotel."' GROUP BY id_oferta ORDER BY denumire ";
	$queALO = mysql_query($selALO) or die(mysql_error());
	while($rowALO = mysql_fetch_array($queALO)) {
		echo '<option value="'.$rowALO['id_oferta'].'">'.$rowALO['denumire'].', '.$rowALO['nr_nopti'].' nopti - '.$rowALO['id_oferta'].'</option>';
	} @mysql_free_result($queALO); ?>
  </select>
	  </td>
    </tr>
  </table>
  <br><br>
  
  <div style="padding:5px;"><input type="submit" name="connect_hotel" class="submit" value="Conecteaza oferta noastra cu furnizorul"></div>
</form>

<script type="text/javascript">
$("#clickConnect2").click(function() {
	$("#connect2").toggle("fast");
});
$("#clickConnect1").click(function() {
	$("#connect1").toggle("fast");
});
</script>
