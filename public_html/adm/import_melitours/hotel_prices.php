<?php
$id_hotel_import = $_GET['id_hot_import'];

$sel_hotel = "SELECT * FROM import_hoteluri WHERE id_furnizor = '".$_SESSION['id_furnizor']."' AND id_hotel = '".$id_hotel_import."' ";
$que_hotel = mysql_query($sel_hotel) or die(mysql_error());
$row_hotel = mysql_fetch_assoc($que_hotel);

$sXml = '<pricePeriodRQ>';
$sXml .= $login_data;	
$sXml .= '<Language>EN</Language>
<pricePeriodInfo>
<hotel>'.$id_hotel_import.'</hotel>
<roomType></roomType>
<fromd>'.date('Y').'-01-01</fromd>
<tod>'.date('Y').'-12-31</tod>
</pricePeriodInfo>
</pricePeriodRQ>';

$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "requestXML=".$sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);

$hotel_prices = new SimpleXMLElement($sResponse);

if(($hotel_prices->{'header'}->status=='200') and ($hotel_prices->{'header'}->msg=='successfull')) {
	
$upd_hotels = "UPDATE import_hoteluri SET data_update = NOW(), preturi_disponibile = '1' WHERE id_furnizor = '".$_SESSION['id_furnizor']."' AND id_hotel = '".$id_hotel_import."' ";
$que_hotels = mysql_query($upd_hotels) or die(mysql_error());
@mysql_free_result($que_hotels);

$err = '';

$preturi['id_hotel_furnizor'] = $row_hotel['id_hotel'];
$preturi['nume_hotel'] = $row_hotel['nume_hotel'];
$preturi['stele'] = $row_hotel['stele_hotel'];
$preturi['id_hotel_corespondent'] = $row_hotel['id_hotel_corespondent'];
$preturi['destinatie'] = (string) $hotel_prices->exports[0]->service->city;
$preturi['moneda'] = (string) $hotel_prices->exports[0]->service->curr;

foreach($hotel_prices->exports[0]->service->periods->item as $k_data_start => $v_data_start) {
	$data_start[$k_data_start] = (string) $v_data_start->{'begin'};
	$preturi['data_start'][] = date('Y-m-d',strtotime(str_replace('/','.',$data_start[$k_data_start]))); //data start
}

foreach($hotel_prices->exports[0]->service->periods->item as $k_data_end => $v_data_end) {
	$data_end[$k_data_end] = (string) $v_data_end->{'end'};
	$preturi['data_end'][] = date('Y-m-d',strtotime(str_replace('/','.',$data_end[$k_data_end]))); //data end
}

$i = 0;
foreach($hotel_prices->exports[0]->service->rooms->item as $k_row => $v_row) {
	$i++;
	$denumire_camera[$k_row] = (string) $v_row->name;
	$id_camera[$k_row] = get_id_camera($denumire_camera[$k_row]);
	if($id_camera[$k_row]=='') {
		$err .= '<h3 class="red">EROARE ! Nu toate camerele au corespondent!</h3><strong>- '.$denumire_camera[$k_row].'</strong><br><br>';
	} else {
		$preturi['id_camera'][$i] = $id_camera[$k_row]; //id camera
	}
	
	$preturi['tip_masa'][$i] = (string) $v_row->meal; //tip masa
	
	$grad_oc = (string) $v_row->comb;
	$grad_ocupare = explode('+',$grad_oc);
	$preturi['nr_adulti'][$i] = $grad_ocupare[0]; //nr adulti
	$nr_copii[$i] = $grad_ocupare[1] + $grad_ocupare[2] + $grad_ocupare[3];
	$preturi['nr_copii'][$i] = $nr_copii[$i]; //nr copii
	if($nr_copii[$i]>0) {
		$age_copii = explode('/',$grad_ocupare[4]);
		$nrc = 0;
		$i1 = 0;
		$i2 = 0;
		$i3 = 0;
		if($grad_ocupare[1]>0) {
			for($i1=0; $i1<$grad_ocupare[1]; $i1++) {
				$preturi['varste_copii'][$i][$i1] = $age_copii[$nrc]; //varste copii
			}
			$nrc += 1;
		}
		if($grad_ocupare[2]>0) {
			for($i2=$i1; $i2<($grad_ocupare[2]+$i1); $i2++) {
				$preturi['varste_copii'][$i][$i2] = $age_copii[$nrc]; //varste copii
			}
			$nrc += 1;
		}
		if($grad_ocupare[3]>0) {
			for($i3=$i2; $i3<($grad_ocupare[3]+$i2); $i3++) {
				$preturi['varste_copii'][$i][$i3] = $age_copii[$nrc]; //varste copii
			}
		}
	} else {
		$preturi['varste_copii'][$i][0] = ''; //varste copii
	}
	
	foreach($v_row->prices->{'p'} as $k_pret => $v_pret) {
		$price_value = (string) $v_pret;
		if($nr_copii[$i]=='0') {
			$preturi['pret_pivot'][$i][] = $price_value / $grad_ocupare[0];
		}
		$preturi['pret'][$i][] = $price_value;
	}
}

if(strlen($err)>0) {
	echo '<script>alert(\'Au aparut erori.\nPreturile nu au fost importate.\'); </script>';
	echo $err;
	echo '<div class="infos">Camerele se asigneaza din baza de date in tabelul <strong>TIP_CAMERA_CORESPONDENT</strong> cu cele din tabelul <strong>TIP_CAMERA</strong></div><br>';
	echo '<button class="buttons" onClick="window.location.reload(true);">Preia datele din nou</button>';
} else {
?>
<h1>Introducere preturi pentru <strong class="red"><?php echo $preturi['nume_hotel']; ?></strong></h1>
<br><br>
<form action="<?php echo $sitepath_adm; ?>import_melitours.php?pas=2" method="post">
	<strong>Denumire:</strong> <input type="text" name="denumire" value="<?php echo $denumire; ?>" class="mare">
    <br><br>
    <strong>Tip Masa:</strong><br>
    <?php $tip_masa = array_unique($preturi['tip_masa']);
	foreach($tip_masa as $k_tip_masa => $v_tip_masa) { ?>
    <div style="padding:5px;"><label><input type="radio" name="tip_masa" value="<?php echo $v_tip_masa; ?>"> <?php echo $v_tip_masa; ?></label></div>
    <?php } ?>
    <br>
    <input type="submit" name="adauga" value="Pasul urmator" class="buttons" />
</form>
<?php
	$_SESSION['preturi'] = $preturi;
}


} else {
	echo '<h1>Acest hotel nu are preturi disponibile.</h1>';
echo $row_hotel['id_hotel'];
	$upd_hotels = "UPDATE import_hoteluri SET data_update = NOW(), preturi_disponibile = '0' WHERE id_furnizor = '".$_SESSION['id_furnizor']."' AND id_hotel = '".$id_hotel_import."' ";
	$que_hotels = mysql_query($upd_hotels) or die(mysql_error());
	@mysql_free_result($que_hotels);
}
//echo '<pre>';print_r($preturi);echo '</pre>';
?>
