<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
  <h1>Raport Vanzare</h1>




  
 <?php 
 if($_SESSION['mail']=='sofia@ocaziituristice.ro'|| $_SESSION['mail']=='razvan@ocaziituristice.ro' || $_SESSION['mail']=='contabilitate@ecd-conta.ro') 
{
 $vazari_facturi=array();
  $adaos=array();
 $sel_facturi="select sum(facturi.valoare) as suma_totala, facturi.valoare,facturi.tip_operatie, facturi.tip_tranzactie, facturi.id_rezervare,cerere_rezervare.oferta_denumire,
facturi.valuta,  month(cerere_rezervare.data_adaugarii) as luna, year(cerere_rezervare.data_adaugarii) as an
from facturi
inner join cerere_rezervare on facturi.id_rezervare=cerere_rezervare.id_cerere
WHERE
cerere_rezervare.tip =  'rezervare' AND
( cerere_rezervare.stare =  'finalizata' OR cerere_rezervare.stare =  'platit partial' OR cerere_rezervare.stare =  'trebuie trimis voucher' ) and year(cerere_rezervare.data_adaugarii)>2015
AND
month(cerere_rezervare.data_adaugarii) >=  '1' 
 
Group by facturi.valuta, month(cerere_rezervare.data_adaugarii), year(cerere_rezervare.data_adaugarii) ,facturi.tip_operatie,tip, facturi.tip_tranzactie";
// pentru a vedea rezervarle fara furnizor se adauga si group id_rezervaare

//$sel_facturi="select facturi.valoare,facturi.tip_operatie, facturi.tip_tranzactie, facturi.id_rezervare,cerere_rezervare.oferta_denumire, facturi.valuta, id_factura, month(cerere_rezervare.data_adaugarii) as luna, year(cerere_rezervare.data_adaugarii) as an from facturi inner join cerere_rezervare on facturi.id_rezervare=cerere_rezervare.id_cerere WHERE cerere_rezervare.tip =  'rezervare' AND ( cerere_rezervare.stare =  'finalizata' OR cerere_rezervare.stare =  'platit partial' OR cerere_rezervare.stare =  'trebuie trimis voucher' ) and year(cerere_rezervare.data_adaugarii)>2016 AND month(cerere_rezervare.data_adaugarii) >=  '6' ";


 $que_facturi= mysql_query($sel_facturi) or die(mysql_error());
  while ($row_facturi = mysql_fetch_array($que_facturi)){
 //$vanzari_facturi[$row_facturi['luna']][$row_facturi['an']][$row_facturi['id_rezervare'].'-'.$row_facturi['oferta_denumire']][$row_facturi['valuta']][$row_facturi['tip_operatie']][$row_facturi['tip_tranzactie']]=$row_facturi['suma_totala'];

$vanzari_facturi[$row_facturi['luna']][$row_facturi['an']][$row_facturi['valuta']][$row_facturi['tip_operatie']][$row_facturi['tip_tranzactie']]=$row_facturi['suma_totala'];

// aici pentru lista facturi 
//$vanzari_facturi[$row_facturi['an']][$row_facturi['luna']][$row_facturi['id_rezervare']][$row_facturi['tip_operatie']][$row_facturi['tip_tranzactie']]=$row_facturi['valoare'].' '.$row_facturi['valuta'];

  }
//echo '<pre>'; print_r($vanzari_facturi); echo '</pre>';
 
?> 
 <table width="100%" border="1" cellspacing="0" cellpadding="2">
  <?php for ($i = 1; $i <= 12;) {?>
  <tr>
    <td width="31%" bgcolor="#3399FF">Luna <?php  echo $i;?></td>
    <td colspan="2" align="center" bgcolor="#3399FF">2018</td>
    <td colspan="2" bgcolor="#3399FF">2017</td>
    <td width="7%" bgcolor="#3399FF">Procent</td>
  </tr>
  <tr>
    <td> Luna <?php  echo $i;?></td>
    <td width="25%">intrare furnizori eur</td>
    <td width="14%"> <?php echo  $vanzari_facturi[$i]['2018']['EUR']['furnizor']['intrare']?> EURO</td>
    <td width="23%">intrare furnizori eur</td>
    <td width="23%"><?php echo  $vanzari_facturi[$i]['2017']['EUR']['furnizor']['intrare']?> EURO</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>intrare furnizori lei</td>
    <td><?php echo  $vanzari_facturi[$i]['2018']['RON']['furnizor']['intrare']?> LEI</td>
    <td>intrare furnizori lei</td>
    <td><?php echo  $vanzari_facturi[$i]['2017']['RON']['furnizor']['intrare']?> LEI</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Total Furnizori:</td>
    <td><?php echo  $total_furnizor[$i]['2018']=$vanzari_facturi[$i]['2018']['RON']['furnizor']['intrare']+final_price_lei($vanzari_facturi[$i]['2018']['EUR']['furnizor']['intrare'], 'EURO', $adaos=NULL)?> LEI</td>
    <td>Total Furnizori:</td>
    <td><?php echo  $total_furnizor[$i]['2017']=$vanzari_facturi[$i]['2017']['RON']['furnizor']['intrare']+final_price_lei($vanzari_facturi[$i]['2017']['EUR']['furnizor']['intrare'], 'EURO', $adaos=NULL)?> LEI</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>iesire client eur</td>
    <td><?php echo  $vanzari_facturi[$i]['2018']['EUR']['client']['iesire']?> EURO</td>
    <td>iesire client eur</td>
    <td><?php echo  $vanzari_facturi[$i]['2017']['EUR']['client']['iesire']?> EURO</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>iesire client lei</td>
    <td><?php echo  $vanzari_facturi[$i]['2018']['RON']['client']['iesire']?> LEI</td>
    <td>iesire client lei</td>
    <td><?php echo  $vanzari_facturi[$i]['2017']['RON']['client']['iesire']?> LEI</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Total Clienti</td>
    <td bgcolor="#99FF00"><?php echo  $total_client[$i]['2018']=$vanzari_facturi[$i]['2018']['RON']['client']['iesire']+final_price_lei($vanzari_facturi[$i]['2018']['EUR']['client']['iesire'], 'EURO', $adaos=NULL)?> LEI</td>
    <td>Total Clienti</td>
    <td bgcolor="#99FF00"><?php echo  $total_client[$i]['2017']=$vanzari_facturi[$i]['2017']['RON']['client']['iesire']+final_price_lei($vanzari_facturi[$i]['2017']['EUR']['client']['iesire'], 'EURO', $adaos=NULL)?> LEI</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Adaos</td>
    <td><?php echo $adaos[$i]['2018']=$total_client[$i]['2018']-$total_furnizor[$i]['2018']?> Lei</td>
    <td>Adaos</td>
    <td><?php echo $adaos[$i]['2017']=$total_client[$i]['2017']-$total_furnizor[$i]['2017']?> Lei</td>
    <td nowrap="nowrap"><?php 
	if(($adaos[$i]['2018']>$adaos[$i]['2017']) and ($adaos[$i]['2018']>0) and ($adaos[$i]['2017']>0) )
	{echo "crestere +";
		echo (round(100*$adaos[$i]['2018']/$adaos[$i]['2017'])-100)."%";}
	else  if( ($adaos[$i]['2018']>0) and ($adaos[$i]['2017']>0) )	{
		echo "scadere -";
		echo (round(100*$adaos[$i]['2017']/$adaos[$i]['2018'])-100)."%";}
	
	?></td>
  </tr>

  <?php
  $serie_adaos_2014.=$adaos[$i]['2017'].","; 
  $serie_adaos_2015.=$adaos[$i]['2018'].","; 
  $i++;
  
  }?>
  
</table>

<h1>Grafic din sitem </h1> 
<div id="raport" style="width:100%; height:400px;"></div>

<h1>Grafic din balante</h1> 
<div id="raport_balante" style="width:100%; height:400px;"></div>

<script src="/js/highcharts/highcharts.js"></script>
<script>
$(function () {
	$('#raport').highcharts({
		chart: { type: 'line' },
		title: { text: '' },
		subtitle: { text: '' },
		xAxis: { categories: ['Ian', 'Feb', 'Mar', 'Apr', 'Mai', 'Iun', 'Iul', 'Aug', 'Sep', 'Oct', 'Noi', 'Dec'] },
		yAxis: { title: { text: 'valoare lei' } },
		plotOptions: { line: {dataLabels: { enabled: true}, enableMouseTracking: false } },
		series: [{
            name: 'Adaos 2014',
            data: [<?php echo $serie_adaos_2017;?>]
        }, {
            name: 'Adaos 2015',
            data: [<?php echo $serie_adaos_2018;?>]
        },  {
            name: 'Cheltuieli total 2014',
            data: [7896,7585,12545,15352,13342,1506,17204,13870,11857]
        },
	 {
            name: 'Cheltuieli total 2015',
            data: [10160,13075,15388,13021,17931,22784]
        }
		]
	});
	
$('#raport_balante').highcharts({
		chart: { type: 'line' },
		title: { text: '' },
		subtitle: { text: '' },
		xAxis: { categories: ['Ian', 'Feb', 'Mar', 'Apr', 'Mai', 'Iun', 'Iul', 'Aug', 'Sep', 'Oct', 'Noi', 'Dec'] },
		yAxis: { title: { text: 'valoare lei' } },
		plotOptions: { line: {dataLabels: { enabled: true}, enableMouseTracking: false } },
		series: [ {
            name: 'Cheltuieli total 2014',
            data: [7896,7585,12545,15352,13342,14506,17204,13870,11057,6351,10469,9654]
        },
		
		 {
            name: 'venituri total 2014',
            data: [3332,6306,16088,9240,4611,12033,24692,14687,15410,5020,7396,23491]
        },
	
	
{
            name: 'venituri total 2015',
            data: [2842,9617,16527,16513,15227,19311]
        },
		
		{
            name: 'Cheltuieli total 2015',
            data: [10160,13075,15388]
        }
	 
		]
	});	
	
	
	
	
	
});
</script>



<?php }?>
<?php  echo '<pre>'; print_r($vanzari_bon); echo '</pre>';?>
  
  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'footer.php'); ?></div>
</body>
</html>