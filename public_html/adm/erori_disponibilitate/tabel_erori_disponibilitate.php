<script type="text/javascript" src="/js/jquery.jeditable.mini.js"></script>

<table cellpadding="2" cellspacing="2" border="1" width="100%" align="center" class="tabel">
  <tr>
    <th width="20" align="center" valign="top">#</th>
    <th align="center" valign="top" nowrap>Tip unitate</th>
    <th align="center" valign="top">Hotel</th>
    <th align="center" valign="top">Oferta</th>
    <th align="center" valign="top" nowrap>Data</th>
    <th align="center" valign="top">Nr. nopti</th>
    <th align="center" valign="top">Nr. adulti</th>
    <th align="center" valign="top">Nr. copii</th>
    <th align="center" valign="top" nowrap>IP</th>
    <th align="center" valign="top" nowrap>Data adaugare</th>
    <th align="center" valign="top">Solutionat</th>
    <th width="60" align="center" valign="top">ID</th>
  </tr>
<?php $alternate = false; $i=$st-1;
while($row=mysql_fetch_array($afisare_pr)) {
	$i++;
	
	if($row['solved']=='y') {
		$class_tr = 'normal closed';
		$icon = 'yes';
	} else {
		$class_tr = 'normal';
		$icon = 'no';
	}
	
	if($param['tip_unitate']=='Circuit') {
		$linkV = '/circuit/'.fa_link($row['nume_continent']).'/'.fa_link_oferta($row['denumire']).'-'.$row['id_oferta'].'.html';
	} else {
		$linkV = '/hoteluri-'.fa_link($row['denumire_localitate']).'/'.fa_link($row['nume']).'/'.fa_link_oferta($row['denumire']).'-'.$row['id_oferta'].'.html';
	}

?>
  <tr class="<?php echo $class_tr; ?>" onmouseout="this.className='<?php echo $class_tr; ?>'; if(document.getElementById('tools<?php echo $row['id']; ?>').style.visibility == 'visible'){ document.getElementById('tools<?php echo $row['id']; ?>').style.visibility = 'hidden'; }" onmouseover="this.className='hover'; if(document.getElementById('tools<?php echo $row['id']; ?>').style.visibility == 'hidden'){ document.getElementById('tools<?php echo $row['id']; ?>').style.visibility = 'visible'; }else{ document.getElementById('tools<?php echo $row['id']; ?>').style.display = 'hidden'; }">
    <td align="right" valign="top"><?php echo $i; ?></td>
    <td align="left" valign="top"><span class="black"><?php echo $row['tip_unitate']; ?></span></td>
    <td align="left" valign="top"><a href="<?php echo '/hoteluri-'.fa_link($row['denumire_localitate']).'/'.fa_link($row['nume']).'/'.'?plecdata='.date("d.m.Y",strtotime($row['data'])).'&pleczile='.$row['nr_nopti'].'&adulti='.$row['nr_adulti'].'&copii='.$row['nr_copii'].'#calc'; ?>" class="titluArticol" target="_blank"><?php echo $row['nume']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a></td>
    <td align="left" valign="top"><a href="<?php echo $linkV.'?plecdata='.$row['data'].'&pleczile='.$row['nr_nopti'].'&adulti='.$row['nr_adulti'].'&copii='.$row['nr_copii'].'#calc'; ?>" class="titluArticol" target="_blank"><?php echo $row['denumire']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a></td>
    <td align="center" valign="top" nowrap><span class="black"><?php echo $row['data']; ?></span></td>
    <td align="center" valign="top"><span class="black"><?php echo $row['nr_nopti']; ?></span></td>
    <td align="center" valign="top"><span class="black"><?php echo $row['nr_adulti']; ?></span></td>
    <td align="center" valign="top"><span class="black"><?php echo $row['nr_copii']; ?></span></td>
    <td align="center" valign="top" nowrap><?php echo $row['ip']; ?></td>
    <td align="center" valign="top"><?php echo $row['data_adaugare']; ?></td>
    <td align="center" valign="top">
<script type="text/javascript">
$(document).ready(function() {
	$('#solved<?php echo $row['id']; ?>').editable("/adm/erori_disponibilitate/editinplace_update.php", {
		loadurl : '/adm/erori_disponibilitate/load_stare.php',
		type   : "select",
		submit : "OK",
		style  : "inherit" 
	});
});
</script>
		  <div id="solved<?php echo $row['id']; ?>"><img src="images/<?php echo $icon; ?>.png"></div>
    </td>
    <td align="center" valign="top">
	  <?php echo $row['id']; ?><br>
      <button><a href="javascript: if(confirm('Sunteti sigur ca doriti sa stergeti acest rand ?')) { window.location.href='erori_disponibilitate.php?pas=1&sterge=<?php echo $row['id']; ?>'; } ">sterge</a></button>
    </td>
  </tr>
<?php } ?>
</table>
