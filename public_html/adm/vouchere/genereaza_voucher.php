<?php
if(isset($_POST['modifica'])) {
	$data=$_POST['data'];
	$persoane=$_POST['nr_adulti'].' adult';
	if($_POST['nr_adulti']>1) $persoane.='i';
	if($_POST['nr_copii']) $persoane.=' + '.$_POST['nr_copii'].' copi';
	if($_POST['nr_copii']>1) $persoane.='i'; elseif($_POST['nr_copii']=='1') $persoane.='l';
	$destinatie=$_POST['hotel_localitate'].', '.$_POST['hotel_tara'];
	$hotel=$_POST['hotel_denumire'].' '.$_POST['hotel_categorie'].'*';
	$checkin=$_POST['checkin'].' / '.$_POST['checkout'];
	
	$ins_rezervare = "INSERT INTO vouchere SET
	data='".$data."',
	persoane='".$persoane."',
	destinatie='".$destinatie."',
	hotel='".$hotel."',
	camera='".$_POST['nume_camera']."',
	checkin='".$checkin."',
	servicii='".$_POST['servicii']."',
	telefon='".$_POST['telefon']."',
	agent='".$_POST['agent']."',
	lang='".$_POST['lang']."'
	";
	$res_rezervare = mysql_query($ins_rezervare) or die (mysql_error());

	$sel_voucher="SELECT id_voucher FROM vouchere ORDER BY id_voucher DESC";
	$que_voucher=mysql_query($sel_voucher) or die(mysql_error());
	$row_voucher=mysql_fetch_array($que_voucher);
	$idv=$row_voucher['id_voucher'];

	$upd_rezervare = "UPDATE cerere_rezervare SET
	genereaza_voucher='y',
	id_voucher='".$idv."'
	WHERE id_cerere='".$_POST['idc']."'";
	$res_rezervare = mysql_query($upd_rezervare) or die (mysql_error());
	
	$sel_pasageri="SELECT id_pasager FROM pasageri WHERE id_rezervare='".$_POST['idc']."'";
	$que_pasageri=mysql_query($sel_pasageri) or die(mysql_error());
	while($row_pasageri=mysql_fetch_array($que_pasageri)) {
		$ins_legpas = "INSERT INTO leg_voucher_pasageri SET id_voucher='".$idv."', id_pasager='".$row_pasageri['id_pasager']."'";
		$rez_legpas = mysql_query($ins_legpas) or die (mysql_error());
	}

	$actiune_log = $_SESSION['nume'].' a generat Voucherul cu nr. "'.$idv.'"';
	$ins_logadmin = "INSERT INTO log_admin SET id_admin='".$_SESSION['id_user_adm']."', data=NOW(), actiune='$actiune_log'";
	$rez_logadmin = mysql_query($ins_logadmin) or die (mysql_error());
	
	echo '<meta http-equiv="refresh" content="0;URL=\'vouchere.php?pas=2&idv='.$idv.'\'">';
}
?>

<?php
$idc = $_GET['idc'];
?>

<?php
$sel_rez="SELECT cerere_rezervare.*, oferte.id_hotel FROM cerere_rezervare INNER JOIN oferte ON cerere_rezervare.id_oferta=oferte.id_oferta WHERE cerere_rezervare.id_cerere='$idc'";
$que_rez=mysql_query($sel_rez) or die(mysql_error());
$row_rez=mysql_fetch_array($que_rez);
?>

<?php
$servicii = 'Cazare '.$row_rez['nr_nopti'].' nopti, '.$row_rez['tip_masa'];
$nume_camera = $row_rez['nume_camera'];
?>

  <div style="margin:20px;">
  <h1>Genereaza Voucher pentru Rezervare nr. <?php echo $idc; ?></h1>
  <form action="" method="post">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td align="right" valign="middle"><label for="lang">Limba voucher:</label></td>
        <td align="left" valign="middle"><select id="lang" name="lang">
          <option value="RO">RO</option>
          <option value="EN">EN</option>
        </select></td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="data">Data voucher:</label></td>
        <td align="left" valign="middle"><input type="text" id="data" name="data" value="<?php echo date('Y-m-d H:i:s'); ?>" class="mediu" /></td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="nr_adulti">Persoane:</label></td>
        <td align="left" valign="middle"><input type="text" id="nr_adulti" name="nr_adulti" value="<?php echo $row_rez['nr_adulti']; ?>" class="mic" /> adulti + <input type="text" name="nr_copii" value="<?php echo $row_rez['nr_copii']; ?>" class="mic" /> copii</td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="hotel_localitate">Destinatie:</label></td>
        <td align="left" valign="middle"><input type="text" id="hotel_localitate" name="hotel_localitate" value="<?php echo $row_rez['hotel_localitate']; ?>" class="mediu" />, <input type="text" name="hotel_tara" value="<?php echo $row_rez['hotel_tara']; ?>" class="mediu" /></td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="hotel_denumire">Hotel:</label></td>
        <td align="left" valign="middle"><input type="text" id="hotel_denumire" name="hotel_denumire" value="<?php echo $row_rez['hotel_denumire']; ?>" class="mare" /> <input type="text" name="hotel_categorie" value="<?php echo $row_rez['hotel_categorie']; ?>" class="mic" />*</td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="nume_camera">Tip camera:</label></td>
        <td align="left" valign="middle"><input type="text" id="nume_camera" name="nume_camera" value="<?php echo $nume_camera; ?>" class="mediu" /></td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="checkin">Check in / Check out:</label></td>
        <td align="left" valign="middle"><input type="text" id="checkin" name="checkin" value="<?php echo date('d.m.Y',strtotime($row_rez['data'])); ?>" class="date" /> / <input type="text" name="checkout" value="<?php echo date('d.m.Y',strtotime($row_rez['data'].'+'.($row_rez['nr_zile']-1).' days')); ?>" class="date" /></td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="servicii">Servicii:</label></td>
        <td align="left" valign="middle"><input type="text" id="servicii" name="servicii" value="<?php echo $servicii; ?>" class="mare" /></td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="telefon">Telefon confirmare:</label></td>
        <td align="left" valign="middle"><input type="text" id="telefon" name="telefon" value="" class="mediu" />
        <a href="/adm/editare_hotel.php?pas=2&hotel=<?php echo $row_rez['id_hotel']; ?>" target="_blank" class="red">Vezi Hotel</a>
        </td>
      </tr>
      <tr>
        <td align="right" valign="middle"><label for="agent">Agent:</label></td>
        <td align="left" valign="middle"><select id="agent" name="agent">
          <option value="Razvan Draghici">Razvan Draghici</option>
        </select></td>
      </tr>
      <tr>
        <td align="right" valign="middle">&nbsp;</td>
        <td align="left" valign="middle">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="middle">&nbsp;</td>
        <td align="left" valign="middle"><input type="submit" class="submit" name="modifica" value="  Genereaza Voucher  " /></td>
      </tr>
    </table>
    <input type="hidden" name="idv" value="<?php echo $idv; ?>" />
    <input type="hidden" name="idc" value="<?php echo $idc; ?>" />
  </form>
  </div>

<script type="text/javascript">
$('#lang').change(function() {
	var lang = $(this).val();
	if(lang=='RO') {
		$('#servicii').val('<?php echo $servicii; ?>');
		$('#nume_camera').val('<?php echo $nume_camera; ?>');
	} else if(lang=='EN') {
		$('#servicii').val('<?php echo str_replace($search, $replace, strtolower($servicii)); ?>');
		$('#nume_camera').val('<?php echo str_replace($search, $replace, strtolower($nume_camera)); ?>');
	}
});
</script>
