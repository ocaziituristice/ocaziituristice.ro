<?php session_start(); include_once('../restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body { font-family:Arial; color:#000; margin:0; padding:0; }
#main { width:198mm; margin:0; position:relative; }
.pozitie1 { position:relative; height:106mm; margin:0; padding:0 0 10m 0; border-bottom:0.7mm dashed #000; }
.pozitie2 { position:relative; height:106mm; margin:10mm 0 0 0; padding:0; /*padding:0 0 4mm 0; border-bottom:0.7mm dashed #000;*/ }
/*.pozitie3 { position:relative; height:89mm; margin:4mm 0 0 0; padding:0; }*/
.date-firma { position:absolute; top:0; left:0; width:66mm; height:23.5mm; font-size:7pt; line-height:8pt; overflow:hidden; }
.date-firma .titlu { font-size:9pt; line-height:11pt; font-weight:bold; }
.date-agentie { position:absolute; top:0; right:0; width:94mm; height:23.5mm; text-align:center; font-size:9pt; font-weight:bold; line-height:11pt; overflow:hidden; }
.date-agentie img { margin:-4px 0; }
.title { position:absolute; top:24mm; left:0; width:198mm; height:5mm; text-align:center; font-size:18pt; line-height:5mm; font-weight:bold; overflow:hidden; }
fieldset { position:absolute; border:0.5mm solid #000; padding:1mm 2mm; margin:0; overflow:hidden; }
legend { font-size:10pt; line-height:10pt; font-weight:bold; }
.Fnume { top:30mm; left:0; width:93mm; height:22mm; font-size:9pt; line-height:10.5pt; }
.Fpersoane { top:30mm; left:100mm; width:36mm; height:22mm; font-size:9.5pt; line-height:11pt; text-align:center; }
.Fdestinatie { top:30mm; right:0; width:50.3mm; height:22mm; font-size:9.5pt; line-height:11pt; }
.Fcazare { top:55mm; left:0; width:136mm; height:11mm; font-size:9.5pt; line-height:11.5pt; }
.Fcheckin { top:55mm; right:0; width:50.3mm; height:11mm; font-size:9.5pt; line-height:20pt; text-align:center; }
.Fservicii { top:69mm; left:0; width:136mm; height:14mm; font-size:9.5pt; line-height:9.5pt; }
.Fconfirmare { top:86mm; left:0; width:93mm; height:8mm; font-size:9pt; line-height:12pt; }
.Fagent { top:86mm; left:100mm; width:36mm; height:8mm; font-size:9pt; line-height:12pt; }
.Fsemnatura { top:69mm; right:0; width:50.3mm; height:25mm; font-size:9.5pt; line-height:17pt; text-align:center; }
.FstampilaLogo { position:absolute; top:65mm; right:2mm; width:186px; height:160px; }
</style>
<?php include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>
<div id="main">

<?php
$idv = $_GET['idv'];
?>

<?php for($i=1;$i<=2;$i++) { ?>
  <div class="pozitie<?php echo $i; ?>">
  <?php include "continut_voucher.php"; ?>
  </div>
<?php } ?>

</div>
</body>
</html>
