<?php
$sel_voucher="SELECT * FROM vouchere WHERE id_voucher='$idv'";
$que_voucher=mysql_query($sel_voucher) or die(mysql_error());
$row_voucher=mysql_fetch_array($que_voucher);

$lang = $row_voucher['lang'];

if($lang=='EN') {
	$trans[1] = 'VOUCHER NO.';
	$trans[2] = 'from';
	$trans[3] = 'NAME';
	$trans[4] = 'NO. PERS.';
	$trans[5] = 'DESTINATION, COUNTRY';
	$trans[6] = 'ACCOMMODATION';
	$trans[7] = 'Room:';
	$trans[8] = 'CHECK IN / CHECK OUT';
	$trans[9] = 'SERVICES';
	$trans[10] = 'CONFIRMATION';
	$trans[11] = 'Phone:';
	$trans[12] = 'TRAVEL AGENT';
	$trans[13] = 'SIGNATURE';
	$trans[14] = 'born:';
	$trans[15] = 'ID Card:';
}
if($lang=='RO') {
	$trans[1] = 'VOUCHER NR.';
	$trans[2] = 'din';
	$trans[3] = 'NUME';
	$trans[4] = 'NR. PERS.';
	$trans[5] = 'DESTINATIE, TARA';
	$trans[6] = 'CAZARE';
	$trans[7] = 'Camera:';
	$trans[8] = 'CHECK IN / CHECK OUT';
	$trans[9] = 'SERVICII';
	$trans[10] = 'CONFIRMARE';
	$trans[11] = 'Tel:';
	$trans[12] = 'AGENT TURISM';
	$trans[13] = 'SEMNATURA';
	$trans[14] = 'data nasterii:';
	$trans[15] = 'buletin:';
}
?>
    <div class="date-firma">
      <div class="titlu"><?php echo $contact_legal_den_firma; ?></div>
      Nr. reg. com.: <?php echo $contact_legal_reg_com; ?><br />
      Cod fiscal: <?php echo $contact_legal_cui; ?><br />
      Adresa sediu social: <?php echo $contact_legal_adresa; ?><br />
      Banca <?php echo $contact_legal_banca; ?><br />
      RON: <?php echo $contact_legal_cont_lei; ?><br />
      EURO: <?php echo $contact_legal_cont_euro; ?>
    </div>
    
    <div class="date-agentie">
      <img src="/images/logo.png" /><br />
      Licenta tur-operator: <?php echo $settings_licenta; ?><br />
      Email: <?php echo $email_contact; ?><br />
      Tel: <?php echo $contact_telefon; ?>; Tel URGENTE: <?php echo $settings_tel_urgente; ?>
    </div>
    
    <div class="title"><?php echo $trans[1]; ?> <?php echo $row_voucher['id_voucher']; ?> / <?php echo $trans[2]; ?> <?php echo date('d.m.Y',strtotime($row_voucher['data'])); ?></div>
    
    <fieldset class="Fnume">
      <legend align="center"><?php echo $trans[3]; ?></legend>
<?php
$sel_voucher_pasageri="SELECT * FROM pasageri LEFT JOIN leg_voucher_pasageri ON pasageri.id_pasager = leg_voucher_pasageri.id_pasager WHERE leg_voucher_pasageri.id_voucher='$idv'";
$que_voucher_pasageri=mysql_query($sel_voucher_pasageri) or die(mysql_error());
while($row_voucher_pasageri=mysql_fetch_array($que_voucher_pasageri)) {
	$pas_nume = $row_voucher_pasageri['nume'].' '.$row_voucher_pasageri['prenume'];
	$pas_document = '';
	if(($row_voucher_pasageri['data_nasterii']!=NULL) and ($row_voucher_pasageri['data_nasterii']!='0000-00-00')) {
		$pas_document = $trans[14].' '.date('d.m.Y',strtotime($row_voucher_pasageri['data_nasterii']));
	} elseif($row_voucher_pasageri['buletin']!=NULL) {
		$pas_document = $trans[15].' '.$row_voucher_pasageri['buletin'];
	}
	echo $pas_nume.', '.$pas_document.'<br />';
}
?>
    </fieldset>
    
    <fieldset class="Fpersoane">
      <legend align="center"><?php echo $trans[4]; ?></legend>
      <br />
      <?php echo $row_voucher['persoane']; ?>
    </fieldset>
    
    <fieldset class="Fdestinatie">
      <legend align="center"><?php echo $trans[5]; ?></legend>
      <br />
      <?php echo $row_voucher['destinatie']; ?>
    </fieldset>
    
    <fieldset class="Fcazare">
      <legend align="center"><?php echo $trans[6]; ?></legend>
      <?php echo $row_voucher['hotel']; ?><br />
      <strong><?php echo $trans[7]; ?></strong> <?php echo $row_voucher['camera']; ?>
    </fieldset>
    
    <fieldset class="Fcheckin">
      <legend align="center"><?php echo $trans[8]; ?></legend>
      <?php echo $row_voucher['checkin']; ?>
    </fieldset>
    
    <fieldset class="Fservicii">
      <legend align="center"><?php echo $trans[9]; ?></legend>
      <?php echo $row_voucher['servicii']; ?>
    </fieldset>
    
    <fieldset class="Fconfirmare">
      <legend align="center"><?php echo $trans[10]; ?></legend>
      <?php echo $row_voucher['hotel']; ?>, <?php echo $trans[11]; ?> <?php echo $row_voucher['telefon']; ?>
    </fieldset>
    
    <fieldset class="Fagent">
      <legend align="center"><?php echo $trans[12]; ?></legend>
      <?php echo $row_voucher['agent']; ?>
    </fieldset>
    
    <fieldset class="Fsemnatura">
      <legend align="center"><?php echo $trans[13]; ?></legend>
      <br><br>
      _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
    </fieldset>
    <?php if($_GET['stampila']) { ?><div class="FstampilaLogo"><img src="/images/stampila.png"></div><?php } ?>
<?php @mysql_free_result($que_c) ?>
