<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
  <h1>Buna <?php echo $_SESSION['nume']; ?></h1>
  <br/>
	<?php if(eregi('hotel', $acces[$_SESSION['tip']])) { ?>
  <div class="infos"><h1 class="red">Informatii utile</h1>
  *Pentru a seta zonele la care sa apra distanta fata de plaja se modifica variabila array $zone_filtru_distanta din /home/ocazii/config/seteri.php</div>
  <br/>
  <br/>
  <?php /*?><a href="http://rezervari.paralela45.ro/merlinx_ftp/P45-OFERTE.xml" target="_blank">click</a><?php */?>

<?php
$sel_coins = "SELECT
GROUP_CONCAT(DISTINCT(oferte.moneda) SEPARATOR ' ') AS coins,
hoteluri.id_hotel,
hoteluri.nume,
tari.denumire AS denumire_tara,
zone.denumire AS denumire_zona,
localitati.denumire AS denumire_localitate
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
GROUP BY oferte.id_hotel
";
$que_coins = mysql_query($sel_coins) or die(mysql_error());
if(mysql_num_rows($que_coins)) {
?>
  <h2>Lista hoteluri care au mai multe valute (de la diverse oferte active)</h2>
  <br/>
  <ol>
<?php
while($row_coins = mysql_fetch_array($que_coins)) {
	if(strlen($row_coins['coins'])>5) {
		echo '<li><a href="editare_hotel.php?pas=2&hotel='.$row_coins['id_hotel'].'" target="_blank">'.$row_coins['nume'].'</a> &nbsp;|&nbsp; <strong>'.$row_coins['coins'].'</strong> &nbsp;|&nbsp; <em>'.$row_coins['denumire_localitate'].' / '.$row_coins['denumire_zona'].' / '.$row_coins['denumire_tara'].'</em></li>';
	}
}
?>
  </ol>
  <br />
  <br />
<?php } ?>

  <h2>Lista oferte (sejururi, circuite) care <strong class="red">NU au preturi pivot</strong></h2>
  <br/>
  <ol>
<?php
$sel_farapivot="SELECT
oferte.id_oferta,
oferte.denumire_scurta,
tari.denumire AS denumire_tara,
zone.denumire AS denumire_zona,
localitati.denumire AS denumire_localitate,
hoteluri.nume,
hoteluri.tip_unitate
FROM oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
Inner Join tari ON zone.id_tara = tari.id_tara
WHERE oferte.id_oferta_pivot IS NULL 
AND oferte.valabila = 'da'
AND oferte.online_prices <> 'da'
ORDER BY
hoteluri.tip_unitate DESC,
oferte.ultima_modificare DESC,
oferte.data_adaugarii DESC
";
$que_farapivot=mysql_query($sel_farapivot) or die(mysql_error());
while($row_farapivot=mysql_fetch_array($que_farapivot)) {
	echo '<li><strong>'.$row_farapivot['tip_unitate'].'</strong> &nbsp;|&nbsp; <a href="editare_sejur.php?pas=2&oferta='.$row_farapivot['id_oferta'].'">'.$row_farapivot['denumire_scurta'].' '.$row_farapivot['nume'].'</a> &nbsp;|&nbsp; <em>'.$row_farapivot['denumire_localitate'].' / '.$row_farapivot['denumire_zona'].' / '.$row_farapivot['denumire_tara'].'</em></li>';
}
?>
  </ol>

  <br />
  <br />
  <h2>Lista oferte (sejururi, circuite) care AU preturi pivot dar nu e bifat <strong class="red">Modulul nou de rezervare</strong></h2>
  <br/>
  <ol>
<?php
$sel_faranewrez="SELECT
oferte.id_oferta,
oferte.denumire_scurta,
oferte.rezervare_online,
tari.denumire AS denumire_tara,
zone.denumire AS denumire_zona,
localitati.denumire AS denumire_localitate,
hoteluri.nume,
hoteluri.tip_unitate
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.id_oferta_pivot IS NOT NULL
AND oferte.valabila =  'da'
AND oferte.rezervare_online = 'nu'
ORDER BY
hoteluri.tip_unitate DESC,
oferte.ultima_modificare DESC,
oferte.data_adaugarii DESC
";
$que_faranewrez=mysql_query($sel_faranewrez) or die(mysql_error());
while($row_faranewrez=mysql_fetch_array($que_faranewrez)) {
	echo '<li><strong>'.$row_faranewrez['tip_unitate'].'</strong> &nbsp;|&nbsp; <a href="editare_sejur.php?pas=2&oferta='.$row_faranewrez['id_oferta'].'">'.$row_faranewrez['denumire_scurta'].' '.$row_faranewrez['nume'].'</a> &nbsp;|&nbsp; <em>'.$row_faranewrez['denumire_localitate'].' / '.$row_faranewrez['denumire_zona'].' / '.$row_faranewrez['denumire_tara'].'</em> &nbsp;|&nbsp; '.$row_faranewrez['rezervare_online'].'</li>';
}
?>
  </ol>

  <br />
  <br />
  
<?php if(!isset($_REQUEST['nr_zile'])) $nr_zile = '10'; else $nr_zile = $_REQUEST['nr_zile']; ?>
  <h2>Lista oferte <strong>EB fara parinte <strong class="red">expirate</strong></strong> in ultimele <form method="get"><input type="number" class="mic" value="<?php echo $nr_zile; ?>" name="nr_zile" id="nr_zile" onchange="this.form.submit()" /></form> zile</h2>
  <br/>
  <ol>
<?php
$expire_date = date("Y-m-d", strtotime('-'.$nr_zile.' days'));

$sel_ebexpired = "SELECT
oferte.id_oferta,
oferte.denumire_scurta,
tari.denumire AS denumire_tara,
zone.denumire AS denumire_zona,
localitati.denumire AS denumire_localitate,
hoteluri.nume
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
INNER JOIN early_booking ON oferte.id_oferta = early_booking.id_oferta
WHERE oferte.valabila = 'nu'
AND oferte.end_date >= '".$expire_date."'
AND oferte.end_date <= '".date("Y-m-d")."'
AND oferte.oferta_parinte_early = '0'
";
$que_ebexpired = mysql_query($sel_ebexpired) or die(mysql_error());
while($row_ebexpired = mysql_fetch_array($que_ebexpired)) {
	echo '<li><a href="editare_sejur.php?pas=2&oferta='.$row_ebexpired['id_oferta'].'" target="_blank">'.$row_ebexpired['denumire_scurta'].' '.$row_ebexpired['nume'].'</a> &nbsp;|&nbsp; <em>'.$row_ebexpired['denumire_localitate'].' / '.$row_ebexpired['denumire_zona'].' / '.$row_ebexpired['denumire_tara'].'</em></li>';
}
?>
  </ol>
  
  <br/>
  <br/>

<?php
include_once('tari_fara_tip.php');
$sel="select
tari.denumire,
tari.id_tara
from
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
WHERE
oferte.valabila =  'da' and
hoteluri.tip_unitate = 'Circuit'
Group by tari.id_tara ";
$que=mysql_query($sel) or die(mysql_error()); $i=0;
while($row=mysql_fetch_array($que)) {
$selOf="select
oferte.id_oferta
from
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate
inner join zone on localitati.id_zona = zone.id_zona
where
zone.id_tara = '".$row['id_tara']."'
and hoteluri.tip_unitate <> 'Circuit'
Group by oferte.id_oferta
Limit 1";
$queOf=mysql_query($selOf) or die(mysql_error());
if(mysql_num_rows($queOf)==0) { ++$i;
if($i==1) echo '<h2>Tari care au circuite dar nu au sejururi</h2><br/>
<ul>';
echo '<li><a href="'.$sitepath_adm.'editare_tara.php?pas=2&tara='.$row['id_tara'].'" target="_blank">'.$row['denumire'].'</a></li>';
}
@mysql_free_result($queOf);
} @mysql_free_result($que); if($i>1) echo '</ul>'; ?>
	
    <?php } //end if access ?>
  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'footer.php'); ?></div>
</body>
</html>