<table width="100%" border="0" cellspacing="2" cellpadding="4" class="default">
  <tr>
    <th valign="top" align="right" width="160">Tara:</th>
    <td><?php $sel_tari="SELECT id_tara, denumire FROM tari ORDER BY denumire ASC";
	$rez_tari=mysql_query($sel_tari) or die(mysql_error()); ?>
	<select name="param[tara]" id="tara" onchange="ajaxpage('<?php echo $sitepath_adm.'articole/select_zona.php?zona='.$param['zona']; ?>&tara='+this.value,'zon'); copy_text2(this.options[this.selectedIndex].text, 'param[link_area]');">
	  <option value="" disabled="disabled" <?php if(!$param['tara']) echo 'selected'; ?>>--</option>
	  <?php while($row_tari=mysql_fetch_array($rez_tari)) { ?>
	  <option value="<?php echo $row_tari['id_tara']; ?>" <?php if($id_tara==$row_tari['id_tara']) echo 'selected'; ?>><?php echo $row_tari['denumire']; ?></option>
	  <?php } @mysql_free_result($rez_tari); ?>
	</select></td>
  </tr>
  <tr>
    <td colspan="2" align="left" id="zon">
      <table width="100%" border="0" cellspacing="2" cellpadding="2" align="left">
        <tr>
          <th align="right" valign="top" width="160">Zona:</th>
          <?php $sel_zon="SELECT id_zona, denumire FROM zone WHERE id_tara = '".$id_tara."' GROUP BY id_zona ORDER BY denumire ";
          $que_zon=mysql_query($sel_zon) or die(mysql_error()); ?>
          <td class="help_text">
          <select name="param[zona]" id="zona" onchange="ajaxpage('articole/select_localitate.php?id_tara=<?php echo $id_tara; ?>&zona='+this.value,'loc2'); copy_text2(this.options[this.selectedIndex].text, 'param[link_area]');">
            <option value="" <?php if(!$param['zona']) echo 'selected'; ?>>--</option>
            <?php while($row_zon=mysql_fetch_array($que_zon)) { ?>
            <option value="<?php echo $row_zon['id_zona']; ?>" <?php if($row_zon['id_zona']==$id_zona) echo 'selected'; ?> onclick="ajaxpage('articole/select_localitate.php?zona='+this.value,'loc');"><?php echo $row_zon['denumire']; ?></option>
            <?php } @mysql_free_result($que_zon); ?>
          </select></td>
        </tr>
        <tr>
          <th align="right" valign="top">Localitatea:</th>
          <td class="help_text" id="loc2">
          <?php $sel_loc="SELECT id_localitate, denumire FROM localitati WHERE id_zona = '".$id_zona."' GROUP BY id_localitate ORDER BY denumire ";
          $que_loc=mysql_query($sel_loc) or die(mysql_error()); ?>
          <select name="param[localitate]" id="localitate" onchange="copy_text2(this.options[this.selectedIndex].text, 'param[link_area]');">
            <option value="" <?php if(!$param['localitate']) echo 'selected'; ?>>--</option>
            <?php while($row_loc=mysql_fetch_array($que_loc)) { ?>
            <option value="<?php echo $row_loc['id_localitate']; ?>" <?php if($row_loc['id_localitate']==$id_localitate) echo 'selected'; ?>><?php echo $row_loc['denumire']; ?></option>
            <?php } @mysql_free_result($que_loc); ?>
          </select></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th valign="top" align="right">Categorie:</th>
    <td><select name="param[categorie]">
	  <option value="" disabled="disabled" <?php if(!$param['categorie']) echo 'selected'; ?>>Selecteaza</option>
	  <?php $sel_categ = "SELECT COLUMN_TYPE FROM information_schema.columns WHERE table_name = 'articles' AND column_name = 'categorie' ";
      $que_categ = mysql_query($sel_categ) or die(mysql_error());
      $row_categ = mysql_fetch_array($que_categ);
      $categList = explode(",", str_replace("'", "", substr($row_categ['COLUMN_TYPE'], 5, (strlen($row_categ['COLUMN_TYPE'])-6))));
      foreach($categList as $value_categ) { ?>
	  <option value="<?php echo $value_categ; ?>" <?php if($param['categorie']==$value_categ) echo 'selected'; ?>><?php echo $value_categ; ?></option>
	  <?php } @mysql_free_result($que_categ); ?>
	</select></td>
  </tr>
  <tr>
    <th valign="top" align="right">Titlu articol:</th>
    <td><input type="text" name="param[denumire]" id="param[denumire]" class="mare" value="<?php echo $param['denumire']; ?>" onKeyUp="copy_text('param[denumire]', 'param[link]')">
      <div class="infos">Va rugam incercati sa nu folositi caractere speciale(-,*,.,&) in denumire.</div></td>
  </tr>
  <tr>
    <th valign="top" align="right">Link articol:</th>
    <td><strong>/ghid-turistic-</strong>
      <input type="text" name="param[link_area]" id="param[link_area]" class="mic" value="<?php echo $param['link_area']; ?>">
      <strong>/</strong>
      <input type="text" name="param[link]" id="param[link]" class="mediu" style="width:200px;" value="<?php echo $param['link']; ?>">
      <strong>.html</strong>
      <div class="infos">Nu trebuie introdus neaparat, se formeaza automat din titlu.</div></td>
  </tr>
  <tr>
    <th align="right" valign="top">Descriere scurta:</th>
    <td><textarea name="param[descriere_scurta]" class="mic"><?php echo $param['descriere_scurta']; ?></textarea></td>
  </tr>
  <tr>
  <tr>
    <th valign="top" align="right">Descriere:</th>
    <td><textarea name="param[descriere]" class="mare mceEditor"><?php echo $param['descriere']; ?></textarea></td>
  </tr>
  <tr>
    <th valign="top" align="right">Articol principal:</th>
    <td><select name="param[articol_principal]">
      <option value="nu" <?php if($param['articol_principal']=='nu' or !$param['articol_principal']) echo 'selected'; ?>>NU</option>
      <option value="da" <?php if($param['articol_principal']=='da') echo 'selected'; ?>>DA</option>
    </select></td>
  </tr>
  <tr>
    <th valign="top" align="right">Activ:</th>
    <td><select name="param[activ]">
      <option value="da" <?php if($param['activ']=='da' or !$param['activ']) echo 'selected'; ?>>DA</option>
      <option value="nu" <?php if($param['activ']=='nu') echo 'selected'; ?>>NU</option>
    </select></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><input type="submit" name="adauga" value="<?php echo $valut_but_submit; ?>" class="buttons" /></td>
  </tr>
</table>

<script>
function copy_text(from, where) {
	var text1 = document.getElementById(from);
	var text2 = document.getElementById(where);
	var text1_r = text1.value;
	text2.value = text1_r.replace(/\s+/g, '-').toLowerCase();
}
function copy_text2(from, where) {
	var text1 = from;
	var text2 = document.getElementById(where);
	text2.value = text1.replace(/\s+/g, '-').toLowerCase();
}
</script>