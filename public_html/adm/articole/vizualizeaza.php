<?php unset($add_t); ?>
<h1>Vizualizeaza articolul <a href="<?php echo $sitepath_adm.'editare_articol.php?pas=2&articol='.$id_articol; ?>" class="buttons">Editeaza articolul</a> |
<a href="<?php echo $sitepath_adm.'editare_articol.php'; ?>" class="buttons">Toate articolele</a> |
<a href="<?php echo $sitepath_adm.'adauga_articol.php'; ?>" class="buttons">Adauga articol</a>
</h1>

<br />

<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td valign="top" align="left">Tara:</td>
    <td align="left" valign="top"><?php echo $param['den_tara']; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">Zona:</td>
    <td align="left" valign="top"><?php echo $param['den_zona']; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">Localitate:</td>
    <td align="left" valign="top"><?php echo $param['den_localitate']; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">Categorie:</td>
    <td align="left" valign="top"><?php echo $param['categorie']; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">Articol principal:</td>
    <td align="left" valign="top"><?php if($param['articol_principal']=='da') echo '<img src="images/yes.png">'; else echo '<img src="images/no.png">'; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">Link:</td>
    <td align="left" valign="top"><a href="<?php echo '/ghid-turistic-'.$param['link_area'].'/'.$param['link'].'.html'; ?>" target="_blank"><?php echo '/ghid-turistic-'.$param['link_area'].'/'.$param['link'].'.html'; ?></a></td>
  </tr>
  <tr>
    <td valign="top" align="left">Activ:</td>
    <td align="left" valign="top"><?php if($param['activ']=='da') echo '<img src="images/yes.png">'; else echo '<img src="images/no.png">'; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="left" width="160">Denumire articol:</td>
    <td align="left" valign="top"><?php echo $param['denumire']; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">Descriere scurta:</td>
    <td align="left" valign="top"><?php echo $param['descriere_scurta']; ?></td>
  </tr>
  <tr>
    <td valign="top" align="left">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="left">Descriere:</td>
    <td align="left" valign="top"><?php echo html_entity_decode($param['descriere']); ?></td>
  </tr>
</table>
