<?php
$id_hotel = $_GET['id_hotel'];
$id_oferta = $_GET['id_oferta'];
$denumire = trim($_POST['denumire']);
$tip_masa = $_POST['tip_masa'];

$err = '';

$pret['id_hotel'] = $id_hotel;
$pret['id_oferta'] = $id_oferta;
$pret['denumire'] = $denumire;

$link_oferta = trim($_POST['link_oferta']);

$fp = fopen($link_oferta,"r"); 
while(!feof($fp)) {
    $page .= fgets($fp, 4096);
}
$titre = eregi('<table class="price-list-table">(.*)</table>',$page,$regs); 
$string = $regs[1];
fclose($fp);

$string = preg_replace('~>\s+<~', '><', trim($string));
$rows = preg_split('/(<tr>|<\/tr>)/i',$string,-1,PREG_SPLIT_NO_EMPTY);
$nr_rows = sizeof($rows);

$dates = $rows[0];
$dates = str_replace('<th class="dead-cell">Camere / Date plecare</th>','',$dates);
$dates_arr = preg_split('/(<th class="dates">|<\/th>)/i',$dates,-1,PREG_SPLIT_NO_EMPTY);
$nr_columns = sizeof($dates_arr);
foreach($dates_arr as $k_date => $v_date) {
	 $date[$k_date] = preg_split('/\s+/',trim(strip_tags($v_date)),-1,PREG_SPLIT_NO_EMPTY);
	 foreach($date[$k_date] as $k_data => $v_data) {
		 $pret['data_start'][$k_date][$k_data] = make_date_from_zzll($v_data); //date plecare
	 }
}

for($i=1; $i<$nr_rows; $i++) {
	$rand[$i] = preg_split('/(<th class="room-name">|<\/th>)/i',$rows[$i],-1,PREG_SPLIT_NO_EMPTY);
	$rand_nohtml[$i] = strip_tags($rand[$i][0]);
	$rand_no_br[$i] = str_replace('<br/>','',str_replace('<br />','',str_replace('<br>','',str_replace('<br >','',$rand[$i][0]))));
	
	$col_cam[$i] = preg_split('/(\(|\))/i',$rand_no_br[$i],-1,PREG_SPLIT_NO_EMPTY);
	$col_cam_ext[$i] = preg_split('/(<span class="room-caracteristic">|<\/span>)/i',$col_cam[$i][2],-1,PREG_SPLIT_NO_EMPTY);
	for($ii=1; $ii<sizeof($col_cam_ext[$i]); $ii++) {
		if(!preg_match('/varste copii (.*)/',$col_cam_ext[$i][$ii])) {
			$col_cam_nomasa[$i] = substr($col_cam_ext[$i][$ii], 0, strrpos($col_cam_ext[$i][$ii], '/'));
			$ext_cam[$i] = str_replace('+CHD','',str_replace('+EB','',str_replace('/','',$col_cam_nomasa[$i])));
		}
	}
	$col_masa[$i] = substr(substr($rand_nohtml[$i], strrpos($rand_nohtml[$i], '/')), 1);
	if($col_masa[$i]==$tip_masa) {
		$denumire_camera[$i] = trim($col_cam[$i][0]).' '.trim($ext_cam[$i]);
		$denumire_camera[$i] = trim($denumire_camera[$i]);
		$id_camera[$i] = get_id_camera($denumire_camera[$i]);
		if($id_camera[$i]=='') {
			$err .= '<h3 class="red">EROARE ! Nu toate camerele au corespondent!</h3><strong>- '.$denumire_camera[$i].'</strong><br><br>';
		} else {
			$pret['id_camera'][$i]=$id_camera[$i]; //id camera
			$grad_ocup[$i] = explode(', ',trim($col_cam[$i][1]));
			$nr_adulti[$i] = 0;
			$nr_copii[$i] = 0;
			$age_infant[$i] = '0-1,99';
			$age_child[$i] = preg_match('/([0-9]{1}-{1}[0-9]{2})/i',trim($col_cam[$i][2]),$age_copil_match[$i]);
			if(sizeof($age_copil_match[$i])>0) {
				$age_copil[$i] = $age_copil_match[$i][0]; //varsta copil
			}
			$varste_copii[$i] = '';
			foreach($grad_ocup[$i] as $k_go => $v_go) {
				if(substr($grad_ocup[$i][$k_go],-1)=='a') {
					$nr_adulti[$i] += substr($grad_ocup[$i][$k_go],0,1);
				} else if(substr($grad_ocup[$i][$k_go],-1)=='c') {
					$nr_copii[$i] += substr($grad_ocup[$i][$k_go],0,1);
					$varste_copii[$i] .= $age_copil[$i].';';
				} else if(substr($grad_ocup[$i][$k_go],-1)=='i') {
					$nr_copii[$i] += substr($grad_ocup[$i][$k_go],0,1);
					$varste_copii[$i] .= '0-1,99;';
				}
			}
			$varste_copii[$i] = substr($varste_copii[$i],0,-1);
			$varste_copii[$i] = explode(';',$varste_copii[$i]);
			
			$pret['nr_adulti'][$i] = $nr_adulti[$i]; //nr adulti
			$pret['nr_copii'][$i] = $nr_copii[$i]; //nr copii
			if(sizeof($varste_copii[$i]) < $nr_copii[$i]) {
				for($ic=0; $ic<$nr_copii[$i]; $ic++) {
					$varste_copii_final[$i][$ic] = $varste_copii[$i][0];
				}
				$pret['varste_copii'][$i] = $varste_copii_final[$i]; //varste copii
			} else {
				$pret['varste_copii'][$i] = $varste_copii[$i]; //varste copii
			}
			
			if($nr_copii[$i]=='0') {
				if((preg_match('/\+EB/',$rand_nohtml[$i])) or ($pret['nr_adulti'][$i]>2)) {
					$pret_pivot[$i] = 'nu';
				} else {
					$pret_pivot[$i] = 'da';
				}
			} else {
				$pret_pivot[$i] = 'nu';
			}
			$pret['pret_pivot'][$i] = $pret_pivot[$i]; //pret pivot
			
			$col_pret[$i] = preg_split('/(<td class="price">|<\/td>)/i',$rand[$i][1],-1,PREG_SPLIT_NO_EMPTY);
			foreach($col_pret[$i] as $k_cp => $v_cp) {
				$split_col_pret[$i] = explode(' ',$v_cp);
				$pret['pret'][$i][$k_cp] = trim($split_col_pret[$i][0]); //pret
				$pret['moneda'][$i][$k_cp] = reverse_moneda(trim($split_col_pret[$i][1])); //moneda
			}
		}
	} else {
		echo '<h3 class="red">Tipul de masa al acestui pret nu corespunde cu cel selectat</h3>';
	}
}

if(strlen($err)>0) {
	echo '<script>alert(\'Au aparut erori.\nPreturile nu au fost importate.\'); </script>';
	echo $err;
	echo '<div class="infos">Camerele se asigneaza din baza de date in tabelul <strong>TIP_CAMERA_CORESPONDENT</strong> cu cele din tabelul <strong>TIP_CAMERA</strong></div><br>';
	echo '<button class="buttons" onClick="javascript: history.go(-1)">Preia datele din nou</button>';
} else {
	if(sizeof($pret['pret']>0)) {
		// inserare grad ocupare
		foreach($pret['id_camera'] as $key_cam => $value_cam) {
			$ins_id_hotel = $pret['id_hotel'];
			$ins_id_camera = $pret['id_camera'][$key_cam];
			$ins_nr_adulti = $pret['nr_adulti'][$key_cam];
			$ins_nr_copii = $pret['nr_copii'][$key_cam];
			$ins_copil1 = $pret['varste_copii'][$key_cam][0];
			$ins_copil2 = $pret['varste_copii'][$key_cam][1];
			$ins_copil3 = $pret['varste_copii'][$key_cam][2];
			
			$sel_grad_ocupare = "SELECT * FROM grad_ocupare_camera
			WHERE id_hotel = '".$ins_id_hotel."'
			AND id_camera = '".$ins_id_camera."'
			AND nr_adulti = '".$ins_nr_adulti."'
			AND nr_copii = '".$ins_nr_copii."'
			AND copil1 = '".$ins_copil1."'
			AND copil2 = '".$ins_copil2."'
			AND copil3 = '".$ins_copil3."' ";
			$que_grad_ocupare = mysql_query($sel_grad_ocupare);
			$row_grad_ocupare = mysql_fetch_array($que_grad_ocupare);
			$totalRows_grad_ocupare = mysql_num_rows($que_grad_ocupare);
			@mysql_free_result($que_grad_ocupare);
			if($totalRows_grad_ocupare=='0') {
				$ins_grad_ocup = "INSERT INTO grad_ocupare_camera SET
				id_hotel = '".$ins_id_hotel."',
				id_camera = '".$ins_id_camera."',
				nr_adulti = '".$ins_nr_adulti."',
				nr_copii = '".$ins_nr_copii."',
				copil1 = '".$ins_copil1."',
				copil2 = '".$ins_copil2."',
				copil3 = '".$ins_copil3."' ";
				$que_grad_ocup = mysql_query($ins_grad_ocup) or die(mysql_error());
				@mysql_free_result($que_grad_ocup);
			}
			
			$sel_camere_hotel = "SELECT * FROM camere_hotel
			WHERE id_hotel = '".$ins_id_hotel."'
			AND id_camera = '".$ins_id_camera."' ";
			$que_camere_hotel = mysql_query($sel_camere_hotel);
			$row_camere_hotel = mysql_fetch_array($que_camere_hotel);
			$totalRows_camere_hotel = mysql_num_rows($que_camere_hotel);
			@mysql_free_result($que_camere_hotel);
			if($totalRows_camere_hotel=='0') {
				$ins_cam_hot = "INSERT INTO camere_hotel SET
				id_hotel = '".$ins_id_hotel."',
				id_camera = '".$ins_id_camera."' ";
				$que_cam_hot = mysql_query($ins_cam_hot) or die(mysql_error());
				@mysql_free_result($que_cam_hot);
			}
		}
		
		// inserare pret pivot
		//echo '<ol>';
		foreach($pret['pret'] as $key_prices => $value_prices) {
			foreach($pret['data_start'] as $key_dates => $value_dates) {
				foreach($value_dates as $key_data => $value_data) {
					$inspr_pret_pivot = $pret['pret_pivot'][$key_prices];
					$inspr_data = $value_data;
					$inspr_id_hotel = $pret['id_hotel'];
					$inspr_id_oferta = $pret['id_oferta'];
					$inspr_denumire = $pret['denumire'];
					$inspr_id_camera = $pret['id_camera'][$key_prices];
					$inspr_nr_adulti = $pret['nr_adulti'][$key_prices];
					$inspr_nr_copii = $pret['nr_copii'][$key_prices];
					$inspr_copil1 = $pret['varste_copii'][$key_prices][0];
					$inspr_copil2 = $pret['varste_copii'][$key_prices][1];
					$inspr_copil3 = $pret['varste_copii'][$key_prices][2];
					$inspr_pret = $value_prices[$key_dates];
					$inspr_moneda = $pret['moneda'][$key_prices][$key_dates];
					if($inspr_pret_pivot=='da') {
						$inspr_pret = $value_prices[$key_dates] / $inspr_nr_adulti; //pret pt pretul pivot
						
						$ins_pret_pivot = "INSERT INTO pret_pivot SET
						id_hotel = '".$inspr_id_hotel."',
						id_oferta = '".$inspr_id_oferta."',
						data_start = '".$inspr_data."',
						tip_camera = '".$inspr_id_camera."',
						pret = '".$value_prices[$key_dates]."',
						moneda = '".$inspr_moneda."',
						pret_pivot = 'nu',
						adulti = '".$inspr_nr_adulti."',
						copii = '".$inspr_nr_copii."',
						denumire = '".$inspr_denumire."',
						copil1 = '".$inspr_copil1."',
						copil2 = '".$inspr_copil2."',
						copil3 = '".$inspr_copil3."' ";
						$que_pret_pivot = mysql_query($ins_pret_pivot) or die(mysql_error());
						@mysql_free_result($que_pret_pivot);
					}
					//echo '<li>'.$inspr_data.' - '.$inspr_pret.' - '.$inspr_moneda.' - '.$inspr_id_camera.' - '.$inspr_id_hotel.' - '.$inspr_id_oferta.' - '.$inspr_denumire.' - '.$inspr_nr_adulti.' - '.$inspr_nr_copii.' - '.$inspr_copil1.' - '.$inspr_copil2.' - '.$inspr_copil3.' - '.$inspr_pret_pivot.'</li>';
					$ins_pret = "INSERT INTO pret_pivot SET
					id_hotel = '".$inspr_id_hotel."',
					id_oferta = '".$inspr_id_oferta."',
					data_start = '".$inspr_data."',
					tip_camera = '".$inspr_id_camera."',
					pret = '".$inspr_pret."',
					moneda = '".$inspr_moneda."',
					pret_pivot = '".$inspr_pret_pivot."',
					adulti = '".$inspr_nr_adulti."',
					copii = '".$inspr_nr_copii."',
					denumire = '".$inspr_denumire."',
					copil1 = '".$inspr_copil1."',
					copil2 = '".$inspr_copil2."',
					copil3 = '".$inspr_copil3."' ";
					$que_pret = mysql_query($ins_pret) or die(mysql_error());
					@mysql_free_result($que_pret);
					
					echo '<script>alert(\'Preturile au fost importate!\'); document.location.href=\'/adm/pivot.php?pas=2&id_hotel='.$id_hotel.'&id_oferta='.$id_oferta.'\'; </script>';
				}
			}
		}
		//echo '</ol>';
	}
}

//echo '<pre>';print_r($pret);echo '</pre>';
?>