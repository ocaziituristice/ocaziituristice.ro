<?php $id_hotel=$_GET['id_hotel']; ?>
<br><br>
<form method="post" name="modH" action="/adm/metode_import.php?metoda=5&id_hotel=<?php echo $id_hotel.'&id_oferta='.$_GET['id_oferta']; ?>&pas=2">
  <table width="100%" border="0" cellspacing="2" cellpadding="3" id="grad_ocupare" align="left" class="tabel-inside">
    <tr>
      <td align="right" valign="top" width="160">Denumire</td>
      <td align="left" valign="top"><input type="text" name="denumire" class="mediu" /></td>
    </tr>
    <tr>
      <td align="right" valign="top">Link oferta</td>
      <td align="left" valign="top"><textarea name="link_oferta" class="mediu float-left"></textarea>
        <div class="infos float-left"> Se introduce linkul ofertei din adresa de la browser. </div>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Tip masa</td>
      <td align="left" valign="top">
        <select name="tip_masa" class="mediu float-left">
          <option>--</option>
          <option value="BB">BB</option>
          <option value="AB">AB (american breakfast)</option>
          <option value="HB">HB</option>
          <option value="FB">FB (pensiune completa)</option>
          <option value="AI">AI</option>
          <option value="UAI">UAI</option>
          <option value="NO MEAL">NO MEAL</option>
          <option value="NM">NM (fara masa)</option>
        </select>
        <div class="infos float-left"> Se selecteaza tipul mesei din oferta. Daca sunt mai multe tipuri de masa, se alege doar unul. Daca trebuie selectat un tip de masa care nu se regaeste in selectul alaturat, va rugam contactati departamentul tehnic (Daniel) :). </div>
      </td>
    </tr>
    <?php /*?><tr>
      <td align="right" valign="top" width="160">Plecare</td>
      <td align="left" valign="top"><textarea name="plecare" class="mediu float-left"></textarea>
        <div class="infos float-left"> Se introduc <strong>doar</strong> celulele cu datele de plecare (fara coloanele de inceput).<br>
          Dupa paste-ul din Excel, fiecare <strong>coloana</strong> trebuie sa fie cuprinsa intre <strong>ghilimele "</strong>. </div>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Preturi</td>
      <td align="left" valign="top"><textarea name="preturi" class="mediu float-left"></textarea>
        <div class="infos float-left"> Se introduc randurile ce contin informatiile cu gradul de ocupare si preturile aferente acestora.<br>
          Spre deosebire de ceilalti pasi, aici se copiaza <strong>toate celulele</strong> din tabelul in Excel, incluzand coloanele dinaintea preturilor.<br>
          In general, aceste randuri se afla dupa capul de tabel <strong>"Pret pe Camera"</strong>. </div>
      </td>
    </tr><?php */?>
    <tr>
      <td align="center" valign="top" colspan="2"><input type="submit" name="adauga" value="Importa" class="buttons" /></td>
    </tr>
  </table>
</form>
