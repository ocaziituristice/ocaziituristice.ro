<?php
$id_hotel = $_GET['id_hotel'];
$id_oferta = $_GET['id_oferta'];
$denumire = trim($_POST['denumire']);
$tip_masa = trim($_POST['tip_masa']);
$plecari = $_POST['plecari'];
$preturi = $_POST['preturi'];

$err = '';

$pret['id_hotel'] = $id_hotel;
$pret['id_oferta'] = $id_oferta;
$pret['denumire'] = $denumire;
$pret['tip_masa'] = $tip_masa;
$pret['moneda'] = 'EURO';

if(strlen($plecari)>0) {
	$plecari_col = explode("\t",trim($plecari));
	foreach($plecari_col as $k_plecari_col => $v_plecari_col) {
		$plecare = explode(",",$v_plecari_col);
		foreach($plecare as $k_plecare => $v_plecare) {
			$pret['data_start'][$k_plecari_col][$k_plecare] = make_date_from_zzll(trim($v_plecare)); //date plecare
		}
	}
}

if(strlen($preturi)>0) {
	$preturi_col = explode("\r",trim($preturi));
	foreach($preturi_col as $k_preturi_col => $v_preturi_col) {
		$preturi_row = explode("\t",trim($v_preturi_col));
		$preturi_row[0] = str_replace('+CHD','',str_replace('+EB','',$preturi_row[0]));
		$cam_row = explode('/',trim($preturi_row[0]));
		$id_camera = get_id_camera(trim($cam_row[0]));
		$grad_ocupare = explode('+',trim($cam_row[1]));
		$masa_tip = explode(' ',trim($grad_ocupare[0]));
		if($masa_tip[0]==$pret['tip_masa']) {
			if($id_camera=='') {
				$err .= '<h3 class="red">EROARE ! Nu toate camerele au corespondent!</h3><strong>- '.$cam_row[0].'</strong><br><br>';
			} else {
				$pret['id_camera'][$k_preturi_col] = $id_camera; //id camera
			}
			$cam_row[1] = trim(str_replace($pret['tip_masa'],'',$cam_row[1]));
			$grad_ocup[$k_preturi_col] = explode('+',$cam_row[1]);
			$adulti[$k_preturi_col] = explode(' ',$grad_ocup[$k_preturi_col][0]);
			$pret['nr_adulti'][$k_preturi_col] = $adulti[$k_preturi_col][0]; //nr adulti
			if(strlen($grad_ocup[$k_preturi_col][1])>0) $copil[$k_preturi_col][0] = explode(' ',$grad_ocup[$k_preturi_col][1]);
			if(strlen($grad_ocup[$k_preturi_col][2])>0) $copil[$k_preturi_col][1] = explode(' ',$grad_ocup[$k_preturi_col][2]);
			if(strlen($grad_ocup[$k_preturi_col][3])>0) $copil[$k_preturi_col][2] = explode(' ',$grad_ocup[$k_preturi_col][3]);
			if($copil[$k_preturi_col][0][1]=='adl') $pret['nr_adulti'][$k_preturi_col] += $copil[$k_preturi_col][0][0]; //nr adulti
			if($copil[$k_preturi_col][1][1]=='adl') $pret['nr_adulti'][$k_preturi_col] += $copil[$k_preturi_col][1][0]; //nr adulti
			if($copil[$k_preturi_col][2][1]=='adl') $pret['nr_adulti'][$k_preturi_col] += $copil[$k_preturi_col][2][0]; //nr adulti
			$pret['nr_copii'][$k_preturi_col] = 0; //nr copii
			if($copil[$k_preturi_col][0][1]!='adl') $pret['nr_copii'][$k_preturi_col] += $copil[$k_preturi_col][0][0]; //nr copii
			if($copil[$k_preturi_col][1][1]!='adl') $pret['nr_copii'][$k_preturi_col] += $copil[$k_preturi_col][1][0]; //nr copii
			if($copil[$k_preturi_col][2][1]!='adl') $pret['nr_copii'][$k_preturi_col] += $copil[$k_preturi_col][2][0]; //nr copii
			if($pret['nr_copii'][$k_preturi_col]>0) {
				if($copil[$k_preturi_col][0][0]>1) {
					for($i1=0; $i1<$copil[$k_preturi_col][0][0]; $i1++) {
						$pret['varste_copii'][$k_preturi_col][$i1] = str_replace('(','',str_replace(')','',$copil[$k_preturi_col][0][2]));
					}
				} else {
					for($i2=0; $i2<$pret['nr_copii'][$k_preturi_col]; $i2++) {
						$pret['varste_copii'][$k_preturi_col][$i2] = str_replace('(','',str_replace(')','',$copil[$k_preturi_col][$i2][2]));
					}
				}
			} else {
				$pret['varste_copii'][$k_preturi_col][] = '';
			}
			
			if($pret['nr_copii'][$k_preturi_col]=='0') {
				$pret_pivot[$k_preturi_col] = 'da';
			} else {
				$pret_pivot[$k_preturi_col] = 'nu';
			}
			$pret['pret_pivot'][$k_preturi_col] = $pret_pivot[$k_preturi_col]; //pret pivot
			
			for($i=1; $i<=sizeof($pret['data_start']); $i++) {
				$pret['pret'][$k_preturi_col][] = trim($preturi_row[$i]); //pret
			}
		}
	}
}

if(strlen($err)>0) {
	echo '<script>alert(\'Au aparut erori.\nPreturile nu au fost importate.\'); </script>';
	echo $err;
	echo '<div class="infos">Camerele se asigneaza din baza de date in tabelul <strong>TIP_CAMERA_CORESPONDENT</strong> cu cele din tabelul <strong>TIP_CAMERA</strong></div><br>';
	echo '<button class="buttons" onClick="javascript: history.go(-1)">Preia datele din nou</button>';
} else {
	if(sizeof($pret['pret']>0)) {
		if((isset($_POST['trimite']) and (sizeof($_POST['id_rooms'])>0))) {
			// inserare grad ocupare
			foreach($pret['id_camera'] as $key_cam => $value_cam) {
				if(in_array($value_cam,$_POST['id_rooms'],true)) {
					$ins_id_hotel = $pret['id_hotel'];
					$ins_id_camera = $pret['id_camera'][$key_cam];
					$ins_nr_adulti = $pret['nr_adulti'][$key_cam];
					$ins_nr_copii = $pret['nr_copii'][$key_cam];
					$ins_copil1 = $pret['varste_copii'][$key_cam][0];
					$ins_copil2 = $pret['varste_copii'][$key_cam][1];
					$ins_copil3 = $pret['varste_copii'][$key_cam][2];
					
					$sel_grad_ocupare = "SELECT * FROM grad_ocupare_camera
					WHERE id_hotel = '".$ins_id_hotel."'
					AND id_camera = '".$ins_id_camera."'
					AND nr_adulti = '".$ins_nr_adulti."'
					AND nr_copii = '".$ins_nr_copii."'
					AND copil1 = '".$ins_copil1."'
					AND copil2 = '".$ins_copil2."'
					AND copil3 = '".$ins_copil3."' ";
					$que_grad_ocupare = mysql_query($sel_grad_ocupare);
					$row_grad_ocupare = mysql_fetch_array($que_grad_ocupare);
					$totalRows_grad_ocupare = mysql_num_rows($que_grad_ocupare);
					@mysql_free_result($que_grad_ocupare);
					if($totalRows_grad_ocupare=='0') {
						$ins_grad_ocup = "INSERT INTO grad_ocupare_camera SET
						id_hotel = '".$ins_id_hotel."',
						id_camera = '".$ins_id_camera."',
						nr_adulti = '".$ins_nr_adulti."',
						nr_copii = '".$ins_nr_copii."',
						copil1 = '".$ins_copil1."',
						copil2 = '".$ins_copil2."',
						copil3 = '".$ins_copil3."' ";
						$que_grad_ocup = mysql_query($ins_grad_ocup) or die(mysql_error());
						@mysql_free_result($que_grad_ocup);
					}
					
					$sel_camere_hotel = "SELECT * FROM camere_hotel
					WHERE id_hotel = '".$ins_id_hotel."'
					AND id_camera = '".$ins_id_camera."' ";
					$que_camere_hotel = mysql_query($sel_camere_hotel);
					$row_camere_hotel = mysql_fetch_array($que_camere_hotel);
					$totalRows_camere_hotel = mysql_num_rows($que_camere_hotel);
					@mysql_free_result($que_camere_hotel);
					if($totalRows_camere_hotel=='0') {
						$ins_cam_hot = "INSERT INTO camere_hotel SET
						id_hotel = '".$ins_id_hotel."',
						id_camera = '".$ins_id_camera."' ";
						$que_cam_hot = mysql_query($ins_cam_hot) or die(mysql_error());
						@mysql_free_result($que_cam_hot);
					}
				}
			}
			
			// inserare pret pivot
			//echo '<ol>';
			foreach($pret['pret'] as $key_prices => $value_prices) {
				if(in_array($pret['id_camera'][$key_prices],$_POST['id_rooms'],true)) {
					foreach($pret['data_start'] as $key_dates => $value_dates) {
						foreach($value_dates as $key_data => $value_data) {
							$inspr_pret_pivot = $pret['pret_pivot'][$key_prices];
							$inspr_data = $value_data;
							$inspr_id_hotel = $pret['id_hotel'];
							$inspr_id_oferta = $pret['id_oferta'];
							$inspr_denumire = $pret['denumire'];
							$inspr_id_camera = $pret['id_camera'][$key_prices];
							$inspr_nr_adulti = $pret['nr_adulti'][$key_prices];
							$inspr_nr_copii = $pret['nr_copii'][$key_prices];
							$inspr_copil1 = $pret['varste_copii'][$key_prices][0];
							$inspr_copil2 = $pret['varste_copii'][$key_prices][1];
							$inspr_copil3 = $pret['varste_copii'][$key_prices][2];
							$inspr_pret = $value_prices[$key_dates];
							$inspr_moneda = $pret['moneda'];
							
							//echo '<li>'.$inspr_data.' - '.$inspr_pret.' - '.$inspr_moneda.' - '.$inspr_id_camera.' - '.$inspr_id_hotel.' - '.$inspr_id_oferta.' - '.$inspr_denumire.' - '.$inspr_nr_adulti.' - '.$inspr_nr_copii.' - '.$inspr_copil1.' - '.$inspr_copil2.' - '.$inspr_copil3.' - '.$inspr_pret_pivot.'</li>';
							$ins_pret = "INSERT INTO pret_pivot SET
							id_hotel = '".$inspr_id_hotel."',
							id_oferta = '".$inspr_id_oferta."',
							data_start = '".$inspr_data."',
							tip_camera = '".$inspr_id_camera."',
							pret = '".$inspr_pret."',
							moneda = '".$inspr_moneda."',
							pret_pivot = 'nu',
							adulti = '".$inspr_nr_adulti."',
							copii = '".$inspr_nr_copii."',
							denumire = '".$inspr_denumire."',
							copil1 = '".$inspr_copil1."',
							copil2 = '".$inspr_copil2."',
							copil3 = '".$inspr_copil3."' ";
							$que_pret = mysql_query($ins_pret) or die(mysql_error());
							@mysql_free_result($que_pret);
							
							if(in_array($key_prices, $_POST['pret_pivot'])) {
								$inspr_pret = $value_prices[$key_dates] / $inspr_nr_adulti; //pret pt pretul pivot
								
								$ins_pret_pivot = "INSERT INTO pret_pivot SET
								id_hotel = '".$inspr_id_hotel."',
								id_oferta = '".$inspr_id_oferta."',
								data_start = '".$inspr_data."',
								tip_camera = '".$inspr_id_camera."',
								pret = '".$inspr_pret."',
								moneda = '".$inspr_moneda."',
								pret_pivot = 'da',
								adulti = '".$inspr_nr_adulti."',
								copii = '".$inspr_nr_copii."',
								denumire = '".$inspr_denumire."',
								copil1 = '".$inspr_copil1."',
								copil2 = '".$inspr_copil2."',
								copil3 = '".$inspr_copil3."' ";
								$que_pret_pivot = mysql_query($ins_pret_pivot) or die(mysql_error());
								@mysql_free_result($que_pret_pivot);
							}
						}
					}
				}
			}
			echo '<script>alert(\'Preturile au fost importate!\'); document.location.href=\'/adm/pivot.php?pas=2&id_hotel='.$id_hotel.'&id_oferta='.$id_oferta.'\'; </script>';
			//echo '</ol>';
		} else {
			$id_cams = array_unique($pret['id_camera']);
?>
<br>
<h2 class="red">Bifati camerele pentru care doriti sa introduceti preturi</h2>
<br>
<form action="" method="post">
	<input type="hidden" name="denumire" value="<?php echo $denumire; ?>">
	<input type="hidden" name="tip_masa" value="<?php echo $tip_masa; ?>">
	<input type="hidden" name="plecari" value="<?php echo $plecari; ?>">
	<input type="hidden" name="preturi" value="<?php echo $preturi; ?>">
    <?php $i=0;
    foreach($id_cams as $k_id_cams => $v_id_cams) {
		$i++; ?>
    <div style="padding:5px;">
      <label><input type="checkbox" name="id_rooms[<?php echo $i; ?>]" value="<?php echo $v_id_cams; ?>"> <strong><?php echo get_den_camera($v_id_cams); ?></strong></label>
	  <div style="padding:5px 0 15px 30px;"><strong class="red">Alegeti gradul de ocupare pretul pivot</strong><br>
	  <?php foreach($pret['pret_pivot'] as $key_pp => $value_pp) {
		  if($value_pp=='da') {
			  if($pret['id_camera'][$key_pp]==$v_id_cams) { ?>
		<label><input type="radio" name="pret_pivot[<?php echo $i; ?>]" value="<?php echo $key_pp; ?>"> <?php echo $pret['nr_adulti'][$key_pp].' adulti'; ?></label><br>
	  <?php }
		  }
	  } ?>
	  </div>
    </div>
    <?php } ?>
    <br>
    <div style="padding:5px;"><input type="submit" name="trimite" class="submit" value="Insereaza preturile pentru camerele selectate"></div>
</form>
		<?php }
	}
}

//echo '<pre>';print_r($pret);echo '</pre>';
?>