<?php $id_hotel=$_GET['id_hotel']; ?>
<br><br>
<form method="post" name="modH" action="/adm/metode_import.php?metoda=7&id_hotel=<?php echo $id_hotel.'&id_oferta='.$_GET['id_oferta']; ?>&pas=2">
  <table width="100%" border="0" cellspacing="2" cellpadding="3" id="grad_ocupare" align="left" class="tabel-inside">
    <tr>
      <td align="right" valign="top" width="160">Denumire</td>
      <td align="left" valign="top"><input type="text" name="denumire" class="mediu" /></td>
    </tr>
    <tr>
      <td align="right" valign="top">Tip masa</td>
      <td align="left" valign="top">
        <select name="tip_masa" class="mediu float-left">
          <option>--</option>
          <option value="BB">BB</option>
          <option value="AB">AB (american breakfast)</option>
          <option value="HB">HB</option>
          <option value="FB">FB (pensiune completa)</option>
          <option value="AI">AI</option>
          <option value="UAI">UAI</option>
          <option value="NOMEAL">NOMEAL</option>
          <option value="NM">NM (fara masa)</option>
        </select>
        <div class="infos float-left"> Se selecteaza tipul mesei din oferta. Daca sunt mai multe tipuri de masa, se alege doar unul. Daca trebuie selectat un tip de masa care nu se regaeste in selectul alaturat, va rugam contactati departamentul tehnic (Daniel) :). </div>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top" width="160">Plecari</td>
      <td align="left" valign="top"><textarea name="plecari" class="mediu float-left"></textarea>
        <div class="infos float-left"> Se introduc <strong>doar</strong> celulele cu datele de plecare (fara coloanele de inceput, de obicei se afla pe randul galben).</div>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Preturi + Grad ocupare</td>
      <td align="left" valign="top"><textarea name="preturi" class="mare float-left"></textarea>
        <div class="infos float-left"> Se introduc toate randurile ce contin informatiile cu gradul de ocupare si preturile aferente acestora (de obicei imediat sub randul galben).<br>
        <strong>ATENTIE!</strong> Va rugam nu introduceti si suplimentele sau serviciile neincluse (ex: &quot;adl / Supliment HB Apollonia Bay Resort&quot;, sau &quot;adl / Transfer aeroport-hotel-aeroport Elia &quot;, etc).</div>
      </td>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2"><input type="submit" name="adauga" value="Importa" class="buttons" /></td>
    </tr>
  </table>
</form>
