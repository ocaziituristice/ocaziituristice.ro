<?php
$id_hotel = $_GET['id_hotel'];
$id_oferta = $_GET['id_oferta'];
$denumire = trim($_POST['denumire']);
$moneda = trim($_POST['moneda']);
if(isset($_POST['preturi'])) $_SESSION['preturi'] = $_POST['preturi'];
$preturi = $_SESSION['preturi'];

$err = '';

$pret['id_hotel'] = $id_hotel;
$pret['id_oferta'] = $id_oferta;
$pret['denumire'] = $denumire;
$pret['moneda'] = $moneda;

if(strlen($preturi)>0) {
	foreach($preturi as $k_preturi => $v_preturi) {
		if(strlen($v_preturi)>0) {
			$preturi_row[$k_preturi] = explode("\r",trim($v_preturi));
			$ix=0;
			foreach($preturi_row[$k_preturi] as $k_preturi_row => $v_preturi_row) {
				$preturi_col[$k_preturi] = explode("\t",trim($v_preturi_row));
				$iy=0;
				foreach($preturi_col[$k_preturi] as $k_preturi_col => $v_preturi_col) {
					if($ix==0) {
						if($k_preturi_col>0) {
							$sel_gradocup = "SELECT * FROM grad_ocupare_camera_corespondent WHERE denumire = '".$v_preturi_col."' ";
							$que_gradocup = mysql_query($sel_gradocup) or die(mysql_error());
							$row_gradocup = mysql_fetch_array($que_gradocup);
							if(mysql_num_rows($que_gradocup)>0) {
								$pret['grad_ocupare'][$k_preturi][$iy]['nr_adulti'] = $row_gradocup['adulti'];
								$pret['grad_ocupare'][$k_preturi][$iy]['nr_copii'] = $row_gradocup['copii'];
								$pret['grad_ocupare'][$k_preturi][$iy]['copil1'] = $row_gradocup['copil1'];
								$pret['grad_ocupare'][$k_preturi][$iy]['copil2'] = $row_gradocup['copil2'];
								$pret['grad_ocupare'][$k_preturi][$iy]['copil3'] = $row_gradocup['copil3'];
								$pret['grad_ocupare'][$k_preturi][$iy]['id_grad'] = $row_gradocup['id'];
								$pret['grad_ocupare'][$k_preturi][$iy]['denumire'] = $row_gradocup['denumire'];
								$pret['grad_ocupare'][$k_preturi][$iy]['pivot'] = $row_gradocup['pret_pivot'];
							} else {
								$err .= '<h3 class="red">EROARE ! Nu toate gradele de ocupare sunt completate!</h3><strong>- '.$v_preturi_col.'</strong><br><br>';
							}
						}
					} else {
						if($iy==0) {
							if($k_preturi_row>0) {
								$pret['date'][$k_preturi][$ix] = make_date_from_zzll($v_preturi_col);
							}
						} else {
							$price[$k_preturi][$ix][$iy] = explode("+",str_replace(",","",$v_preturi_col));
							$pret_final[$k_preturi][$ix][$iy] = $price[$k_preturi][$ix][$iy][0] + $price[$k_preturi][$ix][$iy][1];
							$pret['pret'][$k_preturi][$ix][$iy] = $pret_final[$k_preturi][$ix][$iy];
						}
					}
					$iy++;
				}
				$ix++;
			}
		}
	}
}

if(strlen($err)>0) {
	echo '<script>alert(\'Au aparut erori.\nPreturile nu au fost importate.\'); </script>';
	echo $err;
	echo '<div class="infos">Gradele de ocupare se asigneaza din baza de date in tabelul <strong>GRAD_OCUPARE_CAMERA_CORESPONDENT</strong> completandu-se compurile acestui tabel.</div><br>';
	echo '<button class="buttons" onClick="javascript: history.go(-1)">Preia datele din nou</button>';
} else {
	if(sizeof($pret['pret']>0)) {
		if((isset($_POST['trimite']) and (sizeof($_POST['id_pp'])>0))) {
			// inserare grad ocupare
			foreach($pret['grad_ocupare'] as $key_cam => $value_cam) {
				foreach($value_cam as $key_grad => $value_grad) {
					$ins_id_hotel = $pret['id_hotel'];
					$ins_id_camera = $key_cam;
					$ins_nr_adulti = $value_grad['nr_adulti'];
					$ins_nr_copii = $value_grad['nr_copii'];
					$ins_copil1 = $value_grad['copil1'];
					$ins_copil2 = $value_grad['copil2'];
					$ins_copil3 = $value_grad['copil3'];
					
					if($value_grad['pivot']=='nu') {
						$sel_grad_ocupare = "SELECT * FROM grad_ocupare_camera
						WHERE id_hotel = '".$ins_id_hotel."'
						AND id_camera = '".$ins_id_camera."'
						AND nr_adulti = '".$ins_nr_adulti."'
						AND nr_copii = '".$ins_nr_copii."'
						AND copil1 = '".$ins_copil1."'
						AND copil2 = '".$ins_copil2."'
						AND copil3 = '".$ins_copil3."' ";
						$que_grad_ocupare = mysql_query($sel_grad_ocupare);
						$row_grad_ocupare = mysql_fetch_array($que_grad_ocupare);
						$totalRows_grad_ocupare = mysql_num_rows($que_grad_ocupare);
						@mysql_free_result($que_grad_ocupare);
						if($totalRows_grad_ocupare=='0') {
							$ins_grad_ocup = "INSERT INTO grad_ocupare_camera SET
							id_hotel = '".$ins_id_hotel."',
							id_camera = '".$ins_id_camera."',
							nr_adulti = '".$ins_nr_adulti."',
							nr_copii = '".$ins_nr_copii."',
							copil1 = '".$ins_copil1."',
							copil2 = '".$ins_copil2."',
							copil3 = '".$ins_copil3."' ";
							//echo $ins_grad_ocup.'<br>';
							$que_grad_ocup = mysql_query($ins_grad_ocup) or die(mysql_error());
							@mysql_free_result($que_grad_ocup);
						}
					}
				}
			}
			
			// inserare pret pivot
			//echo '<ol>';
			foreach($pret['pret'] as $key_cams => $value_cams) {
				foreach($value_cams as $key_row => $value_row) {
					foreach($value_row as $key_col => $value_col) {
						$inspr_data = $pret['date'][$key_cams][$key_row];
						$inspr_id_hotel = $pret['id_hotel'];
						$inspr_id_oferta = $pret['id_oferta'];
						$inspr_denumire = $pret['denumire'];
						$inspr_id_camera = $key_cams;
						$inspr_nr_adulti = $pret['grad_ocupare'][$key_cams][$key_col]['nr_adulti'];
						$inspr_nr_copii = $pret['grad_ocupare'][$key_cams][$key_col]['nr_copii'];
						$inspr_copil1 = $pret['grad_ocupare'][$key_cams][$key_col]['copil1'];
						$inspr_copil2 = $pret['grad_ocupare'][$key_cams][$key_col]['copil2'];
						$inspr_copil3 = $pret['grad_ocupare'][$key_cams][$key_col]['copil3'];
						$inspr_pret = $value_col;
						$inspr_moneda = $pret['moneda'];

						if($pret['grad_ocupare'][$key_cams][$key_col]['pivot']=='nu') {
							$inspr_pret_pivot = 'nu';
							$inspr_introducere = 'da';
						} else if(in_array($pret['grad_ocupare'][$key_cams][$key_col]['id_grad'], $_POST['id_pp'], true)) {
							$inspr_pret_pivot = 'da';
							$inspr_introducere = 'da';
						} else {
							$inspr_pret_pivot = '';
							$inspr_introducere = 'nu';
						}
						
						if($inspr_introducere=='da') {
							$ins_pret = "INSERT INTO pret_pivot SET
							id_hotel = '".$inspr_id_hotel."',
							id_oferta = '".$inspr_id_oferta."',
							data_start = '".$inspr_data."',
							tip_camera = '".$inspr_id_camera."',
							pret = '".$inspr_pret."',
							moneda = '".$inspr_moneda."',
							pret_pivot = '".$inspr_pret_pivot."',
							adulti = '".$inspr_nr_adulti."',
							copii = '".$inspr_nr_copii."',
							denumire = '".$inspr_denumire."',
							copil1 = '".$inspr_copil1."',
							copil2 = '".$inspr_copil2."',
							copil3 = '".$inspr_copil3."' ";
							//echo $ins_pret.'<br>';
							$que_pret = mysql_query($ins_pret) or die(mysql_error());
							@mysql_free_result($que_pret);
						}

						//echo '<li>'.$inspr_data.' - '.$inspr_pret.' - '.$inspr_moneda.' - '.$inspr_id_camera.' - '.$inspr_id_hotel.' - '.$inspr_id_oferta.' - '.$inspr_denumire.' - '.$inspr_nr_adulti.' - '.$inspr_nr_copii.' - '.$inspr_copil1.' - '.$inspr_copil2.' - '.$inspr_copil3.' - '.$inspr_pret_pivot.' - '.$inspr_introducere.'</li>';
					}
				}
			}
			echo '<script>alert(\'Preturile au fost importate!\'); document.location.href=\'/adm/pivot.php?pas=2&id_hotel='.$id_hotel.'&id_oferta='.$id_oferta.'\'; </script>';
			//echo '</ol>';
		} else {
?>
<br>
<h2 class="red">Bifati optiunile pentru care doriti sa introduceti preturi</h2>
<br>
<form action="" method="post">
	<input type="hidden" name="denumire" value="<?php echo $denumire; ?>">
	<input type="hidden" name="moneda" value="<?php echo $moneda; ?>">
<?php foreach($pret['grad_ocupare'] as $k_cams => $v_cams) { ?>
	<div style="padding:5px;">
      <strong><?php echo get_den_camera($k_cams); ?></strong><br>
	<?php foreach($v_cams as $k_grad => $v_grad) {
		if($v_grad['pivot']=='da') { ?>
      <label><input type="radio" name="id_pp[<?php echo $k_cams; ?>]" value="<?php echo $v_grad['id_grad']; ?>"> <?php echo $v_grad['denumire']; ?></label><br>
	<?php }
	}?>
    </div>
<?php } ?>
    <br>
    <div style="padding:5px;"><input type="submit" name="trimite" class="submit" value="Insereaza preturile pentru camerele selectate"></div>
</form>
		<?php }
	}
}

//echo '<pre>';print_r($pret);echo '</pre>';
?>