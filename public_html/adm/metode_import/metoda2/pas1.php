<?php $id_hotel=$_GET['id_hotel']; ?>
<br/>
<br/>
<form method="post" name="modH" action="/adm/metode_import.php?metoda=2&id_hotel=<?php echo $id_hotel.'&id_oferta='.$_GET['id_oferta']; ?>&pas=2">
<table width="100%" border="0" cellspacing="2" cellpadding="3" id="grad_ocupare" align="left" class="tabel-inside">
<tr>
 <td align="right" valign="top" width="160">Denumire</td>
 <td align="left" valign="top"><input type="text" name="denumire" class="mediu" /></td>
</tr>
<tr>
 <td align="right" valign="top" width="160">Plecare</td>
 <td align="left" valign="top"><textarea name="plecare" class="mediu float-left"></textarea>
   <div class="infos float-left">
     Se introduc <strong>doar</strong> celulele cu datele de plecare (fara coloanele de inceput).<br>
     Dupa paste-ul din Excel, fiecare <strong>coloana</strong> trebuie sa fie cuprinsa intre <strong>ghilimele "</strong>.
   </div>
 </td>
</tr>
<?php $selCam="select tip_camera.denumire, tip_camera.id_camera from tip_camera inner join camere_hotel on tip_camera.id_camera = camere_hotel.id_camera where camere_hotel.id_hotel = '".$id_hotel."' Group by tip_camera.id_camera Order by camere_hotel.ordine ";
$queCam=mysql_query($selCam) or die(mysql_error());
while($rowCam=mysql_fetch_array($queCam)) { ?>
<tr>
 <td align="right" valign="top"><?php echo $rowCam['denumire']; ?></td>
 <td align="left" valign="top"><textarea name="preturi[<?php echo $rowCam['id_camera']; ?>]" class="mediu float-left"><?php echo $_POST['preturi'][$rowCam['id_camera']]; ?></textarea>
   <div class="infos float-left">
     Se introduc <strong>doar</strong> celulele cu preturi pentru camera dubla de la tipul respectiv de camera (acelasi numar de celule ca si la date).<br>
     Preturile <strong>trebuie</strong> sa <strong>nu</strong> contina <strong>punct .</strong> ci <strong>virgula ,</strong>.<br>
     Pentru modificarea <strong>automata</strong> a preturilor din tabele, va rugam contactati departamentul tehnic - Daniel :).
   </div>
 </td>
</tr>
<tr>
 <td align="right" valign="top" width="160">Grad ocupare <?php echo $rowCam['denumire']; ?></td>
 <td align="left" valign="top"><textarea name="grad_ocupare[<?php echo $rowCam['id_camera']; ?>]" class="mediu float-left"></textarea>
   <div class="infos float-left">
     Se introduc randurile ce contin informatiile cu gradul de ocupare si preturile aferente acestora.<br>
     Spre deosebire de ceilalti pasi, aici se copiaza <strong>toate celulele</strong> din tabelul in Excel, incluzand coloanele dinaintea preturilor.<br>
     In general, aceste randuri se afla dupa capul de tabel <strong>"Pret pe Camera"</strong>.
   </div>
 </td>
</tr>
<?php } @mysql_free_result($queCam); ?>
<tr>
 <td align="center" valign="top" colspan="2"><input type="submit" name="adauga" value="Importa" class="buttons" /></td>
</tr>
</table>
</form>