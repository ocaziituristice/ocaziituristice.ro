<?php
$id_hotel = $_GET['id_hotel'];
$id_oferta = $_GET['id_oferta'];
$denumire = trim($_POST['denumire']);
$tip_masa = trim($_POST['tip_masa']);
$nr_nopti = trim($_POST['nr_nopti']);
$grad_ocupare = $_POST['grad_ocupare'];
$preturi = $_POST['preturi'];

$err = '';

$pret['id_hotel'] = $id_hotel;
$pret['id_oferta'] = $id_oferta;
$pret['denumire'] = $denumire;
$pret['tip_masa'] = $tip_masa;
$pret['nr_nopti'] = $nr_nopti;
$pret['moneda'] = 'EURO';

foreach($grad_ocupare as $k_grad_ocupare => $v_grad_ocupare) {
	if(strlen($v_grad_ocupare)>0) {
		$grad1 = explode("\t",trim($v_grad_ocupare));
		foreach($grad1 as $k_grad1 => $v_grad1) {
			$grad2[$k_grad1] = explode("\r",str_replace('"','',$v_grad1));
			$grad2[$k_grad1][0] = cameraToAdulti($grad2[$k_grad1][0]);
			if(strpos($grad2[$k_grad1][0], '+EXB') === false) {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_adulti'] = '0';
			} else {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_adulti'] = '1';
			}
			$grad2[$k_grad1][0] = explode('+',str_replace(' ','',str_replace('+EXB','',$grad2[$k_grad1][0])));
			$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_adulti'] += $grad2[$k_grad1][0][0];
			if($grad2[$k_grad1][0][1]=='3CHD') {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_copii'] = '3';
			} else if($grad2[$k_grad1][0][1]=='2CHD') {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_copii'] = '2';
			} else if($grad2[$k_grad1][0][1]=='CHD') {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_copii'] = '1';
			} else {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_copii'] = '0';
			}
			$grad2[$k_grad1][1] = explode('/',trim($grad2[$k_grad1][1]));
			$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil1'] = $grad2[$k_grad1][1][0];
			if(($pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_copii']=='2') and ($grad2[$k_grad1][1][1]=='')) {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil2'] = $grad2[$k_grad1][1][0];
			} else {
				$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil2'] = $grad2[$k_grad1][1][1];
			}
			$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil3'] = $grad2[$k_grad1][1][2];
			
			// inserare grad ocupare
			$sel_grad_ocupare = "SELECT * FROM grad_ocupare_camera
			WHERE id_hotel = '".$pret['id_hotel']."'
			AND id_camera = '".$k_grad_ocupare."'
			AND nr_adulti = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_adulti']."'
			AND nr_copii = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_copii']."'
			AND copil1 = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil1']."'
			AND copil2 = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil2']."'
			AND copil3 = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil3']."' ";
			$que_grad_ocupare = mysql_query($sel_grad_ocupare);
			$row_grad_ocupare = mysql_fetch_array($que_grad_ocupare);
			$totalRows_grad_ocupare = mysql_num_rows($que_grad_ocupare);
			@mysql_free_result($que_grad_ocupare);
			if($totalRows_grad_ocupare=='0') {
				$ins_grad_ocup = "INSERT INTO grad_ocupare_camera SET
				id_hotel = '".$pret['id_hotel']."',
				id_camera = '".$k_grad_ocupare."',
				nr_adulti = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_adulti']."',
				nr_copii = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['nr_copii']."',
				copil1 = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil1']."',
				copil2 = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil2']."',
				copil3 = '".$pret['grad_ocupare'][$k_grad_ocupare][$k_grad1]['copil3']."' ";
				$que_grad_ocup = mysql_query($ins_grad_ocup) or die(mysql_error());
				@mysql_free_result($que_grad_ocup);
			}
		}
	}
}

foreach($preturi as $k_preturi => $v_preturi) {
	if(strlen($v_preturi)>0) {
		$pret_rows[$k_preturi] = explode("\r",trim($v_preturi));
		foreach($pret_rows[$k_preturi] as $k_pret_rows => $v_pret_rows) {
			$pret_cols[$k_pret_rows] = explode("\t",trim($v_pret_rows));
			$pret['preturi'][$k_preturi][$k_pret_rows]['data_start'] = date('Y-m-d',strtotime($pret_cols[$k_pret_rows][0]));
			$pret['preturi'][$k_preturi][$k_pret_rows]['tip_masa'] = $pret_cols[$k_pret_rows][1];
			$pret['preturi'][$k_preturi][$k_pret_rows]['nr_nopti'] = $pret_cols[$k_pret_rows][2];
			for($i=3; $i<sizeof($pret_cols[$k_pret_rows]); $i++) {
				$pret['preturi'][$k_preturi][$k_pret_rows]['pret'][] = $pret_cols[$k_pret_rows][$i];
			}
		}
	}
}

foreach($pret['preturi'] as $kins_cam => $vins_cam) {
	foreach($vins_cam as $kins_row => $vins_row) {
		foreach($vins_row['pret'] as $kins_pret => $vins_pret) {
			if(($pret['tip_masa']==$pret['preturi'][$kins_cam][$kins_row]['tip_masa']) and ($pret['nr_nopti']==$pret['preturi'][$kins_cam][$kins_row]['nr_nopti'])) {
				// inserare pret pivot
				$pret_pivot = 'nu';
				echo $ins_pret_pivot = "INSERT INTO pret_pivot SET
				id_hotel = '".$pret['id_hotel']."',
				id_oferta = '".$pret['id_oferta']."',
				data_start = '".$pret['preturi'][$kins_cam][$kins_row]['data_start']."',
				tip_camera = '".$kins_cam."',
				pret = '".$pret['preturi'][$kins_cam][$kins_row]['pret'][$kins_pret]."',
				moneda = '".$pret['moneda']."',
				pret_pivot = '".$pret_pivot."',
				adulti = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['nr_adulti']."',
				copii = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['nr_copii']."',
				denumire = '".$pret['denumire']."',
				copil1 = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['copil1']."',
				copil2 = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['copil2']."',
				copil3 = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['copil3']."' ";
				echo '<br>';
				$que_pret_pivot = mysql_query($ins_pret_pivot) or die(mysql_error());
				@mysql_free_result($que_pret_pivot);
				
				if((strpos($_POST['grad_ocupare'][$kins_cam], 'DBL') !== false) and ($pret['grad_ocupare'][$kins_cam][$kins_pret]['nr_adulti']=='2') and ($pret['grad_ocupare'][$kins_cam][$kins_pret]['nr_copii']=='0')) {
					$pret_pivot = 'da';
					$price = $pret['preturi'][$kins_cam][$kins_row]['pret'][$kins_pret] / 2;
				}
				
				if($pret_pivot=='da') {
					echo $ins_pret_pivot2 = "INSERT INTO pret_pivot SET
					id_hotel = '".$pret['id_hotel']."',
					id_oferta = '".$pret['id_oferta']."',
					data_start = '".$pret['preturi'][$kins_cam][$kins_row]['data_start']."',
					tip_camera = '".$kins_cam."',
					pret = '".$price."',
					moneda = '".$pret['moneda']."',
					pret_pivot = '".$pret_pivot."',
					adulti = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['nr_adulti']."',
					copii = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['nr_copii']."',
					denumire = '".$pret['denumire']."',
					copil1 = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['copil1']."',
					copil2 = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['copil2']."',
					copil3 = '".$pret['grad_ocupare'][$kins_cam][$kins_pret]['copil3']."' ";
					echo '<br>';
					$que_pret_pivot2 = mysql_query($ins_pret_pivot2) or die(mysql_error());
					@mysql_free_result($que_pret_pivot2);
				}
			}
		}
	}
}
echo '<script>alert(\'Preturile au fost importate!\'); document.location.href=\'/adm/pivot.php?pas=2&id_hotel='.$id_hotel.'&id_oferta='.$id_oferta.'\'; </script>';

//echo '<pre>';print_r($pret);echo '</pre>';
?>