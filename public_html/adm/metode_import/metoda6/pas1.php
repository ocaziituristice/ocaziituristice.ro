<?php $id_hotel=$_GET['id_hotel']; ?>
<br><br>
<form method="post" name="modH" action="/adm/metode_import.php?metoda=6&id_hotel=<?php echo $id_hotel.'&id_oferta='.$_GET['id_oferta']; ?>&pas=2">
  <table width="100%" border="0" cellspacing="2" cellpadding="3" id="grad_ocupare" align="left" class="tabel-inside">
    <tr>
      <td align="right" valign="top" width="180">Denumire</td>
      <td align="left" valign="top"><input type="text" name="denumire" class="mare" /></td>
    </tr>
    <tr>
      <td align="right" valign="top">Tip masa</td>
      <td align="left" valign="top">
        <input name="tip_masa" class="mediu float-left" value="">
        <div class="infos float-left"> Introduceti tipul mesei al ofertei care se introduce. Tipul mesei trebuie introdus <strong>EXACT</strong> cum se regaseste in excel (UAI, AI, BB, HB, etc). </div>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Nr. nopti</td>
      <td align="left" valign="top">
        <input name="nr_nopti" class="mic float-left" value="">
        <div class="infos float-left"> Introduceti numarul de nopti (in cifre) pentru preturile care se vor importa (7, 9, 14, etc). </div>
      </td>
    </tr>
<?php $selCam="select tip_camera.denumire, tip_camera.id_camera from tip_camera inner join camere_hotel on tip_camera.id_camera = camere_hotel.id_camera where camere_hotel.id_hotel = '".$id_hotel."' Group by tip_camera.id_camera Order by camere_hotel.ordine ";
$queCam=mysql_query($selCam) or die(mysql_error());
while($rowCam=mysql_fetch_array($queCam)) { ?>
    <tr>
      <td align="right" valign="top">Grad ocupare <strong><?php echo $rowCam['denumire']; ?></strong></td>
      <td align="left" valign="top"><textarea name="grad_ocupare[<?php echo $rowCam['id_camera']; ?>]" class="mediu float-left"></textarea>
        <div class="infos float-left"> Introduceti <strong>DOAR</strong> valorile cu gradul de ocupare, nu toata linia principala a tipului de camera (in general aceste valori au background turquoise). </div>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Preturi + Plecari <strong><?php echo $rowCam['denumire']; ?></strong></td>
      <td align="left" valign="top"><textarea name="preturi[<?php echo $rowCam['id_camera']; ?>]" class="mare float-left"></textarea>
        <div class="infos float-left"> Introduceti <strong>TOATE</strong> liniile de la tipul de camera respectiv, inclusiv data, masa, nr nopti (nu conteaza daca sunt mai multe tipuri de masa, sau mai multe tipuri de nr nopti, filtrarea se va face la introducere in functie de campurile completate mai sus). </div>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="top"><hr></td>
    </tr>
<?php } ?>
    <tr>
      <td align="center" valign="top" colspan="2"><input type="submit" name="adauga" value="Importa" class="buttons" /></td>
    </tr>
  </table>
</form>
