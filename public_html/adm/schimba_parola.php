<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
 <!-- START CENTRU PAGINA -->
  <h1>Schimba parola </h1>
  <?php if($_POST['edit']) {
   $schimba=new LOGIN($user_db);
   echo $schimba->schimba_parola($_POST['vechea_parola'], $_POST['parola'], $_POST['re_parola']);
  } ?>

    <form name="fr" id="fr" method="post" action="">
        <table width="100%" border="0" cellspacing="2" cellpadding="4" class="default">
         <tr>
          <th align="right" valign="top" width="160">Vechea parola:</th>
          <td align="left" valign="top"><input type="password" class="mediu" name="vechea_parola" value="" /></td>
         </tr>
         <tr>
          <th align="right" valign="top">Noua parola:</th>
          <td align="left" valign="top"><input type="password" class="mediu" name="parola" value="" /></td>
         </tr>
        <tr>
          <th align="right" valign="top">Reintroduceti parola:</th>
          <td align="left" valign="top"><input type="password" class="mediu" name="re_parola" value="" /></td>
         </tr>
         <tr>
           <td align="right" valign="top">&nbsp;</td>
          <td align="left" valign="top"><input type="submit" name="edit" value="Modifica" class="buttons"></td>
         </tr>
   </table>
   </form>
 
  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>