<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&language=ro"></script>
<?php include($_SERVER['DOCUMENT_ROOT'].'/adm/addins_head.php');
if($_GET['hotel']) {
include_once($_SERVER['DOCUMENT_ROOT'].'/adm/class/hotel.php');	
$id_hotel=$_REQUEST['hotel'];
$add_t=new HOTEL;
$param=$add_t->select_camp_hotel($id_hotel);
if($param['latitudine']) $lat=$param['latitudine']; else $lat='44.437711';
if($param['longitudine']) $long=$param['longitudine']; else $long='26.097366'; ?>
<input type="hidden" name="cord_lat" id="cord_lat" value="<?php echo $lat; ?>" />
<input type="hidden" name="cord_lon" id="cord_lon" value="<?php echo $long; ?>" />
<script type="text/javascript" src="js/map/main.js"></script>
<script type="text/javascript" src="js/selectDep.js"></script>
<?php }
include_once($_SERVER['DOCUMENT_ROOT'].'/adm/includes/functii_adm.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
<?php //centru_______________________________________ 
if(!$pas=$_REQUEST['pas']) $pas=1;
switch($pas)
{
	case "1": include_once "hotel/pas1.php"; break;
	case "2": include_once "hotel/pas2.php"; break;
	case "3": include_once "hotel/vizualizeaza.php"; break;
}
?>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>