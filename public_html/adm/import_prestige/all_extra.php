<h2>Alegeti un fisier pentru a efectua uploadul</h2>
<br>
<form action="" method="post">
<?php
$files = glob($_SERVER['DOCUMENT_ROOT']."/includes/import_aida_prestige/*.xml", 1);
//echo '<pre>';print_r($files);echo '</pre>';
foreach($files as $v_files) {
	echo '<label style="display:block; padding:5px;"><input type="radio" name="file" value="'.basename($v_files).'"> '.basename($v_files).' - <strong class="red">'.round((filesize($v_files)/(1024*1024)),2).' MB</strong>';
	if($_SESSION['import_prestige']['extra'][basename($v_files)]) echo ' - <strong>Data import: '.$_SESSION['import_prestige']['extra'][basename($v_files)].'</strong>';
	echo '</label>';
}
?>
	<br>
	<input type="submit" value="Importa" class="buttons">
</form>
<?php
if($_REQUEST['file']) {
	$_SESSION['import_prestige']['extra'][$_REQUEST['file']] = date("d.m.Y H:i:s");

	$sUrl = $sitepath.'includes/import_aida_prestige/'.$_REQUEST['file'];
	//$sUrl = $sitepath.'test/prestige.xml';
	
	$prices = new XMLReader();
	$prices->open($sUrl);
	
	$err = 0;
	
	while($prices->read()) {
		if($prices->nodeType == XMLReader::ELEMENT && $prices->name == 'Package') {
			$doc = new DOMDocument('1.0', 'UTF-8');
			$xml = simplexml_import_dom($doc->importNode($prices->expand(),true));

			$Package_ID = (string) $xml['ID'];
			$Package_Type = (string) $xml['Type'];
			$Package_Name = (string) $xml['Name'];
			$Package_Currency = (string) $xml['Currency'];
			$Package_Code = (string) $xml['Code'];

			if(sizeof($xml->PackEntries)>0) {
				foreach($xml->PackEntries->PackEntry as $k_pack => $v_pack) {
					$PackEntry_ID = (string) $v_pack['ID'];
					$PackEntry_DateStart = (string) $v_pack['DateStart'];
					$PackEntry_DateEnd = (string) $v_pack['DateEnd'];
					$PackEntry_nopti = (string) $v_pack->Duration;
					
					if(sizeof($v_pack->ExtraServices)>0) {
						$ins_sql[$k_pack] = array();
						foreach($v_pack->ExtraServices->ExtraService as $k_extra => $v_extra) {
							$ExtraService_ID = (string) $v_extra['ID'];
							$ExtraService_Name = (string) $v_extra['Name'];
							$ExtraService_Type = (string) $v_extra['Type'];
							$ExtraService_Mandatory = (string) $v_extra['Mandatory'];
							
							if($ExtraService_Mandatory=='true') {
								$ins_sql[$k_pack][] = '("'.$id_furnizor.'", "'.$PackEntry_ID.'", "'.$ExtraService_ID.'", "'.$ExtraService_Name.'", "'.$ExtraService_Type.'", "'.$ExtraService_Mandatory.'")';
							}
						}
	
						if(sizeof($ins_sql[$k_pack])>0) {
							$ins_extra = "INSERT INTO import_aidatemp_extra (id_furnizor, PackEntry_ID, ExtraService_ID, ExtraService_Name, ExtraService_Type, ExtraService_Mandatory) VALUES ".implode(',', $ins_sql[$k_pack])." ";
							$que_extra = mysql_query($ins_extra) or die(mysql_error());
							@mysql_free_result($que_extra);
						}
											
					} else {
						echo '<p>Nu exista <strong>ExtraServices</strong>. Pachetul '.$PackEntry_ID.' nu a fost introdus!</p>';
					}
					
					if(sizeof($v_pack->PackConfigurations)>0) {
						foreach($v_pack->PackConfigurations->Configuration as $k_combo => $v_combo) {
							if(sizeof($v_combo->Transportation)>0) {
								foreach($v_combo->Transportation as $k_transport => $v_transport) {
									$sel_trans = "SELECT * FROM import_aidatemp_transport WHERE id_furnizor = '".$id_furnizor."' AND ServiceID = '".(string) $v_transport['ServiceID']."' ";
									$que_trans = mysql_query($sel_trans) or(mysql_error());
									if(mysql_num_rows($que_trans)==0 and (string) $v_transport['FlightNo']!='') {										
										$flight[$k_transport] = explode(" ", (string) $v_transport['FlightNo']);
										if(count($flight[$k_transport])==1) {
											$flight_1[$k_transport] = preg_split('#(?<=[a-z])(?=\d)#i', $flight[$k_transport][0]);
											$flight_company[$k_transport] = $flight_1[$k_transport][0];
											$flight_no[$k_transport] = $flight_1[$k_transport][1];
										} else if(count($flight[$k_transport])==2) {
											$flight_company[$k_transport] = $flight[$k_transport][0];
											$flight_no[$k_transport] = $flight[$k_transport][1];
										}
										
										if($flight_no[$k_transport]=='OB') $flight_no[$k_transport] = '0B';
										
										$ins_transport = "INSERT INTO import_aidatemp_transport SET
										id_furnizor = '".$id_furnizor."',
										ServiceID = '".(string) $v_transport['ServiceID']."',
										Name = '".(string) $v_transport['Name']."',
										Code = '".(string) $v_transport['Code']."',
										Carrier = '".(string) $v_transport['Carrier']."',
										FlightNo = '".(string) $v_transport['FlightNo']."',
										cod_companie = '".$flight_company[$k_transport]."',
										nr_cursa = '".$flight_no[$k_transport]."'
										";
										$que_transport = mysql_query($ins_transport) or die(mysql_error());
										@mysql_free_result($que_transport);
									}
								}
							}
						}
					}
				}
			}
			$err += 1;
		}
	}
	
	if($err>0) {
		echo '<script>alert("ExtraServices au fost updatate cu succes."); document.location.href="/adm/import_prestige.php?pas=all_extra";</script>';
	} else {
		echo '<br><br><h1>Nu sunt preturi disponibile!</h1>';
		echo '<h2>Se va reveni la aceasta pagina automat in <span id="seconds" class="red">3</span></h2>';
		echo "<script>var seconds = 3; setInterval(function(){if (seconds <= 1) { window.location = '/adm/import_prestige.php?pas=all_extra'; } else { document.getElementById('seconds').innerHTML = --seconds; } }, 1000);</script>";
	}
}
?>
