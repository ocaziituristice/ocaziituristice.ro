<?php include($_SERVER['DOCUMENT_ROOT'].'/adm/cofig_adm.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');

$ids = str_replace(",,", ",", $_GET['oferte']);
?>

Id-uri introduse: <input type="text" name="iduri" id="iduri" value="<?php echo $ids; ?>" class="mare">
<table cellpadding="2" cellspacing="2" border="1" width="100%" align="center" class="tabel">
  <tr>
    <th align="center" valign="top" width="100">#</th>
    <th align="center" valign="top" width="100">Tara</th>
    <th align="center" valign="top" width="150">Zona</th>
    <th align="center" valign="top">Oferta</th>
    <th align="center" valign="top">Hotel</th>
    <th align="center" valign="top" width="80">Transport</th>
    <th align="center" valign="top" width="80">Nr. nopti</th>
    <th align="center" valign="top" width="100">Masa</th>
    <th align="center" valign="top" width="100">Valabila</th>
    <th align="center" valign="top" width="50">Sterge</th>
  </tr>
<?php
//foreach($iduri as $value) {
	$sel_oferte = "SELECT
	oferte.id_oferta,
	oferte.denumire_scurta,
	oferte.nr_nopti,
	oferte.masa,
	oferte.valabila,
	transport.denumire AS den_transport,
	hoteluri.id_hotel,
	hoteluri.nume,
	hoteluri.stele,
	tari.denumire AS den_tara,
	zone.denumire AS den_zona
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN tari ON zone.id_tara = tari.id_tara
	INNER JOIN transport ON oferte.id_transport = transport.id_trans
	WHERE oferte.id_oferta IN (".$ids.")
	ORDER BY tari.denumire ASC, zone.denumire ASC, hoteluri.nume ASC, oferte.denumire ASC
	";
	$que_oferte = mysql_query($sel_oferte) or die(mysql_error());
	$i=0;
	while($row_oferte = mysql_fetch_array($que_oferte)) {
		$i++;
	?>
    <tr class="normal" onmouseout="this.className='normal'" onmouseover="this.className='hover'" id="<?php echo $row_oferte['id_oferta']; ?>">
      <td align="center" valign="top"><?php echo $row_oferte['id_oferta']; ?></td>
      <td align="center" valign="top"><?php echo $row_oferte['den_tara']; ?></td>
      <td align="left" valign="top"><?php echo $row_oferte['den_zona']; ?></td>
      <td align="left" valign="top"><a href="<?php echo "editare_sejur.php?pas=2&oferta=".$row_oferte['id_oferta']; ?>" class="titluArticol" target="_blank"><?php echo $row_oferte['denumire_scurta']; ?></a></td>
      <td align="left" valign="top"><a href="editare_hotel.php?pas=2&hotel=<?php echo $row_oferte['id_hotel']; ?>" target="_blank"><?php echo $row_oferte['nume'].' <img src="'.$sitepath.'images/spacer.gif" class="stele-mici-'.$row_oferte['stele'].'" />'; ?></a></td>
	  <td align="left" valign="top"><?php echo $row_oferte['den_transport']; ?></td>
      <td align="left" valign="top"><?php echo $row_oferte['nr_nopti'].' nopti'; ?></td>
      <td align="left" valign="top"><?php echo $row_oferte['masa']; ?></td>
      <td align="center" valign="top"><?php if($row_oferte['valabila']=='da') echo '<img src="images/yes.png"> <span class="green bold">Valabila</span>'; else echo '<img src="images/no.png"> <span class="red bold">Nevalabila</span>'; ?></td>
      <td align="left" valign="top"><a class="buttons" onClick="remove_offer('<?php echo $row_oferte['id_oferta']; ?>')" style="cursor:pointer;">X</a></td>
    </tr>
<?php }
//} ?>
</table>

<script>
function remove_offer(id) {
	var ids = document.getElementById('iduri').value;
	var ids_new = ids.replace(id, "");
	var ids_final = ids_new.replace(",,", ",");
	/*document.getElementById('iduri').value = ids_final;*/
	$("#iduri").val(ids_final);
	$("#"+id).remove();
}
</script>
