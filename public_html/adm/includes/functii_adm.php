<?php
function select($tip,$filtru) {
	if($tip=='tari') $rez=select_tari($filtru);
	if($tip=='zona') $rez=select_zona($filtru);
	if($tip=='localitate') $rez=select_localitate($filtru);
	if($tip=='hotel') $rez=select_hotel($filtru);
	if($tip=='furnizori') $rez=select_furnizor($filtru);
	if($tip=='cupoane') $rez=select_cupoane($filtru);
	if($tip=='cupoane_campanii') $rez=select_cupoane_campanii($filtru);
	if($tip=='sejur') $rez=select_sejur($filtru);
	if($tip=='aeroporturi') $rez=select_aeroporturi($filtru);
	if($tip=='excursii') $rez=select_excursii($filtru);
	if($tip=='circuit') $rez=select_circuit($filtru);
	if($tip=='rezervari') $rez=select_rezervari($filtru);
	if($tip=='vouchere') $rez=select_vouchere($filtru);
	if($tip=='cereri') $rez=select_cereri($filtru);
	if($tip=='useri') $rez=select_useri($filtru);
	if($tip=='erori_disponibilitate') $rez=select_erori_disponibilitate($filtru);
	if($tip=='articole') $rez=select_articole($filtru);
	if($tip=='tranzactii') $rez=select_tranzactii($filtru);
	return $rez;
}

//selecturi_________________________
function select_tari($filtru) {
	$query='SELECT
	tari.id_tara,
	tari.denumire,
	tari.descriere,
	tari.nume_continent,
	tari.poza1,
	tari.poza2,
	tari.poza3,
	tari.id_continent,
	tari.tag,
	tari.ordine,
	tari.descriere_scurta,
	tari.descriere_seo,
	tari.titlu_seo,
	tari.cuvinte_cheie_seo,
	continente.nume_continent,
	tari.prima_pagina,
	tari.steag
	FROM
	tari
	left join zone on tari.id_tara = zone.id_tara
	Left Join localitati ON zone.id_zona = localitati.id_zona
	Inner Join continente ON tari.id_continent = continente.id_continent 
	where 1=1 ';
	if($filtru['cauta']) $query=$query.' and (TRIM(LOWER(tari.denumire)) like "%'.trim(desfa_link_seach($filtru['cauta'])).'%" or TRIM(LOWER(tari.denumire)) like "%'.trim(desfa_link($filtru['cauta'])).'%" OR tari.id_tara = "'.$filtru['cauta'].'" ) ';
	$query=$query.' Group By tari.id_tara
	Order by tari.id_tara DESC ';
	
	return $query;
}

function select_zona($filtru) {
	$query='SELECT
	zone.id_tara,
	zone.id_zona,
	tari.denumire as denumire_tara,
	zone.denumire,
	zone.descriere,
	zone.poza1,
	zone.poza2,
	zone.poza3,
	zone.harta,
	zone.tag,
	zone.descriere_scurta,
	zone.descriere_seo,
	zone.titlu_seo,
	zone.cuvinte_cheie_seo,
	zone.harta
	FROM
	zone
	left join tari on zone.id_tara = tari.id_tara
	where 1=1 ';
	if($filtru['tara']) $query=$query.' and zone.id_tara = "'.$filtru['tara'].'" ';
	if($filtru['cauta']) $query=$query.' and (TRIM(LOWER(zone.denumire)) like "%'.trim(desfa_link_seach($filtru['cauta'])).'%" or TRIM(LOWER(zone.denumire)) like "%'.trim(desfa_link($filtru['cauta'])).'%" or zone.id_zona = "'.$filtru['cauta'].'" )';
	$query=$query.' Group By zone.id_zona
	Order by zone.id_zona DESC ';
	
	return $query;
}

function select_localitate($filtru) {
	$query='SELECT
	localitati.id_localitate,
	zone.id_zona,
	tari.id_tara,
	zone.denumire as denumire_zona,
	tari.denumire as denumire_tara,
	localitati.denumire,
	localitati.descriere,
	localitati.poza1,
	localitati.poza2,
	localitati.poza3,
	localitati.harta,
	localitati.descriere_scurta,
	localitati.descriere_seo,
	localitati.titlu_seo,
	localitati.cuvinte_cheie_seo,
	localitati.oras_plecare
	FROM
	localitati
	left join zone on localitati.id_zona = zone.id_zona
	left join tari on zone.id_tara = tari.id_tara
	where 1=1  ';
	if($filtru['tara']) $query=$query.' and zone.id_tara = "'.$filtru['tara'].'" ';
	if($filtru['cauta']) $query= $query.' and (TRIM(LOWER(localitati.denumire)) like "%'.trim(desfa_link_seach($filtru['cauta'])).'%" or TRIM(LOWER(localitati.denumire)) like "%'.trim(desfa_link($filtru['cauta'])).'%" or localitati.id_localitate = "'.$filtru['cauta'].'" ) ';
	$query=$query.' Group By localitati.id_localitate
	Order by localitati.id_localitate DESC ';
	
	return $query;
}

function select_hotel($filtru) {
	if((!$filtru['cauta'] || ($filtru['cauta'] && !is_numeric($filtru['cauta']))) && !$filtru['furnizor'] && !$filtru['recomandata'] && !$filtru['compare_date']) {
		$query='SELECT
		localitati.id_localitate,
		zone.id_zona,
		hoteluri.id_hotel,
		tari.id_tara,
		localitati.denumire AS denumire_localitate,
		zone.denumire AS denumire_zona,
		tari.denumire AS denumire_tara,
		hoteluri.nume,
		hoteluri.descriere,
		hoteluri.poza1,
		hoteluri.poza2,
		hoteluri.poza3,
		hoteluri.descriere_scurta,
		hoteluri.descriere_seo,
		hoteluri.titlu_seo,
		hoteluri.cuvinte_cheie_seo,
		';
		if($filtru['not_facilitati']) $query = $query.' facilities.facilitati, ';
		$query=$query.' hoteluri.stele
		FROM hoteluri
		LEFT JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		LEFT JOIN zone ON localitati.id_zona = zone.id_zona
		LEFT JOIN tari ON zone.id_tara = tari.id_tara
		';
		if($filtru['not_facilitati']) $query = $query." LEFT JOIN (SELECT GROUP_CONCAT(lower(caracteristici_hotel.denumire) SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		$query=$query.' WHERE 1=1 AND hoteluri.tip_unitate <> "Circuit" ';
		if($filtru['tara']) $query=$query." AND tari.id_tara = '".$filtru['tara']."' ";
		if($filtru['zona']) $query=$query." AND zone.id_zona ='".$filtru['zona']."' ";
		if($filtru['localitate']) $query=$query." AND localitati.id_localitate ='".$filtru['localitate']."' ";
		if($filtru['cauta']) {
			$query=$query.' AND ( TRIM(LOWER(hoteluri.nume)) LIKE "%'.trim(desfa_link_seach($filtru['cauta'])).'%" OR TRIM(LOWER(hoteluri.nume)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%" OR hoteluri.id_hotel = "'.$filtru['cauta'].'" ';
			if($filtru['search_all']) $query=$query.'
			OR TRIM(LOWER(hoteluri.descriere)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.descriere_scurta)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.comentariul_nostru)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.new_descriere)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.new_camera)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.new_teritoriu)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.new_relaxare)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.new_pentru_copii)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.new_plaja)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.general)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			OR TRIM(LOWER(hoteluri.servicii)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%"
			';
			$query=$query.' ) ';
		}
		if($filtru['not_facilitati']) $query=$query.' AND ( facilities.facilitati NOT LIKE "%'.trim(desfa_link($filtru['cauta'])).'%" OR facilities.facilitati IS NULL ) ';
		$query=$query.' GROUP BY hoteluri.id_hotel
		ORDER BY hoteluri.id_hotel DESC';
		if($filtru['cauta']) {
			$rez=mysql_query($query) or die(mysql_error());
			if(mysql_num_rows($rez)=='0') $query=select_hotel_bysejur($filtru);
			@mysql_free_result($rez);
		}
	} else $query=select_hotel_bysejur($filtru);
	
	return $query;
}

function select_hotel_bysejur($filtru) {
	$query='SELECT
	localitati.id_localitate,
	zone.id_zona,
	hoteluri.id_hotel,
	tari.id_tara,
	localitati.denumire as denumire_localitate,
	zone.denumire as denumire_zona,
	tari.denumire as denumire_tara,
	hoteluri.nume,
	hoteluri.descriere,
	hoteluri.poza1,
	hoteluri.poza2,
	hoteluri.poza3,
	hoteluri.descriere_scurta,
	hoteluri.descriere_seo,
	hoteluri.titlu_seo,
	hoteluri.cuvinte_cheie_seo,
	hoteluri.stele
	FROM
	oferte
	left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	left Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	left join transport ON oferte.id_transport = transport.id_trans
	left Join localitati ON hoteluri.locatie_id = localitati.id_localitate
	left Join zone ON localitati.id_zona = zone.id_zona
	left Join tari ON zone.id_tara = tari.id_tara
	WHERE 1=1 and hoteluri.tip_unitate <> "Circuit" ';
	if($filtru['tara']) $query=$query." and tari.id_tara = '".$filtru['tara']."' ";
	if($filtru['zona']) $query=$query." AND zone.id_zona ='".$filtru['zona']."' ";
	if($filtru['localitate']) $query=$query." AND localitati.id_localitate ='".$filtru['localitate']."' ";
	if($filtru['furnizor']) $query=$query." AND oferte.furnizor = '".$filtru['furnizor']."' ";
	if($filtru['recomandata']) $query=$query." AND oferte.recomandata = 'da' ";
	if($filtru['compare_date']) $query=$query." AND oferte.ultima_modificare ".$filtru['compare']." '".$filtru['compare_date']."' ";
	if($filtru['cauta']) $query=$query.' and (TRIM(LOWER(oferte.denumire)) like "%'.trim(desfa_link_seach($filtru['cauta'])).'%" or TRIM(LOWER(oferte.denumire)) like "%'.trim(desfa_link($filtru['cauta'])).'%" or oferte.id_oferta = "'.$filtru['cauta'].'" ) ';
	$query=$query.' Group By hoteluri.id_hotel
	Order by hoteluri.id_hotel DESC';
	return $query;
}

function select_circuit($filtru) {
	if((!$filtru['cauta'] || ($filtru['cauta'] && !is_numeric($filtru['cauta']))) && !$filtru['furnizor'] && !$filtru['recomandata']) {
		$query = "SELECT
		hoteluri.id_hotel,
		hoteluri.nume,
		hoteluri.descriere,
		hoteluri.poza1,
		hoteluri.descriere_scurta,
		tari.id_tara,
		tari.denumire as denumire_tara,
		continente.nume_continent
		FROM hoteluri
		LEFT JOIN traseu_circuit ON (traseu_circuit.id_hotel_parinte = hoteluri.id_hotel AND traseu_circuit.tara_principala = 'da')
		LEFT JOIN tari ON tari.id_tara = traseu_circuit.id_tara
		LEFT JOIN continente ON continente.id_continent = tari.id_continent
		LEFT JOIN oferte ON oferte.id_hotel = hoteluri.id_hotel
		WHERE 1=1
		AND hoteluri.tip_unitate = 'Circuit' ";
		if($filtru['tara']) $query = $query." AND tari.id_tara = '".$filtru['tara']."' ";
		if($filtru['furnizor']) $query = $query." AND oferte.furnizor = '".$filtru['furnizor']."' ";
		if($filtru['recomandata']) $query = $query." AND oferte.recomandata = 'da' ";
		if($filtru['cauta']) $query = $query." AND (TRIM(LOWER(hoteluri.nume)) like '%".trim(desfa_link_seach($filtru['cauta']))."%' or TRIM(LOWER(hoteluri.nume)) like '%".trim(desfa_link($filtru['cauta']))."%' or hoteluri.id_hotel = '".$filtru['cauta']."' ) ";
		$query = $query." GROUP BY hoteluri.id_hotel
		ORDER BY hoteluri.id_hotel DESC ";
		if($filtru['cauta']) {
			$rez=mysql_query($query) or die(mysql_error());
			if(mysql_num_rows($rez)=='0') $query=select_circuit_bysejur($filtru);
			@mysql_free_result($rez);
		}
	} else $query=select_circuit_bysejur($filtru);
	return $query;
}

function select_circuit_bysejur($filtru) {
	$query = "SELECT
	hoteluri.id_hotel,
	hoteluri.nume,
	hoteluri.descriere,
	hoteluri.poza1,
	hoteluri.descriere_scurta,
	tari.id_tara,
	tari.denumire as denumire_tara,
	continente.nume_continent
	FROM hoteluri
	LEFT JOIN traseu_circuit ON (traseu_circuit.id_hotel_parinte = hoteluri.id_hotel AND traseu_circuit.tara_principala = 'da')
	LEFT JOIN tari ON tari.id_tara = traseu_circuit.id_tara
	LEFT JOIN continente ON continente.id_continent = tari.id_continent
	LEFT JOIN oferte ON oferte.id_hotel = hoteluri.id_hotel
	WHERE 1=1
	AND hoteluri.tip_unitate = 'Circuit' ";
	if($filtru['tara']) $query = $query." AND tari.id_tara = '".$filtru['tara']."' ";
	if($filtru['furnizor']) $query = $query." AND oferte.furnizor = '".$filtru['furnizor']."' ";
	if($filtru['recomandata']) $query = $query." AND oferte.recomandata = 'da' ";
	if($filtru['cauta']) $query = $query." AND (TRIM(LOWER(hoteluri.nume)) like '%".trim(desfa_link_seach($filtru['cauta']))."%' or TRIM(LOWER(hoteluri.nume)) like '%".trim(desfa_link($filtru['cauta']))."%' or hoteluri.id_hotel = '".$filtru['cauta']."' ) ";
	$query = $query." GROUP BY hoteluri.id_hotel
	ORDER BY hoteluri.id_hotel DESC";
	
	return $query;
}

function select_sejur($filtru) {
	$query = 'SELECT
	oferte.id_oferta,
	oferte.denumire,
	oferte.data_adaugarii,
	oferte.start_date,
	oferte.end_date,
	oferte.valabila,
	oferte.id_transport,
	oferte.nr_zile,
	oferte.nr_nopti,
	oferte.masa,
	hoteluri.nume,
	hoteluri.poza1 AS poza1,
	hoteluri.stele,
	hoteluri.nume AS denumire_hotel,
	hoteluri.tip_unitate,
	transport.denumire AS denumire_trans,
	localitati.denumire AS denumire_localitate,
	tari.denumire AS denumire_tara
	FROM oferte
	LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	LEFT JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	LEFT JOIN transport ON oferte.id_transport = transport.id_trans
	LEFT JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	LEFT JOIN zone ON localitati.id_zona = zone.id_zona
	LEFT JOIN tari ON zone.id_tara = tari.id_tara
	WHERE 1=1 AND hoteluri.tip_unitate <> "Circuit" ';
	if($filtru['valabila']) $query=$query.' AND oferte.valabila = "da" ';
	if($filtru['nevalabila']) $query=$query.' AND (oferte.valabila = "nu" || oferte.valabila is null ) ';
	if($filtru['availability']) $query=$query.' AND oferte.valabila = "'.$filtru['availability'].'" ';
	if($filtru['id_hotel']) $query=$query.' AND oferte.id_hotel ="'.$filtru['id_hotel'].'" ';
	if($filtru['id_zona']) $query=$query.' AND zone.id_zona = "'.$filtru['id_zona'].'" ';
	if($filtru['id_localitate']) $query=$query.' AND localitati.id_localitate ="'.$filtru['id_localitate'].'" ';
	if($filtru['tara']) $query=$query.' AND tari.id_tara = "'.$filtru['tara'].'" ';
	if($filtru['furnizor']) $query=$query." AND oferte.furnizor = '".$filtru['furnizor']."' ";
	if($filtru['recomandata']) $query=$query." AND oferte.recomandata = 'da' ";
	if($filtru['transport']) $query=$query." AND oferte.id_transport = '".$filtru['transport']."' ";
	if($filtru['cautare_live']) $query=$query." AND hoteluri.cautare_live = '".$filtru['cautare_live']."' ";
	if($filtru['cauta']) $query=$query.' AND (oferte.denumire LIKE "%'.trim(desfa_link_seach($filtru['cauta'])).'%" OR oferte.denumire LIKE "%'.trim(desfa_link($filtru['cauta'])).'%" OR oferte.id_oferta = "'.$_GET['cauta'].'" ) ';
	$query=$query.' GROUP BY oferte.id_oferta
	ORDER BY oferte.data_adaugarii DESC ';

	return $query;
}

function select_furnizor($filtru) {
	$query="SELECT * FROM furnizori WHERE 1=1 ";
	if($filtru['cauta']) $query= $query." AND (TRIM(LOWER(denumire)) LIKE '%".trim(desfa_link_seach($filtru['cauta']))."%' OR TRIM(LOWER(denumire)) LIKE '%".trim(desfa_link($filtru['cauta']))."%' OR id_furnizor = '".$filtru['cauta']."' ) ";
	$query=$query."GROUP BY id_furnizor
	ORDER BY denumire";
	
	return $query;
}

function select_cupoane($filtru) {
	$query = "SELECT
	cupoane.*,
	cupoane_campanii.*,
	useri_fizice.*,
	tari.denumire AS denumire_tara,
	zone.denumire AS denumire_zona,
	localitati.denumire AS denumire_localitate
	FROM cupoane
	INNER JOIN cupoane_campanii ON cupoane_campanii.id_campanie = cupoane.id_campanie
	LEFT JOIN useri_fizice ON useri_fizice.id_useri_fizice = cupoane.id_useri_fizice
	LEFT JOIN tari ON tari.id_tara = cupoane_campanii.id_tara
	LEFT JOIN zone ON zone.id_zona = cupoane_campanii.id_zona
	LEFT JOIN localitati ON localitati.id_localitate = cupoane_campanii.id_localitate
	WHERE 1=1 ";
	if($filtru['cauta']) $query = $query." AND (TRIM(LOWER(useri_fizice.nume)) LIKE '%".trim(desfa_link_seach($filtru['cauta']))."%' OR TRIM(LOWER(useri_fizice.prenume)) LIKE '%".trim(desfa_link($filtru['cauta']))."%' OR TRIM(LOWER(CONCAT(useri_fizice.prenume,' ',useri_fizice.nume)) LIKE '%".trim(desfa_link($filtru['cauta']))."%' OR TRIM(LOWER(CONCAT(useri_fizice.nume,' ',useri_fizice.prenume)) LIKE '%".trim(desfa_link($filtru['cauta']))."%' OR TRIM(LOWER(cupoane_campanii.denumire_campanie)) LIKE '%".trim(desfa_link($filtru['cauta']))."%' OR id_cupon = '".$filtru['cauta']."' ) ";
	$query=$query."GROUP BY id_cupon
	ORDER BY id_cupon DESC";
	
	return $query;
}

function select_cupoane_campanii($filtru) {
	$query = "SELECT 
	cupoane_campanii.*,
	tari.denumire AS denumire_tara,
	zone.denumire AS denumire_zona,
	localitati.denumire AS denumire_localitate
	FROM cupoane_campanii
	LEFT JOIN tari ON tari.id_tara = cupoane_campanii.id_tara
	LEFT JOIN zone ON zone.id_zona = cupoane_campanii.id_zona
	LEFT JOIN localitati ON localitati.id_localitate = cupoane_campanii.id_localitate
	WHERE 1=1 ";
	if($filtru['cauta']) $query = $query." AND (TRIM(LOWER(cupoane_campanii.denumire_campanie)) LIKE '%".trim(desfa_link($filtru['cauta']))."%' OR id_campanie = '".$filtru['cauta']."' ) ";
	$query=$query."GROUP BY id_campanie
	ORDER BY id_campanie DESC";
	
	return $query;
}

function select_vouchere($filtru) {
	$query="SELECT id_voucher, data, destinatie, hotel FROM vouchere WHERE 1=1 ";
	if($filtru['cauta']) $query=$query.' AND (TRIM(LOWER(destinatie)) LIKE "%'.trim(desfa_link_seach($filtru['cauta'])).'%" OR TRIM(LOWER(hotel)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%" OR id_voucher = "'.$filtru['cauta'].'" ) ';
	$query=$query."GROUP BY id_voucher
	ORDER BY id_voucher DESC";
	
	return $query;
}

function select_cereri($filtru) {
	$query="SELECT
	cerere_rezervare.id_cerere,
	cerere_rezervare.tip,
	cerere_rezervare.stare,
	cerere_rezervare.data_adaugarii,
	cerere_rezervare.data,
	cerere_rezervare.nr_nopti,
	cerere_rezervare.adaugat_admin,
	cerere_rezervare.hoteluri_similare,
	cerere_rezervare.oferta_denumire,
	useri_fizice.id_useri_fizice,
	cerere_rezervare_date_facturare.denumire,
	useri_fizice.nume,
	useri_fizice.prenume
	FROM cerere_rezervare
	INNER JOIN useri_fizice ON useri_fizice.id_useri_fizice = cerere_rezervare.id_useri_fizice
	LEFT JOIN oferte ON oferte.id_oferta = cerere_rezervare.id_oferta
	LEFT JOIN cerere_rezervare_date_facturare ON cerere_rezervare.id_cerere = cerere_rezervare_date_facturare.id_cerere
	WHERE 1=1 ";
	if($filtru['tip_cerere']) $query=$query." AND cerere_rezervare.tip = '".$filtru['tip_cerere']."' ";
	//if($filtru['stare_cerere']) $query=$query." AND cerere_rezervare.stare = '".str_replace('_',' ',$filtru['stare_cerere'])."' "; /*else $query=$query." AND cerere_rezervare.stare<>'anulat' ";*/
	
	if(isDate($filtru['data_start'] ) and isDate($filtru['data_end'] )) 

{ $query=$query."AND cerere_rezervare.data_adaugarii between '".$filtru['data_start']."' and '".$filtru['data_end']."'" ;}
	
	if($filtru['stare_cerere']) {
		$stare_cerere = str_replace('_',' ',substr($filtru['stare_cerere'],0,-1));
		$stare_cerere = "'".str_replace(",","','",$stare_cerere)."'";
		$query=$query." AND cerere_rezervare.stare IN (".$stare_cerere.") ";
	}
	if($filtru['plecare_cerere']=='plec_asc') $query=$query." AND cerere_rezervare.data > NOW() ";
	if($filtru['plecare_cerere']=='sos_asc') $query=$query." AND DATE_ADD(cerere_rezervare.data, INTERVAL cerere_rezervare.nr_nopti DAY) < NOW() ";
	if($filtru['cauta']) $query=$query.' AND (TRIM(LOWER(cerere_rezervare.oferta_denumire)) LIKE "%'.trim($filtru['cauta']).'%" OR TRIM(LOWER(useri_fizice.nume)) LIKE "%'.trim($filtru['cauta']).'%" OR TRIM(LOWER(useri_fizice.prenume)) LIKE "%'.trim($filtru['cauta']).'%" OR TRIM(LOWER(CONCAT(useri_fizice.nume," ",useri_fizice.prenume))) LIKE "%'.trim($filtru['cauta']).'%" OR TRIM(LOWER(CONCAT(useri_fizice.prenume," ",useri_fizice.nume))) LIKE "%'.trim($filtru['cauta']).'%" OR TRIM(LOWER(cerere_rezervare_date_facturare.denumire)) LIKE "%'.trim($filtru['cauta']).'%" OR TRIM(LOWER(cerere_rezervare.id_rezervare_furnizor)) LIKE "%'.trim($filtru['cauta']).'%" OR cerere_rezervare.id_cerere = "'.$filtru['cauta'].'" ) ';
	$query=$query."GROUP BY cerere_rezervare.id_cerere ";
	if($filtru['plecare_cerere']=='plec_asc') $query=$query." ORDER BY cerere_rezervare.data ASC ";
	else if($filtru['plecare_cerere']=='sos_asc') $query=$query." ORDER BY DATE_ADD(cerere_rezervare.data, INTERVAL cerere_rezervare.nr_nopti DAY) DESC ";
	else $query=$query." ORDER BY cerere_rezervare.ordine DESC, cerere_rezervare.id_cerere DESC ";
	

	//echo $query;
	return $query;
}

function select_useri($filtru) {
	$query="SELECT *
	FROM useri_fizice ";
	if($filtru['bought']) $query=$query." INNER JOIN cerere_rezervare ON useri_fizice.id_useri_fizice = cerere_rezervare.id_useri_fizice ";
	$query=$query." WHERE 1=1 ";
	if($filtru['sex']) $query=$query." AND useri_fizice.sex = '".$filtru['sex']."' ";
	if($filtru['bought']) $query=$query." AND cerere_rezervare.tip = 'rezervare' AND (cerere_rezervare.stare = 'finalizata' OR cerere_rezervare.stare = 'trebuie trimis voucher' OR cerere_rezervare.stare = 'platit partial') ";
	if($filtru['born']) $query=$query." AND DATE_FORMAT('".$filtru['born']."', '%m') = DATE_FORMAT(useri_fizice.data_nasterii, '%m') AND DATE_FORMAT('".$filtru['born']."', '%d') = DATE_FORMAT(useri_fizice.data_nasterii, '%d') ";
	if($filtru['cauta']) $query=$query." AND (TRIM(LOWER(useri_fizice.nume)) LIKE '%".trim($filtru['cauta'])."' OR TRIM(LOWER(useri_fizice.prenume)) LIKE '%".trim($filtru['cauta'])."' OR TRIM(LOWER(useri_fizice.email)) LIKE '%".trim($filtru['cauta'])."' OR TRIM(LOWER(CONCAT(useri_fizice.nume,' ',useri_fizice.prenume))) LIKE '%".trim($filtru['cauta'])."' OR TRIM(LOWER(CONCAT(useri_fizice.prenume,' ',useri_fizice.nume))) LIKE '%".trim($filtru['cauta'])."' OR id_useri_fizice = '".$filtru['cauta']."' ) ";
	$query=$query."GROUP BY useri_fizice.email ORDER BY useri_fizice.id_useri_fizice DESC ";
		echo $query;
	return $query;
}

function getUserInfo($id_user) {
	$sel_email = "SELECT email FROM useri_fizice WHERE id_useri_fizice = '".$id_user."' ";
	$que_email = mysql_query($sel_email) or die(mysql_error());
	$row_email = mysql_fetch_array($que_email);
	$info['email'] = $row_email['email'];

	$sel_cereri = "SELECT COUNT(cerere_rezervare.id_cerere) AS nr_cereri FROM cerere_rezervare INNER JOIN useri_fizice ON useri_fizice.id_useri_fizice = cerere_rezervare.id_useri_fizice WHERE useri_fizice.email = '".$info['email']."' ";
	$que_cereri = mysql_query($sel_cereri) or die(mysql_error());
	$row_cereri = mysql_fetch_array($que_cereri);
	$info['nr_cereri'] = $row_cereri['nr_cereri'];
	
	$sel_cereri = "SELECT COUNT(reviews.id) AS nr_reviews FROM reviews INNER JOIN useri_fizice ON useri_fizice.id_useri_fizice = reviews.id_client WHERE useri_fizice.email = '".$info['email']."' ";
	$que_cereri = mysql_query($sel_cereri) or die(mysql_error());
	$row_cereri = mysql_fetch_array($que_cereri);
	$info['nr_reviews'] = $row_cereri['nr_reviews'];
	
	return $info;
}

function select_aeroporturi($filtru) {
	$query='SELECT
	aeroport.id_aeroport,
	aeroport.cod,
	aeroport.denumire,
	oras.denumire as oras
	FROM
	aeroport
	left join localitati as oras on oras.id_localitate = aeroport.id_localitate
	left join zone on oras.id_zona = zone.id_zona
	where 1=1  ';
	if($filtru['tara']) $query=$query.' and zone.id_tara = "'.$filtru['tara'].'" ';
	if($filtru['cauta']) $query= $query.' and (TRIM(LOWER(aeroport.denumire)) like "%'.trim(desfa_link_seach($filtru['cauta'])).'%" or TRIM(LOWER(aeroport.denumire)) like "%'.trim(desfa_link($filtru['cauta'])).'%" or TRIM(LOWER(oras.denumire)) like "%'.trim(desfa_link_seach($filtru['cauta'])).'%" or TRIM(LOWER(oras.denumire)) like "%'.trim(desfa_link($filtru['cauta'])).'%" or aeroport.id_aeroport = "'.$filtru['cauta'].'" ) ';
	$query=$query.' Group By aeroport.id_aeroport
	Order by aeroport.id_aeroport DESC ';
	
	return $query;
}

function select_excursii($filtru) {
	$query='SELECT
	excursii.id_excursie,
	tari.id_tara,
	tari.denumire as denumire_tara,
	excursii.denumire,
	excursii.poza1
	FROM
	excursii
	left join tari on excursii.id_tara = tari.id_tara
	where 1=1  ';
	if($filtru['tara']) $query=$query.' and tari.id_tara = "'.$filtru['tara'].'" ';
	if($filtru['cauta'])   $query= $query.' and (TRIM(LOWER(excursii.denumire)) like "%'.trim(desfa_link_seach($filtru['cauta'])).'%" or TRIM(LOWER(excursii.denumire)) like "%'.trim(desfa_link($filtru['cauta'])).'%" or excursii.id_excursie = "'
	.$filtru['cauta'].'" ) ';
	$query=$query.' Group By excursii.id_excursie
	Order by excursii.id_excursie DESC ';
	
	return $query;
}

function select_rezervari($filtru) {
	$selR="SELECT
	rezervari.*,
	oferte.denumire,
	oferte.id_oferta
	FROM
	rezervari ";
	if($filtru['tip']=='sejur') $selR=$selR."INNER JOIN oferte ON rezervari.id_oferta = oferte.id_oferta ";
	else $selR=$selR."INNER JOIN oferte ON rezervari.id_circuit = oferte.id_oferta ";
	$selR=$selR." WHERE
	rezervari.procesata = 'nu'
	GROUP BY rezervari.id_rezervare
	ORDER BY data_adaugarii DESC ";
	
	return $selR;
}

function select_erori_disponibilitate($filtru) {
	$query = "SELECT
	erori_disponibilitate.*,
	oferte.id_oferta,
	oferte.denumire,
	hoteluri.id_hotel,
	hoteluri.nume,
	hoteluri.tip_unitate,
	localitati.denumire AS denumire_localitate,
	zone.denumire AS denumire_zona,
	tari.denumire AS denumire_tara,
	continente.nume_continent
	FROM erori_disponibilitate
	LEFT JOIN oferte ON erori_disponibilitate.id_oferta = oferte.id_oferta
	LEFT JOIN hoteluri ON erori_disponibilitate.id_hotel = hoteluri.id_hotel
	LEFT JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	LEFT JOIN zone ON localitati.id_zona = zone.id_zona
	LEFT JOIN tari ON zone.id_tara = tari.id_tara
	LEFT JOIN continente ON tari.id_continent = continente.id_continent
	WHERE 1=1 ";
	if($filtru['cauta']) $query = $query.' AND (TRIM(LOWER(oferte.denumire)) LIKE "%'.trim(str_replace("-"," ",$filtru['cauta'])).'%" OR TRIM(LOWER(hoteluri.nume)) LIKE "%'.trim(str_replace("-"," ",$filtru['cauta'])).'%") ';
	if($filtru['hotel']) $query = $query.' AND hoteluri.id_hotel = "'.$filtru['hotel'].'" ';
	if($filtru['oferta']) $query = $query.'  AND oferte.id_oferta = "'.$filtru['oferta'].'" ';
	if($filtru['data']) $query = $query.' AND DATE(erori_disponibilitate.data_adaugare) = "'.date("Y-m-d",strtotime($filtru['data'])).'" ';
	$query = $query." GROUP BY erori_disponibilitate.id
	ORDER BY erori_disponibilitate.id DESC ";
	
	return $query;
}

function make_select_erori($type, $order) {
	$query = "SELECT
	oferte.id_oferta,
	oferte.denumire,
	hoteluri.id_hotel,
	hoteluri.nume,
	hoteluri.tip_unitate
	FROM erori_disponibilitate
	INNER JOIN oferte ON erori_disponibilitate.id_oferta = oferte.id_oferta
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	WHERE 1=1 ";
	if($type) $query = $query." GROUP BY ".$type."
	ORDER BY ".$order." ASC ";
	
	return $query;
}

function select_articole($filtru) {
	$query = 'SELECT
	articles.*,
	tari.id_tara,
	tari.denumire AS den_tara,
	zone.id_zona,
	zone.denumire AS den_zona,
	localitati.id_localitate,
	localitati.denumire AS den_localitate
	FROM articles
	LEFT JOIN tari ON articles.id_tara = tari.id_tara
	LEFT JOIN zone ON articles.id_zona = zone.id_zona
	LEFT JOIN localitati ON articles.id_localitate = localitati.id_localitate
	WHERE 1=1 ';
	if($filtru['cauta']) $query = $query.' AND (TRIM(LOWER(articles.denumire)) LIKE "%'.trim(desfa_link_seach($filtru['cauta'])).'%" OR TRIM(LOWER(articles.denumire)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%" OR articles.id_article = "'.$filtru['cauta'].'" ) ';
	if($filtru['id_zona']) $query = $query.' AND articles.id_zona = "'.$filtru['id_zona'].'" ';
	$query = $query.' GROUP BY articles.id_article
	ORDER BY articles.id_article DESC ';
	
	return $query;
}

function select_tranzactii($filtru) {
	$query = 'SELECT
	facturi.*,
	furnizori.denumire AS den_furnizor,
	cerere_rezervare_date_facturare.denumire AS den_client,
	cerere_rezervare.id_useri_fizice
	FROM facturi
	LEFT JOIN furnizori ON furnizori.id_furnizor = facturi.id_furnizor
	LEFT JOIN cerere_rezervare_date_facturare ON cerere_rezervare_date_facturare.id = facturi.id_client
	LEFT JOIN cerere_rezervare ON cerere_rezervare.id_cerere = cerere_rezervare_date_facturare.id_cerere
	WHERE 1=1 ';
	if($filtru['cauta']) $query = $query.' AND (TRIM(LOWER(facturi.numar_document)) LIKE "%'.trim(desfa_link_seach($filtru['cauta'])).'%" OR TRIM(LOWER(facturi.id_rezervare)) LIKE "%'.trim(desfa_link($filtru['cauta'])).'%" ) ';
	if($filtru['furnizor']) $query = $query.' AND facturi.id_furnizor = "'.$filtru['furnizor'].'" ';
	if($filtru['destinatar']) $query = $query.' AND facturi.tip_operatie = "'.$filtru['destinatar'].'" ';
	if($filtru['ordonare']=='scadenta') $query = $query.' AND facturi.scadenta <> "0000-00-00" AND facturi.scadenta > NOW() ';
	$query = $query.' GROUP BY facturi.id_factura ';
	if($filtru['ordonare'] and $filtru['ordonare_sens']) $query = $query.' ORDER BY facturi.'.$filtru['ordonare'].' '.strtoupper($filtru['ordonare_sens']).' ';
	else $query = $query.' ORDER BY facturi.id_factura DESC ';
	
	return $query;
}

//functii pentru afisare tabel___________________________
function nr_sejururi_active($id_tara, $id_zona, $id_localitate, $id_hotel) {
	$sel="select
	oferte.id_oferta
	from
	oferte
	left join hoteluri on oferte.id_hotel = hoteluri.id_hotel
	left join localitati on hoteluri.locatie_id = localitati.id_localitate
	left join zone on localitati.id_zona = zone.id_zona
	left join tari on zone.id_tara = tari.id_tara
	where
	oferte.valabila = 'da' ";
	if($id_tara)  $sel=$sel." and tari.id_tara = '".$id_tara."' ";
	if($id_zona)  $sel=$sel." and zone.id_zona = '".$id_zona."' ";
	if($id_localitate)  $sel=$sel." and localitati.id_localitate = '".$id_localitate."' ";
	if($id_hotel)  $sel=$sel." and hoteluri.id_hotel = '".$id_hotel."' ";
	$sel=$sel." Group by oferte.id_oferta";
	$que=mysql_query($sel) or die(mysql_error());
	$nr=mysql_num_rows($que);
	@mysql_free_result($que);
	
	return $nr;
}

function nr_sejururi_neactive($id_tara, $id_zona, $id_localitate, $id_hotel) {
	$sel="select
	oferte.id_oferta
	from
	oferte
	left join hoteluri on oferte.id_hotel = hoteluri.id_hotel
	left join localitati on hoteluri.locatie_id = localitati.id_localitate
	left join zone on localitati.id_zona = zone.id_zona
	left join tari on zone.id_tara = tari.id_tara
	where
	(oferte.valabila = 'nu' || oferte.valabila is null ) ";
	if($id_tara)  $sel=$sel." and tari.id_tara = '".$id_tara."' ";
	if($id_zona)  $sel=$sel." and zone.id_zona = '".$id_zona."' ";
	if($id_localitate)  $sel=$sel." and localitati.id_localitate = '".$id_localitate."' ";
	if($id_hotel)  $sel=$sel." and hoteluri.id_hotel = '".$id_hotel."' ";
	$sel=$sel." Group by oferte.id_oferta";
	$que=mysql_query($sel) or die(mysql_error());
	$nr=mysql_num_rows($que);
	@mysql_free_result($que);
	return $nr; }
	function nr_zone($id_tara) {
	$sel="select count(zone.id_zona) as nr from zone where zone.id_tara = '$id_tara'";
	$que=mysql_query($sel) or die(mysql_error());
	$row=mysql_fetch_array($que);
	mysql_free_result($que);
	return $row['nr']; }
	function nr_localitati($id_tara, $id_zona) {
	$sel="select
	localitati.id_localitate
	from
	localitati
	left join zone on localitati.id_zona = zone.id_zona
	left join tari on zone.id_tara = tari.id_tara
	where
	1=1";
	if($id_tara)  $sel=$sel." and tari.id_tara = '".$id_tara."' ";
	if($id_zona)  $sel=$sel." and zone.id_zona = '".$id_zona."' ";
	$sel=$sel." Group by localitati.id_localitate ";
	$que=mysql_query($sel) or die(mysql_error());
	$nr=mysql_num_rows($que);
	@mysql_free_result($que);
	
	return $nr;
}

function nr_hoteluri($id_tara, $id_zona, $id_localitate) {
	$sel="select
	hoteluri.id_hotel
	from
	hoteluri
	left join localitati on hoteluri.locatie_id = localitati.id_localitate
	left join zone on localitati.id_zona = zone.id_zona
	left join tari on zone.id_tara = tari.id_tara
	where
	1=1 ";
	if($id_tara)  $sel=$sel." and tari.id_tara = '".$id_tara."' ";
	if($id_zona)  $sel=$sel." and zone.id_zona = '".$id_zona."' ";
	if($id_localitate)  $sel=$sel." and localitati.id_localitate = '".$id_localitate."' ";
	$sel=$sel." Group by  hoteluri.id_hotel";
	$que=mysql_query($sel) or die(mysql_error());
	$nr=mysql_num_rows($que);
	@mysql_free_result($que);
	return $nr; }
	function nr_croaziere($id_vapor) {
	$sel="select
	croaziere.id_croaziera
	from
	croaziere
	where
	croaziere.id_vapor = '$id_vapor'
	Group by  croaziere.id_croaziera ";
	$que=mysql_query($sel) or die(mysql_error());
	$nr=mysql_num_rows($que);
	@mysql_free_result($que);
	return $nr;
}

//sterge___________________________
function sterge_tara($id_tara) {
	$select_poza="select * from tari where id_tara= '$id_tara' ";
	$query_poza=mysql_query($select_poza) or die(mysql_error());
	$row_poza=mysql_fetch_assoc($query_poza);
	
	$nume_tara=$row_poza['denumire'];
	@mysql_free_result($query_poza);
	
	for($i=1;$i<=3;$i++){
		$poza_de_sters=$row_poza['poza'.$i];
		if($poza_de_sters!=""){
			@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_tara/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_tara/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_tara/".$poza_de_sters);
		}
	}
	
	if($row_poza['harta']) {
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_harta_tara/".$row_poza['harta']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_harta_tara/".$row_poza['harta']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_harta_tara/".$row_poza['harta']);
	}
	
	if($row_poza['steag']) {
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_steag_tara/".$row_poza['steag']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_steag_tara/".$row_poza['steag']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_steag_tara/".$row_poza['steag']);
	}
	
	$del_of="delete from tari where tari.id_tara = '$id_tara' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	return $nume_tar;
}

function sterge_zona($id_zona) {
	$select_poza="select * from zone where id_zona= '$id_zona' ";
	$query_poza=mysql_query($select_poza) or die(mysql_error());
	$row_poza=mysql_fetch_assoc($query_poza);
	
	$nume_tara=$row_poza['denumire'];
	@mysql_free_result($query_poza);
	
	for($i=1;$i<=3;$i++){
		$poza_de_sters=$row_poza['poza'.$i];
		if($poza_de_sters!=""){
			@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_zona/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_zona/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_zona/".$poza_de_sters);
		}
	}
	
	if($row_poza['harta']) {
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_harta_zona/".$row_poza['harta']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_harta_zona/".$row_poza['harta']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_harta_zona/".$row_poza['harta']);
	}
	
	$del_of="delete from zone where id_zona = '$id_zona' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	return $nume_tar;
}

function sterge_localitate($id_localitate) {
	$select_poza="select * from localitati where id_localitate= '$id_localitate' ";
	$query_poza=mysql_query($select_poza) or die(mysql_error());
	$row_poza=mysql_fetch_assoc($query_poza);
	
	$nume_tara=$row_poza['denumire'];
	@mysql_free_result($query_poza);
	
	for($i=1;$i<=3;$i++){
		$poza_de_sters=$row_poza['poza'.$i];
		if($poza_de_sters!=""){
			@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_localitate/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_localitate/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_localitate/".$poza_de_sters);
		}
	}
	
	if($row_poza['harta']) {
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_harta_localitate/".$row_poza['harta']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_harta_localitate/".$row_poza['harta']);
		@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_harta_localitate/".$row_poza['harta']);
	}
	
	$del_of="delete from localitati where id_localitate = '$id_localitate' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	return $nume_tar;
}

function sterge_hotel($id_hotel) {
	$select_poza="select * from hoteluri where id_hotel= '$id_hotel' ";
	$query_poza=mysql_query($select_poza) or die(mysql_error());
	$row_poza=mysql_fetch_assoc($query_poza);
	
	$nume_tara=$row_poza['nume'];
	@mysql_free_result($query_poza);
	
	for($i=1;$i<=8;$i++){
		$poza_de_sters=$row_poza['poza'.$i];
		if($poza_de_sters!=""){
			@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_hotel/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_hotel/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_hotel/".$poza_de_sters);
		}
	}
	
	$del_of="delete from hoteluri where id_hotel = '$id_hotel' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	$del_of="delete from camere_hotel where id_hotel = '$id_hotel' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	return $nume_tar;
}

function sterge_circuit($id_hotel) {
	$selOF="select id_oferta from oferte where id_hotel = '".$id_hotel."' ";
	$queOF=mysql_query($selOF) or die(mysql_error());
	$rowOF=mysql_fetch_array($queOF);
	
	$id_oferta=$rowOF['id_oferta'];
	@mysql_free_result($queOF);
	
	$select_poza="select * from hoteluri where id_hotel= '$id_hotel' ";
	$query_poza=mysql_query($select_poza) or die(mysql_error());
	$row_poza=mysql_fetch_assoc($query_poza);
	
	$nume_tara=$row_poza['nume'];
	@mysql_free_result($query_poza);
	
	for($i=1;$i<=8;$i++){
		$poza_de_sters=$row_poza['poza'.$i];
		if($poza_de_sters!=""){
			@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_hotel/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_hotel/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_hotel/".$poza_de_sters);
		}
	}
	
	$del_of="delete from hoteluri where id_hotel = '$id_hotel' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);

	$del_of="delete from camere_hotel where id_hotel = '$id_hotel' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);

	$del_of="delete from traseu_circuit where id_hotel_parinte = '$id_hotel' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	if($id_oferta) {
		$del="delete from oferte where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from data_pret_oferta where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from early_booking where id_oferta = '".$id_oferta."' and tip = 'sejur' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from oferta_sejur_tip where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from nr_nopti_minim where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from oferte_servicii where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from reduceri_speciale where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from sold_out where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from zile_anulare where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from zile_plata where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from oferte_excursii where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from oferte_transport_autocar where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);

		$del="delete from oferte_transport_avion where id_oferta = '".$id_oferta."' ";
		$queD=mysql_query($del) or die(mysql_error());
		@mysql_free_result($queD);
	}
	
	return $nume_tar;
}

function sterge_excursie($id_excursie) {
	$select_poza="select * from excursii where id_excursie = '$id_excursie' ";
	$query_poza=mysql_query($select_poza) or die(mysql_error());
	$row_poza=mysql_fetch_assoc($query_poza);

	$nume_tara=$row_poza['denumire'];
	@mysql_free_result($query_poza);

	for($i=1;$i<=3;$i++){
		$poza_de_sters=$row_poza['poza'.$i];
		if($poza_de_sters!=""){
			@unlink($_SERVER["DOCUMENT_ROOT"]."/thumb_excursii/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_excursii/".$poza_de_sters);
			@unlink($_SERVER["DOCUMENT_ROOT"]."/img_prima_excursii/".$poza_de_sters);
		}
	}
	
	$del_of="delete from excursii where id_excursie = '$id_excursie' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	return $nume_tar;
}

function sterge_furnizor($id_furnizor) {
	$sel_furnizor="SELECT * FROM furnizori WHERE id_furnizor = '$id_furnizor' ";
	$que_furnizor=mysql_query($sel_furnizor) or die(mysql_error());
	$row_furnizor=mysql_fetch_assoc($que_furnizor);

	$nume_furnizor=$row_furnizor['denumire'];
	@mysql_free_result($que_furnizor);

	$del_of="DELETE FROM furnizori WHERE id_furnizor = '$id_furnizor' ";
	$que_of=mysql_query($del_of) or die(mysql_error());
	@mysql_free_result($que_of);
	
	return $nume_furnizor;
}

function sterge_cupon($id_cupon) {
	$sel_cupon = "SELECT * FROM cupoane WHERE id_cupon = '$id_cupon' ";
	$que_cupon = mysql_query($sel_cupon) or die(mysql_error());
	$row_cupon = mysql_fetch_assoc($que_cupon);

	$id_coupon = $row_cupon['id_cupon'];
	@mysql_free_result($que_cupon);

	$del_coupon = "DELETE FROM cupoane WHERE id_cupon = '$id_coupon' ";
	$que_coupon = mysql_query($del_coupon) or die(mysql_error());
	@mysql_free_result($que_coupon);
	
	return $id_coupon;
}

function sterge_articol($id_articol) {
	$sel_articol = "SELECT * FROM articles WHERE id_article = '".$id_articol."' ";
	$que_articol = mysql_query($sel_articol) or die(mysql_error());
	$row_articol = mysql_fetch_assoc($que_articol);

	$id_article = $row_articol['id_article'];
	@mysql_free_result($que_articol);

	$del_article = "DELETE FROM articles WHERE id_article = '".$id_article."' ";
	$que_article = mysql_query($del_article) or die(mysql_error());
	@mysql_free_result($que_article);
	
	return $id_article;
}

function sterge_tranzactie($id_tranzactie) {
	$sel_tranzactie = "SELECT * FROM facturi WHERE id_factura = '".$id_tranzactie."' ";
	$que_tranzactie = mysql_query($sel_tranzactie) or die(mysql_error());
	$row_tranzactie = mysql_fetch_assoc($que_tranzactie);

	$id_factura = $row_tranzactie['id_factura'];
	@mysql_free_result($que_tranzactie);

	$del_factura = "DELETE FROM facturi WHERE id_factura = '".$id_factura."' ";
	$que_factura = mysql_query($del_factura) or die(mysql_error());
	@mysql_free_result($que_factura);
	
	return $id_factura;
}

function cameraToAdulti($value) {
	$replacement = array(
		"SGL" => "1",
		"DBL" => "2",
		"TRPL" => "3",
		"4 PAX" => "4",
		"5 PAX" => "5",
		"6 PAX" => "6"
	);
	while(list($cam, $adl) = each($replacement)) {
		$value = str_replace($cam, $adl, $value);
	}
	
	return $value;
}

if($_GET['id_hotel']) {
	$sel_hot = "SELECT nume FROM hoteluri WHERE id_hotel = '".$_GET['id_hotel']."' ";
	$que_hot=mysql_query($sel_hot) or die(mysql_error());
	$row_hot=mysql_fetch_array($que_hot);
	$nume_hotel = $row_hot['nume'];
}

/*function objectToArray( $object ) {
	if(!is_object($object) && !is_array($object)) {
		return $object;
	}
	if(is_object($object)) {
		$object = get_object_vars($object);
	}
	return array_map('objectToArray', $object);
}*/
function isDate($value) 
{
    if (!$value) {
        return false;
    }

    try {
        new \DateTime($value);
        return true;
    } catch (\Exception $e) {
        return false;
    }
}

?>