<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<!-- iPhone, iPad and Android specific settings -->	
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
		
		<title>Adminica | The Professional Admin Theme</title>
		
		<!-- Create an icon and splash screen for iPhone and iPad -->
		<link rel="apple-touch-icon" href="images/iOS_icon.png">
		<link rel="apple-touch-startup-image" href="images/iOS_startup.png"> 

		<link rel="stylesheet" type="text/css" href="css/all.css" media="screen">
		
		<!-- These stylesheets are used  -->
		<link rel="stylesheet" type="text/css" href="css/theme/switcher5.php?default=switcher.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/theme/switcher.css" media="screen">
		<link rel="stylesheet" type="text/css" href="css/theme/switcher1.php?default=switcher.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/theme/switcher2.php?default=switcher.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/theme/switcher3.php?default=switcher.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/theme/switcher4.php?default=switcher.css" media="screen" />

		<!-- OCAZIITURISTICE -->
		<link rel="stylesheet" type="text/css" href="css/theme/theme_blue.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/theme/bg_noise.css" media="screen" />

		<!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
		<!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
			
		<!-- Load JQuery -->		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

		<!-- Load JQuery UI -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
		
		<!-- Load Interface Plugins -->
		<script type="text/javascript" src="js/uniform/jquery.uniform.js"></script>

		<script type="text/javascript" src="js/iPhone/jquery.iphoneui.js"></script>
		<script type="text/javascript" src="js/uitotop/js/jquery.ui.totop.js"></script>

		<!-- This file configures the various jQuery plugins for Adminica. Contains links to help pages for each plugin. -->
		<script type="text/javascript" src="js/adminica/adminica_ui.js"></script>

		
	</head>
	<body>