<?php
class ws_geography {}
try {
    $client = new SoapClient($url, array('trace' => 1, 'login' => $user, 'password' => $pass, 'classmap' => array('Geography'=>'ws_geography')));
} catch (SoapFault $exception) {
    throw $exception;
}
try {
    $response = $client->GetGeography();
} catch (SoapFault $exception) {
    $info = $client->__getLastRequest();
    var_dump($info);
    $info = $client->__getLastResponse();
    var_dump($info);
    throw $exception;
}

$localizare = objectToArray($response);

function return_loc($Name, $Id, $id_furnizor) {
	$sel_loc = "SELECT * FROM localitati_corespondent WHERE nume_furnizor = '".$Name."' AND code_furnizor = '".$Id."' AND id_furnizor = '".$id_furnizor."' ";
	$que_loc = mysql_query($sel_loc);
	$row_loc = mysql_fetch_array($que_loc);
	if(mysql_num_rows($que_loc)<1) {
		$ins_city = "INSERT INTO localitati_corespondent (nume_furnizor, code_furnizor, id_furnizor) VALUES ('".$Name."', '".$Id."', '".$id_furnizor."') ";
		$que_city = mysql_query($ins_city) or die(mysql_error());
		@mysql_free_result($que_city);
		
		$sel_oras = "SELECT id FROM localitati_corespondent WHERE nume_furnizor = '".$Name."' AND code_furnizor = '".$Id."' AND id_furnizor = '".$id_furnizor."' ";
		$que_oras = mysql_query($sel_oras);
		$row_oras = mysql_fetch_array($que_oras);
		@mysql_free_result($que_oras);
		
		$hotel_city[$Id] = $row_oras['id'];
	} else {
		$hotel_city[$Id] = $row_loc['id'];
	}
	@mysql_free_result($que_loc);
	
	return $hotel_city[$Id];
}

foreach($localizare['Children'] as $v_continent) {
	foreach($v_continent['Children'] as $v_tara) {
		foreach($v_tara['Children'] as $v_zona) {
			if(sizeof($v_zona['Children'])>0) {
				foreach($v_zona['Children'] as $v_loc) {
					if(sizeof($v_loc['Children'])>0) {
						foreach($v_loc['Children'] as $v_loc2) {
							$hotel_city[$v_loc2['Id']] = return_loc($v_loc2['Name'], $v_loc2['Id'], $id_furnizor);
						}
					}
					$hotel_city[$v_loc['Id']] = return_loc($v_loc['Name'], $v_loc['Id'], $id_furnizor);
				}
			}
			$hotel_city[$v_zona['Id']] = return_loc($v_zona['Name'], $v_zona['Id'], $id_furnizor);
		}
	}
}
//echo '<pre>';print_r($hotel_city);echo '</pre>';



class ws_hotels {}
try {
    $client = new SoapClient($url, array('trace' => 1, 'login' => $user, 'password' => $pass, 'classmap' => array('Hotels'=>'ws_hotels')));
} catch (SoapFault $exception) {
    throw $exception;
}
try {
    $response = $client->GetHotels();
} catch (SoapFault $exception) {
    $info = $client->__getLastRequest();
    var_dump($info);
    $info = $client->__getLastResponse();
    var_dump($info);
    throw $exception;
}

$hotels = objectToArray($response);

foreach($hotels as $k_hotel => $v_hotel) {
	$id_hotel[$k_hotel] = $v_hotel['Id'];
	$den_hotel[$k_hotel] = str_replace("'","`",$v_hotel['Name']);
	$stele[$k_hotel] = $v_hotel['Class'];
	$id_localitate[$k_hotel] = $hotel_city[$v_hotel['Location']];
	
	$sel_hotels = "SELECT * FROM import_hoteluri WHERE nume_hotel = '".$den_hotel[$k_hotel]."' AND stele_hotel = '".$stele[$k_hotel]."' AND city = '".$id_localitate[$k_hotel]."' ";
	$que_hotels = mysql_query($sel_hotels);
	$row_hotels = mysql_fetch_array($que_hotels);
	if(mysql_num_rows($que_hotels)<1) {
		$ins_sql[] = '("'.$id_hotel[$k_hotel].'", "'.$den_hotel[$k_hotel].'", "'.$stele[$k_hotel].'", "'.$id_localitate[$k_hotel].'", "'.$id_furnizor.'")';
	}
	//echo '<li>("'.$den_hotel[$k_hotel].'", "'.$stele[$k_hotel].'", "'.$id_localitate[$k_hotel].'", "'.$id_furnizor.'")</li>';
	@mysql_free_result($que_hotels);

	foreach($v_hotel['RoomCategories'] as $k_rooms => $v_rooms) {
		$id_room[$k_hotel][$k_rooms] = $v_rooms['Id'];
		$camera[$k_rooms] = explode("/", $v_rooms['Name']);
		$den_cam[$k_hotel][$k_rooms] = trim($camera[$k_rooms][0]);

	echo "<br />"	.$sel_cams = "SELECT * FROM import_tip_camera WHERE id_hotel = '".$id_hotel[$k_hotel]."' AND id_camera = '".$id_room[$k_hotel][$k_rooms]."' AND denumire = '".$den_cam[$k_hotel][$k_rooms]."' AND id_furnizor = '".$id_furnizor."' ";
		$que_cams = mysql_query($sel_cams);
		$row_cams = mysql_fetch_array($que_cams);
		if(mysql_num_rows($que_cams)<1) {
			$ins_sql_cams[$k_hotel][] = '("'.$id_hotel[$k_hotel].'", "'.$id_room[$k_hotel][$k_rooms].'", "'.$den_cam[$k_hotel][$k_rooms].'", "'.$id_furnizor.'")';
		}
		@mysql_free_result($que_cams);
	}
	if(sizeof($ins_sql_cams[$k_hotel])>0) {
	$ins_sql_cams[$k_hotel]=str_replace("\"Yali\"","Yali ",$ins_sql_cams[$k_hotel]) ;
	//echo"<br />". $ins_sql_cams[$k_hotel];
	echo"<br />".	$ins_camere = "INSERT INTO import_tip_camera (id_hotel, id_camera, denumire, id_furnizor) VALUES ".implode(',', $ins_sql_cams[$k_hotel])." ";
		$que_camere = mysql_query($ins_camere) or die(mysql_error());
		@mysql_free_result($que_camere);
	}
}

if(sizeof($ins_sql)>0) {
	$ins_hotel = "INSERT INTO import_hoteluri (id_hotel, nume_hotel, stele_hotel, city, id_furnizor) VALUES ".implode(',', $ins_sql)." ";
	$que_hotel = mysql_query($ins_hotel) or die(mysql_error());
	@mysql_free_result($que_hotel);
}

/*echo '<script>alert("Hotelurile au fost updatate cu succes."); document.location.href="/adm/import_cocktails.php";</script>';*/
echo '<h2>Hotelurile au fost updatate cu succes.</h2>';

?>