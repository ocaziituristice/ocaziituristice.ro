<?php session_start(); include_once('restrictionare.php'); ?>
<?php
if($_REQUEST['scadenta']) $scadenta = $_REQUEST['scadenta']; else $scadenta = 5;
$scadenta_end = date("Y-m-d", strtotime('+'.$scadenta.' days'));
$scadenta_start = date("Y-m-d", strtotime('-'.$scadenta.' days'));
//$scadenta_start = date("Y-m-d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
  
<h1>Plati furnizori</h1>

<div class="filtrare">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td align="left" valign="middle">
      <form name="filtru" method="post" action="plati_furnizori.php">
        Facturi scadente in <input type="number" name="scadenta" class="mic" value="<?php echo $scadenta; ?>" /> zile
        <input type="hidden" name="filtrare" value="da" />
        &nbsp;&nbsp;
        <button type="submit">Go</button>
      </form>
      <form name="filtru" method="post" action="plati_furnizori.php">
        <button type="submit">Reset</button>
      </form>
      </td>
    </tr>
  </table>
</div>   

<br/>

<table cellpadding="2" cellspacing="2" border="1" width="98%" align="center" class="tabel">
  <tr>
    <th align="left" valign="top">Furnizor</th>
    <th align="right" valign="top" width="100">Total vandut</th>
    <th align="right" valign="top" width="100">Total de plata</th>
  </tr>
<?php
$sel_furnizori = "SELECT * FROM furnizori ORDER BY denumire ";
$que_furnizori = mysql_query($sel_furnizori) or die(mysql_error());
while($row_furnizori = mysql_fetch_array($que_furnizori)) {
	$total_furnizor = 0;
	$total_vandut = 0;
	$sel_financiar_furnizor = "SELECT *
	FROM facturi
	INNER JOIN cerere_rezervare ON facturi.id_rezervare = cerere_rezervare.id_cerere
	WHERE facturi.id_furnizor = '".$row_furnizori['id_furnizor']."'
	AND tip_operatie = 'furnizor' AND cerere_rezervare.stare<>'anulat' and year(data_adaugarii)=2017";
	if($_REQUEST['scadenta']) $sel_financiar_furnizor .= " AND scadenta <= '".$scadenta_end."' AND scadenta >= '".$scadenta_start."' ";
	$que_financiar_furnizor = mysql_query($sel_financiar_furnizor) or die(mysql_error());
	while($row_financiar_furnizor = mysql_fetch_array($que_financiar_furnizor)) {
		if($row_financiar_furnizor['tip_tranzactie']=='intrare') {
			//$total_furnizor -= $row_financiar_furnizor['valoare'];
			$total_vandut += $row_financiar_furnizor['valoare'];
		}
		//if($row_financiar_furnizor['tip_tranzactie']=='iesire') $total_furnizor += $row_financiar_furnizor['valoare'];
		$valuta_furnizor = $row_financiar_furnizor['valuta'];
	}
	//if($total_furnizor<0) $total_furnizor = -$total_furnizor;
	//$total_furnizor = round($total_furnizor);
	$total_vandut = round($total_vandut);
	if(mysql_num_rows($que_financiar_furnizor)>0) {
?>
  <tr>
	<td align="left" valign="top">
	  <a href="editare_furnizor.php?pas=2&furnizor=<?php echo $row_furnizori['id_furnizor']; ?>" target="_blank" class="titluArticol"><?php echo $row_furnizori['denumire']; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a>
      &nbsp;&nbsp; | &nbsp;&nbsp;
      <span class="clickFurnizor<?php echo $row_furnizori['id_furnizor']; ?> black underline expandable" style="cursor:pointer;">Detalii plati</span>
<div class="furnizor<?php echo $row_furnizori['id_furnizor']; ?>" style="display:<?php if($_REQUEST['scadenta']) echo 'block'; else echo 'none'; ?>; padding:5px;">
<table cellspacing="0" cellpadding="5" border="1" width="100%" class="tabel">
  <tr>
    <th></th>
    <th>ID rezervare</th>
    <th>ID client</th>
    <th>Document</th>
    <th>Data</th>
    <th>Valoare</th>
    <th>Scadenta</th>
    <th>Observatii</th>
    <th>Rezumat</th>
  </tr>
<?php
$sel_financiar = "SELECT facturi.*,
cerere_rezervare.id_useri_fizice
FROM facturi
INNER JOIN cerere_rezervare ON facturi.id_rezervare = cerere_rezervare.id_cerere
WHERE facturi.id_furnizor = '".$row_furnizori['id_furnizor']."'
AND facturi.tip_operatie = 'furnizor' and cerere_rezervare.stare<>'anulat' and year(data_adaugarii)>=2016";
if($_REQUEST['scadenta']) $sel_financiar .= " AND scadenta <= '".$scadenta_end."' ";
$sel_financiar .= " ORDER BY facturi.id_rezervare DESC ";
$que_financiar = mysql_query($sel_financiar) or die(mysql_error());
$idfur = $row_furnizori['id_furnizor'];
$i=0;
while($row_financiar = mysql_fetch_array($que_financiar)) {
	$i++;
	if($row_financiar['tip_tranzactie']=='intrare') $semn = '+';
	else if($row_financiar['tip_tranzactie']=='iesire') $semn = '-';
	
	$col[$idfur]['id_rez'][$i] = $row_financiar['id_rezervare'];
	$col[$idfur]['id_usr'][$row_financiar['id_rezervare']] = $row_financiar['id_useri_fizice'];
	$col[$idfur]['tip_document'][$row_financiar['id_rezervare']][] = $row_financiar['tip_document'];
	$col[$idfur]['numar_document'][$row_financiar['id_rezervare']][] = $row_financiar['numar_document'];
	$col[$idfur]['data_document'][$row_financiar['id_rezervare']][] = $row_financiar['data_document'];
	$col[$idfur]['semn'][$row_financiar['id_rezervare']][] = $semn;
	$col[$idfur]['valoare'][$row_financiar['id_rezervare']][] = $row_financiar['valoare'];
	$col[$idfur]['valuta'][$row_financiar['id_rezervare']][] = $row_financiar['valuta'];
	$col[$idfur]['scadenta'][$row_financiar['id_rezervare']][] = $row_financiar['scadenta'];
	$col[$idfur]['observatii'][$row_financiar['id_rezervare']][] = $row_financiar['observatii'];
}
	$col[$idfur]['id_rez'] = array_unique($col[$idfur]['id_rez']);
	//echo '<pre>';print_r($col[$idfur]);echo '</pre>';

$j=0;
foreach($col[$idfur]['id_rez'] as $k_rez => $v_rez) {
	$j++;
	$rowspan[$v_rez] = count($col[$idfur]['valoare'][$v_rez]);
	
	$rest_plata[$v_rez] = 0;
	foreach($col[$idfur]['valoare'][$v_rez] as $k_val => $v_val) $rest_plata[$v_rez] += ($col[$idfur]['semn'][$v_rez][$k_val]."1")*$v_val;
?>
  <tr <?php if($j%2==0) echo 'style="background:#F3F3F3;"'; ?>>
    <td align="center" rowspan="<?php echo $rowspan[$v_rez]; ?>"><?php echo $j; ?></td>
    <td align="center" rowspan="<?php echo $rowspan[$v_rez]; ?>"><a href="cereri.php?id_cerere=<?php echo $v_rez; ?>" target="_blank"><?php echo $v_rez; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a></td>
    <td align="center" rowspan="<?php echo $rowspan[$v_rez]; ?>"><a href="useri.php?id_user=<?php echo $col[$idfur]['id_usr'][$v_rez]; ?>" target="_blank"><?php echo $col[$idfur]['id_usr'][$v_rez]; ?> <img src="/images/link_extern.png" style="vertical-align:middle;"></a></td>
<?php
	$ii=0;
	$scadent = 0;
	foreach($col[$idfur]['valoare'][$v_rez] as $k_val => $v_val) {
		$ii++;
		if($col[$idfur]['scadenta'][$v_rez][$k_val] <= $scadenta_end and $col[$idfur]['semn'][$v_rez][$k_val]=='+' ) $scadent += 1;
?>
    <td align="center"><?php echo $col[$idfur]['tip_document'][$v_rez][$k_val].' '.$col[$idfur]['numar_document'][$v_rez][$k_val]; ?></td>
    <td align="center"><?php echo $col[$idfur]['data_document'][$v_rez][$k_val]; ?></td>
    <td align="center"><?php echo '<span class="bold ';
	if($col[$idfur]['semn'][$v_rez][$k_val]=='-') echo 'green'; else echo 'red';
	echo '">'.$col[$idfur]['semn'][$v_rez][$k_val].' '.$col[$idfur]['valoare'][$v_rez][$k_val].' '.$col[$idfur]['valuta'][$v_rez][$k_val].'</span>'; ?></td>
    <td align="center"><span class="<?php if($col[$idfur]['scadenta'][$v_rez][$k_val] <= $scadenta_end and $col[$idfur]['scadenta'][$v_rez][$k_val] >= $scadenta_start) echo 'bold bigger-12em red'; ?>"><?php echo $col[$idfur]['scadenta'][$v_rez][$k_val]; ?></span></td>
    <td align="center"><?php echo $col[$idfur]['observatii'][$v_rez][$k_val]; ?></td>
    <?php if($ii==1) { ?><td align="left" rowspan="<?php echo $rowspan[$v_rez]; ?>">
    	<?php if($rest_plata[$v_rez]>0) {
			$total_furnizor += $rest_plata[$v_rez];
			echo '<span class="bold black">Rest: &nbsp; <span class="red">'.$rest_plata[$v_rez].' '.$col[$idfur]['valuta'][$v_rez][$k_val].'</span></span>';
			if($scadent>0) echo '<br /><span class="bold bigger-13em red">SCADENT !</span>';
		} else {
			echo '<span class="bold green">Achitat</span>';
		} ?>
	</td><?php } ?>
  </tr>
<?php
		if($ii<$rowspan[$v_rez]) {
			echo '<tr';
			if($j%2==0) echo ' style="background:#F3F3F3;"';
			echo '>';
		}
	}
} ?>
</table>
</div>
	</td>
	<td align="right" style="vertical-align:top;"><span class="black bold"><span class="bigger-11em green"><?php echo $total_vandut; ?> </span> <?php echo moneda($valuta_furnizor); ?></span></td>
	<td align="right" style="vertical-align:top;"><span class="black bold"><span class="bigger-12em red"><?php echo round($total_furnizor); ?> </span> <?php echo moneda($valuta_furnizor); ?></span></td>
  </tr>
<script type="text/javascript">
$(".clickFurnizor<?php echo $row_furnizori['id_furnizor']; ?>").click(function() {
	$(".furnizor<?php echo $row_furnizori['id_furnizor']; ?>").toggle("fast");
});
</script>
<?php }
} ?>
</table>

  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>