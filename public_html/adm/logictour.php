<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie <?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'includes/functii_adm.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
<?php //centru_______________________________________ 
if(!$pas=$_REQUEST['pas']) $pas=1;
switch($pas)
{
	case "1": include_once "logictour/pas1.php"; break;
	case "2": include_once "logictour/pas2.php"; break;
	case "3": include_once "logictour/pas3.php"; break;
	case "4": include_once "logictour/pas4.php"; break;
	case "5": include_once "logictour/pas5.php"; break;
	case "6": include_once "logictour/pas6.php"; break;
	case "grad_ocupare": include_once "logictour/grad_ocupare.php"; break;
	case "adauga_pivot": include_once "logictour/copiaza_pivot.php"; break;
}
?>
  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include('footer.php'); ?></div>

</body>
</html>