<?php unset($add_t); ?>
<h1>Vizualizeaza tara <a href="<?php echo $sitepath_adm.'editare_tara.php?pas=2&tara='.$id_tara; ?>" class="buttons">Editeaza tara</a> | <a href="<?php echo $sitepath_adm.'editare_tara.php'; ?>" class="buttons">Toate tarile</a> |
  <form name="adauga_zona" action="adauga_zona.php" method="post">
    <input type="hidden" name="param[tara]" value="<?php echo $param['denumire']; ?>" />
    <button class="buttons" type="submit">Adauga zona in <?php echo $id_tara; ?></button>
  </form>
  |
  <form name="adauga_hotel" action="adauga_hotel.php" method="post">
    <input type="hidden" name="param[tara]" value="<?php echo $id_tara; ?>" />
    <button type="submit" class="buttons">Adauga Hotel</button>
  </form>
  <a href="<?php echo $sitepath_adm.'adauga_tara.php'; ?>" class="buttons">Adauga tara</a> </h1>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td valign="top" align="right" width="160">Denumire tara:</td>
    <td align="left" valign="top" class="help_text"><?php echo $param['denumire']; ?></td>
  </tr>
  <tr>
    <td align="right" valign="top">Descriere scurta: <br /></td>
    <td align="left" valign="top"><?php echo $param['descriere_scurta']; ?></td>
  </tr>
  <tr>
  <tr>
    <td valign="top" align="right">Descriere:</td>
    <td align="left" valign="top" class="help_text"><?php echo $param['descriere']; ?></td>
  </tr>
  <tr>
    <td align="right" valign="top">Steag:</td>
    <td align="left" valign="top"><?php if($param['steag']) { ?>
      <img src="../thumb_steag_tara/<?php echo $param['steag']; ?>" />
      <?php } ?></td>
  </tr>
  <tr>
    <td align="right" valign="top">Harta:</td>
    <td align="left" valign="top"><?php if($param['harta']) { ?>
      <img src="../thumb_harta_tara/<?php echo $param['harta']; ?>" />
      <?php } ?></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><h2>Poze:</h2></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="2" cellpadding="4">
        <tr>
          <td align="left" valign="middle"><?php for($i=1;$i<=3;$i++) {
	 if($param['poza'.$i]) { ?>
            <a href="<?php echo $sitepath; ?>img_prima_tara/<?php echo $param['poza'.$i]; ?>" rel="gallery"><img src="<?php echo $sitepath; ?>thumb_tara/<?php echo $param['poza'.$i]; ?>" /></a>
            <?php } }?></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><h2>Search Engine Optimization</h2></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellpadding="2" cellspacing="2">
        <tr>
          <td width="160" align="right" valign="top">Titlu SEO:</td>
          <td class="help_text"><?php echo $param['titlu_seo']; ?></td>
        </tr>
        <tr>
          <td valign="top" align="right">Descriere SEO:</td>
          <td class="help_text"><?php echo $param['descriere_seo']; ?></td>
        </tr>
        <tr>
          <td valign="top" align="right">Cuvinte cheie:</td>
          <td class="help_text"><?php echo $param['cuvinte_seo']; ?></td>
        </tr>
      </table></td>
  </tr>
</table>
