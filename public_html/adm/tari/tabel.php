<table width="100%" border="0" cellspacing="2" cellpadding="4" class="default">
<?php if($mesaj) { ?>
<tr>
  <td colspan="2"><font class="red"><?php echo $mesaj; ?></font></td></tr>  
<?php } ?>
<tr>
  <th valign="top" align="right" width="160">Denumire tara:</th>
  <td><input type="text" name="param[denumire]" class="mare" value="<?php echo $param['denumire']; ?>" onblur="ajaxpage('check/check_tara.php?tara='+this.value, 'check_tara'); document.getElementById('address').value=this.value; codeAddress();"> <div id="check_tara"></div>
  <div class="infos">Va rugam incercati sa nu folositi caractere speciale(-,*,.,&) in denumire.</div></td>
</tr>
<tr>
    <th align="right" valign="top">Continent:</th>
    <td><?php $sel_c="select id_continent, nume_continent from continente group by id_continent";
	$que_c=mysql_query($sel_c) or die(mysql_error()); ?>
    <select name="param[continent]">
      <?php while($row_c=mysql_fetch_array($que_c)) { ?>
      <option value="<?php echo $row_c['id_continent']; ?>" <?php if($param['continent']==$row_c['id_continent']) { ?> selected="selected" <?php } ?>><?php echo $row_c['nume_continent']; ?></option>
      <?php } @mysql_free_result($que_c); ?>
     </select>
     </td>
   </tr>   
 
 <tr>
    <th align="right" valign="top">Descriere scurta: <br /></th>
    <td><textarea name="param[descriere_scurta]" class="mic"><?php echo $param['descriere_scurta']; ?></textarea></td>
 </tr>
 <tr>
<tr>
  <th valign="top" align="right">Descriere:</th><td class="help_text"> <textarea name="param[descriere]" class="mare mceS"><?php echo $param['descriere']; ?></textarea></td>
</tr>
<tr>
  <th valign="top" align="right">Info scurte:</th><td class="help_text"> <textarea name="param[info_scurte]" class="mic mceS"><?php echo $param['info_scurte']; ?></textarea><br /><div class="infos">Se foloseste pentru afisarea in coloane.</div></td>
</tr>

<?php if($valut_but_submit=='Modifica') include_once($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'clima.php'); ?>

<tr>
  <td align="left" colspan="2"><h2>Capitole</h2></td>
</tr>
<?php $sel_caract="select caracteristici_localizare.* from caracteristici_localizare  where caracteristici_localizare.activ = 'da' group by caracteristici_localizare.id"; 
$que_caract=mysql_query($sel_caract) or die(mysql_error());
while($row_caract=mysql_fetch_array($que_caract)) { ?>
<tr>
  <th align="right" valign="top"><?php echo $row_caract['nume']; ?>:</th>
  <td align="left" valign="top"><?php if($row_caract['tip_file']=='input')  {  ?>
  <input type="text" name="param[caracteristica][<?php echo $row_caract['id']; ?>]" class="mediu" value="<?php echo $param['caracteristica'][$row_caract['id']]; ?>" />
<?php  } else { ?>
 <textarea name="param[caracteristica][<?php echo $row_caract['id']; ?>]" class="mare mceS"><?php echo $param['caracteristica'][$row_caract['id']]; ?></textarea>
<?php } ?> </td>
<?php } @mysql_free_result($que_caract); ?>
<tr>
  <th align="right" valign="top">Adauga Steag: <a name="ad_steag"></a></th>
  <td align="left" valign="top"><?php if($param['steag']) { ?>
  <a href="../img_prima_steag_tara/<?php echo $param['steag']; ?>" rel="gallery"> <img src="../thumb_steag_tara/<?php echo $param['steag']; ?>" /><input type="hidden" name="param[p_steag]" value="<?php echo $param['steag']; ?>" /></a>
  <a href="editare_tara.php?pas=2&tara=<?php echo $id_tara; ?>&del_steag=<?php echo $param['steag']; ?>" class="buttons">Sterge</a> <?php } else { ?><input type="file" name="steag[1]" /><?php if($error_steag) echo '<br/><font class="red">'.$error_steag.'</font>'; } ?></td>
</tr> 
<tr>
  <th align="right" valign="top">Adauga Harta: <a name="ad_harta"></a></th>
  <td align="left" valign="top"><?php if($param['harta']) { ?>
  <a href="../img_prima_harta_tara/<?php echo $param['harta']; ?>" rel="gallery"> <img src="../thumb_harta_tara/<?php echo $param['harta']; ?>" width="100" /><input type="hidden" name="param[p_harta]" value="<?php echo $param['harta']; ?>" /></a>
  <a href="editare_tara.php?pas=2&tara=<?php echo $id_tara; ?>&del_harta=<?php echo $param['harta']; ?>" class="buttons">Sterge</a> <?php } else { ?><input type="file" name="harta[1]" /><?php if($error_harta) echo '<br/><font class="red">'.$error_harta.'</font>'; } ?></td>
</tr>
<tr>
   <td colspan="2" align="left"><h2>Coordonate:</h2></td>
 </tr>
  <tr>
    <th align="right" valign="top" width="160">Latitudine:</th>
    <td align="left" valign="top"><input type="text" name="param[latitudine]" id="latitude" class="mediu" value="<?php if($param['latitudine']) echo $param['latitudine']; ?>" /></td>
  </tr>
  <tr>
    <th align="right" valign="top">Longitudine:</th>
    <td align="left" valign="top"><input type="text" name="param[longitudine]" id="longitude" class="mediu" value="<?php if($param['longitudine']) echo $param['longitudine']; ?>" /></td>
  </tr>
  <tr>
    <td colspan="2"><input type="text" name="param[search]" id="address" class="mare" value="<?php echo $param['search']; ?>" /> <input type="button" name="Cauta" value="Cauta" class="buttons" onclick="codeAddress();" /></td>
  </tr>
 <tr>
  <td colspan="2"><div id="map_canvas" style="width:600px; height:400px"></div><br/></div></td>
 </tr>
<tr>
   <td colspan="2" align="left" valign="top"><h2>Adauga Poze:</h2> <a name="poze"></a></td>
</tr>
<?php for($i=1; $i<=3; $i++) { ?>
<tr>
  <th align="right" valign="top">Poza<?php echo $i; ?>:</th>
  <td align="left" valign="top"><?php if ($param['poza'.$i]<>"") { ?>
  <a href="../img_prima_tara/<?php echo $param['poza'.$i]; ?>" rel="gallery"> <img src="../thumb_tara/<?php echo $param['poza'.$i]; ?>" width="100" /><input type="hidden" name="param[poza<?php echo $i; ?>]" value="<?php echo $param['poza'.$i]; ?>" /></a>
  <a href="editare_tara.php?pas=2&tara=<?php echo $id_tara; ?>&del=<?php echo $i.'&poza='.$param['poza'.$i]; ?>" class="buttons">Sterge</a>
  <?php } else { ?><input type="file" name="attachments[<?php echo $i; ?>]" /> <?php if($error_poza[$i]) echo '<br/><font class="red">'.$error_poza[$i].'</font>'; } ?></td>
</tr>
<?php } ?>
<tr>
  <th align="right" valign="top"><a name="tip_oferta"></a>Tip oferta:</th>
  <td align="left" valign="top"><table width="600" cellpadding="2" cellspacing="0" border="0">
  <tr> <?php $i=0; $tipuri_bifate=array(); $selT="select tip_oferta.id_tip_oferta, tip_oferta.denumire_tip_oferta"; if($id_tara) $selT=$selT.", tari_tip_sejur.id_tip_oferta as tip";  $selT=$selT." from tip_oferta ";
if($id_tara) $selT=$selT." left join tari_tip_sejur on (tari_tip_sejur.id_tara = '".$id_tara."' and tip_oferta.id_tip_oferta = tari_tip_sejur.id_tip_oferta) ";
$selT=$selT." where tip_oferta.activ = 'da' and tip_oferta.tip is not null Group by tip_oferta.id_tip_oferta ";
if($id_tara) $selT=$selT." Order by tari_tip_sejur.id_tip_oferta DESC, tip_oferta.denumire_tip_oferta ASC "; else $selT=$selT." Order by tip_oferta.denumire_tip_oferta ASC ";
$queT=mysql_query($selT) or die(mysql_error());
while($rowT=mysql_fetch_array($queT)) {
if($rowT['tip']) $tipuri_bifate[]=$rowT['tip']; $i++; ?>
 <td align="left" valign="top"><label><input type="checkbox" name="param[tip_oferta][<?php echo $rowT['id_tip_oferta']; ?>]" value="<?php echo $rowT['id_tip_oferta']; ?>" <?php if($rowT['id_tip_oferta']==$rowT['tip'] || $rowT['id_tip_oferta']==$param['tip_oferta'][$rowT['id_tip_oferta']]) { ?> checked="checked" <?php } ?> /><?php echo $rowT["denumire_tip_oferta"]; ?></label></td>
<?php if($i%1==0) echo '</tr>
<tr>'; 
} @mysql_free_result($queT); ?></tr>
</table></td>
</tr>
<tr>
  <td align="right" valign="top">&nbsp;</td>
  <td align="left" valign="top"><div class="infos">Se bifeaza maxim 4 tipuri de oferta care se doresc sa pe pagina de localitate, in tabelul cu hoteluri si oferte</div></td>
</tr>
<?php $tipuri=get_tiputi_oferte('', '', '', $id_tara, '', '');
if(sizeof($tipuri)>0) { ?>
<tr>
  <th align="right" valign="top">Tip oferte existente nebifate: </th>
  <td align="left" valign="top"><?php foreach($tipuri as $key=>$value) if(!in_array($key,$tipuri_bifate)) echo $value['denumire'].', '; ?>
  <div class="infos">* sunt afisate tipurile de oferta care sunt selectate in momentul actual la toate ofertele valabile din tara respectiva</div></td>
</tr>
<?php } ?>

        <tr>
          <td colspan="2" align="left" valign="top"><a href="javascript:;" class="link" onmousedown="if(document.getElementById('mydiv1').style.display == 'none'){ document.getElementById('mydiv1').style.display = 'block'; }else{ document.getElementById('mydiv1').style.display = 'none'; }"><h2>Search Engine Optimization</h2></a></td>
        </tr>
<tr><td colspan="2">
<div id="mydiv1" style="display:none;">
<table cellpadding="2" cellspacing="2" border="0" width="100%">     
        <tr>
<th valign="top" align="right" width="160">Titlu SEO:</th><td class="help_text"><input name="param[titlu_seo]" class="mare" value="<?php echo $param['titlu_seo'];?>" /> </td>
</tr>
<tr>
<th valign="top" align="right">Descriere SEO:</th><td class="help_text"> <textarea name="param[descriere_seo]" class="mic"><?php echo $param['descriere_seo']; ?></textarea><br /></td>
</tr>
<tr>
<th valign="top" align="right">Cuvinte cheie:</th><td class="help_text"> <textarea name="param[cuvinte_seo]" class="mic"><?php echo $param['cuvinte_seo']; ?></textarea><br /></td>
</tr>
</table>
</div>
</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
  <td>&nbsp;</td>
  <td align="left"><input type="submit" name="adauga" value="<?php echo $valut_but_submit; ?>" class="buttons" /></td>
</tr>
</table>