<?php

$sitepath_adm = '/adm/';
$GLOBALS['path_adm'] = $sitepath_adm;
$sitepath = $_SERVER['HTTP_HOST'];;
$sitepath_subdomeniu = $_SERVER['HTTP_HOST'];
$GLOBALS['site_path'] = $sitepath;
$imgpath = $_SERVER['HTTP_HOST'] . '/images/';
$user_db = 'useri';
$agentie_nume = 'Ocazii Turistice';
$tip_admin = ['administator_gen', 'editor_articole', 'editor'];

$acces['administator_gen'] = 'schimba_parola, creare_cont, erori_disponibilitate, localizare, hotel, furnizori, sejur, circuit, rezervari, financiar';
$acces['editor_articole'] = 'schimba_parola';
$acces['editor'] = 'schimba_parola, localizare';

$search = ['cazare', 'nopti', 'demipensiune', 'mic dejun', 'camera dubla', 'vedere la mare', 'vedere la parc'];
$replace = ['Accommodation', 'nights', 'Halfboard', 'Bed & Breakfast', 'Double room', 'sea view', 'park view'];

$serie_document['proforma'] = 'OCZP';
$serie_document['factura'] = 'OCZWEB';
$serie_document['intern']['factura'] = 'OCZ-ROM';
$serie_document['extern']['factura'] = 'OCZ-EXT';
$serie_document['chitanta'] = 'OCZAG';

?>
