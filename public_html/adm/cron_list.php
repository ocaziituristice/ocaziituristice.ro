<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cron list - Agentie<?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php'); ?>
</head>

<body>

<div id="header"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?></div>

<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
  <h1>Lista cronuri</h1>
  <br/>
  <table width="100%" border="0" cellpadding="10" cellspacing="0">
    <tr>
      <td width="33%" align="left" valign="top" style="vertical-align:top;">
		  <a href="<?php echo $sitepath_subdomeniu; ?>cron/valabil_oferte.php" target="_blank" class="buttons">Valabilatate oferte</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Verifica si actualizeaza valabilitatea ofertelor</div>
          <br />
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/update_preturi_hotel.php" target="_blank" class="buttons">Update Preturi hoteluri</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * se adauga preturi din cache in data pret oferta </div>
          <br />
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/update_hoteluri.php" target="_blank" class="buttons">Update hoteluri</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * se adauga localitati de plecare </div>
          <br />
          
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/make_pret_minim.php" target="_blank" class="buttons">Adauga pretul minim la sejururi</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Calculeaza pretul minim pt fiecare sejur</div>
          <br />
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/make_ordonare.php" target="_blank" class="buttons">Make ordonare hoteluri</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Creaza ordonarea implicita a hotelurilor</div>
          <br />
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/dezactivare_cupoane.php" target="_blank" class="buttons">Dezactivare cupoane expirate</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Se dezactiveaza cupoanele care au iesit din perioada de valabilitate</div>
          <br />
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/make_submeniu.php" target="_blank" class="buttons">Make submeniu</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga tarile pentru submeniul de la Turism Extern</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/make_autocomplete.php" target="_blank" class="buttons">Make lista autocomplete</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Creaza lista de tari, zone, localitati pentru autocomplete</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/make_cautare.php" target="_blank" class="buttons">Make lista cautare</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Creaza lista de tari, zone, localitati pentru cautare</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/tari_fara_tip.php" target="_blank" class="buttons">Make lista dashboard</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga in lista de pe dashboard, tarile care nu au tip oferta selectat dar au oferte, localitatile, zonele si tarile fara coordonate</div>
          <br/>
      <a href="<?php echo $sitepath_subdomeniu; ?>cron/anunturi_addwords.php" target="_blank" class="buttons">Anuntuti adwords cu hoteluri</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Anuntuti adwords cu hoteluri </div>            
          <br/>
      <a href="<?php echo $sitepath_subdomeniu; ?>cron/addwords.php" target="_blank" class="buttons">adwords + feed newsman</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Anuntuti adwords cu hoteluri </div>        
         
          
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/useri_anulari.php" target="_blank" class="buttons">Stergere baza de date</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; se modifica statusul abonatilor</div>            
        
          <br/>
         
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/cron_general.php" target="_blank" class="buttons">CRON GENERAL</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Reuneste toate cronurile din aceasta pagina si le ruleaza</div>
          <br/>
      </td>
      <td width="33%" align="left" valign="top" style="vertical-align:top;">
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/luni_tari.php" target="_blank" class="buttons">Luni tari</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga lunile pt tari</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/luni_localitati.php" target="_blank" class="buttons">Luni localitati</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga lunile pt localitati</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/luni_zone.php" target="_blank" class="buttons">Luni zone</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga lunile pt zone</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/make_harta_tari.php" target="_blank" class="buttons">Adauga coordonate harta tara</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga coordonatele hotelurilor la hartile de la tari</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/make_harta_localitati.php" target="_blank" class="buttons">Adauga coordonate harta localitate</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga coordonatele hotelurilor la hartile de la localitati</div>
          <br/>
      </td>
      <td width="33%" align="left" valign="top" style="vertical-align:top;">
          <?php /*?><a href="<?php echo $sitepath_subdomeniu; ?>cron/sitemap_oferte.php" target="_blank" class="buttons">Sitemap Oferte</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga link-urile de la detalii oferte (sejururi, cazari, circuite)</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/sitemap_pagini.php" target="_blank" class="buttons">Sitemap Pagini</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga link-urile de la paginile de oferte (sejururi, cazari, ciruite)</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/sitemap_hoteluri.php" target="_blank" class="buttons">Sitemap Hoteluri</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Adauga link-urile de la detalii hoteluri</div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/><?php */?>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/delete_oferte_fara_hoteluri.php" target="_blank" class="buttons">CURATA oferte fara hoteluri</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Sterge ofertele ale caror hoteluri au fost sterse</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/delete_pret_fara_oferta.php" target="_blank" class="buttons">CURATA preturi fara oferta</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Sterge preturile ale caror oferte au fost sterse</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/delete_pret_pivot_fara_pret.php" target="_blank" class="buttons">CURATA preturi pivot adaugate</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Sterge preturile pivot adaugate care nu mai au corespondenta in oferte</div>
          <br/>
          <a href="<?php echo $sitepath_subdomeniu; ?>cron/delete_pret_pivot_expirat.php" target="_blank" class="buttons">CURATA preturi pivot expirate</a><br/>
             <div class="infos">&nbsp;&nbsp;&nbsp; * Sterge preturile pivot care sunt expirate (data start cu 30 zile in urma)</div>
          <br/>
      </td>
    </tr>
  </table>
  </div>

  <div class="clear"></div>
</div>

<div id="footer"><?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'footer.php'); ?></div>
</body>
</html>