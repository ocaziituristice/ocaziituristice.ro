<?php session_start(); include_once('restrictionare.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agentie<?php echo ucwords(strtolower($agentie_nume)); ?></title>
<?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'/addins_head.php'); ?>
</head>
<body>
<div id="header">
  <?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'header.php'); ?>
</div>
<div id="content">
  <div id="left"><?php echo $CL_acces->mk_left(); ?></div>
  <div id="centru">
    <h1>Creare cont</h1>
    <?php $mesaj='';
	if($_POST['creaza']) {
		$email=$_POST['email'];
		$nume=$_POST['nume'];
		$tip=$_POST['tip'];
		$log=new LOGIN($user_db);
		$mesaj=$log->creare_cont($email, $nume, $tip);
		unset($log);
	} ?>
    <form name="cr_cont" action="" method="post">
      <table width="100%" border="0" cellpadding="2" cellspacing="4" class="default">
        <?php if($mesaj) { ?>
        <tr>
          <td colspan="2" align="left"><font class="red"><?php echo $mesaj; ?></font></td>
        </tr>
        <?php } ?>
        <tr>
          <th width="160" align="right" valign="top">*Nume:</th>
          <td align="left" valign="top"><input type="text" name="nume" class="mediu" value="<?php if($_POST['nume']) echo $_POST['nume']; ?>" /></td>
        </tr>
        <tr>
          <th align="right" valign="top">*Email:</th>
          <td align="left" valign="top"><input type="text" name="email" class="mediu" value="<?php if($_POST['email']) echo $_POST['email']; ?>" /></td>
        </tr>
        <tr>
          <th align="right" valign="top">*Tip user:</th>
          <td align="left" valign="top"><select name="tip">
              <option value="">Selectati</option>
              <?php foreach($tip_admin as $key=>$value) { ?>
              <option value="<?php echo $value; ?>" <?php if($_POST['tip']==$value) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left" valign="top"><input type="submit" name="creaza" value="Adauga user" class="buttons"></td>
        </tr>
      </table>
    </form>
  </div>
  <div class="clear"></div>
</div>
<div id="footer">
  <?php include($_SERVER['DOCUMENT_ROOT'].$sitepath_adm.'footer.php'); ?>
</div>
</body>
</html>