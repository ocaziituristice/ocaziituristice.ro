<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
$tip = "city break";
$linktip = "city-break";
$dentip = "City Break";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte City Break, cazare City Break</title>
<meta name="description" content="Oferte city break, sejururi, cazari City Break hoteluri" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte City Break</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="pad5">
          <img src="images/icon-city-break.jpg" alt="Oferte City Break" class="float-left" style="margin:3px 10px 0 0;" />
          <h2>Pachete city break in orasele Europei</h2>
          <p class="text-justify">Opteaza pentru oferta City Break in care sa te relaxezi si sa petreci un weekend de vis intr-una din numeroasele destinatii: Austria, Franta, Italia, Marea Britanie, Spania, Grecia, Portugalia sau Ungaria si nu vei regreta nici un moment. Printr-un sejur <strong>City Break</strong> poti vizita si admira diferite monumente sau locatii la care visai de foarte mult timp sa le vezi: Turnul Eiffel, Arrabida - zona declarata Parc Natural, Roma, Venetia, Milano sau Barcelona. Acestea sunt numai cateva din minunatele locuri in care poti pleca la preturi accesibile si in care poti sa iti faci timpul liber din weekend un moment special din viata ta.</p>
          
          <br class="clear"><br>
          
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Oferte <?php echo $dentip; ?>:</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
          
        </div>
        
		<?php $sel_cb="SELECT
		COUNT(oferte.id_oferta) AS numar,
		localitati.denumire AS denumire_localitate,
		zone.denumire AS denumire_zona,
		tari.denumire AS denumire_tara,
		tari.steag
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel=hoteluri.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id=localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona=zone.id_zona
		INNER JOIN tari ON zone.id_tara=tari.id_tara
		LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
		WHERE oferte.valabila='da'
		AND hoteluri.tip_unitate<>'Circuit'
		AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
		AND oferte.last_minute='nu'
		AND tari.id_tara<>'1'
		GROUP BY denumire_localitate
		ORDER BY numar DESC, denumire_localitate ASC ";
		$rez_cb=mysql_query($sel_cb) or die(mysql_error());
		while ($row_cb=mysql_fetch_array($rez_cb)) {
			$den_loc=$row_cb['denumire_localitate'];
			$den_zon=$row_cb['denumire_zona'];
			$den_tar=$row_cb['denumire_tara'];
			$link='/oferte-'.$linktip.'/'.fa_link($den_tar).'/'.fa_link($den_zon).'/'.fa_link($den_loc).'/';
		?>
        <a href="<?php echo $link; ?>" title="City Break <?php echo $den_loc; ?>" class="cb-item" style="background:url(images/<?php echo fa_link($den_loc); ?>.jpg) left top no-repeat;"><span class="titlu" style="font-size:<?php if(strlen($den_loc)>8) echo '40px'; else echo '42px'; ?>; letter-spacing:-1pt;">City Break <?php echo $den_loc; ?> <img src="/thumb_steag_tara/<?php echo $row_cb['steag']; ?>" alt="steag <?php echo $den_tar; ?>" /></span></a>
		<?php } @mysql_free_result($rez_cb); ?>
        
        <br class="clear" /><br />
        
      </div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=<?php echo $id_tip_sejur; ?>");
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>