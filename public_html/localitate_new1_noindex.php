<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
if(!$_REQUEST['from']) $from=1; else $from=$_REQUEST['from'];
if(!$nr_pe_pagina=$_COOKIE['nr_pe_pagina']) $nr_pe_pagina=10;
$id_tara=$_REQUEST['tari'];
$tara=get_den_tara($id_tara);
$link_tara=fa_link($tara);
$den_tara=ucwords($tara);
$titlu_pag=$den_tara;
$cont_link='';
$indice='sejur-';
$id_tip='';
$iduri='';

if($_REQUEST['zone']) {
	$id_zona=$_REQUEST['zone'];
	$zona=get_den_zona($id_zona);
	$den_zona=ucwords($zona);
	$link_zona=fa_link($zona);
	$cont_link=$cont_link.'&zone='.$link_zona;
	$titlu_pag=ucwords($zona).', '.$den_tara;
	$titlu_h2=ucwords($zona);
	$titlu_h3=ucwords($zona);
	$cuvant_cheie=$titlu_pag;
} else $id_zona='';

if($_REQUEST['oras']) {
	$id_localitate=$_REQUEST['oras'];
	$oras=get_den_localitate($id_localitate);
	$oras=str_replace('-',' ',$oras);
	if(($oras=='lara' || $oras=='kundu') && $tara=='turcia') {
		header("HTTP/1.0 301 Moved Permanently");
		header("Location: ".$sitepath.'sejur-turcia/antalya/lara-kundu/');
		exit();
	}
	
	$sel="select denumire, descriere_seo, titlu_seo, cuvinte_cheie_seo, luni_plecari, data_plecare from localitati where id_localitate = '".$id_localitate."' ";
	$que=mysql_query($sel) or die(mysql_error());
	$row=mysql_fetch_array($que);
	$localitate=$row['denumire'];
	@mysql_fetch_array($que);
	
	$luni_plecari=explode(',', $row['luni_plecari']);
	$dates_plecare = $row['data_plecare'];
	$link_oras=fa_link($oras);
	//$link_oras=$oras;
	$cont_link=$cont_link.'&oras='.$link_oras;
	$den_loc=$row['denumire'];
	$den_oras=$den_loc;
	$titlu_pag=$den_loc.', '.$den_tara;
	$titlu_h2=$row['denumire'];
	$titlu_h3=$row['denumire'];
	$cuvant_cheie=$titlu_pag;
} else $id_localitate='';

if(!$id_localitate || !$id_zona || !$id_tara) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}

$selSEO="SELECT * FROM seo WHERE id_tip = '0' AND id_oras = '".$id_localitate."' ";
$queSEO=mysql_query($selSEO) or die(mysql_error());
$rowSEO=mysql_fetch_array($queSEO);
@mysql_free_result($queSEO);
if($rowSEO['h1']) $titlu_pag=$rowSEO['h1'];
if($rowSEO['h2']) $titlu_h2=$rowSEO['h2'];
if($rowSEO['h3']) $titlu_h3=$rowSEO['h3'];
if($rowSEO['cuvant_cheie']) $cuvant_cheie=$rowSEO['cuvant_cheie'];

$link_p=$sitepath.'sejur-'.$link_tara.'/'.$link_zona.'/'.$link_oras.'/';
$link=$link_p;
$err=0;
if(!$_REQUEST['data-plecare']) $link='?optiuni=da'; else $link='?data-plecare='.$_REQUEST['data-plecare'];

//optiuni____
if($_GET['early-booking']=='da') {
	$early='da';
	$link=$link.'&early-booking=da';
} else $early='';

if(isset($_COOKIE['grad_ocupare'])) {
	$grdocup = explode("*", $_COOKIE['grad_ocupare']);
	$details_transport = get_details_transport($grdocup[8]);
}

if(($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') or $grdocup[8]!='') {
	if($details_transport['denumire']!='') $_REQUEST['transport'] = fa_link($details_transport['denumire']);
	$trans=desfa_link($_REQUEST['transport']);
	$transport=$_REQUEST['transport'];
	$id_transport=get_id_transport($trans);
	if($id_transport<1) $err++;
	$link=$link."&transport=".$_REQUEST['transport'];
} else {
	$trans='toate';
	$transport=$trans;
	$id_transport='';
}

if($_REQUEST['plecare-avion'] && $_REQUEST['plecare-avion']<>'toate') {
	$plecare_avion=$_REQUEST['plecare-avion'];
	$plecare=desfa_link($plecare_avion);
	$id_loc_plecare_av=get_id_localitate($plecare, '');
	if($id_loc_plecare_av<1) $err++;
	$link=$link."&plecare-avion=".$_REQUEST['plecare-avion'];
} else {
	$plecare_avion='toate';
	$id_loc_plecare_av='';
}

if($_REQUEST['plecare-autocar'] && $_REQUEST['plecare-autocar']<>'toate') {
	$plecare_autocar=$_REQUEST['plecare-autocar'];
	$plecare=desfa_link($plecare_autocar);
	$id_loc_plecare_aut=get_id_localitate($plecare, '');
	if($id_loc_plecare_aut<1) $err++;
	$link=$link."&plecare-autocar=".$_REQUEST['plecare-autocar'];
} else {
	$plecare_autocar='toate';
	$id_loc_plecare_aut='';
}

if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') {
	$stele=explode(',',$_REQUEST['stele']);
	$nr_stele=$_REQUEST['stele'];
	if(!$nr_stele) $err++;
	$link=$link."&stele=".$_REQUEST['stele'];
} else {
	$stele='toate';
	$nr_stele='';
}

if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') {
	$masa=explode(',',$_REQUEST['masa']);
	foreach($masa as $key_masa=>$value_masa) {
		$Lmasa[$key_masa]=$value_masa;
	}
	$Rmasa=$_REQUEST['masa'];
	$nmasa=desfa_link($_REQUEST['masa']);
	if(ereg('[^a-z0-9-]', $_REQUEST['masa'])) $err++;
	$link=$link."&masa=".$_REQUEST['masa'];
} else {
	$masa='toate';
	$nmasa='';
	$Lmasa=$masa;
}

if($_REQUEST['distanta'] && $_REQUEST['distanta']<>'toate') {
	$distanta=$_REQUEST['distanta'];
	$ds=explode('-',trim($distanta));
	if(ereg('[^a-z0-9-]', $_REQUEST['distanta'])) $err++;
	$link=$link."&distanta=".$distanta;
} else {
	$distanta='toate';
	$ds=array();
}

if($_REQUEST['concept'] && $_REQUEST['concept']<>'toate') {
	$concept=explode(',',$_REQUEST['concept']);
	foreach($concept as $key_concept=>$value_concept) {
		$Lconcept[$key_concept]=$value_concept;
	}
	$Rconcept=$_REQUEST['concept'];
	$nconcept=desfa_link($_REQUEST['concept']);
	if(ereg('[^a-z0-9-]', $_REQUEST['concept'])) $err++;
	$link=$link."&concept=".$_REQUEST['concept'];
} else {
	$concept='toate';
	$nconcept='';
	$Lconcept=$concept;
}

if($_REQUEST['facilitati'] && $_REQUEST['facilitati']<>'toate') {
	$facilitati = explode(',',$_REQUEST['facilitati']);
	foreach($facilitati as $key_facilitati => $value_facilitati) {
		$Lfacilitati[$key_facilitati] = $value_facilitati;
	}
	$Rfacilitati = $_REQUEST['facilitati'];
	$nfacilitati = desfa_link($_REQUEST['facilitati']);
	if(ereg('[^a-z0-9-]', $_REQUEST['facilitati'])) $err++;
	$link = $link."&facilitati=".$_REQUEST['facilitati'];
} else {
	$facilitati = 'toate';
	$nfacilitati = '';
	$Lfacilitati = $facilitati;
}

if($_REQUEST['data-plecare']) {
	$din_luna=$_REQUEST['data-plecare'];
	$timeout = time() + 60 * 60 * 24 * 5;
	setcookie('lona_plecare', $din_luna, $timeout);
}

if(isset($_COOKIE['grad_ocupare'])) {
	$checkin = $grdocup[0];
	$link = $link."&checkin=".$checkin;
} else if($_REQUEST['checkin']) {
	$checkin = $_REQUEST['checkin'];
	$link = $link."&checkin=".$checkin;
}

/*if($err>0) echo '<script>document.location.href=\''.$link_p.'\'; </script>';*/
$luna=array('01'=>'Ianuarie', '02'=>'Februarie', '03'=>'Martie', '04'=>'Aprilie', '05'=>'Mai', '06'=>'Iunie', '07'=>'Iulie', '08'=>'August', '09'=>'Septembrie', '10'=>'Octombrie', '11'=>'Noiembrie', '12'=>'Decembrie'); ?>
<?php
if($rowSEO['title_seo']=="") {
	$metas_title = 'Oferte '.$den_loc.' - '.$den_tara.' '.date('Y').' | '.$denumire_agentie;
} else {
	$metas_title = $rowSEO['title_seo'];
}
if($rowSEO['description']=="") {
	$metas_description = 'Oferte '.$den_loc.' '.date('Y'). ' in '.$den_tara.' la cele mai avantajoase tarife pentru vacanta ta in '.$den_loc.', '.$den_zona.' oferite de '.$denumire_agentie;
} else {
	$metas_description = $rowSEO['description'];
}
if($rowSEO['keywords']=="") {
	$metas_keywords = $cuvant_cheie. ', sejur '.$den_loc.', cazare '.$den_loc.', oferte '.$den_loc;
} else {
	$metas_keywords = $rowSEO['keywords'];
}

$link_of_pag=w3c_and($link);
if($_REQUEST['ordonare']) $link=$link."&ordonare=".$_REQUEST['ordonare'];
if($link=='?optiuni=da') $link='';

$oferte=new AFISARE_SEJUR_NORMAL();
$oferte->setAfisare($from, $nr_pe_pagina);
$oferte->config_paginare('nou');
if($early=='da') $oferte->setEarly('da');
if($link_tara) $oferte->setTari($link_tara);
if($link_zona) $oferte->setZone($link_zona);
if($link_oras) $oferte->setOrase($link_oras);
if($id_tip) $oferte->setTipOferta($_REQUEST['tip']);
if($_REQUEST['ordonare']) {
$tipO=explode('-',$_REQUEST['ordonare']);
if($tipO[0]=='tip_pret') $oferte->setOrdonarePret($tipO[1]);
elseif($tipO[0]=='tip_numH') $oferte->setOrdonareNumeH($tipO[1]); }

$watermark_cautare='Cauta hoteluri din '.$den_tara;
if($_REQUEST['nume_hotel'] && $_REQUEST['nume_hotel']<>$watermark_cautare) $oferte->setCautaHotel($_REQUEST['nume_hotel']);
if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $oferte->setTransport($_REQUEST['transport']);
if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') $oferte->setStele($_REQUEST['stele']);
if($_REQUEST['concept'] && $_REQUEST['concept']<>'toate') $oferte->setConcept($_REQUEST['concept']);
if($_REQUEST['facilitati'] && $_REQUEST['facilitati']<>'toate') $oferte->setFacilitati($_REQUEST['facilitati']);
if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') $oferte->setMasa($_REQUEST['masa']);
if($din_luna) $oferte->setLunaPlecare($din_luna);
if($checkin) $oferte->setCheckIn($checkin);
if($id_loc_plecare_av) $oferte->setPlecareAvion($id_loc_plecare_av);
if($id_loc_plecare_aut) $oferte->setPlecareAutocar($id_loc_plecare_aut);
if(sizeof($ds)==3) $oferte->setDistanta($ds);
$oferte->initializeaza_pagini($link_p, 'pag-###/', $link);
$nr_hoteluri=$oferte->numar_oferte();

$meta_pages = $oferte->meta_pages($link_tara, $link_zona, $link_oras);
$meta_prev = $meta_pages['meta_prev'];
$meta_next = $meta_pages['meta_next'];
$meta_canonical = '<link rel="canonical" href="'.$meta_pages['link_canonical'].'" />';

$afisare_info = '<strong>Oferte '.$den_loc.'</strong>';
if($checkin) $afisare_info .= ' cu plecare in ziua <strong>'.date("d.m.Y",strtotime($checkin)).'</strong> (&plusmn; 4 zile)';
if($nr_stele) {
	$afisare_info .= ' de <strong>'.$nr_stele;
	if($nr_stele>1) $afisare_info .= ' stele'; else $afisare_info .= ' stea';
	$afisare_info .= '</strong>';
}
if($id_transport) {
	if($id_transport==1) $afisare_info .= ' <strong>'.$trans.'</strong>'; else $afisare_info .= ' cu transport <strong>'.$trans.'</strong>';
}
if($nmasa) $afisare_info .= ' si masa <strong>'.$nmasa.'</strong> inclusa';
if($plecare_avion<>'toate') $afisare_info .= ', plecare din '.ucwords($plecare_avion);
if($plecare_autocar<>'toate') $afisare_info .= ', plecare din '.ucwords($plecare_autocar);
if($nfacilitati) $afisare_info .= ', cu <strong>'.$nfacilitati.'</strong>';
if($nconcept) $afisare_info .= ', pentru <strong>'.$nconcept.'</strong>';

if($_REQUEST['optiuni']) {
	$metas_title = strip_tags($afisare_info);
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?></title>
<meta name="description" content="<?php echo $metas_description; ?>" />
<meta name="keywords" content="<?php echo $metas_keywords; ?>" />
<?php echo $meta_canonical." ".$meta_prev." ".$meta_next."\n"; ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/localitate_new1.php"); ?>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/circuite_tari.php"); ?>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
