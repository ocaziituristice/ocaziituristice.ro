Buna ziua stimate <?php echo $turist_nume; ?>,<br /><br />
<strong>Bine ati sosit acasa!</strong><br /><br />
Va multumim ca ati apelat la serviciile agentiei noastre si, speram ca sejurul petrecut sa fi fost unul deosebit.<br /><br />
Intrucat <strong>parerea</strong> dumneavoastra este <strong>esentiala</strong> pentru noi, am fi incantati daca ne-ati <strong>marturisi</strong> impresiile privind <strong>vacanta</strong> ce tocmai s-a incheiat.<br /><br />
<strong>Cu speranta ca veti reveni la agentia noastra cat de curand, va uram toate cele bune!</strong>
