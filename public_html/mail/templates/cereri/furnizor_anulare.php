Buna ziua stimate colaborator,<br><br>
Va contactez de la <?php echo $contact_den_agentie.' <strong>('.$denumire_agentie.')</strong>'; ?> in legatura cu rezervarea noastra la <strong><?php echo $hotel_nume; ?></strong><?php if($tip_unitate!='Circuit') { ?>  <strong><?php echo $hotel_stele.'*'; ?></strong> din <strong><?php echo $localitate_nume.' / '.$tara_nume; ?></strong><?php } ?>, in perioada <strong><?php echo $data_inceput.' - '.$data_sfarsit; ?></strong> (<?php echo $nr_nopti; ?> nopti), ID intern rezervare <strong><?php echo $idc; ?></strong>, cu urmatorii pasageri:<br>
<ul>
<?php while($row_pasageri=mysql_fetch_array($que_pasageri)) {
	echo '<li><strong>'.$row_pasageri['nume'].' '.$row_pasageri['prenume'].'</strong></li>';
} ?>
</ul>
Am dori sa <strong class="red">ANULAM</strong> aceasta rezervare, intrucat clientul nostru nu si-a indeplinit atributiile contractuale sau a dorit modificarea unuia/unora din serviciile incluse in pachetul turistic.<br><br>
Va multumim pentru intelegere si ne cerem scuze in avans daca prin aceasta rezervare v-am creat neplaceri in vreun fel.<br><br>
Asteptam confirmarea dumneavoastra pentru aceasta <strong>anulare</strong>.
