Buna ziua stimate <?php echo $turist_nume; ?>,<br /><br />
Cu parere de rau te informam ca am anulat rezervarea ta la <strong><?php echo $oferta_denumire; if($hotel_stele>0) echo $hotel_stele.'*'; ?></strong> efectuata de tine in data de <strong><?php echo date("d.m.Y", $data_adaugarii); ?></strong> la ora <strong><?php echo date("H:i", $data_adaugarii); ?></strong>, cu urmatoarele coordonate:<br />
<table cellpadding="0" cellspacing="0" border="0" width="350" class="offer">
  <tr>
    <td width="100" align="left" valign="top">ID oferta:</td>
    <td align="left" valign="top"><a href="<?php echo $oferta_link; ?>" target="_blank" class="link"><strong><?php echo $id_oferta; ?></strong></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong>Data inceput:</strong></td>
    <td align="left" valign="top"><?php echo date("d.m.Y",strtotime($cerere_data)); ?></td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong>Durata:</strong></td>
    <td align="left" valign="top"><?php echo $cerere_nopti; ?> nopti</td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong>Transport:</strong></td>
    <td align="left" valign="top"><?php echo $cerere_transport; ?></td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong>Tip masa:</strong></td>
    <td align="left" valign="top"><?php echo $cerere_tip_masa; ?></td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong>Grad ocupare:</strong></td>
    <td align="left" valign="top"><?php echo $cerere_nr_adulti.' adulti'; if($cerere_nr_copii>0) echo ' + '.$cerere_nr_copii.' copii'; ?></td>
  </tr>
  <?php if(isset($cerere_pret)) { ?>
  <tr>
    <td align="left" valign="top"><strong>Tarif total:</strong></td>
    <td align="left" valign="top"><strong><?php echo $cerere_pret.' '.moneda($cerere_moneda); ?></strong></td>
  </tr>
  <?php } ?>
</table>
<br />
<strong>Motivul anularii:</strong> <strong class="red">- -</strong><br /><br />
Iti multumim pentru interesul acordat site-ului nostru si te asteptam cu o noua rezervare care speram sa o rezolvam cu succes.