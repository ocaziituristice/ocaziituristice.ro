Hello dear partner,<br><br>
I am contacting you from the Travel Agency <?php echo $contact_legal_den_agentie.' <strong>('.$denumire_agentie.')</strong>'; ?> for an availability request at <strong><?php echo $hotel_nume; ?></strong><?php if($tip_unitate!='Circuit') { ?>  <strong><?php echo $hotel_stele.'*'; ?></strong> from <strong><?php echo $localitate_nume.' / '.$tara_nume; ?></strong><?php } ?>.<br><br>
Our client is interested in <strong><?php echo $nr_nopti; ?> nights</strong>, accommodation with <strong><?php echo $tip_masa; ?></strong>, starting with the date <strong><?php echo $data_inceput; ?></strong> (so the accommodation period would be <strong><?php echo $data_inceput.' - '.$data_sfarsit; ?></strong>), for <strong>X double rooms</strong>.<br><br>
Do you have available this hotel with these specifications? If you don’t, can you please recommend us another period at this hotel or another similar hotel in this period?<br><br>
Waiting with interest for your answer.
