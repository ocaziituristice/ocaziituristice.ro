Buna ziua,<br><br>
Ati efectuat o rezervare pe site-ul nostru <a href="<?php echo $link_site; ?>" target="_blank" class="link"><?php echo $denumire_agentie; ?></a> pentru <strong><?php echo $hotel_denumire; ?></strong>.<br><br>
Cu parere de rau am observat ca nu a fost achitat avansul necesar pentru rezervarea ferma a locurilor dorite de dumneavoastra.<br>
Astfel doresc sa va reamintesc ca puteti opta pentru orice din metodele noastre de plata disponibile (<a href="<?php echo $link_site; ?>info-cum-platesc.html" target="_blank" class="link">click aici pentru metodele de plata</a>) pentru achitarea rezervarii dumneavoastra.<br><br>
<?php if($procent_avans) { ?>Suma de plata pentru avans: <strong class="red"><?php echo ($cerere_pret * $procent_avans).' '.moneda($cerere_moneda); ?></strong>.<br><?php } ?>
Suma totala de plata: <strong><?php echo $cerere_pret.' '.moneda($cerere_moneda); ?></strong>.<br><br>
<strong>IMPORTANT!</strong> Intrucat termenul de plata a rezervarii dumneavoastra a fost depasit, va rog respectuos sa ne contactati inainte sa achitati, pentru a va reconfirma disponibilitatea locurilor dorite.<br>
Deasemeni, nu ezitati sa ne contactati pentru orice intrebare sau nelamurire aveti.