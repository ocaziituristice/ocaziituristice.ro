<p>Buna ziua,<br><br>
  Atasat va trimit voucherele de calatorie pentru <strong><?php echo $cerere_oferta_den; ?></strong>, pe care <strong>trebuie</strong> sa le aveti la dumneavoastra <strong>printate</strong>.</p>
<p>Va rog sa-mi confirmati primirea lor precum si corectitudinea datelor din acestea.<br>
  <br>
  Va multumim pentru ca ati ales agentia noastra pentru achizitia vacantei dumneavoastra si va dorim <strong>Drum Bun</strong> si <strong>Vacanta Placuta</strong> !
</p>
