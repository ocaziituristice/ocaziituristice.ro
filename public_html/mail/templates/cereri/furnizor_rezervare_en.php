Dear collaborator,<br><br>
I am contacting you from the Travel Agency <?php echo $contact_legal_den_agentie.' <strong>('.$denumire_agentie.')</strong>'; ?> for a <strong>BOOKING </strong> at <strong><?php echo $hotel_nume; ?></strong><?php if($tip_unitate!='Circuit') { ?> <strong><?php echo $hotel_stele.'*'; ?></strong> from <strong><?php echo $localitate_nume.' / '.$tara_nume; ?></strong><?php } ?>.<br><br>
<strong>BOOKING DETAILS:</strong><br>
<ul>
  <li>Name of offer: <strong><?php echo $oferta_nume; ?></strong></li>
  <li>Internal booking ID: <strong><?php echo $idc; ?></strong></li>
  <li>Booking period: <strong><?php echo $data_inceput.' - '.$data_sfarsit; ?></strong> (<?php echo $nr_nopti; ?> nights)</li>
  <?php if($early_booking=='da') { ?><li>Offer type: <strong>Early Booking</strong><?php } ?>
  <li>Room type: <strong>1 x <?php echo $row_cerere['nume_camera']; ?></strong></li>
  <li>Meal: <strong><?php echo $row_cerere['tip_masa']; ?></strong></li>
  <li>PASSENGERS (<?php echo $nr_adulti.' ad'; if($nr_copii>0) echo ' + '.$nr_copii.' chd'; ?>):
    <ol>
      <?php while($row_pasageri=mysql_fetch_array($que_pasageri)) {
		  echo '<li><strong>'.$row_pasageri['nume'].' '.$row_pasageri['prenume'].', '.$row_pasageri['sex'];
		  if($row_pasageri['data_nasterii']!='0000-00-00') echo ', '.date('d.m.Y',strtotime($row_pasageri['data_nasterii']));
		  if($row_pasageri['buletin']) echo ', '.$row_pasageri['buletin'];
		  echo '</strong></li>';
	  } ?>
    </ol>
  </li>
</ul>
Waiting for your confirmation to this reservation.<br />
Have a nice day!
