Buna ziua stimate <?php echo $turist_nume; ?>,<br /><br />
<strong>Bine ati sosit acasa!</strong><br /><br />
Va multumim ca ati apelat la serviciile agentiei noastre si speram ca sejurul petrecut sa fi fost unul deosebit.<br /><br />
Intrucat <strong>parerea</strong> dumneavoastra este <strong>esentiala</strong> pentru noi si pentru a ii ajuta si pe viitorii turisti sa faca alegerea buna, am dori sa aflam despre experienta dumneavoastra la <strong><?php echo $hotel_denumire; ?></strong>.<br /><br />
Va rugam sa dati click pe linkul urmator pentru a va exprima parerea si a ne impartasii impresiile dumneavoastra:<br />
<a href="<?php echo $comentarii_link; ?>"><?php echo $comentarii_link; ?></a><br /><br />
<strong>Cu speranta ca veti reveni la agentia noastra cat de curand, va uram toate cele bune!</strong>
