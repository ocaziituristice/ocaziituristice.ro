Buna ziua,<br><br>
Atasat va trimit documentele pentru rezervarea dumneavoastra la <strong><?php echo $cerere_oferta_den; ?></strong>.<br><br>
<strong>Status rezervare:</strong> <strong class="red">CONFIRMATA</strong> de catre <?php  echo $furnizor_denumire_agentie."-".$furnizor_denumire_societate;?><br>

<br>
Suma totala de plata: <strong><?php echo $cerere_pret.' '.moneda($cerere_moneda); ?></strong>.<br><br>
<?php if($procent_avans) { ?>Suma minima de plata pentru avans: <strong class="red"><?php echo ($cerere_pret * $procent_avans).' '.moneda($cerere_moneda); ?></strong>.<br><?php } ?>
Termenul pentru achitarea avansului: <strong><?php echo date('d.m.Y',strtotime($plata_avans)); ?></strong>.<br>
Termenul pentru achitarea restului de plata: <strong><?php echo date('d.m.Y',strtotime($plata_rest)); ?></strong>.<br><br>
Dupa achitarea facturii proforme, va rugam, in limita posibilitatii, sa ne anuntati sau sa ne trimiteti o copie dupa dovada platii, pentru a va emite in cel mai scurt timp factura fiscala.