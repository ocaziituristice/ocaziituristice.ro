Buna ziua stimate <?php echo $turist_nume; ?>,<br><br>
Se apropie ziua plecarii in sejurul selectat.<br><br>
<?php if($cerere_transport=='Fara transport') { ?>
<?php } else { ?>
Astfel doresc sa va instiintez ca pentru <strong><?php echo $cerere_oferta_den; ?></strong> cu plecare in data de <strong><?php echo date("d.m.Y",strtotime($cerere_data)); ?></strong> avem urmatoarele informatii:<br><br>
<strong>Punct de intalnire:</strong> <br>
<strong>Persoana de contact:</strong> <br>
<strong>Ghid insotitor:</strong> <br>
<strong>Orar de zbor:</strong> <br>
DUS: <br>
INTORS: <br>
<br>
<strong>Avem rugamintea sa nu sunati persoanele de contact decat in ziua plecarii!</strong><br><br>
<?php } ?>
Va dorim drum bun si vacanta sa va ofere momente minunate si clipe de neuitat!<br>
Va multumim ca ati apelat la serviciile agentiei noastre de turism si speram sa va putem ajuta si in vacantele viitoare.