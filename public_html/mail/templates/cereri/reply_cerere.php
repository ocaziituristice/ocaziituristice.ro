Buna ziua,<br><br>
Va multumim ca ati apelat la serviciile agentiei noastre de turism si va asigur ca veti beneficia de tot sprijinul si seriozitatea noastra pana la finalizarea cerintelor dumneavoastra.<br><br>
Am placerea sa va prezint mai jos oferta solicitata si <strong>va confirm disponibilitatea locurilor</strong>:<br><br>
<strong class="titlu_hotel"><?php echo $hotel_denumire.' '.$hotel_stele.'*'; ?></strong><br><br>
<div class="descriere"><?php echo $hotel_descriere; ?></div><br>
<strong class="blue">Link <?php echo $denumire_agentie; ?>:</strong> <a href="<?php echo $oferta_link; ?>" class="link"><?php echo $oferta_link; ?></a><br><br>
<table cellpadding="0" cellspacing="0" border="0" class="offer">
  <tr>
    <th width="110" align="center" valign="top"><strong>Intrare</strong></th>
    <th width="100" align="center" valign="top"><strong>Durata</strong></th>
    <th width="160" align="center" valign="top"><strong>Grad ocupare</strong></th>
    <th width="180" align="center" valign="top"><strong>Tip camera</strong></th>
    <th width="110" align="center" valign="top"><strong>Pret</strong></th>
  </tr>
  <tr>
    <td align="center" valign="middle" class="red"><strong>zz.ll.aaaa</strong></td>
    <td align="center" valign="middle" class="red"><strong>x nopti</strong></td>
    <td align="center" valign="middle" class="red"><strong>2 adulti + 1 copil</strong></td>
    <td align="center" valign="middle" class="red"><strong>denumire_cam</strong></td>
    <td align="center" valign="middle" class="red"><strong>0000 RON</strong></td>
  </tr>
</table>
<br>
<?php if($early_booking=='da') { ?>
<div><strong>Tarifele afisate includ reducerea Early Booking de <span class="red"><?php echo $early_booking_discount; ?>%</span>, valabila pana la <span class="red"><?php echo $early_booking_data; ?></span></strong><br>
</div>
<br>
<?php } ?>
<?php if($servicii_incluse) { ?>
<div><strong>Servicii incluse:</strong><br>
<?php echo $servicii_incluse; ?></div>
<br>
<?php } ?>
<?php if($servicii_neincluse) { ?>
<div><strong>Servicii neincluse:</strong><br>
<?php echo $servicii_neincluse; ?></div>
<br>
<?php } ?>
<?php if($descriere_oferta) { ?>
<div><strong>Descrierea programului:</strong><br>
<?php echo afisare_frumos(schimba_caractere($descriere_oferta), 'nl2br'); ?></div>
<br>
<?php } ?>
<?php if($nota) { ?>
<div><strong>Nota:</strong><br>
<?php echo $nota; ?></div>
<br>
<?php } ?>
<div><strong>Conditii plata:</strong><br>
<?php echo $conditii_plata; ?></div>
<br>
<div><strong>Conditii anulare:</strong><br>
<?php echo $conditii_anulare; ?></div>
<br>
<div><em>Pentru rezervarea ferma avem nevoie sa ne furnizati urmatoarele: <strong>numele turistilor</strong>, <strong>datele de nastere</strong> (obligatoriu pentru copii, optional adulti) si <strong>datele de facturare ale unei persoane</strong> (nume, adresa, telefon, email). Apoi, noi vom emite <strong>bonul de comanda</strong>, <strong>contractul cu turistul</strong> si <strong>factura proforma</strong> cu data expirarii a ofertei curente. Dupa achitarea facturii proforme va vom elibera factura fiscala. Factura fiscala se poate face pe persoana fizica, cat si pe persoana juridica.</em></div>
