Buna ziua,<br><br>
Prin acest email doresc sa va confirm inregistrarea platii dumneavaostra pentru rezervarea <strong><?php echo $cerere_oferta_den; ?></strong>.<br><br>
<strong>Va multumim pentru efectuarea platii!</strong><br><br>
Suma care a mai ramas de plata este: <strong><?php //echo ($cerere_pret - ($cerere_pret * $procent_avans)).' '.moneda($cerere_moneda); ?>
<?php echo $rest_plata;?>
</strong><br>
Termenul pentru achitarea restului de plata este: <strong><?php echo date('d.m.Y',strtotime($plata_rest)); ?></strong>.<br><br>
Deasemeni, va atasez factura fiscala aferenta platii dumneavoastra. Aceasta factura NU trebuie platita ea reprezinta dovada fiscala a platii.
