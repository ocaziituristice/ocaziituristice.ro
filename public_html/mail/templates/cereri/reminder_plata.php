Buna ziua,<br><br>
Prin prezentul email doresc sa va instiintez despre achitarea restului de plata pentru rezervarea dumneavoastra la <strong><?php echo $hotel_denumire; ?></strong>.<br><br>
<?php if($procent_avans) { ?>
	Suma platita pana acum: <?php echo ($cerere_pret * $procent_avans).' '.moneda($cerere_moneda); ?><br>
    <span style="text-decoration:underline"><strong>Suma de plata ramasa:</strong> <strong class="red"><?php echo ($cerere_pret - ($cerere_pret * $procent_avans)).' '.moneda($cerere_moneda); ?></strong></span>.<br>
<?php } ?>
Termenul pentru achitarea restului de plata: <strong><?php echo date('d.m.Y',strtotime($plata_rest)); ?></strong>.<br><br>
Dupa achitarea restului de plata, va rugam, in limita posibilitatii, sa ne anuntati sau sa ne trimiteti o copie dupa dovada platii, pentru a va emite in cel mai scurt timp factura fiscala si voucherele.