Buna ziua stimate colaborator,<br><br>
Va contactez de la <?php echo $contact_den_agentie.' <strong>('.$denumire_agentie.')</strong>'; ?> in vederea unei <strong>REZERVARI </strong> pentru <strong><?php echo $hotel_nume; ?></strong><?php if($tip_unitate!='Circuit') { ?> <strong><?php echo $hotel_stele.'*'; ?></strong> din <strong><?php echo $localitate_nume.' / '.$tara_nume; ?></strong><?php } ?>.<br><br>
<strong>DETALII REZERVARE:</strong><br>
<ul>
  <li>Denumire oferta: <strong><?php echo $oferta_nume; ?></strong></li>
  <li>ID intern rezervare: <strong><?php echo $idc; ?></strong></li>
  <li>Perioada rezervare: <strong><?php echo $data_inceput.' - '.$data_sfarsit; ?></strong> (<?php echo $nr_nopti; ?> nopti)</li>
  <?php if($early_booking=='da') { ?><li>Regim oferta: <strong>Early Booking</strong><?php } ?>
  <li>Tip camera: <strong>1 x <?php echo $row_cerere['nume_camera']; ?></strong></li>
  <li>Tip masa: <strong><?php echo $row_cerere['tip_masa']; ?></strong></li>
  <li>PASAGERI (<?php echo $nr_adulti.' ad'; if($nr_copii>0) echo ' + '.$nr_copii.' chd'; ?>):
    <ol>
      <?php while($row_pasageri=mysql_fetch_array($que_pasageri)) {
		  echo '<li><strong>'.$row_pasageri['nume'].' '.$row_pasageri['prenume'].', '.$row_pasageri['sex'];
		  if($row_pasageri['data_nasterii']!='0000-00-00') echo ', '.date('d.m.Y',strtotime($row_pasageri['data_nasterii']));
		  if($row_pasageri['buletin']) echo ', '.$row_pasageri['buletin'];
		  echo '</strong></li>';
	  } ?>
    </ol>
  </li>
</ul>
Asteptam confirmarea dumneavoastra pentru aceasta rezervare.
