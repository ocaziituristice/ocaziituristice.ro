Buna ziua stimate <?php echo $turist_nume; ?>,<br /><br />
Va multumim ca ati apelat la serviciile agentiei noastre.<br /><br />
Am adauga o facilitate noua: <strong>COMENTARII (impresii) LA HOTEL</strong>.<br />
<br />
Intrucat <strong>parerea</strong> dumneavoastra este <strong>esentiala</strong> pentru noi si pentru a ii ajuta si pe viitorii turisti sa faca alegerea buna, am dori sa aflam despre experienta dumneavoastra la <strong><?php echo $hotel_denumire.' '.$hotel_stele.'*'; ?></strong>.<br /><br />
Va rugam sa dati click pe linkul urmator pentru a va exprima parerea si a ne impartasii impresiile dumneavoastra:<br />
<a href="<?php echo $comentarii_link; ?>"><?php echo $comentarii_link; ?></a><br /><br />
<strong>Cu speranta ca veti reveni la agentia noastra cat de curand, va uram toate cele bune!</strong>
