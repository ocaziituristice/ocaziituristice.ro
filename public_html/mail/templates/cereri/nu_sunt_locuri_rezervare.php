Buna ziua,<br><br>
Va multumim ca ati apelat la serviciile agentiei noastre de turism si va asigur ca veti beneficia de tot sprijinul si seriozitatea noastra pana la finalizarea cerintelor dumneavoastra.<br><br>
Din pacate la <strong><?php echo $hotel_denumire.' '.$hotel_stele.'*'; ?></strong> pentru <strong><?php echo $cerere_nr_adulti.' adult'; if($cerere_nr_adulti>1) echo 'i'; if($cerere_nr_copii>0) { echo ' + '.$cerere_nr_copii.' copi'; if($cerere_nr_copii=='1') echo 'l'; else echo 'i'; } ?></strong> nu mai sunt locuri disponibile in perioada aleasa de dumneavoastra <strong>(<?php echo date('d.m.Y',strtotime($cerere_data)).', '.$cerere_nopti.' nopti'; ?>)</strong>.<br>
Mentionez ca perioada <strong>xx.xx - xx.xx</strong> este trecuta la hotel ca <strong class="red">STOP SALES</strong>.<br><br>
Va pot sugera cateva hoteluri asemanatoare cu cel dorit de dumneavoastra:<br><br>
