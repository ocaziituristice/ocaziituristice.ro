<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
$meta_index = "noindex,follow";
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Rezervare expirata! -<?php echo $denumire_agentie; ?></title>
<meta name="description" content=" <?php echo $denumire_agentie; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <span class="titlu_modala">Pagina expirata</span>
        
        <div class="Hline"></div>
        
        <div class="text-justify pad20 article">
         <br />
         <br />
         <?php //echo session_id();?>
          Rezervarea dumneavostra a fost salvata la noi in sitem, dar pagina de plata a expirat!<br />
          In cel mai scurt timp o sa fiti contactat de unnul din agentii nostri
           <br />
         <br />
        </div>
        
      </div>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>