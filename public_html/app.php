<?php

$_SERVER['REQUEST_URI'] = str_ireplace('/legacy', '', $_SERVER['REQUEST_URI']);

$uri = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

if ($uri !== '/' && file_exists(__DIR__ . '/public' . $uri)) {
    return false;
}

$_GET['_url'] = $_SERVER['REQUEST_URI'];


require __DIR__ . '/../phalcon/app/bootstrap_web.php';