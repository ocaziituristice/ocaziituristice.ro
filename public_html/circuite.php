<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
if(!$_REQUEST['from']) $from=1; else $from=$_REQUEST['from'];
if(!$nr_pe_pagina=$_COOKIE['nr_pe_pagina']) $nr_pe_pagina=10;
$err=0;
if($_REQUEST['tip']) {
	$tip=desfa_link($_REQUEST['tip']);
	$id_tip=get_id_tip_sejur($tip);
	if(!$id_tip) {
		header("HTTP/1.0 301 Moved Permanently");
		header("Location: ".$sitepath.'circuite/');
		exit();
	}
	$indice='circuite-'.$_REQUEST['tip'].'/';
} else $indice='circuite/';

$sel_plecari = "SELECT date_format(data_pret_oferta.data_start, '%m-%Y') AS new_date
FROM data_pret_oferta
INNER JOIN oferte ON data_pret_oferta.id_oferta = oferte.id_oferta
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
AND data_pret_oferta.data_start > NOW()
GROUP BY new_date
ORDER BY data_pret_oferta.data_start ";
$que_plecari = mysql_query($sel_plecari) or die(mysql_error());
while($row_plecari = mysql_fetch_array($que_plecari)) {
	$luni_plecari[] = $row_plecari['new_date'];
}
@mysql_fetch_array($que_plecari);
$luna=array('01'=>'Ianuarie', '02'=>'Februarie', '03'=>'Martie', '04'=>'Aprilie', '05'=>'Mai', '06'=>'Iunie', '07'=>'Iulie', '08'=>'August', '09'=>'Septembrie', '10'=>'Octombrie', '11'=>'Noiembrie', '12'=>'Decembrie');

$titlu_pag='Circuite';
$link_p=$sitepath.$indice;
$link=$link_p;
$err=0;
/*if(!$_REQUEST['optiuni'])*/ $link='?optiuni=da';

//optiuni____
if($_GET['early-booking']=='da') {
	$early='da';
	$link=$link.'&early-booking=da';
} else $early='';

if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') {
	$trans=desfa_link($_REQUEST['transport']);
	$transport=$_REQUEST['transport'];
	$id_transport=get_id_transport($trans);
	if($id_transport<1) $err++;
	$link=$link."&transport=".$_REQUEST['transport'];
} else {
	$trans='toate';
	$transport=$trans;
	$id_transport='';
}

if($_REQUEST['plecare-avion'] && $_REQUEST['plecare-avion']<>'toate') {
	$plecare_avion=$_REQUEST['plecare-avion'];
	$plecare=desfa_link($plecare_avion);
	$id_loc_plecare_av=get_id_localitate($plecare, '');
	if($id_loc_plecare_av<1) $err++;
	$link=$link."&plecare-avion=".$_REQUEST['plecare-avion'];
} else {
	$plecare_avion='toate';
	$id_loc_plecare_av='';
}

if($_REQUEST['nr_zile'] && $_REQUEST['nr_zile']<>'toate') {
	$zile=explode(',',$_REQUEST['nr_zile']);
	$nr_zile=$_REQUEST['nr_zile'];
	if(!$nr_zile) $err++;
	$link=$link."&nr_zile=".$_REQUEST['nr_zile'];
} else {
	$zile='toate';
	$nr_zile='';
}

if($_REQUEST['data-plecare']) {
	$din_luna=$_REQUEST['data-plecare'];
	$timeout = time() + 60 * 60 * 24 * 5;
	setcookie('lona_plecare', $din_luna, $timeout);
	$link=$link."&data-plecare=".$_REQUEST['data-plecare'];
}

if($err>0){
	header("HTTP/1.0 301 Moved Permanently");
	header("Location: ".$link_p);
	exit();
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php");
if($id_tip) {
	$keyword_nice[1]='Circuite '.ucwords($tip).' Europa, Asia, America, Africa';
	$keyword_nice[0]='Ocaziituristice.ro ofera, Circuit '.ucwords($tip).' Europa, Circuit '.ucwords($tip).' Asia, Circuit '.ucwords($tip).' America, Circuit '.ucwords($tip).' Africa';
} else {
	$keyword_nice[1]="Circuite Europa, Asia, America, Africa";
	$keyword_nice[0]="Ocaziituristice.ro ofera Circuit Europa, Circuit Asia, Circuit America, Circuit Africa";
} ?>
<title><?php echo $keyword_nice[1]; ?></title>
<meta name="description" content="<?php echo $keyword_nice[0]; ?>" />
<link rel="canonical" href="<?php echo $link_p; ?>" />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/circuite/circuite.php"); ?>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>