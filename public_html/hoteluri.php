<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_hotel/class_hoteluri.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Hoteluri<?php if($_REQUEST['stele']) echo " de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; ?>, sejururi, cazari | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Hoteluri <?php if($_REQUEST['stele']) echo "de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; ?>, oferte sejur, cazari, circuite de la peste 500 agentii de turism" />
<meta name="keywords" content="Hoteluri Romania, Hoteluri Grecia, Hoteluri Italia, Hoteluri Bulgaria, Hoteluri Egipt, Hoteluri Franta, Hoteluri Austria, Hoteluri Ungaria, Hoteluri Cehia, hoteluri <?php if($_REQUEST['stele']) echo "de ".$_REQUEST['stele']." ".$_REQUEST['stea_nume']; ?>, agentii de turism" />
<?php $link="http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI']; ?>
<meta name="Publisher" content="ocaziituristice.ro" />

<meta name="robots" content="noindex, nofollow">
<meta name="robots" content="noarchive">
<meta name="googlebot" content="nosnippet">
<meta name="googlebot" content="noodp">
<meta name="language" content="ro" />

<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
  <table class="mainTableClass" border="0" cellspacing="0" cellpadding="0">
	<tr>
      <td colspan="2" align="left" valign="top">
        <div class="breadcrumb">
         <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
        </div>
      </td>
    </tr>
	<tr>
	  <td class="mainTableColumnLeft" align="left" valign="top">
      <div id="columnLeft">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri/hoteluri.php"); ?>
      </div>
      </td>
	  <td class="mainTableColumnRight" align="left" valign="top">
        <div id="columnRight">
        <?php //include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta.php"); ?>
        <?php //include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_main_reclama.php"); ?>
        </div>
      </td>
	</tr>
  </table>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>