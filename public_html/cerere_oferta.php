<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
//require_once($_SERVER['DOCUMENT_ROOT'].'/config/recaptchalib.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cerere oferta</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<?php
$link="http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI']; ?>
<meta name="Publisher" content="ocaziituristice.ro" />
<meta name="ROBOTS" content="INDEX,FOLLOW" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/cerere_oferta.php"); ?>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/datepicker_ro.js"></script>
<script type="text/javascript">
//<![CDATA[
$(function() {
  $("#data_start").datepicker({
	  numberOfMonths: 3,
	  dateFormat: "yy-mm-dd",
	  altField: "#data_end",
	  showButtonPanel: true
  });
});
function include(page) {
$("#continut").load("/includes/cerere_oferta_"+page+".php"); }
//]]>
</script>
</body>
</html>
