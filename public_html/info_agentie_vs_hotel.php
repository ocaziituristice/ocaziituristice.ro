<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); ?>
<?php /*?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Agentie de turism vs hotel - <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Cum cumpar <?php echo $denumire_agentie; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full"><?php */?>
      <div id="NEW-destinatie" class="clearfix">

        <span class="titlu_modala">Agenție de turism vs. Hotel</span>
        
        <div class="Hline"></div>
        
        <div class="pad20 article" align="justify">
          <div>Întrebarea care ne frământă pe toți:</div><br />
          <div><strong>De ce să rezerv de la o agenție de turism și să nu sun direct la hotel, astfel înlăturând intermediarul?</strong></div><br />
          <div>Iată nu unul, ci mai multe motive pentru care agenția de turism ar fi alegerea mai bună:</div><br />
          <ul class="article">
            <li>Agenția de turism îți poate oferi un tarif mai mic decât dacă suni la hotel. De ce? Cum? Simplu! Agenția de turism are contract cu hotelul în care este stipulat că va vinde la un tarif mai mic decât la recepție și uneori are chiar și alocații ceea ce înseamnă că se pot reduce și mai mult costurile, iar ocuparea locurilor se poate face imediat. În plus, agenția poate negocia diverse reduceri sezoniere cu hotelul, obținând din nou un tarif mai mic, special pentru clienți.</li>
            <li>Agenția de turism îți poate oferi un pachet complet de servicii turistice - cazare hotel, transport, transfer de la hotel la punctul de îmbarcare (pentru transportul cu avionul), asigurare de călătorie, etc. - pe când dacă rezervi direct la hotel, va trebui sa te ocupi singur de toate aceste lucruri.</li>
            <li>Agenția de turism îți poate oferi excursii în țară și în străinătate special create pentru clienții săi la prețuri foarte atractive, prin faptul ca se rezerva cu mult timp înainte hotelul și se închiriază un avion special pentru acele curse, astfel turiștii beneficiind de tarife mult mai mici la aceste curse charter decât la cursele de linie.</li>
            <li>Agenția de turism îți va aloca un agent de turism care se va ocupa în întregime de rezervarea ta până la finalizarea cu succes a acesteia. Agentul de turism te va consilia și te poate ajuta cu orice informație legată de rezervare sau de locurile din jur.</li>
            <li>În general, agenția de turism va avea nevoie de un avans pentru rezervare (dacă cererea se înregistrează cu mai mult de 21 de zile înainte de plecare), pe când multe hoteluri îți vor cere suma integrală.</li>
            <li>Agenția de turism Ocaziituristice.ro oferă clienților săi și o premiere pentru fidelitatea lor, oferindu-le <a href="/info-puncte-fidelitate.html" target="_blank" rel="nofollow" class="infos link-blue"><strong class="blue">puncte de fidelitate</strong></a> care pot fi folosite la rezervări ulterioare.</li>
            <li>Agenția de turism vorbește limba ta, indiferent de țara unde vrei sa pleci, înlăturându-se astfel eventualele confuzii sau neînțelegeri care pot apărea în conversația ta directă cu personalul hotelului. Știm din experiență ca în unele țări nu se vorbește foarte fluent Engleză, iar comunicarea este îngreunată din aceasta cauză.</li>
          </ul>
          <div>Acestea sunt doar câteva din avantajele de care poți beneficia alegând să efectuezi o rezervare prin agenția de turism Dream Voyage International, deținătoare a portalului de turism Ocaziituristice.ro .</div><br />
          <div>Sperăm ca cele scrise mai sus să te convingă și te așteptăm cu drag cu o rezervare, iar dacă ai orice întrebări, nelămuriri, îți stăm la dispoziție. <a href="/contact.html" target="_blank" class="link-blue"><span class="blue">Click aici</span></a> pentru modalitățile de contact.</div>
        </div>
        
      </div>
<?php /*?>    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html><?php */?>