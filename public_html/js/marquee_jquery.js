	  $(function() {
	  
		//cache the ticker
		var ticker = $("#ticker");
		  
		//wrap dt:dd pairs in divs
		ticker.children().filter("span").each(function() {
		  
		  var span = $(this),
		    container = $("<span>");
		  
		  span.next().appendTo(container);
		  span.prependTo(container);
		  
		  container.appendTo(ticker);
		});
				
		//hide the scrollbar
		ticker.css("overflow", "hidden");
		
		//animator function
		function animator(currentItem) {
		    
		  //work out new anim duration
		  var distance = currentItem.width();
			duration = (distance + parseInt(currentItem.css("marginLeft"))) / 0.025;

		  //animate the first child of the ticker
		  currentItem.animate({ marginLeft: -distance }, duration, "linear", function() {
		    
			//move current item to the bottom
			currentItem.appendTo(currentItem.parent()).css("marginLeft", 0);

			//recurse
			animator(currentItem.parent().children(":first"));
		  }); 
		};
		
		//start the ticker
		animator(ticker.children(":first"));
				
	  });



