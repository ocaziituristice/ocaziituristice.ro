var anylinkmenu3={divclass:'anylinkmenucols', inlinestyle:'', linktarget:'secwin'} //Third menu variable. Same precaution.
anylinkmenu3.cols={divclass:'column', inlinestyle:''} //menu.cols if defined creates columns of menu links segmented by keyword "efc"
anylinkmenu3.items=[
	["Anglia", "/anglia/"],
	["Austria", "/austria/"],
	["Bulgaria", "/bulgaria/"],
	["Cehia", "/cehia/"],
	["Cipru", "/cipru/"],
	["Croatia", "/croatia/"],
	["Cuba", "/cuba/"],
	["Egipt", "/egipt/"],
	["Emiratele Arabe U", "/emiratele_arabe_unite/"],
	["Franta", "/franta/", "efc"],
	["Grecia", "/grecia/"],
	["Italia", "/italia/"],
	["Macedonia", "/macedonia/"],
	["Malta", "/malta/"],
	["Mexic", "/mexic/"],
	["Muntenegru", "/muntenegru/"],
	["Norvegia", "/norvegia/"],
	["Peru", "/peru/"],
	["Portugalia", "/portugalia/", "efc"],
	["Republica Dominic", "/republica_dominicana/"],
	["Romania", "/romania/"],
	["Senegal", "/senegal/"],
	["Spania", "/spania/"],
	["Statele Unite Ale", "/statele_unite_ale_americii/"],
	["Suedia", "/suedia/"],
	["Tunisia", "/tunisia/"],
	["Turcia", "/turcia/"],
	["Ungaria", "/ungaria/", "efc"] //no comma following last entry!
]


