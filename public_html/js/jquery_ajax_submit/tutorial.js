$(function() {
	$('.error').hide();
	
	$(".button").click(function() {
		$('.error').hide();
		
		//var name = $("input#name").val();
		var name = document.getElementById("name").value;
		if (name == "") {
			$("label#name_error").show();
			$("input#name").focus();
			return false;
		}
		var id_cerere = document.getElementById("id_cerere").value;
		
		var dataString = 'name=' + name + '&id_cerere=' + id_cerere;
		//alert (dataString);return false;
		
		$.ajax({
			type: "POST",
			url: "/adm/cereri/observatii_proces.php",
			data: dataString,
			success: function() {
				$('#observatii_form').html("<div id='message'></div>");
				$('#message').html("<strong class='red'>Observatia a fost adaugata!</strong>")
			}
		});
		return false;
	});
});
runOnLoad(function() {
	$("textarea#name").select().focus();
});
