var t = -1; 

function selectMenu(id) {
    hideAllSubmenus();
    
    document.getElementById('submenu' + id).style.display = 'block';
    document.getElementById('menu' + id).setAttribute('class', 'selected-menu');
    document.getElementById('menu' + id).className = 'selected-menu';
    
    try {
        if (navigator.appVersion.indexOf('MSIE') != -1) {
            var selects = document.getElementsByTagName('select');
            for (var i = 0; i < selects.length; i++) { 
                selects[i].style.visibility = 'hidden'; 
            }
        }        
    } catch (e) {}
}

function deselectMenu(id) {
    document.getElementById('submenu' + id).style.display = 'none';
    document.getElementById('menu' + id).setAttribute('class', 'deselected-menu');
    document.getElementById('menu' + id).className = 'deselected-menu';
    
    try {
        if (navigator.appVersion.indexOf('MSIE') != -1) {
            var selects = document.getElementsByTagName('select');
            for (var i = 0; i < selects.length; i++) { 
                selects[i].style.visibility = 'visible'; 
            }
        }        
    } catch (e) {}
}

function hideAllSubmenus() {
    deselectMenu('1');
}

function cancelDeselect() {
    if (t != -1) {
        clearTimeout(t);
        t = -1;
    }
}

function startDeselect(id) {
    if (t == -1) {
        t = setTimeout("deselectMenu('" + id + "')", 100);
    }
}


