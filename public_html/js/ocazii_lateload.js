/* Autocomplete */
(function( $ ) {
	var proto = $.ui.autocomplete.prototype,
		initSource = proto._initSource;

	function filter( array, term ) {
		var matcher = new RegExp( $.ui.autocomplete.escapeRegex(term), "i" );
		return $.grep( array, function(value) {
			return matcher.test( $( "<div>" ).html( value.label || value.value || value ).text() );
		});
	}

	$.extend( proto, {
		_initSource: function() {
			if ( this.options.html && $.isArray(this.options.source) ) {
				this.source = function( request, response ) {
					response( filter( this.options.source, request.term ) );
				};
			} else {
				initSource.call( this );
			}
		},

		_renderItem: function( ul, item) {
			return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( $( "<a></a>" )[ this.options.html ? "html" : "text" ]( item.label ) )
			.appendTo( ul );
		}
	});
})( jQuery );

/* Facebook */
/*(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=314711018608962";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));*/

/* Twitter */
/*!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');*/

/* Zopim */
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?ruE5NKUoYm5ac8afZImobWsy47zOj9p9';z.t=+new Date;$.type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');

/* Trusted.ro */
/*var z="";function verify(a,b,c,d){b=b.replace(/|-|./gi,"");var e=new RegExp("\s","g");b=b.replace(e,"");if(!z.closed&&z.location){z.location.href=encodeUrl(a)}else{z=window.open(encodeUrl(a),b,"location=no, scrollbars=yes, resizable=yes, toolbar=no, menubar=no, width="+c+", height="+d);if(!z.opener)z.opener=self}if(window.focus){z.focus()}}function encodeUrl(a){if(a.indexOf("?")>0){encodedParams="?";parts=a.split("?");params=parts[1].split("&");for(i=0;i<params.length;i++){if(i>0){encodedParams+="&"}if(params[i].indexOf("=")>0){p=params[i].split("=");encodedParams+=(p[0]+"="+escape(encodeURI(p[1])))}else{encodedParams+=params[i]}}a=parts[0]+encodedParams}return a}*/
