String.prototype.replaceAll = function(
 strTarget, // The substring you want to replace
 strSubString // The string you want to replace in.
 ){
 var strText = this;
 var intIndexOfMatch = strText.indexOf( strTarget );
  
 // Keep looping while an instance of the target string
 // still exists in the string.
 while (intIndexOfMatch != -1){
 // Relace out the current instance.
 strText = strText.replace( strTarget, strSubString )
  
 // Get the index of any next matching substring.
 intIndexOfMatch = strText.indexOf( strTarget );
 }
  
 // Return the updated string with ALL the target strings
 // replaced out with the new substring.
 return( strText );
 }

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function keywords()
{
	var a=document.keywordsform.search.value;

	document.keywordsform.submitted.value='yes';
	a = a.replaceAll( " ", "+" )
	MM_goToURL('parent','/tag/'+a+'.html');
	return false;
}
