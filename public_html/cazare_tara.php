<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii_cazarii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
$tara=desfa_link($_REQUEST['tari']);
$id_tari=get_id_tara($tara);
if($_REQUEST['tip']) { $tip=desfa_link($_REQUEST['tip']);
$id_tip=get_id_tip_sejur($tip);
$id_tari=get_id_tara($tara);
if(!$id_tip || !$id_tari) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}
$tip_fii=get_id_tip_sejur_fii($id_tip);
$iduri="'".$id_tip."'"; if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri'];
} elseif(!$id_tari) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php");
$parametru['pagina']='new_sejururi1';
$new_formula_key = new new_formula_keywords_new($parametru);
$keyword_nice = $new_formula_key->do_nice_cazare2_new(); ?>
<title><?php echo $keyword_nice[1];?></title>
<meta name="description" content="<?php echo $keyword_nice[0]; ?>" />
<meta name="keywords" content="<?php echo $keyword_nice[2];?>" />
<?php
$link="http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI']; ?>
<meta name="Publisher" content="ocaziituristice.ro" />
<meta name="ROBOTS" content="INDEX,FOLLOW" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
  <table class="mainTableClass" border="0" cellspacing="0" cellpadding="0">
	<tr>
      <td colspan="2" align="left" valign="top">
        <div class="breadcrumb">
         <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
        </div>
      </td>
    </tr>
	<tr>
	  <td class="mainTableColumnLeft" align="left" valign="top">
      <div id="columnLeft">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/cazari/cazare_tara.php"); ?>
      </div>
      </td>
	  <td class="mainTableColumnRight" align="left" valign="top">
        <div id="columnRight">
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta.php"); ?>
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_tip_cazare.php"); ?>
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_main_reclama.php"); ?>
        </div>
      </td>
	</tr>
  </table>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>