<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$den_tara = 'Egipt';
$id_tara = get_id_tara($den_tara);

$link_tara = fa_link_oferta($den_tara);

if(str_replace("/sejur-","",$_SERVER['REQUEST_URI'])==$link_tara."/") $img_path = 'images/';
	else $img_path = '/sejur-'.$link_tara.'/images/';

$sel_dest = "SELECT
MIN(oferte.pret_minim / oferte.nr_nopti) AS pret_minim,
COUNT(distinct oferte.id_hotel) AS numar,
zone.id_zona,
zone.denumire AS denumire_zona,
zone.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '".$id_tara."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_zona
ORDER BY numar DESC, denumire_zona ASC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['numar_hoteluri'][] = $row_dest['numar'];
	$destinatie['link'][] = '/sejur-'.fa_link($den_tara).'/'.fa_link($row_dest['denumire_zona']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$row_dest['id_zona'].'/';
	$destinatie['id_zona'][] = $row_dest['id_zona'];
	$destinatie['info_scurte'][] = $row_dest['info_scurte'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
} @mysql_free_result($rez_dest);
//$destinatie['den_zona'][]="Seniori Egipt";
//$destinatie['den_zona'][]="Circuite in Egipt";
//$destinatie['link'][] = '/oferte-program-pentru-seniori/egipt/';

//$destinatie['link'][] = '/circuite/egipt/';


?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Sejur Egipt  | Ocaziituristice.ro</title>
<meta name="description" content="Sejur Turcia la cele mai luxoase si ravnite hoteluri de pe litoralul Turciei la preturi foarte avantajoase cu all inclusive" />
<link rel="canonical" href="<?php echo '/sejur-'.$link_tara.'/'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body >
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Sejur Egipt 2015-2016</h1>
      <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?>
  
        
        
             <br class="clear">
       <h2 class="green" align="center">Comparam preturile pentru <span class="textatentie">&nbsp;Egipt &nbsp;</span> de la TourOperatori si va oferim <span class="textatentie">cel mai bun Pret!</span> </h2> 
        
        
        <br class="clear">
   <h2 class="blue" align="center">Pachetele pentru Egipt contin Cazare + Transport + Trasfer si au Toate taxele Incluse</h2>     
        <?php /*?><div class="Hline"></div>
        
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
        
        <br class="clear"><?php */?>
        
		<?php $tipuri = get_tiputi_oferte('', '', '', $id_tara, '', '');
		$link_new = $link_tara.'/'; ?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/sejur-'.$link_new; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		if(sizeof($tipuri)>0) {
			foreach($tipuri as $key_t=>$value_t) {
				$i++;
				$valTip = fa_link($value_t['denumire']);
				if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire']; ?></a></li>
		<?php }
		} ?>
                
          <li><a href="<?php echo '/sejur-'.$link_tara.'/?optiuni=da&masa=all-inclusive'; ?>" title="<?php echo $den_tara; ?> all inclusive"><?php echo $den_tara; ?> all inclusive</a></li>
          <li><a href="<?php echo '/sejur-'.$link_tara.'/?optiuni=da&stele=5'; ?>" title="Hoteluri de 5 stele din <?php echo $den_tara; ?>">Hoteluri <?php echo $den_tara; ?> 5*</a></li>
        </ul>

        <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie['den_zona'] as $key1 => $value1) {
			  if($value1 != 'Istanbul') { ?>
          <div class="destItem NEW-round4px clearfix" onclick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
            <div class="topBox">
              <img src="<?php echo $img_path.fa_link($destinatie['den_zona'][$key1]); ?>.jpg" alt="<?php echo $destinatie['den_zona'][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie['den_zona'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?><div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><?php } ?>
            <div class="bottomBox clearfix">
            
              <div class="price" align="center"><span class="red">

              
              
              </span>  </div>
              <div  class="clearfix" align="center" ><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Sejur <?php echo $destinatie['den_loc'][$key1]; ?>" class="link-blue block">Oferte <strong><?php echo $destinatie['den_zona'][$key1] ?> </strong></a></div>
             <?php /*?>  
              <div class="details">
             <a href="<?php echo $destinatie['link_noindex'][$key1]; ?>" rel="nofollow noindex"><img src="/images/but_nou_detalii.png" alt="detalii" width="90"></a>
              </div><?php */?>
            </div>
          </div>
          <?php }
		  } ?>
        </div>
        <?php } ?>
        
        <br>
        
        <div class="clearfix">
        </div>
		<div class="pad10"> <a href="http://www.ocaziituristice.ro/vremea-in-hurghada_6.html"><img src="<?php echo $img_path; ?>temperatura-hurghada.jpg" alt="Temperatura Hurghada" class="float-left" style="margin:10px 10px 0 0;"></a>
        
        </div>
            <br class="clear">
  <div class="pad10"> <a href="http://www.ocaziituristice.ro/vremea-in-sharm-el-sheikh_6.html"><img src="<?php echo $img_path; ?>temperatura-sharm-el-sheikh.jpg" alt="Temperatura Sharm El Sheikh" class="float-left" style="margin:10px 10px 0 0;"></a>
        
        </div>      
        <br class="clear">
       <div class="pad10" align="center">
       
       <h2>Harta Egipt</h2>

<img src="images/harta-egipt.jpg" alt="Harta Egipt" usemap="#Map" style="margin:10px 10px 0 0;">
<map name="Map">
  <area shape="rect" coords="209,80,323,161" href="http://www.ocaziituristice.ro/sejur-egipt/sharm-el-sheikh/" alt="Sharm El Sheik">
  <area shape="rect" coords="198,195,328,274" href="http://www.ocaziituristice.ro/sejur-egipt/hurghada/" alt="Hurghada">
</map>

       
       </div>
        
        <br class="clear">
        
        
        <div class="pad10">
          <img src="<?php echo $img_path; ?>icon-sejur-egipt.jpg" alt="Sejur Egipt" class="float-left" style="margin:10px 10px 0 0;">
          <h2><strong>Sejur Egipt</strong> - vacanţa perfectă </h2>
         
        </div>

        <br class="clear">
        
       <?php /*?> <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <h3 class="mar10"><?php echo $den_tara; ?> - sejururi și cazări disponibile</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie['den_zona'] as $key2 => $value2) {
			  $part_anchor = 'Sejur ';
		  ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie['link'][$key2]; ?>" title="<?php echo $part_anchor.$value2; ?>" class="link-blue block"><?php echo $part_anchor.$value2; ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?><?php */?>
        
        <br class="clear"><br><br>
        
        <div class="pad10"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?></div>
        
        <br class="clear"><br>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>