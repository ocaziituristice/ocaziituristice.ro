<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
$meta_index = "noindex,follow";
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Termeni si conditii <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Termeni si conditii Ocazii Turistice" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <h1 class="blue" style="float:left;">Termeni şi Condiţii de utlizare a site-ului</h1>
        
        <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>

        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="text-justify pad20 article">
          <h2>Condiţii generale</h2>
          <p>Dream Voyage International SRL cu sediu in Bd-ul Gh. Magheru, Nr. 28-30, Et. 5, Cam 517, Sector 1, București, Nr.ord.Reg.Com.: J40/12651/2011, Cod de înregistrare fiscală: 29259993, Banca: Transilvania - Ag. Piața Romană, IBAN: RO93BTRLRONCRT0248462901 este proprietarul/administratorul websiteului www.ocaziituristice.ro</p>
          <p>Dream Voyage International SRL, denumită în cele ce urmează şi Ocazii Turistice, îşi rezervă dreptul de a schimba şi actualiza în orice moment conţinutul acestui website, precum şi Termenii şi condiţiile de folosire, fără nici un fel de notificare prealabilă. De aceea, vă rugăm vizitaţi periodic această secţiune pentru a verifica termenii şi condiţiile pe care aţi agreat să le respectaţi.</p>
          <p>Ocazii Turistice deţine dreptul de autor asupra întregului conţinut al site-ului. Pentru raportări asupra conținutului asupra căruia se consideră că Ocazii Turistice nu deţine drepturi de autor, se va folosi adresa de email <strong>info@ocaziituristice.ro</strong>.</p>
          <p>Orice folosire a materialelor Ocazii Turistice, incluzând, dar fără a se limita la reproducere sau distribuţie, copiere, download/descărcare de fişiere în orice reţea, intranet sau Internet, este strict interzisă fără permisiunea scrisă a Ocazii Turistice.</p>
          <p>Acest website poate conţine documente, imagini, informaţii sau alte materiale care nu sunt proprietatea Ocazii Turistice, cum ar fi fotografii, clip art, sau numele, logouri şi alte mărci ale unor terţi, care sunt proprietatea respectivelor părţi. Ele pot include articole, date, informaţii, imagini, artwork, grafică, fişiere multimedia, elemente audio sau alte materiale publicate în ziare, reviste sau alte tipuri de media şi pot de asemenea include numele sau mărcile respectivelor canale de media. Orice folosire a acestor materiale este strict interzisă fără permisiunea respectivilor terţi proprietari.</p>
          <p>Dream Voyage International SRL îşi rezervă dreptul de a modifica în orice moment conţinutul şi structura website-ului. Dream Voyage International SRL nu garantează că informaţia de pe Site (incluzând, dar fără limitare, tarifele, descrierile sau datele) nu poate avea erori sau lipsuri, dar vom depune tot efortul să corectăm erorile de îndată ce ne sunt aduse la cunoştinţă.</p>
          <br />
          <h2>Cum cumpăr?</h2>
		  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/info_cum_cumpar.php"); ?>
          <h2>Cum plătesc?</h2>
		  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/info_cum_platesc.php"); ?>
          <h2>Politica de returnare a produselor</h2>
          <p>Conform Art. 12 din OG 130/2000, Ocazii Turistice nu are o politică de returnare a produselor.</p>
          <br />
          <h2>Condiții de garanție / service</h2>
          <p>Înainte de a lansa comanda, clientul trebuie să fie de acord cu "Termenii și condițiile" Ocazii Turistice. Existând diferențe între tipurile de servicii oferite,  hoteluri, city-break-uri, asigurări medicale de călătorie, rezervarea nu se poate efectua până la capăt dacă clientul nu bifează căsuţa aferentă acceptării "Termenilor și condițiilor". Conform Art. 12 din OG 130/2000, condițiile de garanție / service nu se aplică serviciilor rezervate pe site-ul www.ocaziituristice.ro<a href="http://www.vola.ro/"></a>. Art. 12 din OG 130/2000 precizează: "Art. 3-4, ale art. 7-10 şi ale art. 11 alin. (1) nu se aplică contractelor privind:</p>
          <p>a) vânzarea de produse alimentare, de băuturi sau de produse de uz curent utilizate în gospodărie, livrate cu regularitate de către comerciant la domiciliul, reședința sau la locul de muncă al consumatorului;</p>
          <p>b) furnizarea de servicii de cazare, de transport, de furnizare de preparate culinare, de agrement, atunci când comerciantul se angajează prin contractul încheiat să efectueze aceste prestații la o dată precisă sau într-o perioadă specificată; în mod excepțional, în cazul activităților de agrement organizate în aer liber, comerciantul își poate rezerva dreptul de a nu respecta prevederile art. 11 alin. (2), în circumstanțe specifice."</p>
          <br />
          <h2>Reclamaţii</h2>
          <p>Problema survenită în timpul șederii turistului, trebuie comunicată de urgență către Ocazii Turistice pentru a o putea rezolva în timp util.</p>
          <p>Pentru rezolvarea cât mai rapidă, eficientă și pe cale amiabilă a eventualelor probleme apărute, vă rugăm să trimiteți mesajele dumneavoastră în atenția domnului <strong>Răzvan Drăghici</strong>, la adresa <strong>razvan@ocaziituristice.ro</strong>, tel: <strong>0758.015.209</strong>.</p>
<br />
          <h2>Garanţia returnării banilor</h2>
          <p>Orice pachet turistic sau de servicii turistice achiziţionat de pe site-ul nostru, sau direct de la sediul agenţiei, beneficiază de <strong>Garanţia Returnării Banilor</strong>. Prin această garanţie vă asigurăm că vă vom returna <strong>integral</strong> suma plătită de dvs. Aveţi însă grijă să vă încadraţi în termenele stipulate în contractul încheiat. Fiecare hotel sau furnizor are o politică diferită de anulare, astfel că termenele de returnare integrală a banilor diferă, deasemeni şi termenele limită de penalizare cu 100% din întreaga sumă.</p>
          <br />
          <h2>Obligaţiile utilizatorului</h2>
          <p>Sunteţi de acord să respectaţi următoarele obligaţii incluzând (fără a se limita):</p>
          <ul>
            <li>responsabilitatea financiară pentru toate tranzacţiile efectuate în conţul sau în numele dvs.</li>
            <li>aveţi vârsta peste 18 ani şi capacitatea legală de a iniţia acţiuni juridice</li>
            <li>garantarea veridicităţii datelor furnizate despre dvs. sau membrii familiei dvs.</li>
            <li>neutilizarea Site-ului în scopuri speculative, a generării de rezervări false sau frauduloase</li>
            <li>interzicerea transmiterii materialelor politice, pornografice, rasiste sau a altor materiale care contravin legii</li>
            <li>obligativitatea de a nu altera, copia, transmite, distribui, vinde, afişa, licenţia sau reproduce conţinutul Site-ului cu excepţia utilizării în scop personal şi necomercial a unei singure copii a informaţiei conţinută în Site</li>
          </ul>
          <h2>Informaţii privind protecţia datelor personale</h2>
          <p>Conform cerinţelor Legii nr. 677/2001 pentru protecţia persoanelor cu privire la prelucrarea datelor cu caracter personal şi liberă circulaţie a acestor date, modificată şi completată Dream Voyage International SRL are obligaţia de a administra în condiţii de siguranţă şi numai pentru scopurile specificate, datele personale pe care ni le furnizaţi despre dumneavoastră, un membru al familiei dumneavoastră ori o altă persoană. Scopul colectării datelor este: promovarea de servicii şi produse adresate direct clienţilor persoane fizice şi promovării ofertelor prin mijloace de marketing direct.</p>
          <p>Dream Voyage International SRL este <?php echo $contact_protectia_datelor; ?> <a href="<?php echo $sitepath; ?>files/protectia_datelor.jpg" title="Protectia Datelor" target="_blank" rel="nofollow"><img src="<?php echo $imgpath; ?>/icon_external_link.png" alt="" /></a> .</p>
          <p>Sunteţi obligat(ă) să furnizaţi datele, acestea fiind necesare procesării solicitării dumneavoastră. Refuzul dumneavoastră determină imposibilitatea concretizării serviciului solicitat.</p>
          <p>Informaţiile înregistrate sunt destinate utilizării de către operator şi sunt comunicate numai firmelor partenere prin care se realizează serviciul solicitat.</p>
          <p>Conform Legii nr. 677/2001, beneficiaţi de dreptul de acces, de intervenţie asupra datelor, dreptul de a nu fi supus unei decizii individuale şi dreptul de a vă adresa justiţiei. Totodată, aveţi dreptul să vă opuneţi prelucrării datelor personale care vă privesc şi să solicitaţi ştergerea datelor(*). Pentru exercitarea acestor drepturi, vă puteţi adresa cu o cerere scrisă, datată şi semnată către Dream Voyage International SRL.</p>
          <p>De asemenea, vă este recunoscut dreptul de a vă adresa justiţiei. Dacă unele din datele despre dumneavoastră sunt incorecte, vă rugăm să ne informaţi cât mai curând posibil.</p>
          <p>Orice litigiu apărut între clienţi şi Dream Voyage International SRL  va fi rezolvat pe cale amiabilă. În cazul în care nu s-a reuşit stingerea conflictului pe cale amiabilă, competenţa revine instanţelor de judecată române.</p>
          <p><em><strong>Notă (*)</strong><br />
          Orice persoană are dreptul de a se opune, pentru motive legitime, la prelucrarea datelor ce o privesc. Acest drept de opoziţie poate fi exclus pentru anumite prelucrări prevăzute de lege (de ex.: prelucrări efectuate de serviciile financiare şi fiscale, de poliţie, justiţie, securitate socială). Prin urmare, această menţiune nu poate figura dacă prelucrarea are un caracter obligatoriu; orice persoană are, de asemenea, dreptul de a se opune, în mod gratuit şi fără nicio justificare, la prelucrările datelor sale personale în scopuri de marketing direct.</em></p>
        </div>
        
      </div>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>