<?php session_start();
ob_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>jQuery Autocomplete Plugin</title>
<script type="text/javascript" src="jquery.js"></script>
<script type='text/javascript' src='jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />
<script type="text/javascript">
$().ready(function() {
	$("#course").autocomplete("get_course_list.php", {
		width: 260,
		//matchContains: true,
		mustMatch: true,
		minChars: 3,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
</head>
<body>
<h2 id="banner">Ajax Autocomplete Demo 
- Jquery, PHP, Mysql</h2>
<div>
<div id="content">
	<form name="auto" method="post" action="redirect.php" autocomplete="off">
		<p>
			Course Name <label>:</label>
			<input type="text" name="course" id="course" />
			<input type="button" value="Get Value" onClick="document.auto.submit();" />
            <div id="redir">&nbsp;</div>
	</form>
</div>
</p>
</body>
</html>
