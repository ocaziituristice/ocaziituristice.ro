<?php //error_reporting(E_ALL);
//ini_set('display_errors', 1);?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$link_new = '/oferte-turism-extern/';

$sel_dest = "SELECT

COUNT(oferte.id_oferta) AS numar,
tari.denumire,
tari.id_tara,
tari.descriere_scurta
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND tari.id_tara <> '1'
AND hoteluri.tip_unitate <> 'Circuit'
AND oferte.last_minute = 'nu'
GROUP BY tari.id_tara
ORDER BY numar DESC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_tara'][] = $row_dest['denumire'];
	$destinatie['numar'][] = $row_dest['numar'];
	$destinatie['link'][] = '/sejur-'.fa_link($row_dest['denumire']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$row_dest['id_tara'].'/';
	$destinatie['id_tara'][] = $row_dest['id_tara'];
	$destinatie['info_scurte'][] = $row_dest['descriere_scurta'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
} @mysql_free_result($rez_dest);

?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte turism extern <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Oferte turism extern - vacante in stainatate, oferte de la tour operatori " />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/header/header_responsive.php"); ?>
</head>
<body>
 <header>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/breadcrumb_responsive.php"); ?>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/search_responsive_gol.php"); ?>
    </div>
<script src="<?php echo PATH_JS?>/jquery-ui.min.js"></script>

<div class="layout">

    <div id="middle">

        <div id="middleInner">

            <div class="NEW-column-full">

                <div id="NEW-destinatie" class="clearfix">

                    <h1 class="blue" style="float:left;">Oferte turism extern - Vacante in strainatate </h1>

                    <div class="float-right">

                        <?php /*?><div class="fb-share-button" data-href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/;src=sdkpreparse">Distribuie</a></div>
<?php */?>
                    </div>

                    <br class="clear" />


                    

                        <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">

                                <div class="NEW-search-wide">

                                    <div class="chapter-title blue">Caută Oferte Turism Extern:</div>

                                    <div id="afis_filtru"></div>

                                </div>

                            </div></div>

                        <br class="clear">

                    
    <?php
		$tip = "SELECT
		tip_oferta.denumire_tip_oferta
		FROM oferte
		INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		WHERE oferte.valabila = 'Da'
		AND hoteluri.tip_unitate <> 'Circuit'
		AND tip_oferta.activ = 'da'
		AND tip_oferta.apare_site = 'da'
		AND tari.id_tara <> '1'
		AND tip_oferta.tip IS NOT NULL
		AND oferte.last_minute = 'nu'
		GROUP BY tip_oferta.denumire_tip_oferta
		ORDER BY tip_oferta.ordine ASC, tip_oferta.denumire_tip_oferta ASC ";
		$que_tip = mysql_query($tip) or die(mysql_error());
		if(mysql_num_rows($que_tip)>0) {
		?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo $link_new; ?>" class="sel">Toate</a></li>
		<?php while($row_tip = mysql_fetch_array($que_tip)) { ?>
          <li><a href="<?php echo '/oferte-'.fa_link($row_tip['denumire_tip_oferta']).'/'; ?>" title="<?php echo $row_tip['denumire_tip_oferta']; ?>"><?php echo $row_tip['denumire_tip_oferta']; ?></a></li>
		<?php } ?>
        </ul>
        <?php } ?>                
                    
                    
        <?php if(sizeof($destinatie['den_tara'])>0) { ?>
 <div id="destinations"  class="clearfix">
 
               <?php foreach($destinatie['den_tara'] as $key1 => $value1) { ?>

                        <div class="destItem NEW-round4px clearfix" >

                            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">

                                <img src="/images/tari-mari/<?php echo fa_link($destinatie['den_tara'][$key1]); ?>.jpg" alt=" <?php echo $destinatie['den_tara'][$key1]; ?>">

                                <span class="over"></span>

                                <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>

                                <h2> <?php echo $destinatie['den_tara'][$key1]; ?></h2>

                            </div>

                            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><div class="bottomBox clearfix">

                                <div class="price" align="center"><span class="red"><span class="pret"><?php echo $destinatie['numar'][$key1]?></span> </span> Oferte <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte  <?php echo $destinatie['den_tara'][$key1]; ?>  ">

			 <?php echo $destinatie['den_tara'][$key1]; ?></a></span></span></div>

                            </div>

                        </div>
<?php }?>    

                    </div>

<?php }?>

                    <br class="clear"><br>

                   

                </div>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    $("#afis_filtru").load("/includes/search/filtru.php?turism-extern");

</script>



<script src="/js/ajaxpage.js"></script>

<script src="/js/jquery.rating.js"></script>

<script>

    $('input.rating').rating({

        callback: function(value) {

            var dataString = 'rating=' + value;

            $.ajax({

                type: "POST",

                url: "/voting/////",

                data:dataString,

                success: function(data) {

                    $('#rating').html(data);

                }

            })

        },required:'hidden'

    });

</script>



<script src="/js/ocazii_main.js"></script>

<script>document.oncopy = addLink;</script>








<script>

    $().ready(function() {

        $("#data_start").datepicker({

            numberOfMonths: 3,

            dateFormat: "yy-mm-dd",

            showButtonPanel: true

        });

        $("#data_end").datepicker({

            numberOfMonths: 3,

            dateFormat: "yy-mm-dd",

            showButtonPanel: true

        });

    });

</script>



<!--<script src="/js/jquery.masonry.min.js"></script>-->

<script>

    $(window).load(function(){

        $('#destinations').masonry({

            itemSelector: '.destItem'

        });

    });

</script>













<script>

    $(window).bind("load", function() {

        $.getScript('/js/ocazii_lateload.js', function() {});

    });

</script>

<!-- Google Code for Remarketing Tag -->

<!--------------------------------------------------

Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup

--------------------------------------------------->









<script type="text/javascript">


    $(document).ready(function() {

// Tooltip only Text

        $('.masterTooltip').hover(function(){

            // Hover over code

            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<p class="tooltip"></p>')

                .text(title)

                .appendTo('body')

                .fadeIn('slow');

        }, function() {

            // Hover out code

            $(this).attr('title', $(this).data('tipText'));

            $('.tooltip').remove();

        }).mousemove(function(e) {

            var mousex = e.pageX + 20; //Get X coordinates

            var mousey = e.pageY + 10; //Get Y coordinates

            $('.tooltip')

                .css({ top: mousey, left: mousex })

        });



    });

    const mq = window.matchMedia( "(max-width: 768px)" );



    if(mq){



        $('.topBox').each(function(){

            // Cache event

            var existing_event = this.onclick;



            // Remove the event from the link

            this.onclick = null;



            // Add a check in for the class disabled

//            $(this).click(function(e){

//                $(this).addClass('active_img');

//

//                if($(this).hasClass('active_img')){

//                    e.stopImmediatePropagation();

//                    e.preventDefault();

//                }

//

//            });



            // Reattach your original onclick, but now in the correct order

            // if it was set in the first place

            if(existing_event) $(this).click(existing_event);

        });

    }





</script>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php"); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php"); ?>

