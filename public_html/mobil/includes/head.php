<!DOCTYPE html>
<html lang="ro">
<head>
	<meta charset="UTF-8" />
	<title>Agentie de turism online - disponibilitate in timp real | Ocaziituristice.ro</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<link rel="shortcut icon" href="<?php echo PATH_IMAGES; ?>favicon.ico?<?php echo CACHE; ?>" />
	<link href="css/ocaziituristice.css" rel="stylesheet" type="text/css" />

	<script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/ocaziituristice.js"></script>

	<?php if(basename($_SERVER['PHP_SELF'])=='hotel.php'): ?>
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key_map ?>&language=ro"></script>
	<?php endif; ?>
</head>

<body>