<section class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="m-title">Oferte speciale la reducere<br><em>Abonati-va la promotiile noastre</em></h2>
            </div>

            <form class="form-inline" name="abonare_newsletter_form" id="abonare_newsletter_form">
                <div class="form-group col-sm-3">
                    <input class="form-control" name="prenume" type="text" required="" placeholder="Prenume">
                </div>
                <div class="form-group col-sm-3">
                    <input class="form-control" name="nume" type="text" required="" placeholder="Nume">
                </div>
                <div class="form-group col-sm-3">
                    <input class="form-control" name="email" type="email" required="" placeholder="Email">
                </div>
                <div class="form-group col-sm-3">
                    <input type="submit" value="Abonare" class="btn btn-success" style="width: 100%" onclick="return submitForm()">
                </div>
            </form>
        </div>
    </div>
</section>