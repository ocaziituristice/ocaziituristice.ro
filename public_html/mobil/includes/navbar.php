<div class="navbar-bg">
    <div class="container">
        <nav class="navbar navbar-toggleable-md navbar-light">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand hidden-lg-up" href="#">
                <img src="images/logo.png" width="200" alt="">
            </a>
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Litoral România 2016</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Sejur România</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Stațiuni Balneare</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Turism Extern</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Circuite</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Seniori</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-volume-control-phone"></i> Contactaza-ne</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>