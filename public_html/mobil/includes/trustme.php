                <ul class="trustme-box">
	<li>
		<i class="fa fa-volume-control-phone"></i>
		<p>
			<strong>Rezervări online sau telefonice</strong>
			<a href="tel:+40213117464" class="phone">021.311.7464</a>
		</p>
	</li>

	<li>
		<i class="fa fa-thumbs-up"></i>
		<p><strong>Siguranță</strong>. Avem parteneri pe cei mai mari tour-operatori din țară și din afară (<a href="#">vezi &raquo;</a>)</p>
	</li>

	<li>
		<i class="fa fa-thumbs-up"></i>
		<p><strong>Transparență</strong>. Afișăm <strong>toate detaliile</strong> necesare la fiecare ofertă. <strong>Fără taxe ascunse</strong>!</p>
	</li>

	<li>
		<i class="fa fa-thumbs-up"></i>
		<p>Multiple metode de plată (<a href="#">vezi &raquo;</a>) <strong>Comisioane bancare <span class="text-danger">ZERO!</span></strong></p>
	</li>

	<li>
		<i class="fa fa-thumbs-up"></i>
		<p>Afișăm disponibilitatea locurilor de cazare și în avion <strong class="text-danger">ÎN TIMP REAL</strong>!</p>
	</li>

	<li>
		<i class="fa fa-thumbs-up"></i>
		<p>Capital Social al Agenției de Turism <strong>50.000 Lei</strong>!<br><a href="#">Licență de turism valabila</a></p>
	</li>
</ul>







