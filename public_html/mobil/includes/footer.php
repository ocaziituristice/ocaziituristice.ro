<footer class="footer">
	<div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4">
                <h2>Ocaziituristice.ro</h2>
                <ul class="triangle">
                    <li><a href="#">Prima pagina</a></li>
                    <li><a href="#">Despre noi</a></li>
                    <li><a href="#">Termeni si Conditii</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">ANPC - 0219551</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">
                <h2>Utile</h2>
                <ul class="triangle">
                    <li><a href="#">Cum rezolv</a></li>
                    <li><a href="#">Cum platesc</a></li>
                    <li><a href="#">Despre cookie-uri</a></li>
                    <li><a href="#">Asigurari de calatorie</a></li>
                    <li><a href="#">Regimul de vize</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">
                <h2>Curs valutar BNR</h2>
                <div>
                    <h6>06.01.2017</h6>
                    <h6>1 EURO = 4.4973 Lei</h6>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <h2>Ne gasiti pe</h2>
                <a href=""><i id="facebook-icon" class="fa fa-facebook-square fa-3x social-icon"></i></a>
	            <a href=""><i id="google-icon" class="fa fa-google-plus-square fa-3x social-icon"></i></a>
                <a href=""><i id="twitter-icon" class="fa fa-twitter-square fa-3x social-icon"></i></a>
	            <a href=""><i id="pinterest-icon" class="fa fa-pinterest-square fa-3x social-icon"></i></a>
	            <a href=""><i id="rss-icon" class="fa fa-rss-square fa-3x social-icon"></i></a>
                <address class="contact-details">
                    <span class="contact-phone"><i class="fa fa-phone"></i> 021.311.7464</span>
                    <span class="contact-address"><i class="fa fa-map-marker"></i> Bd-ul Gh. Magheru, Nr. 28-30, Sector 1, 010336, Bucuresti</span>
                </address>
            </div>
        </div>

        <div class="copyright">
            <span>Copyright © 2008 - 2017 Agentia de turism <a href="">www.ocaziituristice.ro.</a></span>
            <div class="pull-right">
                <img src="images/teztour-logo.gif" alt="Tez Tour">
                <img src="images/mobilepay-footer.png" alt="Plati prin Mobile Pay">
            </div>
        </div>
	</div>
</footer>
<!-- END FOOTER SECTION -->


<div class="modal-overlay"></div>

<script>
$(document).ready(function(){
  var windowWidth = $(window).width();
  if(windowWidth <= 1024)
     $('.panel-collapse').removeClass('show')
});
</script>

</body>

</html>