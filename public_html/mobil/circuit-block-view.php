<?php require_once("includes/head.php"); ?>
<?php require_once("includes/top-bar.php"); ?>
<?php require_once("includes/header.php"); ?>
<?php require_once("includes/navbar.php"); ?>

<section id="content">
    <div class="container">

        <?php require_once("includes/breadcrumb.php"); ?>

        <div class="heading">
            <h1>Circuite 2016</h1>
        </div>

        <div class="card-columns circuit">
            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/israel.jpg" alt="">
                    <div class="overlay">
                        <h2>Israel</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 Eur</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>
            
            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/italia.jpg" alt="">
                    <div class="overlay">
                        <h2>Italia</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/spania.jpg" alt="">
                    <div class="overlay">
                        <h2>Spania</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/elvetia.jpg" alt="">
                    <div class="overlay">
                        <h2>Elvetia</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/israel.jpg" alt="">
                    <div class="overlay">
                        <h2>Israel</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 Eur</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/italia.jpg" alt="">
                    <div class="overlay">
                        <h2>Italia</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/spania.jpg" alt="">
                    <div class="overlay">
                        <h2>Spania</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/elvetia.jpg" alt="">
                    <div class="overlay">
                        <h2>Elvetia</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/israel.jpg" alt="">
                    <div class="overlay">
                        <h2>Israel</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 Eur</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/italia.jpg" alt="">
                    <div class="overlay">
                        <h2>Italia</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/spania.jpg" alt="">
                    <div class="overlay">
                        <h2>Spania</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>

            <div class="card block-view">
                <div class="hovereffect">
                    <img class="card-img-top" src="images/elvetia.jpg" alt="">
                    <div class="overlay">
                        <h2>Elvetia</h2>
                        <a class="info" href="#">Vezi detalii</a>
                    </div>
                </div>
                <div class="card-block">
                    <label class="description">Regiunea datează din timpul fenicienilor si a fost locuita de greci, romani și mauri, toți aceștia punandu-si amprenta asupra arhitecturii și culturii din zona. Cu o gamă largă de facilități și atracții turistice, Costa del Sol ofera pentru toate gusturile si varstele ceva care sa incante.</label>
                    <div class="card-footer">
                        <label class="price-wrapper">de la <span class="price">169 EUR</span> / pers</label>
                        <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-desc">
            <img class="pull-left" src="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/images/icon-programe-seniori.jpg" style="margin: 5px 10px 0 0;" alt="">
            <h5>Programe pentru Seniori în Europa</h5>
            <p>Sejururi atractive, la preţuri pentru pensionari, pentru relaxare după o viaţă de muncă. Seniorul poate fi însoţit de întreaga familie de juniori, la aceleaşi preţuri. Cele mai numeroase şi deosebit de bine organizate sunt ofertele pentru Spania (Mallorca, Costa del Sol, Costa Brava) si Cipru (Limassol, Larnaca). Sfatul nostru este: invitaţi un senior din familie şi însoţiţi-l într-o astfel de călătorie.</p>
            <p>Ofertele pentru programele turistice pentru seniori, au început să se răspândească în întreaga lume! Beneficiază acum de ofertele de seniori pentru Israel, şi vizitează locurile sfinte.</p>
            
            <h5 class="text-primary">Informaţii şi condiţii generale pentru Programele pentru Seniori</h5>
            <p><b>Programul pentru Seniori</b> se adresează cetăţenilor europeni de <b>peste 55 ani</b> (şi nu numai) prin care pot beneficia de un pachet complet de servicii turistice pentru a îşi petrece vacanţa cu <b>reduceri substanţiale</b> în perioada octombrie-mai, putând vizita <i>cele mai cunoscute locuri turistice şi istorice.</i></p>
            <p>Prin această iniţiativă, se oferă tuturor posibilitatea de a descoperi lumea şi frumuseţile ei, aceste tipuri de programe încadrându-se în "<i>turism social</i>".</p>
            <p>În ultima vreme s-au mai adus modificări asupra condiţiilor acestor programe, astfel că, pentru a putea beneficia de aceste super reduceri, <b>cel puţin unul din cei 2 ocupanţi</b> ai unei camere duble trebuie să aibă vârsta <b>minimă de 55 ani</b>.</p>
            <p>Există posibilitatea ca la unele oferte să achiziţionaţi "<i>Senior Voyage Club Card</i>", astfel beneficiind de anumite extra oferte.</p>
        </div>

    </div>
</section>

<?php require_once("includes/newsletter.php"); ?>
<?php require_once("includes/footer.php"); ?>