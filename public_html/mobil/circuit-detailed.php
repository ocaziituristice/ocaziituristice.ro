<?php require_once("includes/head.php"); ?>
<?php require_once("includes/top-bar.php"); ?>
<?php require_once("includes/header.php"); ?>
<?php require_once("includes/navbar.php"); ?>

<section id="content">
    <div class="container">

        <?php require_once("includes/breadcrumb.php"); ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="heading" style="padding-bottom: 10px">
                    <h1>Israel - Pamantul Fagaduintei</h1>
                    <span class="location"><i class="fa fa-map-marker fa-lg"></i> Localitati traversate: Tel Aviv, Haifa, Cana Galileii, Capernaum, Nazareth, TABGHA, Ierusalim</span>
                </div>
            </div>

            <div class="col-sm-8 col-md-9">

                <!--<div id="carousel-circuit" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-circuit" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-circuit" data-slide-to="1"></li>
                        <li data-target="#carousel-circuit" data-slide-to="2"></li>
                    </ol>
  
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img src="http://mapamond.ro/wp-content/uploads/2016/10/is.jpg" alt="">
                        </div>
    
                        <div class="carousel-item">
                            <img src="http://i2.cdn.turner.com/money/dam/assets/131120111651-israel-tech-tel-aviv-1024x576.jpg" alt="">
                        </div>
             
                        <div class="carousel-item">
                            <img src="http://vacanteisrael.ro/wp-content/uploads/2013/04/tel_aviv.jpg" alt="">
                        </div>
                    </div>

                    <a class="left carousel-control" href="#carousel-circuit" role="button" data-slide="prev">
                        <span class="icon-prev"></span>
                    </a>
                  
                    <a class="right carousel-control" href="#carousel-circuit" role="button" data-slide="next">
                        <span class="icon-next"></span>
                    </a>
                </div>-->

                <div class="circuit-content">

                    <div class="price-table">
                        <table class="table">
                            <thead class="thead-default">
                                <tr>
                                    <th>Plecare</th>
                                    <th>Camera dubla</th>
                                    <th class="hidden-md-down">Camera dubla partajata</th>
                                    <th class="hidden-md-down">Camera single</th>
                                </tr>
                            </thead>
                        
                            <tbody>
                                <tr>
                                    <th scope="row">05 mai 2017</th>
                                    <td>950 €</td>
                                    <td class="hidden-md-down">950 €</td>
                                    <td class="hidden-md-down">1209 €</td>
                                </tr>
                                <tr>
                                    <th scope="row">16 iunie 2017</th>
                                    <td>960 €</td>
                                    <td class="hidden-md-down">960 €</td>
                                    <td class="hidden-md-down">1219 €</td>
                                </tr>
                                <tr>
                                    <th scope="row">30 august 2017</th>
                                    <td>970 €</td>
                                    <td class="hidden-md-down">970 €</td>
                                    <td class="hidden-md-down">1229 €</td>
                                </tr>
                            </tbody>
                        </table>
                        <span>* tarifele sunt exprimate in EURO pe sejur/pers</span>
                    </div>

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#price-box" role="tab">Calculati pretul pentru Program Economic - Israel - 1 iunie 2017</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="price-box" role="tabpanel">
                            <div id="pret" class="price-box clearfix">
                                <form>
                                    <div class="form-group col-xs-6 col-md-3">
                                        <label>Data plecarii</label>
                                        <select class="form-control">
                                            <option>05 mai 2017</option>
                                            <option>16 iunie 2017</option>
                                            <option>30 august 2017</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-6 col-md-2">
                                        <label>Adulti</label>
                                        <select class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-6 col-md-2">
                                        <label>Copii</label>
                                        <select class="form-control">
                                            <option>0</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                            
                                    <div class="form-group col-xs-6 col-md-3" style="float: right;">
                                        <a href="" class="btn btn-success btn-block">Vezi pret</a>
                                    </div>
                                </form>
                            </div>

                            <div class="rooms">
                                <div class="row">
                                <div class="col-sm-6 rooms-details">
                                    <div class="float-lg-right">
                                        <img src="https://www.ocaziituristice.ro/images/people_01_m.png" alt="adult">
                                        <img src="https://www.ocaziituristice.ro/images/people_01_m.png" alt="adult">
                                    </div>
                                    <div><b>Camera dubla</b></div>
                                    <span>2 adl</span>
                                    <div>Perioada: <b class="text-primary">23.05 - 04.06.2017</b></div>
                                </div>
                                <div class="col-sm-3">
                                    Tarif/camera<br>
                                    12 nopti
                                    <span>1038 €</span>
                                </div>
                                <div class="col-sm-3">

                                </div>
                                </div>
                            </div>

                            <div class="sidebar-box circuit-sidebar border-box">
                                <div class="title">Tarifele afisate includ</div>
                                <div class="content">
                                    <p>Transport cu autocar climatizat, clasificat 2 - 4</p>
                                    <p>6 cazări cu Mic dejun în hoteluri de 2 - 4*</p>
                                    <p>6 cazări cu Mic dejun şi cină într-o staţiune de pe Costa Brava, la hotel 2,3 şi 4* (în funcţie de varianta aleasă la rezervare)</p>
                                    <p>Excursiile la Padova, Andorra, Genova</p>
                                    <p>Taxe de intrare la obiectivele din program, cu excepția Massada</p>
                                    <p>Ghid însoţitor din partea agenţiei pe traseu; • asistenţă turistică pe Costa Brava</p>
                                    <p>Asistenta turistica in limba romana</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="program">
                        <div class="title"><span class="red">Ziua 1.</span> Bucuresti - Tel Aviv - Mormantul Sf. Gheorghe - Ierusalim - Bethlehem</div>
                        <span>Intalnire la aeroportul Henri Coanda cu reprezentantul agentiei cu 3 ore inainte de plecare. La ora 10:45 decolare din Bucuresti, cu sosire in aeroportul din Tel Aviv la 13:25. Se va incepe programul cu Biserica Sf. Gheorghe - Lod, care adaposteste mormantul Sfantului. Plecare spre Asezamantul Romanesc din Ierusalim - cu hramul Sf. Mare Mucenic Gheorghe. Cina si cazare Bethlehem</span>

                        <div class="title"><span class="red">Ziua 2.</span> Ierusalim - Ein Karem - Muntele Eleon - Bethlehem - Biserica Nasterii Mantuitorului</div> 
                        <span>Mic dejun. Plecare spre Ein Karem - patria Sf. Ioan Botezatorul, unde se viziteaza Biserica Nasterii Sf. Ioan Botezatorul si Izvorul - unde a avut loc intalnirea Maicii Domnului cu Elisabeta. Oprire la Zidul Plangerii. Se continua cu M-tele Eleon la Biserica Ortodoxa a Inaltarii Mantuitorului la Cer, Biserica unde s-a aflat Capul Sf. Ioan Botezatorul, Manastirea Sf. Pelaghia - unde se afla Icoana Mantuitorului facatoare de minuni - Pantocrator, Muntele Maslinilor, Valea Chedronului - Locul Judecatii de Apoi si Poarta de aur. Deplasare spre Bethlehem, unde se viziteaza Biserica Nasterii Mantuitorului, Grota Pruncilor - ucisi in timpul regelui Irod si Biserica Sf. Ecaterina. Optional: Manastirea Sf. Sava cel Sfintit in desertul Iudeii si Manastirea Sf. Teodosie care adaposteste mai multe Sfinte Moaste (10 euro/pers). Cina si cazare Bethlehem.</span>

                        <div class="title"><span class="red">Ziua 3.</span> Ierusalim - Biserica Mormantul Maicii Domnului - Gradina Ghetimani - Drumul Crucii</div>
                        <span>Mic dejun. Plecare spre Ierusalim. Participare la Sf. Liturghie la Mormantul Maicii Domnului. Gradina Ghetimani - locul mult iubit de Domnul, acolo unde obisnuia sa se roage si unde a fost prins. Se va intra pe poarta Sf. Stefan si se parcurge Drumul Crucii cu cele 14 opriri. Ajungand in Cetatea Sfanta, se viziteaza Biserica Sfintei Invieri cu Golgota, Piatra Ungerii, Sfantul Mormant si locul unde s-a aflat Sfanta Cruce. Timp liber la dispozitie pentru rugaciune si pentru eventualele cumparaturi în zona comerciala din imprejurimi. Muntele Sion cu visita la Biserica Adormirii Maicii Domnului, Cina cea de Taina si Mormantul prorocului David. Cina si cazare Bethlehem.</span>

                        <div class="title"><span class="red">Ziua 4.</span> Hozeva - Sf. Ioan Iacob Romanul - Ierihon - Marea Moarta - Manastirea Sf. Gherasim</div>
                        <span>Mic dejun. Optional: Vizita la Manastirea Hozeva din desertul Iudeii, care adaposteste Moastele Sfantului Ioan Iacob Romanul. Ierihon - este considerat cel mai vechi oras din lume si aici se viziteaza Asezamantul Romanesc - unde parintele va face o rugaciune pentru cei care ajung in acest lacas, Biserica Dudul lui Zaheu, Muntele Ispitirii “Carantania”-vedere generala cu oprire la Marea Moarta pentru baie si cumparaturi produse cosmetice. Pesterile de la Qumran - vedere generala din autocar. Plecare spre hotel cu oprire la Manastirea Sfantului Gherasim de la Iordan (45 euro/pers). Cina si cazare Bethlehem.</span>

                        <div class="title"><span class="red">Ziua 5.</span> Raul Iordan - Tiberias - Galileea - Muntele Fericirilor - Capernaum</div>
                        <span>Mic dejun. Se incepe programul cu Raul Sf. Iordan - in care a fost botezat Mantuitorul Isus Hristos, oprire pentru baie si botez simbolic. Tiberias - orasul construit de Irod cel Mare in cinstea Imparatului Tiberiu. Muntele Fericirilor - unde Iisus a rostit frumoasa predică a “Fericirilor”. Tabgha - saturarea celor cinci mii de oameni cu doi pesti si cinci paini, Capernaum - orasul lui Iisus, locul unde Domnul si-a inceput activitatea Sa mesianica. Optional: Croaziera pe Marea Galileii pe urmele Sf. Apostol Petru, unde se va servi pranzul gustand din “ Pestele Sf. Petru” (30 euro/pers.). Oprire la Biserica Ortodoxa cu hramul Sfintilor Apostoli Petru si Pavel - Capernaum. Cina si cazare Tiberias/Nazareth.</span>

                        <div class="title"><span class="red">Ziua 6.</span> Cana Galileii - Nazareth - Muntele Tabor - Haifa - Tel Aviv</div>
                        <span>Mic dejun. Cana Galileii - Biserica din locul primei minuni savarsite de Iisus la nunta lui Simon si transformarea apei in vin. Vizita la Biserica Sf. Fecioare Maria si la Izvorul Maicii Domnului, unde Arhanghelul Gavril i-a adus Maicii Domnului vestea că va naste pe Mesia. Optional: Muntele Tabor - Biserica Schimbarii la Fata, unde Domnul le-a aratat ucenicilor firea Sa Dumnezeiasca (10 euro/pers). Deplasare spre Haifa, unde se pot admira faimoasele gradini Bahai’s. Plecare spre aeroportul din Tel-Aviv, decolare la 18:05 cu sosire in Bucuresti - Henri Coanda la ora 20:55.</span>
                    </div>

                    <div class="alert alert-info">Continutul ofertei este valabil la data introducerii acesteia. Datorita specificului industriei de turism ofertele se pot schimba foarte des si nu pot fi actualizate in timp util.</div>

                </div>

            </div>

            <div class="col-sm-4 col-md-3">

                <div class="sidebar-box circuit-sidebar hidden-md-down">
                    <div class="content" style="padding-top: 0">
                        <div class="info">
                            <span class="list">Cod oferta: <b>OCI24323</b></span>
                            <span class="list">Sejur: <b>5 nopti</b></span>
                            <span class="list">Masa: <b>Demipensiune</b></span>
                            <span class="list">Transport: <b>Avion si Autocar local</b></span>
                        </div>
                        <span class="price clearfix">
                            <small class="pull-left">Tarif de la</small>
                            <span class="pull-right">239 EUR</span>
                        </span>
                        <a href="" class="btn btn-success btn-block">Vezi pret</a>
                    </div>
                </div>

                <div class="sidebar-box circuit-sidebar" style="background-color: rgba(240, 173, 78, 0.2)">
                    <div class="title">Tarifele includ</div>
                    <div class="content">
                        <p><b class="text-success">Taxele de aeroport</b> incluse</p>
                        <p>Asistenţă în aeroport la sosire</p>
                        <p>5 nopţi cazare la hoteluri de 4* și 3*(categoria este valabilă şi aferentă pentru Israel) conform clasificarii</p>
                        <p>Demipensiune (2 mese pe zi - conform programului; intrare cu cină, iesire cu Mic dejun)</p>
                        <p>Taxe de intrare la obiectivele din program, cu excepția Massada</p>
                        <p>Croazieră pe Marea Galileii •transport cu autocar cu aer condiţionat, conform programului</p>
                        <p>Preot însoţitor din partea agentiei și ghid local vorbitor de limba română</p>
                        <p>Transport avion București – Tel Aviv – București, taxe de aeroport</p>
                    </div>
                </div>

                <div class="sidebar-box circuit-sidebar">
                    <div class="title">Neincluse in tarif</div>
                    <div class="content">
                        <p>Slujba de noapte la mormantul sfant - se achita in israel - <span class="text-info">Optional</span></p>
                        <p>Asigurare stornare si sanatate - <b>11 € / sejur / persoana</b> - <span class="text-info">Optional</span></p>
                    </div>
                </div>

                <div class="sidebar-box circuit-sidebar">
                    <div class="title">Orar de zbor</div>
                    <div class="content flight">
                        <div class="flight-img">
                            <img alt="" src="https://www.ocaziituristice.ro/images/avion/29.jpg">
                        </div>
                        <h4 class="flight-title">EL AL Israel Airlines</h4>
                        <p><b>Plecare:</b> Otopeni-Henri Coanda (Bucuresti) - <b>10:45</b></p>
                        <p><b>Sosire:</b> Ben Gurion International Airport (Tel Aviv) - <b>13:25</b></p>
                        <div class="devider"></div>
                        <p><b>Plecare:</b> Ben Gurion International Airport (Tel Aviv) - <b>18:05</b></p>
                        <p><b>Sosire:</b> Otopeni-Henri Coanda (Bucuresti) - <b>22:55</b></p>
                    </div>
                </div>

                <div class="sidebar-box circuit-sidebar">
                    <div class="title">Nota</div>
                    <div class="content">
                        <span class="note">-Documente necesare calatoriei: pasaport valabil cu cel putin 6 luni de la data intoarcerii.</span>
                        <span class="note">-Programul pe zile si ordinea vizitelor se pot modifica, respectand in totalitate vizitele mentionate în program.</span>
                        <span class="note">-Categoria hotelurilor si a mijloacelor de transport este in conformitate cu normele locale.</span>
                        <span class="note">-Persoanele - si pentru care agentia nu gaseste partaj in camera dubla sau tripla, nu vor plati diferenta de single, aceasta fiind suportata de catre agentie.</span>
                        <span class="note">-Serviciile neincluse (taxi, taxe, excursiile optionale) se achita la agentie.</span>
                        <span class="note">-Excursia trebuie achitata integral cu cel putin 21 zile inaintea plecarii. Pentru inscriere se va achita un avans de 30% din valoarea totala a pachetului de servicii.
                    </div>
                </div>

                <div class="list-group hidden-lg-up">
                    <div href="#" class="list-group-item clearfix">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-volume-control-phone text-danger"></i>
                            </div>
                            <div class="media-body">
                                <span class="media-heading"><b>Rezervări online sau telefonice</b></span>
                                <h2><a href="tel:+40213117464" class="text-danger">021.311.7464</a></h2>
                            </div>
                        </div>
                    </div>
                    <div href="#" class="list-group-item clearfix">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-thumbs-up text-success"></i>
                            </div>
                            <div class="media-body">
                                <span class="media-heading"><b>Siguranță.</b> Avem parteneri pe cei mai mari tour-operatori din țară și din afară (<a href="">vezi »</a>)</span>
                            </div>
                        </div>
                    </div>
                    <div href="#" class="list-group-item clearfix">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-thumbs-up text-success"></i>
                            </div>
                            <div class="media-body">
                                <span class="media-heading"><b>Transparență.</b> Afișăm <b>toate detaliile</b> necesare la fiecare ofertă. <b>Fără taxe ascunse!</b></span>
                            </div>
                        </div>
                    </div>
                    <div href="#" class="list-group-item clearfix">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-thumbs-up text-success"></i>
                            </div>
                            <div class="media-body">
                                <span class="media-heading">Multiple metode de plată (<a href="">vezi »</a>) <b>Comisioane bancare <span class="text-danger">ZERO!</span></b></span>
                            </div>
                        </div>
                    </div>
                    <div href="#" class="list-group-item clearfix">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-thumbs-up text-success"></i>
                            </div>
                            <div class="media-body">
                                <span class="media-heading">Afișăm disponibilitatea locurilor de cazare și în avion <b class="text-danger">ÎN TIMP REAL!</b></span>
                            </div>
                        </div>
                    </div>
                    <div href="#" class="list-group-item clearfix">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-thumbs-up text-success"></i>
                            </div>
                            <div class="media-body">
                                <span class="media-heading">Capital Social al Agenției de Turism <b>50.000 Lei!</b> <a href="">Licență de turism valabila</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="#pret" class="btn btn-success btn-lg btn-block hidden-lg-up">Calculati pretul</a>

            </div>

            <div class="col-sm-12">

                <div class="heading">
                    <h2>Clienții care au văzut <span class="text-danger">Israel - Pamantul Fagaduintei</span> au fost interesați și de:</h2>
                </div>
                
                <div class="card-deck-wrapper circuit">
                    <div class="card-deck">
    
                        <div class="card block-view">
                            <img class="card-img-top" src="https://www.ocaziituristice.ro/img_mediu_hotel/8487hotel_the_bay1hotel.jpg" alt="">
                            <div class="card-block">
                                <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                                <h4 class="title">Hotel The Bay</h4>
                                <label class="description">Bucovina / Gura Humorului</label>
                            </div>
                        </div>
            
                        <div class="card block-view">
                            <img class="card-img-top" src="https://www.ocaziituristice.ro/img_mediu_hotel/8487hotel_the_bay1hotel.jpg" alt="">
                            <div class="card-block">
                                <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                                <h4 class="title">Hotel The Bay</h4>
                                <label class="description">Bucovina / Gura Humorului</label>
                            </div>
                        </div>

                        <div class="card block-view">
                            <img class="card-img-top" src="https://www.ocaziituristice.ro/img_mediu_hotel/8487hotel_the_bay1hotel.jpg" alt="">
                            <div class="card-block">
                                <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                                <h4 class="title">Hotel The Bay</h4>
                                <label class="description">Bucovina / Gura Humorului</label>
                            </div>
                        </div>

                        <div class="card block-view">
                            <img class="card-img-top" src="https://www.ocaziituristice.ro/img_mediu_hotel/8487hotel_the_bay1hotel.jpg" alt="">
                            <div class="card-block">
                                <a href="" class="btn btn-info btn-sm pull-right">Detalii</a>
                                <h4 class="title">Hotel The Bay</h4>
                                <label class="description">Bucovina / Gura Humorului</label>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
</section>


<?php require_once("includes/footer.php"); ?>