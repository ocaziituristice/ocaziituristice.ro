<?php require_once("includes/head.php"); ?>
<?php require_once("includes/top-bar.php"); ?>
<?php require_once("includes/header.php"); ?>
<?php require_once("includes/navbar.php"); ?>

<section id="content">
    <div class="container">

        <?php require_once("includes/breadcrumb.php"); ?>

        <div class="row">
            <div class="col-sm-4 col-md-3">

                <div class="filter-container">
                    <div class="panel">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter">Filtreaza dupa</a>
                        </h4>
                       
                        <div class="panel-collapse collapse show" id="filter">
                            <div class="panel-content">
                                <form>
                                    <div class="form-group">
                                        <label for="tara">Tara</label>
                                        <select class="form-control" id="tara">
                                            <option>Selectati</option>
                                            <option>Israel</option>
                                            <option>Italia</option>
                                            <option>Spania</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Ocazii speciale</label>
                                        <input type="checkbox" /> Toate<br />
                                        <input type="checkbox" /> 1 Mai<br />
                                        <input type="checkbox" /> Pelerinaje
                                    </div>
                                    <div class="form-group">
                                        <label for="plecare">Luna plecarii</label>
                                        <select class="form-control" id="plecare">
                                            <option>Alege inceperea circuitului</option>
                                            <option>Decembrie 2016</option>
                                            <option>Ianuarie 2017</option>
                                            <option>Februarie 2017</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Transport</label>
                                        <input type="checkbox" /> Toate<br />
                                        <input type="checkbox" /> Autocar<br />
                                        <input type="checkbox" /> Avion
                                    </div>
                                    <div class="form-group">
                                        <label>Durata</label>
                                        <input type="checkbox" /> Toate<br />
                                        <input type="checkbox" /> 5 zile<br />
                                        <input type="checkbox" /> 8 zile
                                    </div>
                                    <button type="button" class="btn btn-success btn-block">Filtreaza</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="col-sm-8 col-md-9">

                <div class="heading">
                    <h1>Circuite</h1>
                </div>

                <div class="card-columns circuit">
                            <div class="card grid-view">
                                <img class="card-img-top" src="http://mapamond.ro/wp-content/uploads/2016/10/is.jpg" alt="">
                                <div class="early-booking">Early<br>Booking</div>
                                <div class="discount">-20%</div>
                                <div class="card-block">
                                    <span class="price"><small>de la</small>495€</span>
                                    <h4 class="title">Program Economic <small>6 zile / 5 nopti</small></h4>
                                    <div class="transport">
                                        <span><i class="fa fa-plane fa-lg"></i> Avion <i class="fa fa-bus fa-lg"></i> Autocar local</span>
                                    </div>
                                    <div class="row date">
                                        <div class="departures col-sm-6">
                                            <i class="fa fa-clock-o"></i>
                                            <div>
                                                <span>Plecari</span><br>Februarie<br>Martie<br>Aprilie<br>Februarie<br>Martie<br>Aprilie
                                            </div>
                                        </div>
                                        <div class="countries col-sm-6">
                                            <i class="fa fa-map-marker"></i>
                                            <div>
                                                <span>Tari vizitate</span><br>Israel
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="info"><i class="fa fa-info-circle"></i> Toate taxele incluse</p>
                                    <a href="" class="btn btn-info btn-block">Vezi Detalii</a>
                                </div>
                            </div>

                            <div class="card grid-view">
                                <img class="card-img-top" src="https://www.pensiata.ro/wp-content/uploads/2015/04/Pensie-Italia-810x506.jpg" alt="">
                                <div class="early-booking">Early<br>Booking</div>
                                <div class="discount">-20%</div>
                                <div class="card-block">
                                    <span class="price"><small>de la</small>405€</span>
                                    <h4 class="title">Italia <small>9 zile / 8 nopti</small></h4>
                                    <div class="transport">
                                        <span><i class="fa fa-bus fa-lg"></i> Autocar</span>
                                    </div>
                                    <div class="row date">
                                        <div class="departures col-sm-6">
                                            <i class="fa fa-clock-o"></i>
                                            <div>
                                                <span>Plecari</span><br>Iulie
                                            </div>
                                        </div>
                                        <div class="countries col-sm-6">
                                            <i class="fa fa-map-marker"></i>
                                            <div>
                                                <span>Tari vizitate</span><br>Elvetia<br>Germania<br>Italia
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="info"><i class="fa fa-info-circle"></i> Toate taxele incluse</p>
                                    <a href="" class="btn btn-info btn-block">Vezi Detalii</a>
                                </div>
                            </div>

                            <div class="card grid-view">
                                <img class="card-img-top" src="http://www.pelerinaj.ro/wp-content/uploads/2016/04/SPANIA-SANTIAGO-DE-COMPOSTELA-2-1024x681.jpg" alt="">
                                <div class="early-booking">Early<br>Booking</div>
                                <div class="discount">-20%</div>
                                <div class="card-block">
                                    <span class="price"><small>de la</small>950€</span>
                                    <h4 class="title">Spania <small>15 zile / 14 nopti</small></h4>
                                    <div class="transport">
                                        <span><i class="fa fa-plane fa-lg"></i> Avion <i class="fa fa-bus fa-lg"></i> Autocar local</span>
                                    </div>
                                    <div class="row date">
                                        <div class="departures col-sm-6">
                                            <i class="fa fa-clock-o"></i>
                                            <div>
                                                <span>Plecari</span><br>Mai<br>Iunie<br>August
                                            </div>
                                        </div>
                                        <div class="countries col-sm-6">
                                            <i class="fa fa-map-marker"></i>
                                            <div>
                                                <span>Tari vizitate</span><br>Portugalia<br>Spania
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="info"><i class="fa fa-info-circle"></i> Toate taxele incluse</p>
                                    <a href="" class="btn btn-info btn-block">Vezi Detalii</a>
                                </div>
                            </div>

                            <div class="card grid-view">
                                <img class="card-img-top" src="http://mapamond.ro/wp-content/uploads/2016/10/is.jpg" alt="">
                                <div class="early-booking">Early<br>Booking</div>
                                <div class="discount">-20%</div>
                                <div class="card-block">
                                    <span class="price"><small>de la</small>495€</span>
                                    <h4 class="title">Program Economic <small>6 zile / 5 nopti</small></h4>
                                    <div class="transport">
                                        <span><i class="fa fa-plane fa-lg"></i> Avion <i class="fa fa-bus fa-lg"></i> Autocar local</span>
                                    </div>
                                    <div class="row date">
                                        <div class="departures col-sm-6">
                                            <i class="fa fa-clock-o"></i>
                                            <div>
                                                <span>Plecari</span><br>Februarie<br>Martie<br>Aprilie<br>Februarie<br>Martie<br>Aprilie
                                            </div>
                                        </div>
                                        <div class="countries col-sm-6">
                                            <i class="fa fa-map-marker"></i>
                                            <div>
                                                <span>Tari vizitate</span><br>Israel
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="info"><i class="fa fa-info-circle"></i> Toate taxele incluse</p>
                                    <a href="" class="btn btn-info btn-block">Vezi Detalii</a>
                                </div>
                            </div>

                            <div class="card grid-view">
                                <img class="card-img-top" src="https://www.pensiata.ro/wp-content/uploads/2015/04/Pensie-Italia-810x506.jpg" alt="">
                                <div class="early-booking">Early<br>Booking</div>
                                <div class="discount">-20%</div>
                                <div class="card-block">
                                    <span class="price"><small>de la</small>405€</span>
                                    <h4 class="title">Italia <small>9 zile / 8 nopti</small></h4>
                                    <div class="transport">
                                        <span><i class="fa fa-bus fa-lg"></i> Autocar</span>
                                    </div>
                                    <div class="row date">
                                        <div class="departures col-sm-6">
                                            <i class="fa fa-clock-o"></i>
                                            <div>
                                                <span>Plecari</span><br>Iulie
                                            </div>
                                        </div>
                                        <div class="countries col-sm-6">
                                            <i class="fa fa-map-marker"></i>
                                            <div>
                                                <span>Tari vizitate</span><br>Elvetia<br>Germania<br>Italia
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="info"><i class="fa fa-info-circle"></i> Toate taxele incluse</p>
                                    <a href="" class="btn btn-info btn-block">Vezi Detalii</a>
                                </div>
                            </div>

                            <div class="card grid-view">
                                <img class="card-img-top" src="http://www.pelerinaj.ro/wp-content/uploads/2016/04/SPANIA-SANTIAGO-DE-COMPOSTELA-2-1024x681.jpg" alt="">
                                <div class="early-booking">Early<br>Booking</div>
                                <div class="discount">-20%</div>
                                <div class="card-block">
                                    <span class="price"><small>de la</small>950€</span>
                                    <h4 class="title">Spania <small>15 zile / 14 nopti</small></h4>
                                    <div class="transport">
                                        <span><i class="fa fa-plane fa-lg"></i> Avion <i class="fa fa-bus fa-lg"></i> Autocar local</span>
                                    </div>
                                    <div class="row date">
                                        <div class="departures col-sm-6">
                                            <i class="fa fa-clock-o"></i>
                                            <div>
                                                <span>Plecari</span><br>Mai<br>Iunie<br>August
                                            </div>
                                        </div>
                                        <div class="countries col-sm-6">
                                            <i class="fa fa-map-marker"></i>
                                            <div>
                                                <span>Tari vizitate</span><br>Portugalia<br>Spania
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="info"><i class="fa fa-info-circle"></i> Toate taxele incluse</p>
                                    <a href="" class="btn btn-info btn-block">Vezi Detalii</a>
                                </div>
                            </div>
                </div>

            </div>
        </div>

    </div>
</section>

<?php require_once("includes/newsletter.php"); ?>
<?php require_once("includes/footer.php"); ?>