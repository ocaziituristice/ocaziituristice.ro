<?php require_once("includes/head.php"); ?>
<?php require_once("includes/top-bar.php"); ?>
<?php require_once("includes/header.php"); ?>
<?php require_once("includes/navbar.php"); ?>
<?php  $google_api_key_static='AIzaSyB1zp5lmkCYyI6zkkUEBZIO4wB79XCipLE'?>
<section id="content" class="hotel">
    <div class="container">

        <?php require_once("includes/breadcrumb.php"); ?>

           <div class="row">
            <div class="col-sm-8">
                <div class="heading" style="padding-bottom: 10px">
                    <h1>Hotel Kempinski Grand Arena <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></h1>
                    <span class="location"><i class="fa fa-map-marker fa-lg"></i> Bd Mamaia Nord, Mamaia nr. 91, Mamaia, Romania, <strong>50m de plaja</strong></span>
                </div>
            </div>
             <div class="col-sm-4">
                <a href="#"><span class="tripLabel">TripAdvisor Rating</span><br><img src="https://static.tacdn.com/img2/ratings/traveler/4.0.gif" alt="Rating TripAdvisor 4 din 5 stele"><br>248 de pareri</a>
            </div>

            <div class="col-lg-9 col-sm-12 col-md-12">
                <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-fit="cover" data-loop="true" data-ratio="1.7">
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7103hotel_phoenicia_luxury1hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/7692hotel_phoenicia_luxury2hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/3171hotel_phoenicia_luxury4hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5339hotel_phoenicia_luxury5hotel.jpg"></a>
					<a href="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/5219hotel_phoenicia_luxury13hotel.jpg"></a>
				</div>
				<div class="discount eb">
					Early Booking <span>-20%</span>
				</div>
				<!-- <div class="discount lm">
					Last Minute <span>-20%</span>
				</div> -->
				<!-- <div class="discount bf">
					Black Friday <span>-20%</span>
				</div> -->
            </div>

            <div class="col-sm-3 hidden-md-down" style="padding-left: 0">
                <?php require_once("includes/trustme.php"); ?>
            </div>
        
        <div class="recommended">
                <i class="fa fa-thumbs-up"></i>
                <p>Recomandat pentru: Familii cu copii</p>
            </div>
        
        </div>

        <div class="price-box">
            <h4><i class="fa fa-search"></i> Calculator tarife Cleopatra Apartments Hersonissos</h4>
            <form class="form-inline">
                <div class="form-group col-md-2">
                    <label>Data check-in:</label>
                    <select class="form-control">
                        <option>05 mai 2017</option>
                        <option>16 iunie 2017</option>
                        <option>30 august 2017</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label>Nr. Nopti:</label>
                    <select class="form-control">
                        <option>7</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label>Nr. Adulti</label>
                    <select class="form-control">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label>Nr. Copii</label>
                    <select class="form-control">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label>Transport:</label>
                    <select class="form-control">
                        <option>Avion/Bucuresti</option>
                        <option>Avion/Cluj</option>
                        <option>Avion/Timisoara</option>
                    </select>
                </div>           
                <div class="form-group col-md-2">
                    <a href="" class="btn btn-success btn-block">Vezi pret</a>
                </div>
            </form>
        </div>

 <table class="table table-responsive" style="margin-top: 20px; margin-bottom: 0">
            <tbody>
                <tr>
                    <th>Data plecării</th>
                    <th>Durată</th>
                    <th>Tip cameră</th>
                    <th>Tip masă</th>
                    <th>Preț / cameră</th>
                    <th style="width:20px;"></th>
                    <th style="width:192px;"></th>
                </tr>

                <tr>
                    <td style="background-color: #efffe9 !important">	  
		                <h6>31.05.2017</h6>
		  			</td>
		  			<td style="background-color: #efffe9 !important">7 nopți</td>
		  			<td style="background-color: #efffe9 !important">
		  			    <h6>Studio</h6>
		  			    <span class="text-danger">Early Booking 40% pana la 31.03.2017</span><br>
		  			    <span class="text-success">Plecare din <strong>Otopeni-Henri Coanda</strong> - Aegean-Airlines</span><br>
		  			    <a data-toggle="collapse" href="#additional-info" aria-expanded="false" aria-controls="additional-info">
                            Additional info
                        </a>
		  			</td>
		  			<td style="background-color: #efffe9 !important">Fără masă</td>
		  			<td style="background-color: #efffe9 !important">
		  			    <span class="old-price text-primary">902 €</span>
		  			    <span class="current-price text-success">615 €</span>
		  			</td>
		  			<td style="background-color: #efffe9 !important">
		  			    <span class="badge badge-success">Disponibil</span><br>
		  			    <span class="text-center"><i class="fa fa-check-circle text-success"></i></span>
		  			</td>
		  			<td style="background-color: #efffe9 !important"><a href="" class="btn btn-primary">Rezerva acum</a></td>
		  		</tr>
                
                <tr>
                    <td colspan="7" style="padding: 0; border: 0">
                        <div class="collapse" id="additional-info" aria-expanded="true">
                            <div class="card card-block" style="background-color: #efffe9 !important; border: 0; border-radius: none;">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12">
                                        <h5>Servicii Incluse</h5>
                                        <ul>
                                            <li>7 nopți cazare cu masă conform ofertei</li>
                                            <li>Bilet avion Bucuresti - Heraklion și retur</li>
                                            <li class="text-success">Taxele de aeroport Agentia de turism</li>
                                            <li>Bagaj de mână și bagaj de cală</li>
                                            <li>Transfer aeroport - hotel - aeroport</li>
                                            <li>Asistență turistică în limba română</li>
                                            <li>Cazare cu masa conform ofertei</li>
                                        </ul>
                                    </div>

                                    <div class="col-lg-8 col-sm-12">
                                        <table class="table table-responsive" style="background-color: #efffe9 !important;">
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">Companie</th>
                                                    <th class="text-center">Nr. cursă</th>
                                                    <th class="text-center">Plecare</th>
                                                    <th class="text-center">Sosire</th>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
                                                    <td class="text-center">4231</td>
                                                    <td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
                                                    <td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
                                                    <td class="text-center">4230</td>
                                                    <td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
                                                    <td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="background-color: #fafafa !important">     
                        <h6>31.05.2017</h6>
                    </td>
                    <td style="background-color: #fafafa !important">7 nopți</td>
                    <td style="background-color: #fafafa !important">
                        <h6>Studio</h6>
                        <span class="text-danger">Early Booking 40% pana la 31.03.2017</span><br>
                        <span class="text-success">Plecare din <strong>Otopeni-Henri Coanda</strong> - Aegean-Airlines</span><br>
                        <a data-toggle="collapse" href="#additional-info-3" aria-expanded="false" aria-controls="additional-info">
                            Additional info
                        </a>
                    </td>
                    <td style="background-color: #fafafa !important">Fără masă</td>
                    <td style="background-color: #fafafa !important">
                        <span class="old-price text-primary">902 €</span>
                        <span class="current-price text-success">615 €</span>
                    </td>
                    <td style="background-color: #fafafa !important">
                        <span class="badge badge-success">Disponibil</span><br>
                        <span class="text-center"><i class="fa fa-check-circle text-success"></i></span>
                    </td>
                    <td style="background-color: #fafafa !important"><a href="" class="btn btn-primary">Rezerva acum</a></td>
                </tr>
                
                <tr>
                    <td colspan="7" style="padding: 0; border: 0">
                        <div class="collapse" id="additional-info-3" aria-expanded="true">
                            <div class="card card-block" style="background-color: #fafafa !important; border: 0; border-radius: none;">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12">
                                        <h5>Servicii Incluse</h5>
                                        <ul>
                                            <li>7 nopți cazare cu masă conform ofertei</li>
                                            <li>Bilet avion Bucuresti - Heraklion și retur</li>
                                            <li class="text-success">Taxele de aeroport Agentia de turism</li>
                                            <li>Bagaj de mână și bagaj de cală</li>
                                            <li>Transfer aeroport - hotel - aeroport</li>
                                            <li>Asistență turistică în limba română</li>
                                            <li>Cazare cu masa conform ofertei</li>
                                        </ul>
                                    </div>

                                    <div class="col-lg-8 col-sm-12">
                                        <table class="table table-responsive" style="background-color: #fafafa !important;">
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">Companie</th>
                                                    <th class="text-center">Nr. cursă</th>
                                                    <th class="text-center">Plecare</th>
                                                    <th class="text-center">Sosire</th>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
                                                    <td class="text-center">4231</td>
                                                    <td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
                                                    <td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
                                                    <td class="text-center">4230</td>
                                                    <td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
                                                    <td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>	  
		                <h6>31.05.2017</h6>
		  			</td>
		  			<td>7 nopți</td>
		  			<td>
		  			    <h6>Studio</h6>
		  			    <span class="text-danger">Early Booking 40% pana la 31.03.2017</span><br>
		  			    <span class="text-success">Plecare din <strong>Otopeni-Henri Coanda</strong> - Aegean-Airlines</span><br>
		  			    <a data-toggle="collapse" href="#additional-info-2" aria-expanded="false" aria-controls="additional-info">
                            Additional info
                        </a>
		  			</td>
		  			<td>Fără masă</td>
		  			<td>
		  			    <span class="old-price text-primary">902 €</span>
		  			    <span class="current-price text-success">615 €</span>
		  			</td>
		  			<td>
		  			    <span class="badge badge-danger">Indisponibil</span><br>
		  			    <span class="text-center"><i class="fa fa-minus-circle text-danger"></i></span>
		  			</td>
		  			<td><a href="" class="btn btn-primary">Rezerva acum</a></td>
		  		</tr>

                 <tr>
                    <td colspan="7" style="padding: 0; border: 0">
                        <div class="collapse" id="additional-info-2" aria-expanded="true">
                            <div class="card card-block" style="border: 0; border-radius: none;">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12">
                                        <h5>Servicii Incluse</h5>
                                        <ul>
                                            <li>7 nopți cazare cu masă conform ofertei</li>
                                            <li>Bilet avion Bucuresti - Heraklion și retur</li>
                                            <li class="text-success">Taxele de aeroport Agentia de turism</li>
                                            <li>Bagaj de mână și bagaj de cală</li>
                                            <li>Transfer aeroport - hotel - aeroport</li>
                                            <li>Asistență turistică în limba română</li>
                                            <li>Cazare cu masa conform ofertei</li>
                                        </ul>
                                    </div>

                                    <div class="col-lg-8 col-sm-12">
                                        <table class="table table-responsive">
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">Companie</th>
                                                    <th class="text-center">Nr. cursă</th>
                                                    <th class="text-center">Plecare</th>
                                                    <th class="text-center">Sosire</th>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
                                                    <td class="text-center">4231</td>
                                                    <td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
                                                    <td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
                                                    <td class="text-center">4230</td>
                                                    <td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
                                                    <td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
		  	</tbody>
		</table>

		<div style="background-color: #fafafa; padding: 15px; font-size: 14px; margin-bottom: 20px">
		    <strong>Legendă:</strong> <i class="fa fa-check-circle text-success"></i> Disponibil, <i class="fa fa-check-circle text-primary"></i> Disponibil, necesită confirmare, <i class="fa fa-minus-circle text-danger"></i> Indisponibil
	        <br><br>
	        <strong>OcaziiTuristice.ro</strong> se conecteaza in timp real la sistemele tour operatorilor din Romania si strainatate pentru a va oferi cele mai bune preturi disponibile.
	        <br><br>
	        <strong>Atenție!</strong> <i>Trimiterea unei rezervări <strong>nu implică</strong> nici o obligație financiară din partea dumneavoastră!</i> 
	    </div>

	   




		<div class="content">
            <a data-toggle="collapse" href="#hotel-description" aria-expanded="true" aria-controls="hotel-description"><h4>Descriere Hotel Kempinski Grand Arena</h4></a>
            <div class="collapse show" id="hotel-description">
                <div class="row">
		            <div class="col-sm-12">
				        <p>Gandit pentru a gazdui in mod special familiile cu copii, oferind super confort atat pentru odihna, dar si pentru relaxare si distractie, Phoenicia Holiday Luxury este un nou concept turistic in Romania, fiind unic in felul sau.</p>
				        <p>Situat chiar pe malul marii, cu o arhitectura ultramederna dar si calitatea dotarilor, toate acestea fac din Hotelul Phoenicia Luxury unul dintre cela mai selecte locatii de pe litoralul romanesc.</p>
				        <p>Hotelul, ofera spre cazare 160 de spati, toate fiind dotate elegant si modern in conformitate cu cerintele celor 5 stele ce le detine. Camerele avand suprafata de 45 mp, iar apartamentele 71 mp, sunt confortabile oferind un spatiu generos de cazare si, toate au vedere laterala catre mare.</p>
				        <p>Complexul pune la dispozitia turistilor piscina mare, piscina mica, piscina exterioara, interioara( in total 6 piscine atat, pentru cei mari dar si pentru cei mici), o frumoasa plaja privata completata cu nisip fin cernut si ingrijit permanent, o baza sportiva unde se poate juca fotbal, volei, tenis de camp pe terenurile special amenajate puse gratuit la dispozitia turistilor.</p>
			        </div>
			        <div class="col-sm-4">
			            <ul>
                            <h5>General</h5>
                            <li>- restaurant, bar, terasă, terasă la soare</li>
                            <li>- piscină în aer liber (deschisă în sezon)</li>
                            <li>- seif, lift, încălzire, aer condiţionat</li>
                            <li>- recepţie deschisă nonstop</li>
                            <li>- cameră de bagaje</li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
			            <ul>
                            <h5>Servicii</h5>
                            <li>- închirieri auto</li>
                            <li>- săli de conferinţă şi petreceri</li>
                            <li>- fax/copiator</li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
			            <ul>
                            <h5>Internet</h5>
                            <li><span class="free-policy">Gratuit!</span> Internet wireless este disponibil în întregul hotel şi este gratuit.</li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
			            <ul>
                            <h5>Parcare</h5>
                            <li><span class="free-policy">Gratuit!</span> Este posibilă parcarea publică gratuit la proprietate (nu este posibilă rezervarea).</li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
			            <ul>
                            <h5>Check-in / check-out</h5>
                            <li>incepand cu ora 18:00 / incepand cu ora 12:00</li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
			            <ul>
                           <h5>Animale companie</h5>
                           <li>Animalele de companie nu sunt acceptate.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <a data-toggle="collapse" href="#hotel-rooms" aria-expanded="true" aria-controls="hotel-rooms"><h4>Detalii camere Hotel Kempinski Grand Arena</h4></a>
            <div class="collapse show" id="hotel-rooms">
            <div class="row">
                <div class="col-sm-6">
                    <ul>
                        <h5>Camera dubla</h5>
                        <li>Camera dubla cu pat matimonial, cuprinde un spatiu deschis cu o suprafata de 45 mp, zona pentru dormit cu un pat matrimonial, canapea modulara, extensibila sau fixa, baie cu cada si balcon.</li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul>
                        <h5>Family Suite de lux</h5>
                        <li>Camerele co municante, este formata din doua camera duble, una matimoniala si una twin, insumand un spatiu generos de 90 mp, cu doua bai cu cada.</li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul>
                        <h5>Apartament cu 1 dormitor</h5>
                        <li>Cuprinde un spatiu generos de 71 mp, format dintr-un living si un dormitor cu pat matrimonial, 2 bai, una de serviciu si una cu cada, balcon, cu vedere frontala fie spre mare, fie spre lacul Siutghiol.</li>
                    </ul>
                </div>
            </div>
            </div>

            <a data-toggle="collapse" href="#hotel-location" aria-expanded="true" aria-controls="hotel-location"><h4>Localizare pe harta Hotel Kempinski Grand Arena</h4></a>
            <div class="collapse show" id="hotel-location">
            <div class="row">
                <div class="col-sm-8">

                </div>

                <div class="col-sm-4">
                    <div class="nearby">

				        <h5>Hoteluri din Bansko in apropiere</h5>

				        <a href="#">
					        <img src="https://www.ocaziituristice.ro/thumb_hotel/2300hotel_emerald_spa1hotel.jpg" alt="">
					        <span class="title">Hotel Emerald Spa</span>
					        <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
					        <span>71 metri</span>
				        </a>
				        <a href="#">
					        <img src="https://www.ocaziituristice.ro/thumb_hotel/8479aspen_aparthotel1hotel.jpg" alt="">
					        <span class="title">Aspen Aparthotel</span>
					        <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
					        <span>158 metri</span>
				        </a>
				        <a href="#">
					        <img src="https://www.ocaziituristice.ro/thumb_hotel/7949hotel_comfort1hotel.jpg" alt="">
					        <span class="title">Hotel Comfort</span>
					        <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
					        <span>182 metri</span>
				        </a>
				        <a href="#">
					        <img src="https://www.ocaziituristice.ro/thumb_hotel/4604hotel_astera_spa1hotel.jpg" alt="">
					        <span class="title">Hotel Astera Spa</span>
					        <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
					        <span>184 metri</span>
				        </a>
				        <a href="#">
					        <img src="https://www.ocaziituristice.ro/thumb_hotel/2114mpm_sport_spa_and_wellness1hotel.jpg" alt="">
					        <span class="title">MPM SPORT SPA and Wellness</span>
					        <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
					        <span>201 metri</span>
				        </a>
				        <a href="#">
					        <img src="https://www.ocaziituristice.ro/thumb_hotel/6407hotel_casa_karina1hotel.jpg" alt="">
					        <span class="title">Hotel Casa Karina</span>
					        <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
					        <span>250 metri</span>
				        </a>
			        </div>
                </div>
            </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">

                <div class="heading">
                    <h2>Clienții care au văzut <span class="text-danger">Cleopatra Apartments</span> au fost interesați și de:</h2>
                </div>
                
                <div class="card-deck-wrapper circuit">
                    <div class="card-deck">
    
                        <div class="card block-view">
                            <div class="card-block">
                                <h4 class="title">Hotel Olymp</h4>
                                <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                <label class="description">
                                    <img src="https://www.ocaziituristice.ro/thumb_hotel/4678hotel_olymp1hotel.jpg" class="thumb" alt="">
                                    Cu toate ca este situat într-o zonă liniștită, hotelul este la doar 1,5 km de teleschiuri (unde se poate ajunge gratuit cu un autobuz de schi) și la 10 minute de mers pe jos de zona centrală a orasului. Facilităţile de spa ale...</label>
                                <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                            </div>
                        </div>
            
                        <div class="card block-view">
                            <div class="card-block">
                                <h4 class="title">Hotel Aseva House</h4>
                                <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                <label class="description">
                                    <img src="https://www.ocaziituristice.ro/thumb_hotel/7318hotel_aseva_house1hotel.jpg" class="thumb" alt="">
                                    Hotelul, situat la aproximativ situat la 300 m de gondola si 100 m de centrul statiunii, oferă .restaurantul principal, taverna cu semineu, internet in lobby, camera pentru depozitarea echipamentului pentru schi,parcare in limita...</label>
                                <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                            </div>
                        </div>

                        <div class="card block-view">
                            <div class="card-block">
                                <h4 class="title">Hotel Lina</h4>
                                <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                <label class="description">
                                    <img src="https://www.ocaziituristice.ro/thumb_hotel/9008hotel_lina1hotel.jpg" class="thumb" alt="">
                                    Hotelul Lina, situat într-o zonă liniştită din Bansko, la 10 minute de centrul oraşului şi de principala staţie de telegondolă, oferă transport la staţie, camere spaţioase şi confortabile, cu acces gratuit la internet Wi-Fi.</label>
                                <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                            </div>
                        </div>

                        <div class="card block-view">
                            <div class="card-block">
                                <h4 class="title">Hotel Mount View Lodge</h4>
                                <span class="stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                <label class="description">
                                    <img src="https://www.ocaziituristice.ro/thumb_hotel/6344hotel_mount_view_lodge1hotel.jpg" class="thumb" alt="">
                                    Hotelul oferă studiouri spațioase de lux și apartamente cu un design interior deosebit, fiecare dintre acestea fiind dotate cu chicinetă complet echipata si semineu. Centrul de fitness si spa oferă saună, baie de aburi...</label>
                                <a href="" class="btn btn-info btn-sm btn-block">Vezi preturi</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>



<script>

var locations = [

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-tanne/'>Hotel Tanne - Bansko</a>", 41.827449798583984, 23.484731674194336, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"],

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/aparthotel-orbilux/'>Aparthotel Orbilux - Bansko</a>", 41.838020324707030, 23.489030838012695, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"],

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-belvedere-holiday-club/'>Hotel Belvedere Holiday Club - Bansko</a>", 41.831783294677734, 23.475282669067383, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"],

  ["<strong>Hotel Bansko Spa and Holidays - Bansko</strong>", 41.826488494873050, 23.478584289550780, "https://www.ocaziituristice.ro/images/icon_maps_hotel.png"],

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-emerald-spa/'>Hotel Emerald Spa - Bansko</a>", 41.826423645019530, 23.479434967041016, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"]

];



function initGoogleMap(container){



  var infowindow = new google.maps.InfoWindow();

  var map = new google.maps.Map(document.getElementById(container), {

      zoom: 16,

      center: new google.maps.LatLng(41.826488494873050, 23.478584289550780),

      mapTypeId: google.maps.MapTypeId.ROADMAP

  });

  

  function placeMarker( loc ) {

    var latLng = new google.maps.LatLng( loc[1], loc[2]);

    var markerIcon = loc[3];

    var marker = new google.maps.Marker({

      position : latLng,

      map      : map,

      icon     : markerIcon

    });

    google.maps.event.addListener(marker, 'click', function(){

        infowindow.close();

        infowindow.setContent( "<div id='infowindow'>"+ loc[0] +"</div>");

        infowindow.open(map, marker);

    });

  }



  for(var i=0; i<locations.length; i++) {

    placeMarker( locations[i] );

  } 

  

}





$(function(){

	$(".address").click(function(e){

		e.preventDefault();

		goToByScroll($(this).data("scroll"), 'h2');

	});

	

	$(".trust").load("includes/trustme.php");



	modalOpen('span.static', 'modals/map_hotel.php');

});



$(window).load(function(){

	$(".visited ul li p").dotdotdot();

});



$(window).resize(function(){

	$(".visited ul li p").dotdotdot();

});







if(isDevice) {

	$("#map").html('<span class="static" style="background-image: url(\'https://maps.googleapis.com/maps/api/staticmap?center=41.826488494873050,23.478584289550780&zoom=16&size=1000x500&maptype=roadmap&markers=icon:https://www.ocaziituristice.ro/images/icon_maps_hotel.png|41.826488494873050,23.478584289550780&key=AIzaSyBjF-m8eaYUB5xEhpBZ0eJcGE6KqRI-QCM\'"></span>');

} else {

	google.maps.event.addDomListener(window, 'load', initGoogleMap('map'));

}


</script>


<?php require_once("includes/newsletter.php"); ?>
<?php require_once("includes/footer.php"); ?>