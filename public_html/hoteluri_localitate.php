<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_hotel/class_hoteluri.php');

	/*header("HTTP/1.1 301 Moved Permanently");
	header('Location: '.$sitepath.'sejur-'.$_REQUEST['tara'].'/'.$_REQUEST['zona'].'/'.$_REQUEST['oras'].'/');
	exit();*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php");
$tara=desfa_link($_REQUEST['tara']);
$zona=desfa_link($_REQUEST['zona']);
$id_tara=get_id_tara($tara);
$id_zona=get_id_zona($zona, $id_tara);
$tara_seo=ucwords($tara);
$localitati=desfa_link($_REQUEST['oras']); ?>
<title>Hoteluri din <?php echo ucwords($localitati); ?> - <?php echo ucwords($zona).', '.$tara_seo; ?> | Hoteluri <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Hoteluri din zona <?php echo ucwords($localitati).', '.ucwords($zona); ?> - <?php echo $tara_seo; ?>, oferte sejururi <?php echo ucwords($localitati); ?>" />
<meta name="keywords" content="Hoteluri <?php echo $tara_seo; ?>, hoteluri <?php echo ucwords($localitati); ?>, sejururi <?php echo ucwords($localitati); ?>" />
<meta name="Publisher" content="ocaziituristice.ro" />

<meta name="robots" content="noindex, nofollow">
<meta name="robots" content="noarchive">
<meta name="googlebot" content="nosnippet">
<meta name="googlebot" content="noodp">
<meta name="language" content="ro" />

<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
  <table class="mainTableClass" border="0" cellspacing="0" cellpadding="0">
	<tr>
      <td colspan="2" align="left" valign="top">
        <div class="breadcrumb">
         <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
        </div>
      </td>
    </tr>
	<tr>
	  <td class="mainTableColumnLeft" align="left" valign="top">
      <div id="columnLeft">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri/hoteluri_localitate.php"); ?>
      </div>
      </td>
	  <td class="mainTableColumnRight" align="left" valign="top">
        <div id="columnRight">
        <?php //include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta.php"); ?>
        <?php //include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_main_reclama.php"); ?>
        </div>
      </td>
	</tr>
  </table>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>