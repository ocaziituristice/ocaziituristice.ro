<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip = "revelion";
$linktip = "revelion";
$dentip = "Revelion";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii = get_id_tip_sejur_fii($id_tip_sejur);
$iduri = "'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri = $iduri.','.$tip_fii['id_tipuri'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Revelion <?php echo date("Y")+1; ?> | ocaziituristice.ro</title>
<meta name="description" content="Oferte Revelion <?php echo date("Y")+1; ?> in Romania si strainate, sejururi de Revelion si circuite, cazare Revelion Romania, sejuri speciale de Revelion " />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body>
<header>
		<?php require( "includes/header/meniu_header_responsive.php" ); ?>
    </header>
<div id="middle">
  <div id="middleInner">
        <div class="layout">
		<?php require( "includes/header/breadcrumb_responsive.php" ); ?>
		<?php require( "includes/header/search_responsive.php" ); ?>
    </div>

    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Turism Intern - Romania</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear">
        
        <div class="Hline"></div>
        
        <div class="pad5">
   
          <br class="clear"><br>
          
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Hoteluri in Romania</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
          
          <div class="float-left" style="width:630px;">
            <h2 style="margin:0; padding:0;"><span class="titlu">Sejururi Romania</span></h2>
            <div class="coloana-links" style="width:190px; margin-right:30px;">
			<?php
			$sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND zone.tpl_romania = '2'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			while($row_zone=mysql_fetch_array($que_zone)) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
            <div class="coloana-links" style="width:190px; margin-right:30px;">
			<?php
			$sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND zone.tpl_romania IS NULL
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			while($row_zone=mysql_fetch_array($que_zone)) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
            <div class="coloana-links" style="width:190px;">
			<?php
            $sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND zone.tpl_romania = '1'
			AND zone.denumire = 'Statiuni Balneare'
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			$row_zone=mysql_fetch_array($que_zone);
			if(mysql_num_rows($que_zone)>0) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
          </div>
          
          <div class="float-right" style="width:300px;">
            <h2 >tematici</h2>
            <div class="coloana-links" style="width:300px;">
           ---
</div>
          </div>
          
        </div>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>

<?php require_once( "includes/footer/footer_responsive.php" ); ?>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>
    
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=<?php echo $id_tip_sejur; ?>");
</script>
</body>
</html>
