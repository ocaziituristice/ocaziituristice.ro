<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); ?>
<?php
$id_tara = $_REQUEST['tara'];
$id_zona = get_id_zona(desfa_link($_REQUEST['dest']), $id_tara);
$den_destinatie = get_den_zona($id_zona);
$den_zona = get_den_zona($id_zona);
$link_destinatie = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';
if(fa_link(get_den_zona($id_zona))!=fa_link(get_den_tara($id_tara))) $link_destinatie .= fa_link(get_den_zona($id_zona)).'/';
$check_articles = check_articles($id_tara, $id_zona);

$link_articles = '/ghid-turistic-'.fa_link($den_destinatie).'/';

$den_tara = get_den_tara($id_tara);
$link_tara = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';

$sel_clima = "SELECT
tari_caracteristici.value AS clima_tara,
zone_caracteristici.value AS clima_zona,
clima.temp_max,
clima.temp_min,
clima.temp_apa,
clima.temp_precipitatii
FROM clima
INNER JOIN tari ON tari.id_tara = clima.id_tara
LEFT JOIN tari_caracteristici ON (tari_caracteristici.id_tara = clima.id_tara AND tari_caracteristici.id_caracteristica = '2')
LEFT JOIN zone ON zone.id_zona = clima.id_zona
LEFT JOIN zone_caracteristici ON (zone_caracteristici.id_zona = clima.id_zona AND zone_caracteristici.id_caracteristica = '2')
WHERE clima.id_tara = '".$id_tara."'
AND clima.id_zona = '".$id_zona."'
AND clima.id_localitate = '0' ";
$que_clima = mysql_query($sel_clima) or die(mysql_error());
$row_clima = mysql_fetch_array($que_clima);

$js_medii = '';

if($row_clima['temp_max']) {
	$temp_max = explode(";",$row_clima['temp_max']);
	
	$row_temp_max = '<tr class="bkg-orange"><th>Temperatura Maxima (&deg;C)</th>';
	foreach($temp_max as $maxime) $row_temp_max .= '<td>'.$maxime.'</td>';
	$row_temp_max .= '</tr>';
	
	$js_medii .= "{ name: 'Temperatura Maxima (°C)', data: [".str_replace(";",",",$row_clima['temp_max'])."] },";
}
if($row_clima['temp_min']) {
	$temp_min = explode(";",$row_clima['temp_min']);
	
	$row_temp_min = '<tr class="bkg-teal"><th>Temperatura Minima (&deg;C)</th>';
	foreach($temp_min as $minime) $row_temp_min .= '<td>'.$minime.'</td>';
	$row_temp_min .= '</tr>';
	
	$js_medii .= "{ name: 'Temperatura Minima (°C)', data: [".str_replace(";",",",$row_clima['temp_min'])."] },";
}
if($row_clima['temp_apa']) {
	$temp_apa = explode(";",$row_clima['temp_apa']);
	
	$row_temp_apa = '<tr class="bkg-blue"><th>Temperatura apei (&deg;C)</th>';
	foreach($temp_apa as $medii_apa) $row_temp_apa .= '<td>'.$medii_apa.'</td>';
	$row_temp_apa .= '</tr>';
	
	$js_medii .= "{ name: 'Temperatura apei (°C)', data: [".str_replace(";",",",$row_clima['temp_apa'])."] },";
}
if($row_clima['temp_precipitatii']) {
	$temp_precipitatii = explode(";",$row_clima['temp_precipitatii']);
	
	$row_temp_precipitatii = '<tr class="bkg-light-teal"><th>Precipitatii (nr. zile)</th>';
	foreach($temp_precipitatii as $medii_precipitatii) $row_temp_precipitatii .= '<td>'.$medii_precipitatii.'</td>';
	$row_temp_precipitatii .= '</tr>';
	
	$js_medii .= "{ name: 'Precipitatii (nr. zile)', data: [".str_replace(";",",",$row_clima['temp_precipitatii'])."] },";
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Vremea in <?php echo $den_destinatie; ?>, Temperatura in <?php echo $den_destinatie; ?></title>
<meta name="description" content="Vremea in <?php echo $den_destinatie; ?>, mediile lunare in <?php echo $den_destinatie; ?> ale temperaturilor maxime, minime, ale apei din <?php echo $den_destinatie; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <div class="NEW-column-left2">
          <br>
          <div class="filters NEW-round6px">
            <div class="item NEW-round6px mar0">
              <div class="heading mar0"><?php echo $den_destinatie; ?></div>
              <div class="articles">
                <?php if($den_destinatie!=$den_tara) { ?><a href="<?php echo $link_tara; ?>" title="Sejur <?php echo $den_tara; ?>">Sejur <?php echo $den_tara; ?></a><?php } ?>
                <a href="<?php echo $link_destinatie; ?>" title="Sejur <?php echo $den_destinatie; ?>">Sejur <?php echo $den_destinatie; ?></a>
                <?php if($check_articles==1) { ?><a href="<?php echo $link_articles; ?>" title="Ghid turistic <?php echo $den_destinatie; ?>">Ghid turistic <?php echo $den_destinatie; ?></a><?php } ?>
                <?php $articole_principale = articles_side_menu($id_tara, $id_zona);
				if(count($articole_principale)>0) {
					foreach($articole_principale as $k_articles => $v_articles) {
						echo '<a href="/ghid-turistic-'.$v_articles['link_area'].'/'.$v_articles['link'].'.html" title="'.$v_articles['denumire'].'">'.$v_articles['denumire'].'</a>';
					}
				} ?>
              </div>
            </div>
          </div>
        </div>
        
        <div class="NEW-column-right2 clearfix">
          <h1 class="blue float-left">Vremea în <?php echo $den_destinatie; ?></h1>
          <br class="clear">
          <div class="Hline"></div>
          <div class="float-left clearfix"><?php include($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
          <div class="float-right mar0-10"><?php include($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?></div>
          <br class="clear"><br>
          <div class="pad10 article bigger-12em text-justify">
            <?php if(isset($id_zona)) { ?>
            <div class="pad10"><?php echo afisare_frumos(schimba_caractere($row_clima["clima_zona"]), 'nl2p'); ?></div>
            <?php } else if(isset($id_tara)) { ?>
            <div class="pad10"><?php echo afisare_frumos(schimba_caractere($row_clima["clima_tara"]), 'nl2p'); ?></div>
            <?php } ?>
            <h2 class="blue">Valori medii lunare în <?php echo $den_destinatie; ?></h2>
            <div id="weather" style="width:100%; height:400px;"></div>
            <br>
            <div class="pad10">
              <table class="weather">
                <tr>
                  <th class="w220">&nbsp;</th>
                  <th>Ian</th>
                  <th>Feb</th>
                  <th>Mar</th>
                  <th>Apr</th>
                  <th>Mai</th>
                  <th>Iun</th>
                  <th>Iul</th>
                  <th>Aug</th>
                  <th>Sep</th>
                  <th>Oct</th>
                  <th>Noi</th>
                  <th>Dec</th>
                </tr>
                <?php echo $row_temp_max; ?>
                <?php echo $row_temp_min; ?>
                <?php echo $row_temp_apa; ?>
                <?php echo $row_temp_precipitatii; ?>
              </table>
            </div>
            <br>
          <div class="pad10" align="center">
          <h2 class="blue"> <a href="<?php echo $link_destinatie; ?>" title="Sejur <?php echo $den_destinatie; ?>">Vezi aici ofertele de sejur pentru <?php echo $den_zona?> </a></h2>
          </div>
          
          
          </div>
        </div>
        
      </div>
    </div>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script src="/js/highcharts/highcharts.js"></script>
<script>
$(function () {
	$('#weather').highcharts({
		chart: { type: 'line' },
		title: { text: '' },
		subtitle: { text: '' },
		xAxis: { categories: ['Ian', 'Feb', 'Mar', 'Apr', 'Mai', 'Iun', 'Iul', 'Aug', 'Sep', 'Oct', 'Noi', 'Dec'] },
		yAxis: { title: { text: '' } },
		plotOptions: { line: {dataLabels: { enabled: true}, enableMouseTracking: false } },
		series: [<?php echo substr($js_medii,0,-1); ?>]
	});
});
</script>
</body>
</html>