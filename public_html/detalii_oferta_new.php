<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
setlocale(LC_TIME, array('ro.utf-8', 'ro_RO.UTF-8', 'ro_RO.utf-8', 'ro', 'ro_RO', 'ro_RO.ISO8859-2'));
$id_oferta=$_REQUEST['id_oferta'];
$det = new DETALII_SEJUR();
/*$id_tara=get_id_tara(desfa_link($_REQUEST['tara']));
if($id_tara) $det->getTara($_REQUEST['tara']);*/
$detalii=$det->select_det_sejur($id_oferta);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');
$id_hotel=$detalii['id_hotel'];
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$link_hotel = substr($sitepath,0,-1).make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire']);
$link_oferta = substr($sitepath,0,-1).make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $detalii['denumire_scurta'], $id_oferta);

/*if($_SERVER['REQUEST_URI']<>$link_oferta) {
	header("HTTP/1.1 301 Moved Permanently");
	header('Location: '.$link_oferta);
	exit();
}*/
/*if($_REQUEST['tara']<>fa_link_oferta($detalii_hotel['tara']) || $_REQUEST['localitate']<>fa_link_oferta($detalii_hotel['localitate'])) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}*/
if(!isset($detalii['id_sejur']) and !isset($detalii['denumire'])) {
	$link_fara_oferta=$_SERVER['REQUEST_URI'];
	$plorp = substr(strrchr($link_fara_oferta,'/'), 1);
	$string = substr($link_fara_oferta, 0, - strlen($plorp));
	header("HTTP/1.1 301 Moved Permanently");
	header('Location: '.$string);
	//header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	//$handle = curl_init($sitepath.'404.php');
	//curl_exec($handle);
	exit();
}

$metas_title = $detalii_hotel['denumire'].' '.$detalii_hotel['stele'].' stele '.$detalii['denumire_scurta'];
$metas_description = $detalii['denumire_scurta'].' '.$detalii_hotel['denumire'].' '.str_repeat('★',$detalii_hotel['stele']).' '.$detalii['nr_zile'].' zile, masa '.$detalii['masa'].', transport '.$detalii['transport'];
if($detalii['taxa_avion']['inclus']=='da') {
	$metas_description = $metas_description.' cu toate taxele incluse';
}

include($_SERVER['DOCUMENT_ROOT'].'/includes/hoteluri/reviews_top.php');
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?></title>
<meta name="description" content="<?php echo $metas_description; ?>" />
<link rel="canonical" href="<?php echo $link_hotel; ?>" />


	
	
	


<meta property="og:title" content="<?php echo $metas_title; ?>" />
<meta property="og:description" content="<?php echo $metas_description; ?>" />
<meta property="og:url" content="<?php echo curPageURL(); ?>" />
<meta property="og:image" content="<?php echo $sitepath.'img_prima_hotel/'.$detalii_hotel['poza1']; ?>" />
<meta property="og:site_name" content="OcaziiTuristice.ro" />
<meta property="og:type"          content="website" />
<?php /*?><meta property="og:type" content="ocaziituristice:trip" /><?php */?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>


<body onLoad="load_submenu()">

<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>

<?php if(isset($_GET['cerere_oferta'])) { ?>
	<div class="NEW-column-full pad20" style="margin-bottom:10px;">
      <h2>Multumim pentru cererea efectuata! <span class="green">Care sunt urmatorii pasi?</span></h2>
      <div class="NEW-orange NEW-round8px" style="border:1px solid #ffd0d0; padding:10px 5px;">
        <img src="<?php echo $imgpath; ?>/cerere_oferta.jpg" class="float-left" style="margin:10px;" />
        <div class="float-right" style="margin:0 10px 0 50px; width:260px;">
          <p>Pentru orice nelamuriri sau intrebari nu ezita sa ne contactezi:</p>
          <strong>Telefon:</strong> <?php echo $contact_telefon; ?><br />
          <strong>Mobil:</strong> <?php echo $contact_mobil; ?><br />
          <strong>Program:</strong> <?php echo $contact_program; ?>
        </div>
        <p style="padding-top:17px;" class="black">In scurt timp vei fi contactat (telefonic sau prin e-mail) de catre un agent de turism din echipa Ocaziituristice.ro in care ti se va prezenta oferta noastra pentru cererea ta.</p>
        <p class="black">Iti multumim pentru interesul acordat!</p>
        <br class="clear" />
      </div>
      <?php /*?><div class="fb-like-box" data-href="http://www.facebook.com/OcaziiTuristice.ro" data-width="292" data-show-faces="true" data-stream="false" data-header="false" style="float:right;"></div><?php */?>
      <br /><br />
      <h2 class="red">Spune-le si prietenilor tai despre aceasta oferta</h2>
      <br />
      <img src="<?php echo '/thumb_hotel/'.$detalii_hotel['poza1']; ?>" class="images" style="margin:10px;" />
      <h3 style="margin:10px 10px 0 10px;"><?php echo $metas_title; ?></h3>
      <div class="grey italic"><?php echo $sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii['denumire']).'-'.$id_oferta.'.html'; ?></div>
      <div style="float:left; margin:30px 0 0 30px;">
        <a name="fb_share"></a>
	    <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
      </div>
      <br class="clear" />
    </div>
<?php } ?>

    <div class="NEW-column-full">
    <?php if($detalii['oferta_saptamanii']=='da') {
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/detalii_oferta_saptamanii_new.php");
	} else {
		if(($detalii['cazare']=='da' and $detalii['online_prices']=='da') or $detalii['new_layout']=='da') {
			include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/detalii_oferta_new1.php");
		} else {
			include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/detalii_oferta_new.php");
		}
	} ?>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php");  <div id="alteOferteVizitate"></div>?>
   
      <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri_vizualizate_altii.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>

<?php
if( !is_bot() and $link_oferta==(substr($sitepath,0,-1).$_SERVER['REQUEST_URI'] and isset($err_logare_admin) ) ) {
	$sel_ofvisit = "SELECT link FROM oferte_vizitate WHERE id_sesiune_vizitator = '".session_id()."' AND link = '".$link_oferta."' ";
	$que_ofvisit = mysql_query($sel_ofvisit) or die(mysql_error());
	if(mysql_num_rows($que_ofvisit)==0) {
		$que_of_visit = "INSERT INTO oferte_vizitate (id_oferta,id_hotel, link, referrer, data_vizita, ip_vizitator, id_sesiune_vizitator, user_agent) VALUES ('".$id_oferta."', '".$id_hotel."','".substr($sitepath,0,-1).$_SERVER['REQUEST_URI']."', '".$_SERVER['HTTP_REFERER']."', NOW(), '".$_SERVER['REMOTE_ADDR']."', '".session_id()."', '".$_SERVER['HTTP_USER_AGENT']."')";
		$res_of_visit = mysql_query($que_of_visit) or die (mysql_error());
	}
}
?>

<?php /*?><script>
$("#alteOferteVizitate").empty().html('<img src="/images/loader.gif" alt="" />');
$("#alteOferteVizitate").load("/includes/hoteluri_vizualizate_altii.php?id_hotel=<?php echo $id_oferta; ?>");
</script><?php */?>
<?php if($detalii['oferta_saptamanii']=='da') { ?>
<script>
function initialize() {
	<?php if($rowH['latitudine']!='') { ?>
		var mapDiv = document.getElementById('harta-localizare');
		var map = new google.maps.Map(mapDiv, {
			center: new google.maps.LatLng(<?php echo $rowH['latitudine']; ?>, <?php echo $rowH['longitudine']; ?>),
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		
		var content = '<h4 class="blue"><?php echo $rowH['nume']; ?> <img src="<?php echo $imgpath; ?>/spacer.gif" class="stele-mici-<?php echo $rowH['stele']; ?>" alt="" /></h4><img src="<?php echo $sitepath_parinte; ?>thumb_hotel/<?php echo $rowH['poza1']; ?>" style="float:left; margin-right:10px; width:60px;" /><?php echo $rowH['adresa'].' Nr. '. $rowH['nr'].', '.$rowH['localitate'].', '.$rowH['tara']; ?>';
		
		var infowindow = new google.maps.InfoWindow({
			content: content
		});
		
		var image = '<?php echo $imgpath; ?>/icon_maps_hotel.png';
		var marker = new google.maps.Marker({
			map: map,
			position: map.getCenter(),
			icon: image
		});
		
		infowindow.open(map, marker);
		
		google.maps.event.addListener(map, "idle", function(){
			marker.setMap(map);
		});
	<?php } ?>
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php } ?>
<?php if($detalii_hotel['cod_remarketing']) echo $detalii_hotel['cod_remarketing']; ?>
</body>
</html>
