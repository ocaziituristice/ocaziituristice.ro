<?php include_once($_SERVER['DOCUMENT_ROOT']."/redirect.php"); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/peste_tot.php"); ?>
<?php //include_once($_SERVER['DOCUMENT_ROOT']."/statistici/statistica.php"); ?><?php
$connection1=$_SERVER['DOCUMENT_ROOT']."/settings/connect.php";
include_once($connection1);
$connection2=$_SERVER['DOCUMENT_ROOT']."/settings/functions.php";
include_once($connection2);
$usuer_fizic=$_SESSION['id_persoana_fizica'];
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/header_charset.php"); ?>
<title>Concurs - Termeni si conditii - Ocazii Turistice</title>
<?php
$link="http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI'];
$key=new Keywords_by_link($link);
$key->load();
?>
<meta name="description" content="" />
<meta name="keywords" content="" />
<META NAME="Publisher" CONTENT="ocaziituristice.ro" />
<META NAME="ROBOTS" CONTENT="INDEX,FOLLOW" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/addins_head.php'); ?>

<!-- rezervari -->
<link rel="stylesheet" href="/lightbox.css" media="screen,projection" type="text/css" />
<script type="text/javascript" src="/js/prototype.js"></script>
<script type="text/javascript" src="/js/lightbox.js"></script>
<!-- rezervari -->

</head>

<body>
<?php include($_SERVER['DOCUMENT_ROOT'].'/addins_body_top.php'); ?>
<div id="header">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/header_index.php"); ?>
</div>
<div id="middle">
  <div id="middleInner">
  <table class="mainTableClass" border="0" cellspacing="0" cellpadding="0">
	<tr>
      <td colspan="2" align="left" valign="top">
        <div class="breadcrumb">
         <?php include_once($_SERVER['DOCUMENT_ROOT']."/navigator_new.php"); ?> 
        </div>
      </td>
    </tr>
	<tr>
	  <td class="mainTableColumnLeft" align="left" valign="top">
      <div id="columnLeft">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/stanga_main_concurs_termeni_si_conditii.php"); ?>
      </div>
      </td>
	  <td class="mainTableColumnRight" align="left" valign="top">
        <div id="columnRight">
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta.php"); ?>
        <? if(!$usuer_fizic) { /*include_once($_SERVER['DOCUMENT_ROOT']."/dreapta_main_index.php");  */}
		else { include_once($_SERVER['DOCUMENT_ROOT']."/profil/dreapta_main_profil.php"); } ?>
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_main_reclama.php"); ?>
        </div>
      </td>
	</tr>
  </table>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/footer_index.php"); ?>
</div>
</body>
</html>
