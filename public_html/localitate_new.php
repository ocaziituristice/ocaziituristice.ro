<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_localitate.php');
$dt=desfa_link($_REQUEST['tari']);
$id_tara=get_id_tara($dt);
$zona=desfa_link($_REQUEST['zone']);
$id_zona=get_id_zona($zona, $id_tara);
$dl=desfa_link($_REQUEST['oras']);
$id_localitate=get_id_localitate($dl, $id_zona);
if(!$id_localitate) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}
$det=new AFISARE_LOCALITATE($id_tara, $id_localitate);
$param=$det->detalii_localitate();
$tara=$param['denumire_tara']; 
$localitate=$param['denumire']; 
$link_p=$sitepath.'sejur-'.$_REQUEST['tari'].'/'.$_REQUEST['zone'].'/'.$_REQUEST['oras'].'/';
if($_REQUEST['tip']) { $tip=desfa_link($_REQUEST['tip']);
$id_tip=get_id_tip_sejur($tip); }
$tip=$det->get_tip_oferta_tari_localitate();
if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') {
$trans=desfa_link(str_replace('_','-',$_REQUEST['transport']));
$id_transport=get_id_transport($trans);
} else { $trans='toate'; $id_transport=''; }
if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') { $stele=$_REQUEST['stele']; $nr_stele=$_REQUEST['stele']; } else { $stele='toate'; $nr_stele=''; }
if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') { $masa=desfa_link(str_replace('_','-',$_REQUEST['masa'])); $nmasa=desfa_link($_REQUEST['masa']); } else { $masa='toate'; $nmasa=''; } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); 
$parametru['pagina']='new_sejururi1';	
$new_formula_key = new new_formula_keywords_new($parametru);
$keyword_nice = $new_formula_key->do_nice_sejur4_new(); ?>
<title><?php echo $keyword_nice[1]; ?></title>
<meta name="title" content="<? echo $keyword_nice[1]; ?>" />
<meta name="description" content="<?php echo $keyword_nice[0]; ?>" />
<meta name="keywords" content="<?php echo $keyword_nice[2]; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="initialize(); load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/localitate_new.php"); ?>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>