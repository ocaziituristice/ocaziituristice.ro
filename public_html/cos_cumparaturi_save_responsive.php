<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/peste_tot.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_sejur.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/class_sejururi/class_sejur.php' );

/*** check login admin ***/
$logare_admin     = new LOGIN( 'useri' );
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/
?>
<!DOCTYPE html>
<html lang="ro">
<head>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/header_charset.php" ); ?>
    <title>Ofertele mele - Wishlist | Ocaziituristice.ro</title>
	<?php require_once( "includes/header/header_responsive.php" ); ?>
    <meta name="robots" content="noindex, nofollow">
</head>

<body class="offers-page">
<?php include( $_SERVER['DOCUMENT_ROOT'] . '/includes/header/admin_bar_responsive.php' ); ?>

<?php // Header ?>
<header>
	<?php require( "includes/header/meniu_header_responsive.php" ); ?>
</header>

<?php // Breadcrumbs and general search ?>
<div class="layout">
	<?php require( "includes/header/breadcrumb_responsive_intreg.php" ); ?>
</div>


<div class="layout">
    <div class="layout-wrapper">
        <h1 class="blue float-left">Ofertele mele - Wishlist</h1>
        <div class="float-right">
			<?php if ( isset( $_SESSION['page_back'] ) ) {
				echo '<a href="' . $_SESSION['page_back'] . '" class="btn offers-btn">&lt;&nbsp; Inapoi la oferte</a>';
			} else {
				echo '<a href="' . $_SESSION['page_back_offer'] . '" class="btn offers-btn">&lt;&nbsp; Inapoi la detaliile ofertei</a>';
			} ?>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php if ( isset( $_COOKIE['cos_cumparaturi'] ) ) : ?>
    <div class="layout">
        <div class="layout-wrapper">
            <div id="accordion">
                <h3 class="bigger-15em black">BINE DE ȘTIUT ! <span class="smaller-08em">(click aici)</span></h3>
                <div class="bigger-12em" style="line-height:1.5em;">
                    <ul>
                        <li>Sejururile adăugate în această listă (wishlist) <strong>nu sunt
                                rezervate</strong></li>
                        <li>Apasă butonul <strong>"Rezervă"</strong> din dreptul sejurului dorit pentru a
                            efectua rezervarea acesteia
                        </li>
                        <li>Aici poți adăuga <strong>un număr nelimitat de sejururi</strong> de la un număr
                            nelimitat de hoteluri
                        </li>
                        <li>Perioada de valabilitate a acestei liste este de 30 zile. <strong>Nu rata
                                ofertele noastre speciale!</strong></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="layout">
        <div class="layout-wrapper">
			<?php
			$det    = new DETALII_SEJUR();
			$arrCos = explode( '+;++;+', $_COOKIE['cos_cumparaturi'] );
			foreach ( $arrCos as $k_arrCos => $v_arrCos ) {
				$det_cos[ $k_arrCos ]        = explode( '##', base64_decode( $v_arrCos ) );
				$id_oferta_cos[ $k_arrCos ]  = $det_cos[ $k_arrCos ][0]; // id oferta
				$cos_id_pret[ $k_arrCos ]    = $det_cos[ $k_arrCos ][1]; // id pret
				$cos_plecare[ $k_arrCos ]    = $det_cos[ $k_arrCos ][2]; // tip preturi (plecari sau perioade)
				$cos_data[ $k_arrCos ]       = $det_cos[ $k_arrCos ][3]; // data plecarii
				$cos_zile[ $k_arrCos ]       = $det_cos[ $k_arrCos ][4]; // nr nopti
				$cos_pret[ $k_arrCos ]       = $det_cos[ $k_arrCos ][5]; // pret
				$cos_id_cam[ $k_arrCos ]     = $det_cos[ $k_arrCos ][6]; // id_cam
				$cos_nr_adulti[ $k_arrCos ]  = $det_cos[ $k_arrCos ][7]; // nr_adulti
				$cos_nr_copii[ $k_arrCos ]   = $det_cos[ $k_arrCos ][8]; // nr_copii
				$cos_copil[1][ $k_arrCos ]   = $det_cos[ $k_arrCos ][9]; // copil1
				$cos_copil[2][ $k_arrCos ]   = $det_cos[ $k_arrCos ][10]; // copil2
				$cos_copil[3][ $k_arrCos ]   = $det_cos[ $k_arrCos ][11]; // copil3
				$cos_masa[ $k_arrCos ]       = $det_cos[ $k_arrCos ][12]; // tip masa
				$cos_den_camera[ $k_arrCos ] = $det_cos[ $k_arrCos ][13]; // denumire camera
				$cos_data_early_booking[ $k_arrCos ] = $det_cos[ $k_arrCos ][14]; // data_early_booking
				$cos_procent_reducere[ $k_arrCos ] = $det_cos[ $k_arrCos ][15]; // data_early_booking
				

				$link_sterge[ $k_arrCos ] = '/ofertele-mele/?delete_off=' . $k_arrCos;

				$detalii     = $det->select_det_sejur( $id_oferta_cos[ $k_arrCos ] );
				$avans_plata = $detalii['avans_plata'];
				$nr_zile_plata=$detalii['nr_zile_plata'];
				$denumire_plus='';
//print_r( base64_decode( $v_arrCos ));
				$sel_off = "SELECT
        oferte.id_hotel,
		oferte.denumire_scurta,
		oferte.masa,
		oferte.moneda,
		transport.denumire AS den_transport,
		hoteluri.nume AS den_hotel,
		hoteluri.poza1,
		hoteluri.stele,
		hoteluri.tip_unitate,
		localitati.denumire AS den_localitate,
		zone.denumire AS den_zona,
		tari.denumire AS den_tara,
		continente.nume_continent AS den_continent
		FROM oferte
		INNER JOIN hoteluri ON hoteluri.id_hotel = oferte.id_hotel
		INNER JOIN localitati ON localitati.id_localitate = hoteluri.locatie_id
		INNER JOIN zone ON zone.id_zona = localitati.id_zona
		INNER JOIN tari ON tari.id_tara = zone.id_tara
		INNER JOIN continente ON continente.id_continent = tari.id_continent
		INNER JOIN transport ON transport.id_trans = oferte.id_transport
		WHERE oferte.id_oferta = '" . $id_oferta_cos[ $k_arrCos ] . "'
		";
				$que_off = mysql_query( $sel_off ) or die( mysql_error() );
				$row_off = mysql_fetch_array( $que_off );

				$sel_zbor = "SELECT * FROM pret_cache WHERE id_hotel = '" . $row_off['id_hotel'] . "' and id_oferta='".$id_oferta_cos[ $k_arrCos ]."'";
				$que_zbor = mysql_query( $sel_zbor ) or die( mysql_error() );
				$sel_zbor = mysql_fetch_array( $que_zbor );

				if ( $row_off['den_transport'] == 'Avion' ) {
					unset($detalii_zbor);
					$detalii_zbor = $det->get_avion_sejur( $id_oferta_cos[ $k_arrCos ] );

					unset($found);
					$found   = find_items( $detalii_zbor, 'tip', 'dus', $found = array() );
					$plecare='';
					$plecare = '&plecare-avion=' . strtolower( $found['0']['denumire_loc_plecare'] );
					
					$denumire_plus.=' plecare din '.ucfirst($found['0']['denumire_loc_plecare']);

				}
		// echo 'aa'.$cos_data_early_booking[ $k_arrCos ].'--';date('d-m-Y',strtotime($cos_data_early_booking[ $k_arrCos ] ));
		 if ($cos_data_early_booking[ $k_arrCos ]!='..' and strtotime($cos_data_early_booking[ $k_arrCos ] )>date() ) 
		 {
		 $denumire_plus.=' Early Booking '.date( 'd-m-Y', strtotime($cos_data_early_booking[ $k_arrCos ] ) );
		 }
		
 				$sel_tip_camera = "SELECT
		pret_pivot_adaugat.*,
		tip_camera.denumire AS denumire_camera
		FROM pret_pivot_adaugat
		LEFT JOIN tip_camera ON tip_camera.id_camera=pret_pivot_adaugat.tip_camera
		WHERE pret_pivot_adaugat.id_pret='" . $cos_id_pret[ $k_arrCos ] . "' ";
				$que_tip_camera = mysql_query( $sel_tip_camera ) or die( mysql_error() );
				$row_tip_camera = mysql_fetch_array( $que_tip_camera );

				$denumire_camera = $cos_den_camera[ $k_arrCos ];

				if ( $row_off['tip_unitate'] == 'Circuit' ) {
					$link_oferta[ $k_arrCos ]    = make_link_circuit( $row_off['den_hotel'], $id_oferta_cos[ $k_arrCos ] );
					$link_rezervare[ $k_arrCos ] = make_link_oferta( $row_off['den_localitate'], $row_off['den_hotel'], null, null ) . 'rezervare-' . $id_oferta_cos[ $k_arrCos ] . '_' . base64_encode( $cos_id_pret[ $k_arrCos ] . '_' . $cos_plecare[ $k_arrCos ] . '_' . $cos_data[ $k_arrCos ] . '_' . $cos_zile[ $k_arrCos ] . '_' . $cos_pret[ $k_arrCos ] );
				} else {
					$link_oferta[ $k_arrCos ]    = make_link_oferta( $row_off['den_localitate'], $row_off['den_hotel'], null, $id_oferta_cos[ $k_arrCos ] ) . '?plecdata=' . date( 'd.m.Y', strtotime( $cos_data[ $k_arrCos ] ) ) . $plecare . '&transport=' . $row_off['den_transport'] . '&pleczile=' . $cos_zile[ $k_arrCos ] . '&adulti=' . $cos_nr_adulti[ $k_arrCos ] . '&copii=' . $cos_nr_copii[ $k_arrCos ] . '&age[0]=' . $cos_copil[1][ $k_arrCos ] . '&age[1]=' . $cos_copil[2][ $k_arrCos ] . '&age[2]=' . $cos_copil[3][ $k_arrCos ];
					$link_rezervare[ $k_arrCos ] = make_link_oferta( $row_off['den_localitate'], $row_off['den_hotel'], null, null ) . 'rezervare-' . $id_oferta_cos[ $k_arrCos ] . '_' . base64_encode( $cos_id_pret[ $k_arrCos ] . '_' . $cos_plecare[ $k_arrCos ] . '_' . $cos_data[ $k_arrCos ] . '_' . $cos_zile[ $k_arrCos ] . '_' . $cos_pret[ $k_arrCos ] . '_' . $cos_id_cam[ $k_arrCos ] . '_' . $cos_nr_adulti[ $k_arrCos ] . '_' . $cos_nr_copii[ $k_arrCos ] . '_' . $cos_copil[1][ $k_arrCos ] . '_' . $cos_copil[2][ $k_arrCos ] . '_' . $cos_copil[3][ $k_arrCos ] . '_' . $cos_masa[ $k_arrCos ] . '_' . $denumire_camera );
				}
				?>
                <div class="afisare-oferta">
                    <div class="col-md-12 columns">
                        <h2 class="hotel-name">
                            <a href="<?php echo $link_oferta[ $k_arrCos ]; ?>">
								<?php echo $row_off['denumire_scurta'] .$denumire_plus.' - ' . $cos_zile[ $k_arrCos ] . ' nopți'; ?>
                            </a>
                        </h2>
                    </div>
                    <div class="col-md-4 columns">
                        <a href="<?php echo $link_oferta[ $k_arrCos ]; ?>">
                            <img src="/img_hotel/<?php echo $row_off['poza1']; ?>"
                                 alt="<?php echo $row_off['den_hotel']; ?>" class="poza">
                        </a>
                    </div>
                    <div class="col-md-4 columns">
                        <div class="circuit-details">
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-building-o" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            <?php echo $row_off['den_hotel']; ?>
                                            <img src="/images/spacer.gif"
                                                 class="stele-mici-<?php echo $row_off['stele']; ?>"
                                                 alt="<?php echo $row_off['stele']; ?> stele"/>
                                        </span>
                                    </span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            <?php echo $row_off['den_localitate'] . ' / ' . $row_off['den_zona'] . ' / ' . $row_off['den_tara']; ?>
                                        </span>
                                    </span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-cutlery" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Masă:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo $cos_masa[ $k_arrCos ]; ?></span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-bus" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Transport:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo $row_off['den_transport']; ?></span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Dată plecare:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo denLuniRo( date( 'd F Y', strtotime( $cos_data[ $k_arrCos ] ) ) ); ?></span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-bed" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Tip cameră:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo $denumire_camera; ?></span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Grad ocupare:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result">
                                        <?php echo $cos_nr_adulti[ $k_arrCos ] . ' adult';
                                        if ( $cos_nr_adulti[ $k_arrCos ] > 1 ) {
	                                        echo 'i';
                                        }
                                        if ( $cos_nr_copii[ $k_arrCos ] > 0 ) {
	                                        echo ' + ' . $cos_nr_copii[ $k_arrCos ] . ' copi';
	                                        if ( $cos_nr_copii[ $k_arrCos ] == 1 ) {
		                                        echo 'l';
	                                        } else {
		                                        echo 'i';
	                                        }
                                        }
                                        ?>
                                    </span>
                                <span class="clear" style="display: block"></span>
                            </p>
							<?php if ( $cos_nr_copii[ $k_arrCos ] > 0 ) : ?>
								<?php for ( $i = 1; $i <= $cos_nr_copii[ $k_arrCos ]; $i ++ ) : ?>
                                    <p class="titlu">
                                            <span class="name">
                                                <i class="fa fa-child" aria-hidden="true"></i>
                                                <span class="bigger-12em bold">
                                                    Copil <?php echo $i ?> :
                                                </span>
                                            </span>
                                        <span class="bigger-12em green bold result">
                                                <?php echo $cos_copil[ $i ][ $k_arrCos ]; ?>
                                                <?php echo $cos_copil[ $i ][ $k_arrCos ] < 2 ? ' an' : ' ani' ?>
                                            </span>
                                        <span class="clear" style="display: block"></span>
                                    </p>
								<?php endfor; ?>
							<?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-4 columns">
                        <div class="price-wrapper">
                            <div class="price text-center black">Tarif / <?php echo $denumire_camera; ?><br>
                                <span class="red"><?php echo new_price( $cos_pret[ $k_arrCos ] ) . ' ' . moneda( $row_off['moneda'] ); ?></span><br/>
								<?php echo $cos_zile[ $k_arrCos ] . ' nopți'; ?>
                            </div>
                            <div class="book-now text-center">
                                <a href="<?php echo $link_oferta[ $k_arrCos ]; ?>" class="details btn">Vezi detalii</a>
                                <a href="<?php echo $link_sterge[ $k_arrCos ]; ?>" class="delete">Șterge oferta</a>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>
                    <div class="col-md-12">
                        <div class="services">
                            <div class="col-md-4 columns">
                                <h5><strong>Servicii incluse</strong></h5>
                                <div class="included_services_list">
                                    <?php $services        = $det->offer_services( $id_hotel, $detalii['id_transport'], $cos_zile[ $k_arrCos ] , get_den_localitate( $found['0'] ['id_loc_plecare'] ), get_den_localitate( $found['0'] ['id_loc_sosire'] ), $id_oferta_cos[ $k_arrCos ], $e_circuit, $detalii['servicii_manual'] );
		//							if ( ! $err_logare_admin )  {echo "----".$id_oferta_cos[ $k_arrCos ]; echo '<pre>';print_r($detalii);echo '</pre>'; 
					
					//echo '<pre>';print_r($services);echo '</pre>';
					//echo '<pre>';print_r($found);echo '</pre>';
					
		//			}
?>
                                    
                                    <p>Tip masa inclusa <?php echo $cos_masa[ $k_arrCos ]; ?></p>
                                    <?php foreach ($services['servicii_incluse'] as $servicii_incluse) {
                                   echo  '<p>'.$servicii_incluse.'</p>';
                                    }?>
                                </div>
                            </div>
                            <div class="col-md-4 columns">
                                <h5><strong>Conditii de anulare</strong></h5>
                                <div class="included_services_list">
<?php /*?>                                    <p>Pentru anularea rezervarii in perioada <strong>21 Decembrie 2017 - 20 Mai
                                            2018</strong> penalizarea este de <strong>20% din valoare totala a
                                            sejurului</strong></p>
                                    <p>Pentru anularea rezervarii in perioada
                                        <strong>21 Mai 2018 - 03 Iunie 2018</strong>
                                        penalizarea este de
                                        <strong>50% din valoare totala a sejurului</strong></p>
                                    <p>Pentru anularea rezervarii in perioada
                                        <strong>03 Iunie 2018 - 26 Iunie 2018</strong>
                                        penalizarea este de
                                        <strong>100% din valoare totala a sejurului</strong>
                                    </p><?php */?>
                                </div>
                            </div>
                            <div class="col-md-4 columns">
                                <h5><strong>Conditii de plata</strong></h5>
                                <div class="included_services_list">
									<?php $nr_zile_expira = abs( floor( ( time() - strtotime( $cos_data[ $k_arrCos ] ) ) / ( 60 * 60 * 24 ) ) ); ?>
									<?php if ( $nr_zile_expira > 20 ) : ?>
										<?php if ( $avans_plata > 5 ) : ?>
                                            <p>Avans minim <strong><?php echo $avans_plata ?>%</strong> din valoarea
                                                sejurului
                                                (<strong><?php echo round( $cos_pret[ $k_arrCos ] * $avans_plata / 100 ) . ' ' . $row_off['moneda']; ?></strong>)
                                            </p>
										<?php endif; ?>
                                      <?php  if (strtotime($cos_data_early_booking[ $k_arrCos ] )>date() and $cos_data_early_booking[ $k_arrCos ]!='..'  ) {  ?>
                                      <p>Restul sumei se va achita pana la data
                                                de <br /><strong><?php echo denLuniRo( date( 'd F Y', strtotime($cos_data_early_booking[ $k_arrCos ] ) ) ); ?></strong>
                                            </p>
                                        <?php } else {?>
										<?php if ( strtotime( $cos_data[ $k_arrCos ] ) > strtotime( "+1 day" ) ) : ?>
                                            <p>Restul sumei se va achita pana la data
                                                de <br /><strong ><?php echo denLuniRo( date( 'd F Y', strtotime( '-'.$nr_zile_plata.' days',strtotime($cos_data[ $k_arrCos ]) ) ) ); ?></strong>
                                            </p>
										<?php else: ?>
                                            <p>Daca rezervarea este in sitem Early Booking restul sumei se va schita
                                                pana la sfarsitul perioadei de early booking</p>
                                            <p>Daca oferta este Black Friday se va achita integral in maxim 24 de ore de
                                                la data confirmarii</p>
                                            <p>Daca rezervarea NU ESTE in sistem Early Booking se va achita integral
                                                pana la
                                                data <?php echo denLuniRo( date( 'd F Y', strtotime( $cos_data[ $k_arrCos ] . ' - ' . $nr_zile_plata . ' days' ) ) ); ?></p>
										
                                        <?php endif; ?>
                                        <?php }?>
									<?php else : ?>
                                        <p>Rezervarea trebuie achitata in maxim 24 de ore de la data confirmarii.</p>
									<?php endif; ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
						<?php
						$id_zbor_rez = $sel_zbor['id_zbor'];
					//	if($id_zbor_rez!='') {
						$sel_avioane = "SELECT * from pret_planes_cache where id_zbor='" . $id_zbor_rez . "' and id_hotel='" . $row_off['id_hotel'] . "'";
						$que_avioane = mysql_query( $sel_avioane ) or die( mysql_error() );
						$ii = 0;
						unset($flight);
						
						while ( $row_avioane = mysql_fetch_array( $que_avioane ) ) {
							$flight[ $row_avioane['id_zbor'] ]['flight_bookable']                                  = $row_avioane['flight_bookable'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['companie']     = $row_avioane['companie'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['nr_cursa']     = $row_avioane['nr_cursa'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['from']         = $row_avioane['from'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['to']           = $row_avioane['to'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['data_plecare'] = $row_avioane['data_plecare'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['data_sosire']  = $row_avioane['data_sosire'];
							$ii ++;
						}
						?>
						<?php
						
					

						if ( sizeof( $flight ) > 0 ) {
							if ( count( $flight ) > 1 ) {
								$counter = 1;
								foreach ( $flight as $id_oferta => $zbor ) {
									if ( $zbor['flight_bookable'] == 1 and sizeof( $zbor['plecare'] ) > 0 and $counter == 1 ) {
										echo '<div id="flight' . $id_oferta . '">';
										echo '<table class="search-rooms mar10-0"><tr class="bkg-white"><th class="text-center">Companie</td><th class="text-center">Nr. cursă</th><th class="text-center">Plecare</th><th class="text-center">Sosire</th>';
										$counter_zbor = 1;
										foreach ( $zbor['plecare'] as $k_av_out => $v_av_out ) {
											if ( $counter_zbor == 1 ) {
												echo '<tr class="bkg-grey">';
												echo '<td class="text-center">';
												if ( strlen( get_airline_by_iata( $v_av_out['companie'] ) ) > 0 ) {
													echo '<img src="/images/avion/' . get_airline_by_iata( $v_av_out['companie'] ) . '.jpg" alt=""> ';
												}
												echo '</td>';
												echo '<td class="text-center">' . $v_av_out['nr_cursa'] . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_out['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_out['data_plecare'] ) . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_out['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_out['data_sosire'] ) . '</td>';
												echo '</tr>';
											}
											$counter_zbor ++;
										}
										$counter_zbor = 1;
										foreach ( $zbor['intoarcere'] as $k_av_in => $v_av_in ) {
											if ( $counter_zbor == 1 ) {
												echo '<tr class="bkg-white">';
												echo '<td class="text-center">';
												if ( strlen( get_airline_by_iata( $v_av_in['companie'] ) ) > 0 ) {
													echo '<img src="/images/avion/' . get_airline_by_iata( $v_av_in['companie'] ) . '.jpg" alt=""> ';
												}
												echo '</td>';
												echo '<td class="text-center">' . $v_av_in['nr_cursa'] . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_in['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_in['data_plecare'] ) . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_in['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_in['data_sosire'] ) . '</td>';
												echo '</tr>';
											}
											$counter_zbor ++;
										}
										echo '</table>';
										echo '</div>';
									}
									$counter ++;
								}
							} else {
								foreach ( $flight as $id_oferta => $zbor ) {
									if ( $zbor['flight_bookable'] == 1 and sizeof( $zbor['plecare'] ) > 0 ) {
										echo '<div id="flight' . $id_oferta . '">';
										echo '<table class="search-rooms mar10-0"><tr class="bkg-white"><th class="text-center">Companie</td><th class="text-center">Nr. cursă</th><th class="text-center">Plecare</th><th class="text-center">Sosire</th>';
										$counter_zbor = 1;
										foreach ( $zbor['plecare'] as $k_av_out => $v_av_out ) {
											if ( $counter_zbor == 1 ) {
												echo '<tr class="bkg-grey">';
												echo '<td class="text-center">';
												if ( strlen( get_airline_by_iata( $v_av_out['companie'] ) ) > 0 ) {
													echo '<img src="/images/avion/' . get_airline_by_iata( $v_av_out['companie'] ) . '.jpg" alt=""> ';
												}
												echo '</td>';
												echo '<td class="text-center">' . $v_av_out['nr_cursa'] . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_out['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_out['data_plecare'] ) . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_out['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_out['data_sosire'] ) . '</td>';
												echo '</tr>';
											}
											$counter_zbor ++;
										}
										$counter_zbor = 1;
										foreach ( $zbor['intoarcere'] as $k_av_in => $v_av_in ) {
											if ( $counter_zbor == 1 ) {
												echo '<tr class="bkg-white">';
												echo '<td class="text-center">';
												if ( strlen( get_airline_by_iata( $v_av_in['companie'] ) ) > 0 ) {
													echo '<img src="/images/avion/' . get_airline_by_iata( $v_av_in['companie'] ) . '.jpg" alt=""> ';
												}
												echo '</td>';
												echo '<td class="text-center">' . $v_av_in['nr_cursa'] . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_in['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_in['data_plecare'] ) . '</td>';
												echo '<td class="text-center"><strong>' . $v_av_in['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_av_in['data_sosire'] ) . '</td>';
												echo '</tr>';
											}
											$counter_zbor ++;
										}
										echo '</table>';
										echo '</div>';
									}
								}
							}

						}
						?>
                    </div>
                </div>
				<?php @mysql_free_result( $que_off );
			}
			if ( isset( $_GET['delete_off'] ) ) {
				$upd_cos_cump = "UPDATE cos_cumparaturi SET data_stergere = NOW(), tip_stergere = 'individual' WHERE cookie = '" . $arrCos[ $_GET['delete_off'] ] . "' AND ip = '" . $_SERVER['REMOTE_ADDR'] . "' AND browser_info = '" . $_SERVER['HTTP_USER_AGENT'] . "' ";
				$rez_cos_cump = mysql_query( $upd_cos_cump ) or die ( mysql_error() );

				unset( $arrCos[ $_GET['delete_off'] ] );

				$cos_cumparaturi = '';
				foreach ( $arrCos as $key_arr => $value_arr ) {
					$cos_cumparaturi .= $value_arr . '+;++;+';
				}
				$cos_cumparaturi = substr( $cos_cumparaturi, 0, - 6 );
				setcookie( 'cos_cumparaturi', $cos_cumparaturi, time() + 2592000, '/', 'ocaziituristice.ro' );
				redirect_php( '/ofertele-mele/' );
			}
			?>
        </div>
    </div>
<?php else : ?>
    <div class="layout">
        <div class="layout-wrapper">
            <h3 style="text-indent:15px;">Lista cu ofertele tale (wishlist) este goală, dar nu trebuie să
                fie așa.</h3>
            <p class="text bigger-11em">Avem foarte multe oferte și suntem convinși că, cel puțin una, este
                pe placul tău. Insă, dacă nu găsești nici o ofertă care să iți satisfacă toate cerințele, ne
                poți <a href="/contact.html" class="link-blue">contacta</a> și te asigurăm că echipa noastră
                va face tot posibilul pentru ca tu să devii clientul nostru mulțumit.</p>
            <img src="/images/img-adauga-in-cos.jpg" class="float-right" alt="adauga in cos">
            <br><br>
            <p class="text bigger-11em float-left" style="width:470px;">Pentru a adăuga oferte în
                <strong>Whislist-ul</strong>
                de pe Ocaziituristice.ro, începe prin a căuta ofertele care te interesează (<a href="/"
                                                                                               class="link-blue">click
                    aici pentru căutare</a>), apoi caută butonul <strong>"Adaugă la comparație"</strong> din
                detaliile ofertei (vezi imaginea alăturată).</p>
            <p class="text italic float-left" style="width:470px;">Ofertele care nu conțin acest buton
                necesită confirmarea unui agent de turism asupra prețului final.</p>
            <br class="clear">
            <ul class="bigger-11em bold mar10" style="margin-bottom:0; padding-bottom:0;">
                <li style="line-height:1.7em;"><a href="/" class="link-blue">Răsfoiește întreg "catalogul"
                        nostru online</a></li>
                <li style="line-height:1.7em;"><a href="/sejur-romania/" class="link-blue">Turism intern.
                        Caută oferte doar în Romania</a></li>
                <li style="line-height:1.7em;"><a href="/oferte-turism-extern/" class="link-blue">Turism
                        extern. Caută oferte în țările vecine sau pe meleaguri îndepărtate</a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
<?php endif; ?>

<div class="layout">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/oferte_vizualizate_responsive.php" ); ?>
</div>

<?php // Footer ?>
<?php require_once( "includes/footer/footer_responsive.php" ); ?>
<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

<script>
    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            active: false
        });
    });
</script>
</body>
</html>
<?php /*}*/ ?>
