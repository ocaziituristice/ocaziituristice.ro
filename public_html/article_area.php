<?php 
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/peste_tot.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );

?>
<?php
$link_area = $_REQUEST['link_area'];
$denumire = ucwords($link_area);

$sel_article = "SELECT articles.*
FROM articles
WHERE articles.activ = 'da'
AND articles.link_area = '".$link_area."'
";
$que_article = mysql_query($sel_article) or die(mysql_error());
$row_article = mysql_fetch_array($que_article);

$id_tara = $row_article['id_tara'];
$id_zona = $row_article['id_zona'];
$id_localitate = $row_article['id_localitate'];

$den_tara = get_den_tara($id_tara);
$link_tara = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';

if($id_localitate>0) {
	$den_destinatie = get_den_localitate($id_localitate);
	$link_destinatie = '/cazare-'.fa_link(get_den_localitate($id_localitate)).'/';
	//if(fa_link(get_den_zona($id_zona))!=fa_link(get_den_tara($id_tara))) $link_destinatie .= fa_link(get_den_zona($id_zona)).'/'.fa_link(get_den_localitate($id_localitate)).'/';
	//if(fa_link(get_den_zona($id_localitate))!=fa_link(get_den_tara($id_zona))) $link_destinatie .= fa_link(get_den_localitate($id_localitate)).'/';
	$den_loc = get_den_localitate($id_localitate);
	$check_weather = check_weather($id_tara, $id_zona);
	$link_weather = '/vremea-in-'.fa_link(get_den_zona($id_zona)).'_'.$id_tara.'.html';
} else if($id_zona>0) {
	$den_destinatie = get_den_zona($id_zona);
	$link_destinatie = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';
	if(fa_link(get_den_zona($id_zona))!=fa_link(get_den_tara($id_tara))) $link_destinatie .= fa_link(get_den_zona($id_zona)).'/';
	$den_zona = get_den_zona($id_zona);
	$check_weather = check_weather($id_tara, $id_zona);
	$link_weather = '/vremea-in-'.fa_link(get_den_zona($id_zona)).'_'.$id_tara.'.html';
} else if($id_tara>0) {
	$den_destinatie = get_den_tara($id_tara);
	$link_destinatie = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';
	$den_tara = get_den_tara($id_tara);
	$check_weather = check_weather($id_tara);
	$link_weather = '/vremea-in-'.fa_link(get_den_tara($id_tara)).'.html';
}
?>


<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<?php
	
	require_once( "includes/header/header_responsive.php" );
?>
<title>Ghid turistic <?php echo $den_destinatie; ?></title>


</head>

<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header/admin_bar_responsive.php'); ?>

	<?php // Header ?>
	<header>
		<?php require( "includes/header/meniu_header_responsive.php" ); ?>
	</header>

	<?php // Breadcrumbs and general search ?>
	<div class="layout">
    		<?php $watermark_cautare = 'Cauta hoteluri din ' . $den_tara;?>

		<?php //require( "includes/header/breadcrumb_responsive_intreg.php" ); ?>

		<?php require( "includes/header/breadcrumb_responsive.php" ); ?>
		<?php require( "includes/header/search_responsive.php" ); ?>    
    </div>

	<div class="layout">
		<div id="payment">
       
       <span class="titlu_modala"><h1>Ghid Turistic <?php echo $den_destinatie; ?> </h1></span> 
    
         <?php
		  $sel_articole = "SELECT * FROM articles WHERE activ = 'da' AND link_area = '".$link_area."' ORDER BY id_article DESC ";
		  $que_articole = mysql_query($sel_articole) or die(mysql_error());
		  while($row_articole = mysql_fetch_array($que_articole)) {
			  $start_first_part = strpos(html_entity_decode($row_articole['descriere']), '<p>');
			  $end_first_part = strpos(html_entity_decode($row_articole['descriere']), '</p>', $start_first_part);
			  $desc = substr(html_entity_decode($row_articole['descriere']), $start_first_part, $end_first_part - $start_first_part + 4);
			  $desc = htmlspecialchars_decode(strip_tags($desc));
			  
			  $start_first_img = strpos(html_entity_decode($row_articole['descriere']), '<div><img');
			  $end_first_img = strpos(html_entity_decode($row_articole['descriere']), '/></div>', $start_first_img);
			  $first_img = substr(html_entity_decode($row_articole['descriere']), $start_first_img, $end_first_img - $start_first_img + 8);
			  
			  $link_articol = '/ghid-turistic-'.$link_area.'/'.$row_articole['link'].'.html';
		  ?><div class="pad10 article bigger-12em text-justify">
          <h2 class="blue"><a href="<?php echo $link_articol; ?>" title="<?php echo $row_articole['denumire']; ?>"><?php echo $row_articole['denumire']; ?></a></h2>
          <?php echo $first_img; ?>
          <p class="mar0 pad0"><?php echo $desc; ?></p>
          <div class="text-right"><a href="<?php echo $link_articol; ?>" class="link-blue">citește tot articolul &raquo;</a></div>
          </div>
          <div class="DashLine"></div>
          <?php } ?>
        
        
			
		
        
			
	<!-- de aici  continutul-->			
  
          

           <h2 class="blue">Alte informatii utile despre <?php echo $den_destinatie; ?></h2>   
       <div class="location-filters">
             
          </div>   
       

		</div>
      <?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/oferte_vizualizate_responsive.php" ); ?>  
        
	</div>

	<?php // Footer ?>
    <?php require_once( "includes/newsletter_responsive.php" ); ?>

	<?php require_once( "includes/footer/footer_responsive.php" ); ?>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

</body>
</html>
