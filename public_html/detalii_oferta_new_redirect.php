<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
setlocale(LC_TIME, array('ro.utf-8', 'ro_RO.UTF-8', 'ro_RO.utf-8', 'ro', 'ro_RO', 'ro_RO.ISO8859-2'));
$id_oferta=$_REQUEST['id_oferta'];
$det = new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');
$id_hotel=$detalii['id_hotel'];
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$link_hotel=$sitepath.'hoteluri-'.fa_link_oferta($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/';
$link_oferta=$sitepath.'hoteluri-'.fa_link_oferta($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'.fa_link_oferta($detalii['denumire_scurta']).'-'.$id_oferta.'.html';

if($_SERVER['REQUEST_URI']<>$link_oferta) {
	header("HTTP/1.1 301 Moved Permanently");
	header('Location: '.$link_oferta);
	exit();
}
?>