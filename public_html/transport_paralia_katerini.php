<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Transport Paralia Katerini | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Transport Bulgaria pe litoral si la munte prin <?php echo $denumire_agentie; ?> gasesti cele mai bune oferte de transport in Bulgaria" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
<link href="/js/jquery-ui/css/redmond/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#datepicker { font-size:13px; overflow:hidden; }
.ui-datepicker table { border-collapse:collapse !important; }
</style>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <span class="titlu_modala">Transport Paralia Katerini</span>
        
        <div class="Hline"></div>
        
        <div class="text-justify article">
          
          <ul class="chn-linkuri float-right">
            <li class="primul black">Linkuri utile Paralia Katerini</li>
            <li class="second"><a href="/sejur-grecia/paralia-katerini/">Sejur Paralia Katerini</a></li>
          </ul>
               
          <p class="bigger-12em">Agentia noastra va pune la dispozitie mai multe oferte de transport cu autocarul in Paralia Katerini prin partenerii nostri</p>
          <?php /*?><ul class="bold bigger-12em" style="margin:5px 0 0 35px;">
            <li><a href="#ROMADRIA" class="green">ROMADRIA</a></li>
          </ul><?php */?>
          
          <br class="clear">
          
          <div id="ROMADRIA" class="NEW-chn-oferta-green NEW-round8px mar10 pad10 clearfix">
            <h2 class="section-title-green underline blue" style="margin-bottom:10px;">Oferta Transport cu Autocarul - <span class="black">Paralia Katerini</span> - <span class="red">Vara 2014</span></h2>
            <p class="bigger-15em bold">Plecari in perioada <span class="underline">16.05.2014 - 26.09.2014</span>:</p>
            <div id="datepicker"></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td width="50%" align="left" valign="top">
                <?php /*?><ul class="bigger-13em bold blue mar0-10 pad0-10">
                  <li>23.05, 30.05</li>
                  <li>06.06, 13.06, 20.06, 27.06</li>
                  <li>04.07, 11.07, 18.07, 25.07</li>
                  <li>01.08, 08.08, 15.08, 22.08, 29.08</li>
                  <li>05.09, 12.09, 19.09</li>
                </ul><?php */?>
                <p>Se pleaca <strong>Vineri</strong> si se ajunge in Paralia Katerini <strong>Sambata dimineata</strong> (se calatoreste noaptea).</p>
                <p><strong>Retur:</strong> <strong>Sambata in intervalul 15:00 - 18:00</strong></p>
                <p>Turistii trebuie sa se prezinte la locul de imbarcare cu <strong>minim 30 de minute inaintea orei anuntate</strong>.</p>
                </td>
                <td width="50%" align="left" valign="top"><table width="100%" border="1" cellspacing="0" cellpadding="5">
                  <tr>
                    <th width="60%" rowspan="2" align="center" valign="top">Plecare din</th>
                    <th width="40%" align="center" valign="top">DUS - INTORS<br>
                      Retur la 1 saptamana</th>
                    </tr>
                  <tr>
                    <th align="center" valign="top">tarif / adult</th>
                    </tr>
                  <tr>
                    <td align="left" valign="top">BUCURESTI - Timpuri Noi</td>
                    <td align="center" valign="top"><strong class="bigger-13em red">65 €</strong></td>
                    </tr>
                  <tr>
                    <td align="left" valign="top">BRASOV, SINAIA, CAMPINA</td>
                    <td align="center" valign="top"><strong class="bigger-13em red">75 €</strong></td>
                    </tr>
                  <tr>
                    <td align="left" valign="top">PLOIESTI</td>
                    <td align="center" valign="top"><strong class="bigger-13em red">70 €</strong></td>
                    </tr>
                  <tr>
                    <td align="left" valign="top">IASI, ROMAN</td>
                    <td align="center" valign="top"><strong class="bigger-13em red">90 €</strong></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">BACAU, ADJUD</td>
                    <td align="center" valign="top"><strong class="bigger-13em red">85 €</strong></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">FOCSANI</td>
                    <td align="center" valign="top"><strong class="bigger-13em red">80 €</strong></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">RM. SARAT, BUZAU</td>
                    <td align="center" valign="top"><strong class="bigger-13em red">75 €</strong></td>
                  </tr>
                </table></td>
              </tr>
            </table>
            <?php /*?><div class="float-right black uppercase smaller-09em underline">Transport asigurat de <strong>ROMADRIA</strong></div><?php */?>
          </div>
          
          <br class="clear"><br>

        </div>
        
      </div>
    </div>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script src="/js/datepicker_ro.js"></script>
<script>
var array = ["2014-05-23","2014-05-30","2014-06-06","2014-06-13","2014-06-20","2014-06-27","2014-07-04","2014-07-11","2014-07-18","2014-07-25","2014-08-01","2014-08-08","2014-08-15","2014-08-22","2014-08-29","2014-09-05","2014-09-12","2014-09-19"]
$(function() {
	$("#datepicker").datepicker({
		numberOfMonths: 5,
		showButtonPanel: true,
		dateFormat: "yy-mm-dd",
		beforeShowDay: function(date) {
			var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
			return [ array.indexOf(string) !== -1 ]
		},
		minDate: "2014-05-23",
		maxDate: "2014-09-19"
	});
	$(".ui-datepicker-inline").width("930px");
});
</script>
</body>
</html>