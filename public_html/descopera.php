<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii_pt_afisare.php");
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<?php $taraaa = ucwords(desfa_link($_REQUEST['tara'])); ?>
<title><?php echo $taraaa; ?> informatii turistice, cazare <?php echo $taraaa; ?>, oferte <?php echo $taraaa; ?></title>
<meta name="description" content="<?php echo $taraaa; ?> cazare si oferte <?php echo $taraaa; ?> pentru toata lumea la cele mai mici preturi. Gaseste hoteluri in <?php echo $taraaa; ?> potrivite pentru gustul tau." />
<meta name="keywords" content="<?php echo $taraaa; ?>, cazare <?php echo $taraaa; ?>, hotel <?php echo $taraaa; ?>, hoteluri <?php echo $taraaa; ?>, sejur <?php echo $taraaa; ?>, oferte <?php echo $taraaa; ?>" />
<meta name="Publisher" content="ocaziituristice.ro" />

<meta name="language" content="ro" />
<meta name="robots" content="noindex, nofollow">
<meta name="robots" content="noarchive">
<meta name="googlebot" content="nosnippet">
<meta name="googlebot" content="noodp">
<meta name="identifier-url" content="http://www.ocaziituristice.ro/" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
  <table class="mainTableClass" border="0" cellspacing="0" cellpadding="0">
	<tr>
      <td colspan="2" align="left" valign="top">
        <div class="breadcrumb">
         <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
        </div>
      </td>
    </tr>
    <tr>
	  <td class="mainTableColumnLeft" align="left" valign="top">
      <div id="columnLeft">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/descopera/stanga_descopera.php"); ?>
      </div>
      </td>
	  <td class="mainTableColumnRight" align="left" valign="top">
        <div id="columnRight">
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta.php"); ?>
        <?php include($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_main_reclama.php"); ?>
        </div>
      </td>
	</tr>
  </table>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
