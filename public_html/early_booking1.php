<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
if(!$_REQUEST['from']) $from=1; else $from=$_REQUEST['from'];
if(!$nr_pe_pagina=$_COOKIE['nr_pe_pagina']) $nr_pe_pagina=10;
$link_p=$sitepath.'early-booking/';
$link=$link_p; 
$link='?optiuni=da';
$titlu_pag='Early Booking - Rezerva din timp pentru preturi mici!';
//optiuni____
if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') {
$trans=desfa_link(str_replace('_','-',$_REQUEST['transport']));
$transport=$_REQUEST['transport'];
$id_transport=get_id_transport($trans); if($id_transport<1) $err++;
$link=$link."&transport=".$_REQUEST['transport'];
} else { $trans='toate'; $transport=$trans; $id_transport=''; }
if($_REQUEST['plecare-avion'] && $_REQUEST['plecare-avion']<>'toate') { $plecare_avion=$_REQUEST['plecare-avion'];
$plecare=desfa_link($plecare_avion);
$id_loc_plecare_av=get_id_localitate($plecare, '');
if($id_loc_plecare_av<1) $err++;
$link=$link."&plecare-avion=".$_REQUEST['plecare-avion']; } else { $plecare_avion='toate'; $id_loc_plecare_av=''; }
if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') { $stele=$_REQUEST['stele']; $nr_stele=$_REQUEST['stele']; if(!$nr_stele) $err++;
$link=$link."&stele=".$_REQUEST['stele']; } else { $stele='toate'; $nr_stele=''; }
if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') { $masa=desfa_link(str_replace('_','-',$_REQUEST['masa'])); $Lmasa=fa_link($masa); $nmasa=desfa_link($_REQUEST['masa']); if(ereg('[^a-z0-9-]', $_REQUEST['masa'])) $err++;
$link=$link."&masa=".$_REQUEST['masa']; } else { $masa='toate'; $nmasa=''; $Lmasa=$masa; }
$distanta='toate'; $ds=array();
if($err>0) {
header("HTTP/1.0 301 Moved Permanently");
header("Location: ".$link_p);
exit(); }
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php $keyword_nice[1]="Oferte Early Booking - Rezerva din timp pentru preturi mici";
$keyword_nice[0]="Early Booking, oferte Early Booking";
$keyword_nice[2]="Destinatii Early Booking, sejururi Early Booking, cazare Early Booking"; ?>
<title><?php echo $keyword_nice[1];?></title>
<meta name="description" content="<?php echo $keyword_nice[0]; ?>" />
<meta name="keywords" content="<?php echo $keyword_nice[2];?>" />
<meta name="Publisher" content="ocaziituristice.ro" />
<meta name="ROBOTS" content="INDEX,FOLLOW" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php if($_REQUEST['from'] || ($id_transport || $nr_stele || $nmasa || $din_luna || ($distanta && $distanta<>'toate') || $_REQUEST['ordonare'])) { ?> <link rel="canonical" href="<?php echo $link_p; ?>" />
<?php } include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/early_booking1.php"); ?>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>