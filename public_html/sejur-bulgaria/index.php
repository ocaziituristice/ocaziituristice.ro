<?php //error_reporting(E_ALL);
//ini_set('display_errors', 1);?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$den_tara = 'Bulgaria';
$id_tara = get_id_tara($den_tara);

$link_tara = fa_link_oferta($den_tara);
$link_zona_litoral = fa_link_oferta($den_zona_litoral);

if(str_replace("/sejur-","",$_SERVER['REQUEST_URI'])==$link_tara."/") $img_path = 'images/';
	else $img_path = '/sejur-'.$link_tara.'/images/';
	

$sel_dest = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
count(distinct(oferte.id_hotel)) as numar_hoteluri,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
zone.denumire as denumire_zona,
localitati.info_scurte

FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = ".$id_tara."
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_loc'][] = $row_dest['denumire_localitate'];
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['link'][] = '/cazare-'.fa_link($row_dest['denumire_localitate']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona_litoral.'/'.$row_dest['id_localitate'].'/';
	$destinatie['id_loc'][] = $row_dest['id_localitate'];
	$destinatie['info_scurte'][] = $row_dest['info_scurte'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);

if($row_dest['numar']==1)
	{$destinatie['numar_hoteluri'][]=$row_dest['numar_hoteluri']." Hotel ";}
	else
	{ $destinatie['numar_hoteluri'][] = $row_dest['numar_hoteluri']." Hoteluri ";}
	
} @mysql_free_result($rez_dest);

?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cazare Bulgaria 2018 |<?php echo $denumire_agentie; ?></title>
<meta name="description" content="Oferte de cazare in Romania la mare si in statiunile balneare" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/header/header_responsive.php"); ?>
</head>
<body>
 <header>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/breadcrumb_responsive.php"); ?>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/search_responsive.php"); ?>
    </div>
<script src="<?php echo PATH_JS?>/jquery-ui.min.js"></script>

<div class="layout">

    <div id="middle">

        <div id="middleInner">

            <div class="NEW-column-full">

                <div id="NEW-destinatie" class="clearfix">

                    <h1 class="blue" style="float:left;">Cazare Bulgaria 2018 </h1>

                    <div class="float-right">

                        <?php /*?><div class="fb-share-button" data-href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/;src=sdkpreparse">Distribuie</a></div>
<?php */?>
                    </div>

                    <br class="clear" />



                        <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">

                                <div class="NEW-search-wide">

                                    <div class="chapter-title blue">Caută Bulgaria:</div>

                                    <div id="afis_filtru"></div>

                                </div>

                            </div></div>

                        <br class="clear"><br>
                        

   <?php
   
  /* echo '<pre>';
        print_r($destinatie);
        echo  '</pre>';*/
   
    if(sizeof($destinatie['den_loc'])>0) { ?>
 <div id="destinations"  class="clearfix">
 
               <?php foreach($destinatie['den_loc'] as $key1 => $value1) { ?>

                        <div class="destItem NEW-round4px clearfix" >

                            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">

                                <img src="/images/localitati/cazare-<?php echo fa_link($destinatie['den_loc'][$key1]); ?>.jpg" alt="Cazare <?php echo $destinatie['den_loc'][$key1]; ?>">

                                <span class="over"></span>

                                <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>

                                <h2><?php echo $destinatie['den_loc'][$key1]; ?></h2>

                            </div>

                            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><div class="bottomBox clearfix">

                                <div class="price" align="center"><span class="red"><span class="pret"></span> </span> Oferte cazare la <?php echo $destinatie['numar_hoteluri'][$key1]?>  din <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte  <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?>  ">

			 <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?></a></span></span></div>

                            </div>

                        </div>
<?php }?>    

                    </div>

<?php }?>

                    <br class="clear"><br>

                    <div class="pad10 newText">

                         <img src="<?php echo $img_path; ?>icon-sejur-bulgaria.jpg" alt="Sejur Bulgariaa" class="float-left" style="margin:3px 10px 0 0;">

                         <h2><strong> Sejur Bulgaria</strong> - <strong>vacanta accesibila pentru toate gusturile</strong></h2>

<p class="text-justify">Bulgaria este cunoscuta drept tara cu cele mai accesibile destinatii de vacanta, fiind adresata tuturor categoriilor de turisti care opteaza pentru sejur Bulgaria, atat iarna cat si vara.</p>
<p class="text-justify">Pentru cei in cautare de aventura in timpul iernii, Bulgaria ofera partii cu instalatie nocturna si piste multiple pentru schi, momente de neuitat pe patinoare bine intretinute, sau vizite in cel mai mare parc de distractii pentru snowboard-uri din Balcani.</p>
<p class="text-justify">In timpul verii, statiunile montane imbie cu numarul mare de lacuri, cascade, trasee turistice pentru expeditii si plimbari, pentru ciclism si catarat pe stanci, excursii organizate pe jos sau calare pe versantii muntilor.</p>
<p class="text-justify">Bulgaria este faimoasa, in primul rand, pentru superbele plaje pline de viata, litoralul bulgaresc al Marii Negre fiind plin de statiuni turistice dar si de statiuni balneare cu ape minerale si namoluri terapeutice.</p>
<p class="text-justify">Servicii ireprosabile la cazare Bulgaria, sistemul All inclusive cu multiple facilitati de agrement, plajele cu sezlong, umbrele, piscina, sau plajele asigurate, spectacole in fiecare seara, cluburi de noapte, aqua park, fitness, atrag turistii din tot mai multe tari.</p>
<p class="text-justify">Cei ce iubesc curatenia si serviciile ireprosabile pe plaja in timpul unui sejur Bulgaria pot profita de recunoasterea pe plan international, distinctia Blue Flag acordata pe criterii ecologice ce confirma calitatea apei si plajelor bulgaresti.
Litoralul, cu cei 370 km de tarm, ii rasfata pe turisti cu plaje deschise, nisipuri fine aurii, dune care s-au format in mod natural in unele locuri, lagune si golfuri adanci adapostite impotriva vanturilor nordice - un loc ideal pentru toate tipurile de sporturi nautice.</p>

<p class="text-justify">Amatorii de senzatii tari se pot bucura de multiple posibilitati de practicare a sporturilor acvatice si nautice intr-un sejur Bulgaria: surfing, banane pe apa, waterjets, windsurf, parapante, scooter, parasute de apa, schi pe apa, yachting, dar si de clipele de neuitat petrecute pe terenuri de fotbal, baschet, volei, tenis si golf.</p>
<p class="text-justify">Cele mai multe hoteluri de pe litoralul bulgaresc sunt construite chiar pe plaja sau foarte aproape de ea, ofertele All inclusive si ultra All inclusive de cazare Bulgaria fiind principala atractie a turistilor dornici de conditii excelente pentru odihna si relaxare.
Sejurul la malul marii este completat de participarea la seri folclorice si de dans, programe distractive pentru copii, cu cantece, dansuri interactive si clovni, restaurante de talie internationala si taverne locale pentru cele mai fine gusturi unde turistii pot savura mancaruri pescaresti si traditionale bulgaresti, baruri, cafenele, pub-uri si cluburi de noapte.</p>
<p class="text-justify">Influentele occidentale si-au lasat o amprenta vizibila asupra organizarii statiunilor, activitatilor turistice si calitatii serviciilor, conferind turistilor o stare de spirit excelenta.</p>           

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    $("#afis_filtru").load("/includes/search/filtru.php?tara=bulgaria");

</script>





<script>

    $('input.rating').rating({

        callback: function(value) {

            var dataString = 'rating=' + value;

            $.ajax({

                type: "POST",

                url: "/voting/////",

                data:dataString,

                success: function(data) {

                    $('#rating').html(data);

                }

            })

        },required:'hidden'

    });

</script>



<script src="/js/ocazii_main.js"></script>

<script>document.oncopy = addLink;</script>

<script>

    $(window).bind("load", function() {

        $.getScript('/js/ocazii_lateload.js', function() {});

    });

</script>



<script type="text/javascript">


    $(document).ready(function() {

// Tooltip only Text

        $('.masterTooltip').hover(function(){

            // Hover over code

            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<p class="tooltip"></p>')

                .text(title)

                .appendTo('body')

                .fadeIn('slow');

        }, function() {

            // Hover out code

            $(this).attr('title', $(this).data('tipText'));

            $('.tooltip').remove();

        }).mousemove(function(e) {

            var mousex = e.pageX + 20; //Get X coordinates

            var mousey = e.pageY + 10; //Get Y coordinates

            $('.tooltip')

                .css({ top: mousey, left: mousex })

        });



    });

    const mq = window.matchMedia( "(max-width: 768px)" );



    if(mq){



        $('.topBox').each(function(){

            // Cache event

            var existing_event = this.onclick;



            // Remove the event from the link

            this.onclick = null;



            if(existing_event) $(this).click(existing_event);

        });

    }





</script>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php"); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php"); ?>

<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

</body>
</html>
