<?php //error_reporting(E_ALL);
//ini_set('display_errors', 1);?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/config_responsive.php');

include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$den_tara = 'Bulgaria';
$id_tara = get_id_tara($den_tara);

$link_tara = fa_link_oferta($den_tara);
$link_zona_litoral = fa_link_oferta($den_zona_litoral);

if(str_replace("/sejur-","",$_SERVER['REQUEST_URI'])==$link_tara."/") $img_path = 'images/';
	else $img_path = '/sejur-'.$link_tara.'/images/';
	

$sel_dest = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
count(distinct(oferte.id_hotel)) as numar_hoteluri,
localitati.id_localitate,
localitati.denumire AS denumire_localitate,
zone.denumire as denumire_zona,
localitati.info_scurte

FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = ".$id_tara."
and zone.id_zona='1317'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_loc'][] = $row_dest['denumire_localitate'];
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['link'][] = '/cazare-'.fa_link($row_dest['denumire_localitate']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$id_zona_litoral.'/'.$row_dest['id_localitate'].'/';
	$destinatie['id_loc'][] = $row_dest['id_localitate'];
	$destinatie['info_scurte'][] = $row_dest['info_scurte'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);

if($row_dest['numar']==1)
	{$destinatie['numar_hoteluri'][]=$row_dest['numar_hoteluri']." Hotel ";}
	else
	{ $destinatie['numar_hoteluri'][] = $row_dest['numar_hoteluri']." Hoteluri ";}
	
} @mysql_free_result($rez_dest);

?>
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cazare Litoral  Bulgaria 2018 |<?php echo $denumire_agentie; ?></title>
<meta name="description" content="Oferte de cazare in Bulgaria pe litoralul marii neggreu, hoteluri de cu all inclusive" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/header/header_responsive.php"); ?>
</head>
<body>
 <header>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>
    </header>
    <div class="layout">
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/breadcrumb_responsive.php"); ?>
        <?php require($_SERVER['DOCUMENT_ROOT']."/includes/header/search_responsive.php"); ?>
    </div>
<script src="<?php echo PATH_JS?>/jquery-ui.min.js"></script>

<div class="layout">

    <div id="middle">

        <div id="middleInner">

            <div class="NEW-column-full">

                <div id="NEW-destinatie" class="clearfix">

                    <h1 class="blue" style="float:left;">Cazare Litoral Bulgaria 2018 </h1>

                    <div class="float-right">

                        <?php /*?><div class="fb-share-button" data-href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/;src=sdkpreparse">Distribuie</a></div>
<?php */?>
                    </div>

                    <br class="clear" />



                        <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">

                                <div class="NEW-search-wide">

                                    <div class="chapter-title blue">Caută Litoral Bulgaria:</div>

                                    <div id="afis_filtru"></div>

                                </div>

                            </div></div>

                        <br class="clear"><br>
                        

   <?php
   
  /* echo '<pre>';
        print_r($destinatie);
        echo  '</pre>';*/
   
    if(sizeof($destinatie['den_loc'])>0) { ?>
 <div id="destinations"  class="clearfix">
 
               <?php foreach($destinatie['den_loc'] as $key1 => $value1) { ?>

                        <div class="destItem NEW-round4px clearfix" >

                            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">

                                <img src="/images/localitati/cazare-<?php echo fa_link($destinatie['den_loc'][$key1]); ?>.jpg" alt="Cazare <?php echo $destinatie['den_loc'][$key1]; ?>">

                                <span class="over"></span>

                                <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>

                                <h2><?php echo $destinatie['den_loc'][$key1]; ?></h2>

                            </div>

                            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><div class="bottomBox clearfix">

                                <div class="price" align="center"><span class="red"><span class="pret"></span> </span> Oferte cazare la <?php echo $destinatie['numar_hoteluri'][$key1]?>  din <br /><span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Oferte  <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?>  ">

			 <?php echo $destinatie['den_loc'][$key1].' - '.$destinatie['den_zona'][$key1]; ?></a></span></span></div>

                            </div>

                        </div>
<?php }?>    

                    </div>

<?php }?>



              </div>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    $("#afis_filtru").load("/includes/search/filtru.php?tara=bulgaria&zone=litoral-bulgaria");

</script>





<script>

    $('input.rating').rating({

        callback: function(value) {

            var dataString = 'rating=' + value;

            $.ajax({

                type: "POST",

                url: "/voting/////",

                data:dataString,

                success: function(data) {

                    $('#rating').html(data);

                }

            })

        },required:'hidden'

    });

</script>



<script src="/js/ocazii_main.js"></script>

<script>document.oncopy = addLink;</script>

<script>

    $(window).bind("load", function() {

        $.getScript('/js/ocazii_lateload.js', function() {});

    });

</script>



<script type="text/javascript">


    $(document).ready(function() {

// Tooltip only Text

        $('.masterTooltip').hover(function(){

            // Hover over code

            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<p class="tooltip"></p>')

                .text(title)

                .appendTo('body')

                .fadeIn('slow');

        }, function() {

            // Hover out code

            $(this).attr('title', $(this).data('tipText'));

            $('.tooltip').remove();

        }).mousemove(function(e) {

            var mousex = e.pageX + 20; //Get X coordinates

            var mousey = e.pageY + 10; //Get Y coordinates

            $('.tooltip')

                .css({ top: mousey, left: mousex })

        });



    });

    const mq = window.matchMedia( "(max-width: 768px)" );



    if(mq){



        $('.topBox').each(function(){

            // Cache event

            var existing_event = this.onclick;



            // Remove the event from the link

            this.onclick = null;



            if(existing_event) $(this).click(existing_event);

        });

    }





</script>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php"); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php"); ?>

<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

</body>
</html>
