<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive_nou.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_sejur.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/class_sejururi/class_sejur.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/suppliers_xml.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/mail/html2text.php' );

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

/*** check login admin ***/
$logare_admin     = new LOGIN( 'useri' );
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

// id_pret_adaugat  plecare  data  nopti  pret
$id_oferta  = $_GET['id_oferta'];
$tip_pagina = 'conversionintent';
$encoded    = explode( '_', base64_decode( $_GET['encode'] ) );
//if ( ! $err_logare_admin )  { echo '<pre>';print_r($encoded);echo '</pre>';}
$pagina_curenta              = "rezervare";
$GLOBALS['make_vizualizata'] = 'nu';
$det                         = new DETALII_SEJUR();
$detalii                     = $det->select_det_sejur( $id_oferta );
$avans_plata                 = $detalii['avans_plata'];
$nr_zile_plata               = $detalii['nr_zile_plata'];

$id_hotel      = $detalii['id_hotel'];
$detalii_hotel = $det->select_camp_hotel( $id_hotel );

//if ( ! $err_logare_admin )  { echo '<pre>';print_r($detalii);echo '</pre>';}
if ( $detalii['valabila'] != 'da' ) {
	redirect_php( fa_link_hotel( $detalii_hotel['localitate'], $detalii_hotel['denumire'] ) );
}
$nr_nopti               = $encoded[3];
$data_plecare           = denLuniRo( denZileRo( date( 'l, d F Y', strtotime( $encoded[2] ) ) ) );
$data_plecare_normal    = date( 'Y-m-d', strtotime( $encoded[2] ) );
$data_intoarcere        = denLuniRo( denZileRo( date( 'l, d F Y', strtotime( $encoded[2] . ' + ' . $nr_nopti . ' days' ) ) ) );
$data_intoarcere_normal = date( 'Y-m-d', strtotime( $encoded[2] . ' + ' . $nr_nopti . ' days' ) );
$id_pret                = $encoded[0];
$plecare                = $encoded[1];


if ( $_COOKIE['transportAutocar'] != '' and $detalii['transport'] == 'Autocar' ) {
	$oras_plecare = get_id_localitate( desfa_link( $_COOKIE['transportAutocar'] ) );
}

$id_camera       = $encoded[5];
$denumire_camera = $encoded[12];
$nr_adulti       = $encoded[6];
$nr_copii        = $encoded[7];
if ( $nr_copii > 0 ) {
	$copil[1] = $encoded[8];
	$copil[2] = $encoded[9];
	$copil[3] = $encoded[10];
}
$total_pret   = $encoded[4];
$total_moneda = moneda( $detalii['moneda'] );


$tip_masa           = $encoded[11];
$comision           = final_price_lei( $encoded[13], $detalii['moneda'] ); //pretul este in EURO
$price_nored        = $encoded[14];
$disponibilitate    = $encoded[15];
$data_eb            = $encoded[16];
$id_oferta_furnizor = str_replace( '*', '_', $encoded[17] );
$cod_camera         = $encoded[18];

$id_zbor_rez = str_replace( "|", "-", $id_oferta_furnizor . '-' . $encoded[2] . '-' . $id_oferta . '-' . $nr_nopti );
//echo $id_zbor_rez;
$pret_puncte = $total_pret;

$sel_pret_cache = "SELECT * FROM `pret_cache` WHERE pret_cache.id_zbor='" . $id_zbor_rez . "' and pret_cache.id_hotel='" . $id_hotel . "'and pret_cache.denumire_camera='" . $denumire_camera . "' and pret='" . round( $total_pret, 0 ) . "'";
$que_pret_cache = mysql_query( $sel_pret_cache ) or die( mysql_error() );
$rezcam_cache = mysql_fetch_array( $que_pret_cache );
//if ( ! $err_logare_admin )  { echo '<pre>';print_r($row_pret_cache);echo '</pre>';}


$luna = array(
	1  => 'Ianuarie',
	2  => 'Februarie',
	3  => 'Martie',
	4  => 'Aprilie',
	5  => 'Mai',
	6  => 'Iunie',
	7  => 'Iulie',
	8  => 'August',
	9  => 'Septembrie',
	10 => 'Octombrie',
	11 => 'Noiembrie',
	12 => 'Decembrie'
);

if ( $detalii_hotel['tip_unitate'] == 'Circuit' ) {
	$e_circuit          = true;
	$link_oferta_return = make_link_circuit( $detalii_hotel['denumire'], $id_oferta );
} else {
	$link_oferta_return = fa_link_hotel( $detalii_hotel['localitate'], $detalii_hotel['denumire'] );
	//$link_oferta_return = make_link_oferta( $detalii_hotel['localitate'], $detalii_hotel['denumire'], $detalii['denumire_scurta'], null );

}

$detalii_online = $det->oferte_online( $id_hotel, $data_plecare, $detalii['id_transport'] );

include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/preturi_online_oferta_rezervare.php" );

$oras_plecare    = $detalii_online['oras_plecare'][ $id_oferta ][0];
$oras_destinatie = $detalii_online['oras_destinatie'][ $id_oferta ][1];
$services        = $det->offer_services( $id_hotel, $detalii['id_transport'], $nr_nopti, get_den_localitate( $oras_plecare ), $oras_destinatie, $id_oferta, $e_circuit, $servicii_manual );
if ( $detalii['id_transport'] == 1 ) {
	unset( $oras_plecare );
}

$included_services = '';

if ( count( $servicii_incluse_din_xml ) > 0 ) {
	$services['servicii_incluse'] = $servicii_incluse_din_xml;
}

if ( $detalii_hotel['cautare_live'] == 'da' ) {
	$preturi_online = 'da';
	if ( sizeof( $services['servicii_incluse'] ) > '0' ) {
		foreach ( $services['servicii_incluse'] as $key => $value ) {
			$included_services .= '- ' . ucfirst( strip_tags( $value ) ) . '<br>';
		}
	}
} else {
	$preturi_online = "nu";
	foreach ( $detalii['denumire_v1'] as $key => $value ) {
		$included_services .= '- ' . ucfirst( $value ) . '<br>';
	}
}

$included_services .= '- ' . $rezcam_cache['servicii_incluse'];

include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/cupoane.php' );
$cupoane      = new CUPOANE();
$coupon       = $cupoane->decode_cupon( $_COOKIE['cupon'] );
$coupon_valid = $cupoane->valideaza_cupon( $coupon['id_cupon'], $detalii_hotel['id_tara'], $detalii_hotel['id_zona'], $detalii_hotel['id_localitate'] );

if ( isset( $_POST['adauga_cupon'] ) ) {
	$register_coupon = $cupoane->inregistreaza_cupon( $_POST['cod_cupon'] );
	if ( $register_coupon == 'eroare' ) {
		$eroare_mesaj = '<h2 class="red">EROARE! Codul introdus nu este corect sau a expirat!</h2><h3 class="black">Va rugam sa reintroduceti codul sau sa ne <a href="/contact.html" target="_blank"><span class="link-blue">contactati</span></a></h3>';
	} else if ( $coupon_valid == 'nu' ) {
		$eroare_mesaj = '<h2 class="red">EROARE! Acest cupon nu poate fi folosit pentru aceasta destinatie.</h2><h3 class="black">Va rugam sa verificati destinatiile valide ale cuponului in mailul primit cu codul acestuia.</h3>';
	} else {
		$link_succes = substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI'];
		redirect_php( $link_succes );
	}
}


if ( $coupon['valoare_campanie'] > 0 and $coupon['tip_valoare'] != 'procent' ) {
	if ( $coupon['moneda_campanie'] == $detalii['moneda'] ) {
		$coupon['valoare_campanie'] = $coupon['valoare_campanie'];
	} else {
		$coupon['valoare_campanie'] = round( final_price_eur( $coupon['valoare_campanie'], $coupon['moneda_campanie'] ) );
		$coupon['moneda_campanie']  = 'EURO';
	}
}


if ( $detalii['valabila'] == 'nu' ) {
	header( "HTTP/1.1 301 Moved Permanently" );
	header( 'Location: ' . $link_oferta_return );
	exit();
}

if ( isset( $_POST['trimite'] ) ) {

	$grad_oc_cam    = explode( ';', $_POST['grad_ocupare_cam'] );
	$data_adaugarii = date( 'Y-m-d H:i:s' );
	$session_id     = session_id();

	$err = 0;

	//start insert rezervation
	if ( isset( $_POST['coupon'] ) ) {
		if ( $_POST['email'] != $coupon['email'] ) {
			$err          = 1;
			$eroare_mesaj = '<h4 class="red">Acest cupon nu poate fi folosit pentru datele de facturare introduse!</h4><h4 class="black">ATENTIE! Cupoanele de reducere nu sunt transmisibile! Detalii <a href="/info-cupoane-reducere.html" target="_blank" rel="nofollow"><span class="link-blue">cupoane de reducere</span></a></h4>';
		}

		$eroare_mesaj_destinatie = '<h4 class="red">Acest cupon nu poate fi folosit pentru destinatia aleasa!</h4><h4 class="black">Cupoanele de reducere sunt valabile pentru rezervarile la hotelurile sau excursiile din destinatiile inscrise in emailul de confirmare. Daca nu mai aveti acel email va rugam sa ne <a href="/contact.html" target="_blank"><span class="link-blue">contactati</span></a></h4>';
		if ( $coupon['id_tara'] == 0 and $coupon['id_zona'] == 0 and $coupon['id_localitate'] == 0 ) {
		} else if ( $coupon['id_localitate'] != 0 and $coupon['id_localitate'] != $detalii_hotel['id_localitate'] ) {
			$err          = 1;
			$eroare_mesaj = $eroare_mesaj_destinatie;
		} else if ( $coupon['id_zona'] != 0 and $coupon['id_zona'] != $detalii_hotel['id_zona'] ) {
			$err          = 1;
			$eroare_mesaj = $eroare_mesaj_destinatie;
		} else if ( $coupon['id_tara'] != 0 and $coupon['id_tara'] != $detalii_hotel['id_tara'] ) {
			$err          = 1;
			$eroare_mesaj = $eroare_mesaj_destinatie;
		}
	}
//	echo $err;
//	echo $eroare_mesaj;
//	if ( ! $err ) {
	if ( isset( $_POST['trimite'] ) and $err < 1 ) {

		if ( validate_email( $_POST['email'] ) == false ) {
			++ $err;
			$err_email = 'Adresa de email completata nu este corecta!';
		}

		$id_usr = insert_user( $_POST['sex_fact'], $_POST['nume'], $_POST['prenume'], $_POST['email'], $_POST['data_nasterii_1'], $_POST['telefon'], 'rezervare' );


		if ( $tip_unitate == 'Circuit' ) {
			$hotel_tara       = $detalii_hotel['nume_continent'];
			$hotel_localitate = '';
			$hotel_denumire   = '';
			$hotel_stele      = '';
		} else {
			$hotel_tara       = $detalii_hotel['tara'];
			$hotel_localitate = $detalii_hotel['localitate'];
			$hotel_denumire   = $detalii_hotel['denumire'];
			$hotel_stele      = $detalii_hotel['stele'];
		}

		if ( $detalii['nr_zile'] = '2' ) {
			$numar_zile = $nr_nopti + 1;
		} else {
			$numar_zile = $detalii['nr_zile'];
		}

		$comision_trimis_google = 'nu';
		$sel_curs               = "SELECT * FROM curs_valutar ";
		$que_curs               = mysql_query( $sel_curs );
		$row_curs               = mysql_fetch_array( $que_curs );
		@mysql_free_result( $que_curs );

		if ( $_POST['early_booking'] == 'da' ) {
			$eb_titlu = "- Early Booking -" . $_POST['termen_eb'];
		}
		$ins_cerere_rezervare = "INSERT INTO cerere_rezervare SET
		id_oferta = '" . $id_oferta . "',
		id_furnizor = '" . $detalii['furnizor'] . "',
		id_useri_fizice = '" . $id_usr . "',
		data = '" . $encoded[2] . "',
		nr_nopti = '" . $nr_nopti . "',
		nr_zile = '" . $numar_zile . "',
		nr_adulti = '" . $grad_oc_cam[1] . "',
		nr_copii = '" . $grad_oc_cam[2] . "',
		data_adaugarii = '" . $data_adaugarii . "',
		oferta_denumire = '" . $detalii['denumire'] . $eb_titlu . "',
		hotel_denumire = '" . $hotel_denumire . "',
		hotel_categorie = '" . $hotel_stele . "',
		hotel_localitate = '" . $hotel_localitate . "',
		hotel_tara = '" . $hotel_tara . "',
		tip_masa = '" . $tip_masa . "',
		transport = '" . $detalii['transport'] . "',
		oras_plecare = '" . $oras_plecare . "',
		early_booking = '" . $_POST['early_booking'] . "',
		procent_avans='" . $_POST['procent_avans'] . "',
		plata_rest='" . $_POST['plata_rest'] . "',
		conditii_anulare = '" . $_POST['camp_anulare'] . "',
		servicii_incluse = '" . $_POST['included_services'] . "',
		comision = '" . $comision . "',
		curs_valutar ='" . $row_curs['EURO'] . "',
		comision_trimis_google='" . $comision_trimis_google . "',
		stare = 'neterminata',
		id_cupon = '" . $coupon['id_cupon'] . "',
		tip = 'rezervare',
		adaugat_admin = '" . $_SESSION['nume'] . "',
		session_id = '" . $session_id . "',
		ip = '" . $_SERVER['REMOTE_ADDR'] . "',
		disponibilitate = '" . $disponibilitate . "',
		preturi_online = '" . $preturi_online . "'
		";
		$rez_cerere_rezervare = mysql_query( $ins_cerere_rezervare ) or die ( mysql_error() );

		$sel_cerere_rezervare = "SELECT id_cerere FROM cerere_rezervare WHERE session_id='" . $session_id . "' AND id_useri_fizice='" . $id_usr . "' ORDER BY id_cerere DESC";
		$que_cerere_rezervare = mysql_query( $sel_cerere_rezervare ) or die( mysql_error() );
		$row_cerere_rezervare = mysql_fetch_array( $que_cerere_rezervare );

		$id_cerere       = $row_cerere_rezervare['id_cerere'];
		$denumire_camere = $_POST['denumire_camere'];

		$sel_cerere_rez_tip_cam = "SELECT id_camera FROM cerere_rezervare_tip_camera WHERE id_cerere='" . $id_cerere . "' AND id_camera_hotel='" . $grad_oc_cam[0] . "' ";
		$que_cerere_rez_tip_cam = mysql_query( $sel_cerere_rez_tip_cam ) or die( mysql_error() );
		$row_cerere_rez_tip_cam   = mysql_fetch_array( $que_cerere_rez_tip_cam );
		$total_cerere_rez_tip_cam = mysql_num_rows( $que_cerere_rez_tip_cam );

		if ( $total_cerere_rez_tip_cam == '0' ) {
			$ins_cerere_rez_tip_cam = "INSERT INTO cerere_rezervare_tip_camera SET id_cerere='" . $id_cerere . "', id_camera_hotel='" . $grad_oc_cam[0] . "', nume_camera='" . $denumire_camere . "', nr_camera='1' ";
			$rez_cerere_rez_tip_cam = mysql_query( $ins_cerere_rez_tip_cam ) or die ( mysql_error() );
		}

		$sel1_cerere_rez_tip_cam = "SELECT * FROM cerere_rezervare_tip_camera WHERE id_cerere='" . $id_cerere . "'";
		$que1_cerere_rez_tip_cam = mysql_query( $sel1_cerere_rez_tip_cam ) or die( mysql_error() );
		$row1_cerere_rez_tip_cam = mysql_fetch_array( $que1_cerere_rez_tip_cam );

		$id_camera_tip_cam = $row1_cerere_rez_tip_cam['id_camera'];

		$sel1_cerere_rez_grad_ocup = "SELECT id_camera FROM cerere_rezervare_grad_ocupare WHERE id_camera='" . $id_camera_tip_cam . "' ";
		$que1_cerere_rez_grad_ocup = mysql_query( $sel1_cerere_rez_grad_ocup ) or die( mysql_error() );
		$row1_cerere_rez_grad_ocup  = mysql_fetch_array( $que1_cerere_rez_grad_ocup );
		$total_cerere_rez_grad_ocup = mysql_num_rows( $que1_cerere_rez_grad_ocup );

		if ( $total_cerere_rez_grad_ocup == '0' ) {
			$ins_cerere_rez_grad_ocup = "INSERT INTO cerere_rezervare_grad_ocupare (`id_grad_ocupare`, `id_camera`, `adulti`, `copii`, `copil1`, `copil2`, `copil3`, `pret`, `moneda`,`pret_lei`) VALUES ('', '$id_camera_tip_cam', '$grad_oc_cam[1]', '$grad_oc_cam[2]', '$grad_oc_cam[3]', '$grad_oc_cam[4]', '$grad_oc_cam[5]', '" . $_POST['total_pret'] . "', '" . $_POST['total_moneda'] . "', '" . final_price_lei( $_POST['total_pret'], $_POST['total_moneda'] ) . "')";
			$rez_cerere_rez_grad_ocup = mysql_query( $ins_cerere_rez_grad_ocup ) or die ( mysql_error() );

		}

		$sel_grad_ocup = "SELECT id_grad_ocupare FROM cerere_rezervare_grad_ocupare WHERE id_camera='" . $id_camera_tip_cam . "' ORDER BY id_grad_ocupare DESC";
		$que_grad_ocup = mysql_query( $sel_grad_ocup ) or die( mysql_error() );
		$row_grad_ocup = mysql_fetch_array( $que_grad_ocup );

		$iii1     = 1;
		$tourists = '';
		for ( $i4 = 1; $i4 <= $grad_oc_cam[1]; $i4 ++ ) {
			$sex_adl[ $i4 ]     = trim( $_POST[ 'sex_adl_' . $i4 ] );
			$nume_adl[ $i4 ]    = ucwords( strtolower( trim( $_POST[ 'nume_adl_' . $i4 ] ) ) );
			$prenume_adl[ $i4 ] = ucwords( strtolower( trim( $_POST[ 'prenume_adl_' . $i4 ] ) ) );
			$data_adl[ $i4 ]    = trim( $_POST[ 'data_adl_' . $i4 ] );
			if ( ( $data_adl[ $i4 ] == '' ) or ( $data_adl[ $i4 ] == '0000-00-00' ) ) {
				$data_adl_[ $i4 ] = null;
			}

			$ins_pasageri1 = "INSERT INTO pasageri (`id_rezervare`, `id_grad_ocupare`, `sex`, `nume`, `prenume`, `data_nasterii`) VALUES ('" . $id_cerere . "', '" . $row_grad_ocup['id_grad_ocupare'] . "', '$sex_adl[$i4]', '$nume_adl[$i4]', '$prenume_adl[$i4]', '$data_adl[$i4]')";
			$res_pasageri1 = mysql_query( $ins_pasageri1 ) or die ( mysql_error() );

			$tourists .= $prenume_adl[ $i4 ] . ' ' . $nume_adl[ $i4 ] . ' ';
			if ( $data_adl[ $i4 ] ) {
				$tourists .= date( 'd.m.Y', strtotime( $data_adl[ $i4 ] ) );
			}
			$tourists .= '<br>';
		}
		for ( $i5 = 1; $i5 <= $grad_oc_cam[2]; $i5 ++ ) {
			$sex_copil[ $i5 ]     = trim( $_POST[ 'sex_copil_' . $i5 ] );
			$nume_copil[ $i5 ]    = ucwords( strtolower( trim( $_POST[ 'nume_copil_' . $i5 ] ) ) );
			$prenume_copil[ $i5 ] = ucwords( strtolower( trim( $_POST[ 'prenume_copil_' . $i5 ] ) ) );
			$data_copil[ $i5 ]    = trim( $_POST[ 'data_copil_' . $i5 ] );

			$ins_pasageri2 = "INSERT INTO pasageri (`id_rezervare`, `id_grad_ocupare`, `sex`, `nume`, `prenume`, `data_nasterii`) VALUES ('" . $id_cerere . "', '" . $row_grad_ocup['id_grad_ocupare'] . "', '$sex_copil[$i5]', '$nume_copil[$i5]', '$prenume_copil[$i5]', '$data_copil[$i5]')";
			$res_pasageri2 = mysql_query( $ins_pasageri2 ) or die ( mysql_error() );

			$tourists .= $prenume_copil[ $i5 ] . ' ' . $nume_copil[ $i5 ] . ' ';
			if ( $data_copil[ $i5 ] ) {
				$tourists .= date( 'd.m.Y', strtotime( $data_copil[ $i5 ] ) ) . '<br>';
			}
		}

		$upd_rezervare = "UPDATE cerere_rezervare SET modalitate_plata='" . $_POST['modalitate_plata'] . "', observatii='" . $_POST['observatii'] . "', stare='noua' WHERE id_cerere='" . $id_cerere . "'";
		$res_rezervare = mysql_query( $upd_rezervare ) or die ( mysql_error() );

		if ( ( $_POST['denumire'] == '' ) and ( $_POST['cui_cnp'] == '' ) and ( $_POST['nr_reg_comert'] == '' ) ) {
			$facturare_tip_persoana = 'persoana_fizica';
			$facturare_denumire     = ucwords( strtolower( $_POST['nume'] ) ) . ' ' . ucwords( strtolower( $_POST['prenume'] ) );
			$facturare_adresa       = $_POST['adresa'];
			$facturare_oras         = $_POST['oras'];
			$facturare_judet        = $_POST['judet'];
		} else {
			$facturare_tip_persoana = 'persoana_juridica';
			$facturare_denumire     = $_POST['denumire'];
			$facturare_adresa       = $_POST['adresa_juridic'];
			$facturare_oras         = $_POST['oras_juridic'];
		}
		$facturare_nr_reg_comert = $_POST['nr_reg_comert'];
		$facturare_cui_cnp       = $_POST['cui_cnp'];
		$facturare_banca         = $_POST['banca'];
		$facturare_iban          = $_POST['iban'];

		$ins_facturare = "INSERT INTO cerere_rezervare_date_facturare (`id_cerere`, `tip_persoana`, `denumire`, `cui_cnp`, `nr_reg_comert`, `banca`, `iban`, `adresa`, `oras`,`judet`, `email`, `telefon`) VALUES ('" . $id_cerere . "', '" . $facturare_tip_persoana . "', '" . $facturare_denumire . "', '" . $facturare_cui_cnp . "', '" . $facturare_nr_reg_comert . "', '" . $facturare_banca . "', '" . $facturare_iban . "', '" . $facturare_adresa . "', '" . $facturare_oras . "', '" . $facturare_judet . "', '" . $_POST['email'] . "', '" . $_POST['telefon'] . "')";
		$res_facturare = mysql_query( $ins_facturare ) or die ( mysql_error() );
		$servicii_incluse = $_POST['included_services'];


		//campuri email
		$sel_user = "SELECT  useri_fizice.nume, useri_fizice.prenume, useri_fizice.email, useri_fizice.sex FROM useri_fizice INNER JOIN cerere_rezervare ON cerere_rezervare.id_useri_fizice=useri_fizice.id_useri_fizice WHERE cerere_rezervare.id_cerere='" . $id_cerere . "'";
		$que_user = mysql_query( $sel_user ) or die( mysql_error() );
		$row_user = mysql_fetch_array( $que_user );

		if ( $row_user['sex'] == 'm' ) {
			$tit = 'Domnul ';
		} elseif ( $row_user['sex'] == 'f' ) {
			$tit = 'Doamna ';
		}
		$GLOBALS['nume_expeditor2'] = $row_user['prenume'] . ' ' . $row_user['nume'];
		$GLOBALS['nume_expeditor']  = $tit . $nume_expeditor2;
		$GLOBALS['mail_expeditor']  = $row_user['email'];
		$GLOBALS['oferta_denumire'] = $detalii['denumire'];
		$GLOBALS['oferta_id']       = $id_oferta;
		$GLOBALS['oferta_link']     = substr( $sitepath, 0, - 1 ) . $link_oferta_return;
		if ( $detalii['transport'] == 'Avion' ) {
			$plecare_link = '&transport=avion&plecare-avion=' . $oras_plecare;
		}

		$GLOBALS['oferta_link']     .= '?plecdata=' . $data_plecare_normal = date( 'd.m.Y', strtotime( $encoded[2] ) ) . $plecare_link . '&pleczile=' . $encoded[3] . '&adulti=' . $encoded[6] . '&copii=' . $encoded[7] . '&age[0]=' . $encoded[8] . '&age[1]=' . $encoded[9] . '&age[2]=' . $encoded[10];
		$GLOBALS['oferta_data']     = date( 'd.m.Y', strtotime( $encoded[2] ) );
		$GLOBALS['oferta_nopti']    = $nr_nopti;
		$GLOBALS['oferta_camera']   = $denumire_camere;
		$GLOBALS['oferta_persoane'] = $grad_oc_cam[1] . ' adult';
		if ( $grad_oc_cam[1] > 1 ) {
			$GLOBALS['oferta_persoane'] .= 'i';
		}
		if ( $grad_oc_cam[2] > 0 ) {
			$GLOBALS['oferta_persoane'] .= ' + ' . $grad_oc_cam[2] . ' copi';
			if ( $grad_oc_cam[2] > 1 ) {
				$GLOBALS['oferta_persoane'] .= 'i';
			} else {
				$GLOBALS['oferta_persoane'] .= 'l';
			}
			for ( $aa = 0; $aa < $grad_oc_cam[2]; $aa ++ ) {
				$varsta_copii .= $grad_oc_cam[ $aa + 3 ] . ' ani,';
			}
			$GLOBALS['oferta_persoane'] .= '(' . $varsta_copii . ')';
		}
		$GLOBALS['oferta_turisti']    = $tourists;
		$GLOBALS['rezervare_id']      = 'OCZ' . $id_cerere;
		$GLOBALS['servicii_incluse']  = $servicii_incluse;
		$GLOBALS['tip_masa']          = $tip_masa;
		$GLOBALS['total_plata']       = $_POST['total_pret'] . ' ' . moneda( $_POST['total_moneda'] );
		$GLOBALS['facturare_tip']     = ucwords( str_replace( '_', ' ', $facturare_tip_persoana ) );
		$GLOBALS['facturare_detalii'] = $facturare_denumire . '<br>';
		if ( $facturare_cui_cnp ) {
			$GLOBALS['facturare_detalii'] .= $facturare_cui_cnp . '<br>';
		}
		if ( $facturare_nr_reg_comert ) {
			$GLOBALS['facturare_detalii'] .= $facturare_nr_reg_comert . '<br>';
		}
		if ( $facturare_banca ) {
			$GLOBALS['facturare_detalii'] .= $facturare_banca . '<br>';
		}
		if ( $facturare_iban ) {
			$GLOBALS['facturare_detalii'] .= $facturare_iban . '<br>';
		}
		if ( $facturare_adresa ) {
			$GLOBALS['facturare_detalii'] .= $facturare_adresa . '<br>';
		}
		if ( $facturare_oras ) {
			$GLOBALS['facturare_detalii'] .= $facturare_oras . '<br>';
		}

		$GLOBALS['sigla']          = $GLOBALS['path_sigla'];
		$GLOBALS['hotel_denumire'] = $hotel_denumire;
		$GLOBALS['hotel_stele']    = $hotel_stele;
		$GLOBALS['link_check_rez'] = $sitepath . '/rezervarea-mea/?login=' . base64_encode( $mail_expeditor . '+++' . $rezervare_id );

		//$send_m = new SEND_EMAIL;
		//email_agentie
		/*$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/rezervare_new.htm';
		$param['subject']='REZERVARE '.$oferta_denumire;
		$param['from_email']=$GLOBALS['email_rezervare'];
		$param['to_email']=$GLOBALS['email_rezervare'];
		$param['to_nume']=$GLOBALS['denumire_agentie'];
		$param['fr_email']=$mail_expeditor;
		$param['fr_nume']=$nume_expeditor2;
		$send_m->send_rezervari($param);
		*/

		//$send_m= new SEND_EMAIL;
		//email_client
		$param['templates'] = $_SERVER['DOCUMENT_ROOT'] . '/mail/templates/rezervare_new.htm';
		$param['subject']   = 'REZERVARE ' . $hotel_denumire . ' ' . $hotel_localitate;
		$param['to_email']  = $GLOBALS['email_rezervare'];
		$param['to_nume']   = $GLOBALS['denumire_agentie'];
		$param['fr_email']  = $mail_expeditor;
		$param['fr_nume']   = $nume_expeditor2;
//$send_m->send_mail_adm($param,$GLOBALS['attachment']);
//		$send_m->send_rezervari( $param );
		include( $_SERVER['DOCUMENT_ROOT'] . '/mail/trimitere_email.php' );
		// se trimtie sms
		$recipient = "0745917872"; //To be fill in !
		$message   = "O noua rezervare pentru " . $hotel_denumire . ' ' . $hotel_localitate . ' ' . $GLOBALS['oferta_link'];
		include( $_SERVER['DOCUMENT_ROOT'] . '/config/trimitere_sms_web2sms.php' );


		if ( ! $err_logare_admin ) {


		} else {
			//email_client
			$param['templates']  = $_SERVER['DOCUMENT_ROOT'] . '/mail/templates/raspuns_automat_rezervare_new.htm';
			$param['subject']    = 'REZERVARE ' . $hotel_denumire . ' ' . $hotel_localitate . ' ' . $GLOBALS['denumire_agentie'];
			$param['from_email'] = $GLOBALS['email_rezervare'];
			$param['to_email']   = $mail_expeditor;
			$param['to_nume']    = $nume_expeditor2;
			$param['fr_email']   = $GLOBALS['email_rezervare'];
			$param['fr_nume']    = $GLOBALS['denumire_agentie'];
			//require_once($_SERVER['DOCUMENT_ROOT'].'/mail/classes/trimitere_email.php');
//			$send_m->send_rezervari( $param );
			include( $_SERVER['DOCUMENT_ROOT'] . '/mail/trimitere_email.php' );

		}


		if ( ! $err_logare_admin ) {
			$actiune_log  = $_SESSION['nume'] . ' a introdus rezervarea cu id-ul ' . $row_cerere_rezervare['id_cerere'];
			$ins_logadmin = "INSERT INTO log_admin SET id_admin='" . $_SESSION['id_user_adm'] . "', data=NOW(), actiune='$actiune_log'";
			$rez_logadmin = mysql_query( $ins_logadmin ) or die ( mysql_error() );
		}


		$arrCos = explode( '+;++;+', $_COOKIE['cos_cumparaturi'] );
		foreach ( $arrCos as $k_arrCos => $v_arrCos ) {
			$upd_cos_cump = "UPDATE cos_cumparaturi SET data_stergere = NOW(), tip_stergere = 'rezervare_finalizata' WHERE cookie = '" . $v_arrCos . "' AND ip = '" . $_SERVER['REMOTE_ADDR'] . "' AND browser_info = '" . $_SERVER['HTTP_USER_AGENT'] . "' ";
			$rez_cos_cump = mysql_query( $upd_cos_cump ) or die ( mysql_error() );
		}
		setcookie( 'cos_cumparaturi', '', time() - 3600, '/', 'ocaziituristice.ro' );

		if ( isset( $_POST['coupon'] ) ) {
			$cupoane->dezactiveaza_cupon( $coupon['cod_cupon'] );
		}

		if ( $detalii_hotel['tip_unitate'] == 'Circuit' ) {
			$link_succes = make_link_circuit( $detalii_hotel['denumire'], $id_oferta, 'da' ) . 'rezervare-succes-' . $id_hotel . '_' . $id_oferta . '_' . $id_usr . '_' . $id_cerere;
		} else {
			$link_succes = make_link_oferta( $detalii_hotel['localitate'], $detalii_hotel['denumire'], null, null ) . 'rezervare-succes-' . $id_hotel . '_' . $id_oferta . '_' . $id_usr . '_' . $id_cerere;
		}

		//if ($err_logare_admin ) {redirect_php( $link_succes );}
		redirect_php( $link_succes );
	}
}

?>
<head>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/header_charset.php" ); ?>
    <title>Rezervare nume hotel <?php echo $denumire_agentie; ?></title>
    <meta name="description" content="pagina de rezervare a hotelului"/>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/addins_head_new_responsive_nou.php' ); ?>
	<?php require_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/header/header_responsive.php" ); ?>


    <link href="/js/jquery_validate/validationEngine.jquery.css" rel="stylesheet" type="text/css"/>
    <script src="/js/jquery_validate/jquery.validationEngine-ro.js"></script>
    <script src="/js/jquery_validate/jquery.validationEngine.js"></script>
    <script>
        $(document).ready(function () {
            $("#bookFrm").validationEngine();
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery("#bookFrm").validationEngine('attach', {promptPosition: "topRight"});
        });

        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>
</head>
<body>
<?php include( $_SERVER['DOCUMENT_ROOT'] . '/includes/header/admin_bar_responsive.php' ); ?>

<header>
	<?php require( $_SERVER['DOCUMENT_ROOT'] . "/includes/header/meniu_header_responsive.php" ); ?>
</header>
<div class="layout">
	<?php require( "includes/header/breadcrumb_responsive.php" ); ?>
</div>
<div class="layout" id="middle">
    <div class="bgallgri bg-noheader">
        <div class="max">


            <div class="input_none static-page bs content_char_book">

				<?php // main right side ?>
                <div class="content_char_book_right">
                    <div class="content_char_book_right_content sticky">
                        <div class=" main-content">
                            <div class="box_rez_sumar_call">
                                <div class="box_rez_sumar_call_row">
                                    <div class="box_rez_sumar_call_icon">
                                    </div>
                                    <div class="box_rez_sumar_call_text">
                                        <span>
                                            <strong>Ai nevoie de ajutor?</strong>
                                            <br/>Suna la <a
                                                    href="tel:<?php echo $contact_telefon; ?>"><?php echo $contact_telefon; ?></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div id="box_rez_sumar" class="box_rez_sumar">
                                <div id="box_rez_sumar_top" class="box_rez_sumar_top">
                                    <ul class="box_rez_sumar_top_in">
                                        <li>
                                            <h2 class="blue" style="font-size: 25px">
												<?php echo $detalii_hotel['denumire']; ?><br/>
												<?php for (
													$stea = 1;
													$stea <= $detalii_hotel['stele'];
													$stea ++
												) {
													$stars .= '<i class="icon-star yellow"></i>';
												}
												echo $stars; ?><br/>
												<?php if ( $detalii_hotel['tip_unitate'] != 'Circuit' ) { ?><span
                                                        class="smaller-09em black"><?php echo " " . $detalii_hotel['tara']; ?>
                                                    / <?php echo $detalii_hotel['localitate']; ?></span><?php } ?>
                                            </h2>
											<?php if ( $disponibilitate == 'disponibil' ) : ?>
                                                <span class="inline-block bkg-dark-green NEW-round8px pad5 w140 smaller-07em white text-center">DISPONIBIL</span>
											<?php endif; ?>
                                        </li>

										<?php if ( strtotime( $data_eb ) > strtotime( "+1 day" ) ) { ?>
                                            <li>
                                                <div class="bkg-blue text-center bigger-15em" align="center">
                                                    Pret special pana
                                                    la <?php echo $termen_eb = denLuniRo( date( 'd F Y', strtotime( $data_eb ) ) ); ?>
                                                </div>
                                            </li>
										<?php } ?>

                                        <li>
                                            <div class="box_rez_sumar_top_in_title">
                                                <div class="hotel-details"></div>
                                                <span class="leave-details">Durată sejur: <?php echo $nr_nopti; ?>
                                                    nopți</span>
                                                <input type="hidden" value="" name="book_hotel_name"
                                                       id="book_hotel_name">
                                            </div>

                                        </li>
                                        <li class="rooms"></li>
                                        <li>
                                            Check-In: <span class="check-in"><?php echo $data_plecare; ?></span>
                                            <br>
                                            Check-Out: <span
                                                    class="check-out"><?php echo $data_intoarcere; ?></span>
                                        </li>
										<?php if ( $detalii['transport'] != 'Fara transport' ) { ?>
                                            <li id="charterTransport">
                                                Transport: <?php echo $detalii['transport']; ?> <?php if ( $oras_plecare ) {
													echo ', plecare din <strong>' . get_den_localitate( $oras_plecare ) . '</strong>';
												} ?></li>
										<?php } ?>
                                        <li>
                                            <div>Masă: <?php echo $tip_masa; ?></div>
                                        </li>
                                        <li class="taxe-neincluse">
                                            <div class="first bigger-15em bold blue">Servicii incluse in pachet :
                                            </div>

                                            <div><?php echo schimba_caractere( ucfirst( $tip_masa ) ); ?></div>
											<?php foreach ( $services['servicii_incluse'] as $key => $value ) { ?>

                                                <div><?php echo schimba_caractere( ucfirst( $value ) ); ?></div>


											<?php } ?>
											<?php if ( strlen( $rezcam_cache['servicii_incluse'] ) > 5 ) {
												echo "<div>" . $rezcam_cache['servicii_incluse'] . "</div>";
											}

											?>
                                        </li>
										<?php if ( $price_nored > $total_pret and $price_nored != 0 ) { ?>
                                            <li style="position: relative;overflow: visible;margin-top: 50px;">
                                                <div class="bubble-asigurari-small bubble-bottom white w140 text-center">
                                                    <span class="bigger-14em">ECONOMISEŞTI<br>
                                                        <span class="bigger-15em" style="line-height:1em;">
                                                            <?php echo round( $price_nored - $total_pret ) . ' ' . $total_moneda; ?>
                                                        </span>
                                                    </span>
                                                </div>
												<?php echo '<span class="old-price blue text-right">' . $price_nored . ' ' . $total_moneda . '</span>'; ?>
                                                <div class="clear"></div>
                                            </li>
										<?php } ?>
										<?php if ( isset( $_COOKIE['cupon'] ) and $coupon_valid == 'da' ) { ?>
                                            <li style="font-size: 18px;padding-bottom: 0;display: none;">
                                                <span class="red" style="float: left">Cupon</span>
                                                <span class="red" style="float: right">
                                                    <span class="discount-value"></span>
													<?php echo $total_moneda ?>
                                                </span>
                                                <div class="clear"></div>
                                            </li>
										<?php } ?>
                                    </ul>
                                </div>
                                <div class="box_rez_sumar_bottom">
                                    <div class="rez_total_price_box">
                                        <div class="rez_tpb_left ">
                                            Tarif total
                                            <span class="tui rez_tpb_left_bug total-pers"></span>
                                        </div>
                                        <div class="rez_tpb_right">
											<?php //if ( isset( $_COOKIE['cupon'] ) and $coupon_valid == 'da' ) {
											//$total_pret = round( $total_pret - ( $total_pret * $coupon['valoare_campanie'] / 100 ), 0 );
											//} ?>
                                            <div class="hidden initial-price"
                                                 data-price="<?php echo round( $total_pret ) ?>"></div>
                                            <input type="hidden" name="final-price"
                                                   value="<?php echo round( $total_pret ) ?>"/>
                                            <i class="rez_right_total"><span class="package-price"></span></i>
                                            <span class="tui">
                                                <span class="total-price-value"><?php echo round( $total_pret ) ?></span>
                                                <span><?php echo ' ' . $total_moneda; ?></span>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				<?php // main left side ?>
                <div class="content_char_book_left">

                    <form accept-charset="utf-8" action="<?php echo $_SERVER['REQUEST_URI']; ?>" id="bookFrm"
                          name="bookFrm"
                          method="post">


						<?php if ( $termen_eb != '' ) {
							$early_booking = 'da'; ?>
                            <input type="hidden" name="termen_eb" value="<?php echo $termen_eb; ?>">
						<?php } else {
							$early_booking = 'nu';
						}
						?>
                        <input type="hidden" name="procent_avans" value="<?php echo $avans_plata; ?>">

                        <input type="hidden" name="early_booking" value="<?php echo $early_booking; ?>">
                        <input type="hidden" name="denumire_camere" value="<?php echo $denumire_camera; ?>">


                        <input type="hidden" name="total_pret" id="total_pret"
                               value="<?php echo round( $total_pret ); ?>">
                        <input type="hidden" name="total_moneda" id="total_moneda"
                               value="<?php echo reverse_moneda( $total_moneda ); ?>">
                        <input type="hidden" name="grad_ocupare_cam"
                               value="<?php echo $id_camera . ';' . $nr_adulti . ';' . $nr_copii . ';' . $copil[1] . ';' . $copil[2] . ';' . $copil[3] . ';' . $tip_masa . ';' . $denumire_camera; ?>">


                        <div class="content_char_book_left_row">
                            <div class="content_char_detalii_left_content">
                                <div class="detalii_char_over_box_header ">
                                    <i class="fa fa-user fa-fw"></i> Informatii turisti
                                </div>
                                <input id="Rooms_0__RoomName" name="Rooms[0].RoomName" type="hidden"
                                       value="<?php echo $denumire_camera; ?>"/>
                                <div class="content_char_detalii_pasageri">

									<?php
									if ( $detalii_hotel['tip_unitate'] == 'Circuit' ) {
										$denumire_camera_afis = $denumire_camera;
									} else
										$denumire_camera_afis = $denumire_camera . ' - ' . $detalii_hotel['denumire'] . ' / ' . $detalii_hotel['localitate'] . ', ' . $detalii_hotel['zona'] . ' - ' . $detalii_hotel['tara']
									?>
                                    <div class="title" style="margin-bottom: 10px;line-height: 25px;">
                                        <span><?php echo $denumire_camera_afis; ?></span>
										<?php if ( $disponibilitate == 'disponibil' ) : ?>
                                            <span class="inline-block bkg-dark-green NEW-round8px pad5 w140 smaller-07em white text-center"
                                                  style="color: #fff;">DISPONIBIL</span>
										<?php endif; ?>
                                    </div>
                                    <div class="content_box">
                                        <div class="box_obligatoriu">
                                            * Informatii obligatorii
                                        </div>
                                        <div class="content_char_detalii_pasageri_notificare">
                                            <b>Important:</b> Numele pasagerilor trebuie sa fie identice cu cele din
                                            C.I
                                            / Pasaport
                                        </div>
										<?php
										$iii1 = 0;
										for ( $i4 = 0; $i4 < $nr_adulti; $i4 ++ ) { ?>
                                            <div class="row">
                                                <div class="box_nume_prenume">
                                                    <div class="box_n_p_1">
                                                        <span> Adult <?php echo( $i4 + 1 ) ?> </span>
                                                    </div>
                                                    <div class="box_n_p_2">
                                                        <div class="form-group  new_check">
                                                            <input id="sex_ad1_m<?php echo( $i4 + 1 ); ?>"
                                                                   name="sex_adl_<?php echo( $i4 + 1 ); ?>"
                                                                   value="M"

																<?php if ( ( $_POST[ 'sex_adl_' . ( $i4 + 1 ) ] == 'M' ) ) {
																	echo ' checked="checked"';
																} ?>
                                                                   type="radio">
                                                            <label for="sex_ad1_m<?php echo( $i4 + 1 ); ?>">M</label>
                                                            &nbsp;
                                                            <input id="sex_ad1_f<?php echo( $i4 + 1 ); ?>"
                                                                   name="sex_adl_<?php echo( $i4 + 1 ); ?>"
                                                                   value="F"
																<?php if ( ( $_POST[ 'sex_adl_' . ( $i4 + 1 ) ] == 'F' ) ) {
																	echo ' checked="checked"';
																} ?>
                                                                   type="radio">
                                                            <label for="sex_ad1_f<?php echo( $i4 + 1 ); ?>">F</label>
                                                        </div>
                                                        <div class="box_n_p_2_1 input new_input_text">
                                                            <input class="necesar"
                                                                   id="nume_adl_<?php echo( $i4 + 1 ); ?>"
                                                                   name="nume_adl_<?php echo( $i4 + 1 ); ?>"
                                                                   type="text"
                                                                   value="<?php echo $_POST[ 'nume_adl_' . ( $i4 + 1 ) ]; ?>"
                                                                   data-validation-engine="validate[required]"/>
                                                            <label class="transition01"
                                                                   for="nume_adl_<?php echo( $i4 + 1 ); ?>">Nume
                                                                adult <?php echo( $i4 + 1 ); ?>*</label>
                                                        </div>
                                                        <div class="box_n_p_2_2 input new_input_text">
                                                            <input class="necesar"
                                                                   id="prenume_adl_<?php echo( $i4 + 1 ); ?>"
                                                                   name="prenume_adl_<?php echo( $i4 + 1 ); ?>"
                                                                   type="text"
                                                                   value="<?php echo $_POST[ 'prenume_adl_' . ( $i4 + 1 ) ]; ?>"
                                                                   data-validation-engine="validate[required]"/>
                                                            <label class="transition01"
                                                                   for="prenume_adl_<?php echo( $i4 + 1 ); ?>">Prenume
                                                                adult <?php echo( $i4 + 1 ); ?>*</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text"
                                                                   id="data_adl_<?php echo( $i4 + 1 ) ?>"
                                                                   name="data_adl_<?php echo( $i4 + 1 ); ?>"
                                                                   placeholder="Data Nasterii"
                                                                   value="<?php echo $_POST[ 'data_adl_' . ( $i4 + 1 ) ]; ?>"
                                                                   class="data_nasterii form-control">
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
										<?php }
										for ( $i5 = 0; $i5 < $nr_copii; $i5 ++ ) {
											if ( $copil[ $i5 + 1 ] < 2 ) {
												$year = ' an';
											} else {
												$year = ' ani';
											} ?>
                                            <div class="row">
                                                <div class="box_nume_prenume">
                                                    <div class="box_n_p_1">
                                                        <span>Copil - <?php echo $copil[ $i5 + 1 ] . $year ?></span>
                                                    </div>
                                                    <div class="box_n_p_2">
                                                        <div class="form-group new_check">
                                                            <input id="Rooms_Kids_<?php echo $i5; ?>_Male"
                                                                   name="sex_copil_<?php echo( $i5 + 1 ); ?>"
                                                                   value="M"
                                                                   type="radio" <?php if ( ( $_POST[ 'sex_copil_' . ( $i5 + 1 ) ] == 'M' ) ) {
																echo ' checked="checked"';
															} ?>>
                                                            <label for="Rooms_Kids_<?php echo $i5; ?>_Male">M</label>
                                                            &nbsp;
                                                            <input id="Rooms_Kids_<?php echo $i5; ?>_Female"
                                                                   name="sex_copil_<?php echo( $i5 + 1 ); ?>"
                                                                   value="F"
                                                                   type="radio" <?php if ( ( $_POST[ 'sex_copil_' . ( $i5 + 1 ) ] == 'F' ) ) {
																echo ' checked="checked"';
															} ?>>
                                                            <label for="Rooms_Kids_<?php echo $i5; ?>_Female">F</label>
                                                        </div>
                                                        <div class="box_n_p_2_1 input new_input_text">
                                                            <input class="necesar"
                                                                   id="Rooms_Kids_<?php echo $i5; ?>_Name"
                                                                   name="nume_copil_<?php echo( $i5 + 1 ) ?>"
                                                                   type="text"
                                                                   value="<?php echo $_POST[ 'nume_copil_' . ( $i5 + 1 ) ]; ?>"
                                                                   data-validation-engine="validate[required]"/>
                                                            <label class="transition01"
                                                                   for="Rooms_Kids_<?php echo $i5; ?>_Name">Nume
                                                                copil <?php echo $copil[ $i5 + 1 ] . $year ?>
                                                                *</label>

                                                        </div>
                                                        <div class="box_n_p_2_2 input new_input_text">
                                                            <input class="necesar"
                                                                   id="Rooms_Kids_<?php echo $i5; ?>_LastName"
                                                                   name="prenume_copil_<?php echo( $i5 + 1 ); ?>"
                                                                   type="text"
                                                                   value="<?php echo $_POST[ 'prenume_copil_' . ( $i5 + 1 ) ]; ?>"
                                                                   data-validation-engine="validate[required]"/>
                                                            <label class="transition01"
                                                                   for="Rooms_Kids_<?php echo $i5 ?>_LastName">Prenume
                                                                copil <?php echo $copil[ $i5 + 1 ] . $year ?>
                                                                *</label>

                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text"
                                                                   name="data_copil_<?php echo( $i5 + 1 ); ?>"
                                                                   id="input_<?php echo $i5; ?>"
                                                                   value="<?php echo $_POST[ 'data_copil_' . ( $i5 + 1 ) ]; ?>"
                                                                   placeholder="Data Nasterii <?php echo $copil[ $i5 + 1 ] . $year ?>*"
                                                                   class="data_nasterii_copil_<?php echo( $i5 + 1 ); ?> form-control"
                                                                   data-validation-engine="validate[required]"></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
										<?php } ?>

                                        <input id="Rooms_<?php echo $iii1 ?>__RoomCode"
                                               name="Rooms[<?php echo $iii1 ?>].RoomCode" type="hidden" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content_char_book_left_row">
                            <div class="content_char_detalii_left_content">
                                <div class="detalii_char_over_box_header">
                                    <i class="fa fa-phone fa-fw"></i> Detalii de facturare
                                </div>
                                <div class="content_char_detalii_facturare">
                                    <div class="char_detalii_facturare_type_box">
                                        <div class="char_detalii_facturare_type_box_pj char_detalii_facturare_type_box_box"
                                             id="content_char_detalii_facturare_pj"
                                             data-billing-type="JuridicalPerson">
                                            Persoana juridica
                                        </div>
                                        <div class="char_detalii_facturare_type_box_pf char_detalii_facturare_type_box_box new_active"
                                             id="content_char_detalii_facturare_pf"
                                             data-billing-type="NaturalPerson">
                                            Persoana fizica
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="content_char_detalii_facturare_box new_active"
                                         id="content_char_detalii_facturare_pf_on">
                                        <div class="row">
                                            <div class="box_detalii_nume_prenume">
                                                <div class="form-title">Titular</div>
                                                <div class="form-wrapper">
                                                    <div class="form-group new_check">

                                                        <input id="sex_m" name="sex_fact" value="M"
                                                               type="radio" <?php if ( ( $_POST['sex_fact'] == 'M' ) ) {
															echo ' checked="checked"';
														} ?> >
                                                        <label for="sex_m" style="padding-right: 15px;">
                                                            M
                                                        </label>
                                                        <input id="sex_f" name="sex_fact" value="F"
                                                               type="radio" <?php if ( ( $_POST['sex_fact'] == 'F' ) ) {
															echo ' checked="checked"';
														} ?>>
                                                        <label for="sex_f">
                                                            F
                                                        </label>
                                                    </div>
                                                    <div class=" input new_input_text ">
                                                        <input class="necesar" id="nume" name="nume" type="text"
                                                               value="<?php echo $_POST['nume']; ?>"
                                                               data-validation-engine="validate[required]"/>
                                                        <label for="nume" class="transition01">
                                                            Nume
                                                            <small>*</small>
                                                        </label>
                                                    </div>
                                                    <div class="input new_input_text">
                                                        <input class="necesar" id="prenume" name="prenume"
                                                               type="text"
                                                               value="<?php echo $_POST['prenume']; ?>"
                                                               data-validation-engine="validate[required]"/>
                                                        <label for="prenume" class="transition01">
                                                            Prenume
                                                            <small>*</small>
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="data_nasterii_1"
                                                               id="data_nasterii_1"
                                                               placeholder="Data Nasterii"
                                                               value="<?php echo $_POST['data_nasterii_1']; ?>"
                                                               
                                                               class="data_nasterii form-control">
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="box_detalii_adresa">
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="adresa"
                                                           name="adresa" type="text"
                                                           value="<?php echo $_POST['adresa']; ?>"
                                                           data-validation-engine="validate[required]"/>
                                                    <label for="adresa" class="transition01">
                                                        Adresa din buletin
                                                        <small>*</small>
                                                    </label>
                                                    <div class="necesar_box">Adauga Adresa de domiciliu din buletin.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="box_detalii_oras_tara">
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="oras"
                                                           name="oras" type="text"
                                                           value="<?php echo $_POST['oras']; ?>"
                                                           data-validation-engine="validate[required]"/>
                                                    <label for="localitate" class="transition01">
                                                        Localitatea
                                                        <small>*</small>
                                                    </label>
                                                    <div class="necesar_box">Adauga Localitatea.</div>
                                                </div>
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="tara"
                                                           name="judet" type="text"
                                                           value="<?php echo $_POST['judet']; ?>"/>
                                                    <label for="judet" class="transition01">
                                                        Judet

                                                    </label>

                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="box_detalii_email_telefon">
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="email"
                                                           name="email" type="text"
                                                           value="<?php echo $_POST['email']; ?>"
                                                           data-validation-engine="validate[required,custom[email]]">
													<?php if ( $err_email ) { ?><label
                                                            class="error"><?php echo $err_email; ?></label><?php } else { ?>

                                                        <label for="email" class="transition01">
                                                            Adresa de email
                                                            <small>*</small>
                                                        </label>
													<?php } ?>

                                                </div>
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="telefon"
                                                           name="telefon" type="text"
                                                           value="<?php echo $_POST['telefon']; ?>"
                                                           data-validation-engine="validate[required]"/>
                                                    <label for="telefon" class="transition01">
                                                        Telefon mobil
                                                        <small>*</small>
                                                    </label>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_char_detalii_facturare_box"
                                         id="content_char_detalii_facturare_pj_on">
                                        <div class="row">
                                            <div class="content_char_detalii_pasageri_notificare">
                                                Datele societății pentru care se va emite factura <br/><br/>
                                            </div>
                                            <div class="box_detalii_adresa">
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="denumire" name="denumire" type="text"
                                                           value=""/>
                                                    <label for="denumire" class="transition01">
                                                        Denumire Societate
                                                        <small>*</small>
                                                    </label>

                                                </div>

                                                <div class="clear"></div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="box_detalii_nume_prenume">
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="cui_cnp"
                                                           name="cui_cnp" type="text" value=""/>
                                                    <label for="cui_cnp" class="transition01">
                                                        CIF
                                                        <small>*</small>
                                                    </label>

                                                </div>
                                                <div class="input new_input_text">
                                                    <input id="nr_reg_comert"
                                                           name="nr_reg_comert" type="text" value=""/>
                                                    <label for="nr_reg_comert"
                                                           class="transition01">
                                                        Nr. inregistrare
                                                        <small>*</small>
                                                    </label>

                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="box_detalii_adresa">
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="adresa_juridic"
                                                           name="adresa_juridic" type="text"
                                                           value=""/>
                                                    <label for="adresa_juridic" class="transition01">
                                                        Adresa sediului social
                                                        <small>*</small>
                                                    </label>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="box_detalii_oras_tara">
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="oras_juridic"
                                                           name="oras_juridic" type="text" value=""/>
                                                    <label for="oras_juridic" class="transition01">
                                                        Localitatea
                                                        <small>*</small>
                                                    </label>

                                                </div>
                                                <div class="input new_input_text">
                                                    <input class="necesar" id="banca"
                                                           name="banca" type="text" value=""/>
                                                    <label for="banca" class="transition01">
                                                        Banca
                                                    </label>

                                                </div>
                                                <div class="clear"></div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

						<?php
						$sel_suplimente = "SELECT * FROM oferte_servicii WHERE id_oferta='" . $id_oferta . "' AND tip_serv='Suplimente' AND obligatoriu='nu' AND value<>'' ORDER BY ordonare ASC ";
						$que_suplimente = mysql_query( $sel_suplimente ) or die( mysql_error() );
						?>
						<?php if ( sizeof( $extra_servicii_pret ) > 0 or mysql_num_rows( $que_suplimente ) > 0 ) { ?>
							<?php
							$extra_servicii_pret = msort( $extra_servicii_pret, array(
								'denumire',
								'tip_persoana'
							), $sort_flags = SORT_REGULAR );
							?>
                            <div class="content_char_book_left_row">
                                <div class="content_char_detalii_left_content">
                                    <div class="detalii_char_over_box_header ">
                                        <i class="fa fa-credit-card fa-fw"></i> Servicii optionale
                                    </div>
                                    <div class="content_char_servicii_optionale">
                                        <div class="row">
                                            <div class="row_in">
                                                <ul class="taxe-neincluse">
													<?php
													if ( sizeof( $extra_servicii_pret ) > 0 ) {
														foreach ( $extra_servicii_pret as $k__extra_servicii_pret => $v_extra_servicii_pret ) { ?>
															<?php if ( $extra_servicii_pret[ $k__extra_servicii_pret ]['denumire'] != $extra_servicii_pret[ $k__extra_servicii_pret - 1 ]['denumire'] and $k__extra_servicii_pret > 0 )
																echo "<br />" ?>
                                                            <li class="clearfix"
                                                                style="background:none; padding-left:0;">
                                                                <div class="title new_check"
                                                                     style="width: 80%;float: left;">
                                                                    <input type="checkbox"
                                                                           id="suplimente[<?php echo $v_extra_servicii_pret['id'] . $k__extra_servicii_pret; ?>]"
                                                                           name="suplimente[<?php echo $v_extra_servicii_pret['id'] . $k__extra_servicii_pret; ?>]"
                                                                           class="chOpt"
                                                                           value="- <?php echo $v_extra_servicii_pret['denumire'] . ' - ' . $value_pers . ' ' . moneda( $total_moneda ) . ' pentru  ' . $nr_persoane . ' = ' . $v_extra_servicii_pret['pret'] . ' ' . moneda( $total_moneda ); ?>"
                                                                           rel="<?php echo $v_extra_servicii_pret['pret']; ?>"
                                                                           toggle="supl_<?php echo $v_extra_servicii_pret['id'] . $k__extra_servicii_pret; ?>">
                                                                    <label for="suplimente[<?php echo $v_extra_servicii_pret['id'] . $k__extra_servicii_pret; ?>]">
																		<?php echo $v_extra_servicii_pret['denumire'] . ' - ' . $v_extra_servicii_pret['tip_persoana'] . ' - <strong>' . $v_extra_servicii_pret['pret'] . ' ' . moneda( $total_moneda ) . ' </strong>'; ?>
                                                                    </label>
                                                                </div>
                                                                <div class="value red" style="float: right;"><span
                                                                            id="supl_<?php echo $v_extra_servicii_pret['id'] . $k__extra_servicii_pret; ?>"
                                                                            style="display:none;"
                                                                            data-price="<?php echo $v_extra_servicii_pret['pret'] ?>"><?php echo $v_extra_servicii_pret['pret'] . ' ' . moneda( $total_moneda ); ?></span>
                                                                </div>
                                                            </li>
															<?php
														}
													} ?>
													<?php if ( mysql_num_rows( $que_suplimente ) > 0 ) { ?>
														<?php
														$isup = 0;
														while ( $row_suplimente = mysql_fetch_array( $que_suplimente ) ) {
															$isup ++;
															{
																if ( $row_suplimente['data_start'] != null and $row_suplimente['data_end'] != null and $row_suplimente['data_start'] != '0000-00-00' and $row_suplimente['data_end'] != '0000-00-00' ) {
																	$intersect_services = getIntersection( $row_suplimente['data_start'], $row_suplimente['data_end'], $data_plecare_normal, $data_intoarcere_normal );
																	if ( sizeof( $intersect_services['start'] ) > 0 ) {
																		$afisare = 'da';
																	} else {
																		$afisare = 'nu';
																	}
																} else if ( ( $row_suplimente['data_start'] == null and $row_suplimente['data_end'] == null ) or ( $row_suplimente['data_start'] == '0000-00-00' and $row_suplimente['data_end'] == '0000-00-00' ) ) {
																	$afisare = 'da';
																}

																if ( $afisare == 'da' ) {

																	$value_pers = str_replace( ',', '.', $row_suplimente['value'] );
																	if ( $row_suplimente['pasager'] == '/ persoana' ) {
																		$value_total = $value_pers * ( $nr_adulti + $nr_copii );
																		$nr_persoane = ( $nr_adulti + $nr_copii ) . ' pers';
																	} else if ( $row_suplimente['pasager'] == '/ adult' ) {
																		$value_total = $value_pers * $nr_adulti;
																		$nr_persoane = $nr_adulti . ' adulti';
																	} else if ( $row_suplimente['pasager'] == '/ copil' ) {
																		$value_total = $value_pers * $nr_copii;
																		$nr_persoane = $nr_copii . ' copii';
																	}
																	if ( $row_suplimente['exprimare'] == '/ zi' ) {
																		$value_total = $value_total * $nr_nopti;
																	} else if ( $row_suplimente['exprimare'] == '/ sejur' ) {
																		$value_total = $value_total;
																	}
																	?>
                                                                    <li class="clearfix"
                                                                        style="background:none; padding-left:0;">
                                                                        <div class="title new_check"
                                                                             style="width: 80%;float: left;">
                                                                            <input type="checkbox"
                                                                                   id="suplimente[<?php echo $isup; ?>]"
                                                                                   name="suplimente[<?php echo $isup; ?>]"
                                                                                   class="chOpt"
                                                                                   value="- <?php echo $row_suplimente['denumire'] . ' - ' . $value_pers . ' ' . moneda( $row_suplimente['moneda'] ) . ' ' . $row_suplimente['exprimare'] . ' x ' . $nr_persoane . ' = ' . $value_total . ' ' . moneda( $row_suplimente['moneda'] ); ?>"
                                                                                   rel="<?php echo $value_total; ?>"
                                                                                   toggle="supl_<?php echo $isup; ?>">
                                                                            <label for="suplimente[<?php echo $isup; ?>]">
																				<?php echo $row_suplimente['denumire'] . ' - <strong>' . $value_pers . ' ' . moneda( $row_suplimente['moneda'] ) . ' ' . $row_suplimente['exprimare'] . ' x ' . $nr_persoane . '</strong>'; ?>
                                                                            </label>
                                                                        </div>
                                                                        <div class="value red"
                                                                             style="float: right;">
                                                                            <span id="supl_<?php echo $isup; ?>"
                                                                                  style="display:none;"
                                                                                  data-price="<?php echo $value_total ?>"><?php echo $value_total . ' ' . moneda( $row_suplimente['moneda'] ); ?></span>
                                                                        </div>
                                                                    </li>
																<?php }

															} ?>

														<?php } ?>
													<?php } ?>
                                                </ul>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php } ?>

                        <div class="content_char_book_left_row">
                            <div class="content_char_detalii_left_content">
                                <div class="detalii_char_over_box_header ">
                                    <i class="fa fa-ticket fa-fw"></i> Reduceri:
                                </div>
                                <div class="content_char_cupon">
                                    <div class="row">
                                        <div class="row_in">
                                            <div class="row_in_left new_check">
                                                <div>
													<?php
													if ( isset( $err ) && $err == 1 ) {
														echo $eroare_mesaj;
													}
													?>
                                                </div>
												<?php if ( isset( $_COOKIE['cupon'] ) and $coupon_valid == 'da' ) { ?>
													<?php if ( $coupon['tip_valoare'] == 'procent' ) {
														$coupon['valoare_campanie'] = round( $total_pret * $coupon['valoare_campanie'] / 100, 0 );
														$coupon['moneda_campanie']  = $total_moneda;
													}
													?>
                                                    <div class="clearfix">
                                                        <div class="title red new_check coupon-reservation-update">
                                                            <input type="checkbox" name="coupon"
                                                                   value="- Cupon de Reducere = -<?php echo $coupon['valoare_campanie'] . ' ' . moneda( $coupon['moneda_campanie'] ); ?>"
                                                                   rel="- <?php echo $coupon['valoare_campanie']; ?>"
                                                                   data-value="<?php echo $coupon['valoare_campanie'] ?>"
                                                                   class="chOpt"
                                                                   id="coupon-reservation">
                                                            <label class="bold" for="coupon-reservation">
                                                                Reducere conform campaniei <span class="blue">CUPON DE REDUCERE</span>
                                                            </label><br/>
                                                            <span class="black italic smaller-09em">
                                                                Sunt de acord să folosesc CUPONUL DE REDUCERE, conform termenilor și condițiilor de folosire și am luat la cunoștință că pot să îl folosesc doar 1 dată. <strong>Detalii</strong> <a
                                                                        href="/info-cupoane-reducere.html"
                                                                        target="_blank" rel="nofollow"
                                                                        onClick="ga('send', 'event', 'cupon reducere', 'citire mai multe detalii', '<?php echo substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI']; ?>');"><span
                                                                            class="link-blue"><strong>cupoane de reducere</strong></span></a></span>
                                                            <div class="value red"
                                                                 style="font-size: 20px;margin-top: 10px;">
																<?php echo '- ' . $coupon['valoare_campanie'] . ' ' . moneda( $coupon['moneda_campanie'] ); ?>
                                                            </div>
                                                        </div>
                                                    </div>
												<?php } else { ?>
                                                    <div id="cupon-wrapper">
                                                        <div class="clearfix black">
                                                            <a onClick="$('#coupon-rezervare').toggle();"
                                                               style="cursor: pointer;"
                                                               class="link-black"><strong>Ai un CUPON DE
                                                                    REDUCERE?</strong>
                                                                click aici să-l introduci</a><br>
                                                            <div id="coupon-rezervare" class="coupon-rezervare"
                                                                 style="display:none;">
                                                                <!--                                                            <form action="-->
																<?php //echo $_SERVER['REQUEST_URI']; ?><!--" method="post">-->
                                                                <strong class="red bigger-11em">Adaugă
                                                                    cupon:</strong>
                                                                <input type="text" name="cod_cupon" id="cod_cupon">
                                                                <div id="adauga-cupon"
                                                                     class="button-blue"
                                                                     style="padding:3px 8px; font-size:13px; -webkit-border-radius:4px; border-radius:4px;"
                                                                     onclick="checkCoupon();">
                                                                    Adauga
                                                                </div>
                                                                <!--                                                            </form>-->
                                                                Detalii <a href="/info-cupoane-reducere.html"
                                                                           target="_blank" rel="nofollow"
                                                                           onClick="ga('send', 'event', 'cupon reducere', 'citire mai multe detalii', '<?php echo substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI']; ?>');"><span
                                                                            class="link-blue">cupoane de reducere</span></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <script>
                                                        function checkCoupon() {
                                                            var totalprice = <?php echo $total_pret ?>;
                                                            var moneda = '<?php echo urlencode( $total_moneda ) ?>';
                                                            $('#cupon-wrapper').load("/includes/cupon_ajax.php?cupon_code=" + $('#cod_cupon').val() + "&total_price=" + totalprice + "&moneda=" + moneda);
                                                        }
                                                    </script>
												<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content_char_book_left_row">
                            <div class="content_char_detalii_left_content">
                                <div class="detalii_char_over_box_header ">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i> Alte detalii și observații
                                </div>
                                <div class="content_char_cupon">
                                    <div class="row">
                                        <div class="row_in">
                                            <div class="row_in_left new_check">
                                                    <textarea id="observatii" name="observatii"
                                                              style="height:150px;"><?php echo $_POST['observatii']; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content_char_book_left_row">
                            <div class="content_char_detalii_left_content">
                                <div class="detalii_char_over_box_header ">
                                    <i class="fa fa-credit-card fa-fw"></i> Metoda de plata
                                </div>
                                <div class="content_char_metode_plata">
                                    <div class="row">
                                        <div class="row_in">
                                            <div class="row_in_left new_check">
                                                <input id="radio_metoda_plata1" name="modalitate_plata"
                                                       class="radio_metoda_plata send"
                                                       data-validation-engine="validate[required]"
                                                       value="card_online" <?php if ( $_POST['modalitate_plata'] == "card_online" ) {
													echo 'checked="checked"';
												} ?>
                                                       type="radio">
                                                <label for="radio_metoda_plata1">
                                                    Online cu cardul bancar </label>
                                                <div class="row_in_child_info row_in_child_1">
                                                    Acceptam si programe de rate pentru posesorii de: Star BT, Card
                                                    Avantaj, BRD Finance, Alpha Card
                                                </div>
                                            </div>
                                            <div class="row_in_right">
                                                <span>Carduri debit si credit acceptate : VISA, MasterCard, Maestro</span>
                                                <ul class="cards-logo">
                                                    <li><img src="/images/visa-master-maestro.png" alt=""></li>
                                                </ul>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="row_in">
                                            <div class="row_in_left new_check">
                                                <input id="radio_metoda_plata2" name="modalitate_plata"
                                                       class="radio_metoda_plata send"
                                                       data-validation-engine="validate[required]"
                                                       value="depunere_in_cont" <?php if ( $_POST['modalitate_plata'] == "depunere_in_cont" ) {
													echo 'checked="checked"';
												} ?>
                                                       type="radio">
                                                <label for="radio_metoda_plata2">Depunere la Banca
                                                    Transilvania </label>
                                                <div class="row_in_child_info row_in_child_1">
                                                    Puteti plati numerar in LEI sau EURO la orice sucursala a Bancii
                                                    Transilvania <strong>fara comision</strong>
                                                </div>
                                            </div>
                                            <div class="row_in_right">
                                                <span>In urma confirmarii rezervari o sa primiti un SMS si o factura proforma</span>
                                                <ul class="cards-logo">
                                                    <li><img src="/images/banca_transilvania.png"
                                                             alt="Banca Transilvania"></li>
                                                </ul>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="row_in">
                                            <div class="row_in_left new_check">
                                                <input id="radio_metoda_plata3"
                                                       name="modalitate_plata" <?php if ( $_POST['modalitate_plata'] == "plata_sediu" ) {
													echo 'checked="checked"';
												} ?>
                                                       class="radio_metoda_plata send" value="plata_sediu"
                                                       data-validation-engine="validate[required]"
                                                       type="radio">
                                                <label for="radio_metoda_plata3">Plata in Agentie </label>
                                                <div class="row_in_child_info row_in_child_1">
                                                    Va asteptam cu drag in agentia noastra din B-dul Ghe.Magheru
                                                    nr.28-30 intre orele 9:00 – 17:30

                                                </div>
                                            </div>
                                            <div class="row_in_right">
                                                <span>Plata se poate efectua in numerar sau card de debit/credit in Lei sau Euro</span>


                                            </div>


                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="row_in">
                                            <div class="row_in_left new_check">
                                                <input id="radio_metoda_plata4" name="modalitate_plata"
                                                       class="radio_metoda_plata send"
                                                       data-validation-engine="validate[required]"
                                                       value="transfer_bancar" <?php if ( $_POST['modalitate_plata'] == "transfer_bancar" ) {
													echo 'checked="checked"';
												} ?>
                                                       type="radio">
                                                <label for="radio_metoda_plata4">Ordin de plata </label>
                                                <div class="row_in_child_info row_in_child_1">
                                                    Puteti achita din orice banca in LEI sau EURO la Banca
                                                    Transilvania

                                                </div>

                                            </div>
                                            <div class="row_in_right">
                                                <span>Plata se poate efectua in numerar sau card de debit/credit in Lei sau Euro</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="row_in">
                                            <div class="row_in_left new_check">
                                                <input id="radio_metoda_plata5" name="modalitate_plata"
                                                       class="radio_metoda_plata send"
                                                       data-validation-engine="validate[required]"
                                                       value="tichete_vacanta" <?php if ( $_POST['modalitate_plata'] == "tichete_vacanta" ) {
													echo 'checked="checked"';
												} ?>
                                                       type="radio">
                                                <label for="radio_metoda_plata5">Tichete de vacanta </label>
                                                <div class="row_in_child_info row_in_child_1">
                                                    Pentru vacante rezervate in Romania acceptam plata cu tichete de
                                                    vacanta
                                                </div>
                                            </div>
                                            <div class="row_in_right">
                                                <span>Tichete de vacanta acceptate: Sodexo (Turist Pass), Edenred (Ticket Vacanta) si UP Romania(Cheque Vacances)</span>
                                                <ul class="cards-logo">
                                                    <li><img src="/images/tichete-vacanta.png" alt=""></li>
                                                </ul>

                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php $sel_avioane = "SELECT * from pret_planes_cache where id_zbor='" . $id_zbor_rez . "' and id_hotel='" . $id_hotel . "' GROUP by pret_planes_cache.ukey, pret_planes_cache.from order BY pret_planes_cache.data_adaugarii desc";
						//echo $sel_avioane;
						$que_avioane = mysql_query( $sel_avioane ) or die( mysql_error() );
						//$row_avioane = mysql_fetch_array($que_avioane);
						$ii = 0;
						while ( $row_avioane = mysql_fetch_array( $que_avioane ) ) {
							$flight[ $row_avioane['id_zbor'] ]['flight_bookable']                                  = $row_avioane['flight_bookable'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['companie']     = $row_avioane['companie'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['nr_cursa']     = $row_avioane['nr_cursa'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['from']         = $row_avioane['from'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['to']           = $row_avioane['to'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['data_plecare'] = $row_avioane['data_plecare'];
							$flight[ $row_avioane['id_zbor'] ][ $row_avioane['tip_cursa'] ][ $ii ]['data_sosire']  = $row_avioane['data_sosire'];
							$ii ++;
						}

						//if(!$err_logare_admin) echo $sel_avioane ;
						//if(!$err_logare_admin) { echo '<pre>';print_r($flight);echo '</pre>'; }

						?>

						<?php if ( sizeof( $flight ) > 0 ) { ?>
                            <div class="content_char_book_left_row">
                                <div class="content_char_detalii_left_content">
                                    <div class="detalii_char_over_box_header ">
                                        <i class="fa fa-plane fa-fw"></i> Orar de zbor
                                    </div>
                                    <div class="content_char_termeni_conditii">
                                        <div class="row row_active box_c_contract_turist">
                                            <div class="row row_active box_c_contract_turist">

												<?php foreach ( $flight as $id_zbor_a => $zbor ) {
													//if(!$err_logare_admin) { echo '<pre>';print_r($zbor);echo '</pre>';echo $id_zbor_a;  }
													if ( $zbor['flight_bookable'] == 1 and sizeof( $zbor['plecare'] ) > 0 and $id_zbor_a == $id_zbor_rez ) { ?>

														<?php
														echo '<div id="flight' . $id_oferta . '" >';
														echo '<table class="search-rooms mar10-0"><tr class="bkg-white"><th class="text-center">Companie</td><th class="text-center">Nr. cursă</th><th class="text-center">Plecare</th><th class="text-center">Sosire</th>';
														foreach ( $zbor['plecare'] as $k_av_out => $v_cursa_out ) {
															//	foreach ( $v_av_out as $k_cursa_out => $v_cursa_out ) {
															echo '<tr class="bkg-grey">';
															echo '<td class="text-center">';
															if ( strlen( get_airline_by_iata( $v_cursa_out['companie'] ) ) > 0 ) {
																echo '<img src="/images/avion/' . get_airline_by_iata( $v_cursa_out['companie'] ) . '.jpg" alt=""> ';
															}
															echo '</td>';
															echo '<td class="text-center">' . $v_cursa_out['nr_cursa'] . '</td>';
															echo '<td class="text-center"><strong>' . $v_cursa_out['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_out['data_plecare'] ) . '</td>';
															echo '<td class="text-center"><strong>' . $v_cursa_out['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_out['data_sosire'] ) . '</td>';
															echo '</tr>';
															//	}
														}
														foreach ( $zbor['intoarcere'] as $k_av_in => $v_cursa_in ) {
															//	foreach ( $v_av_in as $k_cursa_in => $v_cursa_in ) {
															echo '<tr class="bkg-white">';
															echo '<td class="text-center">';
															if ( strlen( get_airline_by_iata( $v_cursa_in['companie'] ) ) > 0 ) {
																echo '<img src="/images/avion/' . get_airline_by_iata( $v_cursa_in['companie'] ) . '.jpg" alt=""> ';
															}
															echo '</td>';
															echo '<td class="text-center">' . $v_cursa_in['nr_cursa'] . '</td>';
															echo '<td class="text-center"><strong>' . $v_cursa_in['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_in['data_plecare'] ) . '</td>';
															echo '<td class="text-center"><strong>' . $v_cursa_in['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_in['data_sosire'] ) . '</td>';
															//if($v_cursa_in['seats']) echo ' - '.$v_cursa_in['seats'].' locuri';
															echo '</tr>';
															//	}
														}
														echo '</table>';
														echo '</div>';
													}
												} ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php } ?>

                        <div class="content_char_book_left_row">
                            <div class="content_char_detalii_left_content">
                                <div class="detalii_char_over_box_header ">
                                    <i class="fa fa-file-o fa-fw"></i>Conditii de anulare si de plata
                                </div>
                                <div class="content_char_termeni_conditii">
                                    <div class="row row_active box_c_contract_turist">
                                        <div class="row row_active box_c_contract_turist">
											<?php if ( $conditii_anulare ) : ?>
                                                <div>
                                                    <div class="row_in new_check conditii-wrapper">
                                                        <h2>Conditii de anulare :</h2>
                                                        <ul class="taxe-neincluse">
															<?php foreach ( $conditii_anulare as $a_key => $a_value ) {
																if ( $a_value['valoare'] == '0' ) {
																	?>
                                                                    <li>
                                                                        <span class="bkg-dark-green underline bigger-13em white pad5"> Anulare <strong>GRATUITA</strong> pana la data de <?php echo denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ); ?></span>
                                                                    </li>
																	<?php $camp_anulare = 'Anulare GRATUITA pana la data de ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ) . '\n'; ?>
																<?php } else {
																	?>
                                                                    <li>Pentru anularea rezervarii in perioada
                                                                        <strong><?php echo denLuniRo( date( 'd F Y', strtotime( $a_value['data_inceput'] ) ) ) . ' - ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ); ?></strong>
                                                                        penalizarea este de
                                                                        <strong><?php echo $a_value['valoare'];
																			if ( $a_value['tip_valoare_anulare'] == 'procent' ) {
																				echo '% din valoare totala a sejurului';
																			}
																			if ( $a_value['tip_valoare_anulare'] == 'suma_fixa' ) {
																				echo '  ';
																			} ?>


                                                                        </strong>
                                                                    </li>
																	<?php $camp_anulare .= '- Pentru anularea rezervarii in perioada ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_inceput'] ) ) ) . ' - ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ) . ' penalizarea este de <strong>' . $a_value['valoare'] . '% </strong> din valoare totala a sejurului \n'; ?>

																<?php } ?>

															<?php } ?>

                                                        </ul>
                                                    </div>
                                                </div>
											<?php endif; ?>
                                            <div>
                                                <div class="row_in new_check conditii-wrapper">
                                                    <h2>Conditii de plata :</h2>
                                                    <ul class="taxe-neincluse">
														<?php $nr_zile_expira = abs( floor( ( time() - strtotime( $encoded[2] ) ) / ( 60 * 60 * 24 ) ) ); ?>
														<?php if ( $nr_zile_expira > 20 ) { ?>
															<?php if ( $avans_plata > 20 ) { ?>
                                                                <li>Avans minim <strong><?php echo $avans_plata ?>
                                                                        %</strong> din valoarea sejurului (
                                                                    <strong><?php echo round( $total_pret * $avans_plata / 100 ) . ' ' . $total_moneda; ?></strong>)
                                                                </li>
															<?php } ?>
															<?php
															if ( strtotime( $data_eb ) > strtotime( "+1 day" ) ) { ?>
                                                                <li>Restul sumei se va achita pana la data
                                                                    de <?php echo denLuniRo( date( 'd F Y', strtotime( $data_eb ) ) );
																	$plata_rest = date( 'Y-m-d', strtotime( $data_eb ) ); ?></li>
															<?php } else { ?>
                                                                <li>Daca rezervarea este in sitem Early Booking
                                                                    restul
                                                                    sumei se va schita pana la sfarsitul perioadei
                                                                    de
                                                                    early booking
                                                                </li>
                                                                <li>Daca oferta este Black Friday se va achita
                                                                    integral
                                                                    in maxim 24 de ore de la data confirmarii
                                                                </li>
                                                                <li>Daca rezervarea NU ESTE in sistem Early Booking
                                                                    se
                                                                    va achita integral pana la
                                                                    data <?php echo denLuniRo( date( 'd F Y', strtotime( $encoded[2] . ' - ' . $nr_zile_plata . ' days' ) ) );

																	$plata_rest = date( 'Y-m-d', strtotime( $encoded[2] . ' - ' . $nr_zile_plata . ' days' ) ); ?>
                                                                </li>
																<?php
															}

														} else { ?>
                                                            <li>Rezervarea trebuie achitata in maxim 24 de ore de la
                                                                data confirmarii.
                                                            </li>

															<?php
															$plata_rest = date( 'Y-m-d' + '1 days' );

														} ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content_char_book_left_row">
                            <div class="content_char_detalii_left_content">
                                <div class="detalii_char_over_box_header ">
                                    <i class="fa fa-file-o fa-fw"></i> Termeni si conditii
                                </div>
                                <div class="content_char_termeni_conditii">
                                    <div class="row row_active box_c_contract_turist">
                                        <div class="row_in new_check">
                                            <input type="hidden" name="included_services" id="included_services"
                                                   value="<?php echo $included_services; ?>">
                                            <input name="trimite" type="hidden" value="trimite"/>
                                            <input type="hidden" name="camp_anulare"
                                                   value="<?php echo $camp_anulare; ?>">
                                            <input type="hidden" name="plata_rest" value="<?php echo $plata_rest ?>">

                                            <input id="c_contract_turist4"
                                                   name="termeni_conditii"
                                                   type="checkbox"
                                                   value="da" <?php if ( $_POST['termeni_conditii'] == 'da' ) {
												echo 'checked="checked"';
											} ?> data-validation-engine="validate[required]"
                                                   value="da" <?php if ( $_POST['termeni_conditii'] == 'da' ) {
												echo 'checked="checked"';
											} ?> />
                                            <!--                                            <input name="ReadTerms" type="hidden" value="false"/>-->
                                            <label for="c_contract_turist4">
                                                Am citit si sunt de acord cu <a href="#"
                                                                                class="a_termeni" target="_blank"
                                                                                title="Se deschide in alta pagina (Contract Turist)">contractul
                                                    de turist</a> si cu <a href="#" class="a_termeni"
                                                                           target="_blank"
                                                                           title="Se deschide in alta pagina (Termeni si Conditii)">termenii
                                                    si conditiile</a> agentiei de turism. </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content_char_book_left_row">
                        </div>
                        <div class="pret_buton_rezerva">
                            <!-- <a href="javascript:{}" onclick="document.getElementById('bookFrm').submit(); return false;" id="final_rezervare" class="transition04 btn">
								REZERVA ACUM
							 </a>-->

                            <input type="submit" value="Finalizeaza rezervarea" class="button-red"/>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    if (document.documentElement.clientWidth > 1000) {
        //Sticky Sidebar
        var stickySidebar = $('.sticky');
        if (stickySidebar.length > 0) {
            var stickyHeight = stickySidebar.height(),
                sidebarTop = stickySidebar.offset().top;
        }

        // on scroll move the sidebar
        $(window).scroll(function () {
            if (stickySidebar.length > 0) {
                var scrollTop = $(window).scrollTop();

                if (sidebarTop < scrollTop) {
                    stickySidebar.css('top', scrollTop - sidebarTop);
                    // stop the sticky sidebar at the footer to avoid overlappinz
                    var sidebarBottom = stickySidebar.offset().top + stickyHeight,
                        stickyStop = $('.main-content').offset().top + $('.main-content').height();
                    if (stickyStop < sidebarBottom) {
                        var stopPosition = $('.main-content').height() - stickyHeight;
                        stickySidebar.css('top', stopPosition);
                    }
                }
                else {
                    stickySidebar.css('top', '0');
                }
            }
        });

        $(window).resize(function () {
            if (stickySidebar.length > 0) {
                stickyHeight = stickySidebar.height();
            }
        });
    }


    function remove_place() {
        $("form .new_input_text > input").each(function (e, a) {
            $(a).val() && $(a).addClass("used");
        })
    }

    remove_place();
</script>

<script>
    $(".data_nasterii").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+0',
        dateFormat: "yy-mm-dd",
        defaultDate: "-18y",
        showButtonPanel: true
    });
	<?php for ( $i5 = 0; $i5 < $nr_copii; $i5 ++ ) : ?>
	<?php
	$newStartDate = date( 'Y-m-d', strtotime( $encoded[2] . ' + 1 day' ) );
	$newStartDate = date( 'Y-m-d', strtotime( $newStartDate . '- ' . ( $copil[ $i5 + 1 ] + 1 ) . 'years' ) );
	if ( ! $err_logare_admin ) {

	}
	?>
    $(".data_nasterii_copil_<?php echo( $i5 + 1 ); ?>").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '-<?php echo ( $copil[ $i5 + 1 ] + 1 ) ?>:+0',
        dateFormat: "yy-mm-dd",
        defaultDate: "-<?php echo( $copil[ $i5 + 1 ] + 1 ) ?>y",
        minDate: "<?php echo $newStartDate ?>",
        maxDate: "<?php echo date( 'Y-m-d' ) ?>",
        showButtonPanel: true
    });
	<?php endfor; ?>

    //Take text value and insert
    $('#nume_adl_1').on('change', function (e) {
        $('#nume').val($(this).val()).addClass('used');
    });
    $('#prenume_adl_1').on('change', function (e) {
        $('#prenume').val($(this).val()).addClass('used');
    });
    $("#data_adl_1").on('change', function () {
        $("#data_nasterii_1").val($(this).val()).addClass('used');
    });
    $('input[name="sex_adl_1"]').on('change', function () {
        var value = $(this).val();
        console.log(value);
        $(document).find('input[name="sex_fact"]').each(function () {
            if ($(this).val() === value) {
                $(this).attr('checked', true);
                $(this).addClass('used');
            } else {
                $(this).attr('checked', false);
                $(this).removeClass('used');
            }
        });
    });

    $('form input').blur(function () {
        if ($(this).val().length !== 0) {
            $(this).addClass('used');
        }
    });

    $('#content_char_detalii_facturare_pj').on('click', function (e) {
        $('#' + $(this).attr('id') + '_on').addClass('new_active');
    });
    $('#content_char_detalii_facturare_pf').on('click', function (e) {
        $('#content_char_detalii_facturare_pj_on').removeClass('new_active');
    });
</script>

<script>
    $(document).on('click', '.coupon-reservation-update label', function () {
        if ($(this).closest('.coupon-reservation-update').find('input:checked').length == 0) {
            $(document).find('.discount-value').html($('#coupon-reservation').attr('rel'));
            $('#coupon-reservation').addClass('apply-discount');
            $(document).find('.discount-value').closest('li').show();
        } else {
            $('#coupon-reservation').removeClass('apply-discount');
            $(document).find('.discount-value').closest('li').hide();
        }
        recalculatePrices();
    });

    $(document).on('click', '.content_char_servicii_optionale li label', function () {
        var check = $(this).siblings('input').attr('toggle');
        $(this).closest('li').find('#' + check).toggle().toggleClass('add-value');
        recalculatePrices()
    });

    function recalculatePrices() {
        var initialPrice = $('.initial-price').data('price');
        var included_services = '<?php echo $included_services?> ';
        $('.content_char_servicii_optionale li .value .add-value').each(function () {
            initialPrice = parseFloat(initialPrice) + parseFloat($(this).data('price'));
            included_services += $(this).closest('li').find('.chOpt').val() + '<br>';
        });
        if ($(document).find('.apply-discount').length > 0) {
            initialPrice = parseFloat(initialPrice) - parseFloat($(document).find('.apply-discount').data('value'));
            included_services += $(document).find('.apply-discount').val() + '<br>';
        }
        $('.total-price-value').html(Math.round(parseFloat(initialPrice)));
        $('.final-price').val(Math.round(parseFloat(initialPrice)));
        $('#total_pret').val(Math.round(parseFloat(initialPrice)));
        $("#included_services").val(included_services);
    }
</script>

<?php require_once( "includes/footer/footer_responsive.php" ); ?>
<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>
</body>
</html>
