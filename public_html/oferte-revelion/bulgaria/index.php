<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$link_new = '/oferte-revelion/bulgaria/';

$sel_dest = "SELECT

COUNT(oferte.id_oferta) AS numar,
zone.denumire as denumire_zona,
zone.id_zona as id_zona,
tari.denumire as denumire_tara,
tari.id_tara,
tari.descriere_scurta
FROM oferte
INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'da'
AND tari.id_tara = '3' and id_tip_oferta='33'
AND hoteluri.tip_unitate <> 'Circuit'
GROUP BY zone.id_zona
ORDER BY numar DESC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['numar'][] = $row_dest['numar'];
	$destinatie['link'][] = '/oferte-revelion/bulgaria/'.fa_link($row_dest['denumire_zona']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$row_dest['id_tara'].'/';
	$destinatie['id_tara'][] = $row_dest['id_tara'];
	$destinatie['info_scurte'][] = "Revelion 2017 ".$row_dest['denumire_zona']." -".$row_dest['denumire_tara'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
} @mysql_free_result($rez_dest);
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Revelion 2017 Bulgaria - Litoral, munte, Veliko Tarnovo</title>
<meta name="description" content="Sejururi turism extern la cele mai bune preturi oferite de Ocaziituristice.ro pentru turistii care doresc sejururi sau circuite in afara Romaniei" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte Revelion 2017 Bulgaria</h1>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>

        <br class="clear">
        <div class="Hline"></div>
        
		<?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>
        
        <br class="clear">
        <br class="clear">
        
<?php /*?>        <div class="pad5">
          <br class="clear">
          
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Oferte <?php echo $dentip; ?>:</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
        </div><?php */?>
        
	

        <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <div id="destinations"  class="clearfix">
          <?php foreach($destinatie['den_zona'] as $key1 => $value1) { ?>
          <div class="destItem NEW-round4px clearfix" >
            <div class="topBox" onclick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
              <img src="/images/revelion-bulgaria/<?php echo fa_link($destinatie['den_zona'][$key1]); ?>.jpg" alt="Revelion <?php echo $destinatie['den_zona'][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white" style="font-size:24px"> Click pentru oferte</i>
              <h2>Revelion 2017 <?php echo $destinatie['den_zona'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?>
            <div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?>
            </div><?php } ?>
            <div class="bottomBox clearfix">
              <div class="price" align="center"><span class="red"><span class="pret"><?php echo $destinatie['numar'][$key1]; ?></span> </span> oferte revelion <span class="red"><span class="pret"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Revelion <?php echo $destinatie['den_zona'][$key1]; ?>">
			  <?php echo $destinatie['den_zona'][$key1]; ?></a></span></span></div>
             
            </div>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        
        <br>
		
        <?php /*?><div class="pad10">
          <img src="images/icon-sejur-grecia.jpg" alt="Sejur Grecia" class="float-left" style="margin:10px 10px 0 0;">
          <h2><strong>Sejur Grecia</strong> - pentru vacanțe de neuitat</h2>
          <p class="newText text-justify">Cei mai mulţi turişti care optează pentru un <em>sejur Grecia</em> se reîntorc an de an. Farmecul acestei zone, diversitatea culturală şi frumuseţea pitorească a peisajelor sunt greu de egalat de alte destinaţii turistice din Europa. Grecia este una dintre puţinele destinaţii de vacanţă cu o astfel de bogăţie şi frumuseţe ce pot fi vizitate inclusiv de persoanele pentru care vacanţa înseamnă întotdeauna buget restrâns şi mare atenţie la cheltuieli.</p>
          
          <br class="clear">
          
          <p class="newText text-justify">În materie de oferte, <strong>Grecia</strong> se numără printre cele mai economice destinaţii din Europa, iar pentru cei care aleg să îşi plănuiască vacanţa din timp, absolut totul, de la biletele de avion sau de autocar până la cazare se încadrează cu uşurinţă chiar şi în cele mai austere bugete de călătorie. Farmecul Greciei stă întocmai în diversitatea culturală a acestei zone şi în numeroasele opţiuni pe care le are de oferit pentru toate tipurile de turişti: numeroase obiective turistice din patrimoniul UNESCO, o bogată cultură locală, numeroase comori nedescoperite şi o diversitate de relief ce transformă un sejur Grecia în cea mai intensă şi bogată experienţă de vacanţă.</p>
          <p class="newText text-justify">Ospitalitatea grecească este bine cunoscută în toată lumea, iar acest detaliu uimeşte şi încântă turiştii încă de la primul contact. Indiferent că este vorba de personalul din hoteluri Grecia, de ospătarii de la restaurante sau de sătenii din pitoreştile zone rurale greceşti, aici turistul se simte mereu ca acasă, mereu bine venit, iar dorinţa de a te reîntoarce curând apare chiar imediat.</p>
          <p class="newText text-justify">În funcţie de buget şi preferinţe, turiştii pot opta pentru diverse tipuri de sejururi, de la cele în <a href="/sejur-grecia/?optiuni=da&masa=all-inclusive" class="link-blue" title="Grecia all inclusive">hoteluri all inclusive Grecia</a> la pensiunile mediteraneene tipice, de la călătorii la mare, la adevărate aventuri care rămân vii în amintire ani la rândul. Indiferent de tipul de pachet ales, o vacanţă în Grecia va însemna întotdeauna mâncare excelentă, peisaje de vis, vreme superbă şi o cultură locală care îşi lasă amprenta, ca o ştampilă pe paşaport, în inima fiecărui turist în parte.</p>
        </div>

        <br class="clear"><?php */?>
        
       <?php /*?> <?php if(sizeof($destinatie['den_tara'])>0) { ?>
        <h3 class="mar10">Sejururi și cazări disponibile</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie['den_tara'] as $key2 => $value2) {
			  $part_anchor = 'Sejur ';
		  ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie['link'][$key2]; ?>" title="<?php echo $part_anchor.$value2; ?>" class="link-blue block"><?php echo $part_anchor.$value2; ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?>
        <?php */?>
        <br class="clear"><br>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>