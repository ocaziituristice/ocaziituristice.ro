<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip = "revelion";
$linktip = "revelion";
$dentip = "Revelion";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii = get_id_tip_sejur_fii($id_tip_sejur);
$iduri = "'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri = $iduri.','.$tip_fii['id_tipuri'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Revelion <?php echo date("Y")+1; ?> | ocaziituristice.ro</title>
<meta name="description" content="Oferte Revelion <?php echo date("Y")+1; ?> in Romania si strainate, sejururi de Revelion si circuite, cazare Revelion Romania, sejuri speciale de Revelion " />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte Revelion <?php echo date("Y")+1; ?></h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear">
        
        <div class="Hline"></div>
        
        <div class="pad5">
              
          <br class="clear"><br>
          
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Oferte <?php echo $dentip; ?>:</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
          
          <div class="float-left" style="width:630px;">
            <h2 style="margin:0; padding:0;"><a href="/oferte-revelion/romania/" title="Oferte Revelion Romania" class="cb-item" style="background:url(images/oferte-revelion-romania.jpg) left top no-repeat; width:610px; height:180px; margin:0; font-weight:normal;"><span class="titlu">Revelion Romania</span></a></h2>
            <div class="coloana-links" style="width:190px; margin-right:30px;">
			<?php
			$sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND zone.tpl_romania = '2'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			while($row_zone=mysql_fetch_array($que_zone)) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
            <div class="coloana-links" style="width:190px; margin-right:30px;">
			<?php
			$sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND zone.tpl_romania IS NULL
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			while($row_zone=mysql_fetch_array($que_zone)) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
            <div class="coloana-links" style="width:190px;">
			<?php
            $sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND zone.tpl_romania = '1'
			AND zone.denumire = 'Statiuni Balneare'
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			$row_zone=mysql_fetch_array($que_zone);
			if(mysql_num_rows($que_zone)>0) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
          </div>
          
          <div class="float-right" style="width:300px;">
            <h2 class="cb-item" style="background:url(images/oferte-revelion-extern.jpg) left top no-repeat; width:280px; height:180px; margin:0; font-weight:normal;"><span class="titlu">Extern</span></h2>
            <div class="coloana-links" style="width:300px;">
            <p class="transport" style="background-image:url(/images/oferte/icon_small_transport_individual.png);"> Transport Individual</p>
			<?php
			 $sel_tari="SELECT
			COUNT(oferte.id_oferta) AS numar,
			tari.denumire,
			tari.id_tara,
			tari.descriere_scurta,
			tari.steag
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel=hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id=localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona=zone.id_zona
			INNER JOIN tari ON zone.id_tara=tari.id_tara
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
			WHERE oferte.valabila='da' AND hoteluri.tip_unitate<>'Circuit'
			AND tari.id_tara<>'1'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND oferte.id_transport='1'
			GROUP BY tari.id_tara
			ORDER BY numar DESC ";
			$que_tari=mysql_query($sel_tari) or die(mysql_error());
			while($row_tari=mysql_fetch_array($que_tari)) {
				echo '<a href="/oferte-'.$linktip.'/'.fa_link($row_tari['denumire']).'/" title="Oferte '.$dentip.' '.$row_tari['denumire'].'" class="extern link-blue"><img src="/thumb_steag_tara/'.$row_tari['steag'].'" alt="steag '.$row_tari['denumire'].'" width="32" height="20"> '.$dentip.' '.$row_tari['denumire'].'</a>';
			}
			?>
            <p class="transport" style="background-image:url(/images/oferte/icon_small_transport_avion.png);"> Transport cu Avionul</p>
			<?php
			$sel_tari="SELECT
			COUNT(oferte.id_oferta) AS numar,
			tari.denumire,
			tari.id_tara,
			tari.descriere_scurta,
			tari.steag
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel=hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id=localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona=zone.id_zona
			INNER JOIN tari ON zone.id_tara=tari.id_tara
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
			WHERE oferte.valabila='da' AND hoteluri.tip_unitate<>'Circuit'
			AND tari.id_tara<>'1'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND oferte.id_transport='4'
			GROUP BY tari.id_tara
			ORDER BY numar DESC ";
			$que_tari=mysql_query($sel_tari) or die(mysql_error());
			while($row_tari=mysql_fetch_array($que_tari)) {
				echo '<a href="/oferte-'.$linktip.'/'.fa_link($row_tari['denumire']).'/" title="Oferte '.$dentip.' '.$row_tari['denumire'].'" class="extern link-blue"><img src="/thumb_steag_tara/'.$row_tari['steag'].'" alt="steag '.$row_tari['denumire'].'" width="32" height="20"> '.$dentip.' '.$row_tari['denumire'].'</a>';
			}
			?>
            <?php /*?><p class="transport" style="background-image:url(/images/oferte/icon_small_transport_autocar.png);"> Transport cu Autocarul</p><?php */?>
			<?php
			$sel_tari="SELECT
			tari.denumire,
			tari.steag,
			continente.nume_continent
			FROM oferte
			INNER JOIN hoteluri on oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
			INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara
			INNER JOIN continente ON continente.id_continent = tari.id_continent
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate = 'Circuit'
			AND traseu_circuit.tara_principala = 'da'
			AND tari.id_tara <> '1'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			GROUP BY tari.denumire
			ORDER BY tari.denumire ASC ";
			$que_tari=mysql_query($sel_tari) or die(mysql_error());
			while($row_tari=mysql_fetch_array($que_tari)) {
				echo '<a href="/circuite-'.$linktip.'/'/*.fa_link($row_tari['nume_continent']).'/'*/.fa_link($row_tari['denumire']).'/" title="Oferte '.$dentip.' '.$row_tari['denumire'].'" class="extern link-blue"><img src="/thumb_steag_tara/'.$row_tari['steag'].'" alt="steag '.$row_tari['denumire'].'" width="32" height="20"> '.$dentip.' '.$row_tari['denumire'].'</a>';
			}
			?>
            </div>
          </div>
          
        </div>
        
      </div>

    </div>
    <br>
      
          <img src="images/oferte-revelion.jpg" width="400" height="250" alt="Oferte Revelion" class="float-left" style="margin:0 10px 0 0;">
          <p class="text-justify text"><strong>Revelionul</strong> marchează nu numai începutul noului an calendaristic, dar și o sărbătoare încărcată de belșug și voie bună. Preluat din limba franceză, <i>termenul Revelion</i> semnifică petrecerea cu  "ospăț la miezul nopții", iar obiceiul  de a sărbători cu multă zarvă, cu petarde și artificii, are scopul de a alunga spiritele rele și a primi noul an cu multă bucurie. Și cum, odată în an petrecem <strong>Revelionul</strong>, nu trebuie să ratăm ocazia de a face din această sărbătoare o amintire specială.</p>
          <p class="text-justify text">Compania familiei sau a prietenilor este, cu siguranță, o alegere potrivită pentru  noaptea dintre ani. O locație inedită, însă, poate face diferența.</p>
          <p class="text-justify text">Lasă-te ademenit de obiceiurile si datinile străbune, de mirosul de cozonac şi de preparatele tradiţionale, de minunatele colinde, de ţuica fiartă şi vinul fiert de pe pârtiile de ski din România, alege să petreci <a href="http://www.ocaziituristice.ro/oferte-revelion/romania/" title="Oferte Revelion Romania" class="link-blue">revelionul acasă</a>.</p>
          <p class="text-justify text">Un revelion petrecut în stil grecesc, așa cum cer datinile și obiceiurile poporului zeilor Olimpului, va condimenta petrecerea va îndulci trecerea anului cu faimoasa prăjitură tradițională în care gospodinele ascund bănuți norocoși.</p>
          <p class="text-justify text"><a href="http://www.ocaziituristice.ro/circuite/asia/israel/" title="Oferte Israel" class="link-blue">Israelul</a> te va fascina cu cele mai bogate ospățuri și îți va asigura un <strong>revelion savuros</strong>. Alege oferta de revelion în Israel și, alături de bucatele rafinate, vei mânca mere unse cu miere sau alte bunătăți numai bune de întâmpinat noul an.</p>
          <p class="text-justify text"><strong>Revelionul în Italia</strong> te îndeamnă să închini paharul la miezul nopții și să arunci cupa de șampanie pe fereastră. Astfel, se crede, se renunță la cele vechi pentru a face loc celor noi.</p>
          <p class="text-justify text">Iar dacă îți plac strugurii, te vei bucura să afli că, în Spania, e musai să mănânci 12 boabe de struguri, ca ritual pentru atragerea norocului.</p>
          <p class="text-justify text">Află tradiții exotice și obiceiuri de <a href="http://www.ocaziituristice.ro/oferte-revelion/dubai/" title="Oferte Revelion Dubai" class="link-blue">revelion din Dubai</a>, Hong Kong sau Mauritius, alegând una dintre ofertele speciale de revelion, lansate de agenția noastră.  Sau lasă-te cucerit de pârtiile de ski din <a href="http://www.ocaziituristice.ro/sejur-bulgaria/" title="Oferte Bulgaria" class="link-blue">Bulgaria</a> și menține-te în formă, petrecând Revelionul în stil sportiv.</p>
          <p class="text-justify text">Oricare va fi alegerea ta, înconjoară-te de cei pe care îi iubești și nu lăsa <strong>Revelionul</strong> să treacă fără să te bucuri cât poți mai mult. Organizarea las-o pe mâna noastră și nu vei regreta!</p>
          <p class="text-justify text">Va urăm şi noi noroc, sănătate şi "La mulţi ani!"</p>   
    
    
    
    
    <br />
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=<?php echo $id_tip_sejur; ?>");
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>