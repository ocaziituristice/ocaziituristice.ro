<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
$redirect_link = $_SERVER['HTTP_REFERER'];
$timeout = time() + 60 * 60 * 24 * 30;
$timeout_del = time() - 60 * 60;

if(isset($_REQUEST['gengroc'])) {
	$tip_transport = $_REQUEST['tip_transport'];
	if(isset($_REQUEST['oras_plecare_autocar'])) $orasplec = $_REQUEST['oras_plecare_autocar'];
		else if(isset($_REQUEST['oras_plecare_avion'])) $orasplec = $_REQUEST['oras_plecare_avion'];
		else $orasplec = get_id_localitate('Bucuresti');
	
	$value_grad_ocupare = $_REQUEST['gengroc'].'*'.$tip_transport.'*'.$orasplec;
	
	setcookie("grad_ocupare", $value_grad_ocupare, $timeout, "/");
	
	$redirect_link = $redirect_link.'#calc';
}

if(isset($_REQUEST['del']) and $_REQUEST['del']=='grdocp') {
	setcookie("grad_ocupare", "", $timeout_del, "/");
	$redirect_link = $redirect_link.'#calc';
}

redirect_php($redirect_link);
?>
