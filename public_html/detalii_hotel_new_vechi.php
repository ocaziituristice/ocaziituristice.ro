<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');

$nume_hotel = desfa_link($_REQUEST['nume']);
//echo $nume_hotel = strtr($nume_hotel,'_w_','and');
//desfa_link($_REQUEST['nume'])
$id_hotel = get_idhotel($nume_hotel, desfa_link($_REQUEST['localitate']));
//echo $_REQUEST['localitate'];
$det = new DETALII_HOTEL();
$detalii_hotel = $det->select_camp_hotel($id_hotel);
$det->hotel_vizitat($id_hotel);
$tip_pagina='offerdetail';


$offers = $det->get_oferte($id_hotel);
$nr_oferte = count($offers);



$id_oferta = '';
$den_hotel=ucwords($detalii_hotel['denumire']);
$den_tara=ucwords($detalii_hotel['tara']);
$den_zona=ucwords($detalii_hotel['zona']);
$den_localitate=ucwords($detalii_hotel['localitate']);
if($_REQUEST['localitate']<>fa_link_oferta($detalii_hotel['localitate']) || !$detalii_hotel['denumire'] || $detalii_hotel['tip_unitate']=='Circuit') {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}

if($detalii_hotel['titlu_seo']=="") {
	$metas_title = $detalii_hotel['denumire'].' din '.$detalii_hotel['localitate'].', '.$detalii_hotel['tara'];
} else {
	$metas_title = $detalii_hotel['titlu_seo'];
}
if($detalii_hotel['descriere_seo']=="") {
	$metas_description = $detalii_hotel['denumire'].' '.$detalii_hotel['localitate'].' '.$detalii_hotel['tara'].' - '.$detalii_hotel['stele'].' stele . Oferte si sejururi de cazare la '.$detalii_hotel['denumire'].' din '.$detalii_hotel['zona'].', '.$detalii_hotel['tara'];
} else {
	$metas_description = $detalii_hotel['descriere_seo'];
}

include($_SERVER['DOCUMENT_ROOT'].'/includes/hoteluri/reviews_top.php');
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?> <?php if($nr_oferte<1)echo "0 oferte disponibile" ?></title>

<meta name="description" content="<?php echo $metas_description; ?>" />
<link rel="canonical" href="<?php echo $sitepath.'cazare-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'; ?>" />

<meta property="og:title" content="<?php echo $metas_title; ?>" />
<meta property="og:description" content="<?php echo $metas_description; ?>" />
<meta property="og:type" content="hotel" />
<meta property="og:url" content="<?php echo $sitepath.'hoteluri-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'; ?>" />
<meta property="og:image" content="<?php echo $sitepath.'img_mediu_hotel/'.$detalii_hotel['poza1']; ?>" />
<meta property="og:latitude" content="<?php echo $detalii_hotel['latitudine']; ?>" />
<meta property="og:longitude" content="<?php echo $detalii_hotel['longitudine']; ?>" />
<meta property="og:street-address" content="<?php echo $detalii_hotel['adresa']; if($detalii_hotel['nr']) echo ' nr. '.$detalii_hotel['nr']; ?>" />
<meta property="og:locality" content="<?php echo $detalii_hotel['localitate']; ?>" />
<meta property="og:region" content="<?php echo $detalii_hotel['zona']; ?>" />
<meta property="og:postal-code" content="<?php echo $detalii_hotel['cod_postal']; ?>" />
<meta property="og:site_name" content="<?php echo $denumire_agentie; ?>" />
<meta property="fb:admins" content="100000435587231" />
<meta property="fb:app_id" content="314711018608962" />

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>

</head>

<body onLoad="initialize(); load_submenu()">
<!--retargeting pag produs-->

<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
  
    <?php 
	//echo $_SERVER['DOCUMENT_ROOT'];
	//if($_SESSION['mail']=='razvan@ocaziituristice.ro') {
	//include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri/detalii_hotel_new2.php");}
	//else
	//{
	include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri/detalii_hotel_new2.php");	
	//	}
	
	 ?>
    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?><br>
  
    <?php 
	include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri_vizualizate_altii.php"); ?>
    
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<?php if($detalii_hotel['cod_remarketing']) echo $detalii_hotel['cod_remarketing']; ?>
</body>
</html>
