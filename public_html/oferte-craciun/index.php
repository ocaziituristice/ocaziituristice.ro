<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip = "craciun";
$linktip = "craciun";
$dentip = "Craciun";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii = get_id_tip_sejur_fii($id_tip_sejur);
$iduri = "'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri = $iduri.','.$tip_fii['id_tipuri'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Craciun <?php echo date("Y"); ?> | ocaziituristice.ro</title>
<meta name="description" content="Oferte Craciun <?php echo date("Y"); ?> in Romania si pietele de Craciun din strainatate, o atmosfera de poveste la cele mai mici preturi" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte Craciun <?php echo date("Y"); ?></h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear">
        
        <div class="Hline"></div>
        
        <div class="pad5">
          
          <img src="images/oferte-craciun.jpg" width="400" height="250" alt="Oferte Craciun" class="float-left" style="margin:0 10px 0 0;">
          <p class="text-justify text">Reprezentând inițial o sărbătoare strict religioasă, închinată nașterii lui Isus Christos, în zilele noastre, <strong>Crăciunul</strong> a adăugat încărcăturii spirituale o latură dedicată familiei, celebrării armoniei și generozității, prin oferirea de cadouri. Primite de la cei dragi sau chiar de la <strong>Moș Crăciun</strong>, care nu îi uită niciodată pe copiii cuminți, cadourile de Crăciun sunt dovezi ale iubirii sau recunoștinței noastre reciproce. Un cadou special, un Crăciun altfel, pot reprezenta pentru copii o amintire de neuitat pentru tot restul vieții. Și cum nimeni nu simte emoția Crăciunului mai mult decât cei mici, noi vă oferim cea mai bună idee pentru a face din Crăciunul copilului dumneavoastră o experiență fascinantă.</p>
          <p class="text-justify text">Petrecerea <a href="http://www.ocaziituristice.ro/oferte-craciun/romania/" title="Oferte Craciun Romania" class="link-blue">Crăciunului acasă</a>, pe plaiuri mioritice te trimite în lumea minunată a copilăriei, trăind alături de cei mici, emoţiile de neegalat odată cu venirea lui <strong>Moş Craciun</strong>, întâmpinat după dăţi străbune... miros de.. ţuică fiartă... sarmale... cozonac...</p>
          <p class="text-justify text">O vizită în Laponia, acasă la Moș Crăciun, va avea cu siguranță efectul dorit. Iar dacă sunteți amatori ai schiului, iar copilul dumneavoastră adoră pârtiile, alegeți oferta de Crăciun în Bulgaria sau Austria.</p>
          <p class="text-justify text">Opțiunea petrecerii Crăciunului în Republica Domiciană sau Sri Lanka va asigura o experiență de neuitat pentru întreaga familie.</p>
          <p class="text-justify text">În apropierea sărbătorii Crăciunului, vizualizați și ofertele speciale din <strong>Pieţe de Craciun</strong>, unde puteți găsi obiecte deosebite pentru împodobirea bradului sau a mesei de sărbători.</p>
          <p class="text-justify text">Vă urăm "Crăciun Fericit" şi să reveniţi la noi şi la anul!</p>
<br class="clear"><br>
          
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Oferte <?php echo $dentip; ?>:</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
          
          <div class="float-left" style="width:630px;">
            <h2 style="margin:0; padding:0;"><a href="/oferte-craciun/romania/" title="Oferte Craciun Romania" class="cb-item" style="background:url(images/oferte-craciun-romania.jpg) left top no-repeat; width:610px; height:180px; margin:0; font-weight:normal;"><span class="titlu">Craciun Romania</span></a></h2>
            <div class="coloana-links" style="width:190px; margin-right:30px;">
			<?php
			$sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND zone.tpl_romania = '2'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			while($row_zone=mysql_fetch_array($que_zone)) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
            <div class="coloana-links" style="width:190px; margin-right:30px;">
			<?php
			$sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND zone.tpl_romania IS NULL
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			while($row_zone=mysql_fetch_array($que_zone)) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
            <div class="coloana-links" style="width:190px;">
			<?php
            $sel_zone = "SELECT
			Count(oferte.id_oferta) AS numar,
			zone.id_zona,
			zone.denumire,
			zone.tpl_romania
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona = zone.id_zona
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate <> 'Circuit'
			AND zone.id_tara = '1'
			AND oferte.last_minute = 'nu'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND zone.tpl_romania = '1'
			AND zone.denumire = 'Statiuni Balneare'
			GROUP BY zone.id_zona
			ORDER BY numar DESC ";
			$que_zone=mysql_query($sel_zone) or die(mysql_error());
			$row_zone=mysql_fetch_array($que_zone);
			if(mysql_num_rows($que_zone)>0) {
				echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/" title="'.$dentip.' '.$row_zone['denumire'].'" class="titlu link-black">'.$row_zone['denumire'].'</a>';
				$sel_loc="SELECT
				Count(oferte.id_oferta) AS numar,
				localitati.denumire
				FROM oferte
				INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
				INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
				INNER JOIN zone ON localitati.id_zona = zone.id_zona
				LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
				WHERE oferte.valabila = 'da'
				AND hoteluri.tip_unitate <> 'Circuit'
				AND zone.id_zona = '".$row_zone['id_zona']."'
				AND oferte.last_minute = 'nu'
				AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
				GROUP BY localitati.id_localitate
				ORDER BY numar DESC ";
				$que_loc=mysql_query($sel_loc) or die(mysql_error());
				while($row_loc=mysql_fetch_array($que_loc)) {
					echo '<a href="/oferte-'.$linktip.'/romania/'.fa_link($row_zone['denumire']).'/'.fa_link($row_loc['denumire']).'/" title="'.$dentip.' '.$row_loc['denumire'].'" class="link-blue">'.$dentip.' '.$row_loc['denumire'].'</a>';
				}
			}
			?>
            </div>
          </div>
          
          <div class="float-right" style="width:300px;">
            <h2 class="cb-item" style="background:url(images/oferte-craciun-extern.jpg) left top no-repeat; width:280px; height:180px; margin:0; font-weight:normal;"><span class="titlu">Extern</span></h2>
            <div class="coloana-links" style="width:300px;">
            <p class="transport" style="background-image:url(/images/oferte/icon_small_transport_individual.png);"> Transport Individual</p>
			<?php
			$sel_tari="SELECT
			COUNT(oferte.id_oferta) AS numar,
			tari.denumire,
			tari.id_tara,
			tari.descriere_scurta,
			tari.steag
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel=hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id=localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona=zone.id_zona
			INNER JOIN tari ON zone.id_tara=tari.id_tara
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
			WHERE oferte.valabila='da' AND hoteluri.tip_unitate<>'Circuit'
			AND tari.id_tara<>'1'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND oferte.id_transport='1'
			GROUP BY tari.id_tara
			ORDER BY numar DESC ";
			$que_tari=mysql_query($sel_tari) or die(mysql_error());
			while($row_tari=mysql_fetch_array($que_tari)) {
				echo '<a href="/oferte-'.$linktip.'/'.fa_link($row_tari['denumire']).'/" title="Oferte '.$dentip.' '.$row_tari['denumire'].'" class="extern link-blue"><img src="/thumb_steag_tara/'.$row_tari['steag'].'" alt="steag '.$row_tari['denumire'].'" width="32" height="20"> '.$dentip.' '.$row_tari['denumire'].'</a>';
			}
			?>
            <p class="transport" style="background-image:url(/images/oferte/icon_small_transport_avion.png);"> Transport cu Avionul</p>
			<?php
			$sel_tari="SELECT
			COUNT(oferte.id_oferta) AS numar,
			tari.denumire,
			tari.id_tara,
			tari.descriere_scurta,
			tari.steag
			FROM oferte
			INNER JOIN hoteluri ON oferte.id_hotel=hoteluri.id_hotel
			INNER JOIN localitati ON hoteluri.locatie_id=localitati.id_localitate
			INNER JOIN zone ON localitati.id_zona=zone.id_zona
			INNER JOIN tari ON zone.id_tara=tari.id_tara
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
			WHERE oferte.valabila='da' AND hoteluri.tip_unitate<>'Circuit'
			AND tari.id_tara<>'1'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			AND oferte.id_transport='4'
			GROUP BY tari.id_tara
			ORDER BY numar DESC ";
			$que_tari=mysql_query($sel_tari) or die(mysql_error());
			while($row_tari=mysql_fetch_array($que_tari)) {
				echo '<a href="/oferte-'.$linktip.'/'.fa_link($row_tari['denumire']).'/" title="Oferte '.$dentip.' '.$row_tari['denumire'].'" class="extern link-blue"><img src="/thumb_steag_tara/'.$row_tari['steag'].'" alt="steag '.$row_tari['denumire'].'" width="32" height="20"> '.$dentip.' '.$row_tari['denumire'].'</a>';
			}
			?>
            <p class="transport" style="background-image:url(/images/oferte/icon_small_transport_autocar.png);"> Transport cu Autocarul</p>
			<?php
			$sel_tari="SELECT
			tari.denumire,
			tari.steag,
			continente.nume_continent
			FROM oferte
			INNER JOIN hoteluri on oferte.id_hotel = hoteluri.id_hotel
			INNER JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
			INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara
			INNER JOIN continente ON continente.id_continent = tari.id_continent
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta=oferta_sejur_tip.id_oferta
			WHERE oferte.valabila = 'da'
			AND hoteluri.tip_unitate = 'Circuit'
			AND traseu_circuit.tara_principala = 'da'
			AND tari.id_tara <> '1'
			AND oferta_sejur_tip.id_tip_oferta IN (".$iduri.")
			GROUP BY tari.denumire
			ORDER BY tari.denumire ASC ";
			$que_tari=mysql_query($sel_tari) or die(mysql_error());
			while($row_tari=mysql_fetch_array($que_tari)) {
				echo '<a href="/circuite-'.$linktip.'/'/*.fa_link($row_tari['nume_continent']).'/'*/.fa_link($row_tari['denumire']).'/" title="Oferte '.$dentip.' '.$row_tari['denumire'].'" class="extern link-blue"><img src="/thumb_steag_tara/'.$row_tari['steag'].'" alt="steag '.$row_tari['denumire'].'" width="32" height="20"> '.$dentip.' '.$row_tari['denumire'].'</a>';
			}
			?>
            </div>
          </div>
          
        </div>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=<?php echo $id_tip_sejur; ?>");
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>