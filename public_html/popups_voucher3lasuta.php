<div id="NEW-destinatie" align="center">
  <h1 class="green">Oferta speciala doar azi <?php echo date('d.m.Y'); ?> !</h1>
  <div style="border:1px solid #D60808; -webkit-border-radius:12px; -moz-border-radius:12px; border-radius:12px; margin:15px 10px 10px 10px;">
    <div style="float:left; padding:0 20px; background:#D60808; font-size:3em; line-height:2.2em; font-weight:bold; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius:10px;" class="white">3% reducere</div>
    <div style="float:left; padding:10px 0 10px 15px; font-size:1.5em; line-height:1.5em;">
      la orice rezervare <strong>ONLINE</strong> la ofertele cu avion din <strong>Turcia</strong><br />
      <a href="/sejur-turcia/" class="link-red"><span class="red">vezi aici</span></a> ofertele din Turcia si nu rata <strong class="red">MEGA Discountul</strong>!
    </div>
    <div class="clear"></div>
  </div>
  <br />
  <p class="grey">* reducerea se aplica sub forma de voucher doar la rezervarile efectuate online si se poate folosi la comenzi ulterioare<br />
  * <a href="/info-reducere.html" target="_blank"><span style="color:#777;"><strong>click aici</strong></span></a> pentru mai multe detalii despre cupoanele de reduceri</p>
</div>
