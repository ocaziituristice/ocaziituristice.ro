<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$den_tara = 'Bulgaria';
$id_tara = get_id_tara($den_tara);
//$id_zona = get_id_zona($den_zona, $id_tara);
$den_tara = get_den_tara($id_tara);

$link_tara = fa_link_oferta($den_tara);
//$link_zona = fa_link_oferta($den_zona);

$sel_loc = "SELECT
MIN(oferte.pret_minim_lei / oferte.nr_nopti) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
COUNT(DISTINCT hoteluri.id_hotel) AS numar_hoteluri,
localitati.id_localitate,
localitati.id_zona,
zone.denumire as denumire_zona,
localitati.denumire AS denumire_localitate,
localitati.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '".$id_tara."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_localitate
ORDER BY numar DESC, denumire_localitate ASC ";
$rez_loc = mysql_query($sel_loc) or die(mysql_error());
while($row_loc = mysql_fetch_array($rez_loc)) {
	$destinatie['den_loc'][] = $row_loc['denumire_localitate'];
	
	$destinatie['link'][] = '/sejur-bulgaria/'.fa_link($row_loc['denumire_zona']).'/'.fa_link($row_loc['denumire_localitate']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$row_loc['id_zona'].'/'.$row_loc['id_localitate'].'/';
	$destinatie['id_loc'][] = $row_loc['id_localitate'];
	$destinatie['numar_hoteluri'][] = $row_loc['numar_hoteluri'];
	$destinatie['info_scurte'][] = $row_loc['info_scurte'];
	$destinatie['pret_minim'][] = round($row_loc['pret_minim']);
} @mysql_free_result($rez_loc);
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Sejur Bulgaria, Cazare Bulgaria | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Sejur Bulgaria, cazare Bulgaria, Litoral bulgaria, oferte cu pretul real, disponibilitate in timp real a locurilor" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Sejur Bulgaria, cazare Bulgaria</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?>

        <br class="clear">
        <?php /*?><div class="Hline"></div>

        <br class="clear">
        
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
        
        <br class="clear"><?php */?>

		<?php $tipuri = get_tiputi_oferte('', '', $id_zona, $id_tara, '', '');
		if(sizeof($tipuri)>0) {
			$link_new = $link_tara.'/'.$link_zona.'/';
		?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/sejur-'.$link_new; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		foreach($tipuri as $key_t=>$value_t) {
			$i++;
			$valTip = fa_link($value_t['denumire']);
			if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire'].' Bulgaria'; ?></a></li>
		<?php } ?>
        </ul>
		<?php } ?>

        <?php if(sizeof($destinatie['den_loc'])>0) { ?>
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie['den_loc'] as $key1 => $value1) { ?>
          <div class="destItem NEW-round4px clearfix" >
            <div class="topBox" onClick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
              <img src="/sejur-bulgaria/images/sejur-bulgaria-<?php echo fa_link($destinatie['den_loc'][$key1]); ?>.jpg" alt="<?php echo $destinatie['den_loc'][$key1]; ?>" >
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie['den_loc'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?><div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><?php } ?>
            <div class="bottomBox clearfix">
              <div class="price">
              <span class="red">
               <span class="bold bigger-15em"><?php echo $destinatie['numar_hoteluri'][$key1]?></span></span> hoteluri pentru 
            
               <div  class="clearfix"align="right"><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Cazare <?php echo $destinatie['den_loc'][$key1]; ?>" class="link-blue block">cazare in <strong><?php echo $destinatie['den_loc'][$key1] ?> </strong></a></div></div>
             <?php /*?> <div class="details"><a href="<?php echo $destinatie['link_noindex'][$key1]; ?>" rel="nofollow noindex"><img src="/images/but_nou_detalii.png" alt="detalii" width="90"></a></div><?php */?>
            </div>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        
        <br>
		
        <div class="pad10">
        <?php /*?>  <img src="/sejur-romania/statiuni-balneare/images/icon-statiuni-balneare.jpg" alt="Cazare Statiuni Balneare" class="float-left" style="margin:3px 10px 0 0;"><?php */?>
          <h2><strong>Sejur Bulgaria</strong>- vacanta accesibila pentru toate gusturile </h2>
          <p class="newText text-justify">Bulgaria este cunoscuta drept tara cu cele mai accesibile destinatii de vacanta, fiind adresata tuturor categoriilor de turisti care opteaza pentru sejur Bulgaria, atat iarna cat si vara.</p>
          
          <br class="clear">
          
          <p class="newText text-justify">Pentru cei in cautare de aventura in timpul iernii, Bulgaria ofera partii cu instalatie nocturna si piste multiple pentru schi, momente de neuitat pe patinoare bine intretinute, sau vizite in cel mai mare parc de distractii pentru snowboard-uri din Balcani.</p>
          
          <br class="clear">

<p class="newText text-justify">In timpul verii, statiunile montane imbie cu numarul mare de lacuri, cascade, trasee turistice pentru expeditii si plimbari, pentru ciclism si catarat pe stanci, excursii organizate pe jos sau calare pe versantii muntilor.</p>
          
          <br class="clear">

<p class="newText text-justify">Bulgaria este faimoasa, in primul rand, pentru superbele plaje pline de viata, litoralul bulgaresc al Marii Negre fiind plin de statiuni turistice dar si de statiuni balneare cu ape minerale si namoluri terapeutice.</p>
          
          <br class="clear">

<p class="newText text-justify">Servicii ireprosabile la cazare Bulgaria, sistemul All inclusive cu multiple facilitati de agrement, plajele cu sezlong, umbrele, piscina, sau plajele asigurate, spectacole in fiecare seara, cluburi de noapte, aqua park, fitness, atrag turistii din tot mai multe tari.</p>
          
          <br class="clear">

<p class="newText text-justify">Cei ce iubesc curatenia si serviciile ireprosabile pe plaja in timpul unui sejur Bulgaria pot profita de recunoasterea pe plan international, distinctia Blue Flag acordata pe criterii ecologice ce confirma calitatea apei si plajelor bulgaresti.</p>
          
          <br class="clear">

<p class="newText text-justify">Litoralul, cu cei 370 km de tarm, ii rasfata pe turisti cu plaje deschise, nisipuri fine aurii, dune care s-au format in mod natural in unele locuri, lagune si golfuri adanci adapostite impotriva vanturilor nordice - un loc ideal pentru toate tipurile de sporturi nautice.</p>
          
          <br class="clear">

<p class="newText text-justify">Amatorii de senzatii tari se pot bucura de multiple posibilitati de practicare a sporturilor acvatice si nautice intr-un sejur Bulgaria: surfing, banane pe apa, waterjets, windsurf, parapante, scooter, parasute de apa, schi pe apa, yachting, dar si de clipele de neuitat petrecute pe terenuri de fotbal, baschet, volei, tenis si golf.</p>
          
          <br class="clear">

<p class="newText text-justify">Cele mai multe hoteluri de pe litoralul bulgaresc sunt construite chiar pe plaja sau foarte aproape de ea, ofertele All inclusive si ultra All inclusive de cazare Bulgaria fiind principala atractie a turistilor dornici de conditii excelente pentru odihna si relaxare.</p>
          
          <br class="clear">

<p class="newText text-justify">Sejurul la malul marii este completat de participarea la seri folclorice si de dans, programe distractive pentru copii, cu cantece, dansuri interactive si clovni, restaurante de talie internationala si taverne locale pentru cele mai fine gusturi unde turistii pot savura mancaruri pescaresti si traditionale bulgaresti, baruri, cafenele, pub-uri si cluburi de noapte.</p>
          
          <br class="clear">

<p class="newText text-justify">Influentele occidentale si-au lasat o amprenta vizibila asupra organizarii statiunilor, activitatilor turistice si calitatii serviciilor, conferind turistilor o stare de spirit excelenta.</p>
                 
    
        </div>

        <br class="clear">

		<?php /*?><?php
        $oferte_rec=new AFISARE_SEJUR_NORMAL();
        $oferte_rec->setAfisare(1, 4);
        $oferte_rec->setRecomandata('da');
		$oferte_rec->setRandom(1);
        $oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_early_booking.tpl");
		$oferte_rec->setTari($link_tara);
		$oferte_rec->setZone($link_zona);
		$nr_hoteluri=$oferte_rec->numar_oferte();
        if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
        ?>
        <div class="ofRec" style="padding:8px 5px 6px 5px;">
          <h3 class="blue">Oferte recomandate pentru Statiuni Balneare</h3>
		  <?php $oferte_rec->afiseaza();?>
          <br class="clear">
        </div>
        <?php } ?>

        <br class="clear"><br><?php */?>
        
		<?php /*?><?php $sel_loc = "SELECT
		COUNT(oferte.id_oferta) AS numar,
		localitati.denumire AS denumire_localitate
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		WHERE oferte.valabila = 'da'
		AND hoteluri.tip_unitate <> 'Circuit'
		AND localitati.id_zona = '".$id_zona."'
		AND oferte.last_minute = 'nu'
		GROUP BY denumire_localitate
		ORDER BY numar DESC, denumire_localitate ASC ";
		$rez_loc = mysql_query($sel_loc) or die(mysql_error());
		while ($row_loc = mysql_fetch_array($rez_loc)) {
			$den_loc = $row_loc['denumire_localitate'];
			$link = '/sejur-romania/statiuni-balneare/'.fa_link($den_loc).'/';
			//if(strlen($row_loc['denumire_localitate'])>13) {
			//	$den_lunga_loc = 'style="font-size:44px; letter-spacing:-1.5pt;"';
			//} else {
			//	$den_lunga_loc = 'style="font-size:44px;"';
			//}
		?>
        <a href="<?php echo $link; ?>" title="Cazare <?php echo $den_loc; ?>" class="cb-item" style="background:url(images/old_statiuni-balneare-<?php echo fa_link($den_loc); ?>.jpg) left top no-repeat;"><span class="titlu" <?php echo $den_lunga_loc; ?>><?php echo $den_loc; ?></span></a>
		<?php } @mysql_free_result($rez_loc); ?>
        
        <br class="clear"><br><?php */?>
        
       <?php /*?> <?php if(sizeof($destinatie['den_loc'])>0) { ?>
        <h3 class="mar10">Sejur Bulgaria - cazări disponibile in statiunile din Bulgaria</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie['den_loc'] as $key2 => $value2) { ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie['link'][$key2]; ?>" title="Cazare <?php echo $destinatie['den_loc'][$key2]; ?>" class="link-blue block">Cazare <?php echo $destinatie['den_loc'][$key2]; ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?><?php */?>
        
        <br class="clear"><br><br>
        
        <div class="pad10"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?></div>
        
        <br class="clear"><br>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>