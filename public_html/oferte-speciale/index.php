<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$sel_tip_oferta = "SELECT
tip_oferta.id_tip_oferta,
tip_oferta.descriere_scurta,
tip_oferta.reducere
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
INNER JOIN tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND oferte.last_minute = 'nu'
AND tip_oferta.tip = 'oferte_speciale'
AND CURDATE() BETWEEN tip_oferta.data_inceput_eveniment AND tip_oferta.data_sfarsit_eveniment
GROUP BY tip_oferta.id_tip_oferta
ORDER BY tip_oferta.ordine DESC ";
$rez_tip_oferta = mysql_query($sel_tip_oferta) or die(mysql_error());
while($row_tip_oferta = mysql_fetch_array($rez_tip_oferta)) {
	$id_tip_oferta = $row_tip_oferta['id_tip_oferta'];
	$tip_oferta[$id_tip_oferta] = $row_tip_oferta['descriere_scurta'];
	$tip_oferta = array_unique($tip_oferta);
	
	$sel_dest = "SELECT
	MIN(oferte.pret_minim / oferte.nr_nopti * 7) AS pret_minim,
	oferte.exprimare_pret,
	COUNT(oferte.id_oferta) AS numar,
	zone.id_zona,
	zone.denumire AS denumire_zona,
	zone.info_scurte
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND oferte.last_minute = 'nu'
	AND oferta_sejur_tip.id_tip_oferta = '".$id_tip_oferta."'
	GROUP BY denumire_zona
	ORDER BY numar DESC, denumire_zona ASC ";
	$rez_dest = mysql_query($sel_dest) or die(mysql_error());
	while($row_dest = mysql_fetch_array($rez_dest)) {
		$destinatie['den_zona'][$id_tip_oferta][] = $row_dest['denumire_zona'];
		$destinatie['link'][$row_dest['denumire_zona']] = '/oferte-speciale-'.fa_link($row_dest['denumire_zona']).'/';
		$destinatie['link_noindex'][$id_tip_oferta][] = '/oferte-speciale-'.fa_link($row_dest['denumire_zona']).'/';
		$destinatie['id_zona'][$id_tip_oferta][] = $row_dest['id_zona'];
		$destinatie['info_scurte'][$id_tip_oferta][] = $row_dest['info_scurte'];
		$destinatie['pret_minim'][$id_tip_oferta][] = round($row_dest['pret_minim']);
		$destinatie['exprimare_pret'][$id_tip_oferta][] = round($row_dest['exprimare_pret']);
		$destinatie['pret_old'][$id_tip_oferta][] = round($row_dest['pret_minim']) + $row_dest['reducere'];
	} @mysql_free_result($rez_dest);
}

/*if($_SESSION['mail']=='daniel@ocaziituristice.ro') {
	echo $sel_dest;
	echo '<pre>';print_r($tip_oferta);echo '</pre>';
	echo '<pre>';print_r($destinatie);echo '</pre>';
}*/
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Speciale, discounturi, reduceri | Ocaziituristice.ro</title>
<meta name="description" content="Oferte Speciale la preturi reduse, profitati acum de discounturile la vacante oferite de Ocaziituristice.ro" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <div class="float-left">
          <h1 class="blue">OFERTE SPECIALE</h1>
          <div class="pad5 newText bigger-13em" style="margin-top:-10px;">Cele mai mici preţuri. Locuri limitate.</div>
        </div>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>

        <br class="clear">
        <div class="Hline"></div>
        
        <br class="clear">
        
        <?php if(sizeof($tip_oferta)>0) {
			foreach($tip_oferta as $key => $value) { ?>
        <div class="ofsp-titlu green newText"><?php echo $value; ?> pentru destinaţiile</div>
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie['den_zona'][$key] as $key1 => $value1) { ?>
          <div class="destItem NEW-round4px clearfix" onClick="javascript:location.href='<?php echo $destinatie['link_noindex'][$key][$key1]; ?>'">
            <div class="topBox lower">
              <img src="/images/zone/<?php echo fa_link($destinatie['den_zona'][$key][$key1]); ?>.jpg" alt="<?php echo $destinatie['den_zona'][$key][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie['den_zona'][$key][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key][$key1]) { ?><div class="middleBox"><?php echo $destinatie['info_scurte'][$key][$key1]; ?></div><?php } ?>
            <div class="bottomBox clearfix">
             <?php /*?> <div class="price">de la <?php if($destinatie['pret_old'][$key][$key1]>$destinatie['pret_minim'][$key][$key1]) echo '<span class="strikethrough black bold bigger-12em">&nbsp;'.$destinatie['pret_old'][$key][$key1].' '.moneda($destinatie['exprimare_pret'][$key][$key1]).'&nbsp;</span>'; ?><br><span class="red"><span class="pret"><?php echo $destinatie['pret_minim'][$key][$key1]; ?></span> <span class="bold bigger-15em"><?php echo moneda($destinatie['exprimare_pret'][$key][$key1]); ?></span></span> /sejur/persoana</div><?php */?>
              <div class="details"><img src="/images/but_nou_detalii.png" alt="detalii" width="90"></div>
            </div>
          </div>
          <?php } ?>
        </div>
        <br class="clear"><br>
        <?php }
		} ?>
                
        <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <h3 class="mar10">Oferte Speciale pentru destinaţiile</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie['link'] as $key2 => $value2) {
			  $part_anchor = 'Oferte Speciale ';
		  ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $value2; ?>" title="<?php echo $part_anchor.$key2; ?>" class="link-blue block"><?php echo $part_anchor.$key2; ?></a></li>
          <?php } ?>
        </ul>
        <?php }?>
        
      </div>

    </div>
    <br><br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>