<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');

/*** check login admin ***/
$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

//$encoded = base64_encode(gzcompress(serialize($original)));
//$original = unserialize(gzuncompress(base64_decode($_POST['encoded'])));

/*if(isset($_GET['data'])) {
	$codedData = $_GET['data'];
	
	if(!isset($_COOKIE['cos_cumparaturi'])) {
		setcookie('cos_cumparaturi', $codedData, time()+2592000, '/', 'ocaziituristice.ro');
	} else {
		$cos_array = explode('+;++;+', $_COOKIE['cos_cumparaturi']);
		if(!in_array($codedData, $cos_array)) {
			$cookie_content = $_COOKIE['cos_cumparaturi'].'+;++;+'.$codedData;
			setcookie('cos_cumparaturi', $cookie_content, time()+2592000, '/', 'ocaziituristice.ro');
		}
	}
	
	$det_cos = explode('_', base64_decode($codedData));
	$id_oferta_cos = $det_cos[0]; // id oferta
	$cos_id_pret = $det_cos[1]; // id pret
	$cos_plecare = $det_cos[2]; // tip preturi (plecari sau perioade)
	$cos_data = $det_cos[3]; // data plecarii
	$cos_zile = $det_cos[4]; // nr nopti
	$cos_pret = $det_cos[5]; // pret
	$cos_id_cam = $det_cos[6]; // id_cam
	$cos_nr_adulti = $det_cos[7]; // nr adulti
	$cos_nr_copii = $det_cos[8]; // nr copii
	$cos_copil1 = $det_cos[9]; // copil1
	$cos_copil2 = $det_cos[10]; // copil2
	$cos_copil3 = $det_cos[11]; // copil3
	$cos_masa = $det_cos[12]; // tip masa
	$cos_den_camera = $det_cos[13]; // denumire camera
	
	$ins_cos_cumparaturi = "INSERT INTO cos_cumparaturi (id_cos, cookie, id_client, id_oferta, id_pret, plecari, data_plecare, nr_nopti, id_cam, den_cam, nr_adulti, nr_copii, copil1, copil2, copil3, tip_masa, pret, data_adaugare, data_stergere, tip_stergere, ip, browser_info) VALUES ('', '$codedData', NULL, '$id_oferta_cos', '$cos_id_pret', '$cos_plecare', '$cos_data', '$cos_zile', '$cos_id_cam', '$cos_den_camera', '$cos_nr_adulti', '$cos_nr_copii', '$cos_copil1', '$cos_copil2', '$cos_copil3', '$cos_masa', '$cos_pret', now(), NULL, NULL, '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."') ";
	$rez_cos_cumparaturi = mysql_query($ins_cos_cumparaturi) or die (mysql_error());
	
	redirect_php('/ofertele-mele/');
} else {*/
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Ofertele mele - Wishlist | Ocaziituristice.ro</title>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
<meta name="robots" content="noindex, nofollow">
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie">
		<h1 class="blue float-left">Ofertele mele - Wishlist</h1>
        
        <div class="pad20 float-right">
		<?php if(isset($_SESSION['page_back'])) echo '<a href="'.$_SESSION['page_back'].'" class="button-blue">&lt;&nbsp; Inapoi la oferte</a>';
		else echo '<a href="'.$_SESSION['page_back_offer'].'" class="button-blue">&lt;&nbsp; Inapoi la detaliile ofertei</a>'; ?>
        </div>
        
        <br class="clear">
        
        <div class="pad20">
        
<?php if(isset($_COOKIE['cos_cumparaturi'])) { ?>
        <div id="accordion">
          <h3 class="bigger-15em black">BINE DE ȘTIUT ! <span class="smaller-08em">(click aici)</span></h3>
          <div class="bigger-12em" style="line-height:1.5em;">
            <ul>
              <li>Sejururile adăugate în această listă (wishlist) <strong>nu sunt rezervate</strong></li>
              <li>Apasă butonul <strong>"Rezervă"</strong> din dreptul sejurului dorit pentru a efectua rezervarea acesteia</li>
              <li>Aici poți adăuga <strong>un număr nelimitat de sejururi</strong> de la un număr nelimitat de hoteluri</li>
              <li>Perioada de valabilitate a acestei liste este de 30 zile. <strong>Nu rata ofertele noastre speciale!</strong></li>
            </ul>
          </div>
        </div>
        <br>
<?php
$det = new DETALII_SEJUR();
	$arrCos = explode('+;++;+', $_COOKIE['cos_cumparaturi']);
	foreach($arrCos as $k_arrCos => $v_arrCos) {
		$det_cos[$k_arrCos] = explode('##', base64_decode($v_arrCos));
		$id_oferta_cos[$k_arrCos] = $det_cos[$k_arrCos][0]; // id oferta
		$cos_id_pret[$k_arrCos] = $det_cos[$k_arrCos][1]; // id pret
		$cos_plecare[$k_arrCos] = $det_cos[$k_arrCos][2]; // tip preturi (plecari sau perioade)
		$cos_data[$k_arrCos] = $det_cos[$k_arrCos][3]; // data plecarii
		$cos_zile[$k_arrCos] = $det_cos[$k_arrCos][4]; // nr nopti
		$cos_pret[$k_arrCos] = $det_cos[$k_arrCos][5]; // pret
		$cos_id_cam[$k_arrCos] = $det_cos[$k_arrCos][6]; // id_cam
		$cos_nr_adulti[$k_arrCos] = $det_cos[$k_arrCos][7]; // nr_adulti
		$cos_nr_copii[$k_arrCos] = $det_cos[$k_arrCos][8]; // nr_copii
		$cos_copil[1][$k_arrCos] = $det_cos[$k_arrCos][9]; // copil1
		$cos_copil[2][$k_arrCos] = $det_cos[$k_arrCos][10]; // copil2
		$cos_copil[3][$k_arrCos] = $det_cos[$k_arrCos][11]; // copil3
		$cos_masa[$k_arrCos] = $det_cos[$k_arrCos][12]; // tip masa
		$cos_den_camera[$k_arrCos] = $det_cos[$k_arrCos][13]; // denumire camera
		//echo '<li>'.$id_oferta_cos[$k_arrCos].' - '.$cos_id_pret[$k_arrCos].' - '.$cos_plecare[$k_arrCos].' - '.$cos_data[$k_arrCos].' - '.$cos_zile[$k_arrCos].' - '.$cos_pret[$k_arrCos].'</li>';
		
		$link_sterge[$k_arrCos] = '/ofertele-mele/?delete_off='.$k_arrCos;
		
		$sel_off = "SELECT
		oferte.denumire_scurta,
		oferte.masa,
		oferte.moneda,
		transport.denumire AS den_transport,
		hoteluri.nume AS den_hotel,
		hoteluri.poza1,
		hoteluri.stele,
		hoteluri.tip_unitate,
		localitati.denumire AS den_localitate,
		zone.denumire AS den_zona,
		tari.denumire AS den_tara,
		continente.nume_continent AS den_continent
		FROM oferte
		INNER JOIN hoteluri ON hoteluri.id_hotel = oferte.id_hotel
		INNER JOIN localitati ON localitati.id_localitate = hoteluri.locatie_id
		INNER JOIN zone ON zone.id_zona = localitati.id_zona
		INNER JOIN tari ON tari.id_tara = zone.id_tara
		INNER JOIN continente ON continente.id_continent = tari.id_continent
		INNER JOIN transport ON transport.id_trans = oferte.id_transport
		WHERE oferte.id_oferta = '".$id_oferta_cos[$k_arrCos]."'
		";
		$que_off = mysql_query($sel_off) or die(mysql_error());
		$row_off = mysql_fetch_array($que_off);




if($row_off['den_transport']=='Avion') {

$detalii=$det->get_avion_sejur($id_oferta_cos[$k_arrCos]);

$found=find_items($detalii,'tip','dus',$found=array());
//print "<pre>";print_r($found);print "</pre>";	
	$plecare='&plecare-avion='.strtolower($found['0']['denumire_loc_plecare']);
	
	}
		
		$sel_tip_camera="SELECT
		pret_pivot_adaugat.*,
		tip_camera.denumire AS denumire_camera
		FROM pret_pivot_adaugat
		LEFT JOIN tip_camera ON tip_camera.id_camera=pret_pivot_adaugat.tip_camera
		WHERE pret_pivot_adaugat.id_pret='".$cos_id_pret[$k_arrCos]."' ";
		$que_tip_camera=mysql_query($sel_tip_camera) or die(mysql_error());
		$row_tip_camera=mysql_fetch_array($que_tip_camera);
		
		$denumire_camera = $cos_den_camera[$k_arrCos];
		
		if($row_off['tip_unitate']=='Circuit') {
			$link_oferta[$k_arrCos] = make_link_circuit($row_off['den_hotel'], $id_oferta_cos[$k_arrCos]);
			$link_rezervare[$k_arrCos] = make_link_oferta($row_off['den_localitate'], $row_off['den_hotel'], NULL, NULL).'rezervare-'.$id_oferta_cos[$k_arrCos].'_'.base64_encode($cos_id_pret[$k_arrCos].'_'.$cos_plecare[$k_arrCos].'_'.$cos_data[$k_arrCos].'_'.$cos_zile[$k_arrCos].'_'.$cos_pret[$k_arrCos]);
		} else {
			$link_oferta[$k_arrCos] = make_link_oferta($row_off['den_localitate'], $row_off['den_hotel'], NULL, $id_oferta_cos[$k_arrCos]).'?plecdata='.date('d.m.Y', strtotime($cos_data[$k_arrCos])).$plecare.'&transport='.$row_off['den_transport'].'&pleczile='.$cos_zile[$k_arrCos].'&adulti='.$cos_nr_adulti[$k_arrCos].'&copii='.$cos_nr_copii[$k_arrCos].'&age[0]='.$cos_copil[1][$k_arrCos].'&age[1]='.$cos_copil[2][$k_arrCos].'&age[2]='.$cos_copil[3][$k_arrCos];
			$link_rezervare[$k_arrCos] = make_link_oferta($row_off['den_localitate'], $row_off['den_hotel'], NULL, NULL).'rezervare-'.$id_oferta_cos[$k_arrCos].'_'.base64_encode($cos_id_pret[$k_arrCos].'_'.$cos_plecare[$k_arrCos].'_'.$cos_data[$k_arrCos].'_'.$cos_zile[$k_arrCos].'_'.$cos_pret[$k_arrCos].'_'.$cos_id_cam[$k_arrCos].'_'.$cos_nr_adulti[$k_arrCos].'_'.$cos_nr_copii[$k_arrCos].'_'.$cos_copil[1][$k_arrCos].'_'.$cos_copil[2][$k_arrCos].'_'.$cos_copil[3][$k_arrCos].'_'.$cos_masa[$k_arrCos].'_'.$denumire_camera);
		}
		?>
        <div class="oferta-cos NEW-round8px clearfix">
          <a href="<?php echo $link_oferta[$k_arrCos]; ?>"><img src="/thumb_hotel/<?php echo $row_off['poza1']; ?>" alt="<?php echo $row_off['den_hotel']; ?>" class="poza float-left"></a>
          <h2 class="blue"><a href="<?php echo $link_oferta[$k_arrCos]; ?>"><?php echo $row_off['denumire_scurta'].' - '.$cos_zile[$k_arrCos].' nopți'; ?></a></h2>
          <h3 class="black"><?php echo $row_off['den_hotel']; ?> <img src="/images/spacer.gif" class="stele-mici-<?php echo $row_off['stele']; ?>" alt="<?php echo $row_off['stele']; ?> stele"></h3>
          <div class="float-left" style="width:200px; overflow:hidden;">
            <p class="pad5-0"><?php echo $row_off['den_localitate'].' / '.$row_off['den_zona'].' / '.$row_off['den_tara']; ?></p>
            <p class="pad5-0">Masă: <strong><?php echo $cos_masa[$k_arrCos]; ?></strong></p>
            <p class="pad5-0">Transport: <strong><?php echo $row_off['den_transport']; ?></strong></p>
          </div>
          <div class="float-left bigger-12em" style="width:230px; margin:0 0 0 10px; line-height:14px !important;">
            <p class="pad5-0">Dată plecare: <strong><?php echo denLuniRo(date('d F Y', strtotime($cos_data[$k_arrCos]))); ?></strong></p>
            <p class="pad5-0">Tip cameră: <strong><?php echo $denumire_camera; ?></strong></p>
            <p class="pad5-0">Grad ocupare: <strong>
			<?php echo $cos_nr_adulti[$k_arrCos].' adult';
			if($cos_nr_adulti[$k_arrCos]>1) echo 'i';
			if($cos_nr_copii[$k_arrCos]>0) {
				echo ' + '.$cos_nr_copii[$k_arrCos].' copi';
				if($cos_nr_copii[$k_arrCos]==1) echo 'l'; else echo 'i';
			}
			?>
            </strong></p>
            <?php if($cos_nr_copii[$k_arrCos]>0) {
				$children = '<p class="smaller-08em"> - ';
				for($i=1; $i<=$cos_nr_copii[$k_arrCos]; $i++) {
					$children .= '<strong>copil '.$i.':</strong> '.$cos_copil[$i][$k_arrCos].' ani, ';
				}
				$children = substr($children, 0, -2).'</p>';
				echo $children;
			} ?>
          </div>
          <div class="price text-right black">Tarif / <?php echo $denumire_camera; ?><br>
		    <?php echo $cos_zile[$k_arrCos].' nopți'; ?>
			<span class="red"><?php echo new_price($cos_pret[$k_arrCos]).' '.moneda($row_off['moneda']); ?></span>
          </div>
          <div class="book-now text-center">
            <?php /*?><a href="<?php echo $link_rezervare[$k_arrCos]; ?>"><img src="/images/button_new_rezerva6.png" alt="Rezerva"></a><?php */?>
            <a href="<?php echo $link_oferta[$k_arrCos]; ?>"><img src="/images/but_nou_vezi_detalii1.png" alt="Vezi detalii"></a>
            <a href="<?php echo $link_sterge[$k_arrCos]; ?>" class="delete">ȘTERGE DIN COS </a>
          </div>
        </div>
		<?php @mysql_free_result($que_off);
	}
	if(isset($_GET['delete_off'])) {
		$upd_cos_cump = "UPDATE cos_cumparaturi SET data_stergere = NOW(), tip_stergere = 'individual' WHERE cookie = '".$arrCos[$_GET['delete_off']]."' AND ip = '".$_SERVER['REMOTE_ADDR']."' AND browser_info = '".$_SERVER['HTTP_USER_AGENT']."' ";
		$rez_cos_cump = mysql_query($upd_cos_cump) or die (mysql_error());

		unset($arrCos[$_GET['delete_off']]);
		
		$cos_cumparaturi = '';
		foreach($arrCos as $key_arr => $value_arr){
			$cos_cumparaturi .= $value_arr.'+;++;+';
		}
		$cos_cumparaturi = substr($cos_cumparaturi, 0, -6);
		setcookie('cos_cumparaturi', $cos_cumparaturi, time()+2592000, '/', 'ocaziituristice.ro');
		redirect_php('/ofertele-mele/');
	}
} else {
?>
		  <h3 style="text-indent:15px;">Lista cu ofertele tale (wishlist) este goală, dar nu trebuie să fie așa.</h3>
          <p class="text bigger-11em">Avem foarte multe oferte și suntem convinși că, cel puțin una, este pe placul tău. Insă, dacă nu găsești nici o ofertă care să iți satisfacă toate cerințele, ne poți <a href="/contact.html" class="link-blue">contacta</a> și te asigurăm că echipa noastră va face tot posibilul pentru ca tu să devii clientul nostru mulțumit.</p>
          <img src="/images/img-adauga-in-cos.jpg" class="float-right" alt="adauga in cos">
          <br><br>
          <p class="text bigger-11em float-left" style="width:470px;">Pentru a adăuga oferte în <strong>Whislist-ul</strong> de pe Ocaziituristice.ro, începe prin a căuta ofertele care te interesează (<a href="/" class="link-blue">click aici pentru căutare</a>), apoi caută butonul <strong>"Adaugă la comparație"</strong> din detaliile ofertei (vezi imaginea alăturată).</p>
          <p class="text italic float-left" style="width:470px;">Ofertele care nu conțin acest buton necesită confirmarea unui agent de turism asupra prețului final.</p>
          <br class="clear">
          <ul class="bigger-11em bold mar10" style="margin-bottom:0; padding-bottom:0;">
            <li style="line-height:1.7em;"><a href="/" class="link-blue">Răsfoiește întreg "catalogul" nostru online</a></li>
            <li style="line-height:1.7em;"><a href="/sejur-romania/" class="link-blue">Turism intern. Caută oferte doar în Romania</a></li>
            <li style="line-height:1.7em;"><a href="/oferte-turism-extern/" class="link-blue">Turism extern. Caută oferte în țările vecine sau pe meleaguri îndepărtate</a></li>
          </ul>
          <br>
<?php } ?>
		</div>

	  </div>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
    <br><br>
	</div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script>
$(function() {
	$("#accordion").accordion({
		collapsible: true,
		active: false
	});
});
</script>
</body>
</html>
<?php /*}*/ ?>
