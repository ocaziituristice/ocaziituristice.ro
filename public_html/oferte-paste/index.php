<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip = "paste";
$linktip = "paste";
$dentip = "Paste";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii = get_id_tip_sejur_fii($id_tip_sejur);
$iduri = "'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri = $iduri.','.$tip_fii['id_tipuri'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Paste 2015, cazare Paste 2015</title>
<meta name="description" content="Oferte Paste 2015, pachete turistice pentru Paste in Romania si alte tari, cazare de Paste 2015" />
<meta name="keywords" content="paste 2015, oferte paste 2015, cazare paste 2015" />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte Paste 2015</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="float-left text-justify" style="width:490px;">
          <div class="article pad10 newText">
            <p>Paştele reprezintă una dintre cele mai importante sărbători anuale creştine, care comemorează evenimentul fundamental al creştinismului, Învierea lui Iisus Hristos. Este o sărbătoare creştină şi iudaică, dar fiecare are semnificaţia ei.</p>
            <p>Ouăle roşii tradiţionale româneşti sunt simbolul sărbătorii Învierii. Se spune că Maica Domnului, care venise să-şi plângă fiul răstignit, a aşezat coşul cu ouă lângă cruce şi acestea s-au înroşit de la sângele care picură din rănile lui Iisus. Domnul, văzând că ouăle s-au înroşit, a spus celor de faţă: "De acum înainte să faceţi şi voi ouă roşii şi împestriţate întru aducere aminte de răstignirea mea, după cum am făcut şi eu astăzi!". Nu rataţi un <a href="http://www.ocaziituristice.ro/circuite/asia/israel/" class="link-blue">pelerinaj la Ierusalim</a> sau în Grecia cu ocazia sfintelor sărbători de Paşti.</p>
          </div>
          <?php /*?><a href="/oferte-paste/bulgaria/" title="Paste Bulgaria" class="cb-item" style="background:url(images/paste-bulgaria.jpg) left top no-repeat;"><span class="titlu">Bulgaria <img src="/thumb_steag_tara/1090bulgaria1steag_tara.jpg" alt="steag Bulgaria" /></span></a><?php */?>
          <?php /*?><a href="/oferte-paste/romania/" title="Paste Romania" class="cb-item" style="background:url(images/paste-romania.jpg) left top no-repeat;"><span class="titlu">Romania <img src="/thumb_steag_tara/4593romania1steag_tara.jpg" alt="steag Romania" /></span></a><?php */?>
		  <?php /*?><a href="/oferte-paste/grecia/" title="Paste Grecia" class="cb-item" style="background:url(images/paste-grecia.jpg) left top no-repeat;"><span class="titlu">Grecia <img src="/thumb_steag_tara/5304grecia1steag_tara.jpg" alt="steag Grecia" /></span></a><?php */?>
		  <?php /*?> <a href="/oferte-paste/spania/" title="Paste Spania" class="cb-item" style="background:url(images/paste-spania.jpg) left top no-repeat;"><span class="titlu">Spania <img src="/thumb_steag_tara/3490spania1steag_tara.jpg" alt="steag Spania" /></span></a><?php */?>
          <?php /*?><a href="/circuite-paste/asia/israel/" title="Paste Israel" class="cb-item" style="background:url(images/paste-israel.jpg) left top no-repeat;"><span class="titlu">Israel <img src="/thumb_steag_tara/1738israel1steag_tara.jpg" alt="steag Israel" /></span></a><?php */?>
		  <?php /*?><a href="/oferte-paste/turcia/" title="Paste Turcia" class="cb-item" style="background:url(images/paste-turcia.jpg) left top no-repeat;"><span class="titlu">Turcia <img src="/thumb_steag_tara/3378turcia1steag_tara.jpg" alt="steag Turcia" /></span></a><?php */?>
        </div>
        
        <div style="float:right; width:484px;">
          <br><img src="images/img-paste.jpg" alt="Oferte Paste" /><br><br><br><br>

		<?php
        $oferte_rec=new AFISARE_SEJUR_NORMAL();
        $oferte_rec->setAfisare(1, 8);
        $oferte_rec->setRecomandata('da');
        $oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_paste.tpl");
        $oferte_rec->setTipOferta('paste');
		$oferte_rec->setRandom(1);
        $nr_hoteluri=$oferte_rec->numar_oferte();
        if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
        ?>
          <div class="ofRec" style="background:none; margin:0; padding:0; border:none;">
            <h2>Oferte recomandate pentru Paşte</h2>
            <?php $oferte_rec->afiseaza();?>
            <br class="clear" />
          </div>
        <?php } ?>
        </div>
        
        <br class="clear" /><br />
        
      </div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?tip_oferta=<?php echo $id_tip_sejur; ?>");
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>