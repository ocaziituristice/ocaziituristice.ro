<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
if(!$_REQUEST['from']) $from=1; else $from=$_REQUEST['from'];
if(!$nr_pe_pagina=$_COOKIE['nr_pe_pagina']) $nr_pe_pagina=10;
$err=0;
//$continent=desfa_link($_REQUEST['continent']);
//$id_continent=get_id_continent($continent);
$id_tara=$_REQUEST['tara'];
$tara=get_den_tara($id_tara);

$sel_plecari = "SELECT date_format(data_pret_oferta.data_start, '%m-%Y') AS new_date, tari.steag
FROM data_pret_oferta
INNER JOIN oferte ON data_pret_oferta.id_oferta = oferte.id_oferta
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara = '".$id_tara."' AND traseu_circuit.tara_principala = 'da')
INNER JOIN tari ON tari.id_tara = traseu_circuit.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
AND data_pret_oferta.data_start > NOW()
GROUP BY new_date
ORDER BY data_pret_oferta.data_start ";
$que_plecari = mysql_query($sel_plecari) or die(mysql_error());
while($row_plecari = mysql_fetch_array($que_plecari)) {
	$luni_plecari[] = $row_plecari['new_date'];
	$steag_tara=$row_plecari['steag'];
}
@mysql_fetch_array($que_plecari);
$luna=array('01'=>'Ianuarie', '02'=>'Februarie', '03'=>'Martie', '04'=>'Aprilie', '05'=>'Mai', '06'=>'Iunie', '07'=>'Iulie', '08'=>'August', '09'=>'Septembrie', '10'=>'Octombrie', '11'=>'Noiembrie', '12'=>'Decembrie');

$selSEO = "SELECT * FROM seo_circuite WHERE id_tip = '0' AND id_tara = '".$id_tara."' ";
$queSEO = mysql_query($selSEO) or die(mysql_error());
$rowSEO = mysql_fetch_array($queSEO);
@mysql_free_result($queSEO);

$den_tara=ucwords($tara);
$link_tara=fa_link($tara);

if(!$id_tara) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}

if($_REQUEST['tip']) {
	$tip=desfa_link($_REQUEST['tip']);
	$id_tip=get_id_tip_sejur($tip);
	if(!$id_tip) {
		header("HTTP/1.0 301 Moved Permanently");
		header("Location: ".$sitepath.'circuite'.'/'.$link_tara.'/');
		exit();
	}
	$indice='circuite-'.$_REQUEST['tip'].'/';
} else $indice='circuite/';

//$den_continent=ucwords($continent);
//$link_continent=$continent;
$titlu_pag='Circuite '.$den_tara;
$link_p=$sitepath.$indice./*$link_continent.'/'.*/$link_tara.'/';
$link=$link_p;
$err=0;
/*if(!$_REQUEST['optiuni'])*/ $link='?optiuni=da';

//optiuni____
if($_GET['early-booking']=='da') {
	$early='da';
	$link=$link.'&early-booking=da';
} else $early='';

if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') {
	$trans=desfa_link(str_replace('_','-',$_REQUEST['transport']));
	$transport=$_REQUEST['transport'];
	$id_transport=get_id_transport($trans);
	if($id_transport<1) $err++;
	$link=$link."&transport=".$_REQUEST['transport'];
} else {
	$trans='toate';
	$transport=$trans;
	$id_transport='';
}

if($_REQUEST['plecare-avion'] && $_REQUEST['plecare-avion']<>'toate') {
	$plecare_avion=$_REQUEST['plecare-avion'];
	$plecare=desfa_link($plecare_avion);
	$id_loc_plecare_av=get_id_localitate($plecare, '');
	if($id_loc_plecare_av<1) $err++;
	$link=$link."&plecare-avion=".$_REQUEST['plecare-avion'];
} else {
	$plecare_avion='toate';
	$id_loc_plecare_av='';
}

if($_REQUEST['nr_zile'] && $_REQUEST['nr_zile']<>'toate') {
	$zile=explode(',',$_REQUEST['nr_zile']);
	$nr_zile=$_REQUEST['nr_zile'];
	if(!$nr_zile) $err++;
	$link=$link."&nr_zile=".$_REQUEST['nr_zile'];
} else {
	$zile='toate';
	$nr_zile='';
}

if($_REQUEST['distanta'] && $_REQUEST['distanta']<>'toate') {
	$distanta=$_REQUEST['distanta'];
	$ds=explode('-',trim($distanta));
	if(ereg('[^a-z0-9-]', $_REQUEST['distanta'])) $err++;
	$link=$link."&distanta=".$distanta;
} else {
	$distanta='toate';
	$ds=array();
}

if($_REQUEST['data-plecare']) {
	$din_luna=$_REQUEST['data-plecare'];
	$timeout = time() + 60 * 60 * 24 * 5;
	setcookie('lona_plecare', $din_luna, $timeout);
	$link=$link."&data-plecare=".$_REQUEST['data-plecare'];
}

if($err>0){
	header("HTTP/1.0 301 Moved Permanently");
	header("Location: ".$link_p);
	exit();
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php");
if($id_tip) { 
$keyword_nice[1]='Circuite '.ucwords($tip).' '.$den_tara.', circuite '.$den_continent;
$keyword_nice[0]=$denumire_agentie.' va ofera circuit '.ucwords($tip).' '.$den_tara.', circuite '.$den_continent;
$keyword_nice[2]='Circuit '.ucwords($tip).' '.$den_tara.', circuite '.$den_continent;
} else { $keyword_nice[1]='Circuite '.$den_tara.', circuite '.$den_continent;
$keyword_nice[0]=$denumire_agentie.' va ofera circuit '.$den_tara.', circuite '.$den_continent;
$keyword_nice[2]='Circuit '.$den_tara.', circuite '.$den_continent; } ?>
<title><? echo $keyword_nice[1]; ?></title>
<meta name="description" content="<?php echo $keyword_nice[0]; ?>" />
<meta name="keywords" content="<?php echo $keyword_nice[2]; ?>" />
<link rel="canonical" href="<?php echo $link_p; ?>" />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/circuite/circuite_tara.php"); ?>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/sejururi_tari.php"); ?>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<?php if($rowSEO['cod_remarketing']) echo $rowSEO['cod_remarketing']; ?>
</body>
</html>