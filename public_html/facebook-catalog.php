<?php 
namespace FacebookAds\Object;
use FacebookAds\Object\ProductCatalog;
use FacebookAds\Object\Fields\ProductCatalogFields;
use FacebookAds\Object\Values\ProductCatalogVerticalValues;

$product_catalog = new ProductCatalog(null,10152817314014208);

$product_catalog->setData(array(
  ProductCatalogFields::NAME => "Hotel Catalog",
  ProductCatalogFields::VERTICAL => ProductCatalogVerticalValues::HOTELS,
));

$product_catalog->create(); ?>