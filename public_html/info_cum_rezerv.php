<?php 
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/peste_tot.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
$meta_index = "noindex,follow";
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
	<title>Cum platesc <?php echo $denumire_agentie; ?></title>
	<meta name="description" content="Cum platesc <?php echo $denumire_agentie; ?>" />
<?php
	include( $_SERVER['DOCUMENT_ROOT'] . '/includes/hoteluri/reviews_top.php' );
	require_once( "includes/header/header_responsive.php" );
?>
</head>

<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header/admin_bar_responsive.php'); ?>

	<?php // Header ?>
	<header>
		<?php require( "includes/header/meniu_header_responsive.php" ); ?>
	</header>

	<?php // Breadcrumbs and general search ?>
	<div class="layout">
		<?php require( "includes/header/breadcrumb_responsive_intreg.php" ); ?>
	</div>

	<div class="layout">
		<div id="payment">
			<span class="titlu_modala"><h1>Cum rezerv o vacanța pe Ocaziituristice.ro</h1></span>
		
			<div class="text-justify pad10 article">
				<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/info_cum_rezerv.php"); ?>
			</div>
		</div>
	</div>

	<?php // Footer ?>
    <?php require_once( "includes/newsletter_responsive.php" ); ?>

	<?php require_once( "includes/footer/footer_responsive.php" ); ?>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

</body>
</html>
