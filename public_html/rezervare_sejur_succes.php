<?php 
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include_once($_SERVER['DOCUMENT_ROOT']."/adm/class/rezervare.php"); 
$meta_index = "noindex,follow";

/*** check login admin ***/
$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

$id_rezervare=$_GET['rez'];
$rezervare = new REZERVARE();
$row_rez=$rezervare->date_rezervare($id_rezervare);
//apelare clasa rezervare

//$id_oferta=$_GET['oferta'];
//$id_hotel=$_GET['hotel'];
$id_oferta=$row_rez['id_oferta'];
 $id_sesiune=$row_rez['session_id'];
//echo "=".session_id();
$modalitate_plata=$row_rez['modalitate_plata'];

$timp_de_la_rezervare=floor((time()-strtotime($row_rez['data_adaugarii']))/(60));
if($timp_de_la_rezervare>100)
{
	header("Location: ".$sitepath."rezervare_expirata.php"); 
}
$tip_pagina='conversion';
$id_hotel=$row_rez['id_hotel'];
//$id_user=$_GET['usr'];
$GLOBALS['make_vizualizata']='nu';
$det= new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');

$link_oferta_return = substr($sitepath,0,-1).make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $detalii['denumire_scurta'], $id_oferta);


// comision la google in analitycs
$pagetype="rezervare";
$comision_google = $row_rez['comision_trimis_google'];
//echo "0000";
$sel_bnr="SELECT * FROM curs_valutar ";
$que_bnr=mysql_query($sel_bnr) or die(mysql_error());
$row_bnr=mysql_fetch_array($que_bnr);
@mysql_free_result($que_bnr);

$pret_total = final_price_lei($row_rez['pret'], $row_rez['moneda']);
$pret_total_euro = final_price_euro(final_price_lei($row_rez['pret'], $row_rez['moneda']));
$procent_avans=$row_rez['procent_avans'];
$comision = $row_rez['comision'];
if ($comision<1){$comision=$pret_total*7/100;}

?>
<script>
fbq('track', 'Purchase', {value: <?php echo $comision;?>, currency:'RON'});
</script>

<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
	<title>Rezervare succes<?php echo $denumire_agentie; ?></title>
	<meta name="description" content="Rezervare finalizata <?php echo $denumire_agentie; ?>" />
<?php
	include( $_SERVER['DOCUMENT_ROOT'] . '/includes/hoteluri/reviews_top.php' );
	require_once( $_SERVER['DOCUMENT_ROOT'] ."/includes/header/header_responsive.php" );
?>
<style>
table.plata_card{ text-align:left;}
.plata_card td{ text-align:left;}
</style>
</head>

<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header/admin_bar_responsive.php'); ?>

	<?php // Header ?>
	<header>
		<?php require( $_SERVER['DOCUMENT_ROOT'] ."/includes/header/meniu_header_responsive.php" ); ?>
	</header>

	<?php // Breadcrumbs and general search ?>
	<div class="layout">
		<?php require( $_SERVER['DOCUMENT_ROOT'] ."/includes/header/breadcrumb_responsive_intreg.php" ); ?>
	</div>

	<div class="layout">
		<div id="payment">
			<h1>Bravo!! Rezervare finalizata!</h1>
		
<div class="text-justify pad10 article">
	<h2><span class="red"><?php echo $row_rez['prenume']; ?></span> multumim pentru rezervarea efectuata! <span class="green"><br />
      In cel mai scurt timp vei fi contactat de unul din agentii nostri?</span></h2>
      
        
      <div class="clearfix bigger-11em" style="padding:0 10px 0 0; line-height:150% !important;">
      
      
      <div class="afisare-oferta">
                    <div class="col-md-12 columns">
                        <h2 class="hotel-name">
          <?php echo $row_rez['hotel_denumire']; ?>
		<?php for($stea=1; $stea<=$detalii_hotel['stele']; $stea++) {$stars .= '<i class="icon-star yellow"></i>';} echo $stars; ?>
		<?php if($detalii_hotel['tip_unitate']!='Circuit') { ?><?php } ?>
                        </h2>
                    </div>
                    <div class="col-md-3 columns">
                            <img src="<?php echo $sitepath; ?>img_mediu_hotel/<?php echo $detalii_hotel['poza1']; ?>" alt="" class="poza">
                    </div>
                    <div class="col-md-9 columns">
                        <div class="circuit-details">
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-building-o" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                           <?php echo $row_rez['hotel_denumire']; ?>
		
		<?php if($detalii_hotel['tip_unitate']!='Circuit') { ?>
                                             <?php $stea=1;
											 for($stea=1; $stea<=$detalii_hotel['stele']; $stea++) {$stars1 .= '<i class="icon-star yellow"></i>';} echo $stars1; ?>
                                          <?php }?>   
                                        </span>
                                    </span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                       <?php //if ( ! $err_logare_admin )  { echo '<pre>';print_r($detalii_hotel);echo '</pre>';}?>

                                            <?php echo $detalii_hotel['localitate']?> / <?php echo $detalii_hotel['zona']?> / <?php echo $detalii_hotel['tara']?>                                       </span>
                                    </span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            
                           
                                   <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-bed" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Durata:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"> <?php echo $row_rez['nr_nopti']; ?> nopti</span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-cutlery" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Masă:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo $row_rez['tip_masa']; ?></span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-bus" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Transport:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo $row_rez['transport']; ?></strong><?php if($row_rez['oras_plecare']) echo ', plecare din <strong>'.get_den_localitate($row_rez['oras_plecare']).'</strong>'; ?></span>
                                <span class="clear" style="display: block"></span>
                            </p>
                            <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Dată incepere sejur:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo denLuniRo(denZileRo(date('l, d F Y', strtotime($row_rez['data_plecare'])))) ; ?></span>
                                <span class="clear" style="display: block"></span>
                            </p>
                                  <p class="titlu">
                                    <span class="name">
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                        <span class="bigger-12em bold">
                                            Tarif:
                                        </span>
                                    </span>
                                <span class="bigger-12em green bold result"><?php echo $row_rez['pret'].' '.$row_rez['moneda']; ?></strong></span>
                                <span class="clear" style="display: block"></span>
                            </p>   
   							 </div>
                    </div>

                </div>
      
      
         <div class="clear"><br /></div>
     
    
    
    
      <table width="100%" cellspacing="2" cellpadding="2" class="plata_card">
  
  <tr>
    <td><?php echo $row_rez['hotel_denumire']." - ".$row_rez['hotel_categorie'] ." stele/".$row_rez['hotel_localitate']?>
    &nbsp; -  &nbsp;
   <strong> <?php echo $suma_de_plata=final_price_lei($row_rez['pret'], $row_rez['moneda'], $adaos=NULL)?></strong> Lei
    <br />
    <?php echo $row_rez['specificatii_oferta']?>
    
    Data inceput sejur:<?php echo $row_rez['data_plecare']. " / nr nopti:" .$row_rez['nr_nopti']." tip masa:".$row_rez['tip_masa']?>
    <br />
     
    </td>

    
  </tr>
  <tr>
    <td><?php if ($row_rez['disponibilitate']=="disponibil")	
		
		{
		$OrderNumber=$id_rezervare;	
		$suma_de_plata=final_price_lei($row_rez['pret'], $row_rez['moneda'], $adaos=NULL);
		$denumire_produs=$row_rez['oferta_denumire'];
		$descriere_produs="descriere _ ".$row_rez['oferta_denumire'];
		$prenume=$row_rez['prenume'];
		$nume=$row_rez['nume'];
		$adresa_facturare=$row_rez['adresa_facturare'];
		$oras_facturare=$row_rez['oras_facturare'];
		$telefon_facturare=$row_rez['telefon_facturare'];
		$email_facturare=$row_rez['email_facturare'];
		$denumire_facturare=$row_rez['denumire_facturare'];
		$reg_comert=$row_rez['reg_comert'];
		$cui_cnp=$row_rez['cui_cnp'];
		$linie_factura='Rezervare nr.'.$OrderNumber.'<br/>'.$denumire_produs;
		$total_pret=$row_rez['pret'];
	

   
	$nr_zile_expira=floor((strtotime($row_rez['data_plecare'])-time())/(60*60*24));
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/plata_card_mobil_pay.php");
		//include_once($_SERVER['DOCUMENT_ROOT']."/includes/plata_card.php");
		 }
			?></td>
           
   
  </tr>
          </table>
    
    
      	
        
        </div>		
                
			</div>
		</div>
	</div>

	<?php // Footer ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_responsive.php" ); ?>

	<?php require_once( $_SERVER['DOCUMENT_ROOT']."/includes/footer/footer_responsive.php" ); ?>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/addins_bodybottom_responsive.php" ); ?>

</body>
</html>
