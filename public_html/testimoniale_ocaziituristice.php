<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Testimoniale <?php echo $denumire_agentie; ?>, pareri si impresii de la turistii care au calatorit cu Agentia de turism <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Testimoniale <?php echo $denumire_agentie; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <h1 class="blue float-left">Testimoniale</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="pad10 article text-justify">
          
<?php
$sel_testimoniale="SELECT * FROM testimoniale ORDER BY id DESC";
$que_testimoniale=mysql_query($sel_testimoniale) or die(mysql_error());
?>
          <?php while($row_testimoniale=mysql_fetch_array($que_testimoniale)) { ?>
          <span id="<?php echo $row_testimoniale['id']; ?>"></span>
          <?php if($_GET['id']==$row_testimoniale['id']) { ?><div class="testimoniale t-sel"><?php } else { ?><div class="testimoniale"><?php } ?>
            <div class="item-left">
              <span class="bigger-13em blue"><?php echo $row_testimoniale['nume']; ?></span>
              <span class="black"><?php echo $row_testimoniale['tip_turist']; ?></span>
              <span class="smaller-08em data"><?php echo ucwords(denLuniRo(date('F Y',strtotime($row_testimoniale['data'])))); ?></span>
            </div>
            <div class="content black"><?php echo afisare_frumos($row_testimoniale['comentariu'],'nl2br'); ?></div>
            <br class="clear" />
          </div>
          <?php } ?>
          
        </div>
        
      </div>
    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>