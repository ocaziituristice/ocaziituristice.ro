<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
$tip=desfa_link($_REQUEST['tip']);
$id_tip_sejur=get_id_tip_sejur($tip);
if(!$id_tip_sejur) {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'"; if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<?php $parametru['pagina']='new_sejururi1';
$new_formula_key = new new_formula_keywords_new($parametru);
$keyword_nice = $new_formula_key->do_nice_sejur_new(); ?>
<title><?php echo $keyword_nice[1];?></title>
<meta name="description" content="<?php echo $keyword_nice[0]; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">
        <h1 class="blue">Oferte <?php echo ucwords($tip); ?></h1>
        <br class="clear" />
        <div class="Hline"></div>
		<div class="NEW-column-left1">
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/turism_extern_tematica.php"); ?>
        </div>
        <div class="NEW-column-right1">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_turism_extern_tematica.php"); ?>
        </div>
      </div>
    </div>
    <br /><br />
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
