<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
?> 
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Turism extern, sejururi, cazari, circuite | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="oferte sejururi turism extern, de la agentiiile de turism din romania, oferte last minute, early booking, cazari, circuite, luna de miere " />
<?php $tip_c=desfa_link($_GET['tip']);
if($_SERVER['REQUEST_URI']=="/oferte-".fa_link_vechi($tip_c)."/") { ?><link rel="canonical" href="<?php echo "https://www.ocaziituristice.ro/oferte-".fa_link($tip_c)."/"; ?>" /><?php } ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">

      <div id="NEW-destinatie">
  
        <h1 class="blue float-left">Sejururi Turism Extern, Cazari</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear">
        <div class="Hline"></div>
          
        <div class="pad5">
          <br class="clear">
          
          <div class="chenar chn-color-blue" style="margin:10px 0;"><div class="clearfix">
            <div class="NEW-search-wide">
              <div class="chapter-title blue">Caută Oferte <?php echo $dentip; ?>:</div>
              <div id="afis_filtru"></div>
            </div>
          </div></div>
          
          <br class="clear"><br>
        </div>
          
        <div class="NEW-column-left1">
          <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/turism_extern.php"); ?>
        </div>
        <div class="NEW-column-right1">
          <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_turism_extern.php"); ?>
        </div>
    
		<br class="clear"><br>
    
      </div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<script type="text/javascript">
$("#afis_filtru").load("/includes/search/filtru.php?turism-extern");
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
