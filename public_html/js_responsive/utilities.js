var tui = tui || {};
tui.utilities = {

    getParameterByName: function(e) {
        e = String(e).replace(/[.*+?|()[]{}\]/g, "$&");
        var a = RegExp("[?&]" + e + "=([^&]*)").exec(window.location.search);
        return a ? decodeURIComponent(a[1].replace(/\+/g, " ")) : null
    },
    getParameterByNameFromUrl: function(e, a) {
        e = String(e).replace(/[.*+?|()[]{}\]/g, "$&");
        var t = RegExp("[?&]" + e + "=([^&]*)").exec("?" + a.split("?")[1]);
        return t ? decodeURIComponent(t[1].replace(/\+/g, " ")) : null
    },
    GetDataRooms: function(e, a) {
        var t = {};
        if (null != e && "null" != e && "" != e && e.indexOf("-") >= 0 && "undefined" != typeof e) {
            var r = e.split("-"),
                o = [];
            r.length > 0 && "" != r[1] && (o = r[1].split(";")), t = {
                Adults: r[0].toUpperCase().replace("ADT", ""),
                Children: o
            }
        }
        return t
    },
    GetDataRoomForRequest: function(e, a) {
        var t = {};
        if (null != e && "null" != e && "" != e && e.indexOf("-") >= 0 && "undefined" != typeof e) {
            var r = e.split("-"),
                o = [];
            r.length > 0 && "" != r[1] && (o = r[1].split(";")), t = {
                Adults: r[0].toUpperCase().replace("ADT", ""),
                Children: o.length,
                ChildrenAges: o
            }
        }
        return t
    },
    SetDataRoom: function(e) {
        var a = $("#hotel-room" + e + " #hotels-r" + e + "-adult span.value").text(),
            t = $("#hotel-room" + e + " #hotels-r" + e + "-children span.value").text(),
            r = [];
        return parseInt(t) > 0 && (r = $.map($("#hotel-room" + e + " #children_age_pop_box_hotels" + e + " select"), function(e) {
            return $(e).val()
        })), {
            Adults: a,
            Children: t,
            ChildrenAges: r
        }
    },
    SetEventsChildrenDropDown: function() {
        $(".form-hotels-box-room").on("click", ".children-ages-hotels", function() {
            var e = parseInt($(this).parent().data("room"));
            $(".children_age_pop_hotels").hide();
            var a = $("#children_hotels" + e + " select").length,
                t = parseInt($(this).find("a span.value").text());
            if (a > t) {
                var r = a - t;
                0 == t && (r = a - 1, $("#children_hotels" + e + " select:first").val("1"));
                for (var o = 0; o < r; o++) $("#children_hotels" + e + " select:last").remove()
            } else if (a < t)
                for (var o = 0; o < t - a; o++) {
                    var n = $("#children_hotels" + e + " select:last").clone();
                    $(n).data("child", parseInt($(n).data("child")) + 1), $(n).val("1"), $("#children_hotels" + e + " #children_age_pop_box_hotels" + e).append(n)
                }
            t > 0 && $("#children_hotels" + e).show(), $(".form-hotels-box-room").on("click", ".children_age_pop_close_hotels, .apply_children_age_pop_hotels", function() {
                $(this).closest(".children_age_pop_hotels").hide()
            })
        }), $("div[id^='add-room']").click(function() {
            var e = parseInt($(this).parent().data("room"));
            $("#hotels-r" + (e + 1) + "-adult").html('1<span class="value">1</span>'), $("#edit-room" + e).hide(), $("#edit-room" + (e + 1) + ", #hotel-room" + (e + 1)).show()
        }), $("div[id^='delete-room']").click(function() {
            var e = parseInt($(this).parent().data("room"));
            $("#hotels-r" + e + "-adult").html('1<span class="value">1</span>'), $("#hotels-r" + e + "-children").html('0<span class="value">0</span>'), $("#children_hotels" + e + " select:not(:first-child)").remove(), $("#children_hotels" + e + " select:first").val("1"), $("#hotel-room" + e).hide(), $("#edit-room" + (e - 1)).show()
        })
    },
    HotelsCalendar: function(e) {
        $("#hoteluri_box_calendar_container").show(), e && null != $("#hotels-data-checkin-ah1").data().dateRangePicker && $("#hotels-data-checkin-ah1").data().dateRangePicker.destroy(), $("#hotels-data-checkin-ah1").dateRangePicker({
            autoClose: !0,
            showShortcuts: !1,
            container: "#hotels-box-calendar-container-checkin",
            minDays: 1,
            maxDays: 31,
            hoveringTooltip: function(e) {
                var a = ["", '<span class="bug-next-date">Acum alegeti data de Check Out</span>'];
                return e > 2 ? a[e] ? a[e] : e - 1 + " nopti" : a[e] ? a[e] : e - 1 + " noapte"
            },
            startOfWeek: "monday",
            startDate: $.datepicker.formatDate("dd/mm/yy", new Date),
            getValue: function() {
                return $("#hotels-data-checkin-ah1").val() ? $("#hotels-data-checkin-ah1").val() : ""
            },
            setValue: function(e, a, t) {
                if (Number($(".selected-days-num").text()) > 0) {
                    var r = a.split("/");
                    a = r[2] + "/" + r[1] + "/" + r[0];
                    var o = t.split("/");
                    t = o[2] + "/" + o[1] + "/" + o[0];
                    try {
                        $("#hotels-data-checkin-ah1").val(a + " - " + t), $(".search-title-boxPeriod").html("<span>" + $(".selected-days-num").text() + " nopti</span>")
                    } catch (e) {}
                }
            },
            beforeShowDay: function(e) {
                var a = !0;
                try {
                    var t = new Date;
                    t.setDate(t.getDate()), e < t && (a = !1)
                } catch (e) {}
                var r = "",
                    o = "";
                return [a, r, o]
            },
            language: tui.language
        }), $(".apply-btn").removeClass("hide").show(), $(".apply-btn").click(function() {
            $(".date-picker-wrapper").fadeOut(200)
        }).fadeIn(200)
    },
    decodeGiata: function(e) {
        var a = "",
            t = e.toUpperCase();
        return t = t.replace(/P/g, "0"), t = t.replace(/Q/g, "1"), t = t.replace(/S/g, "2"), t = t.replace(/E/g, "3"), t = t.replace(/F/g, "4"), t = t.replace(/T/g, "5"), t = t.replace(/H/g, "6"), t = t.replace(/U/g, "7"), t = t.replace(/K/g, "8"), t = t.replace(/O/g, "9"), a = t.trim()
    },
    imgError: function(e) {
        return e.onerror = "", e.src = "/Client/Images/tui.png", !0
    },
    makePost: function(e, a, t, r, o) {
        var n = 2e4;
        "undefined" != typeof o && (n = o), $.ajax({
            url: e,
            type: "POST",
            dataType: "json",
            timeout: n,
            data: a,
            success: t,
            error: r
        })
    },
    makeGet: function(e, a, t, r) {
        $.ajax({
            url: e,
            type: "GET",
            dataType: "JSON",
            timeout: 2e4,
            data: a,
            success: t,
            error: r
        })
    },
    redirectErrorPage: function(e) {
        console.log(e);
        var a = location.origin + "/error-page";
        window.location.href = a
    },
    addHotelHeliviewVideo: function(e) {
        "undefined" != typeof e && "" != $.trim(e) && null != e && 0 != e && "0" != e && $.ajax({
            type: "POST",
            url: location.origin + "/sitecore/customapi/TUITravelCenterHotelDetails/GetHotelHeliviewVideoByGiata",
            data: {
                giataCode: e
            },
            dataType: "json",
            success: function(e) {
                if ("Success" == e.Status) {
                    var a = e.Fields.HotelVideoLink[0];
                    if (null != a && "undefined" != typeof a && "" != a) {
                        var t = "<li><div class='iframe-cover'></div><iframe height='40' frameborder='0' mozallowfullscreen='' webkitallowfullscreen='' allowfullscreen='' width='46' scrolling='no' marginwidth='0px' marginheight='0px' id='singlePlayerIframe' name='singlePlayerIframe' style='width:46px;height:40px;display:block;' src='" + a + "&autoPlay=false'></iframe></li>",
                            r = $(t);
                        0 == $(".es-carousel ul").length ? ($(".es-carousel").append("<ul>" + t + "</ul>"), Gallery.init()) : Gallery.prependItems(r)
                    }
                }
            }
        })
    },
    EncodeUrlParameter: function(e) {
        return (null == e ? "" : e).replace(".", " ").replace("?", " ").replace("*", " ").replace("/", " ").replace(" ", "-").replace("&", "_and_")
    },
    DecodeUrlParameter: function(e) {
        return (null == e ? "" : e).replace("-", " ").replace("_and_", "&")
    },
    RemovePlaceholdersForCompletedFields: function() {
        $("form .new_input_text > input").each(function(e, a) {
            $(a).val() && $(a).addClass("used")
        })
    },
    scrollToSection: function(e) {
        return $("html, body").animate({
            scrollTop: $(e).offset().top
        }, 2e3), !1
    },
    SideBreadCrumbs: function() {
        if ($("nav.cd-vertical-nav").length && 0 == $("#allresults .hotels_no_results").length) {
            var e = $("div[data-side-breadcrumb-title]"),
                a = $("nav.cd-vertical-nav").html('<ul class="nav navbar"></ul>'),
                t = 0;
            e.each(function(e, r) {
                var o = "",
                    n = $(r),
                    l = n.data().sideBreadcrumbTitle,
                    i = n.data().componentName;
                n.attr("id", "SideNav-" + l.replace(/ /g, "-")), l.length > 0 && (n.offset().top > t ? (t = n.offset().top, a.find(".nav").append('<li class="' + o + '"><a id="SideNavLink-' + i + '" href="#SideNav-' + l.replace(/ /g, "-") + '" class="section-link">' + l + "<span></span></a></li>")) : a.find(".nav li:last").before('<li class="' + o + '"><a id="SideNavLink-' + i + '" href="#SideNav-' + l.replace(/ /g, "-") + '" class="section-link">' + l + "<span></span></a></li>"))
            });
            var r = $(".scrollToTop").addClass("inactive").hide().text();
            a.find(".nav").append('<li><a id="SideNav-to-top" href="#" class="section-link">' + r + '<span class="fa fa-arrow-circle-up"></span></a></li>'), $("#SideNav-to-top").click(function(e) {
                return e.preventDefault(), $("html, body").animate({
                    scrollTop: 0
                }, 600), !1
            }), $("body").scrollspy({
                target: ".cd-vertical-nav"
            })
        }
    }
}, $.datepicker.regional.ro = {
    closeText: "Ănchide",
    prevText: "&laquo; Luna precedentÄ",
    nextText: "Luna urmÄtoare &raquo;",
    currentText: "Azi",
    monthNames: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
    monthNamesShort: ["Ian", "Feb", "Mar", "Apr", "Mai", "Iun", "Iul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    dayNames: ["DuminicÄ", "Luni", "MarĹŁi", "Miercuri", "Joi", "Vineri", "SĂ˘mbÄtÄ"],
    dayNamesShort: ["Dum", "Lun", "Mar", "Mie", "Joi", "Vin", "SĂ˘m"],
    dayNamesMin: ["Du", "Lu", "Ma", "Mi", "Jo", "Vi", "SĂ˘"],
    weekHeader: "SÄpt",
    dateFormat: "dd.mm.yy",
    firstDay: 1,
    isRTL: !1,
    showMonthAfterYear: !1,
    yearSuffix: ""
}, $.datepicker.setDefaults($.datepicker.regional.ro), jQuery.global.preferCulture("ro-RO");