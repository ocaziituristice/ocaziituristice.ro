
var bustcachevar = 0; //bust potential caching of external pages after initial request? (1=yes, 0=no)
var loadstatustext = '<div class="text-center"><img src="/images/loader.gif"></div>';

var defaultcontentarray=new Object()
var bustcacheparameter = ""

function ajaxpage(url, containerid){
    var page_request = false
    if (window.XMLHttpRequest) // if Mozilla, Safari etc
        page_request = new XMLHttpRequest()
    else if (window.ActiveXObject){ // if IE
        try {
            page_request = new ActiveXObject("Msxml2.XMLHTTP")
        }
        catch (e){
            try{
                page_request = new ActiveXObject("Microsoft.XMLHTTP")
            }
            catch (e){}
        }
    }
    else
        return false

    document.getElementById(containerid).innerHTML = '<div class="text-center"><img src="/images/loader.gif"></div>';
    page_request.onreadystatechange=function(){
        loadpage(page_request, containerid)
    }

    if (bustcachevar) //if bust caching of external page
        bustcacheparameter=(url.indexOf("?")!=-1)? "&"+new Date().getTime() : "?"+new Date().getTime()
    page_request.open('GET', url+bustcacheparameter, true)
    page_request.send(null)
}

function loadpage(page_request, containerid){
    if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1)){
        var Result = page_request.responseText.split("||javascript||");
        document.getElementById(containerid).innerHTML=Result[0];
        if(Result[1]) eval(Result[1]);
    }
}

function loadobjs(revattribute){
    if (revattribute!=null && revattribute!=""){ //if "rev" attribute is defined (load external .js or .css files)
        var objectlist=revattribute.split(/\s*,\s*/) //split the files and store as array
        for (var i=0; i < objectlist.length; i++){
            var file=objectlist[i]
            var fileref=""
            if (file.indexOf(".js")!=-1){ //If object is a js file
                fileref=document.createElement('script')
                fileref.setAttribute("type","text/javascript");
                fileref.setAttribute("src", file);
            }
            else if (file.indexOf(".css")!=-1){ //If object is a css file
                fileref=document.createElement("link")
                fileref.setAttribute("rel", "stylesheet");
                fileref.setAttribute("type", "text/css");
                fileref.setAttribute("href", file);
            }
        }
        if (fileref!=""){
            document.getElementsByTagName("head").item(0).appendChild(fileref)
            loadedobjects+=file+" " //Remember this object as being already added to page
        }
    }
}

var nav_links = Array();
var now_link = 1;
nav_links[now_link] = 'main.php';

var now_loaded = '';
var now_loaded_rel;

function chatvirte(es){
// if(now_loaded != es.getAttribute("url")){
    ajaxpage(es.getAttribute("url"), es.getAttribute("rel"));
    if(es.getAttribute("rel")=='main_div'){
        now_link++;
        nav_links[now_link]=es.getAttribute("url");
    }
    // now_loaded = es.getAttribute("url");
    // now_loaded_rel = es.getAttribute("rel");
// }
    return false;
}

function reload(){
    ajaxpage(es.getAttribute("url"), es.getAttribute("rel"));
}

function reload2(){
    ajaxpage(nav_links[now_link], 'main_div');
}

function ajax_go_back(){
    if(now_link>1){
        now_link--;
        ajaxpage(nav_links[now_link], 'main_div');
    }
}

function ajax_go_forward(){
    var go_forwad = now_link+1;
    if(nav_links[go_forwad ]){
        now_link++;
        ajaxpage(nav_links[now_link], 'main_div');
    }
}

