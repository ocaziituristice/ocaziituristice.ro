function af_filtru_responsive(tip, tip_oferta) {
    var linkF = '';
    var tara = document.getElementById('tara').value;
    if (tara != '') {
        linkF = '?tara=' + tara;
        var localitate = document.getElementById('localitate').value;
        if (localitate != '') {
            linkF = linkF + localitate;
        }
        if (tip == 2) {
            var transport = document.getElementById('transport').value;
            if (transport != '') linkF = linkF + transport;
            var stele = document.getElementById('stele').value;
            if (stele != '') linkF = linkF + '&stele=' + stele;
        }
    }
    if (tip_oferta != '' && typeof(tip_oferta) != 'undefined') {
        if (tara != '') linkF = linkF + '&'; else linkF = linkF + '?';
        linkF = linkF + 'tip_oferta=' + tip_oferta;
    }
    var adresa = '/includes/search/';
    $("#afis_filtru").load(adresa + "filtru_responsive.php" + linkF);
}

function af_filtru_mobile_responsive(tip, tip_oferta) {
    var linkF = '';
    var tara = document.getElementById('tara').value;
    if (tara != '') {
        linkF = '?tara=' + tara;
        var localitate = document.getElementById('localitate').value;
        if (localitate != '') {
            linkF = linkF + localitate;
        }
        if (tip == 2) {
            var transport = document.getElementById('transport').value;
            if (transport != '') linkF = linkF + transport;
            var stele = document.getElementById('stele').value;
            if (stele != '') linkF = linkF + '&stele=' + stele;
        }
    }
    if (tip_oferta != '' && typeof(tip_oferta) != 'undefined') {
        if (tara != '') linkF = linkF + '&'; else linkF = linkF + '?';
        linkF = linkF + 'tip_oferta=' + tip_oferta;
    }
    adresa = '/includes/search/';
    $("#afis_filtru").load(adresa + "filtru_mobile_responsive.php" + linkF);
}

$(document).ready(function () {
    if ($(window).width() < 767) {
        $('.homepage-boxes .prod').on('click', function () {
            $('.homepage-boxes .prod').removeClass('show-mobile');
            $(this).addClass('show-mobile');
        });
    }
});
