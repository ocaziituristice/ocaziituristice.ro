/****** JQUERY UI EASING ******/
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
    {
        def: 'easeOutQuad',
        swing: function (x, t, b, c, d) {
            //alert(jQuery.easing.default);
            return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
        },
        easeInQuad: function (x, t, b, c, d) {
            return c*(t/=d)*t + b;
        },
        easeOutQuad: function (x, t, b, c, d) {
            return -c *(t/=d)*(t-2) + b;
        },
        easeInOutQuad: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t + b;
            return -c/2 * ((--t)*(t-2) - 1) + b;
        },
        easeInCubic: function (x, t, b, c, d) {
            return c*(t/=d)*t*t + b;
        },
        easeOutCubic: function (x, t, b, c, d) {
            return c*((t=t/d-1)*t*t + 1) + b;
        },
        easeInOutCubic: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t + b;
            return c/2*((t-=2)*t*t + 2) + b;
        },
        easeInQuart: function (x, t, b, c, d) {
            return c*(t/=d)*t*t*t + b;
        },
        easeOutQuart: function (x, t, b, c, d) {
            return -c * ((t=t/d-1)*t*t*t - 1) + b;
        },
        easeInOutQuart: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
            return -c/2 * ((t-=2)*t*t*t - 2) + b;
        },
        easeInQuint: function (x, t, b, c, d) {
            return c*(t/=d)*t*t*t*t + b;
        },
        easeOutQuint: function (x, t, b, c, d) {
            return c*((t=t/d-1)*t*t*t*t + 1) + b;
        },
        easeInOutQuint: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
            return c/2*((t-=2)*t*t*t*t + 2) + b;
        },
        easeInSine: function (x, t, b, c, d) {
            return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
        },
        easeOutSine: function (x, t, b, c, d) {
            return c * Math.sin(t/d * (Math.PI/2)) + b;
        },
        easeInOutSine: function (x, t, b, c, d) {
            return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
        },
        easeInExpo: function (x, t, b, c, d) {
            return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
        },
        easeOutExpo: function (x, t, b, c, d) {
            return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
        },
        easeInOutExpo: function (x, t, b, c, d) {
            if (t==0) return b;
            if (t==d) return b+c;
            if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
            return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
        },
        easeInCirc: function (x, t, b, c, d) {
            return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
        },
        easeOutCirc: function (x, t, b, c, d) {
            return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
        },
        easeInOutCirc: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
            return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
        },
        easeInElastic: function (x, t, b, c, d) {
            var s=1.70158;var p=0;var a=c;
            if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
            if (a < Math.abs(c)) { a=c; var s=p/4; }
            else var s = p/(2*Math.PI) * Math.asin (c/a);
            return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        },
        easeOutElastic: function (x, t, b, c, d) {
            var s=1.70158;var p=0;var a=c;
            if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
            if (a < Math.abs(c)) { a=c; var s=p/4; }
            else var s = p/(2*Math.PI) * Math.asin (c/a);
            return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
        },
        easeInOutElastic: function (x, t, b, c, d) {
            var s=1.70158;var p=0;var a=c;
            if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
            if (a < Math.abs(c)) { a=c; var s=p/4; }
            else var s = p/(2*Math.PI) * Math.asin (c/a);
            if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
            return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
        },
        easeInBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c*(t/=d)*t*((s+1)*t - s) + b;
        },
        easeOutBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
        },
        easeInOutBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
            return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
        },
        easeInBounce: function (x, t, b, c, d) {
            return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
        },
        easeOutBounce: function (x, t, b, c, d) {
            if ((t/=d) < (1/2.75)) {
                return c*(7.5625*t*t) + b;
            } else if (t < (2/2.75)) {
                return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
            } else if (t < (2.5/2.75)) {
                return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
            } else {
                return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
            }
        },
        easeInOutBounce: function (x, t, b, c, d) {
            if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
            return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
        }
    });
/****** JQUERY UI EASING ******/

/****** JQUERY UI TOTOP ******/
(function(a){a.fn.UItoTop=function(b){var c={text:"To Top",min:200,inDelay:600,outDelay:400,containerID:"toTop",containerHoverID:"toTopHover",scrollSpeed:1200,easingType:"linear"};var d=a.extend(c,b);var e="#"+d.containerID;var f="#"+d.containerHoverID;a("body").append('<a href="#" id="'+d.containerID+'">'+d.text+"</a>");a(e).hide().click(function(){a("html, body").animate({scrollTop:0},d.scrollSpeed,d.easingType);a("#"+d.containerHoverID,this).stop().animate({opacity:0},d.inDelay,d.easingType);return false}).prepend('<span id="'+d.containerHoverID+'"></span>').hover(function(){a(f,this).stop().animate({opacity:1},600,"linear")},function(){a(f,this).stop().animate({opacity:0},700,"linear")});a(window).scroll(function(){var b=a(window).scrollTop();if(typeof document.body.style.maxHeight==="undefined"){a(e).css({position:"absolute",top:a(window).scrollTop()+a(window).height()-50})}if(b>d.min)a(e).fadeIn(d.inDelay);else a(e).fadeOut(d.Outdelay)})}})(jQuery)
/****** JQUERY UI TOTOP ******/

/****** VTIP *****************/
this.vtip=function(){this.xOffset=-10;this.yOffset=30;$(".vtip").unbind().hover(function(a){this.t=this.title;this.title="";this.top=(a.pageY+yOffset);this.left=(a.pageX+xOffset);$("body").append('<p id="vtip"><img id="vtipArrow" />'+this.t+"</p>");$("p#vtip #vtipArrow").attr("src","/images/vtip_arrow.png");$("p#vtip").css("top",this.top+"px").css("left",this.left+"px").fadeIn("slow")},function(){this.title=this.t;$("p#vtip").fadeOut("slow").remove()}).mousemove(function(a){this.top=(a.pageY+yOffset);this.left=(a.pageX+xOffset);$("p#vtip").css("top",this.top+"px").css("left",this.left+"px")})};jQuery(document).ready(function(a){vtip()});
/****** VTIP *****************/

/****** MULTIPLE COLUMNS SUBMENU *******/
if(typeof(jQuery144)!='undefined') {j=jQuery144.noConflict();}
else j=jQuery;

j(document).ready(function ($){
    $('#NEW-menu a').mouseover(function(){

        var sm=$('#submenus #sub_'+$(this).attr('id'));
        $('#submenus .submenu').slideUp('fast');
        $('#submenus #sub_'+$(this).attr('id')+' .sm_column:odd').addClass('even');
        $('#submenus #sub_'+$(this).attr('id')+' ul:odd').addClass('even');
        sm.slideDown('fast');
        $('#submenus').slideDown('fast');

        $('#NEW-menu a').removeClass('sel');

        $(this).addClass('sel');
        var tabs=$('.divClose').position();

        var a_p=$(this).offset();
        var l=$('#pageNavbar').offset();
        var a_w=$(this).outerWidth();
        var sm_w=sm.outerWidth();
        var sm_p=0;

        /*if(a_p.left+sm_w<l.left+980)
        {*/
        sm_p=a_p.left;
        /*}
        else if(sm_w>=980) {sm_p=l.left}
        else sm_p=l.left+(a_p.left+a_w/2-l.left)-(a_p.left+a_w/2-l.left)/980*sm_w;*/
        sm.css('left',sm_p);
        sm.css('top',tabs.top+$('.divClose').outerHeight);
    });

    $('#pageNavbar').mouseleave(function (){
        $('#submenus').slideUp('fast');
        $('#NEW-menu a').removeClass('sel');
    });

    set_submenu();

});

function set_submenu() {
    j('#submenus').css('left',(j(window).width()-980)/2);
}
/****** MULTIPLE COLUMNS SUBMENU *******/

/****** FILTRARE *******/
function filtrare_get_nou_circuite(prefix, destinatie) {
    if(destinatie) l='/'+prefix+destinatie+'/';
    else l='/'+prefix;
    document.location.href=l;
}
function filtrare_get_nou(prefix, tara, destinatie) {
    if(tara!='') {
        if(destinatie) l='/'+prefix+tara+'/'+destinatie+'/';
        else l='/'+prefix+tara+'/'; } else  l='/'+prefix;
    document.location.href=l;
}
function filtrare_get_nou_lm(prefix, tara) {
    if(tara) l='/'+prefix+'/'+tara+'/';
    else l='/'+prefix+'/';
    document.location.href=l;
}

function filtrare_restu_nou(link_p, transport, stele, masa, distanta, plecare_avion, plecare_autocar, concept) {
    var link_nou=''
    if(transport!='toate') link_nou=link_nou+'&transport='+transport;
    if(masa!='toate') link_nou=link_nou+'&masa='+masa;
    if(stele!='toate') link_nou=link_nou+'&stele='+stele;
    if(distanta!='toate') link_nou=link_nou+'&distanta='+distanta;
    if(plecare_avion!='toate' && transport!='toate') link_nou=link_nou+'&plecare-avion='+plecare_avion;
    if(plecare_autocar!='toate' && transport!='toate') link_nou=link_nou+'&plecare-autocar='+plecare_autocar;
    if(concept!='toate') link_nou=link_nou+'&concept='+concept;
    if(link_nou!='') link_p=link_p+link_nou;
    document.location.href=link_p;
}

function filtrare_restu_nou_lm(link_p, transport ,stele, masa, distanta, plecare_avion, concept, data_plecarii) {
    var link_nou=''
    if(transport!='toate') link_nou=link_nou+'&transport='+transport;
    if(masa!='toate') link_nou=link_nou+'&masa='+masa;
    if(stele!='toate') link_nou=link_nou+'&stele='+stele;
    if(distanta!='toate') link_nou=link_nou+'&distanta='+distanta;
    if(plecare_avion!='toate' && transport!='toate') link_nou=link_nou+'&plecare-avion='+plecare_avion;
    if(concept!='toate') link_nou=link_nou+'&concept='+concept;
    if(data_plecarii!='toate') link_nou=link_nou+'&data_plecarii='+data_plecarii;
    if(link_nou!='') link_p=link_p+link_nou;
    document.location.href=link_p;
}

function filtrare_circuite(link_p, transport , nr_zile, plecare_avion) {
    var link_nou=''
    if(transport!='toate') link_nou=link_nou+'&transport='+transport;
    if(nr_zile!='toate') link_nou=link_nou+'&nr_zile='+nr_zile;
    if(plecare_avion!='toate' && transport!='toate') link_nou=link_nou+'&plecare-avion='+plecare_avion;
    if(link_nou!='') link_p=link_p+link_nou;
    document.location.href=link_p;
}

function af_filtru(tip,tip_oferta) {
    var linkF='';
    var tara=document.getElementById('tara').value;
    if(tara!='') {
        linkF='?tara='+tara;
        var localitate=document.getElementById('localitate').value;
        if(localitate!='') {
            linkF=linkF+localitate;
        }
        if(tip==2) {
            /*var luni_plecari=document.getElementById('luni_plecari').value;
                if(luni_plecari!='') linkF=linkF+'&luni_plecari='+luni_plecari;*/
            var transport=document.getElementById('transport').value;
            if(transport!='') linkF=linkF+transport;
            var stele=document.getElementById('stele').value;
            if(stele!='') linkF=linkF+'&stele='+stele;
            var masa=document.getElementById('masa').value;
            if(masa!='') linkF=linkF+'&masa='+masa;
        }
    }
    if(tip_oferta!='' && typeof(tip_oferta)!='undefined') {
        if(tara!='') linkF=linkF+'&'; else linkF=linkF+'?';
        linkF=linkF+'tip_oferta='+tip_oferta;
    }
    if(window.location.pathname=='/') var adresa = '/'; else var adresa = '/includes/search/';
    $("#afis_filtru").load(adresa+"filtru.php"+linkF);
}
/****** FILTRARE *******/

/****** SET COOKIES *******/
function setCookie(c_name,value,exdays) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; path=/; expires=" + exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}
function set_grad_ocupare(plecdata, pleczile, adulti, copii, copil1, copil2, copil3, tip_masa, tip_transport, orasplec) {
    var cookie_value = getCookie('grad_ocupare');
    var cookie_value_replaced = cookie_value.replace(/%2A/g, "*")
    var grdocup = cookie_value_replaced.split('*');
    var value_grad_ocupare = '';
    if(plecdata=='') plecdata = grdocup[0];
    if(pleczile=='') pleczile = grdocup[1];
    if(adulti=='') adulti = grdocup[2];
    if(copii=='') copii = grdocup[3];
    if(copil1=='') copil1 = grdocup[4];
    if(copil2=='') copil2 = grdocup[5];
    if(copil3=='') copil3 = grdocup[6];
    if(tip_masa=='') tip_masa = grdocup[7];
    if(tip_transport=='') tip_transport = grdocup[8];
    if(tip_transport==0) tip_transport = '';
    if(orasplec=='') orasplec = grdocup[9];
    if(orasplec==0) orasplec = '';

    value_grad_ocupare = plecdata + '*' + pleczile + '*' + adulti + '*' + copii + '*' + copil1 + '*' + copil2 + '*' + copil3 + '*' + tip_masa + '*' + tip_transport + '*' + orasplec;
    setCookie('grad_ocupare', value_grad_ocupare, 30);
}
/****** SET COOKIES *******/

/****** JQUERY UI DIALOG *******/
$(document).ready(function() {
    $("a.infos").each(function() {
        var $link = $(this);
        var $dialog = $('<div></div>')
            .load($link.attr('href') + ' #NEW-destinatie')
            .dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                title: $link.attr('title'),
                width: 750,
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery($dialog).dialog('close');
                    })
                }
            });
        $link.click(function() {
            $dialog.dialog('open');
            return false;
        });
    });
    $("a.infos2").each(function() {
        var $link = $(this);
        var $dialog = $('<div></div>')
            .load($link.attr('href') + ' #NEW-destinatie')
            .dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                title: $link.attr('title'),
                width: 850,
                height: 570,
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery($dialog).dialog('close');
                    })
                }
            });
        $link.click(function() {
            $dialog.dialog('open');
            return false;
        });
    });
    $("a.infos3").each(function() {
        var $link = $(this);
        var $dialog = $('<div></div>')
            .load($link.attr('href') + ' #NEW-destinatie')
            .dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                title: $link.attr('title'),
                width: 900,
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery($dialog).dialog('close');
                    })
                }
            });
        $link.click(function() {
            $dialog.dialog('open');
            return false;
        });
    });
    $("a.preturi_normale").each(function() {
        var $link = $(this);
        var $dialog = $('<div></div>')
            .load($link.attr('href') + ' #NEW-detaliiOferta')
            .dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                title: $link.attr('title'),
                width: 750,
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery($dialog).dialog('close');
                    })
                }
            });
        $link.click(function() {
            $dialog.dialog('open');
            return false;
        });
    });
    $("a.excursii_detalii").each(function() {
        var $link = $(this);
        var $dialog = $('<div></div>')
            .load($link.attr('href') + ' #NEW-detaliiOferta')
            .dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                title: $link.attr('title'),
                width: 850,
                height: 570,
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery($dialog).dialog('close');
                    })
                }
            });
        $link.click(function() {
            $dialog.dialog('open');
            return false;
        });
    });
    $("a.infos_current").each(function() {
        var $id = $(this).attr('id');
        var $dialog = $("#flight"+$id)
            .dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                width: 750,
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery($dialog).dialog('close');
                    })
                }
            });
        $(this).click(function() {
            $dialog.dialog('open');
            return false;
        });
    });
});
/****** JQUERY UI DIALOG *******/

/****** CLICK FROM DATA-URL *******/
$('td[data-url]').click(function () {
    window.location.href = $(this).attr('data-url');
});
$('.pointer[data-url]').click(function () {
    window.location.href = $(this).attr('data-url');
});
/****** CLICK FROM DATA-URL *******/

/****** COPYRIGHT COPIED TEXT *******/
function addLink() {
    var body_element = document.getElementsByTagName('body')[0];
    var selection;
    selection = window.getSelection();
    var pagelink = ' Agentia de turism Ocaziituristice.ro <a href="http://www.ocaziituristice.ro" title="Agentie de turism">www.ocaziituristice.ro</a> ';
    var copytext = selection + pagelink;
    var newdiv = document.createElement('div');
    newdiv.style.position='absolute';
    newdiv.style.left='-99999px';
    body_element.appendChild(newdiv);
    newdiv.innerHTML = copytext;
    selection.selectAllChildren(newdiv);
    window.setTimeout(function() {
        body_element.removeChild(newdiv);
    },0);
}
/****** COPYRIGHT COPIED TEXT *******/

function toggleDiv(divId) {
    $("#"+divId).toggle();
}