<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/**
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See https://github.com/mrclay/minify/blob/master/docs/CustomServer.wiki.md for other ideas
 **/

return array(
//    'testJs' => array('//minify/quick-test.js'),
//    'testCss' => array('//minify/quick-test.css'),
'js_ocazii'=> array('//js/ocazii_main.js', '//js/ajaxpage.js','//js/jquery.rating.js'),
'js_ocazii_homepage'=> array( '//js_responsive/ocaziituristice.js','//js_responsive/svginject.js'),
'js_ocazii_responsive'=> array('//js_responsive/datepicker_ro.js', '//js_responsive/typeahead.bundle.js','//js_responsive/bootstrap-scrollspy-3.3.7.min.js','//js_responsive/handlebars-v4.0.5.js','//js_responsive/jquery-deparam.min.js', '//js_responsive/jquery.dotdotdot.min.js', '//js_responsive/jquery.jscroll.js', '//js_responsive/svginject.js','//js_responsive/ocaziituristice.js'),
//    'js' => array('//js/file1.js', '//js/file2.js'),
'css_ocazii_extra' => array('//css_responsive/font-awesome.min.css', '//css_responsive/jquery-ui/jquery-ui.min.css','//css_responsive/jquery-ui/jquery-ui.structure.min.css','//css_responsive/jquery-ui/jquery-ui.theme.min.css','//js_responsive/jquery_validate/validationEngine.jquery.css','//css_responsive/jquery-ui-custom.css',),

'css_ocazii_main' =>array('//css_responsive/ocaziituristice.css','//css_responsive/test_1.css'), 
);

