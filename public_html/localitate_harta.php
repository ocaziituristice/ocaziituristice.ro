<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$id_localitate = get_id_localitate(desfa_link($_REQUEST['localitate']));

$sel_loc = "SELECT localitati.*, zone.denumire AS den_zona, tari.denumire AS den_tara
FROM localitati
LEFT JOIN zone ON zone.id_zona = localitati.id_zona
LEFT JOIN tari ON tari.id_tara = zone.id_tara
WHERE id_localitate = '".$id_localitate."' ";
$que_loc = mysql_query($sel_loc) or die(mysql_error());
$row_loc = mysql_fetch_array($que_loc);
$param['latitudine'] = $row_loc['latitudine'];
$param['longitudine'] = $row_loc['longitudine'];

/*$selSEO="SELECT * FROM seo WHERE id_tip = '0' AND id_oras = '".$id_localitate."' ";
$queSEO=mysql_query($selSEO) or die(mysql_error());
$rowSEO=mysql_fetch_array($queSEO);
@mysql_free_result($queSEO);
if($rowSEO['h1']) $titlu_pag=$rowSEO['h1'];
if($rowSEO['h2']) $titlu_h2=$rowSEO['h2'];
if($rowSEO['h3']) $titlu_h3=$rowSEO['h3'];
if($rowSEO['cuvant_cheie']) $cuvant_cheie=$rowSEO['cuvant_cheie'];

if($rowSEO['title_seo']=="") {
	$metas_title = 'Oferte '.$den_loc.' - '.$den_tara.' '.date('Y').' | '.$denumire_agentie;
} else {
	$metas_title = $rowSEO['title_seo'];
}
if($rowSEO['description']=="") {
	$metas_description = 'Oferte '.$den_loc.' '.date('Y'). ' in '.$den_tara.' la cele mai avantajoase tarife pentru vacanta ta in '.$den_loc.', '.$den_zona.' oferite de '.$denumire_agentie;
} else {
	$metas_description = $rowSEO['description'];
}
if($rowSEO['keywords']=="") {
	$metas_keywords = $cuvant_cheie. ', sejur '.$den_loc.', cazare '.$den_loc.', oferte '.$den_loc;
} else {
	$metas_keywords = $rowSEO['keywords'];
}*/
$metas_title = 'Hoteluri '.$row_loc['denumire'].' din '.$row_loc['den_tara'].' | '.$denumire_agentie;
$metas_description = 'Hoteluri '.$row_loc['denumire'].' aflate in zona '.$row_loc['den_zona'].' din '.$row_loc['den_tara'].'. La '.$denumire_agentie.' gasiti cele mai bune oferte la hotelurile din '.$row_loc['denumire'];
$metas_keywords = 'hoteluri '.$row_loc['denumire']. ', hoteluri '.$row_loc['denumire']. ' din '.$row_loc['den_zona'].', hoteluri '.$row_loc['denumire']. ' din '.$row_loc['den_tara'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?></title>
<meta name="description" content="<?php echo $metas_description; ?>" />
<meta name="keywords" content="<?php echo $metas_keywords; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="initialize(); load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">

<div id="NEW-destinatie">

  <h1 class="blue float-left">Hoteluri <?php echo $row_loc['denumire']; ?></h1>
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  
  <br class="clear"><br>
  
  <div id="map_canvas" class="gmaps"></div>
  <div id="side_bar" class="gmaps_side"></div>
  
  <br class="clear"><br>
  
<?php
$oferte=new AFISARE_SEJUR_NORMAL();
$oferte->setAfisare($from, $nr_pe_pagina);
$oferte->config_paginare('nou');
$oferte->setTari(fa_link($row_loc['den_tara']));
$oferte->setZone(fa_link($row_loc['den_zona']));
$oferte->setOrase(fa_link($row_loc['denumire']));

$filtruOf=$oferte->get_filtru_mare();

$link = '/sejur-'.fa_link($row_loc['den_tara']).'/'.fa_link($row_loc['den_zona']).'/'.fa_link($row_loc['denumire']).'/';
?>

  <div class="linkuri-diverse clearfix">
	<div class="float-left" style="width:750px;">
      <h3>Hoteluri <?php echo $row_loc['denumire']; ?></h3>
      <ul>
<?php
if(sizeof($filtruOf['stele'])>0) {
	krsort($filtruOf['stele']);
	foreach($filtruOf['stele'] as $i=>$nr_s) {
		echo '<li><a href="'.$link.'?optiuni=da&amp;stele='.fa_link($i).'" class="link-black float-left" style="width:240px;">Hoteluri de '.$i.' stele din '.$row_loc['denumire'].' </a></li>';
	}
}

if(sizeof($filtruOf['masa'])>0) {
	ksort($filtruOf['masa']);
	foreach($filtruOf['masa'] as $keyM=>$valueM) {
		foreach($valueM as $denM=>$nr_m) {
			echo '<li><a href="'.$link.'?optiuni=da&amp;masa='.fa_link($denM).'" class="link-black float-left" style="width:240px;">Hoteluri '.$row_loc['denumire'].' cu '.$denM.'</a></li>';
		}
	}
}

if(sizeof($filtruOf['facilitati'])>0) {
	krsort($filtruOf['facilitati']);
	foreach($filtruOf['facilitati'] as $denF=>$nr_f) {
		echo '<li><a href="'.$link.'?optiuni=da&amp;facilitati='.fa_link($denF).'" class="link-black float-left" style="width:240px;">Hoteluri '.$row_loc['denumire'].' cu '.$denF.'</a></li>';
	}
}
?>
      </ul>
    </div>
    <div class="float-left" style="width:230px;">
      <h3>Linkuri utile <?php echo $row_loc['denumire']; ?></h3>
      <ul>
        <li><a href="<?php echo $link; ?>" class="link-black" title="Sejur <?php echo $row_loc['denumire']; ?>">Sejur <?php echo $row_loc['denumire']; ?></a></li>
      </ul>
    </div>
  </div>

  <br class="clear"><br>
  
</div>

    </div>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>
