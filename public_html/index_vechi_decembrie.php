<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/test/openmanagerdemo/jscripts/tiny_mce/plugins/xhtmlxtras/functions.php';
include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Agentie de turism online - disponibilitate in timp real | Ocaziituristice.ro</title>
<meta name="description" content="Mergi la sigur cu Ocaziituristice.ro. Agentie de turism din Bucuresti cu o oferta variata pentru pachete turistice in Romania si externe. Profita acum de reduceri" />
<meta name="keywords" content="agentie de turism, sejururi, last minute, early booking, circuite, oferte revelion, city break, oferte balneo" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()" class="test">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner" style="padding:10px 0 0 0;">
    <div class="NEW-column-full">
      <div id="NEW-index" class="clearfix">
      
        <?php /*?><div class="tmain blue"><h1>Agentia de turism</h1>
         DREAM VOYAGE va pune la dispozitie pe <strong class="black">www.ocaziituristice.ro</strong> o gama variata de pachete turistice.</div><?php */?>
        
        <div class="NEW-search NEW-round8px">
		  <div class="chapter-title white">Cauta vacanta:</div>
          <div id="afis_filtru"></div>
        </div>
        
        <div class="float-right" style="width:720px; height:285px;">
        <div id="jflowSlider" class="NEW-round8px">
          <div id="mySlides">
          
            <div id="slide0" class="slide">
              <a href="https://www.ocaziituristice.ro/sejur-romania/litoral/"><img src="/images/index/banners_030414_romania_litoral.jpg" alt="Litoral romania" class="NEW-round8px" /></a>
            </div>  

            <div id="slide1" class="slide">
              <a href="https://www.ocaziituristice.ro/sejur-turcia/"><img src="/images/index/banners_030414_antalya.jpg" alt="Sejur Turcia" class="NEW-round8px" /></a>
            </div>  

            <div id="slide2" class="slide">
              <a href="https://www.ocaziituristice.ro/sejur-grecia/insula-santorini/"><img src="/images/index/banners_030414_santorini.jpg" alt="Sejur Santorini" class="NEW-round8px" /></a>
            </div>  

            <div id="slide3" class="slide">
              <a href="/oferte-program-pentru-seniori/"><img src="/images/index/banners_25092014_seniori.jpg" alt="Oferte Seniori" class="NEW-round8px" /></a>
            </div>  

            <div id="slide4" class="slide">
              <a href="/sejur-dubai/"><img src="/images/index/banners_25092014_dubai.jpg" alt="Sejur Dubai" class="NEW-round8px" /></a>
            </div>  


            <?php /*?><div id="slide5" class="slide">    
              <a href="/oferte-paste/bulgaria/"><img src="/images/index/banner_paste-bulgaria_03-01-2012.jpg" alt="Paste Bulgaria" class="NEW-round8px" /></a>
              <div class="slideContent">
                <h3>Super Oferte de Paste !</h3>
                <p>Petrece Sarbatorile Pascale pe litoralul Bulgaresc.</p>
              </div>
            </div><?php */?>

          </div>
          <div id="myController">
            <span class="jFlowControl">1</span>
            <span class="jFlowControl">2</span>
            <span class="jFlowControl">3</span>
            <span class="jFlowControl">4</span>
            <span class="jFlowControl">5</span>
          </div>
          <div class="jFlowPrev"></div>
          <div class="jFlowNext"></div>
        </div>
        </div>
		
		<br class="clear">
		
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate_index.php"); ?>
		
		<br class="clear">
        <h1 class="blue" align="center">Ocaziituristice.ro - Agentie de turistim - Disponibilitate si tarife in timp real</h1>
        <br class="clear">
        <?php //include_once("includes/index/revelion_romania.php"); ?>
		<?php //include_once("includes/index/craciun_romania.php"); ?>
		<?php include_once("includes/index/litoral_romania.php"); ?>
        		<?php include_once("includes/index/charter_grecia.php"); ?>

		<?php //include_once("includes/index/litoral_bulgaria.php"); ?>
        <?php include_once("includes/index/individual_grecia.php"); ?>
		<?php //include_once("includes/index/revelion_bulgaria.php"); ?>
		<?php //include_once("includes/index/paste_2014.php"); ?>
		
		<br class="clear">
		
		<a href="/sejur-dubai/" title="Sejur Dubai"><img src="/images/index/dubaibanner.jpg" alt="Charter Dubai" class="NEW-round8px" style="margin:0 0 10px 0;" align="Sejur Dubai"></a>
		
		<br class="clear">
        <?php include_once("includes/index/statiuni_balneare.php"); ?>
		<?php include_once("includes/index/programe_seniori.php"); ?>
        <?php //include_once("includes/index/sejur_egipt.php"); ?>

		<?php include_once("includes/index/sejur_turcia.php"); ?>
        
        <?php //include_once("includes/index/sejur_spania.php"); ?>
		<?php //include_once("includes/index/litoral_romania.php"); ?>
		<?php //include_once("includes/index/autocar_grecia.php"); ?>
		<?php //include_once("includes/index/individual_grecia.php"); ?>
		
		<br class="clear">
		
		<?php //include_once("includes/index/craciun_romania.php"); ?>
		<?php //include_once("includes/index/piata_de_craciun.php"); ?>
		<?php //include_once("includes/index/last_minute.php"); ?>
		
		<?php /*?><br class="clear"><?php */?>
		
		<?php //include_once("includes/index/city_break.php"); ?>
        
        <br class="clear">
		
		<a href="/circuite/israel/" title="Circuite Israel"><img src="/images/index/banner_circuite-israel-index_10-09-2013.jpg" alt="Circuite Israel" class="NEW-round8px" style="margin:0 0 10px 0;"></a>
		
		<br class="clear">
		
<?php /*?><?php
$sel_circuite = "SELECT
oferte.id_oferta,
oferte.denumire AS denumire_oferta,
oferte.denumire_scurta AS denumire_oferta_scurta,
oferte.exprimare_pret,
oferte.nr_zile,
oferte.nr_nopti,
oferte.masa,
oferte.pret_minim,
oferte.moneda,
hoteluri.id_hotel,
hoteluri.poza1,
hoteluri.nume AS denumire_hotel,
hoteluri.stele,
hoteluri.tip_unitate,
continente.nume_continent,
tari.denumire AS denumire_tara,
zone.denumire AS denumire_zona,
localitati.denumire AS denumire_localitate,
transport.denumire AS denumire_transport,
traseu.tari_vizitate
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
INNER JOIN transport ON oferte.id_transport = transport.id_trans
LEFT JOIN (SELECT GROUP_CONCAT(DISTINCT tari.denumire ORDER BY tari.denumire ASC SEPARATOR ', ') AS tari_vizitate, traseu_circuit.id_hotel_parinte FROM traseu_circuit INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara WHERE traseu_circuit.tara_principala = 'da' GROUP BY traseu_circuit.id_hotel_parinte) AS traseu ON hoteluri.id_hotel = traseu.id_hotel_parinte
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
GROUP BY oferte.id_hotel
ORDER BY rand()
LIMIT 0,8 ";
$que_circuite = mysql_query($sel_circuite) or die(mysql_error());

if(mysql_num_rows($que_circuite)>0) {
	$ic = 0;
?>
        <div class="chenar chn-color-grey NEW-round8px">
          <div class="inner NEW-round6px clearfix">
            <a href="/circuite/" class="float-right link-black" rel="nofollow">vezi toate Circuitele</a>
            <h3 class="black underline">Circuite</h3>
            <br class="clear">
            <div class="ofVizitate clearfix">
	<?php while($row_circuite = mysql_fetch_array($que_circuite)) {
		$ic++;
		$link_circuit = make_link_circuit($row_circuite['denumire_hotel'], $row_circuite['id_oferta']);
		$tari_vizitate = explode(",",$row_circuite['tari_vizitate']);
	?>
			<div class="prod clearfix pointer" style="height:255px;" data-url="<?php echo $link_circuit; ?>">
			  <span class="poza2 NEW-round4px" style="background-image:url(<?php echo '/img_mediu_hotel/'.$row_circuite['poza1']; ?>);"><span class="country"><?php echo $tari_vizitate[0]; ?></span></span>
			  <div class="titlu" style="margin-top:0; padding-top:3px;">
				<a href="<?php echo $link_circuit; ?>" class="link-blue" title="<?php echo $row_circuite['denumire_oferta']; ?>"><?php echo $row_circuite['denumire_oferta']; ?></a>
			  </div>
			  <div class="underpic">
				<div><?php echo $row_circuite['nr_zile'].' zile, '.$row_circuite['masa'].', '.$row_circuite['denumire_transport']; ?></div>
			  </div>
			  <div class="price red bold text-center">de la <span><?php echo new_price($row_circuite['pret_minim']).' '.moneda($row_circuite['moneda']); ?></span> pe <?php echo $row_circuite['exprimare_pret']; ?></div>
              <div class="btn-detalii"><img src="/images/but_nou_detalii.png" alt="vezi detalii"></div>
			</div>
			<?php if($ic%4==0 and $ic!=8) echo '<br class="clear"><hr class="product-separator">'; ?>
	<?php } ?>
            </div>
          </div>
        </div>
<?php } ?><?php */?>

<?php
$sel_circuite = "SELECT
MIN(oferte.pret_minim) AS pret_minim,
COUNT(oferte.id_oferta) AS numar,
tari.denumire,
tari.id_tara,
tari.info_scurte,
continente.nume_continent
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
INNER JOIN tari ON traseu_circuit.id_tara = tari.id_tara
INNER JOIN continente ON continente.id_continent = tari.id_continent
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Circuit'
AND traseu_circuit.tara_principala = 'da'
GROUP BY tari.denumire
ORDER BY numar DESC, tari.denumire ASC
LIMIT 0,4 ";
$que_circuite = mysql_query($sel_circuite) or die(mysql_error());

if(mysql_num_rows($que_circuite)>0) {
	$ic = 0;
?>
        <div class="chenar chn-color-grey NEW-round8px">
          <div class="inner NEW-round6px clearfix">
            <a href="/circuite/" class="float-right link-black" rel="nofollow">vezi toate Circuitele</a>
            <h3 class="black underline">Circuite</h3>
            <br class="clear">
            <div class="ofVizitate clearfix">
	<?php while($row_circuite = mysql_fetch_array($que_circuite)) {
		$ic++;
		$link_circuit = '/circuite/'.fa_link($row_circuite['denumire']).'/';
	?>
			<div class="prod clearfix pointer" style="height:235px;" data-url="<?php echo $link_circuit; ?>">
			  <a href="<?php echo $link_circuit; ?>" title="Circuit <?php echo $row_circuite['denumire']; ?>"><span class="poza2 NEW-round6px" style="background-image:url(/circuite/images/<?php echo fa_link($row_circuite['denumire']); ?>.jpg); height:150px;"><span class="country"><?php echo $row_circuite['denumire']; ?></span></span></a>
              <div class="bigger-13em text-center" style="padding:4px 0;"><span class="underline bold blue">Circuite <?php echo $row_circuite['denumire']; ?></span></div>
			  <div class="price bold text-center">de la <span class="red"><?php echo new_price($row_circuite['pret_minim']).' &euro;'; ?></span> /pers/sejur</div>
              <div class="btn-detalii"><img src="/images/but_nou_detalii.png" alt="vezi detalii"></div>
			</div>
			<?php if($ic%4==0 and $ic!=8) echo '<br class="clear"><hr class="product-separator">'; ?>
	<?php } ?>
            </div>
          </div>
        </div>
<?php } ?>

        <br class="clear">

        <div class="float-left" style="width:320px;">
          <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix" style="height:180px;">
            <div id="abonare_newsletter"></div>
			<?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_form.php"); ?>
          </div></div>
        </div>
        
        <div class="fb-like-box float-left NEW-round6px" style="width:318px; margin:0 10px; border:1px solid #CCC;" data-href="<?php echo $facebook_page; ?>" data-width="318" data-height="215" data-show-faces="true" data-stream="false" data-show-border="false" data-header="true"></div>
        
        <div class="float-left" style="width:320px;">
          <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix">
           <!-- <h3 class="black"><a href="http://blog.ocaziituristice.ro" target="_blank">Blog Ocaziituristice.ro</a></h3>-->
<?php /*?>
<?php 
require_once('magpierss/rss_fetch.inc');
$url = 'http://blog.ocaziituristice.ro/feed';
$rss = fetch_rss($url);
//echo "Site: ", $rss->channel['title'], "<br>";

$sf=sizeof($rss->items);
for($i=0;$i<$sf-8;$i++) {

    $item = $rss->items[$i];
	//print_r($item);
	$title = $item[title];
	$url = $item[link];
	$desc = $item[description];
	$date = date('d M Y, H:i',$item[date_timestamp]);
?>
<div class="item3">
  <a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="link-blue bold" target="_blank"><?php echo $title; ?></a>
  <div class="desc"><?php echo truncate_str($desc, 90).' [...]'; ?></div>
  <div class="date"><?php echo $date; ?></div>
</div>
<?php } ?><?php */?>
          </div></div>
        </div>
        
        <br class="clear">
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script type="text/javascript">
$("#afis_filtru").load("filtru.php");
</script>
<script src="/js/jflow.plus.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#myController").jFlow({
		controller: ".jFlowControl",
		slideWrapper: "#jFlowSlider",
		slides: "#mySlides",
		selectedWrapper: "jFlowSelected",
		effect: "flow",
		width: "720px",
		height: "250px",
		duration: 1,
		pause: 4000,
		prev: ".jFlowPrev",
		next: ".jFlowNext",
		auto: true
    });
});
</script>
<script type="text/javascript">
/* Facebook */
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=314711018608962";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>