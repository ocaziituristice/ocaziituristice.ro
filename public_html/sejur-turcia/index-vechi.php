<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$den_tara = 'Turcia';
$id_tara = get_id_tara($den_tara);

$link_tara = fa_link_oferta($den_tara);

if(str_replace("/sejur-","",$_SERVER['REQUEST_URI'])==$link_tara."/") $img_path = 'images/';
	else $img_path = '/sejur-'.$link_tara.'/images/';

$sel_dest = "SELECT
MIN(oferte.pret_minim / oferte.nr_nopti) AS pret_minim,
COUNT(distinct oferte.id_hotel) AS numar,
zone.id_zona,
zone.denumire AS denumire_zona,
zone.info_scurte
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '".$id_tara."'
AND oferte.last_minute = 'nu'
GROUP BY denumire_zona
ORDER BY numar DESC, denumire_zona ASC ";
$rez_dest = mysql_query($sel_dest) or die(mysql_error());
while($row_dest = mysql_fetch_array($rez_dest)) {
	$destinatie['den_zona'][] = $row_dest['denumire_zona'];
	$destinatie['numar_hoteluri'][] = $row_dest['numar'];
	$destinatie['link'][] = '/sejur-'.fa_link($den_tara).'/'.fa_link($row_dest['denumire_zona']).'/';
	$destinatie['link_noindex'][] = '/sejururi/'.$id_tara.'/'.$row_dest['id_zona'].'/';
	$destinatie['id_zona'][] = $row_dest['id_zona'];
	$destinatie['info_scurte'][] = $row_dest['info_scurte'];
	$destinatie['pret_minim'][] = round($row_dest['pret_minim']);
} @mysql_free_result($rez_dest);
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Sejur Turcia | Ocaziituristice.ro</title>
<meta name="description" content="Sejur Turcia la cele mai luxoase si ravnite hoteluri de pe litoralul Turciei la preturi foarte avantajoase cu all inclusive" />
<link rel="canonical" href="<?php echo '/sejur-'.$link_tara.'/'; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Sejur Turcia</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?>

        <br class="clear">
        <?php /*?><div class="Hline"></div>
        
		<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>
        <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
        
        <br class="clear"><?php */?>
        
		<?php $tipuri = get_tiputi_oferte('', '', '', $id_tara, '', '');
		$link_new = $link_tara.'/'; ?>
        <ul class="filterButtonsTop clearfix">
          <li><a href="<?php echo '/sejur-'.$link_new; ?>" class="sel">Toate</a></li>
		<?php $i=0;
		if(sizeof($tipuri)>0) {
			foreach($tipuri as $key_t=>$value_t) {
				$i++;
				$valTip = fa_link($value_t['denumire']);
				if($value_t) $nrO=$value_t; else $nrO=0;
		?>
          <li><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" title="<?php echo $value_t['denumire']; ?>"><?php echo $value_t['denumire']; ?></a></li>
		<?php }
		} ?>
          <li><a href="<?php echo '/sejur-'.$link_tara.'/?optiuni=da&transport=avion'; ?>" title="Charter avion <?php echo $den_tara; ?>">Charter <?php echo $den_tara; ?></a></li>
          <li><a href="<?php echo '/sejur-'.$link_tara.'/?optiuni=da&transport=fara-transport'; ?>" title="<?php echo $den_tara; ?> individual"><?php echo $den_tara; ?> individual</a></li>
          <li><a href="<?php echo '/sejur-'.$link_tara.'/?optiuni=da&masa=all-inclusive'; ?>" title="<?php echo $den_tara; ?> all inclusive"><?php echo $den_tara; ?> all inclusive</a></li>
          <li><a href="<?php echo '/sejur-'.$link_tara.'/?optiuni=da&stele=5'; ?>" title="Hoteluri de 5 stele din <?php echo $den_tara; ?>">Hoteluri <?php echo $den_tara; ?> 5*</a></li>
        </ul>

        <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <div id="destinations" class="clearfix">
          <?php foreach($destinatie['den_zona'] as $key1 => $value1) {
			  if($value1 != 'Istanbul') { ?>
          <div class="destItem NEW-round4px clearfix" onclick="javascript:location.href='<?php echo $destinatie['link'][$key1]; ?>'">
            <div class="topBox">
              <img src="<?php echo $img_path.fa_link($destinatie['den_zona'][$key1]); ?>.jpg" alt="<?php echo $destinatie['den_zona'][$key1]; ?>">
              <span class="over"></span>
              <i class="icon-search white"></i>
              <h2><?php echo $destinatie['den_zona'][$key1]; ?></h2>
            </div>
            <?php if($destinatie['info_scurte'][$key1]) { ?><div class="middleBox"><?php echo $destinatie['info_scurte'][$key1]; ?></div><?php } ?>
            <div class="bottomBox clearfix">
            
              <div class="price" align="center"><span class="red"><span class="pret"><?php echo $destinatie['numar_hoteluri'][$key1]; ?></span> </span> Hoteluri pentru</div>
              <div  class="clearfix" align="center" ><a href="<?php echo $destinatie['link'][$key1]; ?>" title="Sejur <?php echo $destinatie['den_loc'][$key1]; ?>" class="link-blue block">Sejuri in <strong><?php echo $destinatie['den_zona'][$key1] ?> </strong></a></div>
             <?php /*?>  
              <div class="details">
             <a href="<?php echo $destinatie['link_noindex'][$key1]; ?>" rel="nofollow noindex"><img src="/images/but_nou_detalii.png" alt="detalii" width="90"></a>
              </div><?php */?>
            </div>
          </div>
          <?php }
		  } ?>
        </div>
        <?php } ?>
        
        <br>
        
        <div class="clearfix">
		  <a href="/sejur-turcia/istanbul/" title="Shopping Istanbul"><img src="<?php echo $img_path; ?>shopping-istanbul.jpg" alt="Shopping Istanbul" class="destItem NEW-round4px" style="width:310px !important;"></a>
		  <a href="/circuite/turcia/" title="Circuit Turcia"><img src="<?php echo $img_path; ?>circuit-turcia.jpg" alt="Circuit Turcia" class="destItem NEW-round4px" style="width:310px !important;"></a>
		  <a href="/hoteluri-istanbul/hotel-buyuk-sahinler/seniori-istanbul-14004.html" title="Seniori Istanbul"><img src="<?php echo $img_path; ?>seniori-turcia.jpg" alt="Seniori Turcia" class="destItem NEW-round4px" style="width:310px !important;"></a>
        </div>
		
        <div class="pad10">
          <img src="<?php echo $img_path; ?>icon-sejur-turcia.jpg" alt="Sejur Turcia" class="float-left" style="margin:10px 10px 0 0;">
          <h2><strong>Sejur Turcia</strong> - vacanţa perfectă în timpul verii</h2>
          <p class="newText text-justify">Pentru orice turist, un sejur Turcia transformă un vis într-o realitate.<br>
          Turcia este o țară întinsă pe două continente. 97% din suprafața țării se află în Asia (Anatolia) și 3% în Europa (peninsula Balcanică). Este considerată un amestec de peisaje superbe, ireale, cu arhitecturi ca din poveşti şi panorame ce par din alte lumi. Staţiuni cu plaje scăldate în soare, lacuri, munţi şi apa mării de culoare turcoaz, îi duc pe turişti pe tărâmul magiei şi al fanteziei.</p>
          
          <br class="clear">
          
          <p class="newText text-justify">O schimbare majoră faţă de viaţa de zi cu zi se poate obţine într-un <em>sejur Turcia</em>. Renumită pentru sezonul de vară care începe aici cam pe la sfârşitul lunii aprilie şi ţine aproape până la sfârşitul lui octombrie, Turcia însumează aproape 8000 de kilometri presăraţi cu staţiunile turistice situate pe litoralul nisipos al Mării Egee şi al Mării Mediterane care sunt vizitate anual de milioane de turişti străini.</p>
          <p class="newText text-justify">Şi pentru că o vizită nu este de ajuns în această ţară minunată, vizitatorii caută în permanenţă <strong>oferte sejur Turcia</strong> ca să revină iar şi iar să mai descopere câte un colţişor din acest rai al vacanţelor.</p>
          <p class="newText text-justify">Amabilitatea şi ospitalitatea pe care locuitorii săi o arată faţă de turiştii străini este ireproşabilă. Zicala &quot;clientul nostru este stăpânul nostru&quot; li se potriveşte perfect. Au o tradiţie în oferirea de servicii turistice impecabile.</p>
          <p class="newText text-justify">Pentru români, Turcia este o destinaţie atractivă datorită pachetelor all-inclusive, pachete accesibile majorităţii populatiei. Dar ce înseamnă un sejur Turcia all inclusive? Băuturi locale nelimitate. Toate mesele incluse (mic dejun, pranz şi cina). Gustări, băuturi non-alcoolice, cafea, ceai la barurile hotelului. Evenimente organizate de hotel în fiecare seara. Zeci de activităţi pentru distracţie sau petrecerea timpului liber.</p>
          <p class="newText text-justify">Dacă optaţi pentru o vacanţă relaxantă, cu soare, plajă, sport, piscină, shopping şi cu mici excursii, atunci &quot;all inclusive&quot; este alegerea perfectă atât în ceea ce priveşte satisfacţia cât şi preţul.</p>
          <p class="newText text-justify">De ce caută turiştii oferte sejur Turcia? Pentru că Turcia înseamnă mai mult decât hoteluri luxoase și piscine imense, mâncare şi băutură tradiţională la discreţie şi distracţie la saună, masaj, acces la băi turceşti, sală de fitness, cameră de jocuri, biliard, tenis de masă, dart sau zonă de joacă pentru copii în imediata apropiere a hotelului.</p>
          <p class="newText text-justify">Turiştii pot să exploreze cascade, temple, vegetaţie sălbatică, munți înalţi în estul ţării, câmpii fertile în nord-vest şi sud-est şi văile mari cu râuri în vest.</p>
          <p class="newText text-justify">Fiecare loc are farmecul lui aparte, Turcia fiind recomandată cuplurilor în căutare de relaxare la spa urmată de distracţia din cluburi, sau celor care caută aventura şi sunt fascinaţi să viziteze cele Şapte Minuni ale Lumii Antice, temple şi moschee, sau familiilor cu copii cărora hotelurile le oferă extrem de multe facilităţi.</p>
        </div>

        <br class="clear">
        
       <?php /*?> <?php if(sizeof($destinatie['den_zona'])>0) { ?>
        <h3 class="mar10"><?php echo $den_tara; ?> - sejururi și cazări disponibile</h3>
        <ul class="newText pad0 mar0 clearfix">
          <?php foreach($destinatie['den_zona'] as $key2 => $value2) {
			  $part_anchor = 'Sejur ';
		  ?>
          <li class="float-left w220 pad0-10 block"><a href="<?php echo $destinatie['link'][$key2]; ?>" title="<?php echo $part_anchor.$value2; ?>" class="link-blue block"><?php echo $part_anchor.$value2; ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?><?php */?>
        
        <br class="clear"><br><br>
        
        <div class="pad10"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?></div>
        
        <br class="clear"><br>
        
      </div>

    </div>
    <br>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>