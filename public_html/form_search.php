<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $sitepath; ?>autocomplete/jquery.autocomplete.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $sitepath; ?>autocomplete/jquery.autocomplete.css" />
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/datepicker_ro.js"></script>
<script type="text/javascript">
$().ready(function() {
	$("#course").autocomplete("<?php echo $sitepath; ?>autocomplete_nou/get_course_list.php", {
		width: 260,
		matchContains: true,
	    //mustMatch: true,
		minChars: 3,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
 $("#course").result(function(event, data, formatted) {
  $("#course_val").val('da');
 });
 
$("#data_start").datepicker({
	numberOfMonths: 3,
	dateFormat: "yy-mm-dd",
	showButtonPanel: true
});

$("#data_end").datepicker({
	numberOfMonths: 3,
	dateFormat: "yy-mm-dd",
	minDate: (new Date(document.getElementById('data_start').value)),
	showButtonPanel: true
});	
});
</script>

<div style="border:1px solid #CCC; background:#f2f2f2; padding:5px;">
  <strong>Cautare:</strong>
  <div id="content">
	<?php if($_REQUEST['cuvinte']) { $cuv=explode("~", trim(strtolower($_REQUEST['cuvinte']))); } else $cuv=''; ?>
  <form name="auto" method="post" action="/autocomplete_nou/redirect.php">
    <input type="text" name="course" id="course" value="<?php if($cuv) echo $_REQUEST['cuvinte']; else echo "Alege Tara, Zona sau Localitatea"; ?>" onblur="if(this.value=='') this.value='Alege Tara, Zona sau Localitatea';" onfocus="if(this.value=='Alege Tara, Zona sau Localitatea') this.value='';" style="width:280px; margin-bottom:5px;" />
    <input type="hidden" name="course_val" id="course_val" value="<?php if($cuv) echo 'da'; else echo 'nu'; ?>" />
    <div style="float:left; margin-right:10px; font-size:13px; font-weight:bold;">Perioada:</div>
    <input type="text" name="data_start" id="data_start" value="<?php if($_REQUEST['start-date']) echo $_REQUEST['start-date']; ?>" style="width:80px; float:left;" />
    <input type="text" name="data_end" id="data_end" value="<?php if($_REQUEST['end-date']) echo $_REQUEST['end-date']; ?>" style="width:80px; float:left;" />
    <br class="clear" />
    <input type="image" src="<?php echo $imgpath; ?>/buton_cauta.png" onclick="document.auto.submit();" style="margin-top:5px;" />
  </form>     
  </div>
</div>
<br/><br/>

<?php if($_REQUEST['start-date'] && $_REQUEST['end-date'] || $cuv) {
$afisare =new AFISARE_SEJUR();
$afisare->set_oferte_pagina(10);
$afisare->setSearch('da');
   if($cuv[2]) { $oras=array(); $oras=fa_link($cuv[2]); $afisare->setOrase($oras); }
   if($cuv[1]) { $zona=array(); $zona=fa_link($cuv[1]); $afisare->setZone($zona); }
  if($cuv[0]) { $tara=fa_link($cuv[0]); $afisare->setTari($tara);
 $afisare->setTaraCircuit($tara); }
if($_REQUEST['start-date']) $afisare->setStartDate($_REQUEST['start-date']);
if($_REQUEST['end-date']) $afisare->setEndDate($_REQUEST['end-date']);
if($_POST['tip_pret']) $afisare->setOrdonarePret($_POST['tip_pret']);
if($_POST['tip_stele'] && !$_REQUEST['stele']) $afisare->setOrdonareStele($_POST['tip_stele']);
if($_POST['tip_relevanta']) $afisare->setOrdonareRelevanta($_POST['tip_relevanta']);
$afisare->paginare(); } ?>
<div class="spacer"><img src="/images/spacer.gif" width="1" height="6" alt="" /></div>