<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');

$nume_hotel = desfa_link($_REQUEST['nume']);
//echo $nume_hotel = strtr($nume_hotel,'_w_','and');
//desfa_link($_REQUEST['nume'])
$id_hotel = get_idhotel($nume_hotel, desfa_link($_REQUEST['localitate']));
//echo $_REQUEST['localitate'];
$id_hotel=10202;
$det = new DETALII_HOTEL();
$detalii_hotel = $det->select_camp_hotel($id_hotel);
$det->hotel_vizitat($id_hotel);
$tip_pagina='offerdetail';

$offers = $det->get_oferte($id_hotel);
$nr_oferte = count($offers);

$id_oferta = '';
$den_hotel=ucwords($detalii_hotel['denumire']);
$den_tara=ucwords($detalii_hotel['tara']);
$den_zona=ucwords($detalii_hotel['zona']);
$den_localitate=ucwords($detalii_hotel['localitate']);
$hoteluri_localitate = hoteluri_localitate($detalii_hotel['id_localitate'],$id_hotel,$detalii_hotel['latitudine'],$detalii_hotel['longitudine']);

/*if($_REQUEST['localitate']<>fa_link_oferta($detalii_hotel['localitate']) || !$detalii_hotel['denumire'] || $detalii_hotel['tip_unitate']=='Circuit') {
	header("HTTP/1.0 404 Not Found");
	//header("Location: ".$sitepath.'404.php');
	$handle = curl_init($sitepath.'404.php');
	curl_exec($handle);
	exit();
}*/

if($detalii_hotel['titlu_seo']=="") {
	$metas_title = $detalii_hotel['denumire'].' din '.$detalii_hotel['localitate'].', '.$detalii_hotel['tara'];
} else {
	$metas_title = $detalii_hotel['titlu_seo'];
}
if($detalii_hotel['descriere_seo']=="") {
	$metas_description = $detalii_hotel['denumire'].' '.$detalii_hotel['localitate'].' '.$detalii_hotel['tara'].' - '.$detalii_hotel['stele'].' stele . Oferte si sejururi de cazare la '.$detalii_hotel['denumire'].' din '.$detalii_hotel['zona'].', '.$detalii_hotel['tara'];
} else {
	$metas_description = $detalii_hotel['descriere_seo'];
}

include($_SERVER['DOCUMENT_ROOT'].'/includes/hoteluri/reviews_top.php');
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $metas_title; ?> <?php if($nr_oferte<1)echo "0 oferte disponibile" ?></title>

<meta name="description" content="<?php echo $metas_description; ?>" />
<link rel="canonical" href="<?php echo $sitepath.'cazare-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'; ?>" />

<meta property="og:title" content="<?php echo $metas_title; ?>" />
<meta property="og:description" content="<?php echo $metas_description; ?>" />
<meta property="og:type" content="hotel" />
<meta property="og:url" content="<?php echo $sitepath.'hoteluri-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'; ?>" />
<meta property="og:image" content="<?php echo $sitepath.'img_mediu_hotel/'.$detalii_hotel['poza1']; ?>" />
<meta property="og:latitude" content="<?php echo $detalii_hotel['latitudine']; ?>" />
<meta property="og:longitude" content="<?php echo $detalii_hotel['longitudine']; ?>" />
<meta property="og:street-address" content="<?php echo $detalii_hotel['adresa']; if($detalii_hotel['nr']) echo ' nr. '.$detalii_hotel['nr']; ?>" />
<meta property="og:locality" content="<?php echo $detalii_hotel['localitate']; ?>" />
<meta property="og:region" content="<?php echo $detalii_hotel['zona']; ?>" />
<meta property="og:postal-code" content="<?php echo $detalii_hotel['cod_postal']; ?>" />
<meta property="og:site_name" content="<?php echo $denumire_agentie; ?>" />
<meta property="fb:admins" content="100000435587231" />
<meta property="fb:app_id" content="314711018608962" />

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_new_responsive.php'); ?>

</head>

<body>
<!--retargeting pag produs-->
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/meniu_header_responsive.php"); ?>

<div class="layout">

	<div class="breadcrumb">

		<a href=""><i class="icon-home"></i></a>

		<a href="">Sejur Romania</a>

		<a href="">Litoral</a>

		<a href="">Cazare Mamaia</a>

		Hotel Phoenicia Luxury

	</div>



	<div class="search-box">

		<!-- <i class="icon-search"></i> -->

		<input type="text" placeholder="Caută destinaţie/nume hotel din Mamaia">

	</div>

</div>



<div class="layout">

	<section class="hotel">

		<div class="clear">

			<div class="left-column">
            <?php  //if(!$err_logare_admin) { 
			//echo "<pre>";print_r($detalii_hotel);echo"</pre>"; //}?>

				<h1><?php echo $detalii_hotel['denumire']." ". $detalii_hotel['localitate']?> <span class="stars"><?php echo afiseaza_stele( $detalii_hotel['stele'])?></span></h1>



				<p class="address" data-scroll="map"><i class="icon-location"></i><?php  echo $detalii_hotel['adresa'].', '.$detalii_hotel['numar'].', '.$detalii_hotel['localitate'].', '.$detalii_hotel['tara']; ?> 
                
     <?php           if($detalii_hotel['distanta']) {
		echo ', <strong>Distanta până la '.$detalii_hotel['distanta_fata_de'].' - '.$detalii_hotel['distanta'].'m </strong> ';
    }?>
                
                </p>

			</div>

			<div class="right-column">

				<div class="recommended">

					<i class="icon-thumbs-up-alt"></i>

					<p>

						Recomandat pentru:

						<span><?php echo $detalii_hotel['concept']?></span>

					</p>

				</div>

			</div>

		</div>



		<div class="clear">

			<div class="left-column images">

				<div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-fit="cover" data-loop="true" data-ratio="1.7">
                
                <?php for($poza=1;$poza<=20;$poza++) {
					if(strlen($detalii_hotel['poza'.$poza])>0)  
				echo  '<a href="https://www.ocaziituristice.ro/img_prima_hotel/'.$detalii_hotel['poza'.$poza].'" data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/'.$detalii_hotel['poza'.$poza].'"></a>';	
					?>
                

                <?php }?>

					
				</div>

			
    <?php if((sizeof($detalii_online['early_time'])>0 or $detalii_hotel['data_early_booking']>date('Y-m-d')) and $detalii_online['last_minute']<1 ) {?> 	
                <div class="discount eb">

					Reducere <br />Early Booking 
         <?php if(is_array($detalii_online['early_disc'] ))
		 {$reduce_eb=max($detalii_online['early_disc']);} 
			if($reduce_eb<$detalii_hotel['comision_reducere'])
			{$reduce_eb=$detalii_hotel['comision_reducere'];}
			if ($reduce_eb>1)
				{echo date('d-m-Y',strtotime($detalii_hotel['data_early_booking'])).' <span> - '.$reduce_eb.'% </span> ';} ?>           
                    
                   

				</div>
<?php }?>
				<!-- <div class="discount lm">

					Last Minute <span>-20%</span>

				</div> -->

				<!-- <div class="discount bf">

					Black Friday <span>-20%</span>

				</div> -->

			</div>



			<div class="trust right-column"></div>

		</div>



		<h2 class="expand icon">Descriere <?php echo $detalii_hotel['denumire'].' '.$detalii_hotel['localitate']?></h2>

		<div class="expandable description">

			<div class="details">

				<p><?php echo nl2p($detalii_hotel['detalii_concept'])?></p>
                <p><?php echo nl2p($detalii_hotel['descriere_scurta'])?></p>
				<p><?php echo nl2p($detalii_hotel['descriere'])?></p>

			</div>
 
 
 
			<ul class="facilities">

				<li>

					<h3>General</h3>

				<?php echo nl2br($detalii_hotel['general'])?>	

				</li>

				<li>

					<h3>Servicii</h3>

					<?php echo nl2br($detalii_hotel['servicii'])?>	

				</li>

			</ul>

			<ul class="facilities-extra">

				<li>

					<h3>Internet</h3>

				<?php echo nl2br($detalii_hotel['internet'])?>	
				</li>

				<li>

					<h3>Parcare</h3>

					<?php echo nl2br($detalii_hotel['parcare'])?>	

				</li>

				<li>

					<h3>Check-in / check-out</h3>

					<?php echo ($detalii_hotel['check_in'])?>/ <?php echo nl2br($detalii_hotel['check_out'])?>

				</li>

				<li>

					<h3>Animale companie</h3>

					<?php echo nl2br($detalii_hotel['accepta_animale'])?>

				</li>

			</ul>


<div>Clasificarea pe stele a unităților de cazare din program este cea atribuită în mod oficial de Ministerul Turismului din <?php echo $detalii_hotel['tara']; ?> conform standardelor proprii.</div>

		</div>

<?php $camere=$det->camere_hotel($id_hotel);?>
<?php if(sizeof($camere)>0) {?>
		<h2 class="expand icon">Detalii camere <?php echo $detalii_hotel['denumire'].' '.$detalii_hotel['localitate']?></h2>

		<div class="expandable description">

			<ul class="room-details">


	<?php foreach ($camere as $camere_v) {?>
    			<li>

					<h3><?php  echo $camere_v['denumire_camera'];?></h3>

					<?php  echo nl2br($camere_v['descriere']);?>

				</li>
     <?php }?>           

				
			</ul>

		</div>

<?php }?>


		<h2 class="expand icon">Localizare pe harta <?php echo $detalii_hotel['denumire'].' '.$detalii_hotel['localitate']?>
        </h2>


		<div class="map" style="display: flex;">

			<div class="expandable" id="map"></div>

			<div class="nearby">

				<h3>Hoteluri din <?php echo $detalii_hotel['localitate']?> in apropiere</h3>



 <?php $hoteluri_localitate_1=multid_sort($hoteluri_localitate, 'distanta');
 //echo '<pre>';print_r($hoteluri_localitate_1);echo '</pre>';?>    
 <?php if(sizeof($hoteluri_localitate_1)>3) {?>

   
   <?php for ($id=1;$id<=5;$id ++)
   {
	   
echo "<a href=\"#\">

					<img src=\"".$sitepath."thumb_hotel/".$hoteluri_localitate_1[$id]['poza1']."\" alt=\"".$hoteluri_localitate_1[$id]['denumire']." ".$detalii_hotel['localitate']."\">

					<span class=\"title\">".$hoteluri_localitate_1[$id]['denumire']."</span>

					<span class=\"stars\">".afiseaza_stele($hoteluri_localitate_1[$id]['stele'])."</span>

					<span>".$hoteluri_localitate_1[$id]['distanta']." metri</span>

				</a>";

				
	 
	
	   
	   
   }
 }?> 

		</div>

		</div>

	</section>



	<section class="comments">

		

	</section>



	<section class="visited">

<?php 
$viewers = '';
$sel_views1 = "SELECT id_sesiune_vizitator FROM oferte_vizitate WHERE id_hotel = '".$id_hotel."' ";
$que_views1 = mysql_query($sel_views1) or die(mysql_error());
if(mysql_num_rows($que_views1)>0) {
while($row_views1 = mysql_fetch_array($que_views1)) {
	$viewers .= "'".$row_views1['id_sesiune_vizitator']."',";
}
$viewers = substr($viewers,0,-1);

$sel_views = "SELECT *
FROM oferte_vizitate
WHERE id_sesiune_vizitator IN (".$viewers.")
AND id_hotel <> '".$id_hotel."'
AND id_sesiune_vizitator <> '".session_id()."'
";
$que_views = mysql_query($sel_views) or die(mysql_error());

if(mysql_num_rows($que_views)>0) {
$id_hoteluri_viz = '';
while($row_views = mysql_fetch_array($que_views)) {
	$id_hoteluri_viz .= $row_views['id_hotel'].',';
}
$id_hoteluri_viz = substr($id_hoteluri_viz,0,-1);

$selOf="SELECT
oferte.id_oferta,
oferte.denumire,
oferte.denumire_scurta,
oferte.id_oferta,
oferte.nr_zile,
oferte.nr_nopti,
oferte.exprimare_pret,
oferte.pret_minim,
oferte.moneda,
oferte.masa,
transport.denumire AS denumire_transport,
hoteluri.nume,
hoteluri.detalii_concept,
hoteluri.stele,
hoteluri.id_hotel,
hoteluri.tip_unitate,
hoteluri.poza1,
zone.denumire as denumire_zona,
localitati.denumire as denumire_localitate,
tari.denumire as denumire_tara,
continente.nume_continent
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
INNER JOIN transport ON transport.id_trans=oferte.id_transport
LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
WHERE oferte.valabila = 'da' and tip_unitate<>'Circuit'
AND oferte.id_hotel IN (".$id_hoteluri_viz.")
Group by id_hotel
ORDER BY rand() 
LIMIT 0,4 ";
$queOf=mysql_query($selOf) or die(mysql_error());

if(mysql_num_rows($queOf)) {?>



		<h2>Clienții care au văzut <span class="col-red"><?php echo $detalii_hotel['denumire']?></span> din <a href="#"><?php echo $detalii_hotel['localitate']?></a> au fost interesați și de:</h2>



		<ul>
<?php  while($rowOf = mysql_fetch_array($queOf)) {
	if($rowOf['poza1']) $poza = '/thumb_hotel/'.$rowOf['poza1'];
	else $poza = '/images/no_photo.jpg';
	
		$denumire = $rowOf['nume'];
		$descriere_scurta = $rowOf['detalii_concept'];
		$stele = $rowOf['stele'];
		$localizare = $rowOf['denumire_zona']; if($rowOf['denumire_localitate']<>$localizare) $localizare = $localizare.' / '.$rowOf['denumire_localitate'];
		$link_hotel = fa_link_hotel($rowOf['denumire_localitate'], $rowOf['nume']);
?>
			<li>

				<a href="#" class="title"><?php echo $denumire.'<br /> '.$localizare;?></a>
<br />
				<span class="stars"><?php echo afiseaza_stele($stele)?></span>

				<p>

					<a href="#" class="thumb"><img src="<?php  echo $poza;?>" alt="<?php echo $denumire.' '.$localizare;?>"></a>

					<?php echo truncate($descriere_scurta,$length=220,$append="...");?>
				</p>

				<a href="#" class="button-light">Vezi preturi</a>

			</li>
<?php } ?>
			
			

			

		</ul>

<?php } } }?>
	</section>



	<section class="trust bottom"></section>

</div>

<?php echo '<pre>';print_r($hoteluri_localitate_1);echo '</pre>' ?>

<script>

var locations = [
<?php foreach($hoteluri_localitate_1 as $hoteluri_pe_harta) {
	
if ($hoteluri_pe_harta['id_hotel']==$id_hotel)
{ 
echo "[\"".$hoteluri_pe_harta['denumire']."- hotelul curent\",".$hoteluri_pe_harta['latitudine'].",". $hoteluri_pe_harta['longitudine'].", \"https://www.ocaziituristice.ro/images/icon_maps_hotel.png\"],";

	
	} else {	
echo "[\"<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-tanne/'>".$hoteluri_pe_harta['denumire']."</a>\",".$hoteluri_pe_harta['latitudine'].",". $hoteluri_pe_harta['longitudine'].", \"https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png\"],";
 }
}
 ?>

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-tanne/'>Hotel Tanne - Bansko</a>", 41.827449798583984, 23.484731674194336, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"],

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/aparthotel-orbilux/'>Aparthotel Orbilux - Bansko</a>", 41.838020324707030, 23.489030838012695, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"],

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-belvedere-holiday-club/'>Hotel Belvedere Holiday Club - Bansko</a>", 41.831783294677734, 23.475282669067383, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"],

  ["<strong>Hotel Bansko Spa and Holidays - Bansko</strong>", 41.826488494873050, 23.478584289550780, "https://www.ocaziituristice.ro/images/icon_maps_hotel.png"],

  ["<a href='https://www.ocaziituristice.ro/cazare-bansko/hotel-emerald-spa/'>Hotel Emerald Spa - Bansko</a>", 41.826423645019530, 23.479434967041016, "https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png"]

];



function initGoogleMap(){



  var infowindow = new google.maps.InfoWindow();

  var map = new google.maps.Map(document.getElementById('map'), {

      zoom: 16,

      center: new google.maps.LatLng(<?php  echo $detalii_hotel['latitudine']?>,   <?php echo $detalii_hotel['longitudine']?>),

      mapTypeId: google.maps.MapTypeId.ROADMAP

  });

  

  function placeMarker( loc ) {

    var latLng = new google.maps.LatLng( loc[1], loc[2]);

    var markerIcon = loc[3];

    var marker = new google.maps.Marker({

      position : latLng,

      map      : map,

      icon     : markerIcon

    });

    google.maps.event.addListener(marker, 'click', function(){

        infowindow.close();

        infowindow.setContent( "<div id='infowindow'>"+ loc[0] +"</div>");

        infowindow.open(map, marker);

    });

  }



  for(var i=0; i<locations.length; i++) {

    placeMarker( locations[i] );

  } 

  

}



if(isDevice) {

	$("#map").html('<span class="static" style="background-image: url(\'https://maps.googleapis.com/maps/api/staticmap?center=<?php  echo $detalii_hotel['latitudine']?>,<?php  echo $detalii_hotel['longitudine']?>&zoom=16&size=1000x500&maptype=roadmap&markers=icon:https://www.ocaziituristice.ro/images/icon_maps_hotel.png|<?php  echo $detalii_hotel['latitudine']?>,<?php  echo $detalii_hotel['longitudine']?>=<?php echo $google_api_key_static ?>\'"></span>');

} else {

	google.maps.event.addDomListener(window, 'load', initGoogleMap);

}





$(function(){

	$(".address").click(function(e){

		e.preventDefault();

		goToByScroll($(this).data("scroll"), 'h2');

	});

	

	$(".trust").load("includes/trustme.php");

});



$(window).load(function(){

	$(".visited ul li p").dotdotdot();

});



$(window).resize(function(){

	$(".visited ul li p").dotdotdot();

});

</script>



<?php //require_once("includes/footer.php"); ?>
</body>
</html>
