<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); ?>
<?php
$link_article = $_REQUEST['article'];
$link_area = $_REQUEST['link_area'];

$sel_article = "SELECT articles.*
FROM articles
WHERE articles.activ = 'da'
AND articles.link = '".$link_article."'
AND articles.link_area = '".$link_area."'
";
$que_article = mysql_query($sel_article) or die(mysql_error());
$row_article = mysql_fetch_array($que_article);

$id_article = $row_article['id_article'];
$id_tara = $row_article['id_tara'];
$id_zona = $row_article['id_zona'];
$id_localitate = $row_article['id_localitate'];
$categorie = $row_article['categorie'];
$denumire = $row_article['denumire'];
$descriere_scurta = $row_article['descriere_scurta'];
$descriere = $row_article['descriere'];

$den_tara = get_den_tara($id_tara);
$link_tara = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';

if($id_localitate>0) {
	$den_destinatie = get_den_localitate($id_localitate);
	$link_destinatie = '/cazare-'.fa_link(get_den_localitate($id_localitate)).'/';
	
	//if(fa_link(get_den_zona($id_zona))!=fa_link(get_den_tara($id_tara))) $link_destinatie .= fa_link(get_den_zona($id_zona)).'/'.fa_link(get_den_localitate($id_localitate)).'/';
	//if(fa_link(get_den_zona($id_localitate))!=fa_link(get_den_tara($id_zona))) $link_destinatie .= fa_link(get_den_localitate($id_localitate)).'/';
	
	$den_loc = get_den_localitate($id_localitate);
	$check_weather = check_weather($id_tara, $id_zona);
	$link_weather = '/vremea-in-'.fa_link(get_den_zona($id_zona)).'_'.$id_tara.'.html';
} else if($id_zona>0) {
	$den_destinatie = get_den_zona($id_zona);
	$link_destinatie = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';
	if(fa_link(get_den_zona($id_zona))!=fa_link(get_den_tara($id_tara))) $link_destinatie .= fa_link(get_den_zona($id_zona)).'/';
	$den_zona = get_den_zona($id_zona);
	$check_weather = check_weather($id_tara, $id_zona);
	$link_weather = '/vremea-in-'.fa_link(get_den_zona($id_zona)).'_'.$id_tara.'.html';
} else if($id_tara>0) {
	$den_destinatie = get_den_tara($id_tara);
	$link_destinatie = '/sejur-'.fa_link(get_den_tara($id_tara)).'/';
	$den_tara = get_den_tara($id_tara);
	$check_weather = check_weather($id_tara);
	$link_weather = '/vremea-in-'.fa_link(get_den_tara($id_tara)).'.html';
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title><?php echo $denumire; ?></title>
<meta name="description" content="<?php echo $descriere_scurta; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onLoad="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <div class="NEW-column-left2">
          <br>
          <div class="filters NEW-round6px">
            <div class="item NEW-round6px mar0">
              <div class="heading mar0"><?php echo $den_destinatie; ?></div>
              <div class="articles">
                <?php if($den_destinatie!=$den_tara) { ?><a href="<?php echo $link_tara; ?>" title="Sejur <?php echo $den_tara; ?>">Sejur <?php echo $den_tara; ?></a><?php } ?>
              
                <a href="<?php echo $link_destinatie; ?>" title="Sejur <?php echo $den_destinatie; ?>">Cazare <?php echo $den_destinatie; ?></a>

                <?php if($check_weather==1) { ?><a href="<?php echo $link_weather; ?>" title="Vremea in <?php echo $den_destinatie; ?>">Vremea <?php echo $den_destinatie; ?></a><?php } ?>
              </div>
            </div>
          </div>
		<?php $sel_categorii = "SELECT articles.categorie
        FROM articles
        WHERE articles.activ = 'da'
		AND articles.id_article <> '".$id_article."' ";
        if($id_tara>0) $sel_categorii = $sel_categorii." AND articles.id_tara = '".$id_tara."' ";
        if($id_zona>0) $sel_categorii = $sel_categorii." AND articles.id_zona = '".$id_zona."' ";
        if($id_localitate>0) $sel_categorii = $sel_categorii." AND articles.id_localitate = '".$id_localitate."' ";
        $sel_categorii = $sel_categorii." GROUP BY articles.categorie ";
        $que_categorii = mysql_query($sel_categorii) or die(mysql_error());
        while($row_categorii = mysql_fetch_array($que_categorii)) { ?>
          <br>
          <div class="filters NEW-round6px">
            <div class="item NEW-round6px mar0">
              <div class="heading mar0"><?php echo $row_categorii['categorie']; ?></div>
              <div class="articles">
              <?php $sel_alte_articole = "SELECT articles.denumire, articles.link, articles.link_area
			  FROM articles
			  WHERE articles.activ = 'da'
			  AND articles.id_article <> '".$id_article."'
			  AND articles.categorie = '".$row_categorii['categorie']."' ";
			  if($id_tara>0) $sel_alte_articole = $sel_alte_articole." AND articles.id_tara = '".$id_tara."' ";
			  if($id_zona>0) $sel_alte_articole = $sel_alte_articole." AND articles.id_zona = '".$id_zona."' ";
			  if($id_localitate>0) $sel_alte_articole = $sel_alte_articole." AND articles.id_localitate = '".$id_localitate."' ";
			  $sel_alte_articole = $sel_alte_articole." ORDER BY articles.id_article DESC ";
			  $que_alte_articole = mysql_query($sel_alte_articole) or die(mysql_error());
			  while($row_alte_articole = mysql_fetch_array($que_alte_articole)) {
				  echo '<a href="/ghid-turistic-'.$row_alte_articole['link_area'].'/'.$row_alte_articole['link'].'.html" title="'.$row_alte_articole['denumire'].'">'.$row_alte_articole['denumire'].'</a>';
			  } ?>
              </div>
            </div>
          </div>
		<?php } ?>
        </div>
        
        <div class="NEW-column-right2 clearfix">
          <h1 class="blue float-left"><?php echo $denumire; ?></h1>
          <br class="clear">
          <div class="Hline"></div>
          <div class="float-left clearfix"><?php include($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
          <div class="float-right mar0-10"><?php include($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/search_box.php"); ?></div>
          <br class="clear"><br>
          <div class="pad10 article bigger-12em text-justify">
            <?php echo htmlspecialchars_decode($descriere); ?>
          </div>
          <div class="float-left clearfix"><?php include($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
          <br class="clear"><br>
          <div class="fb-comments" data-href="<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>" data-num-posts="10" data-width="750"></div>
        </div>
        
      </div>
    </div>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>