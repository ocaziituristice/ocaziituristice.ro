<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); ?>
<?php /*?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Puncte de fidelitate <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Puncte de fidelitate, modul lor de obtinere pe portalul Ocazii Turistice si cum se aplica reducerile" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full"><?php */?>
      <div id="NEW-destinatie" class="clearfix">

        <span class="titlu_modala">Puncte de fidelitate Ocaziituristice.ro</span>
        
        <div class="Hline"></div>
        
        <div class="pad20 article" align="justify">

		  <h2 class="green">Detalii și regulament de folosire:</h2>
          <br />
          <ul class="article" style="font-size:14px;">
            <li>Valoarea unui punct de fidelitate este de 0.1 RON (ex. 10 puncte = 1 RON).</li>
            <li>Valabilitatea punctelor de fidelitate aferente unei rezervări este de 24 luni din momentul plății integrale.</li>
            <li>Punctele de fidelitate se obțin ca procent 30% din valoarea totală în RON a pachetului, mai puțin serviciile neincluse (taxe aeroport, suplimente masă, excursii opționale, și alte servicii care nu sunt incluse inițial în pachetul prezentat pe site). Ex: dacă un sejur costă 3000 RON, punctele obținute vor fi 900.</li>
            <li>Valoarea punctelor de fidelitate obținute la o rezervare va fi trecută pe bonul de comandă și factură.</li>
            <li>Nu este obligatoriu să folosiți toate punctele din cont pentru a obține discount la o rezervare, având posibilitatea de a folosi exact numărul de puncte dorit de dvs.</li>
            <li>Dacă anulați o comandă în care ați folosit puncte de fidelitate, acestea vă vor fi returnate în cont.</li>
            <li>Punctele folosite pentru a obține discount din valoarea unei comenzi sunt considerate consumate în momentul în care achitați factura aferentă acesteia.</li>
            <li>Dacă anulați o comanda, punctele pe care urma sa le primiți în baza acesteia vor fi de asemenea anulate.</li>
            <li>Punctele nu pot fi transferate de la un client la altul.</li>
            <li>Punctele nu pot fi preschimbate în bani.</li>
          </ul>
          <div><em>* Există posibilitatea ca la achiziționarea anumitor pachete aflate în promoții (Last Minute, Early Booking, Oferte Speciale), să nu fie alocate puncte de fidelitate. În acest caz în dreptul ofertei nu va apărea banda de anunțare a discountului prin puncte de fidelitate.</em></div>
          <br />
          <div><em>** Ocaziituristice.ro își rezervă dreptul de a modifica regulamentul și modul de acordare și folosire al punctelor de fidelitate, fără o avertizare prealabilă.</em></div>
        </div>
        
      </div>
<?php /*?>    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html><?php */?>