<?php

$destinatii = [
	[
		'nume'           => 'Mallorca',
		'tara'           => 'Spania',
		'localitate'     => 'Mallorca',
		'pret_minim'     => '2750',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-spania/mallorca/',
		'imagine'        => '/images/zone/mallorca.jpg'
	],
	[
		'nume'           => 'Insula Creta',
		'tara'           => 'Grecia',
		'localitate'     => '',
		'pret_minim'     => '718',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-grecia/insula-creta/',
		'imagine'        => '/images/zone/insula-creta.jpg'
	],
	[
		'nume'           => 'Antalya',
		'tara'           => 'Turcia',
		'localitate'     => '',
		'pret_minim'     => '448',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-turcia/antalya/',
		'imagine'        => '/sejur-turcia/images/antalya.jpg'
	],
	[
		'nume'           => 'Insula Thassos',
		'tara'           => 'Grecia',
		'localitate'     => '',
		'pret_minim'     => '254',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-grecia/insula-thassos/',
		'imagine'        => '/images/zone/insula-thassos.jpg'
	],
	[
		'nume'           => 'Insula Corfu',
		'tara'           => 'Grecia',
		'localitate'     => '',
		'pret_minim'     => '976',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-grecia/insula-corfu/',
		'imagine'        => '/images/zone/insula-corfu.jpg'
	],
	[
		'nume'           => 'Gran Canaria',
		'tara'           => 'Spania',
		'localitate'     => '',
		'pret_minim'     => '1849',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-spania/gran-canaria/',
		'imagine'        => '/images/zone/gran-canaria.jpg'
	],
	[
		'nume'           => 'Thailanda',
		'tara'           => 'Thailanda',
		'localitate'     => '',
		'pret_minim'     => '1698',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-thailanda/',
		'imagine'        => '/images/tari-mari/thailanda.jpg'
	],
	[
		'nume'           => 'Albania',
		'tara'           => 'Albania',
		'localitate'     => '',
		'pret_minim'     => '933',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/sejur-albania/',
		'imagine'        => '/img_mediu_hotel/7678hotel_grint1hotel.jpg'
	],
];
?>
<?php if ( $destinatii ) : ?>
    <div class="homepage-boxes promoted-destionations">
        <div class="layout">
            <h3 class="subtitle">Destinatiile anului 2018</h3>
            <div class="ofVizitate <?php echo 'columns-' . count( $destinatii ) ?>">
				<?php foreach ( $destinatii as $destinatie ) : ?>
                    <div class="prod">
                        <div class="prod-bg" style="background-image:url(<?php echo $destinatie['imagine'] ?>);"></div>
                        <div class="titlu"><?php echo $destinatie['nume'] ?></div>
                        <div class="prod-overlay">
                            <a href="<?php echo $destinatie['link'] ?>" class="link-blue">
                                <div class="overlay-text">
                                    <h3><?php echo $destinatie['nume'] ?></h3>
                                    <h5>
                                        <?php echo $destinatie['localitate'] ? $destinatie['localitate'] : '' ?>
                                        <?php echo $destinatie['localitate'] && $destinatie['tara'] ? ' / ' : '' ?>
                                        <?php echo $destinatie['tara'] ? $destinatie['tara'] : '' ?>
                                    </h5>
                                    <div>de la</div>
                                    <div class="price">
                                        <span class="red"><?php echo $destinatie['pret_minim'] ?></span> <?php echo $destinatie['moneda'] ?>
                                    </div>
                                    <p><?php echo $destinatie['exprimare_pret'] ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<?php endif; ?>