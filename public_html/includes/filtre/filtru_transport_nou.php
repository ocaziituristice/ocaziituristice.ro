<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
include_once($_SERVER['DOCUMENT_ROOT']."/includes/filtre/preluare_date.php"); 
if(isset($_GET['transport']) and $_GET['transport']<>''){
$transport=$_GET['transport'];
}else {$transport='toate';}


		/********** trans_filtru **********/
		$time_start_transport = microtime(true);
		$sel_filtru = "SELECT transport.denumire AS denumire_transport, oferte_transport_avion.aeroport_plecare as oras_plecare,oferte_transport_avion.tip as tip,oferte.id_oferta 
FROM oferte ";
		if ( $tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $st . "' AND data_pret_oferta.data_end >= '" . $sf . "'))) ";
		if ( $early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $start . "' AND data_pret_oferta.data_end>='" . $endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		left JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans 
		left Join oferte_transport_avion on oferte.id_oferta=oferte_transport_avion.id_oferta 
		left Join oferte_transport_autocar on oferte.id_oferta=oferte_transport_autocar.id_oferta ";
		if ( $tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		//$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		//if ( $this->loc_p_avion ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		//if ( $this->loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$check_in."' AND data_pret_oferta.data_end >= '".$check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$date_min."' AND data_pret_oferta.data_start <= '".$date_max."' ) ) ";
		if ( !$search && !$last_minute && !$recomandata ) {
			if ( !$set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $continent . " ";
		if ( $tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $tip_oferta . ") ";
		if ( $tari && !$tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $tari . " ";
		if ( !$tari && $tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $tari && $tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $tari . ")) OR (traseu_circuit.id_tara = " . $tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $tari ) $sel_filtru .= " AND tari.id_tara = " . $tari . " ";
		if ( $zone ) $sel_filtru .= " AND zone.id_zona = " . $zone . " ";
		if ( $orase ) $sel_filtru .= " AND localitati.id_localitate = " . $orase . " ";
		if ( $recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $id_hotel . "' ";
		if ( $set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $set_exceptieH . "' ";
		if ( $early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		//if ( $this->transport ) $sel_filtru .= " AND oferte.id_transport = '" . $this->transport . "' ";
		//if ( $this->loc_p_avion ) $sel_filtru .= " AND aeroport.id_localitate = '" . $this->loc_p_avion . "' ";
		//if ( $this->loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $this->loc_p_autocar . "' ";
		if ( $data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$this->data_plecarii."' ";
		if ( $nr_stele and $nr_stele!='toate') $sel_filtru .= " AND hoteluri.stele IN (" . $nr_stele . ") ";
		if ( $masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$masa.") ";
		if ( $distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $distanta[2] ) . "' ";
		if ( $concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $concept . ") ";
		//if ( $this->facilitati ) {
			//$facilities = explode(",", $this->facilitati);
			//foreach($facilities as $k_facilities => $v_facilities) {
				//$v_facilities = str_replace("'", "", trim($v_facilities));
				//$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			//}
		//}
		if ( $durata ) $sel_filtru = $sel_filtru . " AND oferte.nr_zile IN (" . $durata . ") ";
		$sel_filtru_transport= $sel_filtru;
		$sel_filtru .= " GROUP BY aeroport_plecare";
		
		//if($_SESSION['mail']=='razvan@ocaziituristice.ro') echo "transport". $tara ;
		
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$numar_oferte_transport=mysql_num_rows($rez_filtru);
		
		$rez_filtru_transport = mysql_query( $sel_filtru ) or die( mysql_error() );
		
		
		$trans_filtru = '';
		$trans_filtru = array();
		$plecare_avion_filtru = '';
		$plecare_avion_filtru = array();
		$plecare_autocar_filtru = '';
		$plecare_autocar_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru_transport ) ) {
			if ( $row_filtru['denumire_transport'] == 'Avion' or $row_filtru['denumire_transport'] == 'Avion si Autocar local' ) {
				$selFTA = "SELECT localitati.denumire as denumire
					FROM oferte_transport_avion
					LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
					LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
					LEFT JOIN zone ON localitati.id_zona = zone.id_zona
					WHERE oferte_transport_avion.id_oferta = '" . $row_filtru['id_oferta'] . "'
					AND zone.id_tara = '1'
					AND oferte_transport_avion.tip <> 'intors'
					
				";
				//if($_SESSION['mail']=='razvan@ocaziituristice.ro') echo "<br/>". $selFTA."<br/>";		
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
				//echo $loc_FTA['denumire']."<br />";
				$plecare_avion_filtru[$loc_FTA['denumire']] = $plecare_avion_filtru[$loc_FTA['denumire']] + 1;
				//$this->plecare_avion[$loc_FTA['denumire']] = $numar_oferte_transport;
				}
		
				
				@mysql_free_result( $queFTA );
			} elseif ( $row_filtru['denumire_transport'] == 'Autocar' ) {
				$selFTA = "SELECT localitati.denumire
					FROM oferte_transport_autocar
					LEFT JOIN localitati on oferte_transport_autocar.id_localitate = localitati.id_localitate
					WHERE
					oferte_transport_autocar.id_oferta = '" . $row_filtru['id_oferta'] . "'
					GROUP BY localitati.denumire
					ORDER BY localitati.denumire
				";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() );
				while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
					$plecare_autocar_filtru[$loc_FTA['denumire']] = $plecare_autocar_filtru[$loc_FTA['denumire']] + 1;
				}
				@mysql_free_result( $queFTA );
			}

			if ( $trans_filtru[$row_filtru['denumire_transport']] ) 
			$trans_filtru[$row_filtru['denumire_transport']] = $trans_filtru[$row_filtru['denumire_transport']] + 1;
			else $trans_filtru[$row_filtru['denumire_transport']] = 1;
		} @mysql_free_result( $rez_filtru );
		
		
		/********** trans_filtru **********/


krsort($trans_filtru); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, 'toate', $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php if($transport=='toate') echo 'class="sel"'; ?> onclick="set_grad_ocupare('', '', '', '', '', '', '', '', '0', '51');">Toate</a></li>
    <?php $i=0; foreach($trans_filtru as $key=>$value) {
		$i++;
		$lingTr=fa_link($key);
		if($value) $nr=$value; else $nr=0;
		if($transport==$lingTr) {
			$tr=$lingTr;
			$selclass='class="sel"';
		} else {
			$tr=$lingTr;
			$selclass='';
		}
	//echo '<pre>'; print_r($plecare_avion_filtru); echo '</pre>';
	?>
    
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $tr, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> onclick="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', '51');"><?php echo $key; ?> <?php //echo '('.$nr.')'; ?></a>
      <?php if($key=='Avion') { ?>
      <select name="oras_plecare" onchange="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', this.value); filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, this.options[this.selectedIndex].innerHTML.toLowerCase(), <?php echo "'".$plecare_autocar."'"; ?>, <?php echo "'".$Rconcept."'"; ?>);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruOf['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($plecare_avion_filtru)) {
			arsort($plecare_avion_filtru);
			foreach($plecare_avion_filtru as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo get_id_localitate($lingTr); ?>" <?php if(strtolower($lingTr)==strtolower($plecare_avion) || ($transport=='avion' && sizeof($plecare_avion_filtru)==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
      <?php if($key=='Autocar') { ?>
      <select name="oras_plecare" onchange="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', this.value); filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'autocar', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, this.options[this.selectedIndex].innerHTML.toLowerCase(), <?php echo "'".$Rconcept."'"; ?>); setCookie('transportAutocar', this.options[this.selectedIndex].innerHTML.toLowerCase(), 30);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_autocar'])==0) echo 'disabled="disabled"'; if($plecare_autocar=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($plecare_autocar_filtru)) {
			//arsort($filtruOf['plecare_autocar']);
			foreach($plecare_autocar_filtru as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo get_id_localitate($lingTr); ?>" <?php if(strtolower($lingTr)==strtolower($plecare_autocar) || ($transport=='autocar' && sizeof($filtruOf['plecare_autocar'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
  </div>
  <?php 
 $time_end_transport = microtime(true);
	//	if($_SESSION['mail']=='razvan@ocaziituristice.ro') {echo "filtru_transport=".$execution_time_transport = ($time_end_transport - $time_start_transport);} 
  ?>