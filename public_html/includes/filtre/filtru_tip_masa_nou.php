<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
include_once($_SERVER['DOCUMENT_ROOT']."/includes/filtre/preluare_date.php"); 

if(isset($_GET['Rmasa']) and $_GET['Rmasa']<>''){
 $Lmasa=explode(',',$Rmasa);
}else {$masa='toate';}


$time_start_masa = microtime(true);
		$sel_filtru = "SELECT hotels_meals.masa
		FROM oferte
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel ";
		if ( $tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $st . "' AND data_pret_oferta.data_end >= '" . $sf . "'))) ";
		if ( $early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $start . "' AND data_pret_oferta.data_end>='" . $endd . "')) ";
		$sel_filtru .= "
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(hotel_meal.masa SEPARATOR ',') AS masa, hotel_meal.id_hotel FROM hotel_meal GROUP BY hotel_meal.id_hotel) AS hotels_meals ON hotels_meals.id_hotel = hoteluri.id_hotel ";
		if ( $data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $loc_p_avion and $cazare != 'da' ) $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$check_in."' AND data_pret_oferta.data_end >= '".$check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$date_min."' AND data_pret_oferta.data_start <= '".$date_max."' ) ) ";
		if ( !$search && !$last_minute && !$recomandata ) {
			if ( !$set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $continent . " ";
		if ( $tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $tip_oferta . ") ";
		if ( $tari && !$tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $tari . " ";
		if ( !$tari && $tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $tari && $tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $tari . ")) OR (traseu_circuit.id_tara = " . $tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $tari ) $sel_filtru .= " AND tari.id_tara = " . $tari . " ";
		if ( $zone ) $sel_filtru .= " AND zone.id_zona = " . $zone . " ";
		if ( $orase ) $sel_filtru .= " AND localitati.id_localitate = " . $orase . " ";
		if ( $recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $id_hotel . "' ";
		if ( $set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $set_exceptieH . "' ";
		if ( $early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $id_transport ) $sel_filtru .= " AND oferte.id_transport = '" . $id_transport . "' ";
		if ( $transport_neselectat ) $sel_filtru .= " AND oferte.id_transport = '" . $transport_neselectat . "' ";
		if ( $loc_p_avion and $cazare != 'da') $sel_filtru .= " AND aeroport.id_localitate = '" . $loc_p_avion . "' ";
		if ( $loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $loc_p_autocar . "' ";
		if ( $data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$data_plecarii."' ";
		if ( $nr_stele and $nr_stele!='toate') $sel_filtru .= " AND hoteluri.stele IN (" . $nr_stele . ") ";
		//if ( $masa and $masa!='toate') $sel_filtru .= " AND LOWER(hotels_meals.masa) IN (".$masa.") ";
		if ( $distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $distanta[2] ) . "' ";
		if ( $concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $concept . ") ";
		if ( $facilitati ) {
			$facilities = explode(",", $facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		//echo $sel_filtru;
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$tip_masa_filtru = '';
		$tip_masa_filtru = array();
		$count_meal_hotels = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['masa'] ) {
				
				$meal = explode(",", $row_filtru['masa']);
				$meal=array_unique($meal);
				foreach($meal as $k_meal => $v_meal) {
					$tip_masa_filtru[$ordine_masa[$v_meal]][$v_meal] = $tip_masa_filtru[$ordine_masa[$v_meal]][$v_meal] + 1;
				}
			}
			/*if(!in_array($row_filtru['id_hotel'], $count_meal_hotels)) {
				$count_meal_hotels[] = $row_filtru['id_hotel'];
				$this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] = $this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] + 1;
			}*/
			//$this->masa_filtru[$ordine_masa[$row_filtru['masa']]][$row_filtru['masa']] = $row_filtru['count_hotels'];
		} @mysql_free_result( $rez_filtru );
	
		/********** masa_filtru **********/
		//if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	{echo  $link_p1; echo '<pre>'; print_r($tip_masa_filtru); echo '</pre>';}

?>

 <?php ksort($tip_masa_filtru); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, 'toate', $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php if($masa=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($tip_masa_filtru as $keyM=>$valueM) {
		foreach($valueM as $denM=>$nr_m) {
			$i++;
			if($nr_m) $nrM=$nr_m; else $nrM=0;
			if(is_array($Lmasa)) {
				if(in_array(fa_link($denM),$Lmasa)) {
					$lingM=remove_current_filter($Rmasa,fa_link($denM));
					$selclass='class="sel"';
				} else {
					$lingM=$Rmasa.','.fa_link($denM);
					$selclass='';
				}
			} else {
				$lingM=fa_link($denM);
				$selclass='';
			}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $lingM, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> ><?php echo $denM; ?> <?php echo '('.$nrM.')'; ?></a></li>
    <?php }
	} ?>
  </ul>
  
	<?php $time_end_masa = microtime(true);
		//if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	echo "filtru_masa=".$execution_time_masa = ($time_end_masa - $time_start_masa); echo $sel_filtru;echo '<pre>'; print_r($_POST); echo '</pre>';?>  