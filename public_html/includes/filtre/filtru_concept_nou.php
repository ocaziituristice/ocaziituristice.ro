<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
include_once($_SERVER['DOCUMENT_ROOT']."/includes/filtre/preluare_date.php"); 
//echo '<pre>'; print_r($_GET); echo '</pre>';

if(isset($_GET['Rconcept']) and $_GET['Rconcept']<>''){
 $Lconcept=explode(',',$Rconcept);
$Rconcept=$_GET['Rconcept'];
}else {$concept='toate';}



		/********** concept_filtru **********/
		$time_start_concept = microtime(true);
		$sel_filtru = "SELECT hoteluri.concept
		FROM oferte ";
		if ( $tip_oferta ) $sel_filtru .= " INNER JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta ";
		if ( $tipofsp==1 ) $sel_filtru .= " INNER JOIN oferta_sejur_tip AS ofsp1 ON oferte.id_oferta = ofsp1.id_oferta
		INNER JOIN tip_oferta AS ofsp2 ON (ofsp1.id_tip_oferta = ofsp2.id_tip_oferta AND CURDATE() BETWEEN ofsp2.data_inceput_eveniment AND ofsp2.data_sfarsit_eveniment AND ofsp2.tip = 'oferte_speciale') ";
		if ( $luna_plecare ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $luna_plecare . "' AND data_pret_oferta.data_start > NOW()) OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $st . "' AND data_pret_oferta.data_end >= '" . $sf . "'))) ";
		if ( $early == 'da' ) $sel_filtru .= " LEFT JOIN (SELECT * FROM early_booking WHERE early_booking.end_date >= now() AND early_booking.tip = 'sejur' GROUP BY id_oferta, end_date, discount ORDER BY end_date) AS early ON oferte.id_oferta = early.id_oferta ";
		if ( $start ) $sel_filtru .= " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND (data_pret_oferta.data_start<='" . $start . "' AND data_pret_oferta.data_end>='" . $endd . "')) ";
		$sel_filtru .= "
		INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		left JOIN hotel_meal ON hoteluri.id_hotel = hotel_meal.id_hotel
		INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
		INNER JOIN zone ON localitati.id_zona = zone.id_zona
		INNER JOIN tari ON zone.id_tara = tari.id_tara
		INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
		if ( $tari_circ ) $sel_filtru .= " LEFT JOIN traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte ";
		if ( $id_tara_circuit ) $sel_filtru .= " INNER JOIN traseu_circuit ON (hoteluri.id_hotel = traseu_circuit.id_hotel_parinte AND traseu_circuit.id_tara IN (" . $id_tara_circuit . ") AND traseu_circuit.tara_principala = 'da') ";
		$sel_filtru .= " LEFT JOIN (SELECT GROUP_CONCAT(caracteristici_hotel.denumire SEPARATOR ',') AS facilitati, hotel_caracteristici.id_hotel FROM hotel_caracteristici INNER JOIN caracteristici_hotel ON caracteristici_hotel.id = hotel_caracteristici.id GROUP BY hotel_caracteristici.id_hotel) AS facilities ON facilities.id_hotel = hoteluri.id_hotel ";
		if ( $data_plecarii ) $sel_filtru = $sel_filtru . " INNER JOIN data_pret_oferta AS data_plecarii ON (oferte.id_oferta = data_plecarii.id_oferta AND oferte.tip_preturi = 'plecari' AND data_plecarii.data_start > NOW()) ";
		if ( $loc_p_avion and $cazare != 'da') $sel_filtru .= " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport ";
		if ( $loc_p_autocar ) $sel_filtru .= " INNER JOIN oferte_transport_autocar ON oferte.id_oferta = oferte_transport_autocar.id_oferta ";
		if($check_in) $sel_filtru .= " INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta ";
		$sel_filtru .= " WHERE oferte.valabila = 'da' ";
		if($check_in) $sel_filtru .= " AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$check_in."' AND data_pret_oferta.data_end >= '".$check_in."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$date_min."' AND data_pret_oferta.data_start <= '".$date_max."' ) ) ";
		if ( !$search && !$last_minute && !$recomandata ) {
			if ( !$set_circuit ) $sel_filtru .= " AND hoteluri.tip_unitate <> 'Circuit' ";
			else $sel_filtru .= " AND hoteluri.tip_unitate = 'Circuit' ";
		}
		if ( $last_minute ) $sel_filtru .= " AND oferte.last_minute = 'da' ";
		if ( $cazare == 'da' ) $sel_filtru .= " AND oferte.cazare = 'da' ";
		if ( $continent ) $sel_filtru .= " AND hoteluri.id_continent = " . $continent . " ";
		if ( $tip_oferta ) $sel_filtru .= " AND oferta_sejur_tip.id_tip_oferta IN (" . $tip_oferta . ") ";
		if ( $tari && !$tari_circ ) $sel_filtru .= " AND zone.id_tara = " . $tari . " ";
		if ( !$tari && $tari_circ ) $sel_filtru .= " AND traseu_circuit.id_tara = " . $tari_circ . " AND traseu_circuit.tara_principala = 'da' ";
		if ( $tari && $tari_circ ) $sel_filtru .= " AND ((hoteluri.tip_unitate<>'Circuit' && tari.id_tara IN (" . $tari . ")) OR (traseu_circuit.id_tara = " . $tari_circ . " AND traseu_circuit.tara_principala = 'da')) ";
		if ( $zone ) $sel_filtru .= " AND zone.id_zona = " . $zone . " ";
		if ( $orase ) $sel_filtru .= " AND localitati.id_localitate = " . $orase . " ";
		if ( $recomandata ) $sel_filtru .= " AND oferte.recomandata = 'da' ";
		if ( $id_hotel ) $sel_filtru .= " AND hoteluri.id_hotel ='" . $id_hotel . "' ";
		if ( $set_exceptieH ) $sel_filtru .= " AND oferte.id_hotel <> '" . $set_exceptieH . "' ";
		if ( $early == 'da' ) $sel_filtru .= " AND early.discount is not null";
		if ( $id_transport ) $sel_filtru .= " AND oferte.id_transport = '" . $id_transport . "' ";
		if ( $loc_p_avion and $cazare != 'da') $sel_filtru .= " AND aeroport.id_localitate = '" . $loc_p_avion . "' ";
		if ( $loc_p_autocar ) $sel_filtru .= " AND oferte_transport_autocar.id_localitate = '" . $loc_p_autocar . "' ";
		if ( $data_plecarii ) $sel_filtru .= " AND data_plecarii.data_start = '".$data_plecarii."' ";
		if ( $nr_stele and $nr_stele!='toate') $sel_filtru .= " AND hoteluri.stele IN (" . $nr_stele . ") ";
		if ( $masa ) $sel_filtru .= " AND LOWER(hotel_meal.masa) IN (".$masa.") ";
		if ( $distanta ) $sel_filtru .= " AND TRIM(hoteluri.distanta_fata_de) = '" . trim( $distanta[0] ) . "' AND hoteluri.distanta>='" . trim( $distanta[1] ) . "' AND hoteluri.distanta<'" . trim( $distanta[2] ) . "' ";
		//if ( $this->concept ) $sel_filtru .= " AND lcase(hoteluri.concept) IN (" . $this->concept . ") ";
		if ( $facilitati ) {
			$facilities = explode(",", $facilitati);
			foreach($facilities as $k_facilities => $v_facilities) {
				$v_facilities = str_replace("'", "", trim($v_facilities));
				$sel_filtru = $sel_filtru . " AND facilities.facilitati LIKE '%".$v_facilities."%' ";
			}
		}
		$sel_filtru .= " GROUP BY oferte.id_hotel ";
		$rez_filtru = mysql_query( $sel_filtru ) or die( mysql_error() );
		$concept_filtru = '';
		$concept_filtru = array();
		while ( $row_filtru = mysql_fetch_array( $rez_filtru ) ) {
			if ( $row_filtru['concept'] ) {
				$concept_filtru[$row_filtru['concept']] = $concept_filtru[$row_filtru['concept']] + 1;
			}
		} @mysql_free_result( $rez_filtru );
			
		/********** concept_filtru **********/


 ?>
<?php arsort($concept_filtru); ?>
<ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, 'toate', $Rfacilitati, $checkin); ?>" <?php if($concept=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($concept_filtru as $denC=>$nr_c) { $i++;
		if($nr_c) $nrC=$nr_c; else $nrC=0;
		if(is_array($Lconcept)) {
			if(in_array(fa_link($denC),$Lconcept)) {
				$lingC=remove_current_filter($Rconcept,fa_link($denC));
				$selclass='class="sel"';
			} else {
				$lingC=$Rconcept.','.fa_link($denC);
				$selclass='';
			}
		} else {
			$lingC=fa_link($denC);
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $lingC, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> ><?php echo $denC; ?> <?php echo '('.$nrC.')'; ?></a></li>
    <?php } ?>
  </ul>
 <?php 
 $time_end_concept = microtime(true);
 		if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	echo "filtru_concept=".$execution_time_concept = ($time_end_concept - $time_start_concept);

 ?> 