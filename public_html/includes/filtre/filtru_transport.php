<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
$transport=$_GET['transport'];
$link_p1=base64_decode($_GET['_link_p1']);
$nr_stele=$_GET['nr_stele'];
$Rmasa=$_GET['Rmasa'];
$distanta=$_GET['distanta'];
$plecare_avion=$_GET['plecare_avion'];
$plecare_autocar=$_GET['plecare_autocar'];
$Rconcept=$_GET['Rconcept'];
$Rfacilitati=$_GET['Rfacilitati'];
$checkin=$_GET['checkin'];
$filtruOf=$_POST;
krsort($filtruOf['trans']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, 'toate', $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php if($transport=='toate') echo 'class="sel"'; ?> onclick="set_grad_ocupare('', '', '', '', '', '', '', '', '0', '51');">Toate</a></li>
    <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) {
		$i++;
		$lingTr=fa_link($key);
		if($value) $nr=$value; else $nr=0;
		if($transport==$lingTr) {
			$tr=$lingTr;
			$selclass='class="sel"';
		} else {
			$tr=$lingTr;
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $tr, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> onclick="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', '51');"><?php echo $key; ?> <?php //echo '('.$nr.')'; ?></a>
      <?php if($key=='Avion') { ?>
      <select name="oras_plecare" onchange="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', this.value); filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, this.options[this.selectedIndex].innerHTML.toLowerCase(), <?php echo "'".$plecare_autocar."'"; ?>, <?php echo "'".$Rconcept."'"; ?>);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruOf['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_avion'])) {
			krsort($filtruOf['plecare_avion']);
			foreach($filtruOf['plecare_avion'] as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo get_id_localitate($lingTr); ?>" <?php if(strtolower($lingTr)==strtolower($plecare_avion )|| ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
      <?php if($key=='Autocar') { ?>
      <select name="oras_plecare" onchange="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', this.value); filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'autocar', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, this.options[this.selectedIndex].innerHTML.toLowerCase(), <?php echo "'".$Rconcept."'"; ?>); setCookie('transportAutocar', this.options[this.selectedIndex].innerHTML.toLowerCase(), 30);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_autocar'])==0) echo 'disabled="disabled"'; if($plecare_autocar=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_autocar'])) {
			//arsort($filtruOf['plecare_autocar']);
			foreach($filtruOf['plecare_autocar'] as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo get_id_localitate($lingTr); ?>" <?php if($lingTr==$plecare_autocar || ($transport=='autocar' && sizeof($filtruOf['plecare_autocar'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
  </div>