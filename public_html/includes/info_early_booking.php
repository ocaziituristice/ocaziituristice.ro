<div class="left article bigger-11em" align="justify">
  <p>Rezervarea unui pachet turistic prin early booking, reprezinta un avantaj deoarece turistii au posibilitatea de a alege din timp atat destinatia dorita, cat si data plecarii in vacanta.</p>
  <p>Acest program se aplica pentru sejururi, circuite sau croziere.</p>
  <p>Tarifele acestor pachete early booking sunt foarte atractive. <strong class="black">Ofertele Early booking</strong> sunt oferte care iti dau posibilitatea de a-ti achizitiona o vacanta cu reduceri foarte mari. Aceste oferte sunt adresate celor care vor sa aiba o vacanta la cel mai avantajos pret, dar in anumite conditii.</p>
  <p><strong class="black">Conditii obligatorii</strong> de comercializare a ofertei speciale - <strong class="black">Inscriere Timpurie</strong> - sau - <strong class="black">Early Booking</strong>:</p>
  <ul>
    <li>Rezervarile facute in perioada <em class="black">Early booking</em> nu permit modificari de nume, perioada sau anulari. Orice modificari aduse rezervarii initiale, duc la recalcularea pretului si implicit la pierderea acestei facilitati;</li>
    <li>Plata se va face <strong class="black">integral</strong> in 5 zile lucratoare de la primirea confirmarii;</li>
    <li>Reducerile se aplica doar la <strong class="black">pachetul de baza</strong> (tarif de cazare). Acestea nu se aplica la taxe sau la suplimente. Reducerile Early booking nu sunt valabile in cazul ofertelor speciale si nu se aplica nici copiilor care beneficiaza de gratuitate la cazare;</li>
    <li>Reducerile <strong class="black">nu se cumuleaza</strong> si <strong class="black">nu sunt transmisibile</strong>. In urma rezervarii si contractarii serviciilor turistice, aparitia unei oferte speciale nu modifica tariful deja stabilit;</li>
    <li>Fiecare destinatie beneficiaza de conditii diferite de early booking; pentru detalii, nu ezitati sa consultati ofertele noastre;</li>
    <li><strong class="red"><u>Anularea</u></strong> rezervarii pentru oferte marcate cu  "Inscrieri Timpurii" sau "Early Booking" se va solda cu <strong class="black">penalizare de 100%</strong> din valoarea sejurului, circuitului sau croazierei, fiind o conditie obligatorie de comercializare a acestei oferte, indiferent cu cate zile inainte de plecare faceti aceasta anulare (in cazul ofertelor early booking nu se aplica penalitatile cuprinse in contractul general incheiat intre agentie si turist), sau de motivul anularii (boala, probleme familiale, anularea concediului de odihna etc).</li>
    <li><strong class="black">Orice modificare</strong> a numelui persoanei se va considera o <strong class="black">anulare</strong> a rezervarii initiale si efectuarea unei noi rezervari. Noile rezervari se refac in functie de disponibilitati si se vor plati la tarif intreg, fara reducere.</li>
  </ul>
</div>
