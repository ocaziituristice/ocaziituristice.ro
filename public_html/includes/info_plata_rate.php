<p class="bigger-13em"><strong>Puteţi plăti serviciile comandate de dvs. prin următoarele modalităţi:</strong></p>
<ul>
  <li><strong class="blue bigger-12em">Numerar</strong><br />
    Puteţi plăti în numerar în <strong>RON</strong> sau <strong>EUR</strong> la sediul Agenţiei de turism Dream Voyage International, <?php echo $contact_adresa; ?><br />
    <strong>Program de lucru:</strong> <?php echo $contact_program; ?>
  </li>
  <li><strong class="blue bigger-12em">Transfer bancar</strong><br />
    Mai jos aveţi datele necesare pentru efectuarea transferului bancar:<br />
    <br />
    <strong><?php echo $contact_legal_den_firma; ?></strong><br /><br />
    Cod fiscal: <?php echo $contact_legal_cui; ?><br /><br />
    Adresa: <?php echo $contact_legal_adresa; ?><br />
    <br />
    Banca <?php echo $contact_legal_banca; ?><br />
    RON: <?php echo $contact_legal_cont_lei; ?><br />
    EUR: <?php echo $contact_legal_cont_euro; ?>
  </li>
  <li><strong class="blue bigger-12em">Cu cardul online</strong><br />
    Plata se va face prin intermediul site-ului <a href="http://www.plationline.ro/" target="_blank" class="link-blue">www.plationline.ro</a> - dacă optaţi pentru această variantă, veţi primi un link securizat şi personalizat pentru achitarea facturii<br />
    <strong>IMPORTANT!</strong> Pe extrasul de cont va apărea ca operator <strong>PlatiOnline</strong> sau <strong>OnlinePay</strong> şi nu Ocazii Turistice sau Dream Voyage International
  </li>
  <li><strong>Informatii carduri</strong></li>
</ul>

<ul>
  
  <p><img src="../imagines/alpha bank.png" width="150" height="93" /></p>
</ul>
<ul>
  <li>Cardurile de cumparaturi in lei de la Alpha Bank iti ofera posibilitatea de a plati in rate cu 0% dobanda, fara alte comisioane sau costuri suplimentare, pentru bunurile si serviciile oferite in reteaua comerciantilor parteneri Alpha Bank.
</li>
  </li>
   <p><img src="../imagines/cardavantaj.jpg" width="150" height="93" /></p>
   <li>Folosind CardAvantaj beneficiezi de 4 RATE fara dobanda sau plata integrala.Pentru a plati online cu cardul, in procesul de comanda selectati optiunea de plata &ldquo;Online cu card bancar&rdquo; si alege modalitatea de plata dorita: <strong>4 </strong><strong>Rate</strong><strong>fara dobanda</strong> sau plata integrala.<br />
   </li>
   <p><img src="../imagines/starcard.jpg" width="150" height="93" /></p>
  <li>Programul de plata in RATE fara Dobanda folosind cardurile de credit Star BT este disponibil pentru cumparaturi online.  Folosind cardurile de credit de la Banca Transilvania, prin programul StarCARD beneficiezi de<strong> 4 rate fara dobanda</strong>.Pentru a plati online cu cardul, in procesul de comanda selectati optiunea de plata &ldquo;Online cu card bancar&rdquo; si numarul de rate dorit pana la maxim 4 rate sau integral. <br />
   </li>
    <p><img src="../imagines/BONUS-card.jpg" width="150" height="93" /></p> 
  <li>Programul de plata in rate fara dobanda folosind cardul Garanti Bonus Card este disponibil pentru cumparaturi turistice online, fiind disponibile optiunile: <strong>4 rate fara dobanda</strong> sau plata integrala.  Pentru a plati online cu cardul, in procesul de comanda selecteaza optiunea de plata &ldquo;Online cu card bancar&rdquo; si alegeti modalitatea de plata dorita pana la maxim 4 rate sau integral.<br />
  </li>          
</ul>
<ul>
  <li><strong class="blue bigger-12em">Cu cardul la sediul agenţiei</strong><br />
    Puteţi efectua plata şi la sediul agenţiei Dream Voyage International prin POS<br />
    <strong>Program de lucru:</strong> <?php echo $contact_program; ?>
  </li>
</ul>
