<?php
include_once("PlatiOnlineRo/clspo.php");
include_once("PlatiOnlineRo/RSALib.php");
		
		$my_class = new PO3();

	$my_class->LoginID = $lid;
	$my_class->KeyEnc = $ke;
	$my_class->KeyMod = $km;

	$my_class->amount = $suma_de_plata;
	$my_class->currency = "RON";
	$my_class->OrderNumber = $OrderNumber;
	$my_class->action = "2";
	$ret = $my_class->InsertHash_Auth();
	//Pt. Rate RZB
	/*
	$my_class->rate = "6";
	$my_class->action = "10";
	$ret = $my_class->InsertHash_AuthRate_RZB();
	*/

	//Pt. Rate BT
	/*
	$my_class->rate = "6";
	$my_class->action = "16";
	$ret = $my_class->InsertHash_AuthRate_RZB();
	*/
	$vOrderString= "<start_string>"; 
	$vOrderString.= "<item>";
	$vOrderString.= "<ProdID>1</ProdID>";
	$vOrderString.= "<qty>1</qty>";
	$vOrderString.= "<itemprice>".$suma_de_plata."</itemprice>";
	$vOrderString.= "<name>".$denumire_produs."</name>";
	$vOrderString.= "<period></period><rec_id>0</rec_id>";
	$vOrderString.= "<description>".$descriere_produs."</description>";
	$vOrderString.= "<pimg></pimg><rec_price>0</rec_price>";
	$vOrderString.= "<vat>0</vat>";
	$vOrderString.= "<lang_id></lang_id><stamp>".htmlspecialchars(date("F j, Y, g:i a")). "</stamp><on_stoc>1</on_stoc>";
	$vOrderString.= "<prodtype_id></prodtype_id><categ_id>0</categ_id><merchLoginID>0</merchLoginID>";
	$vOrderString.= "</item>";

	//cupon
	//$vOrderString .= "<coupon><key>cod</key><value>".abs(round(0.05,2))."</value><percent>1</percent><workingname>Nume cupon</workingname><type>0</type><scop>0</scop><vat>0</vat></coupon>";

	//shipping
	//$vOrderString .= "<shipping><type>Denumire shipping</type><price>1.00</price><pimg></pimg><vat>0</vat></shipping>";
	$vOrderString .= "</start_string>";?>
	<form id="registerForm" autocomplete="off" method="post" action="https://secure2.plationline.ro/">
		<?php echo $ret;?>
		<input type="hidden" name="f_login" value="<?php echo $my_class->LoginID;?>">
		<input type="hidden" name="f_show_form" value="0">
		<input type="hidden" name="f_amount" value="<?php echo $my_class->amount;?>">
		<input type="hidden" name="f_currency" value="<?php echo $my_class->currency;?>">
		<input type="hidden" name="f_order_number" value="<?php echo $my_class->OrderNumber;?>">
		<input type="hidden" name="F_Language" value="ro" >
		<input type="hidden" name="F_Lang" value="ro">
		<input type="hidden" name="f_order_string" value="<?php echo $vOrderString; ?>">
		<input type="hidden" name="f_first_name" id="f_first_name" value="<?php echo $prenume;?>">
		<input type="hidden" name="f_last_name" id="f_last_name" value="<?php echo $nume;?>">
		<input type="hidden" name="f_cnp" value="-">
		<input type="hidden" name="f_address" id="f_address" value="<?php echo $adresa_facturare;?>">
		<input type="hidden" name="f_city" id="f_city" value="<?php echo $oras_facturare;?>">
		<input type="hidden" name="f_state" id="f_state" value="-">
		<input type="hidden" name="f_zip" id="f_zip" value="-">
		<input type="hidden" name="f_country" id="f_country" value="RO">
		<input type="hidden" name="f_phone" id="f_phone" value="<?php echo $telefon_facturare;?>">
		<input type="hidden" name="f_email" id="f_email" value="<?php echo $email_facturare;?>">
		
        <input type="hidden" name="f_company" value="<?php echo $denumire_facturare?>">
		<input type="hidden" name="f_reg_com" value="<?php echo $reg_comert?>">
		<input type="hidden" name="f_cui" value="<?php echo $cui_cnp?>">
		

<!-- daca e test mode START here -->
<!--<input type="hidden" name="f_Test_Request" value="1">-->
<!-- daca e test mode END here -->

	<div>Total de plata: <strong><?php echo $suma_de_plata?>&nbsp;&nbsp;<?php echo $my_class->currency;?>&nbsp;&nbsp;<input type="submit" value="Plateste Acum" class="btn-red NEW-round6px" style="font-size:16px; letter-spacing:1px; padding:4px 30px; margin:26px 10px 0 0;"></div>
        <br />
  Poti plati cu cardul ACUM. NU rata pretul special!<br /> Banii vor luati de pe card doar dupa confirmarea de catre furnizor  <br/>
  <img src="https://secure2.plationline.ro/images/sigle/80px-Mastercard_SecureCode_Logo.png" alt="MasterCard SecureCode">
  <img src="https://secure2.plationline.ro/images/sigle/80px-VerifiedByVISA_Logo.png" alt="Verified by VISA">
	</form>