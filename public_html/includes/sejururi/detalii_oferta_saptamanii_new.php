<?php $_SESSION['page_back_offer'] = $_SERVER['REQUEST_URI']; ?>

<div id="NEW-detaliiOferta" <?php if($detalii['valabila']=='nu') { ?>style="background:#FFF url(/images/oferta_expirata.gif) center 90px;"<?php } ?>>
<?php
$link_rezervare = $sitepath.'rezervare_sejur.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$link_rezervare2 = $sitepath.'rezervare_sejur_new.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$link_cerere_detalii = $sitepath.'cerere_detalii_new.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$valuta = 'nu';
	
$sel_table_SEO="SELECT cuvant_cheie FROM seo WHERE id_tip = '0' AND id_oras = '".$detalii_hotel['locatie_id']."' ";
$que_table_SEO=mysql_query($sel_table_SEO) or die(mysql_error());
$row_table_SEO=mysql_fetch_array($que_table_SEO);
@mysql_free_result($que_table_SEO);
if($row_table_SEO['cuvant_cheie']) $cuvant_cheie_loc=$row_table_SEO['cuvant_cheie']; else $cuvant_cheie_loc='Cazare '.$detalii_hotel['localitate'];
$link_cuvant_cheie_loc=$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/'.fa_link($detalii_hotel['localitate']).'/';

?>
 
  <h1 class="float-left" style="width:660px;">
    <span class="red"><?php echo $detalii_hotel['denumire']; ?></span>
    <img src="/images/spacer.gif" class="stele-mari-<?php echo $detalii_hotel['stele']; ?>" alt="numar de stele" />
    <span class="smaller-07em"><?php echo $detalii['denumire_scurta']; ?></span>
  </h1>
  
  <div class="float-right clearfix" style="padding:10px 0 0 0;">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  </div>
  
  <br class="clear">
  
  <div class="Hline"></div>

  <?php if($detalii['valabila']=='nu') {
	  echo '<br /><br /><h2 class="text-center red" style="font-size:2.5em;">ACEASTA OFERTA NU ESTE VALABILA !</h2>';
	  echo '<br /><div class="text-center" style="font-weight:bold; font-size:2.5em;">Oferta curenta pentru <a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'" class="link-red">'.$detalii_hotel['denumire'].'</a></div><br /><br /><br /><br />';
  } ?>

<div class="NEW-column-left1">

  <div class="NEW-adresa bigger-11em">
    <?php echo $detalii_hotel['adresa'].' ';
	if($detalii_hotel['nr']) echo 'nr. '.$detalii_hotel['nr'].', ';
	echo $detalii_hotel['localitate'].', ';
	echo $detalii_hotel['tara'];
	if($detalii_hotel['distanta']) {
		echo ', <strong>'.$detalii_hotel['distanta'].'m de '.$detalii_hotel['distanta_fata_de'].'</strong>, ';
    }
	if($detalii_hotel['latitudine'] && $detalii_hotel['longitudine'] && $detalii_hotel['latitudine']<> 0 && $detalii_hotel['longitudine'] <> 0) { ?>
		(<a href="#harta" class="link"><strong>Arata harta</strong></a>)
	<?php } ?>
  </div>
    
  <br class="clear" />
  
  <div>
    <div class="NEW-gallery">
      <div class="single-img">
        <div class="images" style="width:660px; height:350px; background-size:660px; background-position:center center; background-image:url(/img_prima_hotel/<?php echo $detalii_hotel['poza1']; ?>);"></div>
      </div>
    </div>
  </div>

  <div>
    <div class="NEW-gallery">
<?php
for($i=1;$i<=8;$i++) {
if($detalii_hotel['poza'.$i]) { ?>    
    <a href="/img_prima_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" onmouseover="document.getElementById('poza_principala').src='/img_mediu_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>';return false;" class="gallery"><img src="/thumb_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" width="75" alt="<?php echo $detalii_hotel['denumire']." imagine ".$i; ?>" class="images" /></a>
<?php }
} ?>
	</div>
  </div>

</div>
<div class="NEW-column-right1">
  
  <div class="text-center" style="background:#d60808; padding:15px 10px; margin:25px 0 0 0; font-size:24px; font-weight:bold; color:#FFF; letter-spacing:1px; text-shadow:1px 1px 1px #333;">Oferta Saptamanii</div>
  
  <div class="info-scurte-new" style="margin-top:10px;">
    <ul>
      <li>Cod oferta: <strong><?php echo 'OSE'.$id_oferta; ?></strong></li>
      <?php if($detalii['nr_nopti']>1) { ?><li>Sejur: <strong><?php echo nopti($detalii['nr_nopti']); ?></strong></li><?php } ?>
      <li>Masa: <strong><?php echo $detalii['masa']; ?></strong></li>
      <li>Transport: <strong><?php echo $detalii['transport']; ?></strong></li>
      <?php if($detalii_hotel['distanta']) { ?><li><?php echo 'Distanta fata de <strong>'.$detalii_hotel['distanta_fata_de'].'</strong>: <strong>'.$detalii_hotel['distanta'].' m</strong>'; ?></li><?php } ?>
	<?php if(sizeof($detalii['denumire_v1'])>'0') { ?>
      <?php foreach($detalii['denumire_v1'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value)); if($detalii['value_v1'][$key]) { echo ' - <strong>'.$detalii['value_v1'][$key].' '.moneda($detalii['moneda_v1'][$key]).'</strong>'; } ?></li>
      <?php } ?>
	<?php } ?>
    </ul>

    <div class="tarif">Tarif de la <span class="pret red"><?php echo new_price($preturi['minim']['pret']); ?></span> <span class="exprimare red"><?php echo moneda($detalii['moneda']); ?></span> pe <?php echo $detalii['exprimare_pret']; ?></div>
    
    <a href="#detalii-oferta" class="vezi-tarife" onclick="jQuery('#ofTabs').tabs('option', 'active', 0 );" rel="nofollow">Vezi toate tarifele</a>
  </div>
  
</div>

<br class="clear" />

<div class="NEW-column-full2">
  
	<div id="detalii-oferta">

<div class="NEW-column-left1">
  
  <div class="NEW-detalii-oferta">

	<?php if($preturi['camera']>0) {
	$early_inclus='nu'; ?>  

    <div style="overflow:auto;">
    <table class="tabel-preturi" style="width:100%;">
      <thead>
      <tr>
        <th class="text-center" style="width:150px;"><?php if($detalii['tip_preturi']=='plecari') echo 'Data plecarii'; else echo 'Perioade'; ?></th>
        <?php $g=0;
		foreach($preturi['camera'] as $key_cam=>$value_cam) {
			$g++;
			if(sizeof($preturi['camera'])==$g) {
				$class='text-center last';
				$g=0;
			} else $class='text-center'; ?>
        	<th class="<?php echo $class; ?>"><?php echo $value_cam; ?></th>
        <?php } ?>
      </tr>
      </thead>
      <tbody>
      <?php $nr_t=0;
	  foreach($preturi['data_start'] as $key_d=>$data_start) {
		  $nr_t++;
		  if($nr_t%2==1) $class_tr='impar'; else $class_tr='par';
		  $numar_minim_nopti='';
		  if(sizeof($detalii['data_start_nr_min'])>'0') {
			  foreach($detalii['data_start_nr_min'] as $key_nm=>$value_nm) {
				  if($value_nm<=$preturi['data_start_normal'][$key_d]) {
					  if($preturi['data_end_normal'][$key_d] && $preturi['data_end_normal'][$key_d]<>'0000-00-00') {
						  if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_end_normal'][$key_d]) {
							  $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
						  }
					  } else {
						  if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_start_normal'][$key_d]) {
							  $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
						  }
					  }
				  }
			  }
		  } ?>
          <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
            <th class="text-left nowrap">
			<?php if(sizeof($preturi['include_tip'][$key_d])>0) {
				echo '<div class="text-right icon-tem">';
				foreach($preturi['include_tip'][$key_d] as $key_inc=>$include) echo '<a href="'.$sitepath.$_REQUEST['tara'].'/'.$_REQUEST['localitate'].'/'.fa_link($include['denumire']).'-'.$include['id_oferta'].'.html"><img src="'.$imgpath.'/oferte/icon_tematica_mica_'.$include['id_tip_oferta'].'.png" /></a>';
				echo '</div>';
			}
			echo $data_start;
			if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') echo ' - '.$preturi['data_end'][$key_d];
			?><br />
            <?php if(strlen($numar_minim_nopti)>4) echo '<em>'.$numar_minim_nopti.'</em><br />'; ?>
            </th>
			<?php $g=0;
			foreach($preturi['camera'] as $key_cam=>$value_cam) {
				$g++;
				if(sizeof($preturi['camera'])==$g) {
					$class='text-center pret last';
					$g=0;
				} else $class='text-center pret';
				if(sizeof($preturi['camera'])=="1") $wdt='70%';
				elseif(sizeof($preturi['camera'])=="2") $wdt='35%';
				elseif(sizeof($preturi['camera'])=="3") $wdt='23%';
				elseif(sizeof($preturi['camera'])=="4") $wdt='17%';
				elseif(sizeof($preturi['camera'])=="5") $wdt='14%';
				elseif(sizeof($preturi['camera'])=="6") $wdt='11%';
			?>
            <td  <?php echo 'class="'.$class.'"'; ?> style="width:<?php echo $wdt; ?>">
			<?php if($preturi['pret'][$key_d][$key_cam]) {
				$pret_afisare = new_price($preturi['pret'][$key_d][$key_cam]).' ';
				if($preturi['moneda'][$key_d][$key_cam]=='EURO') {
					$moneda_afisare = '&euro;';
					$valuta='da';
				} elseif($preturi['moneda'][$key_d][$key_cam]=='USD') {
					$moneda_afisare = '$';
					$valuta='da';
				} else $moneda_afisare = $preturi['moneda'][$key_d][$key_cam];
				if((soldout($id_oferta, $key_cam, $data_start)>0) and !($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000')) {
					echo '<span class="soldout2 red" title="Aceasta perioada nu este disponibila. Camera se afla in STOP SALES.">STOP SALES<span class="pretz">'.$pret_afisare.$moneda_afisare.'</span></span>';
				} else {
					echo $pret_afisare.$moneda_afisare;
				}
			} else echo '-'; ?>
            </td>
			<?php } ?>
          </tr>
      <?php } ?>
      </tbody>
    </table>
    </div>
    <?php } ?>
	
    <div class="grey2 bold" style="text-align:right; padding:0 5px 10px 5px;">* tarifele sunt exprimate in <span class="bigger-13em blue"><?php echo $detalii['moneda']; ?> pe <?php echo $detalii['exprimare_pret']; ?></span></div>

	<?php if($detalii['rezervare_online']=='da') {
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/calculare_pret_new.php");
	} else {
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/cerere_detalii.php");
		
		if(!is_bot() and isset($err_logare_admin) and $detalii['valabila']=='da') {
			erori_disponibilitate($id_oferta, $id_hotel, $plecdata, $pleczile, $adulti, $copii);
		}
		
	} ?>
    
    <br class="clear"><br><br>
    
	<?php
	$sel_detcam = "SELECT camere_hotel.detalii, tip_camera.denumire FROM camere_hotel INNER JOIN tip_camera ON tip_camera.id_camera = camere_hotel.id_camera WHERE id_hotel='".$detalii['id_hotel']."' AND (detalii<>NULL OR detalii<>'')";
	$que_detcam = mysql_query($sel_detcam) or die(mysql_error());
	if(mysql_num_rows($que_detcam)>'0') {
	?>
    <div class="section">
      <h2 class="blue underline"><img src="<?php echo $imgpath; ?>/icon_section_camere.png" style="vertical-align:middle;" alt="Detalii camere" /> Detalii camere <?php echo $detalii_hotel['denumire']?></h2>
		<?php while($row_detcam = mysql_fetch_array($que_detcam)){
            echo '<h3 class="black">'.$row_detcam['denumire'].'</h3>';
            echo '<div class="content">'.nl2br($row_detcam['detalii']).'</div>';
            echo '<div class="clear-line"></div>';
        } ?>
    </div>
    <?php } ?>
    
    <?php if(sizeof($detalii['denumire_v3'])>'0' || sizeof($detalii['denumire_v4'])>'0' || sizeof($excursii)>'0') { ?>
    <div class="section">
      <?php /*?><?php if(sizeof($detalii['denumire_v3'])>'0') { ?>
      <h3 class="black">Suplimente</h3>
      <div class="content">
        <ul>
        <?php foreach($detalii['denumire_v3'] as $key=>$value) { ?>
        <li><?php echo ucfirst($value); if($detalii['value_v3'][$key]) { echo ' - <strong>'.$detalii['value_v3'][$key].' '.moneda($detalii['moneda_v3'][$key]).' '.$detalii['exprimare_v3'][$key].' '.$detalii['pasager_v3'][$key].'</strong>'; } if($detalii['obligatoriu_v3'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>'; else echo ' - <em class="blue">Optional</em>'; ?></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?><?php */?>
	  <?php $excursii=$det->get_excursii($id_oferta);
	  if(sizeof($excursii)>0) { ?>
      <h3 class="red">Excursii optionale</h3>
      <div class="content">
        <ul>
        <?php foreach($excursii as $key_e=>$value_e) { ?>
        <li><a href="<?php echo $sitepath.'excursii-optionale/'.fa_link($detalii_hotel['tara']).'/'.fa_link_oferta($value_e['denumire']).'-'.$value_e['id_excursie'].'.html'; ?>" rel="nofollow" class="excursii_detalii link-blue"><?php echo $value_e['denumire']; if($value_e['pret']) { echo ' - <em class="red">'.$value_e['pret'].' '; echo moneda($value_e['moneda']); echo '</em> - '.ucwords($value_e['tip']); } else echo ' - <em class="red bold">'.ucwords($value_e['tip']).'</em>'; ?></a></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
    </div>
	<?php } ?>

    <br class="clear" />

	<?php if($id_oferta) {
	$valuta='nu';
	if($detalii["descriere_oferta"]) { ?>
    <div class="section">
      <h2 class="section-title" style="margin-bottom:5px;"><?php echo $detalii["denumire"]; ?></h2>
      <?php afisare_frumos(schimba_caractere($detalii["descriere_oferta"]), 'nl2p'); ?>
    </div>
    <?php } ?>

    <br class="clear" />
  
<?php } ?>
    
    <br class="clear" /><br />
    
      <?php if($detalii_hotel['new_descriere']) { ?>
  
      <div class="section" style="margin:0;">
        <?php if($detalii_hotel["comentariul_nostru"]) { ?>
        <p class="black"><strong><u>Comentariul nostru</u>:</strong> <?php afisare_frumos(schimba_caractere($detalii_hotel["comentariul_nostru"]), ''); ?></p>
        <br />
        <?php } ?>
        <?php afisare_frumos(schimba_caractere($detalii_hotel["new_descriere"]), 'nl2p'); ?>
        <div id="detHotNew-container">
          <?php if($detalii_hotel["new_camera"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Camera</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_camera"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_teritoriu"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Teritoriu</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_teritoriu"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_relaxare"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Relaxare si sport</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_relaxare"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_pentru_copii"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Pentru copii</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_pentru_copii"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_plaja"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Plaja</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_plaja"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
        </div>
      </div>
  
      <?php } else { ?>
      
      <div class="section" style="margin:0;">
        <div class="pad10"><?php afisare_frumos(schimba_caractere($detalii_hotel["descriere_scurta"]), 'nl2p'); afisare_frumos(schimba_caractere($detalii_hotel["descriere"]), 'nl2p'); ?></div>
  
      <?php if($detalii_hotel["general"] || $detalii_hotel["servicii"] || $detalii_hotel["internet"] || $detalii_hotel["parcare"]) { ?>
        <?php if($detalii_hotel["general"]) { ?>
        <h3 class="black">General</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["general"], 'nl2br'); ?></div>
        <div class="clear-line"></div> 
        <?php }
        if($detalii_hotel["servicii"]) { ?>
        <h3 class="black">Servicii</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["servicii"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["internet"]) { ?>
        <h3 class="black">Internet</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["internet"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["parcare"]) { ?>
        <h3 class="black">Parcare</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["parcare"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php } ?>
        <?php if( ($detalii_hotel["check_in"]) or ($detalii_hotel["check_out"]) ) { ?>
        <h3 class="black">Check-in / check-out</h3>
        <div class="content"><?php echo $detalii_hotel["check_in"]; ?> / <?php echo $detalii_hotel["check_out"]; ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["info_copii"]) { ?>
        <h3 class="black">Copii si paturi suplimentare</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["info_copii"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["accepta_animale"]) { ?>
        <h3 class="black">Animale companie</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["accepta_animale"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php } ?>
        <br class="clear" /><br />
        <div class="chenar-info NEW-orange NEW-round8px">Clasificarea pe stele a unitatilor de cazare din program este cea atribuita in mod oficial de Ministerul Turismului din <?php echo $detalii_hotel['tara']; ?> conform standardelor proprii.</div>
      <?php } ?>
      </div>
    
      <?php } ?>
      
      <br class="clear"><br>
  
   </div>

  <br class="clear" />
  
</div>
<div class="NEW-column-right1">

  <div class="right-serv" style="margin:7px 0 0 0;">

	<?php if($detalii['conditii_plata']) { ?>
    <div>
      <h3 class="black underline">Conditii de plata</h3>
      <ul>
        <?php echo afisare_frumos($detalii['conditii_plata'], 'nl2li'); ?>
      </ul>
    </div>
	<?php } ?>
    
	<?php if($detalii['conditii_anulare']) { ?>
    <div>
      <h3 class="black underline">Conditii de anulare</h3>
      <ul>
        <?php echo afisare_frumos($detalii['conditii_anulare'], 'nl2li'); ?>
      </ul>
    </div>
	<?php } ?>

	<?php if(sizeof($detalii['denumire_v1'])>'0') { ?>
    <div class="separator">
      <h3 class="black underline">Tarifele includ</h3>
      <ul>
      <?php foreach($detalii['denumire_v1'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value)); if($detalii['value_v1'][$key]) { echo ' - <strong>'.$detalii['value_v1'][$key].' '.moneda($detalii['moneda_v1'][$key]).'</strong>'; } ?></li>
      <?php } ?>
      </ul>
    </div>
	<?php } ?>

	<?php if(sizeof($detalii['denumire_v2'])>'0' or sizeof($detalii['denumire_v3'])>'0') { ?>
    <div class="separator">
      <h3 class="black underline">Neincluse in tarif</h3>
      <ul>
      <?php if(sizeof($detalii['denumire_v2'])>'0') {
		  foreach($detalii['denumire_v2'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value));
          if($detalii['data_start_v2'][$key]) {
              echo ' <em>('.date("d.m.Y", strtotime($detalii['data_start_v2'][$key])).' - '.date("d.m.Y", strtotime($detalii['data_end_v2'][$key])).')</em>';
          }
          if($detalii['value_v2'][$key]) {
              echo ' - <strong>'.$detalii['value_v2'][$key].' '.moneda($detalii['moneda_v2'][$key]).' '.$detalii['exprimare_v2'][$key].' '.$detalii['pasager_v2'][$key].'</strong>';
          }
          if($detalii['obligatoriu_v2'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>';
              else echo ' - <em class="blue">Optional</em>'; ?></li>
      <?php }
	  } ?>
	  <?php if(sizeof($detalii['denumire_v3'])>'0') {
		  foreach($detalii['denumire_v3'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value));
          if($detalii['data_start_v3'][$key]) {
              echo ' <em>('.date("d.m.Y", strtotime($detalii['data_start_v3'][$key])).' - '.date("d.m.Y", strtotime($detalii['data_end_v3'][$key])).')</em>';
          }
          if($detalii['value_v3'][$key]) {
              echo ' - <strong>'.$detalii['value_v3'][$key].' '.moneda($detalii['moneda_v3'][$key]).' '.$detalii['exprimare_v3'][$key].' '.$detalii['pasager_v3'][$key].'</strong>';
          }
          if($detalii['obligatoriu_v3'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>';
              else echo ' - <em class="blue">Optional</em>'; ?></li>
      <?php }
	  } ?>
      </ul>
    </div>
    <?php } ?>

	<?php $aeroport=array(); if($detalii['transport']=='Avion') $aeroport=$det->get_avion_sejur($id_oferta);
    if(sizeof($aeroport)>0) { ?>
    <div class="separator">
      <h3 class="black underline">Orar de zbor</h3>
      <div style="padding:5px 0;">
        <img src="/images/avion/<?php echo $aeroport[0]['companie']; ?>.jpg" alt="<?php echo $aeroport[0]['denumire_companie']; ?>" style="margin:0 5px 0 13px; vertical-align:middle;"> <strong class="underline"><?php echo $aeroport[0]['denumire_companie']; ?></strong>
        <?php $tip_c=''; $tip_v=''; $i=0; foreach($aeroport as $key_a=>$value_a) { $i++; ?>
          <?php if($tip_c<>$value_a['tip']) { ?>
          <?php $tip_c=$value_a['tip']; $tip_v=$value_a['tip']; } ?>
          <ul style="margin-bottom:5px;">
            <li><strong>Plecare:</strong> <?php echo $value_a['denumire_aeroport_plecare'].' ('.$value_a['denumire_loc_plecare'].') - <strong>'.$value_a['ora_plecare'].'</strong>'; ?></li>
            <li><strong>Sosire:</strong> <?php echo $value_a['denumire_aeroport_sosire'].' ('.$value_a['denumire_loc_sosire'].') - <strong>'.$value_a['ora_sosire'].'</strong>'; ?></li>
          </ul>
          <?php if($i<sizeof($aeroport)) echo '&nbsp;&nbsp;&nbsp; --------'; ?>
        <?php } ?>
      </div>
    </div>
    <?php } ?>

	<?php if($detalii['nota']) { ?>
    <div class="separator">
      <h3 class="black underline">Nota</h3>
      <div class="italic" style="padding:5px 13px;">
        <?php afisare_frumos(schimba_caractere(trim($detalii['nota'])), 'nl2br'); ?>
      </div>
    </div>
    <?php } ?>
    
  </div>
  
  <br class="clear"><br>
  
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/new_hotel.php"); ?>
  <?php if(isset($_GET['plecdata'])) include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta_new.php"); ?>

</div>
    
    </div>
    
      <div id="harta-localizare" style="width:100%; height:500px;"></div>
      <div class="text-center smaller-09em grey">Poziționarea hotelurilor pe hartă este cu titlu informativ.</div>

      <br class="clear"><br>
  
</div>

</div>
<br class="clear" />
