<?php session_start(); ob_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/send_mail.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/login.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');

$oferte_rec=new AFISARE_SEJUR_NORMAL();
$oferte_rec->setAfisare(1, 3);
$oferte_rec->setRecomandata('da');
$oferte_rec->setRandom(1);
$oferte_rec->setOfertaSpeciala($_SERVER['DOCUMENT_ROOT']."/templates/new_oferte_recomandate.tpl");

if($_REQUEST['tari']) $oferte_rec->setTari($_REQUEST['tari']);
if($_REQUEST['zone']) $oferte_rec->setZone($_REQUEST['zone']);
if($_REQUEST['tari']=='romania') {
	if($_REQUEST['oras']) $oferte_rec->setOrase($_REQUEST['oras']);
}
if($_REQUEST['tip']) $oferte_rec->setTipOferta($_REQUEST['tip']);
if($_REQUEST['transport']) $oferte_rec->setTransport($_REQUEST['transport']);

$nr_hoteluri=$oferte_rec->numar_oferte();
if(($nr_hoteluri>0) and !isset($_REQUEST['nume_hotel'])) {
	$cuvant_cheie = $_REQUEST['keyword'];
?>
<div class="ofRec NEW-round6px">
  <div class="tit blue">Recomandari hoteluri pentru <?php echo $cuvant_cheie; ?></div>
  <?php $oferte_rec->afiseaza();?>
  <br class="clear" />
</div>
<?php } ?>
