<div class="NEW-chn-oferta-green NEW-round8px mar5">
  <h2 class="section-title-green" style="margin-bottom:10px;">Oferta transport cu autocarul pe litoralul bulgaresc de Paste si 1 Mai</h2>
  <h3>Plecare din Bucuresti pentru <span class="red">Paste: 13 aprilie</span>, retur din Bulgaria pe 16 aprilie (3 nopti cazare)</h3>
  <h3>Plecare din Bucuresti pentru <span class="red">1 Mai: 28 aprilie</span>, retur din Bulgaria pe 1 mai (3 nopti cazare)</h3>
  <h2 class="blue"><strong>PRET :</strong> <strong class="red">35 EUR/adult</strong> statiunile Balchik, Albena, Golden Sands, St. Konstantin</h2>
  <h2 class="blue"><strong>PRET :</strong> <strong class="red">45 EUR/adult</strong> pentru statiunile: Varna, Obzor, Sunny Beach, Nessebar</h2>
  <p class="pad5">Plecarile se fac la orele <strong>06.30 - Gara de Nord (Bucuresti)</strong> la coloane</p>
  <p class="pad5">Distribuirea locurilor se face in ordinea sosirii, iar imbarcarea pasagerilor se va face dupa confirmarea pe diagrama de rezervari aflata in posesia soferilor.</p>
  <p class="pad5">Copiii 0-2 ani au gratuitate dar nu beneficiaza de loc in autocar.</p>
</div>
<br class="clear"><br>
