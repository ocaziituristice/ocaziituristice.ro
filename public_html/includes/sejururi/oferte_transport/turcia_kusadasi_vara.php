<div class="NEW-chn-oferta-green NEW-round8px mar5">
  <h2 class="section-title-green" style="margin-bottom:10px;">Oferta Transport cu autocarul in Kusadasi vara 2013</h2>
  <h3>Plecari in fiecare vineri in perioada 7 iunie - 20 septembrie</h3>
  <p class="pad5"><strong>Traseu:</strong> Bucuresti - Giurgiu - Russe - Veliko Tarnovo - Stara Zagora - Cpt. Andreevo - Edirne - Canakkale - Kusadasi si retur</p>
  <h2><strong>PRET dus-intors:</strong> <strong class="red">80 EUR/adult</strong> ; <strong class="red">70 EUR/copil 2-7 ani</strong></h2>
  <p class="pad5">Plecarile se fac la orele <strong>10.00 - in fata restaurantului Cina (Bucuresti)</strong>, langa Ateneul Roman.</p>
  <p class="pad5"><strong>Tariful nu include:</strong><br>
    - taxa de trecere cu ferryboat-ul la Canakkale - 3 euro/persoana/sens (se achita in autocar)<br>
    - transfer optional pana la / de la hotelul ales - la cerere</p>
  <br>
</div>
<br class="clear"><br>
