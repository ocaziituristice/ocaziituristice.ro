<div class="NEW-chn-oferta-green NEW-round8px mar5">
  <h2 class="section-title-green" style="margin-bottom:10px;">Oferta Transport cu autocarul pe litoralul bulgaresc vara 2013</h2>
  <h3>Plecari zilnice in perioada 20 mai - 15 septembrie</h3>
  <p class="pad5"><strong>Statiuni:</strong> Balchik (doar luni), Albena, Nisipurile de Aur, Sf. Constantin si Elena, Varna, Obzor, Sunny Beach, Nessebar</p>
  <h2><strong>PRET dus-intors:</strong> <strong class="red">30 EUR/adult</strong> ; <strong class="red">25 EUR/copil 2-12 ani</strong></h2>
  <p class="pad5">Plecarile se fac la orele <strong>06.00 - Gara de Nord (Bucuresti)</strong> la coloane</p>
  <p class="pad5">Distribuirea locurilor se face in ordinea sosirii, iar imbarcarea pasagerilor se va face dupa confirmarea pe diagrama de rezervari aflata in posesia soferilor.</p>
  <p class="pad5">Copiii 0-2 ani au gratuitate dar nu beneficiaza de loc in autocar.</p>
</div>
<br class="clear"><br>
