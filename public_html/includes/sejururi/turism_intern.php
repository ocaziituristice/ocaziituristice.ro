<div id="NEW-destinatie">

  <h1 class="blue float-left">Sejur Romania, Cazare Romania</h1>
  
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  
  <br class="clear">
  <div class="Hline"></div>

<div class="NEW-column-left1">

  <!--<a href="<?php echo $sitepath; ?>sejur-romania/litoral/" title="Litoral"><img src="<?php echo $imgpath; ?>/banner/banner_litoral_romania_30-01-2012.gif" class="NEW-round8px" alt="Litoral Romania" /></a>
  
  <br class="clear"><br>-->
  
<?php /*** 1 coloana ***/
$sel_zone = "SELECT
Count(oferte.id_oferta) AS numar,
zone.id_zona,
zone.denumire,
zone.tpl_romania
FROM oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '1'
AND oferte.last_minute = 'nu'
AND zone.tpl_romania = '1'
AND zone.denumire = 'Litoral'
GROUP BY zone.id_zona
ORDER BY numar DESC
";
$que_zone=mysql_query($sel_zone) or die(mysql_error());
while($row_zone=mysql_fetch_array($que_zone)) {
?>
  <div class="chnZoneRomania" style="width:646px;">
     <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" title="Sejur <?php echo $row_zone['denumire']; ?>" class="float-left">
      <span class="titlu"><?php echo $row_zone['denumire']; ?></span>
      <img src="<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.jpg" alt="<?php echo $row_zone['denumire']; ?>" class="NEW-round6px" width="200" height="130">
    </a>
    <div class="links2">
    <?php $sel_loc="SELECT
	Count(oferte.id_oferta) AS numar,
	localitati.denumire
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND zone.id_zona = '".$row_zone['id_zona']."'
	AND oferte.last_minute = 'nu'
	GROUP BY localitati.id_localitate
	ORDER BY numar DESC
	";
	$que_loc=mysql_query($sel_loc) or die(mysql_error());
	while($row_loc=mysql_fetch_array($que_loc)) {
	?>
      <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/".fa_link($row_loc['denumire'])."/"; ?>"><?php echo $row_loc['denumire']; ?></a>
    <?php } @mysql_free_result($que_loc); ?>
    </div>
    <br class="clear">
  </div>
<?php } @mysql_free_result($que_zone); ?>
  <br class="clear">

<?php /*** 2 coloane ***/
$sel_zone = "SELECT
Count(oferte.id_oferta) AS numar,
zone.id_zona,
zone.denumire,
zone.tpl_romania
FROM oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '1'
AND oferte.last_minute = 'nu'
AND zone.tpl_romania = '2'
GROUP BY zone.id_zona
ORDER BY numar DESC
";
$que_zone=mysql_query($sel_zone) or die(mysql_error());
while($row_zone=mysql_fetch_array($que_zone)) {
?>
  <div class="chnZoneRomania" style="width:311px;">
     <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" title="Sejur <?php echo $row_zone['denumire']; ?>" class="float-left">
      <span class="titlu"><?php echo $row_zone['denumire']; ?></span>
      <img src="<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.jpg" alt="<?php echo $row_zone['denumire']; ?>" class="NEW-round6px" width="200" height="130">
    </a>
    <div class="links1">
    <?php $sel_loc="SELECT
	Count(oferte.id_oferta) AS numar,
	localitati.denumire
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND zone.id_zona = '".$row_zone['id_zona']."'
	AND oferte.last_minute = 'nu'
	GROUP BY localitati.id_localitate
	ORDER BY numar DESC
	";
	$que_loc=mysql_query($sel_loc) or die(mysql_error());
	while($row_loc=mysql_fetch_array($que_loc)) {
	?>
      <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/".fa_link($row_loc['denumire'])."/"; ?>"><?php echo $row_loc['denumire']; ?></a>
    <?php } @mysql_free_result($que_loc); ?>
    </div>
    <br class="clear">
  </div>
<?php } @mysql_free_result($que_zone); ?>
  <br class="clear">

<?php /*** 1 coloana ***/
$sel_zone = "SELECT
Count(oferte.id_oferta) AS numar,
zone.id_zona,
zone.denumire,
zone.tpl_romania
FROM oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '1'
AND oferte.last_minute = 'nu'
AND zone.tpl_romania = '1'
AND zone.denumire = 'Statiuni Balneare'
GROUP BY zone.id_zona
ORDER BY numar DESC
";
$que_zone=mysql_query($sel_zone) or die(mysql_error());
while($row_zone=mysql_fetch_array($que_zone)) {
?>
  <div class="chnZoneRomania" style="width:646px;">
     <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" title="Sejur <?php echo $row_zone['denumire']; ?>" class="float-left">
      <span class="titlu"><?php echo $row_zone['denumire']; ?></span>
      <img src="<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.jpg" alt="<?php echo $row_zone['denumire']; ?>" class="NEW-round6px" width="200" height="130">
    </a>
    <div class="links2">
    <?php $sel_loc="SELECT
	Count(oferte.id_oferta) AS numar,
	localitati.denumire
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND zone.id_zona = '".$row_zone['id_zona']."'
	AND oferte.last_minute = 'nu'
	GROUP BY localitati.id_localitate
	ORDER BY numar DESC
	";
	$que_loc=mysql_query($sel_loc) or die(mysql_error());
	while($row_loc=mysql_fetch_array($que_loc)) {
	?>
      <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/".fa_link($row_loc['denumire'])."/"; ?>"><?php echo $row_loc['denumire']; ?></a>
    <?php } @mysql_free_result($que_loc); ?>
    </div>
    <br class="clear">
  </div>
<?php } @mysql_free_result($que_zone); ?>
  <br class="clear">

<?php /*** 2 coloane ***/
$sel_zone = "SELECT
Count(oferte.id_oferta) AS numar,
zone.id_zona,
zone.denumire,
zone.tpl_romania
FROM oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '1'
AND oferte.last_minute = 'nu'
AND zone.tpl_romania IS NULL
GROUP BY zone.id_zona
ORDER BY numar DESC
";
$que_zone=mysql_query($sel_zone) or die(mysql_error());
while($row_zone=mysql_fetch_array($que_zone)) {
?>
  <div class="chnZoneRomania" style="width:311px;">
     <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" title="Sejur <?php echo $row_zone['denumire']; ?>" class="float-left">
      <span class="titlu"><?php echo $row_zone['denumire']; ?></span>
      <img src="<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.jpg" alt="<?php echo $row_zone['denumire']; ?>" class="NEW-round6px" width="200" height="130">
    </a>
    <div class="links1">
    <?php $sel_loc="SELECT
	Count(oferte.id_oferta) AS numar,
	localitati.denumire
	FROM oferte
	INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
	INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
	INNER JOIN zone ON localitati.id_zona = zone.id_zona
	WHERE oferte.valabila = 'da'
	AND hoteluri.tip_unitate <> 'Circuit'
	AND zone.id_zona = '".$row_zone['id_zona']."'
	AND oferte.last_minute = 'nu'
	GROUP BY localitati.id_localitate
	ORDER BY numar DESC
	";
	$que_loc=mysql_query($sel_loc) or die(mysql_error());
	while($row_loc=mysql_fetch_array($que_loc)) {
	?>
      <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/".fa_link($row_loc['denumire'])."/"; ?>" class="link-black"><?php echo $row_loc['denumire']; ?></a>
    <?php } @mysql_free_result($que_loc); ?>
    </div>
    <br class="clear">
  </div>
<?php } @mysql_free_result($que_zone); ?>
  <br class="clear">

<?php /*** 3 coloane ***/
/*$sel_zone = "SELECT
Count(oferte.id_oferta) AS numar,
zone.denumire,
zone.tpl_romania
FROM oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND zone.id_tara = '1'
AND oferte.last_minute = 'nu'
AND zone.tpl_romania IS NULL
GROUP BY zone.id_zona
ORDER BY numar DESC
";
$que_zone=mysql_query($sel_zone) or die(mysql_error());
while($row_zone=mysql_fetch_array($que_zone)) {*/
?>
  <?php /*?><div class="chnZoneRomania" style="width:200px;">
    <a href="<?php echo "/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" title="Sejur <?php echo $row_zone['denumire']; ?>">
      <span class="titlu"><?php echo $row_zone['denumire']; ?></span>
      <img src="<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.jpg" alt="<?php echo $row_zone['denumire']; ?>" class="NEW-round6px" width="200" height="130" />
    </a>
  </div>
<?php } @mysql_free_result($que_zone); ?><?php */?>

  <br class="clear"><br>

  <div class="text-dreapta pad10">
    <h2 class="green">Romania, peisaje pitoresti si obiective importante</h2>
	<p>Turismul in Romania se concentreaza asupra peisajelor naturale si a istoriei sale bogate.</p>
	<img src="<?php echo $imgpath; ?>/texte_romania.jpg" width="160" height="120" alt="Romania" style="float:left; margin-right:10px;">
	<p>Romania are un relief variat, incluzind impaduritii Munti Carpati, coasta Marii Negre si Delta Dunarii, cea mai bine pastrata delta europeana.</p>
	<p>Principalele atractii turistice de vara sunt statiunile de pe litoral precum Mamaia, Neptun, Olimp, Mangalia, Saturn si Venus. Statiunea Mamaia fiind cea mai cautata statiune de pe litoralul Marii Negre.</p>
	<p>Pe timpul iernii statiunile de schi de pe <a href="<?php echo $path; ?>/sejur-romania/valea-prahovei/" title="Valea Prahovei">Valea Prahovei</a> si <a href="<?php echo $path; ?>/sejur-romania/poiana-brasov/" title="Poiana Brasov">Poiana Brasov</a>, ofera sejururi de neuitat, partii consacrate de ski precum si pensiuni si hoteluri cu pachete atractive.</p>
	<img src="<?php echo $imgpath; ?>/texte_sighisoara5.jpg" width="160" height="127" alt="Sighisoara" style="float:right; margin-left:10px;">
    <p>Turistii atrasi de istorie sunt invitati sa alega sejururi in Sighisoara, Sibiu, Brasov, Targu Mures. Mari atractii pentru turisti fiind cetatile si castele medievale (Cetatea Sighisoara, Cetatea Neamtului, Cetatea Deva, Castelul Bran, Castelul Peles si multe altele) si aspectul arhitectonic al oraselor.</p>
	<p>Pentru credinciosi si iubitori de lacase de cult, intalnim pe intreg teritoriul Romaniei, manastiri cu un design uimitor pline de culoare si de viata. Printe ele se numara Putna, Moldovita, Sucevita, Voronet, Humor si Arbore. Manastiri unice in Europa si incluse de UNESCO in tezaurul artei universale.</p>
  </div>

</div>

<div class="NEW-column-right1">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/dreapta_turism_intern.php"); ?>
</div>

</div>

<br class="clear"><br>
