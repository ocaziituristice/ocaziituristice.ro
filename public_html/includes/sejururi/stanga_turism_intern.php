<?php $sel_zone = "SELECT
zone.denumire,
zone.maparea
FROM
oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
inner Join usernames ON oferte.id_utilizator = usernames.id
WHERE
oferte.valabila = 'da' and zone.id_tara = '1'
and usernames.end_valabilitate_abonament >= '$azi'
GROUP BY zone.id_zona "; ?>

<h1 class="blue">Sejururi Turism Intern, Cazare Romania</h1>

<?php /*?><div id="detaliiFormulare">

<script type="text/javascript">
function newImage(arg) {
        if (document.images) {
                rslt = new Image();
                rslt.src = arg;
                return rslt;
        }
}
function changeImages() {
        if (document.images && (preloadFlag == true)) {
                for (var i=0; i<changeImages.arguments.length; i+=2) {
                        document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
                }
        }
}
var preloadFlag = false;
function preloadImages() {
        if (document.images) {
<?php while($row_zone=mysql_fetch_array($que_zone)) { ?>
buttons_01_over = newImage("<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.gif");
<?php } ?>
			buttons = newImage("<?php echo $imgpath; ?>/romania/harta_romania.gif");
			preloadFlag = true;
			}
		}
</script>

<div style="background:url(<?php echo $imgpath; ?>/romania/harta_romania.gif) left top no-repeat;" onload="preloadImages();">
  <img src="<?php echo $imgpath; ?>/spacer.gif" width="650" height="465" border="0" usemap="#mapRomania" name="regiuni" />
  <map name="mapRomania" id="mapRomania">
   <?php $que_zone=mysql_query($sel_zone) or die(mysql_error());
   while($row_zone=mysql_fetch_array($que_zone)) { ?>
    <area shape="poly" coords="<?php echo $row_zone['maparea']; ?>" href="<?php echo $path."/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" alt="<?php echo $row_zone['denumire']; ?>" title="<?php echo $row_zone['denumire']; ?>" onmouseover="changeImages('regiuni', '<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.gif'); return true;" onmouseout="changeImages('regiuni', '<?php echo $imgpath; ?>/spacer.gif'); return true;" />
    <?php } ?>
  </map>
</div>

<br class="clear" /><br />

</div><?php */?>

<?php $que_zone=mysql_query($sel_zone) or die(mysql_error());
while($row_zone=mysql_fetch_array($que_zone)) {  ?>
<div class="chnZoneRomania">
  <div class="titlu">
  <a href="<?php echo $path."/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" title="Sejur <?php echo $row_zone['denumire']; ?>"><?php echo $row_zone['denumire']; ?></a></div>
  <img src="<?php echo $imgpath; ?>/romania/<?php echo fa_link($row_zone['denumire']); ?>.jpg" alt="<?php echo $row_zone['denumire']; ?>" width="200" height="130" onclick="location.href='<?php echo $path."/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>';" style="cursor:pointer;" />
</div>
<?php } @mysql_free_result($que_zone); ?>

<?php /*?><div class="countrydivcontainer">
  <ul class="tabLink">
<?php $que_zone=mysql_query($sel_zone) or die(mysql_error());
while($row_zone=mysql_fetch_array($que_zone)) { ?>
    <li><a href="<?php echo $path."/sejur-romania/".fa_link($row_zone['denumire'])."/"; ?>" title ="Sejur <?php echo $row_zone['denumire']; ?>"><?php echo $row_zone['denumire']; ?></a></li>
	<?
   } @mysql_free_result($que_zone);
?>
  </ul>
  <br class="clear" />
  <div class="filterText">Alege zona pentru a vedea ofertele <? echo ucwords($tip); ?> din Romania</div>
</div><?php */?>

<br class="clear" /><br />

<div class="text-dreapta">
    <h2 class="green">Romania, peisaje pitoresti si obiective importante</h2>
	<p>Turismul in Romania se concentreaza asupra peisajelor naturale si a istoriei sale bogate.</p>
	<img src="<?php echo $imgpath; ?>/texte_romania.jpg" width="160" height="120" alt="Romania" style="float:left; margin-right:10px;" />
	<p>Romania are un relief variat, incluzind impaduritii Munti Carpati, <a href="<?php echo $path; ?>/sejur-romania/delta_dunarii/" title="Litoral Marea Neagra">coasta Marii Negre</a> si <a href="<?php echo $path; ?>/sejur-romania/delta_dunarii/" title="Delta Dunarii">Delta Dunarii</a>, cea mai bine pastrata delta europeana.</p>
	<p>Principalele atractii turistice de vara sunt statiunile de pe litoral precum Mamaia, Neptun, Olimp, Mangalia, Saturn si Venus. Statiunea Mamaia fiind cea mai cautata statiune de pe litoralul Marii Negre.</p>
	<p>Pe timpul iernii statiunile de schi de pe <a href="<?php echo $path; ?>/sejur-romania/valea_prahovei/" title="Valea Prahovei">Valea Prahovei</a> si <a href="<?php echo $path; ?>/sejur-romania/brasov_d_poiana_brasov/" title="Poiana Brasov">Poiana Brasov</a>, ofera sejururi de neuitat, partii consacrate de ski precum si pensiuni si hoteluri cu pachete atractive.</p>
	<img src="<?php echo $imgpath; ?>/texte_sighisoara5.jpg" width="160" height="127" alt="Sighisoara" style="float:right; margin-left:10px;" />
    <p>Turistii atrasi de istorie sunt invitati sa alega sejururi in Sighisoara, Sibiu, Brasov, Targu Mures. Mari atractii pentru turisti fiind cetatile si castele medievale (Cetatea Sighisoara, Cetatea Neamtului, Cetatea Deva, Castelul Bran, Castelul Peles si multe altele) si aspectul arhitectonic al oraselor.</p>
	<p>Pentru credinciosi si iubitori de lacase de cult, intalnim pe intreg teritoriul Romaniei, manastiri cu un design uimitor pline de culoare si de viata. Printe ele se numara Putna, Moldovita, Sucevita, Voronet, Humor si Arbore. Manastiri unice in Europa si incluse de UNESCO in tezaurul artei universale.</p>
</div>
