<div class="filters">
  <div class="blue" align="justify">Pentru o navigare mai usoara, folositi <span class="bold">filtrele de mai jos</span>:</div>
  <p>Tara</p>
  <?php $tari_f=get_tari('','','','','da'); ?>
  <select name="tara" id="tara" onchange="filtrare_get_early(this.value, '');">
   <option value="" disabled="disabled" <?php if(!$den_tara) { ?> selected="selected" <?php } ?>>Selectati</option>
    <?php if(sizeof($tari_f)>0) {
     foreach($tari_f as $key_tf=>$value_tf) { ?>
    <option value="<?php echo fa_link($value_tf); ?>" <?php if($value_tf==$den_tara) { ?> selected="selected" <?php } ?>><?php echo $value_tf; ?></option>
    <?php }
    } ?>
  </select>
  <div class="Hline"></div>
  <p>Destinatii</p>
  <select name="localitate" id="localitate" onchange="filtrare_get_early('<?php echo $link_tara; ?>', this.value);">
    <option value="">Selectati</option>
    <?php if($tara) {
    $zone=get_zone('','','',$id_tara,'da');
    if(sizeof($zone)>0) {
     foreach($zone as $id_zona1=>$value) {
      $link_z=fa_link($value); ?>
     <option value="<?php echo $link_z; ?>" <?php if($link_z==$link_zona) { ?> selected="selected" <?php } ?>><?php echo strtoupper($value); ?></option>
     <?php $loc_f=get_localitate('','',$id_zona1,$id_tara,'da');
     if(sizeof($loc_f)>0) {	   
     foreach($loc_f as $kloc_tf=>$loc_tf) {
     $link_lo=fa_link($loc_tf['denumire']); ?>
    <option value="<?php echo $link_z.'/'.$link_lo; ?>" <?php if($link_lo==$link_oras) { ?> selected="selected" <?php } ?>>&nbsp;&nbsp;&nbsp;<?php echo $loc_tf['denumire']; ?></option>
    <?php }
         }
       }
     } 
    } ?>
  </select>
  <div class="Hline"></div>
  <?php if($id_transport || $nr_stele || $nmasa || $din_luna || ($distanta && $distanta<>'toate')) { ?>
  <a href="<?php echo $link_p; ?>" class="delete"><span>X</span> Sterge toate filtrele</a>
  <div class="Hline"></div>
 <?php }
 if($link_tara) { ?>
  <div class="bkg-blue">
  <p>Luna plecarii</p>
  <select name="luni_plecari" id="luni_plecari" onchange="document.location.href='<?php echo $link_p; ?>?data-plecare='+this.value;">
    <option value="">Selectati</option>
    <?php if(sizeof($luni_plecari)>0) {
     foreach($luni_plecari as $value) {
      $lun=explode('-', $value);
      $link_z=$value; ?>
     <option value="<?php echo $link_z; ?>" <?php if($link_z==$din_luna) { ?> selected="selected" <?php } ?>><?php echo $luna[$lun[0]].' '.$lun[1]; ?></option>
   <?php }
    } ?>
  </select>
  </div>
  <div class="Hline"></div>
 <?php } $link_p1=$link_p;
 if(!$_REQUEST['data-plecare'])  $link_p1= $link_p1.'?optiuni=da'; else $link_p1= $link_p1.'?data-plecare='.$_REQUEST['data-plecare'];
if(sizeof($filtruOf['trans'])>0) { ?>
   <p>Transport</p>
   <?php krsort($filtruOf['trans']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['trans'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['trans'])==0) echo 'disabled="disabled"'; ?> id="optiune0" name="transport[]" <?php if($transport=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, this.value, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiune0">Toate</label><br class="clear" /></li>
    <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) { $i++;
   $lingTr=fa_link($key);
  if($value) $nr=$value; else $nr=0; ?>
    <li <?php if(!$filtruActual['trans'][$key]) echo 'class="disabled"'; ?>><input type="checkbox"  <?php if(!$filtruActual['trans'][$key]) echo 'disabled="disabled"'; ?> name="transport[]" id="optiune<?php echo $i; ?>" <?php if($lingTr==$transport) { ?> checked="checked" <?php } ?> value="<?php echo $lingTr; ?>" onclick="if(this.checked) var tr=this.value; else var tr='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, tr, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiune<?php echo $i; ?>"><?php echo $key; ?></label><br class="clear" />
    <?php if($key=='Avion') { ?>
    <select name="oras_plecare" onchange="filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, this.value);" class="oras-plecare">
    <option value="toate" <?php if(sizeof($filtruActual['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
    <?php if(sizeof($filtruOf['plecare_avion'])) {
   krsort($filtruOf['plecare_avion']);
   foreach($filtruOf['plecare_avion'] as $key=>$value) {
   $lingTr=fa_link($key); ?>
   <option value="<?php echo $lingTr; ?>" <?php if(!$filtruActual['plecare_avion'][$key]) echo 'disabled="disabled"'; if($lingTr==$plecare_avion || ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
   <?php }
    } ?>
    </select>  
  <?php } ?></li>
    <?php } ?>
  </ul>
  <div class="Hline"></div>
  <?php }
  if(sizeof($filtruOf['stele'])>0) { ?>
  <p>Categorie Hotel</p>
   <?php krsort($filtruOf['stele']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['stele'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['stele'])==0) echo 'disabled="disabled"'; ?> id="optiunes0" name="stele[]" <?php if($stele=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, this.value, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiunes0">Toate</label><br class="clear" /></li>
    <?php foreach($filtruOf['stele'] as $i=>$nr_s) {
    if($nr_s) $nrS=$nr_s; else $nrS=0;   ?>
    <li <?php if(!$filtruActual['stele'][$i]) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(!$filtruActual['stele'][$i]) echo 'disabled="disabled"'; ?> id="optiunes<?php echo $i; ?>" name="stele[]" value="<?php echo $i; ?>" <?php if($i==$stele) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var st=this.value; else var st='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, st, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiunes<?php echo $i; ?>"><?php echo $i; if($i>1) echo ' stele '; else echo ' stea '; ?></label><br class="clear" /></li>
    <?php } ?>
  </ul>
  <div class="Hline"></div>
  <?php }
  if(sizeof($filtruOf['masa'])>0) { ksort($filtruOf['masa']); ?>
  <p>Tip masa</p>
  <ul>
    <li <?php if(sizeof($filtruActual['masa'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['masa'])==0) echo 'disabled="disabled"'; ?> id="optiuneM0" name="masa[]" value="toate" <?php if($masa=='toate') { ?> checked="checked" <?php } ?> onclick="filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, this.value, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);"/> <label for="optiuneM0">Toate</label><br class="clear" /></li>
    <?php $i=0; foreach($filtruOf['masa'] as $keyM=>$valueM) {
    foreach($valueM as $denM=>$nr_m) { $i++;
    if($nr_m) $nrM=$nr_m; else $nrM=0;
    $lingM=fa_link($denM); ?>
    <li <?php if(!$filtruActual['masa'][$denM]) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(!$filtruActual['masa'][$denM]) echo 'disabled="disabled"'; ?> name="masa[]" id="optiuneM<?php echo $i; ?>" value="<?php echo $lingM; ?>" <?php if($lingM==$Lmasa) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var ms=this.value; else var ms='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, ms, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiuneM<?php echo $i; ?>"><?php echo $denM; ?></label><br class="clear" /></li>
   <?php }
    } ?>
  </ul>
  <div class="Hline"></div>
  <?php }
  if(in_array($zona, $zone_filtru_distanta) && sizeof($filtruOf['distanta'])) { 
  foreach($filtruOf['distanta'] as $fata_de=>$value) {
  ksort($value); ?>
  <p>Distanta fata de <?php echo $fata_de; ?></p>
  <ul>
   <li <?php if(sizeof($filtruActual['distanta'][$fata_de])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['distanta'][$fata_de])==0) echo 'disabled="disabled"'; ?> id="optiuneD0" name="distanta[]" value="toate" <?php if($distanta=='toate') { ?> checked="checked" <?php } ?> onclick="filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, this.value, <?php echo "'".$plecare_avion."'"; ?>);"/> <label for="optiuneD0">Toate</label><br class="clear" /></li>
  <?php $i=0; foreach($value as $key1=>$value2) { $i++;
  if($value2) $nrD=$value2; else $nrD=0;
  $val_dist=$fata_de.'-'.$inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; ?>
  <li <?php if(!$filtruActual['distanta'][$fata_de][$key1]) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(!$filtruActual['distanta'][$fata_de][$key1]) echo 'disabled="disabled"'; ?> name="distanta[]" id="optiuneD<?php echo $i; ?>" value="<?php echo $val_dist; ?>" <?php if($distanta==$val_dist) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var ds=this.value; else var ds='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, ds, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiuneD<?php echo $i; ?>"><?php if($key1==1) echo '<'.$inteval_distanta[1][2];
  elseif($key1<4) echo $inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; elseif($key1==4) echo '>'.$inteval_distanta[4][1]; echo ' m'; ?></label><br class="clear" /></li>
  <?php }
   } ?>
  </ul>
  <div class="Hline"></div>
  <?php } ?>
</div>

<p>&nbsp;</p>

<?php /*?><div class="fb-like-box" data-href="<?php echo $facebook_page; ?>" data-width="182" data-height="440" data-show-faces="true" data-stream="false" data-header="true"></div>

<p>&nbsp;</p><?php */?>
