<?php $_SESSION['page_back'] = $_SERVER['REQUEST_URI']; ?>

<?php if(($tara!='') and ($zona=='') and ($oras=='')) { ?>
  <ul class="sf-menu sf-vertical">
    <li class="primul black">Destinatii pentru <strong><?php echo $cuvant_cheie; ?></strong></li>
	<?php $zone=get_zone($iduri,'','',$id_tara,$early);
	if(sizeof($zone)>0) {
		foreach($zone as $id_zona1=>$value) {
			$link_z='/'.$indice.fa_link($tara).'/'.fa_link($value).'/'; ?>
    <li><a href="<?php echo $link_z; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $value; ?></a>
      <?php $loc_f=get_localitate($iduri,'',$id_zona1,$id_tara,$early);
	  if(sizeof($loc_f)>0) { ?>
      <ul>
	    <?php foreach($loc_f as $kloc_tf=>$loc_tf) {
		$link_lo=fa_link($loc_tf['denumire']); ?>
        <li><a href="<?php echo $link_z.$link_lo.'/'; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $loc_tf['denumire']; ?></a></li>
        <?php } ?>
      </ul>
      <?php } ?>
    </li>
    <?php }
	} ?>
  </ul>
<?php } ?>

<?php if(($tara!='') and ($zona!='')) { ?>
  <ul class="sf-menu sf-vertical">
    <li class="primul black">Destinatii pentru <strong><?php echo ucwords($tip).' '.$den_zona; ?></strong></li>
	<?php $link_z='/'.$indice.fa_link($tara).'/'.fa_link($zona).'/';
	$loc_f=get_localitate($iduri,'',$id_zona,$id_tara,$early);
    if(sizeof($loc_f)>0) {
		foreach($loc_f as $kloc_tf=>$loc_tf) {
			$link_lo=fa_link($loc_tf['denumire']); ?>
    <li class="second"><?php if($oras==strtolower($loc_tf['denumire'])) { echo '<strong class="current">'.$loc_tf['denumire'].'</strong>'; } else { ?><a href="<?php echo $link_z.$link_lo.'/'; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $loc_tf['denumire']; ?> <?php /*echo '('.$loc_tf['count_hotel'].')';*/ ?></a><?php } ?></li>
    <?php }
	} ?>
  </ul>
<?php } ?>

<div class="filters NEW-round6px">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/popup_filters.php"); ?>

  <div class="titlu blue">Filtrare dupa:</div>

<?php if($_REQUEST['data-plecare'] || $id_transport || $nr_stele || $nmasa || $nconcept || $nfacilitati || $din_luna || ($distanta && $distanta<>'toate') || $_GET['early-booking']=='da' || $checkin) { ?>
  <a href="<?php echo $link_p; ?>" class="delete" rel="nofollow" onclick="setCookie('CheckIn',''+this.value+'',-1);"><span>X</span> Sterge toate filtrele</a>
  <ul class="delete-filters black">
    <?php
    if($checkin) echo '<li>check-in <strong>'.date("d.m.Y", strtotime($checkin)).'</strong></li>';
    //if($_REQUEST['data-plecare']) echo '<li>plecare '.$_REQUEST['data-plecare'].'</li>';
	if($early) echo '<li>early booking</li>';
    if($din_luna) echo '<li>'.$plecare.'</li>';
    if($id_transport) echo '<li>'.$trans.'</li>';
    if($nr_stele) echo '<li>'.$nr_stele.' stele</li>';
    if($nmasa) echo '<li>'.$nmasa.'</li>';
    if($distanta && $distanta<>'toate') echo '<li>'.$distanta.' m</li>';
    if($nconcept) echo '<li>'.$nconcept.'</li>';
    if($nfacilitati) echo '<li>'.$nfacilitati.'</li>';
	?>
  </ul>
  <br>
<?php } ?>
  
<?php if(!isset($_GET['nume_hotel'])) { ?>
  <?php $tipuri=get_tiputi_oferte('', $id_localitate, $id_zona, $id_tara, '', ''); $i=0;
  if(sizeof($tipuri)>0) {
	  $link_new=$link_tara.'/';
	  if($link_zona) $link_new.=$link_zona.'/';
	  if($link_oras) $link_new.=$link_oras.'/';
	  
	  if($link_tara) $den_new=$den_tara;
	  if($link_zona) $den_new=$den_zona;
	  if($link_oras) $den_new=$den_oras;
  ?>
  <div class="item NEW-round6px">
  <div class="heading">Ocazii speciale</div>
  <ul>
    <li class="label"><a href="<?php echo '/sejur-'.$link_new; ?>" <?php if(!$_REQUEST['tip']) echo 'class="sel"'; ?> rel="nofollow">Toate</a></li>
    <?php $i=0; foreach($tipuri as $key_t=>$value_t) { $i++;
		$valTip=fa_link($value_t['denumire']);
		if($value_t) $nrO=$value_t; else $nrO=0;
	?>
    <li class="label"><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" <?php if($valTip==$_REQUEST['tip']) echo 'class="sel"'; ?> title="<?php echo $value_t['denumire']." ".$den_new; ?>"><?php echo $value_t['denumire']." ".$den_new; ?> <?php /*echo '('.$nrO.')';*/ ?></a></li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>
  
<?php $link_p1=$link_p.'?optiuni=da'; ?>

<form method="get" action="" id="filter">

<?php if(($link_tara) and ($nr_hoteluri>0)) { ?>
  <div class="item NEW-round6px">
    <div class="heading">Data Check-in</div>
    <label><input type="text" name="checkin" value="<?php if($checkin) echo date("d.m.Y",strtotime($checkin)); ?>" id="CheckIn" onchange="<?php /*?>document.location.href='<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $Rconcept, $Rfacilitati, TRUE); ?>'+this.value; setCookie('CheckIn',''+this.value+'',10);<?php */?>" class="item-filter calendar bigger-12em NEW-round4px"> <img src="/images/icon_calendar_green.png" alt="Check-in" style="vertical-align:middle;"></label>
  </div>
<?php } ?>

<?php /*if(($link_tara) and ($nr_hoteluri>0)) { ?>
  <div class="item NEW-round6px">
    <div class="heading">Inceputul sejurului</div>
    <select name="luni_plecari" id="luni_plecari" onchange="document.location.href='<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $Rconcept, $Rfacilitati, TRUE); ?>'+this.value;">
      <option value="">Alege inceperea sejurului</option>
      <?php if(sizeof($luni_plecari)>0) {
	   foreach($luni_plecari as $value) {
		$lun=explode('-', $value);
		$link_z=$value; ?>
      <option value="<?php echo $link_z; ?>" <?php if($link_z==$din_luna) { ?> selected="selected" <?php } ?>><?php echo $luna[$lun[0]].' '.$lun[1]; ?></option>
      <?php }
	  } ?>
    </select>
  </div>
<?php }*/ ?>

<?php /*?><?php $link_p1=$link_p;
  if(!$_REQUEST['data-plecare'])  $link_p1=$link_p1.'?optiuni=da'; else $link_p1=$link_p1.'?data-plecare='.$_REQUEST['data-plecare']; ?><?php */?>
  
  <?php if($filtruOf['early_booking']>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Reduceri</div>
  <ul>
    <li class="label"><input type="checkbox" name="early-booking" id="tipE1" value="" <?php if($_GET['early-booking']=='') echo 'checked'; ?> class="item-filter">
    <label for="tipE1" <?php if($_GET['early-booking']=='') echo 'class="sel"'; ?>>Toate</label></li>
    <li class="label"><input type="checkbox" name="early-booking" id="tipE1" value="da" <?php if($_GET['early-booking']=='da') echo 'checked'; ?> class="item-filter">
    <label for="tipE1" <?php if($_GET['early-booking']=='da') echo 'class="sel"'; ?>>Early Booking</label></li>
  </ul>
  </div>
  <?php } ?>

  <?php if($_GET['early-booking']) $link_p1=$link_p1.'&early-booking=da'; if(sizeof($filtruOf['trans'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Transport</div>
  <?php krsort($filtruOf['trans']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, 'toate', $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($transport=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) {
		$i++;
		$lingTr=fa_link($key);
		if($value) $nr=$value; else $nr=0;
		if($transport==$lingTr) {
			$tr=$lingTr;
			$selclass='class="sel"';
		} else {
			$tr=$lingTr;
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $tr, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $key; ?> <?php echo '('.$nr.')'; ?></a>
      <?php if($key=='Avion') { ?>
      <select name="oras_plecare" onchange="filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, this.value, <?php echo "'".$plecare_autocar."'"; ?>, <?php echo "'".$Rconcept."'"; ?>);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_avion'])) {
			krsort($filtruOf['plecare_avion']);
			foreach($filtruOf['plecare_avion'] as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo $lingTr; ?>" <?php if($lingTr==$plecare_avion || ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
      <?php if($key=='Autocar') { ?>
      <select name="oras_plecare" onchange="filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'autocar', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, this.value, <?php echo "'".$Rconcept."'"; ?>); setCookie('transportAutocar', this.value, 30);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_autocar'])==0) echo 'disabled="disabled"'; if($plecare_autocar=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_autocar'])) {
			//arsort($filtruOf['plecare_autocar']);
			foreach($filtruOf['plecare_autocar'] as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo $lingTr; ?>" <?php if($lingTr==$plecare_autocar || ($transport=='autocar' && sizeof($filtruOf['plecare_autocar'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['stele'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Categorie Hotel</div>
  <?php krsort($filtruOf['stele']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, 'toate', $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($stele=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php foreach($filtruOf['stele'] as $i=>$nr_s) {
		if($nr_s) $nrS=$nr_s; else $nrS=0;
		if(is_array($stele)) {
			if(in_array($i,$stele)) {
				$st=remove_current_filter($nr_stele,$i);
				$selclass='class="sel"';
			} else {
				$st=$nr_stele.','.$i;
				$selclass='';
			}
		} else {
			$st=$i;
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $st, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $i; if($i>1) echo ' stele '; else echo ' stea '; ?> <?php echo '('.$nrS.')'; ?></a></li>
    <li class="label"><input type="checkbox" id="optiunes<?php echo $i; ?>" name="stele[]" value="<?php echo $i; ?>" <?php if($i==$st) echo 'checked'; ?> class="item-filter">
    <label for="optiunes<?php echo $i; ?>" <?php echo $selclass; ?>><?php echo $i; if($i>1) echo ' stele '; else echo ' stea '; ?> <?php echo '('.$nrS.')'; ?></label></li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['masa'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Tip masa</div>
  <?php ksort($filtruOf['masa']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, 'toate', $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($masa=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['masa'] as $keyM=>$valueM) {
		foreach($valueM as $denM=>$nr_m) {
			$i++;
			if($nr_m) $nrM=$nr_m; else $nrM=0;
			if(is_array($Lmasa)) {
				if(in_array(fa_link($denM),$Lmasa)) {
					$lingM=remove_current_filter($Rmasa,fa_link($denM));
					$selclass='class="sel"';
				} else {
					$lingM=$Rmasa.','.fa_link($denM);
					$selclass='';
				}
			} else {
				$lingM=fa_link($denM);
				$selclass='';
			}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $lingM, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $denM; ?> <?php echo '('.$nrM.')'; ?></a></li>
    <?php }
	} ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(in_array($zona, $zone_filtru_distanta) && sizeof($filtruOf['distanta'])) {
	  foreach($filtruOf['distanta'] as $fata_de=>$value) {
		  ksort($value); ?>
  <div class="item NEW-round6px">
  <div class="heading">Distanta fata de <?php echo $fata_de; ?></div>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, 'toate', $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($distanta=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($value as $key1=>$value2) { $i++;
		if($value2) $nrD=$value2; else $nrD=0;
		$val_dist=$fata_de.'-'.$inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2];
		if($distanta==$val_dist) {
			$selclass='class="sel"';
		} else {
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $val_dist, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php if($key1==1) echo '<'.$inteval_distanta[1][2]; elseif($key1<4) echo $inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; elseif($key1==4) echo '>'.$inteval_distanta[4][1]; echo ' m'; ?> <?php echo '('.$nrD.')'; ?></a></li>
    <?php }
	 } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['facilitati'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Facilitati</div>
  <?php krsort($filtruOf['facilitati']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, 'toate', $checkin); ?>" rel="nofollow" <?php if($facilitati=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['facilitati'] as $denF=>$nr_f) { $i++;
		if($nr_f) $nrF=$nr_f; else $nrF=0;
		if(is_array($Lfacilitati)) {
			if(in_array(fa_link($denF),$Lfacilitati)) {
				$lingF=remove_current_filter($Rfacilitati,fa_link($denF));
				$selclass='class="sel"';
			} else {
				$lingF=$Rfacilitati.','.fa_link($denF);
				$selclass='';
			}
		} else {
			$lingF=fa_link($denF);
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $lingF, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $denF; ?> <?php echo '('.$nrF.')'; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>

  <?php if(sizeof($filtruOf['concept'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Recomandat pentru</div>
  <?php krsort($filtruOf['concept']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, 'toate', $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($concept=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['concept'] as $denC=>$nr_c) { $i++;
		if($nr_c) $nrC=$nr_c; else $nrC=0;
		if(is_array($Lconcept)) {
			if(in_array(fa_link($denC),$Lconcept)) {
				$lingC=remove_current_filter($Rconcept,fa_link($denC));
				$selclass='class="sel"';
			} else {
				$lingC=$Rconcept.','.fa_link($denC);
				$selclass='';
			}
		} else {
			$lingC=fa_link($denC);
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $lingC, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $denC; ?> <?php echo '('.$nrC.')'; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>

</form>

<?php }//end if(!isset($_GET['nume_hotel'])) ?>

</div>

<br class="clear"><br>

<?php if($den_tara=='Bulgaria') { ?>
<ul class="chn-linkuri">
  <li class="second"><a href="/transport-bulgaria.html">Transport Bulgaria</a></li>
</ul>
<br class="clear"><br>
<?php } ?>
<?php if($den_tara=='Grecia') { ?>
	<?php if($den_zona=='Insula Thassos') { ?>
    <ul class="chn-linkuri">
      <li class="second"><a href="/transport-thassos.html">Transport Insula Thassos</a></li>
    </ul>
    <br class="clear"><br>
    <?php } ?>
	<?php if($den_zona=='Paralia Katerini') { ?>
    <ul class="chn-linkuri">
      <li class="second"><a href="/transport-paralia-katerini.html">Transport Paralia Katerini</a></li>
    </ul>
    <br class="clear"><br>
    <?php } ?>
<?php } ?>

<?php /*?><div style="width:210px; height:430px; overflow:hidden; border:1px solid #CCC;" class="NEW-round6px"><div class="fb-like-box" data-href="<?php echo $facebook_page; ?>" data-width="210" data-height="457" data-show-faces="true" data-stream="false" data-show-border="false" data-header="true"></div></div>

<br class="clear"><br><?php */?>

<?php /*?><a href="<?php echo $sitepath; ?>newsletter.html" title="Abonare Newsletter" rel="infoboxes"><img src="<?php echo $imgpath; ?>/banner/abonare-newsletter-small.png" alt="Abonare Newsletter" /></a>

<p>&nbsp;</p><?php */?>

<?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/linkuri_seo.php"); ?>

<script>
$('.item-filter').change(
	function(){
		$('#filter').submit();
	}
);
</script>
