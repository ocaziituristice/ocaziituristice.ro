<div id="NEW-destinatie">
  <h1 class="blue float-left"><?php echo $titlu_pag; ?></h1>
  
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  <br class="clear" />

  <div style="padding:0 15px;">
	<h2 class="green">Early Booking - Rezerva din timp pentru preturi mici!</h2>
    <p>Daca te-ai hotarat deja unde vrei sa petreci vacanta, ai numai avantaje cu <strong>early booking 2011</strong>. Rezerva din timp si ai preturi mici pentru destinatii exotice sau europene. Poti descoperi fascinantul si misteriosul Egipt cu <strong>early booking Egipt</strong> sau atinge destinatia sporturilor de iarna cu <a href="http://www.ocaziituristice.ro/early_booking/austria/" title="Oferte early booking Austria">early booking Austria</a>. Iti recomandam si <a href="http://www.ocaziituristice.ro/early_booking/cipru/" title="Oferte early booking Cipru">early booking Cipru</a> pentru peisaje luxuriante si petreceri renumite. Odata ce ai ales din varietatea de <strong>oferte early booking</strong>, nu ramane decat sa te pregatesti de amintiri de neuitat si sa zambesti in fiecare zi cu gandul la locurile minunante pe care le vei vizita.</p>
   <br class="clear" />
  </div> 
  <div class="Hline"></div>
  
<div class="NEW-column-right2 clearfix">

  <?php /*?><div class="left clearfix"><?php */?>
  
    <?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/sejururi/tematici.php'); ?>
    
    <div class="rez-afisare red">Afisare oferte early booking <?php if($nr_stele) { ?> pentru hoteluri de <strong><?php echo $nr_stele; if($nr_stele>1) echo ' stele'; else echo ' stea'; ?></strong><?php } if($id_transport) { ?> cu transport <strong><?php echo ucwords($trans); ?></strong><?php } if($nmasa) { ?> si masa <strong><?php echo ucwords($nmasa); ?></strong> inclusa<?php } if($plecare_avion<>'toate') echo ', plecare din '.ucwords($plecare_avion); ?></div>
  
<?php $link_of_pag=$link; if($_REQUEST['ordonare']) $link=$link."&ordonare=".$_REQUEST['ordonare'];
if($link=='?optiuni=da') $link='';
$oferte=new AFISARE_SEJUR_NORMAL();
$oferte->setAfisare($from, $nr_pe_pagina);
$oferte->config_paginare('nou');
$oferte->setEarly('da');
if($_REQUEST['ordonare']) {
$tipO=explode('-',$_REQUEST['ordonare']);
if($tipO[0]=='tip_pret') $oferte->setOrdonarePret($tipO[1]);
elseif($tipO[0]=='tip_numH') $oferte->setOrdonareNumeH($tipO[1]); }
if($din_luna) $oferte->setLunaPlecare($din_luna);
if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $oferte->setTransport($_REQUEST['transport']);
if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') $oferte->setStele($_REQUEST['stele']);
if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') $oferte->setMasa($_REQUEST['masa']);
if($id_loc_plecare_av) $oferte->setPlecareAvion($id_loc_plecare_av);
if(sizeof($ds)==3) $oferte->setDistanta($ds);
$oferte->initializeaza_pagini($link_p, 'pag-###/', $link);
$nr_hoteluri=$oferte->numar_oferte(); ?>
<div class="filter-pag">
    <div class="filter">
      <strong>Ordoneaza dupa: &nbsp; </strong>
      <select name="ordonare" onchange="if(this.value) var ord='&ordonare='+this.value; else var ord=''; document.location.href='<?php echo $link_of_pag; ?>'+ord;">
        <option value="data-introducere" <?php if(!$_REQUEST['ordonare']) { ?> selected="selected"<?php } ?>>Top vanzari</option>
        <option value="tip_pret-asc" <?php if($tipO[0]=='tip_pret' && $tipO[1]=='asc') { ?> selected="selected" <?php } ?>>Pret crescator (tarif pe noapte)</option>
        <option value="tip_pret-desc" <?php if($tipO[0]=='tip_pret' && $tipO[1]=='desc') { ?> selected="selected" <?php } ?>>Pret descrescator (tarif pe noapte)</option>
        <option value="tip_numH-asc" <?php if($tipO[0]=='tip_numH' && $tipO[1]=='asc') { ?> selected="selected" <?php } ?>>Nume hotel crescator</option>
        <option value="tip_numH-desc" <?php if($tipO[0]=='tip_numH' && $tipO[1]=='desc') { ?> selected="selected" <?php } ?>>Nume hotel descrescator</option>
      </select>
      <br />Afisare <strong><?php echo $st=$nr_pe_pagina*($from-1)+1; $sf=$nr_pe_pagina*$from; if($nr_hoteluri<$sf) $sf=$nr_hoteluri; echo '-'.$sf; ?></strong> hoteluri din <strong><?php echo $nr_hoteluri; ?> in total</strong>
    </div>
    <div class="paginatie" align="right"><?php $oferte->printPagini(); ?></div>
  <br class="clear" />
 </div>
 <br class="clear" /><br />
<?php $oferte->afiseaza();
if($nr_hoteluri>0) {
$filtruOf=$oferte->get_filtru_mare();		
$filtruActual=$oferte->get_filtru(); } ?>
	
    <div class="filter-pag">
      <div class="filter">
        Afisare <strong><?php echo $st=$nr_pe_pagina*($from-1)+1; $sf=$nr_pe_pagina*$from; if($nr_hoteluri<$sf) $sf=$nr_hoteluri; echo '-'.$sf; ?></strong> hoteluri din <strong><?php echo $nr_hoteluri; ?> in total</strong><br />
        <span class="blue"><strong>* arata</strong></span>
        <select style="width:50px;" onchange="document.cookie='nr_pe_pagina='+this.value; document.location.href='<?php echo $link_of_pag; ?>';">
          <option <?php if($nr_pe_pagina==10) { ?> selected="selected" <?php } ?> value="10">10</option>
          <option <?php if($nr_pe_pagina==20) { ?> selected="selected" <?php } ?> value="20">20</option>
          <option <?php if($nr_pe_pagina==50) { ?> selected="selected" <?php } ?> value="50">50</option>
        </select>
        <span class="blue"><strong>hoteluri pe pagina</strong></span>
      </div>
      <div class="paginatie" align="right"><?php $oferte->printPagini(); ?></div>
    </div>
    
    <br class="clear" /><br />

</div>
<div class="NEW-column-left2">
  <?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/sejururi/filtru_stanga_early.php'); ?>
</div>
<br class="clear" />
</div>
<br class="clear" />
