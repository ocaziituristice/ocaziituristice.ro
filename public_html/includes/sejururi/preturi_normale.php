<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
$id_oferta=$_GET['oferta'];
$GLOBALS['make_vizualizata']='nu';
$det= new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$preturi=$det->select_preturi_sejur($id_oferta, '', ''); 
$st=explode('-', $preturi['min_start']);
$en=explode('-', $preturi['max_end']); ?>
<?php if($preturi['camera']>0) { ?>

<div id="NEW-detaliiOferta">
  <h2 class="blue">Preturi fara reducerea <span class="red">Early Booking</span> inclusa</h2>
  <table class="tabel-preturi" style="width:100%;">
    <thead>
    <tr>
      <th class="text-center" style="width:150px;"><?php if($detalii['tip_preturi']=='plecari') echo 'Data plecarii'; else echo 'Perioade'; ?></th>
      <?php $g=0;
      foreach($preturi['camera'] as $key_cam=>$value_cam) {
          $g++;
          if(sizeof($preturi['camera'])==$g) {
              $class='text-center last';
              $g=0;
          } else $class='text-center'; ?>
          <th class="<?php echo $class; ?>"><?php echo $value_cam; ?></th>
      <?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php $nr_t=0;
    foreach($preturi['data_start'] as $key_d=>$data_start) {
        $nr_t++;
        if($nr_t%2==1) $class_tr='impar'; else $class_tr='par';
        $numar_minim_nopti='';
        if(sizeof($detalii['data_start_nr_min'])>'0') {
            foreach($detalii['data_start_nr_min'] as $key_nm=>$value_nm) {
                if($value_nm<=$preturi['data_start_normal'][$key_d]) {
                    if($preturi['data_end_normal'][$key_d] && $preturi['data_end_normal'][$key_d]<>'0000-00-00') {
                        if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_end_normal'][$key_d]) {
                            $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
                        }
                    } else {
                        if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_start_normal'][$key_d]) {
                            $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
                        }
                    }
                }
            }
        } ?>
        <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
          <th class="text-left nowrap">
            <?php if(sizeof($preturi['include_tip'][$key_d])>0) foreach($preturi['include_tip'][$key_d] as $key_inc=>$include) echo '<a href="'.$sitepath.$_REQUEST['tara'].'/'.$_REQUEST['localitate'].'/'.fa_link($include['denumire']).'-'.$include['id_oferta'].'.html">!</a>';  echo $data_start; if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') echo ' - '.$preturi['data_end'][$key_d]; ?><br />
            <?php if(strlen($numar_minim_nopti)>4) echo '<em>'.$numar_minim_nopti.'</em><br />'; ?>
            <?php /*if(sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])>0) { ?><div class="red"><a onmousedown="if(document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display == 'none'){ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'block'; }else{ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'none'; }" style="cursor:pointer">toate preturile</a></div><?php }*/ ?>
          </th>
          <?php $g=0;
          foreach($preturi['camera'] as $key_cam=>$value_cam) {
              $g++;
              if(sizeof($preturi['camera'])==$g) {
                  $class='text-center pret last';
                  $g=0;
              } else $class='text-center pret';
              if(sizeof($preturi['camera'])=="1") $wdt='80%';
              elseif(sizeof($preturi['camera'])=="2") $wdt='40%';
              elseif(sizeof($preturi['camera'])=="3") $wdt='27%';
              elseif(sizeof($preturi['camera'])=="4") $wdt='20%';
              elseif(sizeof($preturi['camera'])=="5") $wdt='16%';
              elseif(sizeof($preturi['camera'])=="6") $wdt='13%';
          ?>
          <td <?php echo 'class="'.$class.'"'; ?> style="width:<?php echo $wdt; ?>">
          <?php if($preturi['pret'][$key_d][$key_cam]) {
              echo new_price($preturi['pret'][$key_d][$key_cam]).' ';
              if($preturi['moneda'][$key_d][$key_cam]=='EURO') {
                  echo '&euro;';
                  $valuta='da';
              } elseif($preturi['moneda'][$key_d][$key_cam]=='USD') {
                  echo '$';
                  $valuta='da';
              } else echo $preturi['moneda'][$key_d][$key_cam];
          } else echo '-'; ?>
          </td>
      <?php } ?>
    </tr>
    <?php /*if(sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])>0) { ?>
    <tr>
      <td colspan="5" class="text-left last" style="padding:0;"><div id="alte_preturi<?php echo $key_d; ?>" style="display:none; padding-left:164px; background:#d2f6c5;">
        <table class="tabel-preturi" style="width:100%; margin:0;">
          <tbody>
          <tr>
          <?php $sup_i=0;
          foreach($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]] as $key_sup=>$value_sup) {
              $sup_i++;
              if($sup_i==sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])) $class_sup='text-center last'; else $class_sup='text-center'; ?>
              <th class="<?php echo $class_sup; ?>"><?php echo $key_sup; ?></th>
          <?php } ?>
          </tr>
          <tr>
          <?php $sup_i=0;
          foreach($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]] as $key_sup=>$value_sup) {
              $sup_i++;
              if($sup_i==sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])) $class_sup='last'; else $class_sup=''; ?>
              <td class="text-center pret <?php echo $class_sup; ?>">
              <?php if($value_sup['pret']) {
                  echo $value_sup['pret'].' ';
                  if($value_sup['moneda']=='EURO') echo '&euro;';
                  elseif($value_sup['moneda']=='USD') echo '$';
                  else echo $value_sup['moneda'];
              } ?>
              </td>
          <?php } ?>
          </tr>
          </tbody>
        </table>
      </div></td>
    </tr>
      <?php }*/
    } ?>
    </tbody>
  </table>
</div>

<?php } ?>