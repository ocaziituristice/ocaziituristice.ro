<br>

<?php  $_SESSION['page_back'] = $_SERVER['REQUEST_URI']; ?>

<?php if(!isset($check_link)) { ?>

<?php if(($tara!='') and ($zona=='') and ($oras=='') and ($den_tara!='Dubai')) { ?>
  <ul class="sf-menu sf-vertical">
    <li class="primul black"><strong><?php echo $cuvant_cheie; ?></strong></li>
	<?php $zone=get_zone($iduri,'','',$id_tara,$early);
	if(sizeof($zone)>0) {
		foreach($zone as $id_zona1=>$value) {
			$link_z='/'.$indice.fa_link($tara).'/'.fa_link($value).'/'; ?>
    <li><a href="<?php echo $link_z; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $value; ?></a>
      <?php $loc_f=get_localitate($iduri,'',$id_zona1,$id_tara,$early);
	  if(sizeof($loc_f)>0) { ?>
      <ul>
	    <?php foreach($loc_f as $kloc_tf=>$loc_tf) {
		$link_lo=fa_link($loc_tf['denumire']); ?>
        <li><a href="<?php echo $link_z.$link_lo.'/'; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $loc_tf['denumire']; ?></a></li>
        <?php } ?>
      </ul>
      <?php } ?>
    </li>
    <?php }
	} ?>
  </ul>
<?php } ?>

<?php if(($tara!='') and ($zona!='') and ($den_tara!='Dubai')) { ?>
  <ul class="sf-menu sf-vertical">
    <li class="primul black"><strong><?php echo ucwords($tip).' '.$den_zona; ?></strong></li>
	<?php $link_z='/'.$indice.fa_link($tara).'/'.fa_link($zona).'/';
	$loc_f=get_localitate($iduri,'',$id_zona,$id_tara,$early);
    if(sizeof($loc_f)>0) {
		foreach($loc_f as $kloc_tf=>$loc_tf) {
			$link_lo='/cazare-'.fa_link($loc_tf['denumire']); ?>
    <li class="second"><?php if(strtolower($oras)==strtolower($loc_tf['denumire'])) { echo '<strong class="current">'.ucwords($loc_tf['specificatie_zona'])." ".$loc_tf['denumire'].'</strong>'; } else { ?><a href="<?php echo $link_lo.'/'; if($early) echo '?optiuni=da&early-booking=da'; ?>" title="<?php echo ucwords($loc_tf['specificatie_zona'])." ".$loc_tf['denumire']?>"><?php echo ucwords($loc_tf['specificatie_zona'])." ".$loc_tf['denumire']; ?> <?php /*echo '('.$loc_tf['count_hotel'].')';*/ ?></a><?php } ?></li>
    <?php }
	} ?>
  </ul>
<?php } ?>

<?php } //if(!isset($check_link)) ?>

<div class="filters NEW-round6px">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/popup_filters.php"); ?>

  <div class="titlu blue">Filtreaza</div>

<?php if($_REQUEST['data-plecare'] || $id_transport || $nr_stele || $nmasa || $nconcept || $nfacilitati || $din_luna || ($distanta && $distanta<>'toate') || $_GET['early-booking']=='da' || $checkin) { ?>
  <a href="<?php echo $link_p; ?>" class="delete" rel="nofollow" onclick="setCookie('grad_ocupare','',-1);"><span>X</span> Sterge toate filtrele</a>
  <ul class="delete-filters black">
    <?php
    if($checkin) echo '<li>check-in <strong>'.date("d.m.Y", strtotime($checkin)).'</strong></li>';
    //if($_REQUEST['data-plecare']) echo '<li>plecare '.$_REQUEST['data-plecare'].'</li>';
	if($early) echo '<li>early booking</li>';
    if($din_luna) echo '<li>'.$plecare.'</li>';
    if($id_transport) echo '<li>'.$trans.'</li>';
    if($nr_stele) echo '<li>'.$nr_stele.' stele</li>';
    if($nmasa) echo '<li>'.$nmasa.'</li>';
    if($distanta && $distanta<>'toate') echo '<li>'.$distanta.' m</li>';
    if($nconcept) echo '<li>'.$nconcept.'</li>';
    if($nfacilitati) echo '<li>'.$nfacilitati.'</li>';
	?>
  </ul>
  <br>
<?php } ?>
  
<?php if(!isset($_GET['nume_hotel'])) { ?>

<?php $tipuri=get_tiputi_oferte('', $id_localitate, $id_zona, $id_tara, '', ''); $i=0;
  if(sizeof($tipuri)>0 and !isset($check_link)) {
	  $link_new=$link_tara.'/';
	  if($link_zona) $link_new.=$link_zona.'/';
	  if($link_oras) {$link_new.=$link_oras.'/';
	  $link_toate='/cazare-'.$link_oras;}
	  
	  if($link_tara) $den_new=$den_tara;
	  if($link_zona) $den_new=$den_zona;
	  if($link_oras) $den_new=$den_oras;
  ?>
  <div class="item NEW-round6px">
  <div class="heading">Ocazii speciale</div>
  <ul>
    <li class="label">
    <a href="<?php echo '/sejur-'.$link_new; ?>" <?php if(!$_REQUEST['tip']) echo 'class="sel"'; ?> >Toate</a></li>
    <?php $i=0; foreach($tipuri as $key_t=>$value_t) { $i++;
		$valTip=fa_link($value_t['denumire']);
		if($value_t) $nrO=$value_t; else $nrO=0;
	?>
    <li class="label"><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" <?php if($valTip==$_REQUEST['tip']) echo 'class="sel"'; ?> title="<?php echo $value_t['denumire']." ".$den_new; ?>" ><?php echo $value_t['denumire']." ".$den_new; ?> <?php /*echo '('.$nrO.')';*/ ?></a></li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>
  
<?php $link_p1=$link_p.'?optiuni=da'; ?>

<?php if(($link_tara) and ($nr_hoteluri>0)) { ?>
  <div class="item NEW-round6px">
    <div class="heading">Data Check-in</div>
    <label><input type="text" name="checkin" value="<?php if($checkin) echo date("d.m.Y",strtotime($checkin)); ?>" id="CheckIn" onchange="set_grad_ocupare(this.value, '', '', '', '', '', '', '', '', ''); document.location.href='<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $Rconcept, $Rfacilitati, TRUE); ?>'+this.value; <?php /*?>setCookie('CheckIn',''+this.value+'',10);<?php */?>" class="calendar bigger-12em NEW-round4px"> <img src="/images/icon_calendar_green.png" alt="Check-in" style="vertical-align:middle;"></label>
  </div>
<?php } ?>

<?php /*if(($link_tara) and ($nr_hoteluri>0)) { ?>
  <div class="item NEW-round6px">
    <div class="heading">Inceputul sejurului</div>
    <select name="luni_plecari" id="luni_plecari" onchange="document.location.href='<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $Rconcept, $Rfacilitati, TRUE); ?>'+this.value;">
      <option value="">Alege inceperea sejurului</option>
      <?php if(sizeof($luni_plecari)>0) {
	   foreach($luni_plecari as $value) {
		$lun=explode('-', $value);
		$link_z=$value; ?>
      <option value="<?php echo $link_z; ?>" <?php if($link_z==$din_luna) { ?> selected="selected" <?php } ?>><?php echo $luna[$lun[0]].' '.$lun[1]; ?></option>
      <?php }
	  } ?>
    </select>
  </div>
<?php }*/ ?>

<?php /*?><?php $link_p1=$link_p;
  if(!$_REQUEST['data-plecare'])  $link_p1=$link_p1.'?optiuni=da'; else $link_p1=$link_p1.'?data-plecare='.$_REQUEST['data-plecare']; ?><?php */?>
  
  <?php /*?><?php if($filtruOf['early_booking']>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Reduceri</div>
  <ul>
    <li class="label"><a href="<?php echo '/sejur-'.$link_tara.'/'; if($link_zona) echo $link_zona.'/'; if($link_oras) echo $link_oras.'/'; ?>" <?php if(!$_GET['early-booking']) echo 'class="sel"'; ?> rel="nofollow">Toate</a></li>
    <li class="label"><a href="<?php echo $link_p1.'&amp;early-booking=da'; ?>" <?php if($_GET['early-booking']=='da') echo 'class="sel"'; ?>>Early Booking</a></li>
  </ul>
  </div>
  <?php } ?><?php */?>

  <?php if($_GET['early-booking']) $link_p1=$link_p1.'&early-booking=da'; 
  if(sizeof($filtruOf['trans'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Transport</div>
        <?php 
$link_parametrii="link_p1=".base64_encode($link_p1)."&nr_stele=".$nr_stele."&Rmasa=".$Rmasa."&distanta=".$distanta."&plecare_avion=". $plecare_avion."&plecare_autocar=".$plecare_autocar."&Rconcept=".$Rconcept."&Rfacilitati=".$Rfacilitati."&checkin=".$checkin."&transport=".$transport; ?>
    <div id="filtru_transport"></div>
   <script>
    $("#filtru_transport").empty().html('<img src="/images/loader-cerc.gif" alt="Loading Filter" />');
	$("#filtru_transport").load("/includes/filtre/filtru_transport.php?.<?php echo $link_parametrii;?>",<?php echo json_encode($filtruOf);?>);
	</script>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['stele'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Categorie Hotel</div>
  <div id="filtru_categorie_hotel"></div>
     <script>
    $("#filtru_categorie_hotel").empty().html('<img src="/images/loader-cerc.gif" alt="Loading Filter" />');
	$("#filtru_categorie_hotel").load("/includes/filtre/filtru_categorie_hotel.php?.<?php echo $link_parametrii;?>",<?php echo json_encode($filtruOf);?>);
	</script>
  
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['masa'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Tip masa</div>
  
   <div id="filtru_tip_masa"></div>
     <script>
    $("#filtru_tip_masa").empty().html('<img src="/images/loader-cerc.gif" alt="Loading Filter" />');
	$("#filtru_tip_masa").load("/includes/filtre/filtru_tip_masa.php?.<?php echo $link_parametrii;?>",<?php echo json_encode($filtruOf);?>);
	</script>
  
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['facilitati'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Facilitati</div>
  <div id="filtru_facilitati"></div>
     <script>
    $("#filtru_facilitati").empty().html('<img src="/images/loader-cerc.gif" alt="Loading Filter" />');
	$("#filtru_facilitati").load("/includes/filtre/filtru_facilitati.php?.<?php echo $link_parametrii;?>",<?php echo json_encode($filtruOf);?>);
	</script>
  </div>
  <?php } ?>

  <?php if(in_array($zona, $zone_filtru_distanta) && sizeof($filtruOf['distanta'])) {
	?>
  <div class="item NEW-round6px">
  <div class="heading">Distanta fata de plaja<?php //echo $fata_de; ?></div>
 <div id="filtru_distanta"></div>
     <script>
    $("#filtru_distanta").empty().html('<img src="/images/loader-cerc.gif" alt="Loading Filter" />');
	$("#filtru_distanta").load("/includes/filtre/filtru_distanta.php?.<?php echo $link_parametrii;?>",<?php echo json_encode($filtruOf);?>);
	</script>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['concept'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Recomandat pentru</div>

  <div id="filtru_concept"></div>
     <script>
    $("#filtru_concept").empty().html('<img src="/images/loader-cerc.gif" alt="Loading Filter" />');
	$("#filtru_concept").load("/includes/filtre/filtru_concept.php?.<?php echo $link_parametrii;?>",<?php echo json_encode($filtruOf);?>);
	</script> 
  </div>
  <?php } ?>

<?php }//end if(!isset($_GET['nume_hotel'])) ?>

</div>

<br class="clear"><br>

<?php
$seo_links = array();
$j=0;
if($den_tara=='Dubai') {
	$den_zona = 'Dubai';
	$id_zona = get_id_zona($den_zona, $id_tara);
}
if($id_zona) {
	$den_destinatie = get_den_zona($id_zona);
	$check_weather = check_weather($id_tara, $id_zona);
	if($check_weather==1) {
		$seo_links[$j]['denumire'] = 'Vremea '.$den_destinatie;
		$seo_links[$j]['title'] = 'Vremea in '.$den_destinatie;
		$seo_links[$j]['link'] = '/vremea-in-'.fa_link($den_destinatie).'_'.$id_tara.'.html';
		$j++;
	}
	$check_articles = check_articles($id_tara, $id_zona);
	if($check_articles==1) {
		$seo_links[$j]['denumire'] = 'Obiective turistice '.$den_destinatie;
		$seo_links[$j]['title'] = 'Obiective turistice si informatii utile '.$den_destinatie;
		$seo_links[$j]['link'] = '/ghid-turistic-'.fa_link($den_destinatie).'/';
		$j++;
	}
	$articole_principale = articles_side_menu($id_tara, $id_zona);
	if(count($articole_principale)>0) {
		foreach($articole_principale as $k_articles => $v_articles) {
			$seo_links[$j]['denumire'] = $v_articles['denumire'];
			$seo_links[$j]['title'] = $v_articles['denumire'];
			$seo_links[$j]['link'] = '/ghid-turistic-'.$v_articles['link_area'].'/'.$v_articles['link'].'.html';
			$j++;
		}
	}
}
//echo $id_localitate;
if($id_localitate) {
	
	$den_localitate = get_den_localitate($id_localitate);
	$den_destinatie=$den_localitate;
	//$check_weather = check_weather($id_tara, $id_zona);
	//if($check_weather==1) {
		//$seo_links[$j]['denumire'] = 'Vremea '.$den_destinatie;
		//$seo_links[$j]['title'] = 'Vremea in '.$den_destinatie;
		//$seo_links[$j]['link'] = '/vremea-in-'.fa_link($den_destinatie).'_'.$id_tara.'.html';
		//$j++;
	//}
	$check_articles = check_articles($id_tara, $id_zona,$id_localitate);
	if($check_articles==1) {
		$seo_links[$j]['denumire'] = 'Obiective turistice '.$den_zona;
		$seo_links[$j]['title'] = 'Obiective turistice si informatii utile '.$den_zona;
		$seo_links[$j]['link'] = '/ghid-turistic-'.fa_link($den_zona).'/';
		$j++;
	}
	$articole_principale = articles_side_menu($id_tara, $id_zona,$id_localitate);
	if(count($articole_principale)>0) {
		foreach($articole_principale as $k_articles => $v_articles) {
			$seo_links[$j]['denumire'] = $v_articles['denumire'];
			$seo_links[$j]['title'] = $v_articles['denumire'];
			$seo_links[$j]['link'] = '/ghid-turistic-'.$v_articles['link_area'].'/'.$v_articles['link'].'.html';
			$j++;
		}
	}
}



if($den_tara=='Bulgaria') {
	$seo_links[$j]['denumire'] = 'Transport Bulgaria';
	$seo_links[$j]['title'] = 'Transport Bulgaria';
	$seo_links[$j]['link'] = '/transport-bulgaria.html';
	$j++;
}
if($den_tara=='Grecia') {
	if($den_zona=='Insula Thassos') {
		$seo_links[$j]['denumire'] = 'Transport Insula Thassos';
		$seo_links[$j]['title'] = 'Transport Insula Thassos';
		$seo_links[$j]['link'] = '/transport-thassos.html';
		$j++;
	}
	if($den_zona=='Paralia Katerini') {
		$seo_links[$j]['denumire'] = 'Transport Paralia Katerini';
		$seo_links[$j]['title'] = 'Transport Paralia Katerini';
		$seo_links[$j]['link'] = '/transport-paralia-katerini.html';
		$j++;
	}
}

if(count($seo_links)>0) {
?>
<div class="filters NEW-round6px chn-blue pad0">
  <div class="item NEW-round6px mar0">
    <div class="heading-blue mar0 white NEW-round4-0px"><i class="icon-info-circled white"></i> Info <?php echo $den_destinatie; ?></div>
    <?php if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/seo_links/".$id_zona.".jpg")) echo '<div class="picture"><img src="/images/seo_links/'.$id_zona.'.jpg" alt="'.$den_destinatie.'" class="w-full"></div>'; ?>
    <div class="articles">
      <?php /*?><?php if($den_destinatie!=$den_tara) { ?><a href="<?php echo $link_tara; ?>" title="Sejur <?php echo $den_tara; ?>">Sejur <?php echo $den_tara; ?></a><?php } ?>
      <a href="<?php echo $link_destinatie; ?>" title="Sejur <?php echo $den_destinatie; ?>">Sejur <?php echo $den_destinatie; ?></a><?php */?>
      <?php foreach($seo_links as $key_sl => $value_sl) {
		  echo '<a href="'.$seo_links[$key_sl]['link'].'" title="'.$seo_links[$key_sl]['title'].'">'.$seo_links[$key_sl]['denumire'].'</a>';
	  } ?>
    </div>
  </div>
</div>
<br class="clear"><br>
<?php } ?>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>

<?php /*?><div style="width:210px; height:430px; overflow:hidden; border:1px solid #CCC;" class="NEW-round6px"><div class="fb-like-box" data-href="<?php echo $facebook_page; ?>" data-width="210" data-height="457" data-show-faces="true" data-stream="false" data-show-border="false" data-header="true"></div></div>

<br class="clear"><br><?php */?>

<?php /*?><a href="<?php echo $sitepath; ?>newsletter.html" title="Abonare Newsletter" rel="infoboxes"><img src="<?php echo $imgpath; ?>/banner/abonare-newsletter-small.png" alt="Abonare Newsletter" /></a>

<p>&nbsp;</p><?php */?>

<?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/linkuri_seo.php"); ?>
