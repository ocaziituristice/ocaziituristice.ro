<?php $_SESSION['page_back_offer'] = $_SERVER['REQUEST_URI']; ?>

<div id="NEW-detaliiOferta" <?php if($detalii['valabila']=='nu') { ?>style="background:#FFF url(/images/oferta_expirata.gif) center 90px;"<?php } ?>>
<?php
$link_rezervare = $sitepath.'rezervare_sejur.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$link_rezervare2 = $sitepath.'rezervare_sejur_new.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$link_cerere_detalii = $sitepath.'cerere_detalii_new.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$valuta = 'nu';
$asigurare = 'nu';
	
$sel_table_SEO="SELECT cuvant_cheie FROM seo WHERE id_tip = '0' AND id_oras = '".$detalii_hotel['locatie_id']."' ";
$que_table_SEO=mysql_query($sel_table_SEO) or die(mysql_error());
$row_table_SEO=mysql_fetch_array($que_table_SEO);
@mysql_free_result($que_table_SEO);
if($row_table_SEO['cuvant_cheie']) $cuvant_cheie_loc=$row_table_SEO['cuvant_cheie']; else $cuvant_cheie_loc='Cazare '.$detalii_hotel['localitate'];
$link_cuvant_cheie_loc=$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/'.fa_link($detalii_hotel['localitate']).'/';

$autocar = array();
if($detalii['transport']=='Autocar') $autocar=$det->get_autocar_sejur($id_oferta);
if(count($autocar)>0) {
	/*$autocar = multid_sort($autocar, 'denumire_localitate');
	$autocar = array_map('unserialize', array_unique(array_map('serialize', $autocar)));*/
	$sort = array();
	foreach($autocar as $key_sort => $value_sort) {
		$sort['denumire_localitate'][$key_sort] = $value_sort['denumire_localitate'];
		$sort['pret'][$key_sort] = $value_sort['pret'];
	}
	array_multisort($sort['denumire_localitate'], SORT_ASC, $autocar);
}
?>
 
  <h1 class="float-left" style="width:680px;">
    <span class="red"><?php echo $detalii_hotel['denumire']; ?></span>
    <img src="/images/spacer.gif" class="stele-mari-<?php echo $detalii_hotel['stele']; ?>" alt="numar de stele" />
    <span class="smaller-07em"><?php echo $detalii['denumire_scurta']; ?></span>
  </h1>
  
  <div class="float-right clearfix">
    <?php if($nota_total>0) { ?>
    <div class="text-right NEW-round6px chenar-nota-comentarii">
      <span class="bold blue" style="font-size:20px;"><?php echo $nota_total; ?></span>
      <span class="black smaller-09em">(scor calculat din <strong><?php echo $nr_comentarii; ?></strong> comentarii)</span><br>
      <a href="#comentarii" class="link-red bold" onclick="jQuery('#ofTabs').tabs('option', 'active', 3 );">Vezi toate comentariile</a>
    </div>
    <?php } else { ?>
    <img src="/images/icon_nou_small.png" alt="nou" class="float-left pad5">
    <div class="text-center NEW-round6px chenar-nota-comentarii float-left">
      <span class="black">La acest hotel nu exista comentarii</span><br>
      <a href="#comentarii" class="link-red bold" onclick="jQuery('#ofTabs').tabs('option', 'active', 3 );">Adauga comentariu</a>
    </div>
    <? } ?>
  </div>
  
  <br class="clear">
  
  <div class="Hline"></div>

  <?php if($detalii['valabila']=='nu') {
	  echo '<br /><br /><h2 class="text-center red" style="font-size:2.5em;">ACEASTA OFERTA NU ESTE VALABILA !</h2>';
	  echo '<br /><div class="text-center" style="font-weight:bold; font-size:2.5em;">Oferta curenta pentru <a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'" class="link-red">'.$detalii_hotel['denumire'].'</a></div><br /><br /><br /><br />';
  } ?>

<div class="NEW-column-left1">

  <div class="NEW-adresa bigger-11em">
    <?php echo $detalii_hotel['adresa'].' ';
	if($detalii_hotel['nr']) echo 'nr. '.$detalii_hotel['nr'].', ';
	echo $detalii_hotel['localitate'].', ';
	echo $detalii_hotel['tara'];
	if($detalii_hotel['distanta']) {
		echo ', <strong>'.$detalii_hotel['distanta'].'m de '.$detalii_hotel['distanta_fata_de'].'</strong>, ';
    }
	if($detalii_hotel['latitudine'] && $detalii_hotel['longitudine'] && $detalii_hotel['latitudine']<> 0 && $detalii_hotel['longitudine'] <> 0) { ?>
		(<a href="#harta" class="link" onclick="jQuery('#ofTabs').tabs('option', 'active', 2 );"><strong>Arata harta</strong></a>)
	<?php } ?>
  </div>
    
  <br class="clear">
  
  <div class="float-left" style="width:310px;">
    <div class="NEW-gallery">
    <?php if($detalii_hotel['poza1']) { ?>
      <div class="single-img">
        <img src="/img_mediu_hotel/<?php echo $detalii_hotel['poza1']; ?>" width="300" height="300" alt="<?php echo $detalii_hotel['denumire']." imagine principala"; ?>" class="images" id="poza_principala" />
        <?php if($detalii['last_minute']=='da') { echo '<div class="last-minute"></div>'; } ?>
        <?php if(sizeof($detalii['early_time'])>0) { echo '<div class="early-booking"></div>'; } ?>
        <?php if($detalii['spo_titlu']) { echo '<div class="oferta-speciala"></div>'; } ?>
      </div>
    <?php } else { ?>
      <div class="single-img"><img src="/images/no_photo.jpg" width="300" height="300" alt="" class="images" id="poza_principala" /></div>
    <?php } ?>
    </div>
  </div>

  <div class="float-left" style="width:358px;">
    <div class="NEW-gallery">
<?php
for($i=1;$i<=8;$i++) {
if($detalii_hotel['poza'.$i]) { ?>    
    <a href="/img_prima_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" onmouseover="document.getElementById('poza_principala').src='/img_mediu_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>';return false;" class="gallery"><img src="/thumb_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" width="80" alt="<?php echo $detalii_hotel['denumire']." imagine ".$i; ?>" class="images" /></a>
<?php }
} ?>
	</div>

    <div class="info-scurte-chn" style="padding-top:10px;">
      <?php if($detalii_hotel['concept']) { ?><div class="bigger-11em black bold" style="padding:0 0 10px 10px;">Recomandat pentru: <strong class="bigger-13em red"><?php echo $detalii_hotel['concept']; ?></strong></div><?php } ?>
      <br /><br />
	  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
      <br class="clear" />
    </div>

  </div>

</div>
<div class="NEW-column-right1">
<?php /*?><?php if(!isset($_SESSION['page_back'])) {
	$page_back = $link_cuvant_cheie_loc;
} else {
	$page_back = $_SESSION['page_back'];
} ?>
  <div class="blue" style="padding:0 5px 10px 0; text-align:right;">Inapoi la <a href="<?php echo $page_back; ?>" title="<?php echo $cuvant_cheie_loc; ?>"><?php echo $cuvant_cheie_loc; ?></a> &raquo;</div><?php */?>
  
  <div class="info-scurte-new">
    <?php /*?><div class="float-right">
      <img src="/images/trusted_plationline.png" alt="Garantia Plati Online" width="70"><br>
      <img src="/images/trusted_mastercard.png" alt="Garantia Mastercard" width="70"><br>
      <img src="/images/trusted_visa.png" alt="Garantia Visa" width="70">
    </div><?php */?>
    <ul>
      <li>Cod oferta: <strong><?php echo 'OSE'.$id_oferta; ?></strong></li>
      <?php if($detalii['nr_nopti']>1) { ?><li>Sejur: <strong><?php echo nopti($detalii['nr_nopti']); ?></strong></li><?php } ?>
      <li>Masa: <strong><?php echo $detalii['masa']; ?></strong></li>
      <li>Transport: <strong><?php echo $detalii['transport']; ?></strong></li>
      <?php if($detalii_hotel['distanta']) { ?><li><?php echo 'Distanta fata de <strong>'.$detalii_hotel['distanta_fata_de'].'</strong>: <strong>'.$detalii_hotel['distanta'].' m</strong>'; ?></li><?php } ?>
      <li class="anchor"><a href="#detalii-oferta" class="link-grey" onclick="jQuery('#ofTabs').tabs('option', 'active', 0 );" rel="nofollow">Vezi mai multe detalii</a></li>
    </ul>
    
    <div class="tarif">Tarif de la <span class="pret red"><?php echo new_price($preturi['minim']['pret']); ?></span> <span class="exprimare red"><?php echo moneda($detalii['moneda']); ?></span> pe <?php echo $detalii['exprimare_pret']; ?></div>
    
    <a href="#detalii-oferta" class="vezi-tarife" onclick="jQuery('#ofTabs').tabs('option', 'active', 0 );" rel="nofollow">Vezi toate tarifele</a>
  </div>
  
</div>

<br class="clear">

<?php if($detalii_hotel['detalii_concept']) { echo '<div class="detalii-concept bigger-11em bold"><em>'.$detalii_hotel['detalii_concept'].'</em></div>'; } ?>

<div class="NEW-column-full2">
  
  <div id="ofTabs">
    <ul>
      <li><a href="#detalii-oferta"><?php echo $detalii['denumire_scurta']; if($detalii['nr_nopti']>1) { echo ' - '.$detalii['nr_nopti'].' nopti'; } ?></a></li>
      <li><a href="#detalii-hotel">Detalii Hotel</a></li>
      <li><a href="#harta">Harta</a></li>
      <li><a href="#comentarii" onclick="ga('send', 'event', 'reviews', 'citeste reviews oferta', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');"><img src="/images/icon_nou_small.png" alt="nou"> Comentarii clienti</a></li>
	  <?php $oferte=$det->get_oferte($id_hotel, $id_oferta); if(sizeof($oferte)>0) { ?><li><a href="#alte-oferte">Alte oferte la <?php echo $detalii_hotel['denumire']; ?></a></li><?php } ?>
    </ul>
    
	<div id="detalii-oferta">

<div class="NEW-column-left1">
  
	<?php /*?><?php
	if($detalii_hotel['distanta']) {
		echo '<br /><div class="bold bigger-11em" style="padding:0 0 0 10px;">';
		echo '<img src="'.$imgpath.'/icon_dist_'.$detalii_hotel['distanta_fata_de'].'.jpg" alt="" title="'.$detalii_hotel['distanta'].'m de '.$detalii_hotel['distanta_fata_de'].'" class="NEW-round6px float-left" style="margin:0 10px 0 0;" />';
		echo '<span class="blue" style="line-height:32px;">'.$detalii_hotel['denumire'].'</span> este situat la <span class="bigger-11em red">'.$detalii_hotel['distanta'].'m de '.$detalii_hotel['distanta_fata_de'].'</span>';
		echo '</div><br class="clear" />';
    }
	?><?php */?>

  <div class="NEW-detalii-oferta">

	<?php if($preturi['camera']>0) {
	$early_inclus='nu'; ?>  

    <?php /*?><h2 class="underline"><img src="<?php echo $imgpath; ?>/icon_section_tarife.png" style="vertical-align:middle;" alt="Tarife disponibile" /> Tarife disponibile pentru <span class="bigger-11em blue"><?php echo $detalii_hotel['denumire']; ?> <?php echo $detalii_hotel['stele']; ?>*</span></h2><?php */?>
    <?php /*?><div class="info-scurte-new bigger-11em black bold">
      <table border="1" bordercolor="#FFFFFF" style="border-collapse:collapse;" width="100%">
        <tr>
          <?php if($detalii['nr_zile']>2 || $detalii['nr_nopti']>1) {
			  if(isset($detalii['nr_nopti'])) {
				  echo '<td class="left" width="10%">Sejur:</td><td class="right" width="20%">'.$detalii['nr_nopti'].' nopti</td>';
			  }
		  } else {
			  echo '<td class="left" width="10%">Cazare: </td><td class="right" width="20%">Pe noapte</td>';
		  } ?>
		  <td class="left" width="10%">Masa:</td><td class="right" width="27%"><?php echo $detalii['masa']; ?></td>
		  <td class="left" width="14%">Transport:</td><td class="right" width="19%"><?php echo $detalii['transport']; ?></td>
        </tr>
      </table>
    </div>
    <br class="clear" /><?php */?>
    <?php /*?><div class="section"><?php */?>
      <?php /*?><h2 class="section-title">Tarife pentru <?php echo $detalii['denumire']; ?></h2><?php */?>
      <?php /*?><p class="red" style="text-indent:0;">Reducere <strong>EARLY BOOKING</strong> pentru sejururi cuprinse in perioada <strong><?php echo $preturi['min_start'].' - '.$preturi['max_end']; ?></strong></p><?php */?>
	  <?php if(sizeof($detalii['early_time'])>0) { ?>
      <div class="red" style="padding:7px 0;">
      <?php foreach($detalii['early_time'] as $keyE=>$valueE) {
		  if($detalii['early_inclus'][$keyE]=='da') $early_inclus='da'; $val_erly='';
		if($detalii['early_disc'][$keyE]) { ?>
        <strong><?php echo $detalii['early_disc'][$keyE]; ?>% REDUCERE</strong> pentru rezervarile confirmate si achitate integral pana la data de <strong><?php echo $valueE; ?></strong><br />
      <?php if($detalii['text_early'][$keyE]) echo '&nbsp;&nbsp;&nbsp;-'.nl2br($detalii['text_early'][$keyE]).'<br/>'; } else $val_erly=$valueE;
      } ?>
      </div>
      <?php } ?>
    <?php /*?></div><?php */?>

    <?php if($early_inclus=='da') { ?>
    <div>Tarifele afisate mai jos sunt cu reducerea <a href="<?php echo $sitepath; ?>info-conditii-early-booking.html" rel="nofollow" class="infos link-blue"><strong>Early Booking</strong></a> inclusa <?php if($val_erly) echo 'pana la data de <strong>'.$val_erly.'</strong>'; ?>
		<?php if($detalii['oferta_parinte_early'] and $detalii['online_prices']!='da') { ?>
    	<a href="<?php echo $sitepath.'includes/sejururi/preturi_normale.php?oferta='.$detalii['oferta_parinte_early']; ?>" title="Preturi fara Early Booking" style="float:right" class="preturi_normale link-blue" rel="nofollow">Click aici pentru preturile fara reducere</a>
		<?php } ?>
    </div>
	<?php } ?>
    
    <br class="clear">
    --
    <?php if(sizeof($preturi['data_start'])<=10) { ?>
    <div style="overflow:auto;">
    <table class="tabel-preturi" style="width:100%;">
      <?php $nr_t=0;
	  foreach($preturi['data_start'] as $key_d=>$data_start) {
		  $nr_t++;
		  if($nr_t%2==1) $class_tr='impar'; else $class_tr='par';
		  $numar_minim_nopti='';
		  if(sizeof($detalii['data_start_nr_min'])>'0') {
			  foreach($detalii['data_start_nr_min'] as $key_nm=>$value_nm) {
				  if($value_nm<=$preturi['data_start_normal'][$key_d]) {
					  if($preturi['data_end_normal'][$key_d] && $preturi['data_end_normal'][$key_d]<>'0000-00-00') {
						  if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_end_normal'][$key_d]) {
							  $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
						  }
					  } else {
						  if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_start_normal'][$key_d]) {
							  $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
						  }
					  }
				  }
			  }
		  }
		  $stopsales[$key_d] = 0;
		  foreach($preturi['camera'] as $key_cam_ss=>$value_cam_ss) {
			  if(soldout($id_oferta, $key_cam_ss, $data_start)>0) $stopsales[$key_d] += 1;
		  }
		  if($stopsales[$key_d]==0) { ?>
	  <?php if($nr_t==1) { ?>
      <thead>
      <tr>
        <th class="text-center" style="width:150px;"><?php if($detalii['tip_preturi']=='plecari') echo 'Data plecarii'; else echo 'Perioade'; ?></th>
        <?php /*?><th width="120" align="center" valign="middle">Tip masa</th><?php */?>
        <?php $g=0;
		foreach($preturi['camera'] as $key_cam=>$value_cam) {
			$g++;
			if(sizeof($preturi['camera'])==$g) {
				$class='text-center last';
				$g=0;
			} else $class='text-center'; ?>
        	<th class="<?php echo $class; ?>"><?php echo $value_cam; ?></th>
        <?php } ?>
      </tr>
      </thead>
      <tbody>
	  <?php } ?>
          <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
            <th class="text-left nowrap">
			<?php if(sizeof($preturi['include_tip'][$key_d])>0) {
				echo '<div class="text-right icon-tem">';
				foreach($preturi['include_tip'][$key_d] as $key_inc=>$include) echo '<a href="'.$sitepath.$_REQUEST['tara'].'/'.$_REQUEST['localitate'].'/'.fa_link($include['denumire']).'-'.$include['id_oferta'].'.html"><img src="'.$imgpath.'/oferte/icon_tematica_mica_'.$include['id_tip_oferta'].'.png" /></a>';
				echo '</div>';
			}
			echo $data_start;
			if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') echo ' - '.$preturi['data_end'][$key_d];
			?><br />
            <?php if(strlen($numar_minim_nopti)>4) echo '<em>'.$numar_minim_nopti.'</em><br />'; ?>
			<?php /*if(sizeof($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]])>0) {
				?><div class="red"><a onmousedown="if(document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display == 'none'){ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'block'; }else{ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'none'; }" style="cursor:pointer">toate preturile</a></div>
			<?php }*/ ?>
            </th>
            <?php /*?><td align="center" valign="middle"><?php
			foreach($preturi['camera'] as $key_cam=>$value_cam) {
				echo $preturi['tip_masa'][$key_d][$key_cam];
			}
			?></td><?php */?>
			<?php $g=0;
			foreach($preturi['camera'] as $key_cam=>$value_cam) {
				$g++;
				if(sizeof($preturi['camera'])==$g) {
					$class='text-center pret last';
					$g=0;
				} else $class='text-center pret';
				if(sizeof($preturi['camera'])=="1") $wdt='70%';
				elseif(sizeof($preturi['camera'])=="2") $wdt='35%';
				elseif(sizeof($preturi['camera'])=="3") $wdt='23%';
				elseif(sizeof($preturi['camera'])=="4") $wdt='17%';
				elseif(sizeof($preturi['camera'])=="5") $wdt='14%';
				elseif(sizeof($preturi['camera'])=="6") $wdt='11%';
			?>
            <td  <?php echo 'class="'.$class.'"'; ?> style="width:<?php echo $wdt; ?>">
			<?php if($preturi['pret'][$key_d][$key_cam]) {
				$pret_afisare = new_price($preturi['pret'][$key_d][$key_cam]).' ';
				if($preturi['moneda'][$key_d][$key_cam]=='EURO') {
					$moneda_afisare = '&euro;';
					$valuta='da';
				} elseif($preturi['moneda'][$key_d][$key_cam]=='USD') {
					$moneda_afisare = '$';
					$valuta='da';
				} else $moneda_afisare = $preturi['moneda'][$key_d][$key_cam];
				/*if((soldout($id_oferta, $key_cam, $data_start)>0) and !($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000')) {
					echo '<span class="soldout2 red" title="Aceasta perioada nu este disponibila. Camera se afla in STOP SALES.">STOP SALES<span class="pretz">'.$pret_afisare.$moneda_afisare.'</span></span>';
				} else {
					echo $pret_afisare.$moneda_afisare;
				}*/
				echo $pret_afisare.$moneda_afisare;
			} else echo '-'; ?>
            </td>
			<?php } ?>
          </tr>
          <?php } else echo '<tr><td class="text-center pad10"><span class="red bold bigger-13em">Ne pare rau, momentan nu sunt preturi disponibile.</span></td></tr>'; ?>
      <?php } ?>
      </tbody>
    </table>
    </div>
    <?php } else { ?>
    <div id="tarife" style="overflow:auto;">&nbsp;</div>
    <?php }
	}
	?>
	
    <div class="grey2 bold" style="text-align:right; padding:0 5px 10px 5px;">* tarifele sunt exprimate in <span class="bigger-13em blue"><?php echo $detalii['moneda']; ?> pe <?php echo $detalii['exprimare_pret']; ?></span></div>

    <?php /*?><div class="NEWrezervareLINE">
	  <div class="call-center">
    	<span class="tit black">Call center Rezervari</span>
		<span class="tel red"><?php echo $contact_telefon; ?></span>
		<span class="prg"><?php echo $contact_program; ?></span>
      </div>
      <?php if($detalii['valabila']=='da') { ?><a href="<?php if($detalii['rezervare_online']=='da') { echo $link_rezervare2; } else { echo $link_rezervare; } ?>" rel="nofollow" class="REZERVA-online-NEW2 float-right" style="margin:11px 10px 0 0;"></a><?php } ?>
      <a href="<?php echo $link_cerere_detalii; ?>" rel="nofollow" class="CERE-detalii-NEW2 float-right" style="margin:11px 30px 0 0;"></a>
	  <br class="clear" />
    </div><?php */?>
    
	<?php if($detalii['rezervare_online']=='da' and $detalii['valabila']=='da') {
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/calculare_pret_new.php");
	} else {
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/cerere_detalii.php");
		
		if(!is_bot() and isset($err_logare_admin) and $detalii['valabila']=='da') {
			erori_disponibilitate($id_oferta, $id_hotel, $plecdata, $pleczile, $adulti, $copii);
		}
		
	} ?>
    
    <br class="clear"><br><br>
    
	<?php /*?><?php if($detalii['apare_grad']=='da') { ?>
    <div class="infobox-ok black NEW-round8px clearfix" style="font-size:13px; font-weight:normal; padding:5px;"><img src="<?php echo $imgpath; ?>/icon_calculator.png" alt="" style="float:left; margin:2px 15px 2px 10px;" /><a href="#calculeaza-vacanta" class="af_grad"><img src="<?php echo $imgpath; ?>/icon_arrow_up.png" alt="" style="float:right; margin:2px 0 2px 10px;" /></a>Pentru a vedea preturile si pentru copii va rugam folositi <strong>"CALCULEAZA VACANTA"</strong> de pe coloana din dreapta, imediat sub localizarea hotelului pe harta.</div><br />
    <?php } ?><?php */?>
    
    <?php /*?><div class="pad10">Plata se va face in <?php if($valuta=='nu') { echo 'LEI'; } else { echo $detalii['moneda']; } if($valuta=='da') { echo ' sau in LEI la cursul BNR +2%'; } ?>. <a href="<?php echo $sitepath; ?>info-cum-platesc.html" title="Cum platesc" rel="infos" class="link-blue">Click aici</a> pentru modalitatile de plata disponibile.</div>
    <br class="clear" /><?php */?>
	
 <?php /*   <?php if(($detalii_hotel['tara']=='Turcia') or ($detalii_hotel['tara']=='Grecia') or ($detalii_hotel['tara']=='Dubai')) { ?>
    <div class="puncte-fidelizare">
	  <div><span class="txt1">Aceasta oferta iti poate aduce MINIM</span> <span class="txt2 blue"><?php echo create_puncte($detalii['pret_minim_lei']); ?> puncte</span></div>
      Punctele de fidelitate se obtin sub forma de voucher. <a href="<?php echo $sitepath; ?>info-puncte-fidelitate.html" class="infos link-black2" rel="nofollow" title="Puncte de fidelitate"><strong>Click aici pentru detalii &raquo;</strong></a>
    </div>
    <br>
	<?php } ?>*/?>
    
	<?php
	$sel_detcam = "SELECT camere_hotel.detalii, tip_camera.denumire FROM camere_hotel INNER JOIN tip_camera ON tip_camera.id_camera = camere_hotel.id_camera WHERE id_hotel='".$detalii['id_hotel']."' AND (detalii<>NULL OR detalii<>'')";
	$que_detcam = mysql_query($sel_detcam) or die(mysql_error());
	if(mysql_num_rows($que_detcam)>'0') {
	?>
    <div class="section">
      <h2 class="blue underline"><img src="<?php echo $imgpath; ?>/icon_section_camere.png" style="vertical-align:middle;" alt="Detalii camere" /> Detalii camere <?php echo $detalii_hotel['denumire']?></h2>
		<?php while($row_detcam = mysql_fetch_array($que_detcam)){
            echo '<h3 class="black">'.$row_detcam['denumire'].'</h3>';
            echo '<div class="content">'.nl2br($row_detcam['detalii']).'</div>';
            echo '<div class="clear-line"></div>';
        } ?>
    </div>
    <?php } ?>
    
    <?php
    $sel_stopsales = "SELECT sold_out.camera, tip_camera.denumire FROM sold_out LEFT JOIN tip_camera ON tip_camera.id_camera=sold_out.camera WHERE id_oferta='".$id_oferta."' GROUP BY tip_camera.denumire";
    $que_stopsales = mysql_query($sel_stopsales) or die(mysql_error());
    if(mysql_num_rows($que_stopsales)>0) {
    ?>
    <div class="NEW-chn-oferta-red NEW-round8px mar5">
      <h2 class="section-title-red" style="margin-bottom:10px;"><span class="blue">STOP SALES!</span> Perioadele de <span class="blue">STOP SALES</span> pentru aceasta oferta sunt:</h2>
      <div class="bigger-11em" style="padding:0 10px 10px 10px;">
	  <?php
        while($row_stopsales = mysql_fetch_array($que_stopsales)) {
            echo '<strong>'.$row_stopsales['denumire'].':</strong> &nbsp; ';
            $sel_stopsales2 = "SELECT data_start AS soldout_start, data_end AS soldout_end FROM sold_out WHERE id_oferta='".$id_oferta."' AND camera='".$row_stopsales['camera']."'";
            $que_stopsales2 = mysql_query($sel_stopsales2) or die(mysql_error());
            while($row_stopsales2 = mysql_fetch_array($que_stopsales2)) {
                echo date('d/m/Y',strtotime($row_stopsales2['soldout_start'])).' - '.date('d/m/Y',strtotime($row_stopsales2['soldout_end'])).'; &nbsp; ';
            }
            echo '<br />';
        } ?>
	  </div>
    </div>
	<?php } ?>

	<?php /*if($detalii['taxa_avion']['inclus']=='da') { ?>
    <div class="infobox-ok NEW-round8px clearfix" style="float:left; width:47%;"><img src="<?php echo $imgpath; ?>/icon_ok.png" alt="" style="vertical-align:middle; margin:2px 10px;" />Taxa de aeroport este inclusa</div>
    <div class="infobox-ok NEW-round8px clearfix red" style="float:right; width:48%;"><img src="<?php echo $imgpath; ?>/icon_cross.png" alt="" style="vertical-align:middle; margin:2px 10px;" />Asigurarea medicala este inclusa</div>
    <br class="clear" /><br />
    <?php }*/ /*elseif($detalii['taxa_avion']['inclus']=='nu') { ?>
    <div class="infobox-warning NEW-round8px clearfix"><img src="<?php echo $imgpath; ?>/icon_warning.png" alt="" style="vertical-align:middle; margin:2px 10px;" />Taxa de aeroport nu este inclusa. <span class="blue">Pret: <?php echo $detalii['taxa_avion']['pret']." "; if($detalii['taxa_avion']['moneda']=='EURO') echo '&euro;'; elseif($detalii['taxa_avion']['moneda']=='USD') echo '$'; elseif($detalii['taxa_avion']['moneda']=='PROCENT') echo '%'; else echo $detalii['taxa_avion']['moneda']; ?></span></div><br class="clear" /><br />
	<?php }*/ ?>
	
	<?php if(sizeof($detalii['data_start_reduceri_speciale'])>'0') { ?>
    <div class="section">
      <h2 class="red underline" style="margin-bottom:5px;"><img src="<?php echo $imgpath; ?>/icon_section_reduceri.png" style="vertical-align:middle;" alt="Reduceri" /> Reduceri speciale <?php echo $detalii_hotel['denumire']; ?></h2>
      <?php foreach($detalii['data_start_reduceri_speciale'] as $jey_r=>$value_r) { ?>
      <p style="text-indent:10px;">Pentru sosirile cuprinse in perioada <strong class="green"><?php echo $value_r.' - '.$detalii['data_end_reduceri_speciale'][$jey_r]; ?></strong> <span class="red">platesti <?php echo '<strong>'.$detalii['zile_deaplicare'][$jey_r].' nopti</strong> si stai <strong>'.$detalii['zile_aplicare'][$jey_r].' nopti</strong>'; ?></span></p>
      <?php } ?>
      <div class="chenar-info NEW-orange NEW-round8px mar10">Pentru un calcul corect al tarifului pe camera, va rugam sa selectati numarul de nopti pe care il veti plati (<?php echo $detalii['zile_deaplicare'][0]; ?>) conform promotiei de mai sus, nu cate zile veti sta efectiv (<?php echo $detalii['zile_aplicare'][0]; ?>).</div>
    </div>
    <?php } ?>
    
    <?php if(sizeof($detalii['denumire_v3'])>'0' || sizeof($detalii['denumire_v4'])>'0') { ?>
    <div class="section">
      <?php /*?><h2 class="blue underline"><img src="<?php echo $imgpath; ?>/icon_section_oferta.png" style="vertical-align:middle;" alt="Descriere oferta" /> Informatii suplimentare oferta <?php echo $detalii_hotel['denumire']?></h2><?php */?>
      <?php /*?><?php if(sizeof($detalii['denumire_v3'])>'0') { ?>
      <h3 class="black">Suplimente</h3>
      <div class="content">
        <ul>
        <?php foreach($detalii['denumire_v3'] as $key=>$value) { ?>
        <li><?php echo ucfirst($value);
			if($detalii['data_start_v3'][$key]) {
				echo ' <em>('.date("d.m.Y", strtotime($detalii['data_start_v3'][$key])).' - '.date("d.m.Y", strtotime($detalii['data_end_v3'][$key])).')</em>';
			}
			if($detalii['value_v3'][$key]) {
				echo ' - <strong>'.$detalii['value_v3'][$key].' '.moneda($detalii['moneda_v3'][$key]).' '.$detalii['exprimare_v3'][$key].' '.$detalii['pasager_v3'][$key].'</strong>';
			}
			if($detalii['obligatoriu_v3'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>';
				else echo ' - <em class="blue">Optional</em>'; ?></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?><?php */?>
	  <?php if(sizeof($detalii['denumire_v4'])>'0') { ?>
      <h3 class="red">Reduceri</h3>
      <div class="content">
        <ul>
        <?php foreach($detalii['denumire_v4'] as $key=>$value) { ?>
        <li><?php echo ucfirst($value); if($detalii['value_v4'][$key]) { echo ' - <strong>'.$detalii['value_v4'][$key].' '.moneda($detalii['moneda_v4'][$key]).'</strong>'; } ?></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
    </div>
	<?php } ?>
	<?php $excursii=$det->get_excursii($id_oferta);
	if(sizeof($excursii)>0) { ?>
    <div class="section">
      <h3 class="red">Excursii optionale</h3>
      <div class="content">
        <ul>
        <?php foreach($excursii as $key_e=>$value_e) { ?>
        <li><a href="<?php echo $sitepath.'excursii-optionale/'.fa_link($detalii_hotel['tara']).'/'.fa_link_oferta($value_e['denumire']).'-'.$value_e['id_excursie'].'.html'; ?>" rel="nofollow" class="excursii_detalii link-blue"><?php echo $value_e['denumire']; if($value_e['pret']) { echo ' - <em class="red">'.$value_e['pret'].' '; echo moneda($value_e['moneda']); echo '</em> - '.ucwords($value_e['tip']); } else echo ' - <em class="red bold">'.ucwords($value_e['tip']).'</em>'; ?></a></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
    </div>
	<?php } ?>

    <br class="clear" />

	<?php if($id_oferta) {
	$valuta='nu';
	if($detalii["descriere_oferta"]) { ?>
    <div class="section">
      <h2 class="section-title" style="margin-bottom:5px;"><?php echo $detalii["denumire"]; ?></h2>
      <?php afisare_frumos(schimba_caractere($detalii["descriere_oferta"]), 'nl2p'); ?>
    </div>
    <?php } ?>

    <br class="clear" />
  
    <div class="section">
	  <?php if($detalii['conditii_plata']<0) { ?>
      <h3 class="black">Conditii de plata</h3>
      <div class="content">
        <ul>
          <li><?php echo afisare_frumos($detalii['conditii_plata'], 'nl2br'); ?></li>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
	  <?php if($detalii['conditii_anulare']) { ?>
      <h3 class="black">Conditii de anulare</h3>
      <div class="content">
        <ul>
          <li><?php echo afisare_frumos($detalii['conditii_anulare'], 'nl2br'); ?></li>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
    </div>
    
  <?php /*?><?php $aeroport=array(); if($detalii['transport']=='Avion') $aeroport=$det->get_avion_sejur($id_oferta);
  if(sizeof($aeroport)>0) { ?>
    <div class="section">
      <h2 class="section-title">Orar de zbor</h2>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabel-transport">
        <thead>
        <tr>
          <th align="left" valign="top">&nbsp;</th>
          <th align="center" valign="top">Aeroport Plecare</th>
          <th align="center" valign="top">Ora Plecare</th>
          <th align="center" valign="top">Aeroport Sosire</th>
          <th align="center" valign="top">Ora Sosire</th>
        </tr>
        </thead>
        <tbody>
        <?php $tip_c=''; $tip_v=''; foreach($aeroport as $key_a=>$value_a) {
		if($tip_c<>$value_a['tip']) { ?>
        <tr>
          <th colspan="5" align="left" valign="top"><?php echo strtoupper($value_a['tip']); ?></th>
        </tr>
        <?php $tip_c=$value_a['tip']; $tip_v=$value_a['tip']; } ?>
        <tr>
          <td align="left" valign="middle"><img src="<?php echo $imgpath; ?>/avion/<?php echo $value_a['companie']; ?>.jpg" height="30" alt="<?php echo $value_a['denumire_companie']; ?>" title="<?php echo $value_a['denumire_companie']; ?>" /></td>
          <td align="left" valign="middle"><?php echo $value_a['denumire_aeroport_plecare'].' ('.$value_a['denumire_loc_plecare'].')'; ?></td>
          <td align="center" valign="middle"><?php echo $value_a['ora_plecare']; ?></td>
          <td align="left" valign="middle"><?php echo $value_a['denumire_aeroport_sosire'].' ('.$value_a['denumire_loc_sosire'].')'; ?></td>
          <td align="center" valign="middle"><?php echo $value_a['ora_sosire']; ?></td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>

    <br class="clear" />
    <?php } ?><?php */?>
    
    <?php if(sizeof($autocar)>0) { ?>     
    <div class="section">
      <h2 class="section-title">Orar transport</h2>
      <table class="tabel-transport" style="width:100%;">
        <thead>
        <tr>
          <th class="text-center" style="width:180px;">Localitate Plecare</th>
          <th class="text-center" style="width:120px;">Ora Plecare</th>
          <th class="text-center">Detalii</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($autocar as $key_a=>$value_a) { ?>
        <tr>
          <td class="text-left"><?php echo $value_a['denumire_localitate']; ?></td>
          <td class="text-center"><?php echo $value_a['ora']; ?></td>
          <td class="text-left"><?php echo $value_a['detalii']; ?></td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>

    <br class="clear" />
    <?php } ?>    

	<?php /*?><?php if($detalii['valabila']=='da') { ?>
    <div class="NEW-blue NEW-round8px rezerva-lung clearfix">
      <a href="<?php echo $link_rezervare; ?>" rel="rezervare"><img src="<?php echo $imgpath; ?>/button_rezerva_oferta.gif" alt="" style="float:left;" /></a>
      <div class="telefon" align="right"><span class="green"><?php echo $contact_telefon; ?></span><br /><?php echo $contact_program; ?></div>
      <br class="clear" />
    </div>
	<?php } ?><?php */?>
    
<?php } ?>
    
    <?php /*?><br class="clear" /><br /><br />
    
    <div class="fb-comments" data-href="<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>" data-num-posts="10" data-width="640"></div><?php */?>
  
    <br class="clear" /><br />
    
   </div>

<?php
if(get_id_zona($detalii_hotel['zona'], get_id_tara($detalii_hotel['tara']))=='1317') {
	/*echo '<br />'; include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/oferte_transport/bulgaria_paste_1mai.php");*/
	/*echo '<br />'; include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/oferte_transport/bulgaria_vara.php");*/
}
if(get_id_zona($detalii_hotel['zona'], get_id_tara($detalii_hotel['tara']))=='24') {
	if($detalii['transport']=="Fara transport") {
		/*echo '<br />'; include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/oferte_transport/turcia_kusadasi_vara.php");*/
	}
}
?>
  <br class="clear" />
  
</div>
<div class="NEW-column-right1">

  <?php if($detalii_hotel['tara']=='Bulgaria') { ?>
  <a href="/transport-bulgaria.html" title="Oferta de Transport in Bulgaria" target="_blank" class="button-blue text-center" style="display:block; margin:7px 0 12px 0;"><span class="white">Transport Bulgaria</span></a>
  <?php } ?>
	
  <div class="right-serv" style="margin:7px 0 0 0;">

	<?php if(sizeof($detalii['denumire_v1'])>'0') { ?>
    <div>
      <h3 class="black underline">Tarifele includ</h3>
      <ul>
      <?php foreach($detalii['denumire_v1'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value)); if($detalii['value_v1'][$key]) { echo ' - <strong>'.$detalii['value_v1'][$key].' '.moneda($detalii['moneda_v1'][$key]).'</strong>'; } ?>
      <?php if(strpos(strtolower($value),'asigurare') !== false and (strpos(strtolower($value),'sanatat') !== false or strpos(strtolower($value),'medical') !== false) and strpos(strtolower($value),'storn') !== false) { $asigurare = 'da'; ?><div class="bubble-asigurari-small bubble-top white"><a href="/files/travel.pdf" target="_blank"><strong>Asigurări de călătorie oferite de</strong><br><img src="/images/logo_mondial_assistance.png" alt="Mondial Assistance" class="w100"><span class="float-right smaller-09em underline"><br>&raquo; click aici</span><br class="clear"></a></div><?php } ?>
      </li>
      <?php } ?>
      <?php if(strpos(strtolower($detalii['transport']),'avion') !== false and strpos(strtolower(implode(" ",$detalii['denumire_v1'])),'bagaj') === false) echo '<li>Bagaj de mana si bagaj de cala</li>'; ?>
      <?php if($detalii['transport']!='Fara transport' and strpos(strtolower(implode(" ",$detalii['denumire_v1'])),'asistenta') === false) echo '<li>Asistenta turistica in limba romana</li>'; ?>
      </ul>
    </div>
	<?php } ?>

	<?php if(sizeof($detalii['denumire_v2'])>'0' or sizeof($detalii['denumire_v3'])>'0') { ?>
    <div class="separator">
      <h3 class="black underline">Neincluse in tarif</h3>
      <ul>
      <?php if(sizeof($detalii['denumire_v2'])>'0') {
		  foreach($detalii['denumire_v2'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value));
          if($detalii['data_start_v2'][$key]) {
              echo ' <em>('.date("d.m.Y", strtotime($detalii['data_start_v2'][$key])).' - '.date("d.m.Y", strtotime($detalii['data_end_v2'][$key])).')</em>';
          }
          if($detalii['value_v2'][$key]) {
              echo ' - <strong>'.$detalii['value_v2'][$key].' '.moneda($detalii['moneda_v2'][$key]).' '.$detalii['exprimare_v2'][$key].' '.$detalii['pasager_v2'][$key].'</strong>';
          }
          if($detalii['obligatoriu_v2'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>';
              else echo ' - <em class="blue">Optional</em>'; ?>
	  </li>
      <?php if(strpos(strtolower($value),'asigurare') !== false and (strpos(strtolower($value),'sanatat') !== false or strpos(strtolower($value),'medical') !== false) and strpos(strtolower($value),'storn') !== false) $asigurare = 'nu'; ?>
      <?php }
	  } ?>
	  <?php if(sizeof($detalii['denumire_v3'])>'0') {
		  foreach($detalii['denumire_v3'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value));
          if($detalii['data_start_v3'][$key]) {
              echo ' <em>('.date("d.m.Y", strtotime($detalii['data_start_v3'][$key])).' - '.date("d.m.Y", strtotime($detalii['data_end_v3'][$key])).')</em>';
          }
          if($detalii['value_v3'][$key]) {
              echo ' - <strong>'.$detalii['value_v3'][$key].' '.moneda($detalii['moneda_v3'][$key]).' '.$detalii['exprimare_v3'][$key].' '.$detalii['pasager_v3'][$key].'</strong>';
          }
          if($detalii['obligatoriu_v3'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>';
              else echo ' - <em class="blue">Optional</em>'; ?>
	  </li>
      <?php if(strpos(strtolower($value),'asigurare') !== false and (strpos(strtolower($value),'sanatat') !== false or strpos(strtolower($value),'medical') !== false) and strpos(strtolower($value),'storn') !== false) $asigurare = 'nu'; ?>
      <?php }
	  } ?>
      </ul>
      <?php if($asigurare=='nu' and $detalii_hotel['tara']!='Romania') { ?><div class="bubble-asigurari-small bubble-bottom white mar10"><a href="/info-asigurari-de-calatorie.html" class="infos2"><strong>Asigurări de călătorie oferite de</strong><br><img src="/images/logo_mondial_assistance.png" alt="Mondial Assistance" class="w100"><span class="float-right smaller-09em underline"><br>&raquo; click aici</span><br class="clear"></a></div><?php } ?>
    </div>
    <?php } ?>

	<?php $aeroport=array(); if($detalii['transport']=='Avion') $aeroport=$det->get_avion_sejur($id_oferta);
    if(sizeof($aeroport)>0) { ?>
    <div class="separator">
      <h3 class="black underline">Orar de zbor</h3>
      <div style="padding:5px 0;">
        <img src="/images/avion/<?php echo $aeroport[0]['companie']; ?>.jpg" alt="<?php echo $aeroport[0]['denumire_companie']; ?>" style="margin:0 5px 0 13px; vertical-align:middle;"> <strong class="underline"><?php echo $aeroport[0]['denumire_companie']; ?></strong>
        <?php $tip_c=''; $tip_v=''; $i=0; foreach($aeroport as $key_a=>$value_a) { $i++; ?>
          <?php if($tip_c<>$value_a['tip']) { ?>
          <?php /*?><span class="black bigger-11em bold">- <?php echo strtoupper($value_a['tip']); ?></span><?php */?>
          <?php $tip_c=$value_a['tip']; $tip_v=$value_a['tip']; } ?>
          <ul style="margin-bottom:5px;">
            <li><strong>Plecare:</strong> <?php echo $value_a['denumire_aeroport_plecare'].' ('.$value_a['denumire_loc_plecare'].') - <strong>'.$value_a['ora_plecare'].'</strong>'; ?></li>
            <li><strong>Sosire:</strong> <?php echo $value_a['denumire_aeroport_sosire'].' ('.$value_a['denumire_loc_sosire'].') - <strong>'.$value_a['ora_sosire'].'</strong>'; ?></li>
          </ul>
          <?php if($i<sizeof($aeroport)) echo '&nbsp;&nbsp;&nbsp; --------'; ?>
        <?php } ?>
      </div>
    </div>
    <?php } ?>

	<?php if($detalii['nota']) { ?>
    <div class="separator">
      <h3 class="black underline">Nota</h3>
      <div class="italic" style="padding:5px 13px;">
        <?php afisare_frumos(schimba_caractere(trim($detalii['nota'])), 'nl2br'); ?>
      </div>
    </div>
    <?php } ?>
    
  </div>
  
  <br class="clear"><br>
  
  <?php /*?><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/alte_oferte_hotel.php"); ?><?php */?>
  <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta_new.php"); ?>
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/new_hotel.php"); ?>
  <?php if(isset($_GET['plecdata'])) include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta_new.php"); ?>

</div>

<br class="clear" />

<div class="chenar-info NEW-orange NEW-round8px" style="padding-top:12px; min-height:22px;">Continutul ofertei este valabil la data introducerii acesteia. Datorita specificului industriei de turism ofertele se pot schimba foarte des si nu pot fi actualizate in timp util.</div>
    
    </div>
    
    <div id="detalii-hotel">
  
	  <?php if($detalii_hotel['fisier_upload']) { ?><p style="padding:5px 10px 0 10px;"><span class="blue icon-coffee"></span> <a href="/uploads/hotel/<?php echo $detalii_hotel['fisier_upload']; ?>" target="_blank" class="link-blue"><strong class="blue">Concept <?php echo $detalii_hotel['denumire']; ?></strong></a></p><?php } ?>
	  
	  <?php if($detalii_hotel['website'] and $detalii_hotel['tara']!='Romania') { ?><p style="padding:5px 10px 0 10px;"><span class="blue icon-link"></span> <a href="<?php echo hotel_link($detalii_hotel['website']); ?>" target="_blank" class="link-blue"><strong class="blue">Adresa website <?php echo $detalii_hotel['denumire']; ?></strong>: <?php echo hotel_link($detalii_hotel['website']); ?></a></p><?php } ?>
	  
	  <?php if($detalii_hotel['new_descriere']) { ?>
  
      <div class="section" style="margin:0;">
        <?php if($detalii_hotel["comentariul_nostru"]) { ?>
        <p class="black"><strong><u>Comentariul nostru</u>:</strong> <?php afisare_frumos(schimba_caractere($detalii_hotel["comentariul_nostru"]), ''); ?></p>
        <br />
        <?php } ?>
        <?php afisare_frumos(schimba_caractere($detalii_hotel["new_descriere"]), 'nl2p'); ?>
        <div id="detHotNew-container">
          <?php if($detalii_hotel["new_camera"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Camera</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_camera"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_teritoriu"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Teritoriu</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_teritoriu"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_relaxare"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Relaxare si sport</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_relaxare"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_pentru_copii"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Pentru copii</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_pentru_copii"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_plaja"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Plaja</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_plaja"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
        </div>
      </div>
  
      <?php } else { ?>
      
      <div class="section" style="margin:0;">
        <div class="columns pad10"><?php afisare_frumos(schimba_caractere($detalii_hotel["descriere_scurta"]), 'nl2p'); afisare_frumos(schimba_caractere($detalii_hotel["descriere"]), 'nl2p'); ?></div>
  
      <?php if($detalii_hotel["general"] || $detalii_hotel["servicii"] || $detalii_hotel["internet"] || $detalii_hotel["parcare"]) { ?>
        <?php if($detalii_hotel["general"]) { ?>
        <h3 class="black">General</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["general"], 'nl2br'); ?></div>
        <div class="clear-line"></div> 
        <?php }
        if($detalii_hotel["servicii"]) { ?>
        <h3 class="black">Servicii</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["servicii"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["internet"]) { ?>
        <h3 class="black">Internet</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["internet"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["parcare"]) { ?>
        <h3 class="black">Parcare</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["parcare"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php } ?>
        <?php if( ($detalii_hotel["check_in"]) or ($detalii_hotel["check_out"]) ) { ?>
        <h3 class="black">Check-in / check-out</h3>
        <div class="content"><?php echo $detalii_hotel["check_in"]; ?> / <?php echo $detalii_hotel["check_out"]; ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["info_copii"]) { ?>
        <h3 class="black">Copii si paturi suplimentare</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["info_copii"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["accepta_animale"]) { ?>
        <h3 class="black">Animale companie</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["accepta_animale"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php } ?>
        <br class="clear" /><br />
        <div class="chenar-info NEW-orange NEW-round8px">Clasificarea pe stele a unitatilor de cazare din program este cea atribuita in mod oficial de Ministerul Turismului din <?php echo $detalii_hotel['tara']; ?> conform standardelor proprii.</div>
      <?php } ?>
      </div>
    
      <?php } ?>
      
      <?php if($detalii_hotel['tur_virtual_3d']) echo '<h2 class="blue underline" id="tur3d"><span class="red">NOU!</span> Tur virtual 3D '.$detalii_hotel['denumire'].'</h2><div class="inserts">'.$detalii_hotel['tur_virtual_3d'].'</div><br>'; ?>
      
      <?php if($detalii_hotel['video_youtube']) echo $detalii_hotel['video_youtube'].'<br><br>'; ?>
      
    </div>

    <div id="harta">
       <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/harta-hotel.php"); ?>
      <div class="text-center smaller-09em grey">Poziționarea hotelurilor pe hartă este cu titlu informativ.</div>
    </div>
	
    <div id="comentarii">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri/reviews.php"); ?>
    </div>

	<?php $oferte=$det->get_oferte($id_hotel, $id_oferta);
    if(sizeof($oferte)>0) { ?>
    <div id="alte-oferte" class="afisare-oferta">
      <table class="tabel-oferte" style="width:100%;">
        <thead>
          <tr>
            <th class="text-center">&nbsp;</th>
            <th class="text-center" style="width:80px;">Nr. nopti</th>
            <th class="text-center" style="width:120px;">Masa</th>
            <th class="text-center" style="width:120px;">Transport</th>
            <th class="text-center" style="width:170px;">Tarif de la</th>
            <th class="text-center" style="width:80px;">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
		  <?php foreach($oferte as $key_of=>$value_of) { $c++;
          if($c%2==1) $class_tr='impar'; else $class_tr='par';
          $link=make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $value_of['denumire_scurta'], $value_of['id_oferta']);
          $tipuri=explode('+',$value_of['poza_tip']);
          if($value_of['taxa_aeroport']=='da') {
              $taxa_aeroport = ' - <span class="red" title="Taxe aeroport INCLUSE">Taxe <strong>INCLUSE</strong></span>';
              $taxa_aeroport2 = ' <img src="/images/dificultate_partie_i.gif" alt="Taxe aeroport INCLUSE" title="Taxe aeroport INCLUSE" style="vertical-align:middle;" width="14" />';
          } else {
              $taxa_aeroport = '';
              $taxa_aeroport2 = '';
          }
		  if($value_of['nr_nopti']>0) {
			  $zile=$value_of['nr_nopti'];
			  if($value_of['nr_nopti']>1) $zile=''.$zile.' nopti';
			  else $zile='Pe noapte';
		  }
          ?>
          <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
            <td class="text-center"><a href="<?php echo $link; ?>" title="<?php echo $value_of['denumire']; ?>"><?php echo '<strong class="bigger-11em">'.$value_of['denumire_scurta'].'</strong> '.$taxa_aeroport; ?></a></td>
            <td class="text-center"><?php echo $zile; ?></td>
            <td class="text-center"><?php echo $value_of['masa']; ?></td>
            <td class="text-center"><?php echo transport($value_of['denumire_transport']).' '.$taxa_aeroport2; ?></td>
            <td class="text-center"><span class="pret"><?php if(!$value_of['pret_minim']) { $pret=pret_minim_sejur($value_of['id_oferta'], '', '', ''); if($pret['pret']) { echo $pret['pret'].' '; echo moneda($pret['moneda']); } } else { echo $value_of['pret_minim'].' '; echo moneda($value_of['moneda']); } ?></span> <?php echo $value_of['exprimare_pret']; ?></td>
            <td class="text-center last"><a href="<?php echo $link; ?>" rel="nofollow"><img src="/images/button_detalii.png" alt="<?php echo $value_of['denumire']; ?>" /></a></td>
          </tr>
		  <?php } ?>
        </tbody>
      </table>
    </div>
    <?php } ?>
  </div>

</div>

</div>
<br class="clear" />
