<div class="sejururi">
    <?php //echo "-".$link_zona;?>
    <?php
    if ($nmasa == 'all inclusive') {
        $titlu_pag = 'Oferte All Inclusive ' . $den_zona;
    }
    ?>
    <h1 class="main-title">
        <?php if ($_GET['early-booking']) {
            echo '<span class="green">Early Booking</span> ';
        }
        echo $titlu_pag ?>
        <?php if ($localitate_plecare != '') {
            echo ' <br />Plecare din <span class="green">' . ucwords($localitate_plecare) . "</span>";
        }
        echo '- ' . $nr_hoteluri . ' Hoteluri din ' . $den_loc . ' Disponibile'; ?>
    </h1>
    <h2 class="main-subtitle green">Comparam preturile pentru <span
                class="textatentie">&nbsp;<?php echo $den_loc ?>&nbsp;</span> de la TourOperatori si va oferim
        <span class="textatentie">cel mai bun Pret!</span>
    </h2>
    <?php //include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/localitati_plecare_avion_responsive.php" ); ?>
    <?php
    if ($nr_hoteluri > 0) {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/sejururi/filtru_location_responsive.php');
    }
    ?>
</div>

<div class="sejururi show-mobile">
    <button class="show-filters">
        <span>Filtreaza</span>
        <span class="hide">Inchide filtre</span>
    </button>
    <script>
        $('.show-filters').on('click', function () {
            $('.show-filters').find('span').toggleClass('hide');
            $('.filter-wrapper-div').toggleClass('opened');
            $('.result-list').toggle('slow');
        });
    </script>
</div>

<?php // filters ?>
<div class="filter-wrapper">
    <div class="sejururi filter-wrapper-div">
        <?php
        if ($nr_hoteluri > 0) {
            include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/sejururi/filtru_ajax_responsive.php');
        }
        ?>

        <div class="scroll-to-filters">
            <button class="scroll-filter"
                    onclick="$('html, body').animate({scrollTop: jQuery('.filter-wrapper').offset().top - 10}, 'slow');">
                Arata filtre
            </button>
        </div>
        <script>
            $(document).on('scroll', function () {
                if ($(window).width() > 767 && $('.scroll-to-filters').offset().top < $(document).scrollTop()) {
                    $('.scroll-filter').show();
                } else {
                    $('.scroll-filter').hide();
                }
            });
        </script>
    </div>
</div>

<?php // resultes ?>
<div class="sejururi result-list">
    <?php $oferte->afiseaza(); ?>
    <div class="item-holder" style="display: none;">
        <?php $oferte->printPagini() ?>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.show-prices-button a', function () {
        var wrapper = $(this).closest('.item-holder');
        var hotel_id = $(this).attr('id').replace('vezipret', '');
        if (wrapper.find('.cautare_live').val() == 'da') {
            $('#hotel' + hotel_id + 'O').empty().html('<div align="center"><br><span class="bigger-11em bold black italic">Vă rugăm așteptați. Ofertele se incarca in timp real.</span><br><img src="/images/loader3.gif" alt="Loading" /></div>');
            setTimeout(function () {
                $('#hotel' + hotel_id + 'O').load(wrapper.find('.loader_link').val());
            }, 500);
        } else {
            $('#H' + hotel_id + 'O').empty().html('<br><span class="bigger-11em bold black italic">Vă rugăm așteptați. Incarcam ofertele</span><br><img src="/images/loader3.gif" alt="">');
            setTimeout(function () {
                $('#H' + hotel_id + 'O').load();
            }, 300);
        }
    });
</script>
<script>
    $(document).ready(function () {
        $('.result-list').jscroll({
            loadingHtml: '<img src="/images/loader3.gif" alt="Loading" /> Se incarca',
            padding: 20,
            debug: true,
            nextSelector: 'a.jscroll-next',
            contentSelector: '.item-holder'
        });
    });
</script>
<div class="clear"></div>

<?php // the most important from the current zone ?>
<div class="sejururi">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/localitati_zona_sus_responsive.php"); ?>
</div>
<?php //echo '<pre>';print_r($localitate_detalii);echo  '</pre>'; ?>

<?php if (strlen($localitate_detalii['imagine1']) > 5 and strlen($localitate_detalii['descriere_localitate']) > 20) { ?>
    <div class="sejururi">
        <h1 class="main-title">
            Mai multe informatii despre <?php echo $localitate_detalii['denumire_localitate'] ?>
        </h1>
        <article class="pad10 article bigger-12em text-justify">
            <img src="/img_localitate/<?php echo $localitate_detalii['imagine1']; ?>"
                 alt="<?php echo $localitate_detalii['denumire_localitate'] ?>" class="float-left" style="margin:0 20px 0 0;">
            <?php ($descriere_localitate = $localitate_detalii['descriere_localitate']);
            $descriere_localitate = str_replace(
                '{imagine2}',
                '<img src="/img_localitate/' . $localitate_detalii['imagine2'] . '" alt="'
                . $localitate_detalii['denumire_localitate'] . '" class="float-left" style="margin:3px 10px 0 0;">',
                $descriere_localitate
            );

            $descriere_localitate = str_replace(
                '{imagine3}',
                '<img src="/img_localitate/' . $localitate_detalii['imagine3'] . '" alt="'
                . $localitate_detalii['denumire_localitate'] . '" class="float-left" style="margin:3px 10px 0 0;">',
                $descriere_localitate
            );
            echo $descriere_localitate;
            ?>
        </article>
        <div class="clear"></div>
        <script src="/js/readmore.min.js"></script>
        <script>$('article').readmore({
                collapsedHeight: 200,
                heightMargin: 16,
                speed: 200,
                moreLink: '<a href="#" class="morelink titlu blue" style="text-align: right; float: right;margin: 15px 0;"> Citeste mai mult</a>',
                lessLink: '<a href="#" style="text-align: right; float: right;" class="titlu blue">Inchide</a>',
                embedCSS: true
            });
        </script>
    </div>
<?php } ?>
