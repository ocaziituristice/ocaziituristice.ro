<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');

$id_oferta=$_GET['oferta'];
$GLOBALS['make_vizualizata']='nu';
$det= new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
if($_GET['data_start']) {
	$preturi=$det->select_preturi_sejur($id_oferta, $_GET['data_start'], '');
} else $preturi=$det->select_preturi_sejur($id_oferta, '', '');
$st=explode('-', $preturi['min_start']);
$en=explode('-', $preturi['max_end']); ?>
<script type="text/javascript">
$(function() {
	$("#data_start").datepicker({
		numberOfMonths: 3,
		dateFormat: "yy-mm-dd",
		altField: "#data_end",
		showButtonPanel: true,
		minDate: new Date(<?php echo $st[0].', '.$st[1].' - 1, '.$st[2]; ?>),
		maxDate: new Date(<?php echo $en[0].', '.$en[1].' - 1, '.$en[2]; ?>)
	});
});
</script>
<?php if($preturi['camera']>0) { ?>
  <table class="tabel-preturi" style="width:100%;">
    <thead>
      <tr>
        <th class="text-center" style="width:150px;"><?php if($detalii['tip_preturi']=='plecari') echo 'Data plecarii'; else echo 'Perioade'; ?></th>
		<?php $g=0;
		foreach($preturi['camera'] as $key_cam=>$value_cam) {
			$g++;
			if(sizeof($preturi['camera'])==$g) {
				$class='text-center last';
				$g=0;
			} else $class='text-center'; ?>
        <th class="<?php echo $class; ?>"><?php echo $value_cam; ?></th>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
    <?php $nr_t=0;
	foreach($preturi['data_start'] as $key_d=>$data_start) {
		if((!$_GET['data_start'] && ($nr_t<=10 || $_GET['toate']=='da')) || $_GET['data_start']) {
			if($nr_t%2==1) $class_tr='impar'; else $class_tr='par';
			$numar_minim_nopti='';
			if(sizeof($detalii['data_start_nr_min'])>'0') {
				foreach($detalii['data_start_nr_min'] as $key_nm=>$value_nm) {
					if($value_nm<=$preturi['data_start_normal'][$key_d]) {
						if($preturi['data_end_normal'][$key_d] && $preturi['data_end_normal'][$key_d]<>'0000-00-00') {
							if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_end_normal'][$key_d]) {
								$numar_minim_nopti = 'Numar minim nopti: '.$detalii['nr_min'][$key_nm];
							}
						} else {
							if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_start_normal'][$key_d]) {
								$numar_minim_nopti = 'Numar minim nopti: '.$detalii['nr_min'][$key_nm];
							}
						}
					}
				}
			}
			$stopsales[$key_d] = 0;
			foreach($preturi['camera'] as $key_cam_ss=>$value_cam_ss) {
				if(soldout($id_oferta, $key_cam_ss, $data_start)>0) $stopsales[$key_d] += 1;
			}
			if($stopsales[$key_d]==0) {
				$nr_t++;
				?>
      <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
        <th nowrap="nowrap" <?php if(strlen($numar_minim_nopti)>4) echo 'class="vtip" title="'.$numar_minim_nopti.'"'; ?>><?php if(sizeof($preturi['include_tip'][$key_d])>0) foreach($preturi['include_tip'][$key_d] as $key_inc=>$include) echo '<a href="'.$sitepath.$_REQUEST['tara'].'/'.$_REQUEST['localitate'].'/'.fa_link($include['denumire']).'-'.$include['id_oferta'].'.html">!</a>'; echo $data_start; if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') echo ' - '.$preturi['data_end'][$key_d]; ?><br />
        <?php /*if(sizeof($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]])>0) { ?><div class="red"><a onmousedown="if(document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display == 'none'){ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'block'; }else{ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'none'; }" style="cursor:pointer">toate preturile</a></div><?php }*/ ?>
        </th>
         <?php $g=0; foreach($preturi['camera'] as $key_cam=>$value_cam) { $g++;
		 if(sizeof($preturi['camera'])==$g) { $class='pret last'; $g=0; } else $class='pret';
		 if(sizeof($preturi['camera'])=="1") $wdt='80%'; elseif(sizeof($preturi['camera'])=="2") $wdt='40%'; elseif(sizeof($preturi['camera'])=="3") $wdt='27%'; elseif(sizeof($preturi['camera'])=="4") $wdt='20%'; elseif(sizeof($preturi['camera'])=="5") $wdt='16%'; elseif(sizeof($preturi['camera'])=="6") $wdt='13%';
		 ?>
        <td align="center" valign="middle" width="<?php echo $wdt; ?>" <?php if(strlen($numar_minim_nopti)>4) echo 'class="'.$class.' vtip" title="'.$numar_minim_nopti.'"'; else echo 'class="'.$class.'"'; ?>>
		<?php
        if($preturi['pret'][$key_d][$key_cam]) {
			$pret_afisare = new_price($preturi['pret'][$key_d][$key_cam]).' ';
			if($preturi['moneda'][$key_d][$key_cam]=='EURO') {
				$moneda_afisare = '&euro;'; $valuta='da';
			} elseif($preturi['moneda'][$key_d][$key_cam]=='USD') {
				$moneda_afisare = '$'; $valuta='da';
			} else $moneda_afisare = $preturi['moneda'][$key_d][$key_cam];
			
			/*if(soldout($id_oferta, $key_cam, $data_start)>0) {
				echo '<span class="soldout2 red" title="Aceasta perioada nu este disponibila. Camera se afla in STOP SALES.">STOP SALES<span class="pretz">'.$pret_afisare.$moneda_afisare.'</span></span>';
			} else {
				echo $pret_afisare.$moneda_afisare;
			}*/
			echo $pret_afisare.$moneda_afisare;
			
		} else echo '-';
		?>
        </td>
        <?php } ?>
      </tr>
      <?php } ?>
      <?php /*if(sizeof($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]])>0) { ?>
      <tr>
        <td align="left" valign="middle" colspan="5" class="last" style="padding:0;"><div id="alte_preturi<?php echo $key_d; ?>" style="display:none; padding-left:164px; background:#d2f6c5;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabel-preturi" style="margin:0;">
          <tbody>
          <tr>
			<?php $sup_i=0; if(strlen($numar_minim_nopti)>4) $class_nr='vtip'; else $class_nr=''; foreach($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]] as $key_sup=>$value_sup) { $sup_i++; if($sup_i==sizeof($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]])) $class_sup='last'; else $class_sup='';  ?>
            <th align="center" valign="middle" class="<?php echo $class_sup; if($class_nr) echo ' '.$class_nr; ?>" <?php if(strlen($numar_minim_nopti)>4) echo ' title="'.$numar_minim_nopti.'"'; ?>><?php echo $key_sup; ?></th>
			<?php } ?>
          </tr>
          <tr>
            <?php $sup_i=0; foreach($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]] as $key_sup=>$value_sup) { $sup_i++; if($sup_i==sizeof($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]])) $class_sup='last'; else $class_sup=''; ?>
            <td align="center" valign="middle" class="pret <?php echo $class_sup; if($class_nr) echo ' '.$class_nr; ?>" <?php if(strlen($numar_minim_nopti)>4) echo ' title="'.$numar_minim_nopti.'"'; ?>><?php if($value_sup['pret']) { echo $value_sup['pret'].' '; if($value_sup['moneda']=='EURO') echo '&euro;'; elseif($value_sup['moneda']=='USD') echo '$'; else echo  $value_sup['moneda']; } ?></td>
			<?php } ?>
          </tr>
          </tbody>
        </table>
        </div></td>
      </tr>
		<?php }*/
	    }
      } ?>
      </tbody>
    </table>
<?php } else echo '<div style="font-size:1.15em;"><strong class="blue"><em>Nu s-a gasit nici un pret la acest hotel dupa cautarea dumneavoastra!</em></strong></div>'; ?>
<?php if($_GET['toate']<>'da') { ?><a href="javascript: ;" onClick="cautare('','da');ga('send', 'event', 'pagina oferta', 'vezi toate preturile', '<?php echo $detalii['denumire']; ?>');" class="buton-grey"><span class="white">Vezi toate preturile si perioadele ofertei</span></a><?php } ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabel-preturi">
<tbody>
  <tr class="impar">
    <th align="left" valign="middle" nowrap="nowrap">
      <input type="text" name="data_start" id="data_start" value="<?php if($_GET['data_start']) echo $_GET['data_start']; ?>" class="input-search NEW-round6px" style="width:120px;" />
      <input type="button" value="Arata datele de plecare" id="cauta" onClick="cautare(document.getElementById('data_start').value, 'nu');ga('send', 'event', 'pagina oferta', 'cauta data de plecare', '<?php echo $detalii['denumire']; ?>');" class="btn-black NEW-round6px" />
    </th>
    <th align="left" valign="middle" class="info-small last">
      * introduceti in campul alaturat o data aproximativa de plecare si va vom afisa toate perioadele disponibile cu 5 zile in plus si in minus.
    </th>
  </tr>
</tbody>
</table>

<?php /*?><?php
foreach($preturi['camera'] as $key_cam=>$value_cam) {
	$sel_detcam = "SELECT detalii FROM camere_hotel WHERE id_camera='$key_cam' AND id_hotel='".$detalii['id_hotel']."'";
	$que_detcam = mysql_query($sel_detcam) or die(mysql_error());
	$row_detcam = mysql_fetch_array($que_detcam);
	if($row_detcam['detalii']) {
		echo '<div class="bigger-13em bold black">'.$value_cam.'</div>';
		echo '<div style="padding-left:15px; text-align:justify">'.nl2br($row_detcam['detalii']).'</div><br />';
	}
}
?>

<?php
$sel_stopsales = "SELECT sold_out.camera, tip_camera.denumire FROM sold_out INNER JOIN tip_camera ON tip_camera.id_camera=sold_out.camera WHERE id_oferta='".$id_oferta."' GROUP BY tip_camera.denumire";
$que_stopsales = mysql_query($sel_stopsales) or die(mysql_error());
if(mysql_num_rows($que_stopsales)>0) {
?>
<p style="text-indent:0;"><strong class="blue">SS</strong> = <strong>STOP SALES</strong>. Perioadele de STOP SALES pentru aceasta oferta sunt:<br />
<?php
	while($row_stopsales = mysql_fetch_array($que_stopsales)) {
		echo '<strong>'.$row_stopsales['denumire'].':</strong> &nbsp; ';
		$sel_stopsales2 = "SELECT data_start AS soldout_start, data_end AS soldout_end FROM sold_out WHERE id_oferta='".$id_oferta."' AND camera='".$row_stopsales['camera']."'";
		$que_stopsales2 = mysql_query($sel_stopsales2) or die(mysql_error());
		while($row_stopsales2 = mysql_fetch_array($que_stopsales2)) {
			echo date('d/m/Y',strtotime($row_stopsales2['soldout_start'])).' - '.date('d/m/Y',strtotime($row_stopsales2['soldout_end'])).'; &nbsp; ';
		}
		echo '<br />';
	} 
} ?>
</p><?php */?>
