<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/config/recaptchalib.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/send_mail.php');
$id_oferta=$_REQUEST['oferta'];
$id_hotel=$_REQUEST['hotel'];
$id_circuit=$_REQUEST['circuit'];
$luna=array(1=>'Ianuarie', 2=>'Februarie', 3=>'Martie', 4=>'Aprilie', 5=>'Mai', 6=>'Iunie', 7=>'Iulie', 8=>'August', 9=>'Septembrie', 10=>'Octombrie', 11=>'Noiembrie', 12=>'Decembrie');
if($id_hotel) {
$selH="select hoteluri.nume, hoteluri.stele, tari.denumire as tara, localitati.denumire as localitate, continente.nume_continent"; if($id_oferta || $id_circuit) $selH=$selH.", oferte.denumire, transport.denumire as denumire_transport"; $selH=$selH." from hoteluri inner join localitati on hoteluri.locatie_id = localitati.id_localitate inner join zone on localitati.id_zona = zone.id_zona inner join tari on zone.id_tara = tari.id_tara "; if($id_oferta) $selH=$selH." inner join oferte on (hoteluri.id_hotel = oferte.id_hotel and oferte.id_oferta = '".$id_oferta."') inner join transport on oferte.id_transport = transport.id_trans "; $selH=$selH." left join continente on hoteluri.id_continent = continente.id_continent where hoteluri.id_hotel = '".$id_hotel."' ";
$queH=mysql_query($selH) or die(mysql_error());
$rowH=mysql_fetch_array($queH);
@mysql_free_result($queH);
}
if($_POST['trimite']) {
$err=0;
$err_nume='';
if(strlen(trim($_POST['nume']))<3) { ++$err; $err_nume='Campul nume este obligatoriu!'; }
if(strlen(trim($_POST['prenume']))<3) { ++$err; $err_prenume='Campul prenume este obligatoriu!'; }
if(strlen(trim($_POST['telefon']))<4) { ++$err; $err_tel='Campul telefon este obligatoriu!'; }
if(strlen(trim($_POST['email']))<5) { ++$err; $err_email='Campul E-mail este obligatoriu!'; }
elseif(validate_email($_POST['email'])==false) { ++$err; $err_email='Adresa de email completata nu este corecta!'; }
 if(!$err) {
  $nume=$_POST['nume'];
  $prenume=$_POST['prenume'];
  $email=$_POST['email'];
  if(sizeof($_POST['interese'])>0) {
   foreach($_POST['interese'] as $key=>$value) $interese=$interese.ucwords($value).', ';
  }
  $interese=substr($interese,0,-2);
  $data_nasterii=$_POST['an'].'-'.$_POST['luna'].'-'.$_POST['ziua'];
  $data_nasterii_afisare=$_POST['ziua'].' '.$luna[$_POST['luna']].' '.$_POST['an'];
  $ins="insert into cerere_detalii set id_hotel = '".$id_hotel."', id_oferta = '".$id_oferta."', id_circuit = '".$id_circuit."', sex = '".$_POST['sex']."', nume = '".$nume."', prenume = '".$prenume."', telefon = '".$_POST['telefon']."', email = '".$email."', observatii = '".$_POST['observatii']."', interese = '".$interese."', data_adaugarii = SYSDATE(), ip = '".$_SERVER['REMOTE_ADDR']."', data_nasterii = '".$data_nasterii."', data_nasterii_afisare = '".$data_nasterii_afisare."' ";
  if($que=mysql_query($ins) or die(mysql_error())) {
	if($_POST['sex']=='m') $tit='Domnul '; else $tit='Doamna ';
	$GLOBALS['nume_expeditor2']=$prenume.' '.$nume;
	$GLOBALS['nume_expeditor']=$tit.$nume_expeditor2;
	$GLOBALS['mail_expeditor']=$email;
	$GLOBALS['telefon_expeditor']=$_POST['telefon'];
	$GLOBALS['data_nasterii_afisare']=$data_nasterii_afisare;
	$GLOBALS['observatii']=$_POST['observatii'];
	if($id_hotel && !$id_circuit) {
	$GLOBALS['hotel']=$rowH['nume'];
	$GLOBALS['numar_stele']='- '.$rowH['stele'].' stele';
	$GLOBALS['localizare']=$rowH['tara'].' / '.$rowH['localitate'];
	
	if($id_oferta) $GLOBALS['link']=$sitepath.fa_link($rowH['tara']).'/'.fa_link($rowH['localitate']).'/'.fa_link($rowH['denumire']).'-'.$id_oferta.'.html'; 
	else $GLOBALS['link']=$sitepath.'hoteluri/'.fa_link($rowH['tara']).'/'.fa_link($rowH['localitate']).'/'.fa_link($rowH['nume']).'-'.$id_hotel.'.html'; }
	elseif($id_circuit) {
	$GLOBALS['hotel']=$rowH['nume'];
	$GLOBALS['numar_stele']='';
	$GLOBALS['localizare']='Continent: '.$rowH['nume_continent'];
	$GLOBALS['link']=$sitepath.'circuit/'/*.fa_link($rowH['nume_continent']).'/'*/.fa_link($rowH['nume']).'-'.$id_circuit.'.html';
  }
	$GLOBALS['sigla']=$GLOBALS['path_sigla'];
	//$GLOBALS['email_contact']='daniel@ocaziituristice.ro';
	if($interese) { $GLOBALS['interese_exp']=$interese; } else { $GLOBALS['interese_exp']='-'; }
	if($observatii) { $GLOBALS['observatii']=$_POST['observatii']; } else { $GLOBALS['observatii']='-'; }
	
	//email_agentie
	$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/cere_detalii.htm';
    $param['subject']='Cerere mai multe detalii '.$rowH['nume'];
    $param['to_email']=$GLOBALS['email_rezervare'];
    $param['to_nume']=$GLOBALS['denumire_agentie'];
    $param['fr_email']=$email;
    $param['fr_nume']=$nume_expeditor2;
	$send_m= new SEND_EMAIL;
	$send_m->send($param);
	
	//raspuns_client
	$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/raspuns_expeditor.htm';
	$param['subject']='Cerere mai multe detalii '.$rowH['nume'].' - '.$email_sitepath;
	$param['to_email']=$email;
	$param['to_nume']=$nume_expeditor2;
	$param['fr_email']=$GLOBALS['email_rezervare'];
	$param['fr_nume']=$GLOBALS['denumire_agentie'];
	$send_m->send($param);
	
	unset($send_m);
  }
 }
} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Cere mai multe dealii oferta <?php if($id_oferta || $id_circuit) echo $detalii['denumire']; else echo $detalii_hotel['denumire']; ?></title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>
<body>
<h2 class="green">Cere mai multe detalii <?php if($id_oferta || $id_circuit) echo $rowH['denumire']; else echo $rowH['nume']; ?></h2>
<div>
<?php if($id_oferta) { ?><h3 class="blue"><?php echo $rowH['nume']; ?> <img src="<?php echo $imgpath; ?>/spacer.gif" class="stele-mici-<?php echo $rowH['stele']; ?>" alt="" /></h3>
<?php echo '&nbsp;&nbsp; <strong>Transport:</strong> '.$rowH['denumire_transport'].' &nbsp;&nbsp; | &nbsp;&nbsp; ';
}
if($id_hotel) { ?>
<?php echo $rowH['localitate']; ?>, <?php echo $rowH['tara']; } else echo '&nbsp;&nbsp; <strong>Transport:</strong> '.$rowH['denumire_transport'].' &nbsp;&nbsp; | &nbsp;&nbsp; <strong>Continent:</strong> '.$rowH['nume_continent']; ?>
</div>
<div class="cerere-detalii NEW-round8px clearfix" style="width:540px;">
<?php if(!$_POST['trimite'] || $err>0) { ?>
<form action="" method="post" name="contactForm">
<div class="item clearfix">
  <div class="left">&nbsp;</div>
  <div class="right"><label><input type="radio" name="sex" value="m" style="width:10px" <?php if(!$_POST['sex'] || $_POST['sex']=='m') { ?> checked="checked" <?php } ?>/>Domnul</label>&nbsp;&nbsp;&nbsp;<label><input type="radio" name="sex" value="f" style="width:10px" <?php if($_POST['sex']=='f') { ?> checked="checked" <?php } ?> />Doamna</label></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="nume" class="titlu">* Nume</label></div>
  <div class="right"><input name="nume" id="nume" type="text" value="<?php if($_POST['nume']) echo $_POST['nume']; ?>" class="big" />
  <?php if($err_nume) { ?><label class="error"><?php echo $err_nume; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="prenume" class="titlu">* Prenume</label></div>
  <div class="right"><input name="prenume" id="prenume" type="text" value="<?php if($_POST['prenume']) echo $_POST['prenume']; ?>" class="big" />
  <?php if($err_prenume) { ?><label class="error"><?php echo $err_prenume; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="email" class="titlu">* E-mail</label></div>
  <div class="right"><input name="email" id="email" type="text" value="<?php if($_POST['email']) echo $_POST['email']; ?>" class="big" />
  <?php if($err_email) { ?><label class="error"><?php echo $err_email; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="telefon" class="titlu">* Telefon</label></div>
  <div class="right"><input name="telefon" id="telefon" type="text" value="<?php if($_POST['telefon']) echo $_POST['telefon']; ?>" class="big" />
  <?php if($err_tel) { ?><label class="error"><?php echo $err_tel; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label class="titlu">Data nasterii</label></div>
  <div class="right" style="width:330px;">
    &nbsp;&nbsp; Zi <select name="ziua"><?php for($i=1;$i<=31;$i++) { echo "<option value='$i'"; if($_POST['ziua']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
    Luna <?php ?> <select name="luna"><?php for($i=1;$i<=12;$i++) { echo "<option value='$i'"; if($_POST['luna']==$i) echo "selected='selected'"; echo ">".$luna[$i]."</option>"; } ?></select>
    An <?php ?> <select name="an"><?php for($i=date("Y");$i>=1900;$i--) { echo "<option value='$i'"; if($_POST['an']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
  </div>
</div>
<div class="item clearfix">
  <div class="left"><label class="titlu">Mai multe detalii pentru:</label></div>
  <div class="right">
    <?php if($id_circuit) { ?><label class="check"><input type="checkbox" name="interese[1]" value="program circuit" <?php if($_POST['interese'][1]=='program circuit') { ?> checked="checked" <?php } ?>/>Program circuit</label>
	<?php } else { ?>
    <label class="check"><input type="checkbox" name="interese[1]" value="hotel" <?php if($_POST['interese'][1]=='hotel') { ?> checked="checked" <?php } ?>/>Hotel</label>
	<?php } ?>
    <label class="check"><input type="checkbox" name="interese[2]" value="preturi" <?php if($_POST['interese'][1]=='preturi') { ?> checked="checked" <?php } ?> />Preturi</label>
    <label class="check"><input type="checkbox" name="interese[3]" value="transport" <?php if($_POST['interese'][1]=='transport') { ?> checked="checked" <?php } ?> />Transport</label>
  </div>
</div>
<div class="item clearfix">
  <div class="left"><label for="observatii" class="titlu">Observatii</label></div>
  <div class="right"><textarea name="observatii" id="observatii" class="big"><?php if($_POST['observatii']) echo $_POST['observatii']; ?></textarea></div>
</div>
<div class="item clearfix">
  <div class="left"><input name="trimite" type="hidden" value="trimite" /></div>
  <div class="right"><input type="image" src="<?php echo $imgpath; ?>/buton_trimite.gif" value="Trimite" class="send" /></div>
</div>
<!-- Google Code for incercare_cere_detalii Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1012260793;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "T9ttCJeBnAIQub_X4gM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1012260793/?label=T9ttCJeBnAIQub_X4gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</form>

<?php } else { ?>
Cererea dumneavoastra a fost trimisa catre Echipa OcaziiTuristice.ro<br />
In cel mai scurt timp veti fi contactat in legatura cu cererea trimisa.
<br /><br />
Va multumim,<br />
Echipa OcaziiTuristice.ro
<!-- Google Code for cere_detalii Conversion Page
In your html page, add the snippet and call
goog_report_conversion when someone clicks on the
phone number link or button. -->
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 1012260793;
    w.google_conversion_label = "RyI2CJ-AnAIQub_X4gM";
    w.google_conversion_value = 0;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="http://www.googleadservices.com/pagead/conversion_async.js">
</script>

<?php } ?>  
</div>
</body>
</html>