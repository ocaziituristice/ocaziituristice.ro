<script type="text/javascript">
function validateForm() {
	var err='';
	if(document.forms["calculeaza"]["plecdata"].value=='') err=err+'Va rugam selectati data de plecare!\n';
	if(document.forms["calculeaza"]["pleczile"].value=='') err=err+'Va rugam selectati numarul de nopti!\n';
	/*if(document.forms["calculeaza"]["tipmasa"].value=='') err=err+'Va rugam selectati tipul de masa!\n';*/
	if(document.forms["calculeaza"]["adulti"].value=='') err=err+'Va rugam selectati numarul de adulti!\n';
	if(err!='') {
		alert(err);
		return false;
	}
}

function displayAges(nVal) {
	if(nVal>0) {
		document.getElementById("chd-ages").style.display = "block";
	} else {
		document.getElementById("chd-ages").style.display = "none";
	}
	
	for(var i=0; i<3; i++) {
		if(i<nVal) {
			document.getElementById("varste-copii-"+i).style.display = "inline-block";
		} else {
			document.getElementById("varste-copii-"+i).style.display = "none";
			document.getElementById("varste-copii-"+i).value = 0;
		}
	}
}
</script>
  
<?php
$afisare_servicii = 'da';

$preturi_pivot=$det->select_preturi_pivot($id_hotel, $detalii['id_oferta_pivot'], $detalii['nr_formula_pivot']);
if($detalii['nr_min'][0]) {
	$nr_minim_nopti = $detalii['nr_min'][0];
} else {
	$nr_minim_nopti = '2';
}

$minDate_1 = explode('-',$preturi['min_start']);
$maxDate_1 = explode('-',$preturi['max_end']);
$minDate = $minDate_1[0].', '.$minDate_1[1].' - 1, '.$minDate_1[2];
$maxDate = $maxDate_1[0].', '.$maxDate_1[1].' - 1, '.$maxDate_1[2];

$var_dates = '';
$var_dates_if = '';
if(sizeof($preturi['data_start_normal'])>0) {
	foreach($preturi['data_start_normal'] as $key_ds => $value_ds) {
		$ds[$key_ds] = explode('-', $value_ds);
		$de[$key_ds] = explode('-', $preturi['data_end_normal'][$key_ds]);
		if($detalii['nr_nopti']>1) {
			//$data_end_interval = $de[$key_ds][2] - $detalii['nr_nopti'];
			$data_end_interval = $de[$key_ds][2];
		} else {
			$data_end_interval = $de[$key_ds][2];
		}
		$var_dates .= 'ds_'.$key_ds.' = new Date('.$ds[$key_ds][0].', '.$ds[$key_ds][1].' - 1, '.$ds[$key_ds][2].'), de_'.$key_ds.' = new Date('.$de[$key_ds][0].', '.$de[$key_ds][1].' - 1, '.$data_end_interval.'), ';
		
		$var_dates_if .= '(date >= ds_'.$key_ds.' && date <= de_'.$key_ds.')||';
	}
	$var_dates = substr($var_dates,0,-2);
	$var_dates_if = substr($var_dates_if,0,-2);
}

if($_REQUEST['plecdata']) {
	$pleczile = $_REQUEST['pleczile'];
	$plecnopti = $pleczile-1;
	$plecdata = $_REQUEST['plecdata'];
	$arivdata = date('Y-m-d', strtotime($plecdata.' + '.$plecnopti.' days'));
	//$tipmasa = $_REQUEST['tipmasa'];
	$adulti = $_REQUEST['adulti'];
	$copii = $_REQUEST['copii'];
	$orasplec = $_REQUEST['orasplec'];
	if($orasplec!='') setcookie('transportAutocar', fa_link(get_den_localitate($orasplec)), time()+2592000, '/', 'www.ocaziituristice.ro');
}

$editFormAction = $_SERVER['REQUEST_URI'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
?>

<h3 class="HrezNow red bold" id="calc"><?php if($detalii_hotel['tip_unitate']=='Circuit') echo 'Calculati pretul pentru'; else echo 'Calculati pretul pentru'; ?> <?php echo $detalii_hotel['denumire']; ?></h3>
<div class="NEW-calculeaza" style="border:1px solid #c5dbec; margin-top:-1px;">
<?php /*?><?php
//XML PRICES
if($detalii['online_prices']=='da') {
?>
<form name="calculeaza" id="Calculeaza" onsubmit="return submitForm();" >
    <input type="hidden" name="id_oferta" value="<?php echo $id_oferta; ?>">
<?php
//XML PRICES
} else { ?><?php */?>
<form action="<?php echo $editFormAction; ?>#calc" method="get" name="calculeaza" id="Calculeaza" onsubmit="return validateForm()" >
<?php /*?><?php } ?><?php */?>

  <?php if(sizeof($autocar)>0) { ?>
  <div class="item" style="width:160px;">
    <label for="calculeaza-orasplec">Oras plecare</label>
    <select name="orasplec" id="calculeaza-orasplec" class="plec NEW-round4px">
    <?php if(!$orasplec or !$_COOKIE['transportAutocar']) echo '<option value="" selected>- Selecteaza orasul -</option>'; ?>
      <?php foreach($autocar as $key_a=>$value_a) { ?>
      <option value="<?php echo get_id_localitate($value_a['denumire_localitate']); ?>" <?php if($orasplec==get_id_localitate($value_a['denumire_localitate']) or $_COOKIE['transportAutocar']==fa_link($value_a['denumire_localitate'])) echo 'selected'; ?>><?php echo $value_a['denumire_localitate']; ?></option>
      <?php } ?>
   </select>
  </div>
  <?php } ?>
  
  <?php if($detalii['tip_preturi']=='perioade') { ?>

  <div class="item" style="width:95px;">
    <label for="calculeaza-dataplec">Data plecarii</label>
    <input type="text" name="plecdata" id="calculeaza-dataplec" value="<?php if($plecdata) echo $plecdata; ?>" class="dataper NEW-round4px" />
  </div>
  <div class="item" style="width:65px;">
    <label for="calculeaza-nr-nopti">Nr. nopti</label>
    <select name="pleczile" id="calculeaza-nr-nopti" class="per NEW-round4px">
      <?php if(!$pleczile) echo '<option value="" selected disabled>--</option>'; ?>
      <?php if($detalii['nr_nopti']>1) { ?>
      <option value="<?php echo $detalii['nr_nopti']; ?>" selected><?php echo $detalii['nr_nopti']; ?></option>
      <?php } else { ?>
	  <?php for($i=$nr_minim_nopti; $i<=15; $i++) { ?>
      <option value="<?php echo $i; ?>" <?php if($pleczile==$i) echo 'selected'; ?>><?php echo $i; ?></option>
      <?php } ?>
      <?php } ?>
    </select>
  </div>

  <?php } elseif($detalii['tip_preturi']=='plecari') { ?>

  <div class="item" style="width:160px;">
    <label for="calculeaza-dataplec">Data plecarii</label>
    <select name="plecdata" id="calculeaza-dataplec" class="plec NEW-round4px">
      <?php //if(!$plecdata) echo '<option value="" selected>- Selecteaza data -</option>'; ?>
      <?php foreach($preturi['data_start'] as $key_d=>$data_start) {
		  foreach($preturi['camera'] as $key_cam_ss=>$value_cam_ss) {
			  if(soldout($id_oferta, $key_cam_ss, $data_start)>0) $stopsales[$key_d] += 1;
		  }
		  if($stopsales[$key_d]==0) {
	  ?>
      <option value="<?php echo $preturi['data_start_normal'][$key_d]; ?>" <?php if($plecdata==$preturi['data_start_normal'][$key_d]) echo 'selected'; ?>><?php echo $data_start.' - '.denZileRo(date('l',strtotime(denLuniEn($data_start)))); ?></option>
      <?php }
	  } ?>
    </select>
    <input type="hidden" name="pleczile" value="<?php echo $detalii['nr_nopti']; ?>" />
  </div>
    
  <?php } ?>
  
  <?php /*?><div class="item" style="width:160px;">
    <label for="calculeaza-tipmasa">Tip de masa</label>
    <select name="tipmasa" id="calculeaza-tipmasa" class="plec NEW-round4px">
	  <?php if(sizeof($preturi_pivot['tip_masa'])>1) { ?><option value="" selected disabled>--</option><?php } ?>
	  <?php foreach($preturi_pivot['tip_masa'] as $key_tip_masa=>$value_tip_masa) { ?>
      <option value="<?php echo $key_tip_masa; ?>" <?php if($tipmasa==$key_tip_masa) { ?>selected<?php } ?>><?php echo $value_tip_masa; ?></option>
      <?php } ?>
    </select>
  </div><?php */?>
  
 <?php /* <div class="item" style="width:160px;">
    <label for="calculeaza-tipmasa">Tip de masa</label>
    <select name="tipmasa" id="calculeaza-tipmasa" class="plec NEW-round4px">
      <option value="" selected><?php echo $detalii['masa']; ?></option>
    </select>
  </div>*/?>
  
  <div class="item" style="width:50px;">
    <label for="calculeaza-adulti">Adulti</label>
    <select name="adulti" id="calculeaza-adulti" class="number NEW-round4px">
      <?php if(!$adulti) echo '<option value="" disabled>0</option>'; ?>
	  <?php for($i=1; $i<=3; $i++) { ?>
      <option value="<?php echo $i; ?>" <?php if(!$adulti and $i==2) echo 'selected'; elseif($adulti==$i) echo 'selected'; ?>><?php echo $i; ?></option>
      <?php } ?>
    </select>
  </div>
  
  <div class="item" style="width:50px;">
    <label for="calculeaza-copii">Copii</label>
  <select name="copii" id="calculeaza-copii" class="number NEW-round4px" <?php if($detalii['online_prices']=='da') echo 'onChange="displayAges(this.value)"'; ?>>

  <?php /*    <select name="copii" id="calculeaza-copii" class="number NEW-round4px" <?php  echo 'onChange="displayAges(this.value)"'; ?>>*/ ?>
	  <?php for($j=0; $j<=3; $j++) { ?>
      <option value="<?php echo $j; ?>" <?php if($copii==$j) echo 'selected'; ?>><?php echo $j; ?></option>
      <?php } ?>
    </select>
  </div>
   <div class="item" id="chd-ages" style="width:150px; <?php if(($_REQUEST['age'][0]==0 and $copii==0) or $detalii['online_prices']!='da') echo 'display:none;'; ?>">
 <?php /*   <div class="item" id="chd-ages" style="width:150px; <?php if(($_REQUEST['age'][0]==0 and $copii==0)) echo 'display:none;'; ?>">*/?>
    <label>Varste copii</label>
    <?php for($t=0; $t<3; $t++) { ?>
    <select name="age[<?php echo $t; ?>]" id="varste-copii-<?php echo $t; ?>" class="number NEW-round4px" <?php if($_REQUEST['age'][$t]==0 and $copii<=$t) echo 'style="display:none;"'; ?>>
	  <?php for($j1[$t]=0; $j1[$t]<=17; $j1[$t]++) { ?>
      <option value="<?php echo $j1[$t]; ?>" <?php if($_REQUEST['age'][$t]==$j1[$t]) echo 'selected'; ?>><?php echo $j1[$t]; ?></option>
      <?php } ?>
    </select>
    <?php } ?>
  </div>

  <input type="image" src="/images/but_nou_verifica_disponibilitatea.png" class="float-right" style="padding:16px 0 0 0;" alt="Vezi pret" onclick="ga('send', 'event', 'pagina oferta', 'calculare pret NEW', '<?php echo $detalii['denumire']; ?>');" />
  
  <br class="clear" />
  
  <?php if(sizeof($detalii['early_time'])>0) { ?><div class="pad10 bold bigger-11em" style="margin-bottom:0; padding-bottom:0;">Tarifele de mai jos au inclusa reducerea Early Booking.</div><?php } ?>
  
<?php
//XML PRICES
if($detalii['online_prices']=='da') {
?>
<div id="preturiOnline"></div>
<?php /*?><script>
function submitForm() {
	$.ajax({
		type: 'POST',
		url: '/includes/preturi_online.php',
		data: $('#Calculeaza').serialize(),
		dataType: 'html',
		beforeSend:function(){
			$('#preturiOnline').html('<img src="/images/loader.gif" alt="" />');
		},
		success: function(response) {
			$('#preturiOnline').html(response);
		}
	});
	return false;
}
</script><?php */?>
<?php if(isset($_REQUEST['plecdata'])) { ?>
<script>
$("#preturiOnline").empty().html('<br><span class="bigger-13em bold">Verificăm preţurile şi disponibilitatea în timp real. Vă rugăm așteptați.</span><br><img src="/images/loader3.gif" alt="loading">');
setTimeout(function() { $("#preturiOnline").load("/includes/preturi_online.php?<?php echo 'id_oferta='.$id_oferta.'&plecdata='.$_REQUEST['plecdata'].'&pleczile='.$_REQUEST['pleczile'].'&adulti='.$_REQUEST['adulti'].'&copii='.$_REQUEST['copii'].'&copil1='.$_REQUEST['age'][0].'&copil2='.$_REQUEST['age'][1].'&copil3='.$_REQUEST['age'][2]; if($_REQUEST['orasplec']) echo '&orasplec='.$_REQUEST['orasplec']; ?>"); }, 10);
</script>
<?php } ?>
<?php
} else {
//XML PRICES
?>

<?php
if(isset($plecdata) and isset($pleczile) /*and isset($tipmasa)*/ and isset($adulti) and isset($copii)) {
	
	if($detalii['tip_preturi']=='perioade') {
		
		$sel_tip_camera="SELECT
		pret_pivot_adaugat.*,
		tip_camera.id_camera,
		tip_camera.denumire AS denumire_camera
		FROM pret_pivot_adaugat
		LEFT JOIN tip_camera ON tip_camera.id_camera=pret_pivot_adaugat.tip_camera
		WHERE pret_pivot='nu'
		AND id_hotel='".$id_hotel."'
		AND id_oferta='".$detalii['id_oferta_pivot']."'
		AND nr_formula='".$detalii['nr_formula_pivot']."'
		AND adulti='".$adulti."'
		AND copii='".$copii."'
		ORDER BY denumire_camera ASC, pret_pivot_adaugat.copil1 ASC, pret_pivot_adaugat.copil2 ASC, pret_pivot_adaugat.copil3 ASC ";
		$que_tip_camera=mysql_query($sel_tip_camera) or die(mysql_error());
		while($row_tip_camera=mysql_fetch_array($que_tip_camera)) {
			$intersection = getIntersection($row_tip_camera['data_start'],$row_tip_camera['data_end'],$plecdata,$arivdata);
			if($intersection === false) {
			} else {
				$inters_start = date('Y-m-d',$intersection['start']);
				$inters_end = date('Y-m-d',$intersection['end']);
								
				//if($_SESSION['mail']=='daniel@ocaziituristice.ro') { echo $inters_start.' '.$inters_end.' '.$plecdata.' '.$arivdata.'<br>'; }
				
				if($plecdata==$inters_start) {
					$rezcam1['id_pret'][] = $row_tip_camera['id_pret'];
					$rezcam1['data_start'][] = $inters_start;
					$rezcam1['data_end'][] = $inters_end;
					$rezcam1['id_camera'][] = $row_tip_camera['id_camera'];
					$rezcam1['denumire_camera'][] = $row_tip_camera['denumire_camera'];
					if($detalii['nr_nopti']>1) {
						$rezcam1['pret'][] = $row_tip_camera['pret'];
					} else {
						$rezcam1['pret'][] = dateDiff(date('Y-m-d',$intersection['end']),date('Y-m-d',$intersection['start'])) * $row_tip_camera['pret'];
					}
					$rezcam1['moneda'][] = $row_tip_camera['moneda'];
					$rezcam1['nr_adulti'][] = $row_tip_camera['adulti'];
					$rezcam1['nr_copii'][] = $row_tip_camera['copii'];
					$rezcam1['copil1'][] = $row_tip_camera['copil1'];
					$rezcam1['copil2'][] = $row_tip_camera['copil2'];
					$rezcam1['copil3'][] = $row_tip_camera['copil3'];
				}
				
				if(($arivdata==$inters_end) and ($plecdata<$inters_start)) {
					$rezcam2['id_pret'][] = $row_tip_camera['id_pret'];
					$rezcam2['data_start'][] = $inters_start;
					$rezcam2['data_end'][] = $inters_end;
					$rezcam2['id_camera'][] = $row_tip_camera['id_camera'];
					$rezcam2['denumire_camera'][] = $row_tip_camera['denumire_camera'];
					if($detalii['nr_nopti']>1) {
						$rezcam2['pret'][] = $row_tip_camera['pret'];
					} else {
						$rezcam2['pret'][] = dateDiff(date('Y-m-d',$intersection['end']),date('Y-m-d',$intersection['start'])) * $row_tip_camera['pret'];
					}
					$rezcam2['moneda'][] = $row_tip_camera['moneda'];
					$rezcam2['nr_adulti'][] = $row_tip_camera['adulti'];
					$rezcam2['nr_copii'][] = $row_tip_camera['copii'];
					$rezcam2['copil1'][] = $row_tip_camera['copil1'];
					$rezcam2['copil2'][] = $row_tip_camera['copil2'];
					$rezcam2['copil3'][] = $row_tip_camera['copil3'];
					$second_per = 'da';
				}
				
				if(($plecdata<$inters_start) and ($arivdata>$inters_end)) {
					$rezcam3['id_pret'][] = $row_tip_camera['id_pret'];
					$rezcam3['data_start'][] = $inters_start;
					$rezcam3['data_end'][] = $inters_end;
					$rezcam3['id_camera'][] = $row_tip_camera['id_camera'];
					$rezcam3['denumire_camera'][] = $row_tip_camera['denumire_camera'];
					if($detalii['nr_nopti']>1) {
						$rezcam3['pret'][] = $row_tip_camera['pret'];
					} else {
						$rezcam3['pret'][] = dateDiff(date('Y-m-d',$intersection['end']),date('Y-m-d',$intersection['start'])) * $row_tip_camera['pret'];
					}
					$rezcam3['moneda'][] = $row_tip_camera['moneda'];
					$rezcam3['nr_adulti'][] = $row_tip_camera['adulti'];
					$rezcam3['nr_copii'][] = $row_tip_camera['copii'];
					$rezcam3['copil1'][] = $row_tip_camera['copil1'];
					$rezcam3['copil2'][] = $row_tip_camera['copil2'];
					$rezcam3['copil3'][] = $row_tip_camera['copil3'];
					$third_per = 'da';
				}
				
			}
		}
		//if($_SESSION['mail']=='daniel@ocaziituristice.ro') { echo '<pre>';print_r($rezcam2);echo '</pre>'; }

		if(sizeof($rezcam1['id_pret'])>0) {
			$J=0;
			foreach($rezcam1['id_pret'] as $key_rezcam1 => $value_rezcam1) {
				$rezcam[$J]['id_pret'] = $rezcam1['id_pret'][$key_rezcam1];
				$rezcam[$J]['plecare'] = '0';
				$rezcam[$J]['data_start'] = $rezcam1['data_start'][$key_rezcam1];
				if($second_per=='da') {
					$rezcam[$J]['data_end'] = $rezcam2['data_end'][$key_rezcam1];
				} else {
					$rezcam[$J]['data_end'] = $rezcam1['data_end'][$key_rezcam1];
				}
				$rezcam[$J]['id_camera'] = $rezcam1['id_camera'][$key_rezcam1];
				$rezcam[$J]['denumire_camera'] = $rezcam1['denumire_camera'][$key_rezcam1];
				$rezcam[$J]['pret'] = $rezcam1['pret'][$key_rezcam1] + $rezcam2['pret'][$key_rezcam1] + $rezcam3['pret'][$key_rezcam1];
				$rezcam[$J]['moneda'] = $rezcam1['moneda'][$key_rezcam1];
				$rezcam[$J]['nr_adulti'] = $rezcam1['nr_adulti'][$key_rezcam1];
				$rezcam[$J]['nr_copii'] = $rezcam1['nr_copii'][$key_rezcam1];
				$rezcam[$J]['copil1'] = $rezcam1['copil1'][$key_rezcam1];
				$rezcam[$J]['copil2'] = $rezcam1['copil2'][$key_rezcam1];
				$rezcam[$J]['copil3'] = $rezcam1['copil3'][$key_rezcam1];
				$rezcam[$J]['period'] = date("d.m",strtotime($plecdata)).' - '.date("d.m.Y",strtotime($plecdata.' + '.$pleczile.' days'));
				$J++;
			}
		}
		
		/*if($_SERVER['REMOTE_ADDR']=='89.121.139.246') {
		
		echo '<pre>';print_r($rezcam);echo '</pre>';
		echo '<pre>';print_r($rezcam1);echo '</pre>';
		echo '<pre>';print_r($rezcam2);echo '</pre>';
		echo '<pre>';print_r($rezcam3);echo '</pre>';
		
		}*/
	
	} else if($detalii['tip_preturi']=='plecari') {
		
		$sel_tip_camera="SELECT 
		pret_pivot_adaugat.*,
		tip_camera.id_camera,
		tip_camera.denumire AS denumire_camera
		FROM pret_pivot_adaugat
		LEFT JOIN tip_camera ON tip_camera.id_camera=pret_pivot_adaugat.tip_camera
		WHERE pret_pivot='nu'
		AND id_hotel='".$id_hotel."'
		AND id_oferta='".$detalii['id_oferta_pivot']."'
		AND nr_formula='".$detalii['nr_formula_pivot']."'
		AND adulti='".$adulti."'
		AND copii='".$copii."'
		AND data_start='".$plecdata."'
		ORDER BY denumire_camera ASC, pret_pivot_adaugat.copil1 ASC, pret_pivot_adaugat.copil2 ASC, pret_pivot_adaugat.copil3 ASC ";
		$que_tip_camera=mysql_query($sel_tip_camera) or die(mysql_error());
		$J=0;
		while($row_tip_camera=mysql_fetch_array($que_tip_camera)) {
			$rezcam[$J]['id_pret'] = $row_tip_camera['id_pret'];
			$rezcam[$J]['plecare'] = '1';
			$rezcam[$J]['id_camera'] = $row_tip_camera['id_camera'];
			$rezcam[$J]['denumire_camera'] = $row_tip_camera['denumire_camera'];
			$rezcam[$J]['pret'] = $row_tip_camera['pret'];
			$rezcam[$J]['moneda'] = $row_tip_camera['moneda'];
			$rezcam[$J]['nr_adulti'] = $row_tip_camera['adulti'];
			$rezcam[$J]['nr_copii'] = $row_tip_camera['copii'];
			$rezcam[$J]['copil1'] = $row_tip_camera['copil1'];
			$rezcam[$J]['copil2'] = $row_tip_camera['copil2'];
			$rezcam[$J]['copil3'] = $row_tip_camera['copil3'];
			$rezcam[$J]['period'] = date("d.m",strtotime($plecdata)).' - '.date("d.m.Y",strtotime($plecdata.' + '.$pleczile.' days'));
			$J++;
		}
		
	}

	echo '<div class="camere bkg-green">';
//if($_SESSION['mail']=="daniel@ocaziituristice.ro") {echo '<pre>';print_r($rezcam);echo '</pre>'; }
	if(sizeof($rezcam)>0 and $arivdata<=$preturi['max_end']) {
		
		/*$rezcam = multid_sort($rezcam, 'pret');
		$rezcam = array_map('unserialize', array_unique(array_map('serialize', $rezcam)));*/
		$sort = array();
		foreach($rezcam as $key_sort => $value_sort) {
			$sort['disponibilitate'][$key_sort] = $value_sort['disponibilitate'];
			$sort['pret'][$key_sort] = $value_sort['pret'];
		}
		array_multisort($sort['disponibilitate'], SORT_ASC, $sort['pret'], SORT_ASC, $rezcam);
		
if($detalii_hotel['tip_unitate']=='Circuit') {
	$data_plecare_normal = $plecdata;
	$data_intoarcere_normal = $arivdata;
	$nr_adulti = $adulti;
	$nr_copii = $copii;
	$valoare_totala = 0;
	$service_type = array(
	'Servicii neincluse'=>'Taxe neincluse',
	'Suplimente'=>'Suplimente'
	);
	foreach($service_type as $key_service_type => $value_service_type) {
		$sel_serv_neincluse="SELECT * FROM oferte_servicii WHERE id_oferta='".$id_oferta."' AND tip_serv='".$key_service_type."' AND obligatoriu='da' AND value<>'' ORDER BY ordonare ASC ";
		$que_serv_neincluse=mysql_query($sel_serv_neincluse) or die(mysql_error());
		if(mysql_num_rows($que_serv_neincluse)>0) {
		//echo '<ul class="taxe-neincluse">';
		  while($row_serv_neincluse=mysql_fetch_array($que_serv_neincluse)) {
			  if($row_serv_neincluse['data_start']!=NULL and $row_serv_neincluse['data_end']!=NULL and $row_serv_neincluse['data_start']!='0000-00-00' and $row_serv_neincluse['data_end']!='0000-00-00') {
				  $intersect_services = getIntersection($row_serv_neincluse['data_start'], $row_serv_neincluse['data_end'], $data_plecare_normal, $data_intoarcere_normal);
				  if(sizeof($intersect_services['start'])>0) {
					  $includ_calcul = 'da';
				  } else {
					  $includ_calcul = 'nu';
				  }
			  } else if(($row_serv_neincluse['data_start']==NULL and $row_serv_neincluse['data_end']==NULL) or ($row_serv_neincluse['data_start']=='0000-00-00' and $row_serv_neincluse['data_end']=='0000-00-00')) {
				  $includ_calcul = 'da';
			  }
			  if($includ_calcul=='da') {
				  $value_pers = str_replace(',','.',$row_serv_neincluse['value']);
				  $numar_copil = '0';
				  $numar_infant = '0';
				  
				  $row_tip_camera = mysql_fetch_array($que_tip_camera);
				  
				  for($icopii=0; $icopii<$nr_copii; $icopii++) {
					  if(array_search($row_tip_camera['copil'.($icopii+1)],$infant)!==false) {
						  $numar_copil += '0';
						  $numar_infant += '1';
					  } else {
						  $numar_copil += '1';
						  $numar_infant += '0';
					  }
				  }
				  
				  if($row_serv_neincluse['pasager']=='/ persoana') {
					  $value_total = $value_pers * ($nr_adulti + $nr_copii - $numar_infant);
					  $nr_persoane = ($nr_adulti + $nr_copii - $numar_infant).' pers';
				  } else if($row_serv_neincluse['pasager']=='/ adult') {
					  $value_total = $value_pers * $nr_adulti;
					  $nr_persoane = $nr_adulti.' adult';
					  if($nr_adulti>1) $nr_persoane .= 'i';
				  } else if($row_serv_neincluse['pasager']=='/ copil') {
					  $value_total = $value_pers * $numar_copil;
					  $nr_persoane = $numar_copil.' copi';
					  if($numar_copil>1) $nr_persoane .= 'i'; else $nr_persoane .= 'l';
				  } else if($row_serv_neincluse['pasager']=='/ infant') {
					  $value_total = $value_pers * $numar_infant;
					  $nr_persoane = $numar_infant.' infant';
					  if($numar_infant>1) $nr_persoane .= 'i';
				  }
				  
				  if($row_serv_neincluse['exprimare']=='/ zi') {
					  $value_total = $value_total * $nr_nopti;
				  } else if($row_serv_neincluse['exprimare']=='/ sejur') {
					  $value_total = $value_total;
				  }
				  
				  $valoare_totala += $value_total;
				  
				  $number_pers = explode(' ',$nr_persoane);
				  if($number_pers[0]>0) {
					  $incl_serv['denumire'][] = $row_serv_neincluse['denumire'];
					  $incl_serv['value'][] = $value_pers;
					  $incl_serv['moneda'][] = moneda($row_serv_neincluse['moneda']);
					  $incl_serv['exprimare'][] = $row_serv_neincluse['exprimare'];
					  $incl_serv['nr_persoane'][] = $nr_persoane;
					  //echo '<li>'.$row_serv_neincluse['denumire'].' - <strong>'.$value_pers.' '.moneda($row_serv_neincluse['moneda']).' '.$row_serv_neincluse['exprimare'].' x '.$nr_persoane.'</strong></li>';
				  } //if($number_pers[0]>0)
			  } //if($includ_calcul=='da') 
			  $includ_calcul = 'nu';
		  } //while(
		  //echo '</ul>';
		}
	}
}			

		foreach($rezcam as $key_rezcam => $value_rezcam) {
			echo '<div class="row clearfix">';
			  echo '<div class="detalii bkg-white" style="width:315px;">';
				echo '<div class="ocupanti">';
				for($i1=0; $i1<$rezcam[$key_rezcam]['nr_adulti']; $i1++) {
					echo '<img src="/images/people_01_m.png" alt="adult">';
				}
				for($i2=0; $i2<$rezcam[$key_rezcam]['nr_copii']; $i2++) {
					$copii = array('0-2', '0-2.00', '0.00-2', '0.00-2.00', '0-1,99', '0.00-1,99', '0-1.99', '0.00-1.99', '0', '1');
					if(array_search($rezcam[$key_rezcam]['copil'.($i2+1)], $copii)!=false) {
						echo '<img src="/images/people_03_m.png" alt="infant">';
					} else {
						echo '<img src="/images/people_02_m.png" alt="copil">';
					}
				}
				echo '</div>';
			  echo '<div class="bigger-12em bold">'.$rezcam[$key_rezcam]['denumire_camera'].'</div>';
			  echo $rezcam[$key_rezcam]['nr_adulti'].' adl';
			  if($rezcam[$key_rezcam]['copil1']) echo ' + copil 1 (<strong>'.$rezcam[$key_rezcam]['copil1'].'</strong>)';
			  if($rezcam[$key_rezcam]['copil2']) echo ' + copil 2 (<strong>'.$rezcam[$key_rezcam]['copil2'].'</strong>)';
			  if($rezcam[$key_rezcam]['copil3']) echo ' + copil 3 (<strong>'.$rezcam[$key_rezcam]['copil3'].'</strong>)';
			  if(strlen($rezcam[$key_rezcam]['period'])>0) echo '<div class="black">Perioada: <span class="blue bold bigger-12em">'.$rezcam[$key_rezcam]['period'].'</span></div>';
			  echo '</div>';
			  echo '<div class="pret">';
			  echo 'Tarif/camera<br>'.$pleczile.' nopti';
			  echo '<span class="val red">'.new_price($rezcam[$key_rezcam]['pret']+$valoare_totala).' '.moneda($rezcam[$key_rezcam]['moneda']).'</span></div>';
			  $rezcam[$key_rezcam]['comision'] = calculeaza_comision($rezcam[$key_rezcam]['pret'], $detalii['comision_fix']*($rezcam[$key_rezcam]['nr_adulti']+$rezcam[$key_rezcam]['copil1']), $detalii['procent_comision']);
			  echo '<div class="float-right clearfix" style="width:150px;">';
			  echo '<div class="avl clearfix" style="width:165px;">';
			  if(soldout($id_oferta, $rezcam[$key_rezcam]['id_camera'], $plecdata)>0) {
				  $avail[$key_rezcam] = 'stopsales';
			  } else {
				  $avail[$key_rezcam] = 'disponibil';
			  }
			  if($avail[$key_rezcam]=='stopsales') {
				  echo '<span class="icon stop"></span><span class="red">Indisponibil</span>';
			  } elseif($avail[$key_rezcam]=='disponibil') {
				  echo '<span class="icon disp"></span><span class="green">Necesita confirmare</span>';
			  }
			  echo '</div>';
			  if($avail[$key_rezcam]=='disponibil') {
				  echo '<a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'rezervare-'.$id_oferta.'_'.base64_encode($rezcam[$key_rezcam]['id_pret'].'_'.$rezcam[$key_rezcam]['plecare'].'_'.$_REQUEST['plecdata'].'_'.$_REQUEST['pleczile'].'_'.$rezcam[$key_rezcam]['pret'].'_'.$rezcam[$key_rezcam]['id_camera'].'_'.$rezcam[$key_rezcam]['nr_adulti'].'_'.$rezcam[$key_rezcam]['nr_copii'].'_'.$rezcam[$key_rezcam]['copil1'].'_'.$rezcam[$key_rezcam]['copil2'].'_'.$rezcam[$key_rezcam]['copil3'].'_'.$detalii['masa'].'_'.$rezcam[$key_rezcam]['denumire_camera'].'_'.$rezcam[$key_rezcam]['comision'].'_'.$oldprice.'_'.$rezcam[$key_rezcam]['disponibilitate'].'_'.$rezcam[$key_rezcam]['data_early_booking'].'_'.str_replace('_','*',$rezcam[$key_rezcam]['PackageVariantId']).'_'.$rezcam[$key_rezcam]['RoomCode'].'_'.time()).'" class="rez" onClick="ga(\'send\', \'event\', \'pagina oferta\', \'rezerva oferta\', \''.$detalii['denumire'].'\');">
				  
				  <img src="/images/but_nou_rezerva.png" alt="Rezerva"></a>';
				  echo '<a href="/ofertele-mele/?data='.base64_encode($id_oferta.'_'.$rezcam[$key_rezcam]['id_pret'].'_'.$rezcam[$key_rezcam]['plecare'].'_'.$_REQUEST['plecdata'].'_'.$_REQUEST['pleczile'].'_'.$rezcam[$key_rezcam]['pret'].'_'.$rezcam[$key_rezcam]['id_camera'].'_'.$rezcam[$key_rezcam]['nr_adulti'].'_'.$rezcam[$key_rezcam]['nr_copii'].'_'.$rezcam[$key_rezcam]['copil1'].'_'.$rezcam[$key_rezcam]['copil2'].'_'.$rezcam[$key_rezcam]['copil3'].'_'.$detalii['masa'].'_'.$rezcam[$key_rezcam]['denumire_camera']).'" rel="nofollow" class="addToCart" title="Adauga aceasta oferta in cos" onClick="ga(\'send\', \'event\', \'pagina oferta\', \'adauga in cos\', \''.$detalii['denumire'].'\');">Adauga la comparatie</a>';
			  }
			  echo '</div>';
			  /*echo '<br class="clear"><ul class="taxe-neincluse smaller-09em w420">';
			  echo '<li class="first bigger-12em bold underline">Tariful include urmatoarele taxe obligatorii:</li>';
			  foreach($incl_serv['denumire'] as $k_incl => $v_incl) {
				echo '<li>'.$incl_serv['denumire'][$k_incl].' - <strong>'.$incl_serv['value'][$k_incl].' '.moneda($incl_serv['moneda'][$k_incl]).' '.$incl_serv['exprimare'][$k_incl].' x '.$incl_serv['nr_persoane'][$k_incl].'</strong></li>';
			  }
			  echo '</ul>';*/
			echo '</div>';
		}
		$afisare_servicii = 'nu';
	} else {
		$fara_rezervare = 'da';
		$afisare_servicii = 'da';
		echo '<div class="row clearfix">';
		  echo '<div class="bkg-white pad10">';
		  echo '<span class="bigger-11em bold">Pentru '.$adulti.' adult';
		  if($adulti>1) echo 'i';
		  if($copii>0) {
			  echo ' si '.$copii.' copi';
			  if($copii==1) echo 'l';
			  	else echo 'i';
		  }
		  echo ' va rugam sa ne <span class="blue">contactati telefonic</span> ori sa completati o <span class="blue">cerere pentru mai multe detalii</span></span> <span class="italic">(click pe butonul de mai jos)</span>.';
		  /*echo '<strong class="bigger-12em">Nu am gasit nici o camera disponibila pentru aceasta combinatie.</strong><br><br>';
		  echo '- Va rugam sa incercati o <strong>alta combinatie</strong> sau sa ne <strong>contactati telefonic</strong> ori sa completati o <strong>cerere pentru mai multe detalii</strong>.';*/
		  echo '</div>';
		echo '</div>';
	}
	echo '</div>';
}
?>

  <?php if(sizeof($detalii['denumire_v1'])>0 and $afisare_servicii=='nu' and $fara_rezervare!='da') { ?>
  <br>
  <div class="right-serv clearfix">
	<h3 class="black underline">Tarifele afisate includ</h3>
	<ul>
	<?php if(sizeof($incl_serv['denumire'])>0) {
		foreach($incl_serv['denumire'] as $k_incl => $v_incl) {
			echo '<li>'.$incl_serv['denumire'][$k_incl].' - <strong>'.$incl_serv['value'][$k_incl].' '.moneda($incl_serv['moneda'][$k_incl]).' '.$incl_serv['exprimare'][$k_incl].' x '.$incl_serv['nr_persoane'][$k_incl].'</strong></li>';
		}
	} ?>
	<?php foreach($detalii['denumire_v1'] as $key=>$value) { ?>
	  <li><?php echo schimba_caractere(ucfirst($value)); if($detalii['value_v1'][$key]) { echo ' - <strong>'.$detalii['value_v1'][$key].' '.moneda($detalii['moneda_v1'][$key]).'</strong>'; } ?></li>
	<?php } ?>
      <?php if(strpos(strtolower($detalii['transport']),'avion') !== false and strpos(strtolower(implode(" ",$detalii['denumire_v1'])),'bagaj') === false) echo '<li>Bagaj de mana si bagaj de cala</li>'; ?>
      <?php if($detalii['transport']!='Fara transport' and strpos(strtolower(implode(" ",$detalii['denumire_v1'])),'asistenta') === false) echo '<li>Asistenta turistica in limba romana</li>'; ?>
	</ul>
  </div>
  <?php } ?>

<?php } ?>
</form>
</div>

<?php if($fara_rezervare=='da') {
	//include_once($_SERVER['DOCUMENT_ROOT']."/includes/track_failed_selections.php");
	echo '<br>';
	include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/cerere_detalii.php");
	$afisare_servicii = 'da';
	
	if(!is_bot() and isset($err_logare_admin) and $detalii['valabila']=='da') {
		erori_disponibilitate($id_oferta, $id_hotel, $plecdata, $pleczile, $adulti, $copii);
	}
	
} ?>
