<?php // title and description zone ?>
<div class="sejururi">
	<?php
	if ( $nmasa == 'all inclusive' ) {
		$titlu_pag = 'Oferte All Inclusive ' . $den_zona;
	}
	?>
    <h1 class="main-title">
		<?php if ( $_GET['early-booking'] ) {
			echo '<span class="green">Early Booking</span> ';
		}
		echo $titlu_pag; ?>
		<?php if ( $localitate_plecare != '' and $id_transport=='4' ) {
			echo ' <br />Plecare din <span class="green">' . ucwords( $localitate_plecare ) . "</span>";
		}
		echo '- ' . $nr_hoteluri . ' Hoteluri Disponibile'; ?>
    </h1>
    <h2 class="main-subtitle green">Comparam preturile pentru <span
                class="textatentie">&nbsp;<?php echo $den_zona ?>&nbsp;</span> de la TourOperatori si va oferim
        <span class="textatentie">cel mai bun Pret!</span>
    </h2>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/localitati_plecare_avion_responsive.php" ); ?>
   <h2 class="main-tile green underline">Localitați din <?php echo $den_zona.' - '.$den_tara; ?></h2> 
	<?php
	if ( $nr_hoteluri > 0 ) {
		include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/sejururi/filtru_location_responsive.php' );
	}
	?>
</div>

<div id="show-filters-button" class="sejururi show-mobile">
    <button class="show-filters">
        <span>Filtreaza</span>
        <span class="hide">Inchide filtre</span>
    </button>
</div>

<?php // filters ?>
<div class="filter-wrapper">
	<?php if ( $_REQUEST['data-plecare'] || $id_transport || $nr_stele || $nmasa || $nconcept || $nfacilitati || $din_luna || ( $distanta && $distanta <> 'toate' ) || $_GET['early-booking'] == 'da' || $checkin ) : ?>
        <div class="sejururi">
            <div class="filters">
                <div class="titlu blue">Filtre active</div>
                <a href="<?php echo $link_p; ?>" class="delete" rel="nofollow"
                   onclick="setCookie('grad_ocupare','',-1);"><span>X</span>
                    Sterge toate filtrele</a>
                <ul class="delete-filters black" style="margin: 0;">
					<?php
					if ( $checkin ) {
						echo '<li><a href="">check-in <strong>' . date( "d.m.Y", strtotime( $checkin ) ) . '</strong></a></li>';
					}
					if ( $early ) {
						echo '<li><a href="">early booking</a></li>';
					}
					if ( $din_luna ) {
						echo '<li><a href="">' . $plecare . '</a></li>';
					}
					if ( $id_transport ) {
						$requests = str_replace( '&transport=' . $_REQUEST['transport'], '', $link );
						if ( $_REQUEST['plecare-avion'] ) {
							$requests = str_replace( '&plecare-avion=' . $_REQUEST['plecare-avion'], '', $requests );
						} else {
							$reqs = explode( '&', $requests );
							foreach ( $reqs as $rq ) {
								if ( preg_match( '/plecare-avion=/', $rq ) ) {
									$plecareAvion = $rq;
								}
							}
							$requests = str_replace( '&' . $plecareAvion, '', $requests );
						}

						echo '<li><a onclick="setCookie(\'grad_ocupare\',\'\',-1)" href="' . $link_p . '' . $requests . '">' . $trans . '</a></li>';
					}
					if ( $nr_stele ) {
						$requests = str_replace( '&stele=' . $_REQUEST['stele'], '', $link );
						echo '<li><a href="' . $link_p . '' . $requests . '">' . $nr_stele . ' stele</a></li>';
					}
					if ( $nmasa ) {
						$requests = str_replace( '&masa=' . $_REQUEST['masa'], '', $link );
						echo '<li><a href="' . $link_p . '' . $requests . '">' . $nmasa . '</a></li>';
					}
					if ( $distanta && $distanta <> 'toate' ) {
						echo '<li><a href="">' . $distanta . ' m</a></li>';
					}
					if ( $nconcept ) {
						$requests = str_replace( '&concept=' . $_REQUEST['concept'], '', $link );
						echo '<li><a href="' . $link_p . '' . $requests . '">' . $nconcept . '</a></li>';
					}
					if ( $nfacilitati ) {
						$requests = str_replace( '&facilitati=' . $_REQUEST['facilitati'], '', $link );
						echo '<li><a href="' . $link_p . '' . $requests . '">' . $nfacilitati . '</a></li>';
					}
					?>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                if ($(window).width() < 767) {
                    $('html, body').animate({scrollTop: $('#show-filters-button').offset().top}, 'slow');
                }
            });
        </script>
	<?php endif; ?>
    <div class="sejururi filter-wrapper-div">
		<?php
		if ( $nr_hoteluri > 0 ) {
			include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/sejururi/filtru_ajax_responsive.php' );
		}
		?>
        <div class="clear"></div>
        <div class="show-mobile" style="margin: 30px 0 0;">
            <button class="show-filters">Inchide filtre</button>
        </div>

        <div class="scroll-to-filters">
            <button class="scroll-filter"
                    onclick="$('html, body').animate({scrollTop: jQuery('.filter-wrapper').offset().top - 10}, 'slow');">
                Arata filtre
            </button>
        </div>
        <script>
            $(document).on('scroll', function () {
                if ($(window).width() > 767 && $('.scroll-to-filters').offset().top < $(document).scrollTop()) {
                    $('.scroll-filter').show();
                } else {
                    $('.scroll-filter').hide();
                }
            });
        </script>
        <script>
            $('.show-filters').on('click', function () {
                $('.show-filters').find('span').toggleClass('hide');
                $('.filter-wrapper-div').toggleClass('opened');
                $('.result-list').toggle('slow');
            });
        </script>
    </div>
</div>

<?php // resultes ?>
<div class="sejururi result-list">
	<?php $oferte->afiseaza(); ?>
    <div class="item-holder" style="display: none;">
		<?php $oferte->printPagini() ?>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.show-prices-button a', function () {
        var wrapper = $(this).closest('.item-holder');
        var hotel_id = $(this).attr('id').replace('vezipret', '');
        if (wrapper.find('.cautare_live').val() == 'da') {
            $('#hotel' + hotel_id + 'O').empty().html('<div align="center"><br><span class="bigger-11em bold black italic">Vă rugăm așteptați. Ofertele se incarca in timp real.</span><br><img src="/images/loader3.gif" alt="Loading" /></div>');
            setTimeout(function () {
                $('#hotel' + hotel_id + 'O').load(wrapper.find('.loader_link').val());
                if ($(window).width() < 767) {
                    setTimeout(function () {
                        $('html, body').animate({scrollTop: $(document).find('#tabelhotel' + hotel_id).offset().top}, 'slow');
                    }, 500);
                }
            }, 500);
        } else {
            $('#H' + hotel_id + 'O').empty().html('<br><span class="bigger-11em bold black italic">Vă rugăm așteptați. Incarcam ofertele</span><br><img src="/images/loader3.gif" alt="">');
            setTimeout(function () {
                $('#H' + hotel_id + 'O').load();
            }, 300);
        }
    });
</script>
<script>
    $(document).ready(function () {
        $('.result-list').jscroll({
            loadingHtml: '<img src="/images/loader3.gif" alt="Loading" /> Se incarca',
            padding: 20,
            debug: true,
            nextSelector: 'a.jscroll-next',
            contentSelector: '.item-holder'
        });
    });
</script>
<div class="clear"></div>

<?php // the most important from the current zone ?>
<div class="sejururi">

	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/localitati_zona_sus_responsive.php" ); ?>
</div>

<?php /*?><?php if ( strlen( $zona_detalii['imagine1'] ) > 5 and strlen( $zona_detalii['descriere_zona'] ) > 20 ) { ?>
    <div class="sejururi">
        <h1 class="main-title">
            Mai multe informatii despre <?php echo $zona_detalii['denumire_zona'] ?>
        </h1>
        <article class="pad10 article bigger-12em text-justify">
            <img src="/img_zona/<?php echo $zona_detalii['imagine1']; ?>"
                 alt="<?php echo $zona_detalii['denumire_zona'] ?>" class="float-left" style="margin:0 20px 0 0;">
			<?php ( $descriere_zona = $zona_detalii['descriere_zona'] );
			$descriere_zona = str_replace( '{imagine2}', '<img src="/img_zona/' . $zona_detalii['imagine2'] . '" alt="' . $zona_detalii['denumire_zona'] . '" class="float-left" style="margin:3px 10px 0 0;">', $descriere_zona );

			$descriere_zona = str_replace( '{imagine3}', '<img src="/img_zona/' . $zona_detalii['imagine3'] . '" alt="' . $zona_detalii['denumire_localitate'] . '" class="float-left" style="margin:3px 10px 0 0;">', $descriere_zona );
			echo $descriere_zona;
			?>
        </article>
        <div class="clear"></div>
        
    </div>
<?php } ?><?php */ ?>
