<h1 class="blue">Oferte Early Booking <? echo ucwords(desfa_link($_REQUEST['tip'])); ?></h1>

<div class="text-dreapta">
    <h2 class="green">Early Booking - Rezerva din timp pentru preturi mici!</h2>
    Daca te-ai hotarat deja unde vrei sa petreci vacanta, ai numai avantaje cu <strong>early booking 2011</strong>. Rezerva din timp si ai preturi mici pentru destinatii exotice sau europene. Poti descoperi fascinantul si misteriosul Egipt cu <strong>early booking Egipt</strong> sau atinge destinatia sporturilor de iarna cu <a href="http://www.ocaziituristice.ro/early_booking/austria/" title="Oferte early booking Austria">early booking Austria</a>. Iti recomandam si <a href="http://www.ocaziituristice.ro/early_booking/cipru/" title="Oferte early booking Cipru">early booking Cipru</a> pentru peisaje luxuriante si petreceri renumite. Odata ce ai ales din varietatea de <strong>oferte early booking</strong>, nu ramane decat sa te pregatesti de amintiri de neuitat si sa zambesti in fiecare zi cu gandul la locurile minunante pe care le vei vizita.
</div>    

<?php
 $link_pag="early-booking";
 $transport=desfa_link($_REQUEST['transport']);
 if($transport=='toate') $transport='';
 $masa=desfa_link($_REQUEST['masa']);
 if($masa=='toate') $masa='';
    $tari=new TARI();
	$tari_sejur=$tari->tari_ealy_by_tip_sejur('');
   foreach($tari_sejur as $key => $value)
   {
   if(sizeof($value)>'0') {
   ?>
<div class="countrydivcontainer">
  <?php /*?><ul class="tabLink">
<?
	foreach($value as $key1 => $value1)
		{
			foreach($value1 as $key2 => $value2)
		{ ?>
	 	  <li><a href="/early-booking/<? echo fa_link($key2); ?>/" title="Early Booking <? echo $key2; ?>"><? echo ucwords($key2); ?></a></li>
	  <? }
	}
	}
	?>
  </ul><?php */?>
<?
	foreach($value as $key1 => $value1)
		{
			foreach($value1 as $key2 => $value2)
		{ ?>
	 	  <div class="tabLinks"><a href="/early-booking/<? echo fa_link($key2); ?>/" title="Early Booking <? echo $key2; ?>"><? echo ucwords($key2); ?></a></div>
	  <? }
	}
	}
	?>
  <br class="clear" />
  <div class="filterText">Alege tara pentru a filtra ofertele Early Booking</div>
</div>
<?
  }	?>
  
  <div class="ordonare" align="right">
  <span class="red">Ordoneaza dupa:</span>
<form name="ord_stele" id="ord_stele" method="post" action="">
  <input type="hidden" name="tip_stele" value="<?php if(!$_POST['tip_stele']) echo 'desc'; elseif($_POST['tip_stele']=='desc') echo 'asc'; else echo 'desc'; ?>" />
  <?php if(!$_POST['tip_stele']) { ?>
  <a onclick="$('#loader').show(); document.ord_stele.submit();" class="initial" title="Numar stele Descendent">Numar stele</a>
  <?php } else { ?> <a href="#" onclick="$('#loader').show(); document.ord_stele.submit();" class="selected" title="Numar stele <?php if($_POST['tip_stele']=='desc') echo 'Ascedent'; else echo 'Descendent'; ?>"><?php if($_POST['tip_stele']=='desc') { ?><img src="/images/spacer.gif" class="asc" /> <?php } else { ?><img src="/images/spacer.gif" class="desc" /><?php } ?> Numar stele</a><?php } ?>
</form> |
<form name="ord_relevanta" id="ord_relevanta" method="post" action="">
  <input type="hidden" name="tip_relevanta" value="<?php if(!$_POST['tip_relevanta']) echo 'desc'; elseif($_POST['tip_relevanta']=='desc') echo 'asc'; else echo 'desc'; ?>" />
  <?php if(!$_POST['tip_relevanta']) { ?>
  <a onclick="$('#loader').show(); document.ord_relevanta.submit();" class="initial" title="Relevanta Descendent">Relevanta</a>
  <?php } else { ?> <a href="#" onclick="$('#loader').show(); document.ord_relevanta.submit();" class="selected" title="Relevanta <?php if($_POST['tip_relevanta']=='desc') echo 'Ascedent'; else echo 'Descendent'; ?>"><?php if($_POST['tip_relevanta']=='desc') { ?><img src="/images/spacer.gif" class="asc" /> <?php } else { ?><img src="/images/spacer.gif" class="desc" /><?php } ?> Relevanta</a><?php } ?>
</form> |
  <form name="ord_implicita" id="ord_implicita" method="post" action="">
  <?php if($_POST['tip_relevanta'] || $_POST['tip_stele'] || $_POST['tip_pret']) { ?>
  <a onclick="document.ord_implicita.submit();" class="initial" title="Ordonare implicita">Ordonare implicita</a>
  <?php } else { ?>
  <a onclick="document.ord_implicita.submit();" class="selected" title="Ordonare implicita">Ordonare implicita</a>
  <?php } ?>
  </form>
</div>
<?php
$afisare =new AFISARE_SEJUR();
$afisare->setEarly('da');
if($transport) $afisare->setTransport($transport);
if($masa) $afisare->setMasa($masa);
$afisare->set_oferte_pagina(10);
$afisare->setPromovata(3);
if($_POST['tip_stele']) $afisare->setOrdonareStele($_POST['tip_stele']);
if($_POST['tip_relevanta']) $afisare->setOrdonareRelevanta($_POST['tip_relevanta']);

$afisare->paginare(); ?>
<br/>
<div class="spacer"><img src="/images/spacer.gif" width="1" height="6" alt="" /></div>
<?php include('concurs/banner_concurs.php'); ?>
<div class="spacer"><img src="/images/spacer.gif" width="1" height="6" alt="" /></div>