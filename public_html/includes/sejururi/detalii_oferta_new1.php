<?php $_SESSION['page_back_offer'] = $_SERVER['REQUEST_URI']; ?>
<?php $den_hotel = $detalii_hotel['denumire'];
$den_loc = $detalii_hotel['localitate'];
$id_tara = $detalii_hotel['id_tara'];
$hoteluri_localitate = hoteluri_localitate($detalii_hotel['id_localitate'], $id_hotel, $detalii_hotel['latitudine'], $detalii_hotel['longitudine']); ?>

<div id="NEW-detaliiOferta" <?php if ($detalii['valabila'] == 'nu') { ?>style="background:#FFF url(/images/oferta_expirata.gif) center 90px;"<?php } ?>>
    <?php
    for ($stea = 1; $stea <= $detalii_hotel['stele']; $stea++) {
        $stars .= '<i class="icon-star yellow"></i>';
    }
    $link_rezervare = $sitepath . 'rezervare_sejur.php?hotel=' . $id_hotel . '&amp;oferta=' . $id_oferta;
    $link_rezervare2 = $sitepath . 'rezervare_sejur_new.php?hotel=' . $id_hotel . '&amp;oferta=' . $id_oferta;
    $link_cerere_detalii = $sitepath . 'cerere_detalii_new.php?hotel=' . $id_hotel . '&amp;oferta=' . $id_oferta;
    $valuta = 'nu';
    $asigurare = 'nu';

    if ($detalii['period_data_start'] > $date_now) {
        $dataplec_min = $detalii['period_data_start'];
    } else {
        $dataplec_min = $date_now;
    }
    if ($detalii['cazare'] == 'da' or $detalii['new_layout'] == 'da') {
        $dataplec_max = $detalii['period_data_end'];
    } else {
        $dataplec_max = $detalii['end_date'];
    }

    //if($detalii['cazare']!='da') {
    if ($detalii['new_layout'] == 'da' and $detalii['tip_preturi'] == 'plecari') {
        $var_dates = '';
        if (sizeof($preturi['data_start_normal']) > 0) {
            $var_dates .= 'array_dates = [';
            foreach ($preturi['data_start_normal'] as $key_ds => $value_ds) {
                $var_dates .= '"' . $value_ds . '",';
            }
            $var_dates = substr($var_dates, 0, -1) . ']';
        }
    }

    if ($detalii['tip_preturi'] == 'perioade') {
        $var_dates = '';
        $var_dates_if = '';
        if (sizeof($preturi['data_start_normal']) > 0) {
            foreach ($preturi['data_start_normal'] as $key_ds => $value_ds) {
                $ds[$key_ds] = explode('-', $value_ds);
                $de[$key_ds] = explode('-',
                    $preturi['data_end_normal'][$key_ds]/*date('Y-m-d', strtotime($preturi['data_end_normal'][$key_ds].' - '.$detalii['nr_nopti'].' days'))*/);
                $data_end_interval = $de[$key_ds][2];
                $var_dates .= 'ds_' . $key_ds . ' = new Date(' . $ds[$key_ds][0] . ', ' . $ds[$key_ds][1] . ' - 1, ' . $ds[$key_ds][2] . '), de_' . $key_ds . ' = new Date(' . $de[$key_ds][0] . ', ' . $de[$key_ds][1] . ' - 1, ' . $data_end_interval . '), ';

                $var_dates_if .= '(date >= ds_' . $key_ds . ' && date <= de_' . $key_ds . ')||';
            }
            $var_dates = substr($var_dates, 0, -2);
            $var_dates_if = substr($var_dates_if, 0, -2);
        }
    }

    if (isset($_COOKIE['grad_ocupare'])) {
        $grad_ocupare_cookie = explode('*', $_COOKIE['grad_ocupare']);
        if (isset($_GET['plecdata'])) {
            $date_cookie = date("Y-m-d", strtotime($_GET['plecdata']));
        } else {
            $date_cookie = date("Y-m-d", strtotime($grad_ocupare_cookie[0]));
        }
        if ($dataplec_min > $date_cookie or $dataplec_max < $date_cookie) {
            $show_calendar = 'da';
        }
    }
    if (isset($_REQUEST['plecdata']) and (strtotime($_REQUEST['plecdata']) < strtotime($dataplec_min) or strtotime($_REQUEST['plecdata']) > strtotime($dataplec_max))) {
        $show_calendar = 'da';
    }
    if ($detalii['tip_preturi'] == 'plecari' and !in_array(date("Y-m-d", strtotime($_REQUEST['plecdata'])), $preturi['data_start_normal'])) {
        $show_calendar = 'da';
    }

    $sel_table_SEO = "SELECT cuvant_cheie FROM seo WHERE id_tip = '0' AND id_oras = '" . $detalii_hotel['locatie_id'] . "' ";
    $que_table_SEO = mysql_query($sel_table_SEO) or die(mysql_error());
    $row_table_SEO = mysql_fetch_array($que_table_SEO);
    @mysql_free_result($que_table_SEO);
    if ($row_table_SEO['cuvant_cheie']) {
        $cuvant_cheie_loc = $row_table_SEO['cuvant_cheie'];
    } else {
        $cuvant_cheie_loc = 'Cazare ' . $detalii_hotel['localitate'];
    }
    $link_cuvant_cheie_loc = $link_tip . fa_link($detalii_hotel['tara']) . '/' . fa_link($detalii_hotel['zona']) . '/' . fa_link($detalii_hotel['localitate']) . '/';

    $autocar = [];
    if ($detalii['transport'] == 'Autocar') {
        $autocar = $det->get_autocar_sejur($id_oferta);
    }
    /*if(count($autocar)>0) {
        //$autocar = multid_sort($autocar, 'denumire_localitate');
        //$autocar = array_map('unserialize', array_unique(array_map('serialize', $autocar)));
        $sort = array();
        foreach($autocar as $key_sort => $value_sort) {
            $sort['disponibilitate'][$key_sort] = $value_sort['disponibilitate'];
            $sort['pret'][$key_sort] = $value_sort['pret'];
        }
        array_multisort($sort['denumire_localitate'], SORT_ASC, $autocar);
    }*/
    ?>

    <h1 <?php /*?>class="float-left" style="width:660px;"<?php */ ?>>
        <span class="red"><?php echo $detalii_hotel['denumire']; ?></span>
        <?php echo $stars; ?><br/>
        <span class="smaller-07em"><?php echo $detalii['denumire_scurta']; ?></span>
    </h1>

    <?php /*?><div class="float-right clearfix" style="padding:10px 0 0 0;">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  </div>
  
  <br class="clear"><?php */ ?>

    <div class="NEW-adresa float-left">
        <?php echo $detalii_hotel['adresa'] . ' ';
        if ($detalii_hotel['nr']) {
            echo 'nr. ' . $detalii_hotel['nr'] . ', ';
        }
        echo $detalii_hotel['localitate'] . ', ';
        echo $detalii_hotel['tara'];
        if ($detalii_hotel['distanta']) {
            echo ', <strong>' . $detalii_hotel['distanta'] . 'm de ' . $detalii_hotel['distanta_fata_de'] . '</strong>, ';
        }
        if ($detalii_hotel['latitudine'] && $detalii_hotel['longitudine'] && $detalii_hotel['latitudine'] <> 0 && $detalii_hotel['longitudine'] <> 0) { ?>
            (<a href="#harta" id="button-harta" class="link"><strong>Arată harta</strong></a>)
        <?php } ?>
    </div>

    <br class="clear">

    <div class="Hline"></div>

    <?php if ($detalii['valabila'] == 'nu') {
        echo '<br><br><h2 class="text-center red" style="font-size:2.5em;">ACEASTĂ OFERTĂ NU ESTE VALABILĂ !</h2>';
        echo '<br><div class="text-center bold" style="font-size:2.5em;">Oferta curentă pentru <a href="' . make_link_oferta($detalii_hotel['localitate'],
                $detalii_hotel['denumire'], null, null) . '" class="link-red">' . $detalii_hotel['denumire'] . '</a></div><br><br><br><br>';
    } ?>

    <div class="NEW-column-left1">

        <div>
            <div class="NEW-gallery">
                <div class="single-img">
                    <div class="images"
                         id="poza_principala"
                         style="width:660px; height:350px; background-size:662px; background-position:center center; background-repeat:no-repeat; background-image:url(/img_prima_hotel/<?php echo $detalii_hotel['poza1']; ?>);"></div>
                    <?php if ($detalii['last_minute'] == 'da') {
                        echo '<div class="last-minute"></div>';
                    } ?>
                    <?php if (sizeof($detalii['early_time']) > 0) {
                        echo '<div class="early-booking"></div>';
                    } ?>
                    <?php if ($detalii['spo_titlu']) {
                        echo '<div class="oferta-speciala"></div>';
                    } ?>
                    <div class="comentarii-pic">
                        <?php if ($nota_total > 0) { ?>
                            <div class="text-right">
                                <span class="bold blue" style="font-size:20px;"><?php echo $nota_total; ?></span>
                                <span class="black smaller-09em">(scor calculat din <strong><?php echo $nr_comentarii; ?></strong> comentarii)</span><br>
                                <a href="#comentarii" class="link-red bold" id="button-comentarii">Vezi toate comentariile</a>
                            </div>
                        <?php } else { ?>
                            <div class="text-right">
                                <i class="icon-comments float-left bigger-13em" style="margin:3px 6px 0 0;"></i><span class="black">Ai fost la  <a href="#comentarii"
                                                                                                                                                   class="link-red bold"
                                                                                                                                                   id="button-comentarii"
                                                                                                                                                   title="Impresii <?php echo $detalii_hotel['denumire'] ?>"><?php echo $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate']; ?></a>?</span><br>
                                <?php /*?> <a href="#comentarii" class="link-red bold" id="button-comentarii">Adaugă impresia ta</a><?php */ ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="NEW-gallery">
                <?php
                for ($i = 1; $i <= 16; $i++) {
                    if ($detalii_hotel['poza' . $i]) { ?>
                        <a href="/img_prima_hotel/<?php echo $detalii_hotel['poza' . $i]; ?>"
                           onmouseover="document.getElementById('poza_principala').style.backgroundImage='url(/img_prima_hotel/<?php echo $detalii_hotel['poza' . $i]; ?>)';return false;"
                           class="gallery"><img src="/thumb_hotel/<?php echo $detalii_hotel['poza' . $i]; ?>"
                                                width="75"
                                                alt="<?php echo $detalii_hotel['denumire'] . " imagine " . $i; ?>"
                                                class="images"/></a>
                    <?php }
                } ?>
            </div>
        </div>

    </div>
    <div class="NEW-column-right1 text-left">

        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/incredere_dece_ocazii.php"); ?>

        <?php /*?><?php if($detalii_hotel['tripadvisor']) { ?>
  <div class="tripadvisor">
    <div class="inside">
      <?php echo $detalii_hotel['tripadvisor']; ?>
	</div>
  </div>
  <?php } ?><?php */ ?>

        <br class="clearfix">

    </div>

    <br class="clear">

    <div class="NEW-column-full2">

        <?php if ($detalii['early_time'][0]) { ?>
            <div class="float-left mar10 w520">
                <img src="/images/icon_discount.png" alt="Discount" class="float-left" style="margin:0 11px 0 1px;">
                <div class="bigger-13em black bold" style="margin: 13px 0 0 0;">Reducere <span class="green">Early Booking </span>
                    <?php if ($detalii['early_disc'][0] > 0) { ?><span class="bigger-11em red"><?php echo $detalii['early_disc'][0]; ?>%</span><?php } ?>
                    până la <span class="bigger-11em red"><?php echo $detalii['early_time'][0]; ?></span></div>
            </div>
        <?php } ?>

        <?php if ($detalii_hotel['concept']) { ?>
            <div class="float-left mar10 w280">
                <img src="/images/icon_thumbsup.png" alt="Recomandat pentru" class="float-left" style="margin:0 11px 0 1px;">
                <div class="bigger-12em black bold">Recomandat pentru:<br><span class="bigger-13em red"><?php echo $detalii_hotel['concept']; ?></span></div>
            </div>
        <?php } ?>
        <div class="float-left mar10 ">
            <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/socials_top.php"); ?>
        </div>
        <?php if ($detalii['early_time'][0]) {
            echo '<br class="clear">';
        } ?>

        <?php if ($detalii_hotel['detalii_concept']) { ?>
            <blockquote class="mar10<?php if (!$detalii['early_time'][0]) {
                echo ' float-left w620';
            } ?>">
                <?php echo $detalii_hotel['detalii_concept']; ?>
            </blockquote>
        <?php } ?>

        <br class="clear"><br>
        <?php
        if ($detalii['valabila'] == 'da') {

            // if(!$err_logare_admin) {
            if (($detalii['tip_preturi'] == 'perioade' and $detalii['nr_nopti'] > 3 and $detalii['exprimare_pret'] == 'sejur/pers') || (preg_match("/revelion|craciun|seniori/",
                    strtolower($detalii['denumire_scurta'])))) {
                include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/tabel_preturi.php");
            }
            //}
            ?>
            <?php /*?>  <div id="anounce" class="newText clearfix chn-blue bkg-blue green">
    <i class="icon-lock-open-alt float-left mar5" style="font-size:44px;"></i><div class="float-left mar0-10"><span class="bigger-15em black underline">Asigurați-vă <strong>cel mai bun preț</strong> pentru vacanța dumneavoastră!</span><br><span class="bold">Prețurile pot crește, mai bine faceți rezervarea astăzi.</span></div>
  </div><?php */
            ?>
            <?php /*?><script>$("#anounce").delay(3000).slideDown(1000);</script><?php */
            ?>
            <br class="clear"><br>

            <script>
                function displayAges(nVal) {
                    if (nVal > 0) {
                        document.getElementById("chd-ages").style.display = "block";
                    } else {
                        document.getElementById("chd-ages").style.display = "none";
                    }

                    for (var i = 0; i < 3; i++) {
                        if (i < nVal) {
                            document.getElementById("varste-copii-" + i).style.display = "inline-block";
                        } else {
                            document.getElementById("varste-copii-" + i).style.display = "none";
                            document.getElementById("varste-copii-select-" + i).value = 0;
                        }
                    }
                }
            </script>

        <?php
        if (isset($_COOKIE['grad_ocupare'])) {
            $grad_ocupare = explode("*", $_COOKIE['grad_ocupare']);

            $plecdata = $grad_ocupare[0];
            $pleczile = $grad_ocupare[1];
            $adulti = $grad_ocupare[2];
            $copii = $grad_ocupare[3];
            $copil_age[0] = $grad_ocupare[4];
            $copil_age[1] = $grad_ocupare[5];
            $copil_age[2] = $grad_ocupare[6];
            $tip_masa = $grad_ocupare[7];
            $tip_transport = $grad_ocupare[8];
            $orasplec = $grad_ocupare[9];
        }
        if (isset($_REQUEST['plecdata'])) {
            $plecdata = $_REQUEST['plecdata'];
            $pleczile = $_REQUEST['pleczile'];
            $adulti = $_REQUEST['adulti'];
            $copii = $_REQUEST['copii'];
            $tip_masa = $_REQUEST['tip_masa'];
            $tip_transport = $_REQUEST['tip_transport'];
            $copil_age[0] = $_REQUEST['age'][0];
            $copil_age[1] = $_REQUEST['age'][1];
            $copil_age[2] = $_REQUEST['age'][2];
            $orasplec = $_REQUEST['orasplec'];
        }
        if (!isset($_COOKIE['grad_ocupare']) and !isset($_REQUEST['plecdata'])) {
            $plecdata = date("d.m.Y", strtotime($preturi['data_start_normal'][1]));
            $reference_date = date("d.m.Y", strtotime("+ 4 day"));
            if ($detalii['tip_preturi'] == 'perioade' and strtotime($plecdata) < strtotime($reference_date)) {
                $plecdata = $reference_date;
            }
            $pleczile = $detalii['nr_nopti'];
            $adulti = 2;
            $copii = 0;
            $copil_age[0] = 0;
            $copil_age[1] = 0;
            $copil_age[2] = 0;
            $tip_masa = '';
            $tip_transport = '';
            $orasplec = get_id_localitate('Bucuresti');
        }
        if ($detalii['tip_preturi'] == 'plecari') {
            if (in_array(date("Y-m-d", strtotime($plecdata)), $preturi['data_start_normal'])) {
                $plecdata = $plecdata;
            } elseif (in_array(date("Y-m-d", strtotime("+1 day", strtotime($plecdata))), $preturi['data_start_normal'])) {
                $plecdata = date("d.m.Y", strtotime("+1 day", strtotime($plecdata)));
            } elseif (in_array(date("Y-m-d", strtotime("-1 day", strtotime($plecdata))), $preturi['data_start_normal'])) {
                $plecdata = date("d.m.Y", strtotime("-1 day", strtotime($plecdata)));
            } elseif (in_array(date("Y-m-d", strtotime("+2 day", strtotime($plecdata))), $preturi['data_start_normal'])) {
                $plecdata = date("d.m.Y", strtotime("+2 day", strtotime($plecdata)));
            } elseif (in_array(date("Y-m-d", strtotime("-2 day", strtotime($plecdata))), $preturi['data_start_normal'])) {
                $plecdata = date("d.m.Y", strtotime("-2 day", strtotime($plecdata)));
            } elseif (in_array(date("Y-m-d", strtotime("+3 day", strtotime($plecdata))), $preturi['data_start_normal'])) {
                $plecdata = date("d.m.Y", strtotime("+3 day", strtotime($plecdata)));
            } elseif (in_array(date("Y-m-d", strtotime("-3 day", strtotime($plecdata))), $preturi['data_start_normal'])) {
                $plecdata = date("d.m.Y", strtotime("-3 day", strtotime($plecdata)));
            }
        }

        $detalii_online = $det->oferte_online($id_hotel, date("Y-m-d", strtotime($plecdata)), $detalii['id_transport']);
        //echo '<pre>';print_r($detalii);echo '</pre>';
        $oras_plecare = $detalii_online['oras_plecare'][$id_oferta][0];
        $oras_destinatie = $detalii_online['oras_destinatie'][$id_oferta][1];
        $services = $det->offer_services($id_hotel, $detalii['id_transport'], $pleczile, get_den_localitate($oras_plecare), $oras_destinatie, $id_oferta);

        $make_request = 'nu';
        $showerr = 'da';
        if (isset($_REQUEST['plecdata']) or isset($_COOKIE['grad_ocupare'])) {
            $make_request = 'da';
        }
        if ($err_logare_admin and (strtotime($plecdata) < strtotime($dataplec_min) or strtotime($plecdata) > strtotime($dataplec_max))) {
            $make_request = 'nu';
        }
        //if($detalii['tip_preturi']=='plecari' and !in_array(date("Y-m-d",strtotime($plecdata)), $preturi['data_start_normal'])) $make_request = 'nu';
        if ($err_logare_admin and $detalii['cazare'] == 'da' and $show_calendar == 'da') {
            $make_request = 'nu';
        }
        if (!isset($_REQUEST['plecdata']) and !isset($_COOKIE['grad_ocupare']) and $detalii['tip_preturi'] == "plecari") {
            $make_request = 'da';
        }
        if (!isset($_REQUEST['plecdata'])) {
            $showerr = 'nu';
        }

        if ($detalii['nr_nopti'] > 1) {
            $pleczile = $detalii['nr_nopti'];
        } else {
            $pleczile = $pleczile;
        }
        $plecnopti = $pleczile - 1;
        $arivdata = date('Y-m-d', strtotime($plecdata . ' + ' . $plecnopti . ' days'));
        if (sizeof($detalii['nr_min']) > 0) {
            sort($detalii['nr_min']);
        }

        //echo " aici ".$make_request;

        $editFormAction = $_SERVER['REQUEST_URI'];
        if (isset($_SERVER['QUERY_STRING'])) {
            $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
        }

        ?>
            <i id="preturi"></i>
            <form name="cautare-oferte" id="calc" action="<?php echo $editFormAction; ?>#preturi" method="get" style="width:980px; z-index:1000;">
                <div class="chenar chn-color-green border-green">
                    <div class="NEW-search-wide clearfix">
                        <div class="chapter-title red">Când doriți să <?php if ($detalii_hotel['tip_unitate'] == 'Circuit') {
                                echo 'plecați în';
                            } else {
                                echo 'stați la';
                            } ?> <?php echo $detalii_hotel['denumire']; ?>?
                        </div>
                        <div class="field text-left" style="width:118px;">
                            <label for="calc-dataplec">Data Check-in:</label>
                            <input type="text" name="plecdata" id="calc-dataplec" value="<?php if ($plecdata) {
                                echo $plecdata;
                            } ?>" class="NEW-round4px" style="width:106px;">
                        </div>
                        <div class="field text-left" style="width:118px;">
                            <label for="calculeaza-nr-nopti">Nr. nopţi:</label>
                            <select name="pleczile" id="calculeaza-nr-nopti" class="NEW-round4px">
                                <?php $max_nopti = 15;
                                if ($detalii['cazare'] == 'da') {
                                    $min_nopti = 5;
                                }
                                if ($detalii['nr_min'][0] > 1) {
                                    $min_nopti = $detalii['nr_min'][0];
                                }
                                if ($detalii['nr_nopti'] > 1) {
                                    $min_nopti = $detalii['nr_nopti'];
                                    $max_nopti = $detalii['nr_nopti'];
                                }
                                for ($i = $min_nopti; $i <= $max_nopti; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php if ($pleczile == $i) {
                                        echo 'selected';
                                    } ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="field text-left" style="width:118px;">
                            <label for="calculeaza-adulti">Nr. adulţi:</label>
                            <select name="adulti" id="calculeaza-adulti" class="NEW-round4px">
                                <?php for ($i = 1; $i <= 3; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php if (!$adulti and $i == 2) {
                                        echo 'selected';
                                    } elseif ($adulti == $i) {
                                        echo 'selected';
                                    } ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="field text-left" style="width:118px;">
                            <label for="calculeaza-copii">Nr. copii:</label>
                            <select name="copii" id="calculeaza-copii" class="NEW-round4px" onchange="displayAges(this.value)">
                                <?php for ($i = 0; $i <= 3; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php if ($copii == $i) {
                                        echo 'selected';
                                    } ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php //if($detalii['online_prices']=='da') {
                        ?>
                        <div id="chd-ages" style="margin:0; <?php if ($copii == 0) {
                            echo 'display:none;';
                        } ?>">
                            <?php for ($t = 0; $t < 3; $t++) { ?>
                                <div class="field text-left" style="width:73px; <?php if ($copii < ($t + 1)) {
                                    echo 'display:none;';
                                } ?>" id="varste-copii-<?php echo $t; ?>">
                                    <label for="varste-copii-<?php echo $t; ?>">Copil <?php echo $t + 1; ?></label>
                                    <select name="age[<?php echo $t; ?>]" class="NEW-round4px" id="varste-copii-select-<?php echo $t; ?>">
                                        <?php for ($j1[$t] = 0; $j1[$t] <= 17; $j1[$t]++) { ?>
                                            <option value="<?php echo $j1[$t]; ?>" <?php if ($copil_age[$t] == $j1[$t] and ($t + 1) <= $copii) {
                                                echo 'selected';
                                            } ?>><?php echo $j1[$t]; ?> ani
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                        </div>
                        <?php //}
                        ?>
                        <input type="submit"
                               value="Vezi preț"
                               id="button-prices"
                               class="btn-red NEW-round6px float-right"
                               style="font-size:16px; letter-spacing:1px; padding:4px 19px; margin:27px 10px 0 0;"></a>

                        <br class="clear">

                    </div>
                </div>
            </form>

        <?php $value_furnizor = $detalii['furnizor']; ?>
            <div id="preturiOnline_<?php echo $value_furnizor; ?>"></div>
        <?php if ($make_request == 'da') { ?>
            <script>
                $("#preturiOnline_<?php echo $value_furnizor; ?>").empty().html('<br><span class="bigger-13em bold">Vă rugăm așteptați. Verificăm preţurile şi disponibilitatea în timp real.</span><br><img src="/images/loader3.gif" alt="loading">');
                setTimeout(function () {
                    $("#preturiOnline_<?php echo $value_furnizor; ?>").load("/includes/preturi_online_oferta.php?<?php echo 'id_oferta=' . $id_oferta . '&plecdata=' . $plecdata . '&pleczile=' . $pleczile . '&adulti=' . $adulti . '&copii=' . $copii . '&copil1=' . $copil_age[0] . '&copil2=' . $copil_age[1] . '&copil3=' . $copil_age[2] . '&masa=' . $tip_masa . '&trans=' . $tip_transport . '&orasplec=' . $orasplec . '&showerr=' . $showerr; ?>");
                }, 10);
                $("#button-prices").click(function () {
                    $('html, body').animate({
                        scrollTop: $("#preturi").offset().top
                    });
                });
            </script>
        <br class="clear"><br>
        <?php } ?>

            <div class="section clearfix">
                <?php if ($detalii_hotel['cautare_live'] == 'da') { ?>
                    <?php if (sizeof($services['servicii_incluse']) > '0' and $detalii['furnizor'] != '471') { ?>
                        <h3 class="black">Tarifele includ</h3>
                        <div class="content clearfix">
                            <ul <?php if (strpos(strtolower(implode(" ", $services['servicii_incluse'])), 'asigurare') !== false) {
                                echo 'class="float-left w400"';
                            } ?>>
                                <?php foreach ($services['servicii_incluse'] as $key => $value) { ?>
                                    <li><?php echo schimba_caractere(ucfirst($value)); ?></li>
                                    <?php if (strpos(strtolower($value), 'asigurare') !== false and (strpos(strtolower($value),
                                                'sanatat') !== false or strpos(strtolower($value), 'medical') !== false) and strpos(strtolower($value),
                                            'storn') !== false) {
                                        $asigurare = 'da';
                                    } ?>
                                <?php } ?>
                            </ul>
                            <?php if ($asigurare == 'da') { ?>
                                <div class="bubble-asigurari-small bubble-left white mar10 w240 float-right"><a href="/info-asigurari-de-calatorie.html"
                                                                                                                class="infos2"><strong>Asigurări de călătorie oferite
                                            de</strong><br><img src="/images/logo_mondial_assistance.png"
                                                                alt="Mondial Assistance"
                                                                class="w100"><span class="float-right smaller-09em underline"><br>&raquo; click aici</span><br class="clear"></a>
                                </div><?php } ?>
                        </div>
                        <div class="clear-line"></div>
                    <?php } ?>
                    <?php if (count($services['servicii_neincluse']) > '0') { ?>
                        <h3 class="black">Neincluse în tarif</h3>
                        <div class="content clearfix">
                            <ul <?php if ($asigurare == 'nu') {
                                echo 'class="float-left w400"';
                            } ?>>
                                <?php foreach ($services['servicii_neincluse'] as $key => $value) { ?>
                                    <li><?php echo schimba_caractere(ucfirst($value)); ?></li>
                                    <?php if (strpos(strtolower($value), 'asigurare') !== false and (strpos(strtolower($value),
                                                'sanatat') !== false or strpos(strtolower($value), 'medical') !== false) and strpos(strtolower($value),
                                            'storn') !== false) {
                                        $asigurare = 'nu';
                                    } ?>
                                <?php } ?>
                            </ul>
                            <?php if ($asigurare == 'nu' and $detalii_hotel['tara'] != 'Romania') { ?>
                                <div class="bubble-asigurari-small bubble-left white mar10 w240 float-right"><a href="/info-asigurari-de-calatorie.html"
                                                                                                                class="infos2"><strong>Asigurări de călătorie oferite
                                            de</strong><br><img src="/images/logo_mondial_assistance.png"
                                                                alt="Mondial Assistance"
                                                                class="w100"><span class="float-right smaller-09em underline"><br>&raquo; click aici</span><br class="clear"></a>
                                </div><?php } ?>
                        </div>
                        <div class="clear-line"></div>
                    <?php } ?>
                <?php } else { ?>
                    <?php if ((sizeof($detalii['denumire_v1']) > '0') and ($detalii['furnizor'] != '471')) { ?>
                        <h3 class="black">Tarifele includ</h3>
                        <div class="content clearfix">
                            <ul <?php if (strpos(strtolower(implode(" ", $detalii['denumire_v1'])), 'asigurare') !== false) {
                                echo 'class="float-left w400"';
                            } ?>>
                                <?php foreach ($detalii['denumire_v1'] as $key => $value) { ?>
                                    <li><?php echo schimba_caractere(ucfirst($value));
                                        if ($detalii['value_v1'][$key]) {
                                            echo ' - <strong>' . $detalii['value_v1'][$key] . ' ' . moneda($detalii['moneda_v1'][$key]) . '</strong>';
                                        } ?>
                                        <?php if (strpos(strtolower($value), 'asigurare') !== false and (strpos(strtolower($value),
                                                    'sanatat') !== false or strpos(strtolower($value), 'medical') !== false) and strpos(strtolower($value),
                                                'storn') !== false) {
                                            $asigurare = 'da';
                                        } ?>
                                    </li>
                                <?php } ?>
                                <?php //if(strpos(strtolower($detalii['transport']),'avion') !== false and strpos(strtolower(implode(" ",$detalii['denumire_v1'])),'bagaj') === false) echo '<li>Bagaj de mana si bagaj de cala</li>'; ?>
                                <?php //if($detalii['transport']!='Fara transport' and strpos(strtolower(implode(" ",$detalii['denumire_v1'])),'asistenta') === false) echo '<li>Asistenta turistica in limba romana</li>'; ?>
                            </ul>
                            <?php if ($asigurare == 'da') { ?>
                                <div class="bubble-asigurari-small bubble-left white mar10 w240 float-right"><a href="/info-asigurari-de-calatorie.html"
                                                                                                                class="infos2"><strong>Asigurări de călătorie oferite
                                            de</strong><br><img src="/images/logo_mondial_assistance.png"
                                                                alt="Mondial Assistance"
                                                                class="w100"><span class="float-right smaller-09em underline"><br>&raquo; click aici</span><br class="clear"></a>
                                </div><?php } ?>
                        </div>
                        <div class="clear-line"></div>
                    <?php } ?>

                    <?php if (sizeof($detalii['denumire_v2']) > '0' or sizeof($detalii['denumire_v3']) > '0') { ?>
                        <h3 class="black">Neincluse în tarif</h3>
                        <div class="content clearfix">
                            <ul <?php if ($asigurare == 'nu') {
                                echo 'class="float-left w400"';
                            } ?>>
                                <?php if (sizeof($detalii['denumire_v2']) > '0') {
                                    foreach ($detalii['denumire_v2'] as $key => $value) { ?>
                                        <li><?php echo schimba_caractere(ucfirst($value));
                                            if ($detalii['data_start_v2'][$key]) {
                                                echo ' <em>(' . date("d.m.Y", strtotime($detalii['data_start_v2'][$key])) . ' - ' . date("d.m.Y",
                                                        strtotime($detalii['data_end_v2'][$key])) . ')</em>';
                                            }
                                            if ($detalii['value_v2'][$key]) {
                                                echo ' - <strong>' . $detalii['value_v2'][$key] . ' ' . moneda($detalii['moneda_v2'][$key]) . ' ' . $detalii['exprimare_v2'][$key] . ' ' . $detalii['pasager_v2'][$key] . '</strong>';
                                            }
                                            if ($detalii['obligatoriu_v2'][$key] == 'da') {
                                                echo ' - <em class="red bold">Obligatoriu</em>';
                                            } else {
                                                echo ' - <em class="blue">Opțional</em>';
                                            } ?></li>
                                        <?php if (strpos(strtolower($value), 'asigurare') !== false and (strpos(strtolower($value),
                                                    'sanatat') !== false or strpos(strtolower($value), 'medical') !== false) and strpos(strtolower($value),
                                                'storn') !== false) {
                                            $asigurare = 'nu';
                                        } ?>
                                    <?php }
                                } ?>
                                <?php if (sizeof($detalii['denumire_v3']) > '0') {
                                    foreach ($detalii['denumire_v3'] as $key => $value) { ?>
                                        <li><?php echo schimba_caractere(ucfirst($value));
                                            if ($detalii['data_start_v3'][$key]) {
                                                echo ' <em>(' . date("d.m.Y", strtotime($detalii['data_start_v3'][$key])) . ' - ' . date("d.m.Y",
                                                        strtotime($detalii['data_end_v3'][$key])) . ')</em>';
                                            }
                                            if ($detalii['value_v3'][$key]) {
                                                echo ' - <strong>' . $detalii['value_v3'][$key] . ' ' . moneda($detalii['moneda_v3'][$key]) . ' ' . $detalii['exprimare_v3'][$key] . ' ' . $detalii['pasager_v3'][$key] . '</strong>';
                                            }
                                            if ($detalii['obligatoriu_v3'][$key] == 'da') {
                                                echo ' - <em class="red bold">Obligatoriu</em>';
                                            } else {
                                                echo ' - <em class="blue">Opțional</em>';
                                            } ?></li>
                                        <?php if (strpos(strtolower($value), 'asigurare') !== false and (strpos(strtolower($value),
                                                    'sanatat') !== false or strpos(strtolower($value), 'medical') !== false) and strpos(strtolower($value),
                                                'storn') !== false) {
                                            $asigurare = 'nu';
                                        } ?>
                                    <?php }
                                } ?>
                                <?php if ($detalii['transport'] == 'Fara transport') {
                                    if ($detalii_hotel['tara'] == 'Bulgaria') {
                                        echo '<li class="bold">Transport autocar Bulgaria - <a href="/transport-bulgaria.html" class="link-blue" target="_blank">click aici</a></li>';
                                    }
                                    if ($detalii_hotel['tara'] == 'Grecia') {
                                        if ($detalii_hotel['zona'] == 'Insula Thassos') {
                                            echo '<li class="bold">Transport autocar Thassos - <a href="/transport-thassos.html" class="link-blue" target="_blank">click aici</a></li>';
                                        }
                                        if ($detalii_hotel['zona'] == 'Paralia Katerini') {
                                            echo '<li class="bold">Transport autocar Paralia Katerini - <a href="/transport-paralia-katerini.html" class="link-blue" target="_blank">click aici</a></li>';
                                        }
                                    }
                                } ?>
                            </ul>
                            <?php if ($asigurare == 'nu' and $detalii_hotel['tara'] != 'Romania') { ?>
                                <div class="bubble-asigurari-small bubble-left white mar10 w240 float-right"><a href="/info-asigurari-de-calatorie.html"
                                                                                                                class="infos2"><strong>Asigurări de călătorie oferite
                                            de</strong><br><img src="/images/logo_mondial_assistance.png"
                                                                alt="Mondial Assistance"
                                                                class="w100"><span class="float-right smaller-09em underline"><br>&raquo; click aici</span><br class="clear"></a>
                                </div><?php } ?>
                        </div>
                        <div class="clear-line"></div>
                    <?php } ?>
                <?php } ?>

                <?php if ($detalii["descriere_oferta"]) { ?>
                    <h3 class="black">Detalii ofertă</h3>
                    <div class="content clearfix">
                        <?php afisare_frumos(schimba_caractere($detalii["descriere_oferta"]), 'nl2br'); ?>
                    </div>
                    <div class="clear-line"></div>
                <?php } ?>

                <?php if ($detalii_hotel['cautare_live'] == 'da') { ?>
                    <?php if (count($services['conditii_plata']) < '0') { ?>
                        <h3 class="black">Condiții de plată</h3>
                        <div class="content clearfix">
                            <ul>
                                <?php foreach ($services['conditii_plata'] as $key => $value) { ?>
                                    <li><?php echo schimba_caractere(ucfirst($value)); ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="clear-line"></div>
                    <?php } ?>
                <?php } else { ?>
                    <?php /*?><?php if($detalii['conditii_plata']) { ?>
    <h3 class="black">Condiții de plată</h3>
    <div class="content clearfix">
      <ul>
        <?php echo afisare_frumos($detalii['conditii_plata'], 'nl2li'); ?>
      </ul>
    </div>
    <div class="clear-line"></div>
    <?php } ?><?php */ ?>
                <?php } ?>

                <?php if ($detalii['conditii_anulare']) { ?>
                    <h3 class="black">Condiții de anulare</h3>
                    <div class="content clearfix">
                        <ul>
                            <?php echo afisare_frumos($detalii['conditii_anulare'], 'nl2li'); ?>
                        </ul>
                    </div>
                    <div class="clear-line"></div>
                <?php } ?>

                <?php if ($detalii['nota']) { ?>
                    <h3 class="black">Notă</h3>
                    <div class="content clearfix">
                        <?php afisare_frumos(schimba_caractere(trim($detalii['nota'])), 'nl2br'); ?>
                    </div>
                    <div class="clear-line"></div>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if ($detalii_hotel['cautare_live'] == 'nu') { ?>
            <br>

            <?php $oferte = $det->get_oferte($id_hotel, $id_oferta);
            if (sizeof($oferte) > 0) { ?>
                <div id="alte-oferte">

                    <h2 class="blue underline">Alte oferte disponibile la <?php echo $detalii_hotel['denumire']; ?></h2>

                    <table class="search-rooms">
                        <thead>
                        <tr class="bkg-white">
                            <th class="text-center">Ofertă</th>
                            <th class="text-center w80">Nr. nopți</th>
                            <th class="text-center w120">Masă</th>
                            <th class="text-center w120">Transport</th>
                            <th class="text-center w140">Tarif de la</th>
                            <th class="text-center w100">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($oferte as $key_of => $value_of) {
                            $c++;
                            if ($c % 2 == 1) {
                                $class_tr = 'bkg-grey';
                            } else {
                                $class_tr = 'bkg-white';
                            }
                            $link = make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $value_of['denumire_scurta'], $value_of['id_oferta']);
                            $tipuri = explode('+', $value_of['poza_tip']);
                            if ($value_of['taxa_aeroport'] == 'da') {
                                $taxa_aeroport = ' - <span class="red" title="Taxe aeroport INCLUSE">Taxe <strong>INCLUSE</strong></span>';
                                $taxa_aeroport2 = ' <img src="/images/dificultate_partie_i.gif" alt="Taxe aeroport INCLUSE" title="Taxe aeroport INCLUSE" style="vertical-align:middle;" width="14" />';
                            } else {
                                $taxa_aeroport = '';
                                $taxa_aeroport2 = '';
                            }
                            if ($value_of['nr_nopti'] > 0) {
                                $zile = $value_of['nr_nopti'];
                                if ($value_of['nr_nopti'] > 1) {
                                    $zile = '' . $zile . ' nopți';
                                } else {
                                    $zile = 'Pe noapte';
                                }
                            }
                            ?>
                            <tr class="<?php echo $class_tr; ?>">
                                <td class="text-left bigger-12em" style="padding-left:30px;"><a href="<?php echo $link; ?>"
                                                                                                title="<?php echo $value_of['denumire']; ?>"
                                                                                                class="link-red"><?php echo '<strong class="bigger-11em">' . $value_of['denumire_scurta'] . '</strong> ' . $taxa_aeroport; ?></a>
                                </td>
                                <td class="text-center"><?php echo $zile; ?></td>
                                <td class="text-center"><?php echo $value_of['masa']; ?></td>
                                <td class="text-center"><?php echo $value_of['denumire_transport'] . ' ' . $taxa_aeroport2; ?></td>
                                <td class="text-center bold"><span class="pret red"><?php if (!$value_of['pret_minim']) {
                                            $pret = pret_minim_sejur($value_of['id_oferta'], '', '', '');
                                            if ($pret['pret']) {
                                                echo $pret['pret'] . ' ';
                                                echo moneda($pret['moneda']);
                                            }
                                        } else {
                                            echo $value_of['pret_minim'] . ' ';
                                            echo moneda($value_of['moneda']);
                                        } ?></span> <?php echo $value_of['exprimare_pret']; ?></td>
                                <td class="text-right"><a href="<?php echo $link; ?>" title="<?php echo $value_of['denumire']; ?>"><img src="/images/but_nou_detalii.png"
                                                                                                                                        alt="<?php echo $value_of['denumire']; ?>"/></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <br class="clear"><br>
            <?php }

        } ?>

        <div id="detalii-hotel">

            <h2 class="blue underline">Descriere <?php echo $detalii_hotel['denumire']; ?></h2>

            <?php
            if ($detalii_hotel['distanta']) {
                echo '<p class="bold bigger-11em" style="padding:5px 10px 0 10px;">';
                echo '<img src="' . $imgpath . '/icon_dist_' . $detalii_hotel['distanta_fata_de'] . '.jpg" alt="" title="' . $detalii_hotel['distanta'] . 'm de ' . $detalii_hotel['distanta_fata_de'] . '" class="NEW-round6px float-left" style="margin:0 10px 0 0;" />';
                echo '<span class="blue" style="line-height:32px;">' . $detalii_hotel['denumire'] . '</span> este situat la <span class="bigger-11em red">' . $detalii_hotel['distanta'] . 'm de ' . $detalii_hotel['distanta_fata_de'] . '</span>';
                echo '</p><br class="clear">';
            }
            ?>

            <?php if ($detalii_hotel['new_descriere']) { ?>

                <div class="section" style="margin:0;">
                    <?php if ($detalii_hotel["comentariul_nostru"]) { ?>
                        <p class="black"><strong><u>Comentariul nostru</u>:</strong> <?php afisare_frumos(schimba_caractere($detalii_hotel["comentariul_nostru"]), ''); ?>
                        </p>
                        <br>
                    <?php } ?>
                    <?php afisare_frumos(schimba_caractere($detalii_hotel["new_descriere"]), 'nl2p'); ?>
                    <div id="detHotNew-container">
                        <?php if ($detalii_hotel["new_camera"]) { ?>
                            <div class="detHotelNew2">
                                <div class="titlu">Cameră</div>
                                <?php afisare_frumos(schimba_caractere($detalii_hotel["new_camera"]), 'NEW_nl2li'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($detalii_hotel["new_teritoriu"]) { ?>
                            <div class="detHotelNew2">
                                <div class="titlu">Teritoriu</div>
                                <?php afisare_frumos(schimba_caractere($detalii_hotel["new_teritoriu"]), 'NEW_nl2li'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($detalii_hotel["new_relaxare"]) { ?>
                            <div class="detHotelNew2">
                                <div class="titlu">Relaxare și sport</div>
                                <?php afisare_frumos(schimba_caractere($detalii_hotel["new_relaxare"]), 'NEW_nl2li'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($detalii_hotel["new_pentru_copii"]) { ?>
                            <div class="detHotelNew2">
                                <div class="titlu">Pentru copii</div>
                                <?php afisare_frumos(schimba_caractere($detalii_hotel["new_pentru_copii"]), 'NEW_nl2li'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($detalii_hotel["new_plaja"]) { ?>
                            <div class="detHotelNew2">
                                <div class="titlu">Plajă</div>
                                <?php afisare_frumos(schimba_caractere($detalii_hotel["new_plaja"]), 'NEW_nl2li'); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            <?php } else { ?>

            <div class="clearfix">
                <?php if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/harti_explicative/localitati/' . $detalii_hotel['locatie_id'] . '.jpg')) { ?>
                <div class="harta NEW-round8px float-right mar10">
                    <h3 class="text-center">Hartă <span class="green">Hoteluri <?php echo $detalii_hotel['localitate']; ?></span></h3>
                    <div class="inner">
                        <a href="<?php echo $imgpath . '/harti_explicative/localitati/' . $detalii_hotel['locatie_id'] . '.jpg'; ?>"
                           title="Harta Hoteluri <?php echo $detalii_hotel['localitate']; ?>"
                           class="harti_explicative"><img src="<?php echo $imgpath . '/harti_explicative/localitati/' . $detalii_hotel['locatie_id'] . '_m.jpg'; ?>"
                                                          alt="<?php echo $detalii_hotel['localitate']; ?>"
                                                          width="282"
                                                          class="map"/></a>
                        <div class="text-center">click pe poză pentru mărire</div>
                    </div>
                </div>
                <div class="float-left" style="width:660px;">
                    <?php } else { ?>
                    <div>
                        <?php } ?>
                        <div class="pad10">
                            <?php afisare_frumos(schimba_caractere($detalii_hotel["descriere_scurta"]), 'nl2p');
                            afisare_frumos(schimba_caractere($detalii_hotel["descriere"]), 'nl2p'); ?>
                            <?php if ($detalii_hotel['fisier_upload']) { ?><p><span class="blue icon-coffee"></span>
                                <a href="/uploads/hotel/<?php echo $detalii_hotel['fisier_upload']; ?>"
                                   target="_blank"
                                   rel="nofollow"
                                   class="link-blue"><strong class="blue">Concept <?php echo $detalii_hotel['denumire']; ?></strong></a></p><?php } ?>
                            <?php if ($detalii_hotel['website'] and $detalii_hotel['tara'] != 'Romania') { ?><p><span class="blue icon-link"></span>
                                <a href="<?php echo hotel_link($detalii_hotel['website']); ?>" target="_blank" rel="nofollow" class="link-blue"><strong class="blue">Site
                                        oficial <?php echo $detalii_hotel['denumire']; ?></strong>: <?php echo hotel_link($detalii_hotel['website']); ?></a></p><?php } ?>
                        </div>
                    </div>
                </div>

                <div class="section" style="margin:0;">
                    <?php if ($detalii_hotel["general"] || $detalii_hotel["servicii"] || $detalii_hotel["internet"] || $detalii_hotel["parcare"]) { ?>
                        <?php if ($detalii_hotel["general"]) { ?>
                            <h3 class="black">General</h3>
                            <div class="content"><?php afisare_frumos($detalii_hotel["general"], 'nl2br'); ?></div>
                            <div class="clear-line"></div>
                        <?php }
                        if ($detalii_hotel["servicii"]) { ?>
                            <h3 class="black">Servicii</h3>
                            <div class="content"><?php afisare_frumos($detalii_hotel["servicii"], 'nl2br'); ?></div>
                            <div class="clear-line"></div>
                        <?php }
                        if ($detalii_hotel["internet"]) { ?>
                            <h3 class="black">Internet</h3>
                            <div class="content"><?php afisare_frumos($detalii_hotel["internet"], 'nl2br'); ?></div>
                            <div class="clear-line"></div>
                        <?php }
                        if ($detalii_hotel["parcare"]) { ?>
                            <h3 class="black">Parcare</h3>
                            <div class="content"><?php afisare_frumos($detalii_hotel["parcare"], 'nl2br'); ?></div>
                            <div class="clear-line"></div>
                        <?php } ?>
                        <?php if (($detalii_hotel["check_in"]) or ($detalii_hotel["check_out"])) { ?>
                            <h3 class="black">Check-in / check-out</h3>
                            <div class="content"><?php echo $detalii_hotel["check_in"]; ?> / <?php echo $detalii_hotel["check_out"]; ?></div>
                            <div class="clear-line"></div>
                        <?php }
                        if ($detalii_hotel["info_copii"]) { ?>
                            <h3 class="black">Copii și paturi suplimentare</h3>
                            <div class="content"><?php afisare_frumos($detalii_hotel["info_copii"], 'nl2br'); ?></div>
                            <div class="clear-line"></div>
                        <?php }
                        if ($detalii_hotel["accepta_animale"]) { ?>
                            <h3 class="black">Animale companie</h3>
                            <div class="content"><?php afisare_frumos($detalii_hotel["accepta_animale"], 'nl2br'); ?></div>
                            <div class="clear-line"></div>
                        <?php } ?>
                        <div class="pad10 italic grey smaller-09em">Clasificarea pe stele a unităților de cazare din program este cea atribuită în mod oficial de
                            Ministerul Turismului din <?php echo $detalii_hotel['tara']; ?> conform standardelor proprii.
                        </div>
                        <br class="clear"><br>
                    <?php } ?>
                </div>
                <?php }
                $sel_detcam = "SELECT camere_hotel.detalii, tip_camera.denumire FROM camere_hotel INNER JOIN tip_camera ON tip_camera.id_camera = camere_hotel.id_camera WHERE id_hotel='" . $detalii['id_hotel'] . "' AND (detalii<>NULL OR detalii<>'')";
                $que_detcam = mysql_query($sel_detcam) or die(mysql_error());
                if (mysql_num_rows($que_detcam) > '0') {
                    ?>
                    <h2 class="blue underline">Detalii camere <?php echo $detalii_hotel['denumire'] ?></h2>
                    <div class="section">
                        <?php while ($row_detcam = mysql_fetch_array($que_detcam)) {
                            echo '<h3 class="black">' . $row_detcam['denumire'] . '</h3>';
                            echo '<div class="content">' . nl2br($row_detcam['detalii']) . '</div>';
                            echo '<div class="clear-line"></div>';
                        } ?>
                    </div>
                <?php } ?>

            </div>
            <br class="clear">
            <div id="harta">
                <h2 class="blue underline">Localizare pe hartă <?php echo $detalii_hotel['denumire']; ?></h2>
                <?php //include_once __DIR__ . '/../harta-hotel.php'; ?>
                <div class="text-center smaller-09em grey">Poziționarea hotelurilor pe hartă este cu titlu informativ.</div>
            </div>
            <br class="clear"><br>
            <div id="comentarii">
                <h2 class="blue underline">Impresii <?php echo $detalii_hotel['denumire']; ?></h2>
                <?php include_once __DIR__ . '/../hoteluri/reviews.php'; ?>
            </div>
            <br class="clear">
        </div>

    </div>
    <br class="clear">

    <div id="number_visited" class="small-popup-left NEW-round4px black clearfix">
        <i id="close_visited" class="icon-cancel-circled float-right pointer black"></i>
        <div class="content float-left"><span class="bold bigger-11em"><?php echo get_nr_vizite($id_hotel); ?></span> persoane au fost interesate de acest hotel.</div>
    </div>
    <script>
        $(document).ready(function () {
            $("#number_visited:hidden:first").delay(5000).fadeIn(700);
            $("#number_visited").delay(15000).fadeOut(700);
            $("#close_visited").click(function () {
                $("#number_visited").stop().fadeOut(700, removeMessageLayer);
            });
        });
    </script>
