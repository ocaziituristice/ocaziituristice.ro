<div id="NEW-destinatie">
  <h1 class="blue float-left"><?php echo $titlu_pag; ?></h1>
  
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  <br class="clear">
  <div class="Hline"></div>

<?php if(!$_REQUEST['tara']) { ?>
  <div class="pad10">
	<img src="/images/Last-Minute.jpg" alt="Last Minute" class="float-left" style="margin:3px 10px 0 0;">
	<h2>Pleaca in vacanta cat ai spune "last minute". Mai ieftin nu se poate!</h2>
	<p class="text-justify">Cine spunea ca nu e bine sa lasi totul pe ultima suta de metri? Destinatiile mult dorite sunt mai aproape decat crezi daca esti in cautare de <strong>oferte last minute 2014</strong>. Tocmai planuiesti o escapada cu mult soare sau mai racoroasa? Alege-ti una din ofertele last minute si fa-ti bagajele cat mai repede pentru ca, in cel mult 3-4 zile, pleci in <strong>vacanta</strong>! <strong>Agentiile de turism</strong> ti-au pregatit discounturi de nerefuzat pentru  Grecia,  Egipt, Turcia. Preturile pentru <strong>sejururi</strong> pot fi reduse cu pana la 30%! Grabeste-te in vacanta!</p>
    <br class="clear">
  </div>
<?php } ?>

<br class="clear"><br>
  
<div class="NEW-column-right2 clearfix">

  <?php /*?><div class="left clearfix"><?php */?>
  
    <?php //include_once($_SERVER['DOCUMENT_ROOT'].'/includes/sejururi/tematici.php'); ?>
    
    <div class="rez-afisare red">Afisare oferte Last Minute <?php if($nr_stele) { ?> pentru hoteluri de <strong><?php echo $nr_stele; if($nr_stele>1) echo ' stele'; else echo ' stea'; ?></strong><?php } if($id_transport) { ?> cu transport <strong><?php echo ucwords($trans); ?></strong><?php } if($nmasa) { ?> si masa <strong><?php echo ucwords($nmasa); ?></strong> inclusa<?php } if($plecare_avion<>'toate') echo ', plecare din '.ucwords($plecare_avion); ?></div>
  
<?php $link_of_pag=$link; if($_REQUEST['ordonare']) $link=$link."&ordonare=".$_REQUEST['ordonare'];
if($link=='?optiuni=da') $link='';
$oferte=new AFISARE_SEJUR_NORMAL();
$oferte->setAfisare($from, $nr_pe_pagina);
$oferte->config_paginare('nou');
$oferte->setLastMinute();
if($_REQUEST['ordonare']) {
$tipO=explode('-',$_REQUEST['ordonare']);
if($tipO[0]=='tip_pret') $oferte->setOrdonarePret($tipO[1]);
elseif($tipO[0]=='tip_numH') $oferte->setOrdonareNumeH($tipO[1]); }
if($din_luna) $oferte->setLunaPlecare($din_luna);
if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $oferte->setTransport($_REQUEST['transport']);
if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') $oferte->setStele($_REQUEST['stele']);
if($_REQUEST['concept'] && $_REQUEST['concept']<>'toate') $oferte->setConcept($_REQUEST['concept']);
if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') $oferte->setMasa($_REQUEST['masa']);
if($_REQUEST['data_plecarii'] && $_REQUEST['data_plecarii']<>'toate') $oferte->setDataPlecarii($_REQUEST['data_plecarii']);
if($id_loc_plecare_av) $oferte->setPlecareAvion($id_loc_plecare_av);
if(sizeof($ds)==3) $oferte->setDistanta($ds);
$oferte->initializeaza_pagini($link_p, 'pag-###/', $link);
$nr_hoteluri=$oferte->numar_oferte(); ?>
<div class="filter-pag">
    <div class="filter-big">
      <span><strong class="blue">Data plecării: &nbsp; </strong></span>
<?php $selT="SELECT data_pret_oferta.data_start, oferte.id_oferta
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
INNER JOIN data_pret_oferta ON oferte.id_oferta = data_pret_oferta.id_oferta
WHERE
oferte.valabila = 'da'
AND oferte.last_minute = 'da'
AND hoteluri.tip_unitate <> 'Circuit'
AND data_pret_oferta.data_start > NOW()
GROUP BY data_pret_oferta.data_start
ORDER BY data_pret_oferta.data_start ";
$queT=mysql_query($selT) or die(mysql_error());
$link_p1=$link_p.'?optiuni=da';
?>
        <select name="data_plecarii" onchange="filtrare_restu_nou_lm(<?php echo "'".$link_p1; echo "'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, this.value);" class="black">
          <option value="toate" selected="selected">Selectează data plecării</option>
          <?php while($rowP=mysql_fetch_array($queT)) {
              $sf1=denLuniRo(denZileRo(date('d F - l',strtotime($rowP['data_start'])))); ?>
          <option value="<?php echo $rowP['data_start']; ?>" <?php if($_REQUEST['data_plecarii']==$rowP['data_start']) { ?> selected="selected" <?php } ?>><?php echo $sf1; ?></option>
          <?php } @mysql_free_result($queT); ?>
        </select>
      <br />Afisare <strong><?php echo $st=$nr_pe_pagina*($from-1)+1; $sf=$nr_pe_pagina*$from; if($nr_hoteluri<$sf) $sf=$nr_hoteluri; echo '-'.$sf; ?></strong> hoteluri din <strong><?php echo $nr_hoteluri; ?> in total</strong>
    </div>
    <div class="paginatie" align="right"><?php $oferte->printPagini(); ?></div>
  <br class="clear" />
 </div>
 <br class="clear" /><br />
<?php $oferte->afiseaza();
if($nr_hoteluri>0) {
$filtruOf=$oferte->get_filtru_mare();		
$filtruActual=$oferte->get_filtru(); } ?>
	
    <div class="filter-pag">
      <div class="filter">
        Afisare <strong><?php echo $st=$nr_pe_pagina*($from-1)+1; $sf=$nr_pe_pagina*$from; if($nr_hoteluri<$sf) $sf=$nr_hoteluri; echo '-'.$sf; ?></strong> hoteluri din <strong><?php echo $nr_hoteluri; ?> in total</strong><br />
        <span class="blue"><strong>* arata</strong></span>
        <select style="width:50px;" onchange="document.cookie='nr_pe_pagina='+this.value; document.location.href='<?php echo $link_of_pag; ?>';">
          <option <?php if($nr_pe_pagina==10) { ?> selected="selected" <?php } ?> value="10">10</option>
          <option <?php if($nr_pe_pagina==20) { ?> selected="selected" <?php } ?> value="20">20</option>
          <option <?php if($nr_pe_pagina==50) { ?> selected="selected" <?php } ?> value="50">50</option>
        </select>
        <span class="blue"><strong>hoteluri pe pagina</strong></span>
      </div>
      <div class="paginatie" align="right"><?php $oferte->printPagini(); ?></div>
    </div>
    
    <br class="clear" /><br />

</div>
<div class="NEW-column-left2">
  <?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/sejururi/filtru_stanga_last.php'); ?>
</div>
<br class="clear" />
</div>
<br class="clear" />
