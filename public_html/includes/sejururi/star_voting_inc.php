<?php
if(!$id_tara) $id_tara_vote = 0; else $id_tara_vote = $id_tara;
if(!$id_zona) $id_zona_vote = 0; else $id_zona_vote = $id_zona;
if(!$id_localitate) $id_localitate_vote = 0; else {
	if($id_tara==1 or $id_tara==3) $id_localitate_vote = $id_localitate;
	else $id_localitate_vote = 0;
}
if(!$id_tip) $id_tip_vote = 0; else $id_tip_vote = $id_tip;

$richsnippets = 'yes';
if(isset($id_localitate) and $id_tara!=1 and $id_tara!=3 and $id_tara!=8) $richsnippets = 'no';

$vote = 0;
$ip_list = array();

$sel_votes = "SELECT vot, ip FROM star_voting
WHERE 1=1
AND id_tara = '".$id_tara_vote."'
AND id_zona = '".$id_zona_vote."'
AND id_localitate = '".$id_localitate_vote."'
AND id_tip_oferta = '".$id_tip_vote."' ";
$que_votes = mysql_query($sel_votes) or die(mysql_error());
if(mysql_num_rows($que_votes)>0) {
	$nr_votes = mysql_num_rows($que_votes);
	while($row_votes = mysql_fetch_array($que_votes)) {
		$vote += $row_votes['vot'];
		$ip_list[] = $row_votes['ip'];
	}
	$nota = $vote / $nr_votes;
}

if(in_array($_SERVER['REMOTE_ADDR'], $ip_list)) $same_person = 'yes'; else $same_person = 'no';

$cuvant_cheie = '';
if($id_tip_vote!=0) $cuvant_cheie .= ' '.get_den_tip_oferta($id_tip_vote);
if($id_localitate_vote!=0 and $id_zona_vote!=0 and $id_tara_vote!=0) $cuvant_cheie .= ' '.get_den_localitate($id_localitate_vote);
else if($id_localitate_vote==0 and $id_zona_vote!=0 and $id_tara_vote!=0) $cuvant_cheie .= ' '.get_den_zona($id_zona_vote);
else if($id_localitate_vote==0 and $id_zona_vote==0 and $id_tara_vote!=0) $cuvant_cheie .= ' '.get_den_tara($id_tara_vote);

?>
<div id="rating" class="clearfix bigger-11em black float-left" <?php if($richsnippets=='yes') { ?>itemscope itemtype="http://data-vocabulary.org/Review-aggregate"<?php } ?> <?php /*?>style="margin:0 0 15px 5px;"<?php */?>>
  <div class="float-left pad5" style="padding-left:0 !important;">Cât de mult îţi place <span itemprop="itemreviewed" class="bold"><?php echo $cuvant_cheie; ?></span>?</div>
  <form name="setrating" id="setrating" method="post" class="float-left" <?php /*?>style="margin-left:10px;"<?php */?>>
    <?php for($i1=1; $i1<=5; $i1++) {
    	echo '<input name="rating" class="rating" type="radio" value="'.$i1.'"';
        if(round($nota)==$i1) echo ' checked';
        if($err_logare_admin) { if($same_person=='yes') echo ' disabled'; }
        echo '>';
    } ?>
  </form>
  <div class="float-left" style="margin:5px 0 0 7px;">
	<?php if(mysql_num_rows($que_votes)>0) {
		if($richsnippets=='no') { ?>
    <span class="blue">(Scor: <span class="bold"><?php echo round($nota,2); ?></span> / <span class="bold"><?php echo $nr_votes; ?></span> voturi)</span>
        <?php } else if($richsnippets=='yes') { ?>
    <span class="blue" itemprop="rating" itemscope itemtype="http://data-vocabulary.org/rating">(Scor: <span itemprop="average" class="bold"><?php echo round($nota,2); ?></span><span class="hide" itemprop="best">5</span><span class="hide" itemprop="worst">1</span> / <span itemprop="votes" class="bold"><?php echo $nr_votes; ?></span> voturi)</span>
	<?php }
	} else { ?>
    <span class="grey italic">(click pe stele pentru a vota)</span>
    <?php } ?>
  </div>
  <?php if($same_person=='yes') { ?><br class="clear"><span class="grey italic">Ai votat deja. îţi mulţumim pentru interesul acordat.</span><?php } ?>
</div>
<?php /*?><div class="float-right clearfix"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
<br class="clear"><?php */?>
