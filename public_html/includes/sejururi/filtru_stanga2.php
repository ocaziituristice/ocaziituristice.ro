<br>

<?php $_SESSION['page_back'] = $_SERVER['REQUEST_URI']; ?>

<?php if(!isset($check_link)) { ?>

<?php if(($tara!='') and ($zona=='') and ($oras=='') and ($den_tara!='Dubai')) { ?>
  <ul class="sf-menu sf-vertical">
    <li class="primul black">Destinatii pentru <strong><?php echo $cuvant_cheie; ?></strong></li>
	<?php $zone=get_zone($iduri,'','',$id_tara,$early);
	if(sizeof($zone)>0) {
		foreach($zone as $id_zona1=>$value) {
			$link_z='/'.$indice.fa_link($tara).'/'.fa_link($value).'/'; ?>
    <li><a href="<?php echo $link_z; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $value; ?></a>
      <?php $loc_f=get_localitate($iduri,'',$id_zona1,$id_tara,$early);
	  if(sizeof($loc_f)>0) { ?>
      <ul>
	    <?php foreach($loc_f as $kloc_tf=>$loc_tf) {
		$link_lo=fa_link($loc_tf['denumire']); ?>
        <li><a href="<?php echo $link_z.$link_lo.'/'; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $loc_tf['denumire']; ?></a></li>
        <?php } ?>
      </ul>
      <?php } ?>
    </li>
    <?php }
	} ?>
  </ul>
<?php } ?>

<?php if(($tara!='') and ($zona!='') and ($den_tara!='Dubai')) { ?>
  <ul class="sf-menu sf-vertical">
    <li class="primul black">Destinatii pentru <strong><?php echo ucwords($tip).' '.$den_zona; ?></strong></li>
	<?php $link_z='/'.$indice.fa_link($tara).'/'.fa_link($zona).'/';
	$loc_f=get_localitate($iduri,'',$id_zona,$id_tara,$early);
    if(sizeof($loc_f)>0) {
		foreach($loc_f as $kloc_tf=>$loc_tf) {
			$link_lo='/cazare-'.fa_link($loc_tf['denumire']); ?>
    <li class="second"><?php if(strtolower($oras)==strtolower($loc_tf['denumire'])) { echo '<strong class="current">'.$loc_tf['denumire'].'</strong>'; } else { ?><a href="<?php echo $link_lo.'/'; if($early) echo '?optiuni=da&early-booking=da'; ?>"><?php echo $loc_tf['denumire']; ?> <?php /*echo '('.$loc_tf['count_hotel'].')';*/ ?></a><?php } ?></li>
    <?php }
	} ?>
  </ul>
<?php } ?>

<?php } //if(!isset($check_link)) ?>

<div class="filters NEW-round6px">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/popup_filters.php"); ?>

  <div class="titlu blue">Filtrare dupa:-</div>

<?php if($_REQUEST['data-plecare'] || $id_transport || $nr_stele || $nmasa || $nconcept || $nfacilitati || $din_luna || ($distanta && $distanta<>'toate') || $_GET['early-booking']=='da' || $checkin) { ?>
  <a href="<?php echo $link_p; ?>" class="delete" rel="nofollow" onclick="setCookie('grad_ocupare','',-1);"><span>X</span> Sterge toate filtrele</a>
  <ul class="delete-filters black">
    <?php
    if($checkin) echo '<li>check-in <strong>'.date("d.m.Y", strtotime($checkin)).'</strong></li>';
    //if($_REQUEST['data-plecare']) echo '<li>plecare '.$_REQUEST['data-plecare'].'</li>';
	if($early) echo '<li>early booking</li>';
    if($din_luna) echo '<li>'.$plecare.'</li>';
    if($id_transport) echo '<li>'.$trans.'</li>';
    if($nr_stele) echo '<li>'.$nr_stele.' stele</li>';
    if($nmasa) echo '<li>'.$nmasa.'</li>';
    if($distanta && $distanta<>'toate') echo '<li>'.$distanta.' m</li>';
    if($nconcept) echo '<li>'.$nconcept.'</li>';
    if($nfacilitati) echo '<li>'.$nfacilitati.'</li>';
	?>
  </ul>
  <br>
<?php } ?>
  
<?php if(!isset($_GET['nume_hotel'])) { ?>

<?php $tipuri=get_tiputi_oferte('', $id_localitate, $id_zona, $id_tara, '', ''); $i=0;
  if(sizeof($tipuri)>0 and !isset($check_link)) {
	  $link_new=$link_tara.'/';
	  if($link_zona) $link_new.=$link_zona.'/';
	  if($link_oras) {$link_new.=$link_oras.'/';
	  $link_toate='/cazare-'.$link_oras;}
	  
	  if($link_tara) $den_new=$den_tara;
	  if($link_zona) $den_new=$den_zona;
	  if($link_oras) $den_new=$den_oras;
  ?>
  <div class="item NEW-round6px">
  <div class="heading">Ocazii speciale</div>
  <ul>
    <li class="label">
    <a href="<?php echo '/sejur-'.$link_new; ?>" <?php if(!$_REQUEST['tip']) echo 'class="sel"'; ?> rel="nofollow">Toate</a></li>
    <?php $i=0; foreach($tipuri as $key_t=>$value_t) { $i++;
		$valTip=fa_link($value_t['denumire']);
		if($value_t) $nrO=$value_t; else $nrO=0;
	?>
    <li class="label"><a href="<?php echo '/oferte-'.$valTip.'/'.$link_new; ?>" <?php if($valTip==$_REQUEST['tip']) echo 'class="sel"'; ?> title="<?php echo $value_t['denumire']." ".$den_new; ?>" rel="nofollow"><?php echo $value_t['denumire']." ".$den_new; ?> <?php /*echo '('.$nrO.')';*/ ?></a></li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>
  
<?php $link_p1=$link_p.'?optiuni=da'; ?>

<?php if(($link_tara) and ($nr_hoteluri>0)) { ?>
  <div class="item NEW-round6px">
    <div class="heading">Data Check-in</div>
    <label><input type="text" name="checkin" value="<?php if($checkin) echo date("d.m.Y",strtotime($checkin)); ?>" id="CheckIn" onchange="set_grad_ocupare(this.value, '', '', '', '', '', '', '', '', ''); document.location.href='<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $Rconcept, $Rfacilitati, TRUE); ?>'+this.value; <?php /*?>setCookie('CheckIn',''+this.value+'',10);<?php */?>" class="calendar bigger-12em NEW-round4px"> <img src="/images/icon_calendar_green.png" alt="Check-in" style="vertical-align:middle;"></label>
  </div>
<?php } ?>

<?php /*if(($link_tara) and ($nr_hoteluri>0)) { ?>
  <div class="item NEW-round6px">
    <div class="heading">Inceputul sejurului</div>
    <select name="luni_plecari" id="luni_plecari" onchange="document.location.href='<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $Rconcept, $Rfacilitati, TRUE); ?>'+this.value;">
      <option value="">Alege inceperea sejurului</option>
      <?php if(sizeof($luni_plecari)>0) {
	   foreach($luni_plecari as $value) {
		$lun=explode('-', $value);
		$link_z=$value; ?>
      <option value="<?php echo $link_z; ?>" <?php if($link_z==$din_luna) { ?> selected="selected" <?php } ?>><?php echo $luna[$lun[0]].' '.$lun[1]; ?></option>
      <?php }
	  } ?>
    </select>
  </div>
<?php }*/ ?>

<?php /*?><?php $link_p1=$link_p;
  if(!$_REQUEST['data-plecare'])  $link_p1=$link_p1.'?optiuni=da'; else $link_p1=$link_p1.'?data-plecare='.$_REQUEST['data-plecare']; ?><?php */?>
  
  <?php /*?><?php if($filtruOf['early_booking']>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Reduceri</div>
  <ul>
    <li class="label"><a href="<?php echo '/sejur-'.$link_tara.'/'; if($link_zona) echo $link_zona.'/'; if($link_oras) echo $link_oras.'/'; ?>" <?php if(!$_GET['early-booking']) echo 'class="sel"'; ?> rel="nofollow">Toate</a></li>
    <li class="label"><a href="<?php echo $link_p1.'&amp;early-booking=da'; ?>" <?php if($_GET['early-booking']=='da') echo 'class="sel"'; ?>>Early Booking</a></li>
  </ul>
  </div>
  <?php } ?><?php */?>

  <?php if($_GET['early-booking']) $link_p1=$link_p1.'&early-booking=da'; if(sizeof($filtruOf['trans'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Transport</div>
  <?php krsort($filtruOf['trans']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, 'toate', $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($transport=='toate') echo 'class="sel"'; ?> onclick="set_grad_ocupare('', '', '', '', '', '', '', '', '0', '51');">Toate</a></li>
    <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) {
		$i++;
		$lingTr=fa_link($key);
		if($value) $nr=$value; else $nr=0;
		if($transport==$lingTr) {
			$tr=$lingTr;
			$selclass='class="sel"';
		} else {
			$tr=$lingTr;
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $tr, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow" onclick="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', '51');"><?php echo $key; ?> <?php //echo '('.$nr.')'; ?></a>
      <?php if($key=='Avion') { ?>
      <select name="oras_plecare" onchange="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', this.value); filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, this.options[this.selectedIndex].innerHTML.toLowerCase(), <?php echo "'".$plecare_autocar."'"; ?>, <?php echo "'".$Rconcept."'"; ?>);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_avion'])) {
			krsort($filtruOf['plecare_avion']);
			foreach($filtruOf['plecare_avion'] as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo get_id_localitate($lingTr); ?>" <?php if($lingTr==$plecare_avion || ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
      <?php if($key=='Autocar') { ?>
      <select name="oras_plecare" onchange="set_grad_ocupare('', '', '', '', '', '', '', '', '<?php echo get_id_transport($key); ?>', this.value); filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'autocar', <?php echo "'".$nr_stele."'"; ?>, <?php echo "'".$Rmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, this.options[this.selectedIndex].innerHTML.toLowerCase(), <?php echo "'".$Rconcept."'"; ?>); setCookie('transportAutocar', this.options[this.selectedIndex].innerHTML.toLowerCase(), 30);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_autocar'])==0) echo 'disabled="disabled"'; if($plecare_autocar=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_autocar'])) {
			//arsort($filtruOf['plecare_autocar']);
			foreach($filtruOf['plecare_autocar'] as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo get_id_localitate($lingTr); ?>" <?php if($lingTr==$plecare_autocar || ($transport=='autocar' && sizeof($filtruOf['plecare_autocar'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['stele'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Categorie Hotel</div>
  <?php krsort($filtruOf['stele']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, 'toate', $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($stele=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php foreach($filtruOf['stele'] as $i=>$nr_s) {
		if($nr_s) $nrS=$nr_s; else $nrS=0;
		if(is_array($stele)) {
			if(in_array($i,$stele)) {
				$st=remove_current_filter($nr_stele,$i);
				$selclass='class="sel"';
			} else {
				$st=$nr_stele.','.$i;
				$selclass='';
			}
		} else {
			$st=$i;
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $st, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $i; if($i>1) echo ' stele '; else echo ' stea '; ?> <?php echo '('.$nrS.')'; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['masa'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Tip masa</div>
  <?php ksort($filtruOf['masa']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, 'toate', $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($masa=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['masa'] as $keyM=>$valueM) {
		foreach($valueM as $denM=>$nr_m) {
			$i++;
			if($nr_m) $nrM=$nr_m; else $nrM=0;
			if(is_array($Lmasa)) {
				if(in_array(fa_link($denM),$Lmasa)) {
					$lingM=remove_current_filter($Rmasa,fa_link($denM));
					$selclass='class="sel"';
				} else {
					$lingM=$Rmasa.','.fa_link($denM);
					$selclass='';
				}
			} else {
				$lingM=fa_link($denM);
				$selclass='';
			}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $lingM, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $denM; ?> <?php echo '('.$nrM.')'; ?></a></li>
    <?php }
	} ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['facilitati'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Facilitati</div>
  <?php krsort($filtruOf['facilitati']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, 'toate', $checkin); ?>" rel="nofollow" <?php if($facilitati=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['facilitati'] as $denF=>$nr_f) { $i++;
		if($nr_f) $nrF=$nr_f; else $nrF=0;
		if(is_array($Lfacilitati)) {
			if(in_array(fa_link($denF),$Lfacilitati)) {
				$lingF=remove_current_filter($Rfacilitati,fa_link($denF));
				$selclass='class="sel"';
			} else {
				$lingF=$Rfacilitati.','.fa_link($denF);
				$selclass='';
			}
		} else {
			$lingF=fa_link($denF);
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $Rconcept, $lingF, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $denF; ?> <?php echo '('.$nrF.')'; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>

  <?php if(in_array($zona, $zone_filtru_distanta) && sizeof($filtruOf['distanta'])) {
	  foreach($filtruOf['distanta'] as $fata_de=>$value) {
		  ksort($value); ?>
  <div class="item NEW-round6px">
  <div class="heading">Distanta fata de <?php echo $fata_de; ?></div>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, 'toate', $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($distanta=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($value as $key1=>$value2) { $i++;
		if($value2) $nrD=$value2; else $nrD=0;
		$val_dist=$fata_de.'-'.$inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2];
		if($distanta==$val_dist) {
			$selclass='class="sel"';
		} else {
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $val_dist, $plecare_avion, $plecare_autocar, $Rconcept, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php if($key1==1) echo '<'.$inteval_distanta[1][2]; elseif($key1<4) echo $inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; elseif($key1==4) echo '>'.$inteval_distanta[4][1]; echo ' m'; ?> <?php echo '('.$nrD.')'; ?></a></li>
    <?php }
	 } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['concept'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Recomandat pentru</div>
  <?php krsort($filtruOf['concept']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, 'toate', $Rfacilitati, $checkin); ?>" rel="nofollow" <?php if($concept=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['concept'] as $denC=>$nr_c) { $i++;
		if($nr_c) $nrC=$nr_c; else $nrC=0;
		if(is_array($Lconcept)) {
			if(in_array(fa_link($denC),$Lconcept)) {
				$lingC=remove_current_filter($Rconcept,fa_link($denC));
				$selclass='class="sel"';
			} else {
				$lingC=$Rconcept.','.fa_link($denC);
				$selclass='';
			}
		} else {
			$lingC=fa_link($denC);
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters($link_p1, $transport, $nr_stele, $Rmasa, $distanta, $plecare_avion, $plecare_autocar, $lingC, $Rfacilitati, $checkin); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $denC; ?> <?php echo '('.$nrC.')'; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>

<?php }//end if(!isset($_GET['nume_hotel'])) ?>

</div>

<br class="clear"><br>

<?php
$seo_links = array();
$j=0;
if($den_tara=='Dubai') {
	$den_zona = 'Dubai';
	$id_zona = get_id_zona($den_zona, $id_tara);
}
if($id_zona) {
	$den_destinatie = get_den_zona($id_zona);
	$check_weather = check_weather($id_tara, $id_zona);
	if($check_weather==1) {
		$seo_links[$j]['denumire'] = 'Vremea '.$den_destinatie;
		$seo_links[$j]['title'] = 'Vremea in '.$den_destinatie;
		$seo_links[$j]['link'] = '/vremea-in-'.fa_link($den_destinatie).'_'.$id_tara.'.html';
		$j++;
	}
	$check_articles = check_articles($id_tara, $id_zona);
	if($check_articles==1) {
		$seo_links[$j]['denumire'] = 'Obiective turistice '.$den_destinatie;
		$seo_links[$j]['title'] = 'Obiective turistice si informatii utile '.$den_destinatie;
		$seo_links[$j]['link'] = '/ghid-turistic-'.fa_link($den_destinatie).'/';
		$j++;
	}
	$articole_principale = articles_side_menu($id_tara, $id_zona);
	if(count($articole_principale)>0) {
		foreach($articole_principale as $k_articles => $v_articles) {
			$seo_links[$j]['denumire'] = $v_articles['denumire'];
			$seo_links[$j]['title'] = $v_articles['denumire'];
			$seo_links[$j]['link'] = '/ghid-turistic-'.$v_articles['link_area'].'/'.$v_articles['link'].'.html';
			$j++;
		}
	}
}
//echo $id_localitate;
if($id_localitate) {
	
	$den_localitate = get_den_localitate($id_localitate);
	$den_destinatie=$den_localitate;
	//$check_weather = check_weather($id_tara, $id_zona);
	//if($check_weather==1) {
		//$seo_links[$j]['denumire'] = 'Vremea '.$den_destinatie;
		//$seo_links[$j]['title'] = 'Vremea in '.$den_destinatie;
		//$seo_links[$j]['link'] = '/vremea-in-'.fa_link($den_destinatie).'_'.$id_tara.'.html';
		//$j++;
	//}
	$check_articles = check_articles($id_tara, $id_zona,$id_localitate);
	if($check_articles==1) {
		$seo_links[$j]['denumire'] = 'Obiective turistice '.$den_zona;
		$seo_links[$j]['title'] = 'Obiective turistice si informatii utile '.$den_zona;
		$seo_links[$j]['link'] = '/ghid-turistic-'.fa_link($den_zona).'/';
		$j++;
	}
	$articole_principale = articles_side_menu($id_tara, $id_zona,$id_localitate);
	if(count($articole_principale)>0) {
		foreach($articole_principale as $k_articles => $v_articles) {
			$seo_links[$j]['denumire'] = $v_articles['denumire'];
			$seo_links[$j]['title'] = $v_articles['denumire'];
			$seo_links[$j]['link'] = '/ghid-turistic-'.$v_articles['link_area'].'/'.$v_articles['link'].'.html';
			$j++;
		}
	}
}



if($den_tara=='Bulgaria') {
	$seo_links[$j]['denumire'] = 'Transport Bulgaria';
	$seo_links[$j]['title'] = 'Transport Bulgaria';
	$seo_links[$j]['link'] = '/transport-bulgaria.html';
	$j++;
}
if($den_tara=='Grecia') {
	if($den_zona=='Insula Thassos') {
		$seo_links[$j]['denumire'] = 'Transport Insula Thassos';
		$seo_links[$j]['title'] = 'Transport Insula Thassos';
		$seo_links[$j]['link'] = '/transport-thassos.html';
		$j++;
	}
	if($den_zona=='Paralia Katerini') {
		$seo_links[$j]['denumire'] = 'Transport Paralia Katerini';
		$seo_links[$j]['title'] = 'Transport Paralia Katerini';
		$seo_links[$j]['link'] = '/transport-paralia-katerini.html';
		$j++;
	}
}

if(count($seo_links)>0) {
?>
<div class="filters NEW-round6px chn-blue pad0">
  <div class="item NEW-round6px mar0">
    <div class="heading-blue mar0 white NEW-round4-0px"><i class="icon-info-circled white"></i> Info <?php echo $den_destinatie; ?></div>
    <?php if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/seo_links/".$id_zona.".jpg")) echo '<div class="picture"><img src="/images/seo_links/'.$id_zona.'.jpg" alt="'.$den_destinatie.'" class="w-full"></div>'; ?>
    <div class="articles">
      <?php /*?><?php if($den_destinatie!=$den_tara) { ?><a href="<?php echo $link_tara; ?>" title="Sejur <?php echo $den_tara; ?>">Sejur <?php echo $den_tara; ?></a><?php } ?>
      <a href="<?php echo $link_destinatie; ?>" title="Sejur <?php echo $den_destinatie; ?>">Sejur <?php echo $den_destinatie; ?></a><?php */?>
      <?php foreach($seo_links as $key_sl => $value_sl) {
		  echo '<a href="'.$seo_links[$key_sl]['link'].'" title="'.$seo_links[$key_sl]['title'].'">'.$seo_links[$key_sl]['denumire'].'</a>';
	  } ?>
    </div>
  </div>
</div>
<br class="clear"><br>
<?php } ?>

<div class="filters NEW-round6px chn-blue pad0">
  <div class="item NEW-round6px mar0">
    <div class="heading-blue mar0 white NEW-round4-0px"><i class="icon-info-circled white"></i> Parteneri </div>
    <img src="/images/parteneri.jpg" width="198" height="790" alt="Parteneri Ocaziituristice.ro" />

  
    </div>
</div>





<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/star_voting_inc.php"); ?>

<?php /*?><div style="width:210px; height:430px; overflow:hidden; border:1px solid #CCC;" class="NEW-round6px"><div class="fb-like-box" data-href="<?php echo $facebook_page; ?>" data-width="210" data-height="457" data-show-faces="true" data-stream="false" data-show-border="false" data-header="true"></div></div>

<br class="clear"><br><?php */?>

<?php /*?><a href="<?php echo $sitepath; ?>newsletter.html" title="Abonare Newsletter" rel="infoboxes"><img src="<?php echo $imgpath; ?>/banner/abonare-newsletter-small.png" alt="Abonare Newsletter" /></a>

<p>&nbsp;</p><?php */?>

<?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/linkuri_seo.php"); ?>
