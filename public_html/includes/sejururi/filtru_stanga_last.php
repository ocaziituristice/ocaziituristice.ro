<div class="filters NEW-round6px">
  <?php /*?><div class="blue" align="justify">Pentru o navigare mai usoara, folositi <span class="bold">filtrele de mai jos</span>:</div><?php */?>
  
  <div class="titlu blue">Filtrare dupa:</div>

<?php if($_REQUEST['data-plecare'] || $id_transport || $nr_stele || $nmasa || $nconcept || $nfacilitati || ($data_plecarii && $data_plecarii<>'toate') || $din_luna || ($distanta && $distanta<>'toate') || $_GET['early-booking']=='da' || $checkin) { ?>
  <a href="<?php echo $link_p; ?>" class="delete" rel="nofollow"><span>X</span> Sterge toate filtrele</a>
  <ul class="delete-filters black">
    <?php
    if($checkin) echo '<li>check-in <strong>'.date("d.m.Y", strtotime($checkin)).'</strong></li>';
    //if($_REQUEST['data-plecare']) echo '<li>plecare '.$_REQUEST['data-plecare'].'</li>';
	if($early) echo '<li>early booking</li>';
    if($din_luna) echo '<li>'.$plecare.'</li>';
    if($id_transport) echo '<li>'.$trans.'</li>';
    if($nr_stele) echo '<li>'.$nr_stele.' stele</li>';
    if($nmasa) echo '<li>'.$nmasa.'</li>';
    if($distanta && $distanta<>'toate') echo '<li>'.$distanta.' m</li>';
    if($nconcept) echo '<li>'.$nconcept.'</li>';
    if($nfacilitati) echo '<li>'.$nfacilitati.'</li>';
	?>
  </ul>
  <br>
<?php } ?>
  
  <div class="item NEW-round6px">
  <div class="heading">Tara</div>
  <?php $tari_f=get_tari('','','','','',NULL,'da'); ?>
	<ul>
	  <li class="label"><a href="/last-minute/" <?php if(!$den_tara) echo 'class="sel"'; ?> rel="nofollow">Toate ofertele Last Minute</a></li>
	<?php if(sizeof($tari_f)>0) {
		foreach($tari_f as $key_tf=>$value_tf) { ?>
	  <li class="label"><a href="/last-minute/<?php echo fa_link($value_tf); ?>/" <?php if($value_tf==$den_tara) echo 'class="sel"'; ?> title="Last minute <?php echo $value_tf; ?>">Last minute <?php echo $value_tf; ?></a></li>
    	<?php }
	} ?>
	</ul>
  <?php /*?><?php $tari_f=get_tari('','','','','',NULL,'da'); ?>
  <select name="tara" id="tara" onchange="filtrare_get_nou('<?php echo $indice; ?>', this.value, '');">
    <option value="" <?php if(!$den_tara) { ?> selected="selected" <?php } ?>>Selectati</option>
	<?php if(sizeof($tari_f)>0) {
		foreach($tari_f as $key_tf=>$value_tf) { ?>
    	<option value="<?php echo fa_link($value_tf); ?>" <?php if($value_tf==$den_tara) { ?> selected="selected" <?php } ?>><?php echo $value_tf; ?></option>
    	<?php }
	} ?>
  </select><?php */?>
  </div>
  
<?php $link_p1=$link_p;
if(!$_REQUEST['data-plecare']) $link_p1= $link_p1.'?optiuni=da'; else $link_p1= $link_p1.'?data-plecare='.$_REQUEST['data-plecare'];
if(sizeof($filtruOf['trans'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Transport</div>
  <?php krsort($filtruOf['trans']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['trans'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['trans'])==0) echo 'disabled="disabled"'; ?> id="optiune0" name="transport[]" <?php if($transport=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou_lm(<?php echo "'".$link_p1; echo "'"; ?>, this.value, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);" /> <label for="optiune0" <?php if($transport=='toate') echo 'class="sel"'; ?>>Toate</label></li>
    <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) {
		$i++;
		$lingTr=fa_link($key);
		if($value) $nr=$value; else $nr=0; ?>
	<li <?php if(!$filtruActual['trans'][$key]) echo 'class="disabled"'; ?>><input type="checkbox"  <?php if(!$filtruActual['trans'][$key]) echo 'disabled="disabled"'; ?> name="transport[]" id="optiune<?php echo $i; ?>" <?php if($lingTr==$transport) { ?> checked="checked" <?php } ?> value="<?php echo $lingTr; ?>" onclick="if(this.checked) var tr=this.value; else var tr='toate'; filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, tr, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);" /> <label for="optiune<?php echo $i; ?>" <?php if($lingTr==$transport) echo 'class="sel"'; ?>><?php echo $key; ?></label>
    <?php if($key=='Avion') { ?>
    <select name="oras_plecare" onchange="filtrare_restu_nou_lm(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, this.value, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);" class="oras-plecare">
    <option value="toate" <?php if(sizeof($filtruActual['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
    <?php if(sizeof($filtruOf['plecare_avion'])) {
   krsort($filtruOf['plecare_avion']);
   foreach($filtruOf['plecare_avion'] as $key=>$value) {
   $lingTr=fa_link($key); ?>
   <option value="<?php echo $lingTr; ?>" <?php if(!$filtruActual['plecare_avion'][$key]) echo 'disabled="disabled"'; if($lingTr==$plecare_avion || ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
   <?php }
    } ?>
    </select>  
  <?php } ?></li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>
  
<?php if(sizeof($filtruOf['stele'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Categorie Hotel</div>
  <?php krsort($filtruOf['stele']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['stele'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['stele'])==0) echo 'disabled="disabled"'; ?> id="optiunes0" name="stele[]" <?php if($stele=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, this.value, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);" /> <label for="optiunes0" <?php if($stele=='toate') echo 'class="sel"'; ?>>Toate</label></li>
    <?php foreach($filtruOf['stele'] as $i=>$nr_s) {
    if($nr_s) $nrS=$nr_s; else $nrS=0;   ?>
    <li <?php if(!$filtruActual['stele'][$i]) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(!$filtruActual['stele'][$i]) echo 'disabled="disabled"'; ?> id="optiunes<?php echo $i; ?>" name="stele[]" value="<?php echo $i; ?>" <?php if($i==$stele) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var st=this.value; else var st='toate'; filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, st, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);" /> <label for="optiunes<?php echo $i; ?>" <?php if($i==$stele) echo 'class="sel"'; ?>><?php echo $i; if($i>1) echo ' stele '; else echo ' stea '; ?></label></li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>

<?php if(sizeof($filtruOf['masa'])>0) { ksort($filtruOf['masa']); ?>
  <div class="item NEW-round6px">
  <div class="heading">Tip masa</div>
  <ul>
    <li <?php if(sizeof($filtruActual['masa'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['masa'])==0) echo 'disabled="disabled"'; ?> id="optiuneM0" name="masa[]" value="toate" <?php if($masa=='toate') { ?> checked="checked" <?php } ?> onclick="filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, this.value, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);"/> <label for="optiuneM0" <?php if($masa=='toate') echo 'class="sel"'; ?>>Toate</label></li>
    <?php $i=0; foreach($filtruOf['masa'] as $keyM=>$valueM) {
    foreach($valueM as $denM=>$nr_m) { $i++;
    if($nr_m) $nrM=$nr_m; else $nrM=0;
    $lingM=fa_link($denM); ?>
    <li <?php if(!$filtruActual['masa'][$denM]) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(!$filtruActual['masa'][$denM]) echo 'disabled="disabled"'; ?> name="masa[]" id="optiuneM<?php echo $i; ?>" value="<?php echo $lingM; ?>" <?php if($lingM==$Lmasa) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var ms=this.value; else var ms='toate'; filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, ms, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);" /> <label for="optiuneM<?php echo $i; ?>" <?php if($lingM==$Lmasa) echo 'class="sel"'; ?>><?php echo $denM; ?></label></li>
   <?php }
    } ?>
  </ul>
  </div>
<?php } ?>

<?php if(in_array($zona, $zone_filtru_distanta) && sizeof($filtruOf['distanta'])) { 
	foreach($filtruOf['distanta'] as $fata_de=>$value) {
		ksort($value); ?>
  <div class="item NEW-round6px">
  <div class="heading">Distanta fata de <?php echo $fata_de; ?></div>
  <ul>
    <li <?php if(sizeof($filtruActual['distanta'][$fata_de])==0) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(sizeof($filtruActual['distanta'][$fata_de])==0) echo 'disabled="disabled"'; ?> id="optiuneD0" name="distanta[]" value="toate" <?php if($distanta=='toate') { ?> checked="checked" <?php } ?> onclick="filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, this.value, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);"/>
      <label for="optiuneD0" <?php if($distanta=='toate') echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php $i=0; foreach($value as $key1=>$value2) { $i++;
	if($value2) $nrD=$value2; else $nrD=0;
	$val_dist=$fata_de.'-'.$inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; ?>
    <li <?php if(!$filtruActual['distanta'][$fata_de][$key1]) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(!$filtruActual['distanta'][$fata_de][$key1]) echo 'disabled="disabled"'; ?> name="distanta[]" id="optiuneD<?php echo $i; ?>" value="<?php echo $val_dist; ?>" <?php if($distanta==$val_dist) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var ds=this.value; else var ds='toate'; filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, ds, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>, <?php echo "'".$data_plecarii."'"; ?>);" />
      <label for="optiuneD<?php echo $i; ?>" <?php if($distanta==$val_dist) echo 'class="sel"'; ?>>
        <?php if($key1==1) echo '<'.$inteval_distanta[1][2];
	elseif($key1<4) echo $inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; elseif($key1==4) echo '>'.$inteval_distanta[4][1]; echo ' m'; ?>
      </label>
    </li>
    <?php }
	 } ?>
  </ul>
  </div>
<?php } ?>

<?php if(sizeof($filtruOf['concept'])>0) { ?>
  <div class="item NEW-round6px">
  <div class="heading">Recomandat pentru</div>
  <?php krsort($filtruOf['concept']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['concept'])==0) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(sizeof($filtruActual['concept'])==0) echo 'disabled="disabled"'; ?> id="optiuneC0" name="concept[]" <?php if($concept=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, this.value, <?php echo "'".$data_plecarii."'"; ?>);" />
      <label for="optiuneC0" <?php if($concept=='toate') echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php $i=0; foreach($filtruOf['concept'] as $denC=>$nr_c) { $i++;
	  if($nr_c) $nrC=$nr_c; else $nrC=0;
	  $lingC=fa_link($denC); ?>
    <li <?php if(!$filtruActual['concept'][$denC]) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(!$filtruActual['concept'][$denC]) echo 'disabled="disabled"'; ?> id="optiuneC<?php echo $i; ?>" name="concept[]" value="<?php echo $lingC; ?>" <?php if($lingC==$Lconcept) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var cnt=this.value; else var cnt='toate'; filtrare_restu_nou_lm(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, cnt, <?php echo "'".$data_plecarii."'"; ?>);" />
      <label for="optiuneC<?php echo $i; ?>" <?php if($lingC==$Lconcept) echo 'class="sel"'; ?>><?php echo $denC; ?></label>
    </li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>

</div>

<br class="clear"><br>

<?php /*?><div style="width:210px; height:430px; overflow:hidden; border:1px solid #CCC;" class="NEW-round6px"><div class="fb-like-box" data-href="<?php echo $facebook_page; ?>" data-width="210" data-height="457" data-show-faces="true" data-stream="false" data-show-border="false" data-header="true"></div></div>

<br class="clear"><br><?php */?>
