<?php // title and description zone ?>

<div class="sejururi">
	<?php
	if ( $nmasa == 'all inclusive' ) {
		$titlu_pag = 'Oferte All Inclusive ' . $den_zona;
	}
	?>
    <h1 class="main-title">
		<?php if ( $_GET['early-booking'] ) {
			echo '<span class="green">Early Booking</span> ';
		}
		echo $titlu_pag; ?>
		<?php if ( $localitate_plecare != '' ) {
			echo ' <br />Plecare din <span class="green">' . ucwords( $localitate_plecare ) . "</span>";
		}
		
		
		echo '- ' . $nr_hoteluri . ' Hoteluri Disponibile'.scris_stele ($nr_stele); ?>
    </h1>
    <h2 class="main-subtitle green">Comparam preturile pentru <span
                class="textatentie">&nbsp;<?php echo $den_tara ?>&nbsp;</span> de la TourOperatori si va oferim
        <span class="textatentie">cel mai bun Pret!</span>
    </h2>
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/localitati_plecare_avion_responsive.php" ); ?>
	<?php
	if ( $nr_hoteluri > 0 ) {
		include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/sejururi/filtru_location_responsive.php' );
	}
	?>
</div>

<div class="sejururi show-mobile">
    <button class="show-filters">
        <span>Filtreaza</span>
        <span class="hide">Inchide filtre</span>
    </button>
    <script>
        $('.show-filters').on('click', function () {
            $(this).find('span').toggleClass('hide');
            $('.filter-wrapper').toggleClass('opened');
            $('.result-list').toggle('slow');
        });
    </script>
</div>

<?php // filters ?>
<div class="filter-wrapper">
    <div class="sejururi filter-wrapper-div">
        <?php
        if ( $nr_hoteluri > 0 ) {

            include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/sejururi/filtru_ajax_responsive.php' );
        }
        ?>

        <div class="scroll-to-filters">
            <button class="scroll-filter"
                    onclick="$('html, body').animate({scrollTop: jQuery('.filter-wrapper').offset().top - 10}, 'slow');">
                Arata filtre
            </button>
        </div>
        <script>
            $(document).on('scroll', function () {
                if ($(window).width() > 767 && $('.scroll-to-filters').offset().top < $(document).scrollTop()) {
                    $('.scroll-filter').show();
                } else {
                    $('.scroll-filter').hide();
                }
            });
        </script>
    </div>
</div>

<?php // resultes ?>
<div class="sejururi result-list">
		<?php $id_tip;?> 

	<?php $oferte->afiseaza(); ?>
    <div class="item-holder" style="display: none;">
		<?php $oferte->printPagini() ?>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.show-prices-button a', function() {
        var wrapper = $(this).closest('.item-holder');
        var hotel_id = $(this).attr('id').replace('vezipret', '');
        if ( wrapper.find('.cautare_live').val() == 'da' ) {
            $('#hotel' + hotel_id + 'O').empty().html('<div align="center"><br><span class="bigger-11em bold black italic">Vă rugăm așteptați. Ofertele se incarca in timp real.</span><br><img src="/images/loader3.gif" alt="Loading" /></div>');
            setTimeout( function() {
                $('#hotel' + hotel_id + 'O').load(wrapper.find('.loader_link').val());
                }, 500);
        } else {
            $('#H' + hotel_id + 'O').empty().html('<br><span class="bigger-11em bold black italic">Vă rugăm așteptați. Incarcam ofertele</span><br><img src="/images/loader3.gif" alt="">');
            setTimeout( function() {
                $('#H' + hotel_id + 'O').load();
                }, 300);
        }
    });
</script>
<script>
    $(document).ready(function () {
        $('.result-list').jscroll({
            loadingHtml: '<img src="/images/loader3.gif" alt="Loading" /> Se incarca',
            padding: 20,
            debug: true,
            nextSelector: 'a.jscroll-next',
            contentSelector: '.item-holder'
        });
    });
</script>
<div class="clear"></div>

<?php // the most important from the current zone ?>
<div class="sejururi">
	<?php //include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/localitati_zona_sus_responsive.php" ); ?>
</div>

<?php if ( strlen( $zona_detalii['imagine1'] ) > 5 and strlen( $zona_detalii['descriere_zona'] ) > 20 ) { ?>
    <div class="sejururi">
        <h1 class="main-title">
            1Mai multe informatii despre <?php echo $zona_detalii['denumire_zona'] ?>
        </h1>
        <article class="pad10 article bigger-12em text-justify">
            <img src="/img_zona/<?php echo $zona_detalii['imagine1']; ?>"
                 alt="<?php echo $zona_detalii['denumire_zona'] ?>" class="float-left" style="margin:0 20px 0 0;">
			<?php ( $descriere_zona = $zona_detalii['descriere_zona'] );
			$descriere_zona = str_replace( '{imagine2}', '<img src="/img_zona/' . $zona_detalii['imagine2'] . '" alt="' . $zona_detalii['denumire_zona'] . '" class="float-left" style="margin:3px 10px 0 0;">', $descriere_zona );

			$descriere_zona = str_replace( '{imagine3}', '<img src="/img_zona/' . $zona_detalii['imagine3'] . '" alt="' . $zona_detalii['denumire_localitate'] . '" class="float-left" style="margin:3px 10px 0 0;">', $descriere_zona );
			echo $descriere_zona;
			?>
        </article>
       
<?php } ?>
