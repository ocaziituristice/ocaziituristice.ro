<div class="filters NEW-round8px">
  <?php /*?><h2 class="green underline" style="margin:20px 10px 15px 5px;">FILTRARE:</h2><?php */?>
  <?php /*?><div class="blue" align="justify">Pentru o navigare mai usoara, folositi <span class="bold">filtrele de mai jos</span>:</div><?php */?>
  
  <div class="black bold" align="center" style="padding:5px 0 3px 0;">Introduceti numele hotelului</div>
  <div class="cautare blue">
    <form name="auto" method="get" action="<?php echo $sitepath.'sejur-'.fa_link($den_tara).'/'; ?>" autocomplete="off">
        <input type="text" name="nume_hotel" id="nume_hotel" title="Introdu numele hotelului dorit din <?php echo $den_tara; ?> si apoi apasa pe Cauta" value="<?php echo $_GET['nume_hotel']; ?>" />
        <?php /*?><input type="hidden" name="nume_hotel_val" id="nume_hotel_val" /><?php */?>
        <input type="button" value="" onclick="document.auto.submit();" title="Cauta" />
	</form>
  </div>
  
  <div style="padding:10px;" class="bold">Filtrează ofertele după<br />
  criteriile de mai jos:</div>

  <div class="item NEW-round6px">
  <h3>Tara</h3>
  <?php $tari_f=get_tari($iduri,'','','',''); ?>
  <select name="tara" id="tara" onchange="filtrare_get_nou('<?php echo $indice; ?>', this.value, '');">
  <?php if(sizeof($tari_f)>0) {
      foreach($tari_f as $key_tf=>$value_tf) { ?>
      <option value="<?php echo fa_link($value_tf); ?>" <?php if($value_tf==$den_tara) { ?> selected="selected" <?php } ?>><?php echo $value_tf; ?></option>
      <?php }
  } ?>
  </select>
  </div>
  
  <div class="item NEW-round6px">
  <h3>Destinatia/Localitatea</h3>
  <select name="localitate" id="localitate" onchange="filtrare_get_nou('<?php echo $indice; ?>', '<?php echo $link_tara; ?>', this.value); ga('send', 'event', 'filtru stanga', 'destinatia', this.options[this.selectedIndex].value);">
    <option value="">Destinatia / Localitatea</option>
    <?php if($tara) {
	  $zone=get_zone($iduri,'','',$id_tara,'');
	  if(sizeof($zone)>0) {
	   foreach($zone as $id_zona1=>$value) {
		$link_z=fa_link($value); ?>
    <option value="<?php echo $link_z; ?>" <?php if($link_z==$link_zona) { ?> selected="selected" <?php } ?>><?php echo strtoupper($value); ?></option>
    <?php $loc_f=get_localitate($iduri,'',$id_zona1,$id_tara,'');
	   if(sizeof($loc_f)>0) {	   
	   foreach($loc_f as $kloc_tf=>$loc_tf) {
	   $link_lo=fa_link($loc_tf['denumire']); ?>
    <option value="<?php echo $link_z.'/'.$link_lo; ?>" <?php if($link_lo==$link_oras) { ?> selected="selected" <?php } ?>>&nbsp;&nbsp;&nbsp;<?php echo $loc_tf['denumire']; ?></option>
    <?php }
	       }
	     }
	   } 
	  } ?>
  </select>
  </div>

<?php if(!isset($_GET['nume_hotel'])) { ?>
  <?php $tipuri=get_tiputi_oferte('', $id_localitate, $id_zona, $id_tara, '', ''); $i=0;
if(sizeof($tipuri)>0) { ?>
  <div class="item bkg-green NEW-round6px">
  <h3>Ocazii speciale</h3>
  <ul>
    <li>
      <input type="checkbox" onclick="document.location.href='<?php echo $sitepath.'sejur-'.$link_tara.'/'; if($link_zona) echo $link_zona.'/'; if($link_oras) echo $link_oras.'/'; ?>';" id="tip0" name="evenimente" value="toate" <?php if(!$_REQUEST['tip']) { ?> checked="checked" <?php } ?> />
      <label for="tip0" <?php if(!$_REQUEST['tip']) echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php foreach($tipuri as $key_t=>$value_t) { $i++;
   $valTip=fa_link($value_t['denumire']); ?>
    <li>
      <input type="checkbox" onclick="document.location.href='<?php echo $sitepath.'oferte-'.$valTip.'/'.$link_tara.'/'; if($link_zona) echo $link_zona.'/'; if($link_oras) echo $link_oras.'/'; ?>';" id="tip<?php echo $i; ?>" name="evenimente" value="<?php echo $valTip; ?>" <?php if($valTip==$_REQUEST['tip']) { ?> checked="checked" <?php } ?> />
      <label for="tip<?php echo $i; ?>" <?php if($valTip==$_REQUEST['tip']) echo 'class="sel"'; ?>><?php echo $value_t['denumire']; ?></label>
    </li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>
  
<?php if($id_transport || $nr_stele || $nmasa || $nconcept || $din_luna || ($distanta && $distanta<>'toate') || $_GET['early-booking']=='da') { ?>
  <a href="<?php echo $link_p; ?>" class="delete"><span>X</span> Sterge toate filtrele</a>
<?php } ?>
  
<?php if($link_tara) { ?>
  <div class="item bkg-red NEW-round6px">
    <h3>Inceputul sejurului</h3>
    <select name="luni_plecari" id="luni_plecari" onchange="document.location.href='<?php echo $link_p; ?>?data-plecare='+this.value;">
      <option value="">Alege inceperea sejurului</option>
      <?php if(sizeof($luni_plecari)>0) {
	   foreach($luni_plecari as $value) {
		$lun=explode('-', $value);
		$link_z=$value; ?>
      <option value="<?php echo $link_z; ?>" <?php if($link_z==$din_luna) { ?> selected="selected" <?php } ?>><?php echo $luna[$lun[0]].' '.$lun[1]; ?></option>
      <?php }
	  } ?>
    </select>
  </div>
<?php } ?>

<?php $link_p1=$link_p;
  if(!$_REQUEST['data-plecare'])  $link_p1=$link_p1.'?optiuni=da'; else $link_p1=$link_p1.'?data-plecare='.$_REQUEST['data-plecare']; ?>
  <div class="item NEW-round6px">
  <h3>Reduceri</h3>
  <ul>
    <li>
      <input type="checkbox" onclick="document.location.href='<?php echo $sitepath.'sejur-'.$link_tara.'/'; if($link_zona) echo $link_zona.'/'; if($link_oras) echo $link_oras.'/'; ?>';" id="tipE0" name="early" value="toate" <?php if(!$_GET['early-booking']) { ?> checked="checked" <?php } ?> />
      <label for="tipE0" <?php if(!$_GET['early-booking']) echo 'class="sel"'; ?>>Toate</label>
    </li>
    <li>
      <input type="checkbox" onclick="document.location.href='<?php echo $link_p1.'&early-booking=da'; ?>';" id="tipE1" name="erly1" value="da" <?php if($_GET['early-booking']=='da') { ?> checked="checked" <?php } ?> />
      <label for="tipE1" <?php if($_GET['early-booking']=='da') echo 'class="sel"'; ?>>Early Booking</label>
    </li>
  </ul>
  </div>

  <?php if($_GET['early-booking']) $link_p1=$link_p1.'&early-booking=da'; if(sizeof($filtruOf['trans'])>0) { ?>
  <div class="item NEW-round6px">
  <h3>Transport</h3>
  <?php krsort($filtruOf['trans']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['trans'])==0) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(sizeof($filtruActual['trans'])==0) echo 'disabled="disabled"'; ?> id="optiune0" name="transport[]" <?php if($transport=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, this.value, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);" />
      <label for="optiune0" <?php if($transport=='toate') echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) { $i++;
	 $lingTr=fa_link($key);
	if($value) $nr=$value; else $nr=0; ?>
    <li <?php if(!$filtruActual['trans'][$key]) echo 'class="disabled"'; ?>>
      <input type="checkbox"  <?php if(!$filtruActual['trans'][$key]) echo 'disabled="disabled"'; ?> name="transport[]" id="optiune<?php echo $i; ?>" <?php if($lingTr==$transport) { ?> checked="checked" <?php } ?> value="<?php echo $lingTr; ?>" onclick="if(this.checked) var tr=this.value; else var tr='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, tr, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);" />
      <label for="optiune<?php echo $i; ?>" <?php if($lingTr==$transport) echo 'class="sel"'; ?>><?php echo $key; ?></label>
      <?php if($key=='Avion') { ?>
      <select name="oras_plecare" onchange="filtrare_restu_nou(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, this.value, <?php echo "'".$Lconcept."'"; ?>);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_avion'])) {
     krsort($filtruOf['plecare_avion']);
	 foreach($filtruOf['plecare_avion'] as $key=>$value) {
	 $lingTr=fa_link($key); ?>
        <option value="<?php echo $lingTr; ?>" <?php if(!$filtruActual['plecare_avion'][$key]) echo 'disabled="disabled"'; if($lingTr==$plecare_avion || ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
        <?php }
	  } ?>
      </select>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['stele'])>0) { ?>
  <div class="item NEW-round6px">
  <h3>Categorie Hotel</h3>
  <?php krsort($filtruOf['stele']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['stele'])==0) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(sizeof($filtruActual['stele'])==0) echo 'disabled="disabled"'; ?> id="optiunes0" name="stele[]" <?php if($stele=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, this.value, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);" />
      <label for="optiunes0" <?php if($stele=='toate') echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php foreach($filtruOf['stele'] as $i=>$nr_s) {
	  if($nr_s) $nrS=$nr_s; else $nrS=0;   ?>
    <li <?php if(!$filtruActual['stele'][$i]) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(!$filtruActual['stele'][$i]) echo 'disabled="disabled"'; ?> id="optiunes<?php echo $i; ?>" name="stele[]" value="<?php echo $i; ?>" <?php if($i==$stele) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var st=this.value; else var st='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, st, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);" />
      <label for="optiunes<?php echo $i; ?>" <?php if($i==$stele) echo 'class="sel"'; ?>><?php echo $i; if($i>1) echo ' stele '; else echo ' stea '; ?></label>
    </li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['masa'])>0) { ksort($filtruOf['masa']); ?>
  <div class="item NEW-round6px">
  <h3>Tip masa</h3>
  <ul>
    <li <?php if(sizeof($filtruActual['masa'])==0) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(sizeof($filtruActual['masa'])==0) echo 'disabled="disabled"'; ?> id="optiuneM0" name="masa[]" value="toate" <?php if($masa=='toate') { ?> checked="checked" <?php } ?> onclick="filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, this.value, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);"/>
      <label for="optiuneM0" <?php if($masa=='toate') echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php $i=0; foreach($filtruOf['masa'] as $keyM=>$valueM) {
	  foreach($valueM as $denM=>$nr_m) { $i++;
	  if($nr_m) $nrM=$nr_m; else $nrM=0;
	  $lingM=fa_link($denM); ?>
    <li <?php if(!$filtruActual['masa'][$denM]) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(!$filtruActual['masa'][$denM]) echo 'disabled="disabled"'; ?> name="masa[]" id="optiuneM<?php echo $i; ?>" value="<?php echo $lingM; ?>" <?php if($lingM==$Lmasa) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var ms=this.value; else var ms='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, ms, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);" />
      <label for="optiuneM<?php echo $i; ?>" <?php if($lingM==$Lmasa) echo 'class="sel"'; ?>><?php echo $denM; ?></label>
    </li>
    <?php }
	  } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(in_array($zona, $zone_filtru_distanta) && sizeof($filtruOf['distanta'])) { 
	foreach($filtruOf['distanta'] as $fata_de=>$value) {
	ksort($value); ?>
  <div class="item NEW-round6px">
  <h3>Distanta fata de <?php echo $fata_de; ?></h3>
  <ul>
    <li <?php if(sizeof($filtruActual['distanta'][$fata_de])==0) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(sizeof($filtruActual['distanta'][$fata_de])==0) echo 'disabled="disabled"'; ?> id="optiuneD0" name="distanta[]" value="toate" <?php if($distanta=='toate') { ?> checked="checked" <?php } ?> onclick="filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, this.value, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);"/>
      <label for="optiuneD0" <?php if($distanta=='toate') echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php $i=0; foreach($value as $key1=>$value2) { $i++;
	if($value2) $nrD=$value2; else $nrD=0;
	$val_dist=$fata_de.'-'.$inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; ?>
    <li <?php if(!$filtruActual['distanta'][$fata_de][$key1]) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(!$filtruActual['distanta'][$fata_de][$key1]) echo 'disabled="disabled"'; ?> name="distanta[]" id="optiuneD<?php echo $i; ?>" value="<?php echo $val_dist; ?>" <?php if($distanta==$val_dist) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var ds=this.value; else var ds='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, ds, <?php echo "'".$plecare_avion."'"; ?>, <?php echo "'".$Lconcept."'"; ?>);" />
      <label for="optiuneD<?php echo $i; ?>" <?php if($distanta==$val_dist) echo 'class="sel"'; ?>>
        <?php if($key1==1) echo '<'.$inteval_distanta[1][2];
	elseif($key1<4) echo $inteval_distanta[$key1][1].'-'.$inteval_distanta[$key1][2]; elseif($key1==4) echo '>'.$inteval_distanta[4][1]; echo ' m'; ?>
      </label>
    </li>
    <?php }
	 } ?>
  </ul>
  </div>
  <?php } ?>
  
  <?php if(sizeof($filtruOf['concept'])>0) { ?>
  <div class="item NEW-round6px">
  <h3>Recomandat pentru</h3>
  <?php krsort($filtruOf['concept']); ?>
  <ul>
    <li <?php if(sizeof($filtruActual['concept'])==0) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(sizeof($filtruActual['concept'])==0) echo 'disabled="disabled"'; ?> id="optiuneC0" name="concept[]" <?php if($concept=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, this.value);" />
      <label for="optiuneC0" <?php if($concept=='toate') echo 'class="sel"'; ?>>Toate</label>
    </li>
    <?php $i=0; foreach($filtruOf['concept'] as $denC=>$nr_c) { $i++;
	  if($nr_c) $nrC=$nr_c; else $nrC=0;
	  $lingC=fa_link($denC); ?>
    <li <?php if(!$filtruActual['concept'][$denC]) echo 'class="disabled"'; ?>>
      <input type="checkbox" <?php if(!$filtruActual['concept'][$denC]) echo 'disabled="disabled"'; ?> id="optiuneC<?php echo $i; ?>" name="concept[]" value="<?php echo $lingC; ?>" <?php if($lingC==$Lconcept) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var cnt=this.value; else var cnt='toate'; filtrare_restu_nou(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, <?php echo "'".$stele."'"; ?>, <?php echo "'".$Lmasa."'"; ?>, <?php echo "'".$distanta."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>, cnt);" />
      <label for="optiuneC<?php echo $i; ?>" <?php if($lingC==$Lconcept) echo 'class="sel"'; ?>><?php echo $denC; ?></label>
    </li>
    <?php } ?>
  </ul>
  </div>
  <?php } ?>

<?php }//end if(!isset($_GET['nume_hotel'])) ?>

</div>

<p>&nbsp;</p>

<?php /*?><div class="fb-like-box" data-href="<?php echo $facebook_page; ?>" data-width="182" data-height="440" data-show-faces="true" data-stream="false" data-header="true"></div>

<p>&nbsp;</p><?php */?>

<?php /*?><a href="<?php echo $sitepath; ?>newsletter.html" title="Abonare Newsletter" rel="infoboxes"><img src="<?php echo $imgpath; ?>/banner/abonare-newsletter-small.png" alt="Abonare Newsletter" /></a>

<p>&nbsp;</p><?php */?>

<?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/linkuri_seo.php"); ?>
