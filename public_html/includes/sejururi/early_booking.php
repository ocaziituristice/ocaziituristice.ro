<h1 class="blue">Oferte Early Booking <? echo ucwords(desfa_link($_REQUEST['tip'])); ?></h1>

<div class="text-dreapta">
    <h2 class="green">Early Booking - Rezerva din timp pentru preturi mici!</h2>
    Daca te-ai hotarat deja unde vrei sa petreci vacanta, ai numai avantaje cu <strong>early booking 2011</strong>. Rezerva din timp si ai preturi mici pentru destinatii exotice sau europene. Poti descoperi fascinantul si misteriosul Egipt cu <strong>early booking Egipt</strong> sau atinge destinatia sporturilor de iarna cu <a href="http://www.ocaziituristice.ro/early-booking/austria/" title="Oferte early booking Austria">early booking Austria</a>. Iti recomandam si <a href="http://www.ocaziituristice.ro/early-booking/cipru/" title="Oferte early booking Cipru">early booking Cipru</a> pentru peisaje luxuriante si petreceri renumite. Odata ce ai ales din varietatea de <strong>oferte early booking</strong>, nu ramane decat sa te pregatesti de amintiri de neuitat si sa zambesti in fiecare zi cu gandul la locurile minunante pe care le vei vizita.
</div>    

<?php $link_pag="early-booking";
$selT="select tar.* from ((select
tari.denumire,
tari.id_tara
from
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate
inner join zone on localitati.id_zona = zone.id_zona
inner join tari on zone.id_tara = tari.id_tara
inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur')
where
oferte.valabila = 'da'
and hoteluri.tip_unitate <> 'Circuit'
Group by tari.id_tara)
union
(SELECT
tari.denumire,
tari.id_tara
FROM
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
inner Join early_booking on (oferte.id_oferta = early_booking.id_oferta and early_booking.end_date >= now() and early_booking.discount >'0' and early_booking.tip = 'sejur')
WHERE
oferte.valabila =  'da' AND
hoteluri.tip_unitate = 'Circuit'
and traseu_circuit.tara_principala = 'da'
GROUP BY tari.id_tara)) as tar
Group by tar.id_tara
Order by tar.denumire ";
$queT=mysql_query($selT) or die(mysql_error());
if(mysql_num_rows($queT)>'0') { ?>
<div class="countrydivcontainer">
<?php while($value=mysql_fetch_array($queT)) { ?>
      <div class="tabLinks"><a href="/early-booking/<? echo fa_link($value['denumire']); ?>/" title="Early Booking <? echo $value['denumire']; ?>"><? echo $value['denumire']; ?></a></div>
  <?php } ?>
  <br class="clear" />
  <div class="filterText">Alege tara pentru a filtra ofertele Early Booking</div>
</div>
<?php } @mysql_free_result($queT);	?>
  
  <div class="ordonare" align="right">
  <span class="red">Ordoneaza dupa:</span>
<form name="ord_stele" id="ord_stele" method="post" action="">
  <input type="hidden" name="tip_stele" value="<?php if(!$_POST['tip_stele']) echo 'desc'; elseif($_POST['tip_stele']=='desc') echo 'asc'; else echo 'desc'; ?>" />
  <?php if(!$_POST['tip_stele']) { ?>
  <a onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_stele.submit();" class="initial" title="Numar stele Descendent">Numar stele</a>
  <?php } else { ?> <a href="#" onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_stele.submit();" class="selected" title="Numar stele <?php if($_POST['tip_stele']=='desc') echo 'Ascedent'; else echo 'Descendent'; ?>"><?php if($_POST['tip_stele']=='desc') { ?><img src="/images/spacer.gif" class="asc" /> <?php } else { ?><img src="/images/spacer.gif" class="desc" /><?php } ?> Numar stele</a><?php } ?>
</form> |
<form name="ord_relevanta" id="ord_relevanta" method="post" action="">
  <input type="hidden" name="tip_relevanta" value="<?php if(!$_POST['tip_relevanta']) echo 'desc'; elseif($_POST['tip_relevanta']=='desc') echo 'asc'; else echo 'desc'; ?>" />
  <?php if(!$_POST['tip_relevanta']) { ?>
  <a onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_relevanta.submit();" class="initial" title="Relevanta Descendent">Relevanta</a>
  <?php } else { ?> <a href="#" onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_relevanta.submit();" class="selected" title="Relevanta <?php if($_POST['tip_relevanta']=='desc') echo 'Ascedent'; else echo 'Descendent'; ?>"><?php if($_POST['tip_relevanta']=='desc') { ?><img src="/images/spacer.gif" class="asc" /> <?php } else { ?><img src="/images/spacer.gif" class="desc" /><?php } ?> Relevanta</a><?php } ?>
</form> |
  <form name="ord_implicita" id="ord_implicita" method="post" action="">
  <?php if($_POST['tip_relevanta'] || $_POST['tip_stele'] || $_POST['tip_pret']) { ?>
  <a onclick="document.ord_implicita.submit();" class="initial" title="Ordonare implicita">Ordonare implicita</a>
  <?php } else { ?>
  <a onclick="document.ord_implicita.submit();" class="selected" title="Ordonare implicita">Ordonare implicita</a>
  <?php } ?>
  </form>
</div>
<?php
$afisare =new AFISARE_SEJUR();
$afisare->setEarly('da');
if($transport) $afisare->setTransport($transport);
if($masa) $afisare->setMasa($masa);
$afisare->set_oferte_pagina(10);
if($_POST['tip_stele']) $afisare->setOrdonareStele($_POST['tip_stele']);
if($_POST['tip_relevanta']) $afisare->setOrdonareRelevanta($_POST['tip_relevanta']);

$afisare->paginare(); ?>
<br/>
<div class="spacer"><img src="/images/spacer.gif" width="1" height="6" alt="" /></div>