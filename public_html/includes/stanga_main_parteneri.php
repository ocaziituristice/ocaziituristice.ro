<div class="parteneriOT">

<h1>Parteneri Ocazii Turistice</h1>
<br />

<div class="coloana_1">
<h2 align="center" class="green">Agentii de Turism</h2>

<p><a href="http://www.bigtravel.ro/" target="_blank" title="Big Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/big-travel.jpg" alt="Big Travel" width="88" height="31" class="logo" title="Big Travel" /><span>Big Travel</span></a></p>
<p><a href="http://www.bluetravel.eu/" target="_blank" title="Blue Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/bluetravel.jpg" alt="Blue Travel" width="88" height="31" class="logo" title="Blue Travel" /><span>Blue Travel</span></a></p>
<p><a href="http://www.cazareinbulgaria.ro/" target="_blank" title="Cazare in Bulgaria"><img src="http://www.ocaziituristice.ro/images/parteneri/cazarebulgaria.jpg" alt="Cazare in Bulgaria" width="88" height="31" class="logo" title="Cazare in Bulgaria" /><span>Cazare in Bulgaria</span></a></p>
<p><a href="http://www.ecovoyage.ro/" target="_blank" title="Ecovoyage"><img src="http://www.ocaziituristice.ro/images/parteneri/ecovoyage.jpg" alt="Ecovoyage" width="88" height="31" class="logo" title="Ecovoyage" /><span>Ecovoyage</span></a></p>
<p><a href="http://www.gregorytour.ro/" target="_blank" title="Gregory Tour"><img src="http://www.ocaziituristice.ro/images/parteneri/gregory.jpg" alt="Gregory Tour" width="88" height="31" class="logo" title="Gregory Tour" /><span>Gregory Tour</span></a></p>
<p><a href="http://www.idealtravel.ro/" target="_blank" title="Ideal Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/idealtravel.jpg" alt="Ideal Travel" width="88" height="31" class="logo" title="Ideal Travel" /><span>Ideal Travel</span></a></p>
<p><a href="http://www.last-minut.ro/" target="_blank" title="Last Minut"><img src="http://www.ocaziituristice.ro/images/parteneri/lastminute.jpg" alt="Last Minut" width="88" height="31" class="logo" title="Last Minut" /><span>Last Minut</span></a></p>
<p><a href="http://www.rextravel.ro/" target="_blank" title="Rex Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/rex_travel.jpg" alt="Rex Travel" width="88" height="31" class="logo" title="Rex Travel" /><span>Rex Travel</span></a></p>
<p><a href="http://www.simian-turism.ro/" target="_blank" title="Simian Turism"><img src="http://www.ocaziituristice.ro/images/parteneri/simian.jpg" alt="Simian Turism" width="88" height="31" class="logo" title="Simian Turism" /><span>Simian Turism</span></a></p>
<p><a href="http://www.terrasejur.ro/" target="_blank" title="Terra Holidays"><img src="http://www.ocaziituristice.ro/images/parteneri/terrasejur.jpg" alt="Terra Holidays" width="88" height="31" class="logo" title="Terra Holidays" /><span>Terra Holidays</span></a></p>
<p><a href="http://www.vacantainbulgaria.ro/" target="_blank" title="Vacanta Bulgaria"><img src="http://www.ocaziituristice.ro/images/parteneri/vacantainbulgaria.jpg" alt="Vacanta Bulgaria" width="88" height="31" class="logo" title="Vacanta Bulgaria" /><span>Vacanta Bulgaria</span></a></p>
<p><a href="http://www.vacantainturcia.ro/" target="_blank" title="Vacanta Turcia"><img src="http://www.ocaziituristice.ro/images/parteneri/vacantainturcia.jpg" class="logo" alt="Vacanta Turcia" title="Vacanta Turcia" /><span>Vacanta Turcia</span></a></p>
<p><a href="http://www.vacantaperfecta.com/" target="_blank" title="Vis Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/vacantaperfecta.jpg" alt="Vis Travel" width="88" height="31" class="logo" title="Vis Travel" /><span>Vis Travel</span></a></p>
<p><a href="http://www.visittheworld.ro/" target="_blank" title="Visit Romania"><img src="http://www.ocaziituristice.ro/images/parteneri/visit-romania.jpg" alt="Visit Romania" width="88" height="31" class="logo" title="Visit Romania" /><span>Visit Romania</span></a></p>
<p><a href="http://www.arrowintravel.ro/" target="_blank" title="Arrow International Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/arrow.jpg" alt="Arrow International Travel" width="88" height="31" class="logo" title="Arrow International Travel" /><span>Arrow International Travel</span></a></p>
<p><a href="http://www.alidaholiday.ro/" target="_blank" title="Alida Holiday"><img src="http://www.ocaziituristice.ro/images/parteneri/alidaholiday.jpg" alt="Alida Holiday" width="88" height="31" class="logo" title="Alida Holiday" /><span>Alida Holiday</span></a></p>
<p><a href="http://www.utours.ro/" target="_blank" title="UNTRR Tours"><img src="http://www.ocaziituristice.ro/images/parteneri/sigla_utours.jpg" alt="UNTRR Tours" width="88" height="31" class="logo" title="UNTRR Tours" /><span>UNTRR Tours</span></a></p>
<p><a href="http://www.bliss-travel.ro/" target="_blank" title="Bliss Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/bliss-travel.jpg" alt="Bliss Travel" width="88" height="31" class="logo" title="Bliss Travel" /><span>Bliss Travel</span></a></p>
<p><a href="http://www.vacationtoromania.ro/" target="_blank" title="Bliss Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/vacationtoromania.jpg" alt="Vacation to Romania" width="88" height="31" class="logo" title="Vacation to Romania" /><span>Vacation to Romania</span></a></p>
<p><a href="http://www.infobulgaria.ro" target="_blank" title="infobulgaria.ro"><img src="/parteneri/InfoBulgaria.jpg" border="0" width="88" height="33" alt="infobulgaria.ro" title="www.infobulgaria.ro" class="logo" /><span>Info Bulgaria</span></a></p>
<p><a href="http://helatravel.ro/ro/" target="_blank" title="Hela Travel"><img src="http://www.ocaziituristice.ro/images/parteneri/sigla_hela.jpg" border="0" width="88" height="31" alt="Hela Travel" title="Hela Travel" class="logo" /><span>Hela Travel</span></a></p>
</div>

<div class="coloana_2">
<h2 class="red">Fi partener cu OcaziiTuristice!</h2>
<p>Pentru parteneriat si schimb de linkuri va rugam sa ne contactati la <a href="mailto:webmaster@ocaziituristice.ro" target="_blank">webmaster@ocaziituristice.ro</a></p>
<p>Sunt doua modalitati de afisare. <strong>LINK</strong> sau <strong>BANNER</strong> (de dimensiunea 88x31px) ce va fi afisat in pagina <a href="/parteneri_ocaziituristice.html">Parteneri</a>.</p>
<p>E-mailul trebuie sa contina adresa paginii in care a fost pus banner-ul nostru, precum si codul site-ului dumneavoastra care doriti sa il afisati la noi.</p>
<p>Va rugam sa alegeti una din modalitatile de mai jos:</p>
<h3>Banner imagine 88x31px</h3>
<textarea readonly="readonly" onclick="this.focus(); this.select();" style="height:70px;"><a href="http://www.ocaziituristice.ro/" target="_blank" title="Agentii de turism"><img src="http://www.ocaziituristice.ro/images/parteneri/parteneriat_ocazii.gif" border="0" width="88" height="31" alt="Agentii de turism" title="Agentii de turism" /></a></textarea>
<br /><br />
<h3>Link simplu</h3>
<textarea readonly="readonly" onclick="this.focus(); this.select();" style="height:30px;"><a href="http://www.ocaziituristice.ro/" target="_blank" title="Agentii de turism">Agentii de turism</a></textarea>
</div>

<div class="coloana_3">
<h2 align="center" class="green">Bloguri si Alte Site-uri</h2>

<p><a href="http://www.turo.ro" title="Cazare Mamaia - Rezervari Online Apartamente De Lux Pe Malul Marii" target="_blank"><span>Cazare Mamaia</span></a></p>
<p><a href="http://www.cazareaustria.com/" target="_blank" title="Cazare Austria"><span>Cazare ieftina in Austria</span></a></p>
</div>

<br class="spacer" />

</div>