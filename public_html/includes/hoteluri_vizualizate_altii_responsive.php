<?php

$viewers    = '';
$sel_views1 = "SELECT id_sesiune_vizitator FROM oferte_vizitate WHERE id_hotel = '" . $id_hotel . "' ";
$que_views1 = mysql_query( $sel_views1 ) or die( mysql_error() );
if ( mysql_num_rows( $que_views1 ) > 0 ) {
	while ( $row_views1 = mysql_fetch_array( $que_views1 ) ) {
		$viewers .= "'" . $row_views1['id_sesiune_vizitator'] . "',";
	}
	$viewers = substr( $viewers, 0, - 1 );

	$sel_views = "SELECT *
FROM oferte_vizitate
WHERE id_sesiune_vizitator IN (" . $viewers . ")
AND id_hotel <> '" . $id_hotel . "'
AND id_sesiune_vizitator <> '" . session_id() . "'
";
	$que_views = mysql_query( $sel_views ) or die( mysql_error() );

	if ( mysql_num_rows( $que_views ) > 0 ) {
		$id_hoteluri_viz = '';
		while ( $row_views = mysql_fetch_array( $que_views ) ) {
			$id_hoteluri_viz .= $row_views['id_hotel'] . ',';
		}
		$id_hoteluri_viz = substr( $id_hoteluri_viz, 0, - 1 );

		$selOf = "SELECT
oferte.id_oferta,
oferte.denumire,
oferte.denumire_scurta,
oferte.id_oferta,
oferte.nr_zile,
oferte.nr_nopti,
oferte.exprimare_pret,
oferte.pret_minim,
oferte.moneda,
oferte.masa,
transport.denumire AS denumire_transport,
hoteluri.nume,
hoteluri.stele,
hoteluri.descriere AS descriere,
hoteluri.id_hotel,
hoteluri.tip_unitate,
hoteluri.poza1,
zone.denumire as denumire_zona,
localitati.denumire as denumire_localitate,
tari.denumire as denumire_tara,
continente.nume_continent
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
INNER JOIN transport ON transport.id_trans=oferte.id_transport
LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
WHERE oferte.valabila = 'da' and tip_unitate<>'Circuit'
AND oferte.id_hotel IN (" . $id_hoteluri_viz . ")
Group by id_hotel
ORDER BY rand() 
LIMIT 0,3 ";
		$queOf = mysql_query( $selOf ) or die( mysql_error() );

		if ( mysql_num_rows( $queOf ) ) {
			?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading">
                        <h2>Clienții care au văzut <span
                                    class="text-danger"><?php echo $detalii_hotel['denumire']; ?></span>
							<?php if ( $detalii_hotel['tip_unitate'] == 'Circuit' ) {
							} else {
								echo 'din <a href="/cazare-' . fa_link( $detalii_hotel['localitate'] ) . '/" class="link-blue">' . $detalii_hotel['localitate'] . '</a>';
							} ?>
                            au fost interesați și de:
                        </h2>
                    </div>
                    <div class="card-deck-wrapper circuit">
                        <div class="card-deck" id="alteOferteVizitate">
							<?php
							while ( $rowOf = mysql_fetch_array( $queOf ) ) {
								if ( $rowOf['poza1'] ) {
									$poza = '/img_mediu_hotel/' . $rowOf['poza1'];
								} else {
									$poza = '/images/no_photo.jpg';
								}

								$denumire   = $rowOf['nume'];
								//$stele      = '<span class="stele-mari-' . $rowOf['stele'] . '"></span>';
								$localizare = $rowOf['denumire_zona'];
								if ( $rowOf['denumire_localitate'] <> $localizare ) {
									$localizare = $localizare . ' / ' . $rowOf['denumire_localitate'];
								}
								$link_hotel = fa_link_hotel( $rowOf['denumire_localitate'], $rowOf['nume'] );
								
								$string = $rowOf['descriere'];
								
								// strip tags to avoid breaking any html
								$string = strip_tags($string);

								if (strlen($string) > 700) {

									// truncate string
									$stringCut = substr($string, 0, 700);

									// make sure it ends in a word so assassinate doesn't become ass...
									$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="' .$link_hotel. '">Mai mult</a>'; 
								}
								

								?>                                
                                <div class="oferta col-md-4 clearfix">
									<a href="<?php echo $link_hotel; ?>" rel="nofollow">
										<div class="title"><?php echo $denumire ?><span class="stele-mici-<?php echo $rowOf['stele']; ?>"></span></div>
										<div class="image" style="background-image:url('<?php echo $poza; ?>');"></div>
										<div class="left">
											<p><?php echo $localizare ?></p>
										</div>
									</a>
								</div>
							<?php }
							@mysql_free_result( $queOf ); ?>
                        </div>
                    </div>
                </div>
            </div>
		<?php }
	}
} ?>
