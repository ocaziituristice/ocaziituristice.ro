<div id="NEW-destinatie" align="center">
  <h1 class="blue" style="font-size:3em;"><span class="red">CUMPARA ACUM</span> SI PRIMESTI</h1>
  <div style="border:1px solid #D60808; -webkit-border-radius:12px; -moz-border-radius:12px; border-radius:12px; margin:15px 10px 10px 10px;">
    <div style="float:left; padding:0 20px; background:#D60808; font-size:6em; line-height:1.5em; font-weight:bold; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius:10px;" class="white">180<sup>Lei</sup></div>
    <div style="float:left; padding:10px 0 10px 15px; font-size:1.5em; line-height:1.5em;">
      <strong class="red">REDUCERE IMEDIAT</strong>, la orice rezervare <strong class="blue">ONLINE</strong> din<br />
      <a href="/sejur-turcia/antalya/?optiuni=da&transport=avion&ordonare=tip_pret-asc&utm_campaign=reducere180lei" class="link-blue"><strong class="blue">ANTALYA, Turcia </strong></a>cu transport avion pentru minim 2 persoane<br />
      <strong class="red">&raquo; <a href="/sejur-turcia/antalya/?optiuni=da&transport=avion&ordonare=tip_pret-asc&utm_campaign=reducere180lei" class="link-red"><span class="red">click aici</span></a> pentru oferte &laquo;</strong>
    </div>
    <div class="clear"></div>
  </div>
  <br />
  <p style="color:#555;">* reducerea se acorda <strong>imediat</strong> la rezervarile ONLINE <strong>platite integral</strong> pentru minim 2 persoane.<br />
  Practic platesti mai putin cu 180 Lei valoarea totala a sejurului decat tariful afisat pe site.<br />
</div>
