<?php
if ( isset( $detalii_online['tip_preturi'] ) ) {
	$var_dates    = '';
	$var_dates_if = '';
	if ( ( ( ( in_array( "Avion", $detalii_online['tip_transport'] ) ) or in_array( "Autocar", $detalii_online['tip_transport'] ) ) AND ( count( $detalii_online['tip_transport'] ) == 1 ) || $orasplec != null ) ) {
		if ( sizeof( $detalii_online['data_start'] ) > 0 ) {
			$var_dates .= 'array_dates' . $id_hotel . ' = [';
			foreach ( $detalii_online['data_start'] as $key_ds => $value_ds ) {
				$var_dates .= '"' . $value_ds . '",';
			}
			$var_dates = substr( $var_dates, 0, - 1 ) . ']';
		}

		if ( in_array( date( "Y-m-d", strtotime( $plecdata ) ), $detalii_online['data_start'] ) ) {
			$plecdata = $plecdata;
		} elseif ( in_array( date( "Y-m-d", strtotime( "+1 day", strtotime( $plecdata ) ) ), $detalii_online['data_start'] ) ) {
			$plecdata = date( "d.m.Y", strtotime( "+1 day", strtotime( $plecdata ) ) );
		} elseif ( in_array( date( "Y-m-d", strtotime( "-1 day", strtotime( $plecdata ) ) ), $detalii_online['data_start'] ) ) {
			$plecdata = date( "d.m.Y", strtotime( "-1 day", strtotime( $plecdata ) ) );
		} elseif ( in_array( date( "Y-m-d", strtotime( "+2 day", strtotime( $plecdata ) ) ), $detalii_online['data_start'] ) ) {
			$plecdata = date( "d.m.Y", strtotime( "+2 day", strtotime( $plecdata ) ) );
		} elseif ( in_array( date( "Y-m-d", strtotime( "-2 day", strtotime( $plecdata ) ) ), $detalii_online['data_start'] ) ) {
			$plecdata = date( "d.m.Y", strtotime( "-2 day", strtotime( $plecdata ) ) );
		} elseif ( in_array( date( "Y-m-d", strtotime( "+3 day", strtotime( $plecdata ) ) ), $detalii_online['data_start'] ) ) {
			$plecdata = date( "d.m.Y", strtotime( "+3 day", strtotime( $plecdata ) ) );
		} elseif ( in_array( date( "Y-m-d", strtotime( "-3 day", strtotime( $plecdata ) ) ), $detalii_online['data_start'] ) ) {
			$plecdata = date( "d.m.Y", strtotime( "-3 day", strtotime( $plecdata ) ) );
		}
	} else if ( count( $detalii_online['tip_transport'] ) > 1 ) //else if(in_array("perioade", $detalii_online['tip_preturi']))

	{
		if ( sizeof( $detalii_online['data_start'] ) > 0 ) {

			foreach ( $detalii_online['data_start'] as $key_ds => $value_ds ) {
				if ( $detalii_online['data_end'][ $key_ds ] != "0000-00-00" ) {

					$ds[ $key_ds ]     = explode( '-', $value_ds );
					$de[ $key_ds ]     = explode( '-', $detalii_online['data_end'][ $key_ds ] );
					$data_end_interval = $de[ $key_ds ][2];
					$var_dates         .= 'ds_' . $key_ds . ' = new Date(' . $ds[ $key_ds ][0] . ', ' . $ds[ $key_ds ][1] . ' - 1, ' . $ds[ $key_ds ][2] . '), de_' . $key_ds . ' = new Date(' . $de[ $key_ds ][0] . ', ' . $de[ $key_ds ][1] . ' - 1, ' . $data_end_interval . '), ';

					$var_dates_if .= '(date >= ds_' . $key_ds . ' && date <= de_' . $key_ds . ')||';
				}
			}
			$var_dates    = substr( $var_dates, 0, - 2 );
			$var_dates_if = substr( $var_dates_if, 0, - 2 );
		}
	}
}

if ( $detalii_online['min_data'] > $date_now ) {
	$dataplec_min = $detalii_online['min_data'];
} else {
	$dataplec_min = $date_now;
}
$dataplec_max = $detalii_online['max_data'];
?>
<div class="price-box">
    <h4 class="green"><i class="fa fa-search"></i> Calculator tarife <?php echo $den_hotel . ' ' . $den_localitate ?></h4>
    <form  name="cautare-oferte" id="form-vacant<?php echo $id_hotel; ?>" class="form-inline form-vacant" action="/request-availability/" method="post"  onsubmit="return validateForm();">
        <input name="id_hotel" type="hidden" value="<?php echo $id_hotel; ?>">
        <input name="showerr" type="hidden" value="da">
        <div class="form-group col-md-2">
            <label>Data check-in:</label>
			<?php $plecdata = date( "Y-m-d", strtotime($plecdata) ); ?>
            <input name="plecdata" id="date-range-picked" class="form-control"
                   value="<?php if ( isset( $plecdata ) and $plecdata >= $detalii_online['min_data'] and $plecdata <= $detalii_online['max_data'] ) {
				       echo date( "d.m.Y", strtotime( $plecdata ) );
			       } ?>">
        </div>
        <div class="form-group col-md-2">
            <label>Nr. Nopti:</label>
            <select name="pleczile" class="form-control">
				<?php foreach ( $detalii_online['no_nights'] as $nrnpt ) : ?>
                    <option value="<?php echo $nrnpt; ?>" <?php if ( $pleczile == $nrnpt ) {
						echo 'selected';
					} elseif ( $nrnpt == 7 and $pleczile == '' ) {
						echo 'selected';
					} ?>><?php echo $nrnpt; ?></option>
				<?php endforeach; ?>
            </select>
        </div>
        <div class="form-group col-md-2">
            <label>Nr. Adulti<?php echo $adulti?></label>
            <select name="adulti" class="form-control">
				<?php for ( $i = 1; $i <= 3; $i ++ ) : ?>
                    <option value="<?php echo $i; ?>" <?php if (!is_numeric($adulti) and $i==2 ) {
						echo 'selected';
					} elseif ($adulti == $i ) {
						echo 'selected';
					} ?>><?php echo $i; ?></option>
				<?php endfor; ?>
            </select>
            </select>
        </div>
        <div class="form-group col-md-2" id="select_mobile">
            <label for="calculeaza-copii252255">Nr. copii:</label>
            <select name="copii" id="" class="NEW-round4px form-control" onchange="displayAges(this.value)">
				<?php for ( $i = 0; $i <= 3; $i ++ ) : ?>
                    <option value="<?php echo $i; ?>" <?php if ( $copii == $i ) {
						echo 'selected';
					} ?>><?php echo $i; ?></option>
				<?php endfor; ?>
            </select>
        </div>

        <div class="form-group col-md-2">
  <?php /*?>          <label>Transport:</label>
            <select name="transport" class="form-control">
				<?php echo $alege_transport ?>

				<?php asort( $detalii_online['tip_transport'] );
				foreach ( $detalii_online['tip_transport'] as $k_tiptrans => $v_tiptrans ) { ?>
                    <option value="<?php echo $k_tiptrans;
					if ( $k_tiptrans == 1 ) {
						echo ';51';
					} ?>" <?php if ( $k_tiptrans != 1 ) {
						echo 'disabled';
					} ?>
						<?php if ( $k_tiptrans == $_REQUEST['transport'] ) {
							echo 'selected';
						} ?>><?php echo $v_tiptrans; ?></option>
					<?php if ( $k_tiptrans != 1 ) {
						foreach ( $detalii_online[ $v_tiptrans ] as $k_orasplec => $v_orasplec ) { ?>
                            <option value="<?php echo $k_tiptrans . ';' . $k_orasplec; ?>" <?php if ( $k_tiptrans == $_REQUEST['transport'] and $k_orasplec == $orasplec ) {
								echo 'selected';
							} ?>><?php echo $v_tiptrans . '/' . $v_orasplec; ?></option>
						<?php }
					} ?>
				<?php } ?>
            </select><?php */?>
            
             <label for="calculeaza-transport<?php echo $id_hotel; ?>">Transport:<?php if(!isset($orasplec)&!isset($tip_transport)){ $alege_transport='<option value="" selected">ALEGE TRANSPORTUL</option>';}?></label>
        <select name="transport" id="calculeaza-transport<?php echo $id_hotel; ?>"  class="form-control" onchange="submitCurrentForm('form-vacant<?php echo $id_hotel; ?>')">
		  <?php echo $alege_transport?>
		  
		  <?php asort($detalii_online['tip_transport']);
		  foreach($detalii_online['tip_transport'] as $k_tiptrans => $v_tiptrans) { ?>
          <option value="<?php echo $k_tiptrans; if($k_tiptrans==1) echo ';51'; ?>" <?php if($k_tiptrans!=1) echo 'disabled'; ?> 
		  <?php if($k_tiptrans==$tip_transport) echo 'selected'; ?>><?php echo $v_tiptrans; ?></option>
          <?php if($k_tiptrans!=1) {
		  foreach($detalii_online[$v_tiptrans] as $k_orasplec => $v_orasplec) { ?>
          <option value="<?php echo $k_tiptrans.';'.$k_orasplec; ?>" <?php if($k_tiptrans==$tip_transport and $k_orasplec==$orasplec) echo 'selected'; ?>><?php echo $v_tiptrans.'/'.$v_orasplec; ?></option>
		  <?php }
		  } ?>
          <?php } ?>
        </select>
            
            
            
        </div>
        <div class="form-group col-md-2">
            <input type="submit" value="Vezi preț"
                   class="btn btn-success btn-block button-green NEW-round6px float-right">
        </div>
        
        <div class="chd-ages" style="<?php echo $copii == 0 ? 'display:none;' : '' ?>">
		    <?php for ( $t = 0; $t < 3; $t ++ ) { ?>
                <div class="form-group col-md-2 field text-left varste-copii-<?php echo $t; ?>"
                     style="max-width:120px;<?php if ( $copii < ( $t + 1 ) ) {
				         echo 'display:none;';
			         } ?>">
                    <label for="varste-copii-<?php echo $t; ?>">Copil <?php echo $t + 1; ?></label>
                    <select name="age[<?php echo $t ?>]" id="varste-copii-<?php echo $t; ?>"
                            class="form-control NEW-round4px varste-copii-select-<?php echo $t; ?>">
					    <?php for ( $j1[ $t ] = 1; $j1[ $t ] <= 17; $j1[ $t ] ++ ) { ?>
                            <option value="<?php echo $j1[ $t ]; ?>" <?php if ( $copil_age[ $t ] == $j1[ $t ] and ( $t + 1 ) <= $copii ) {
							    echo 'selected';
						    } ?>><?php echo $j1[ $t ]; ?> ani
                            </option>
					    <?php } ?>
                    </select>
                </div>
		    <?php } ?>
        </div>

        <div id="container_date_picker"></div>
         <input name="parentLink" type="hidden" value="<?php echo "http://".$_SERVER['HTTP_HOST'].strtok($_SERVER['REQUEST_URI'],'?')?>">
	
      <input name="id_hotel" type="hidden" value="<?php echo $id_hotel; ?>">
        <input name="tip_pagina" type="hidden" value="hotel">
        <input name="showerr" type="hidden" value="da">
        <input name="submit" type="hidden" value="1">
    </form>
    <br class="clear"><br>
   <?php /*?> <div class="section clearfix"></div><?php */?>
    <script>
		function validateForm() {
			var date_field = document.getElementById("date-range-picked").value;
			if (date_field==null || !date_field) {
				alert("Data plecării trebuie selectată!");
				document.getElementById("date-range-picked").focus();
				return false;
			}
		}
		
		function submitCurrentForm(formId) {
			var date_field = document.getElementById("date-range-picked").value;
			if (date_field==null || !date_field) {
				alert("Data plecării trebuie selectată!");
				document.getElementById("date-range-picked").focus();
				return false;
			}
			$('#' + formId).find('.btn').trigger('click');
		}

	
		<?php if ( $var_dates ) : ?>
        var <?php echo $var_dates; ?>;
		<?php endif; ?>
        var windowWidth = $(window).width();
        if (windowWidth < 767) {
            var numberOfMonths = 1;
        } else {
            var numberOfMonths = 3;
        }

        $("#date-range-picked").datepicker({
            numberOfMonths: numberOfMonths,
            dateFormat: "dd.mm.yy",
            showButtonPanel: true,
			<?php if ( $var_dates ) : ?>
            beforeShowDay: function (date) {
                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
				<?php if($var_dates_if) : ?>
                return [(<?php echo $var_dates_if; ?>), ''];
				<?php else : ?>
                return [array_dates<?php echo $id_hotel; ?>.indexOf(string) !== -1];
				<?php endif; ?>
            },
			<?php endif; ?>
            minDate: "<?php echo date( "d.m.Y", strtotime( $dataplec_min ) ); ?>",
            maxDate: "<?php echo date( "d.m.Y", strtotime( $dataplec_max ) ); ?>"
        });

        setTimeout(function () {
			<?php if ( ( ! isset( $_REQUEST['plecdata'] ) and ! isset( $_COOKIE['grad_ocupare'] ) ) or $show_calendar == 'da' ) {
			echo '$("#date-range-picked").datepicker("show");';
		} ?>
        }, 1000);
        
 
		
		<?php if(isset($_GET['submit']))
		{ 
		?>
			$(document).ready(function(){
				 setTimeout(function(){
      		console.log('aici');
				$('html,body').animate({
        scrollTop: $("#preturiOnline").offset().top},
        'slow');
		});
        },2000);
	
		<?php } ?>





        $("#date-range-picked").datepicker().bind('click keyup', function () {
            if ($('#ui-datepicker-div :last-child').is('table')) {
                if ($('#ui-datepicker-div').find('.inline-block.pad5').length <= 0) {
                    $('.ui-datepicker-buttonpane').append("<span class='inline-block pad5 bigger-11em italic'>Selectaţi data când doriţi sa plecaţi în concediu şi apoi apăsaţi <strong>\"Vezi preţ\"</strong></span>");
                }
            }
        });
        
        $(document).ready(function() {
			changePosition();
			window.addEventListener('resize', changePosition);
			$(document).find('.ui-datepicker-next').click(function() {
					console.log('here');
					$('.ui-datepicker-next').trigger( "click" );
				});
			var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
				
		});
        
        function changePosition() {
			if ( $(window).width() < 767 ) {
				$('.chd-ages').insertAfter($('#select_mobile'));
			} else {
				$('.chd-ages').insertBefore($('#container_date_picker'));
			}
		}
    </script>
    
	
</div>
