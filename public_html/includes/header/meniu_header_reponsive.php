<div class="layout">

    <span class="tagline">Agenție de Turism. Verifici tariful pe site, primești răspunsul în timpul real</span>

    <div class="top-nav">

        <a href=""><i class="icon-heart"></i>Ofertele mele</a>

        <a href=""><i class="icon-group"></i>Despre noi</a>

    </div>

    <div class="container no_marginp" onclick="myFunction(this)">

        <nav role="navigation">

            <a href="/" class="logo"><img src="/images/logo.svg" class="svg"></a>

            <a href="">Early Booking</a>

            <a href="">Litoral România 2016</a>

            <a href="">Sejur România</a>

            <a href="">Stațiuni Balneare</a>

            <a href="">Turism Extern</a>

            <a href="">Circuite</a>

            <a href="">Seniori</a>

            <a href="contact.php" class="contact"><i class="icon-volume-control-phone"></i>Contactază-ne</a>

            <a href="javascript:void(0);" class="menu">

                <div class="bar1"></div>

                <div class="bar2"></div>

                <div class="bar3"></div>

            </a>

        </nav>

    </div>

</div>
