<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header/admin_bar.php'); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="NEW-header">
  <div class="wrap NEW-header">
    <?php /*?><div class="seoTextTop"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_seo_text_top.php"); ?></div><?php */?>
    <div id="LOGO"><a href="/" title="<?php echo $denumire_agentie; ?>"><img src="<?php echo $imgpath; ?>/logo.png" alt="<?php echo $denumire_agentie; ?>" width="240" height="50" /><span class="logo-text red">Agenție de turism - Prețuri corecte</span></a></div>
    <img src="/images/slogan.jpg" alt="Agentie de turism online" class="slogan">
    <div class="menuTop">
      <a href="/contact.html" title="Contact <?php echo $denumire_agentie; ?>" rel="nofollow" <?php if($_SERVER['REQUEST_URI']=="/contact.html") echo 'class="sel"'; ?>>Contact</a>
	  <a href="/despre-noi.html" title="Despre <?php echo $denumire_agentie; ?>" <?php if($_SERVER['REQUEST_URI']=="/despre-noi.html") echo 'class="sel"'; ?>>Despre noi</a>
	  <?php if(isset($_COOKIE['cos_cumparaturi'])) {
		  $array_cos_cumparaturi = explode('+;++;+', $_COOKIE['cos_cumparaturi']);
		  $count_cos = ' ('.sizeof($array_cos_cumparaturi).')';
	  } else {
		  $info_cos = ' - 0 salvate';
	  } ?>
      <a href="/ofertele-mele/" title="Ofertele mele<?php echo $count_cos.$info_cos; ?>" rel="nofollow" class="cosul-meu clearfix <?php if($_SERVER['REQUEST_URI']=="/ofertele-mele/") echo 'sel'; ?>" onclick="ga('send', 'event', 'meniu', 'cos cumparaturi', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');">Ofertele mele<?php echo $count_cos; ?></a>
	  <?php if($_SERVER['REQUEST_URI']=="/") { /*?>
      <span>Prima pagina</span>
      <?php*/ } else { ?>
      <a href="/" title="Prima pagina" rel="nofollow">Prima pagina</a>
      <?php } ?>
    </div>
    <?php //include($_SERVER['DOCUMENT_ROOT'].'/includes/header/new_marquee.php')?>

	<?php /*?><div class="marquee" id="top_marquee"></div><?php */?>
    <?php if($_SERVER['PHP_SELF']!='/rezervare_sejur_new.php') { ?>
    <div class="tel">
      <span class="red"><?php echo $contact_telefon; ?></span><br />
      <?php echo $contact_program; ?>
    </div>
    <?php } ?>
  </div>
</div>
<div id="NEW-header-sub">
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_meniu.php"); ?>
</div>
