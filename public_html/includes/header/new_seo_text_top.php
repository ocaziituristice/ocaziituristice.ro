<?php
/* default */
$seo_text_top = "";

$addrfile = $_SERVER['PHP_SELF'];

switch($addrfile) {

/* sejururi tari */
case "/tara_new1.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tari']));
	if($_REQUEST['tip']!="") $tematicaseo = " ".str_replace("_"," ",$_REQUEST['tip']);
	$seo_text_top = "Oferte ".$taraseo.$tematicaseo.", vacanta ".$taraseo.$tematicaseo.", sejur ".$taraseo.$tematicaseo.", agentii de turism";
	break;

/* sejururi zone */
case "/zone_new1.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$zonaseo = ucwords(desfa_link($_REQUEST['zone']));
	if($_REQUEST['tip']!="") $tematicaseo = " ".str_replace("_"," ",$_REQUEST['tip']);
	$seo_text_top = "Vacanta ".$zonaseo.$tematicaseo." in ".$taraseo.", cazare ".$zonaseo.$tematicaseo.", agentii de turism";
	break;

/* sejururi localitati */
case "/localitate_new1.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$localitateseo = ucwords(desfa_link($_REQUEST['oras']));
	if($_REQUEST['tip']!="") $tematicaseo = " ".str_replace("_"," ",$_REQUEST['tip']);
	$seo_text_top = "Oferte ".$localitateseo.$tematicaseo." in ".$taraseo.", sejur ".$localitateseo.$tematicaseo.", agentii de turism";
	break;

/* turism extern */	
case "/turism_extern.php":
	$seo_text_top = "Sejururi turism extern, early booking dar si last minute, cazare, circuite prezentate de agentiile de turism.";
	break;

/* turism extern tematica */	
case "/turism_extern_tematica.php":
	$tematicaseo = " ".ucwords(desfa_link($_REQUEST['tip']));
	$seo_text_top = "Oferte".$tematicaseo." alese pentru tine de la agentii de turism care ofera sejur".$tematicaseo.", cazare".$tematicaseo.", vacanta".$tematicaseo;
	break;

/* turism extern tematica tara */	
case "/tematica_tara.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$tematicaseo = " ".ucwords(desfa_link($_REQUEST['tip']));
	$seo_text_top = "Oferte ".$taraseo.$tematicaseo.", vacanta ".$taraseo.$tematicaseo.", sejur ".$taraseo.$tematicaseo.", agentii de turism";
	break;

/* turism extern tematica zone */
case "/tematica_zona.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$zonaseo = ucwords(desfa_link($_REQUEST['zone']));
	$tematicaseo = " ".ucwords(desfa_link($_REQUEST['tip']));
	$seo_text_top = "Vacanta ".$zonaseo.$tematicaseo." in ".$taraseo.", cazare ".$zonaseo.$tematicaseo.", agentii de turism";
	break;

/* turism extern tematica localitati */
case "/tematica_localitate.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$localitateseo = ucwords(desfa_link($_REQUEST['oras']));
	$tematicaseo = " ".ucwords(desfa_link($_REQUEST['tip']));
	$seo_text_top = "Oferte ".$localitateseo.$tematicaseo." in ".$taraseo.", sejur ".$localitateseo.$tematicaseo.", agentii de turism";
	break;

/* turism intern */	
case "/turism_intern.php":
	$seo_text_top = "Oferte Romania, cazare munte, cazare mare in Romania, nenumarate vacante in Romania de la agentii de turism";
	break;

/* last minute */
case "/last_minute.php":
	if(!isset($_REQUEST['tara'])) {
		$seo_text_top = "Oferte last minute, oferte speciale de la agentii de turism care te intampina cu reduceri de sezon, gasite in portalul de turism ocaziituristice.ro";
	} else if(isset($_REQUEST['tara'])) {
		$taraseo = ucwords(desfa_link($_REQUEST['tara']));
		$seo_text_top = "Last minute ".$taraseo.", oferte last minute ".$taraseo." de la agentii de turism cu oferte la super preturi pentru sejururi de neuitat in ".$taraseo."";
	}
	break;

/* early booking */
case "/early_booking.php":
	$seo_text_top = "Early booking - rezervi oferte early booking din timp si profiti de ofertele de preturi la cazari in locurile alese";
	break;

/* early booking tara */
case "/early_booking_tara.phpp":
	$taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$seo_text_top = "Early booking ".$taraseo." - oferte speciale cu preturi de early booking la oferte cazare ".$taraseo."";
	break;
case "/early_booking_zona.php":
    $taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$zonaseo = ucwords(desfa_link($_REQUEST['zone']));
	$seo_text_top = "Early booking ".$zonaseo." ".$taraseo." - oferte speciale cu preturi de early booking la oferte cazare ".$zonaseo."";
	break;
	
case "/early_booking_localitate.php":
    $taraseo = ucwords(desfa_link($_REQUEST['tari']));
	$orasseo = ucwords(desfa_link($_REQUEST['oras']));
	$seo_text_top = "Early booking ".$orasseo." ".$taraseo." - oferte speciale cu preturi de early booking la oferte cazare ".$orasseo."";
	break;
		
/* detalii oferta */
case "/detalii_oferta_new.php":
	$seo_text_top = $detalii['denumire']." la ".$detalii_hotel['denumire']." din ".$detalii_hotel['zona'].", ".$detalii_hotel['tara'];
	break;

/* circuite */
case "/circuite.php":
	$seo_text_top = "Circuite si excursii, oferte circuite in lume la obiective importante";
	break;

/* circuite continent */
case "/circuite_continent.php":
	$continentseo = ucwords(desfa_link($_REQUEST['continente']));
	$seo_text_top = "Circuit ".$continentseo.", oferte circuit ".$continentseo." de la agentii de turism in locuri exotice";
	break;

/* circuite tara */
case "/circuite_tara.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tara']));
	$seo_text_top = "Circuit ".$taraseo.", circuite ".$taraseo." la obiective turistice renumite, oferte ".$taraseo;
	break;

/* detalii circuite */
case "/detalii_circuit.php":
	$seo_text_top = $detalii_hotel['denumire'].", ".$detalii_hotel['nume_continent']." agentii de turism";
	break;

/* hoteluri */
case "/hoteluri.php":
	$seo_text_top = "Hoteluri din Romania, Hoteluri din strainatate, detalii si informatii detaliate, oferte de sezon, pareri si impresii";
	break;

/* hoteluri tari */
case "/hoteluri_tara.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tara']));
	$seo_text_top = "Hoteluri ".$taraseo.", detalii si informatii detaliate ".$taraseo.", oferte de sezon, pareri si impresii ".$taraseo;
	break;

/* hoteluri zone */
case "/hoteluri_zona.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tara']));
	$zonaseo = ucwords(desfa_link($_REQUEST['zona']));
	$seo_text_top = "Hoteluri ".$zonaseo." din ".$taraseo.", oferte si cazari in ".$zonaseo." ".$taraseo;
	break;

/* hoteluri localitati */
case "/hoteluri_localitate.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tara']));
	$zonaseo = ucwords(desfa_link($_REQUEST['zona']));
	$localitateseo = ucwords(desfa_link($_REQUEST['oras']));
	$seo_text_top = "Hoteluri din ".$localitateseo." - ".$taraseo.", cauta hoteluri in ".$taraseo;
	break;

/* detalii hoteluri */
case "/detalii_hotel_new.php":
	$seo_text_top = $detalii_hotel['denumire']." ".$detalii_hotel['stele']."* din ".$detalii_hotel['localitate']." - cauta hotel in ".$detalii_hotel['tara'];
	break;

/* descopera tara */
case "/descopera.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tara']));
	$seo_text_top = "Descopera frumusetile si misterele din ".$taraseo." alegand cele mai bune oferte ".$taraseo." sau hoteluri in ".$taraseo." cu ajutorul informatiilor furnizate de turisti si specialisti.";
	break;

/* descopera harta */
case "/descopera_harta.php":
	$taraseo = ucwords(desfa_link($_REQUEST['tara']));
	$seo_text_top = "Harta ".$taraseo." - harta turistica si rutiera ".$taraseo.", cladiri 3D din orasele importante din ".$taraseo;
	break;

/* partii ski Austria */
case "/partii_austria.php":
	$seo_text_top = "Partii ski in Austria, clasificarea lor, detalii sezon, precum si informatii despre partiile de ski din Austria";
	break;

/* detalii partii ski Austria */
case "/detalii_partii_ski.php":
	$seo_text_top = "Partie ski ".$detalii['denumire']." din Austria la altitudinea ".$detalii['alt_min']."m - ".$detalii['alt_max']."m, deschisa pe perioada ".$detalii['sezon'];
	break;

}

echo $seo_text_top;
?>