<?php
$cont=$cont.'
<div id="sub_litoral" class="submenu">
  <table border="0" cellpadding="0" cellspacing="0">
	<tr>
	  <td class="sm_column">
		<ul>
          <li class="titlu"><a href="/sejur-romania/litoral/" title="Litoral Romania">Romania</a></li>
          <li class="item"><a href="/sejur-romania/litoral/mamaia/">Mamaia</a></li>
          <li class="item"><a href="/sejur-romania/litoral/costinesti/">Costinesti</a></li>
          <li class="item"><a href="/sejur-romania/litoral/eforie-nord/">Eforie Nord</a></li>
          <li class="item"><a href="/sejur-romania/litoral/neptun/">Neptun</a></li>
          <li class="item"><a href="/sejur-romania/litoral/jupiter/">Jupiter</a></li>
          <li class="item"><a href="/sejur-romania/litoral/saturn/">Saturn</a></li>
		</ul>
	  </td>
	  <td class="sm_column">
		<ul>
          <li class="titlu"><a href="/sejur-bulgaria/litoral/" title="Litoral Bulgaria">Bulgaria</a></li>
          <li class="item"><a href="/sejur-bulgaria/litoral/albena/">Albena</a></li>
          <li class="item"><a href="/sejur-bulgaria/litoral/nisipurile-de-aur/">Nisipurile de Aur</a></li>
          <li class="item"><a href="/sejur-bulgaria/litoral/sunny-beach/">Sunny Beach</a></li>
          <li class="item"><a href="/sejur-bulgaria/litoral/sf-constantin-si-elena/">Sf Constantin si Elena</a></li>
          <li class="item"><a href="/sejur-bulgaria/litoral/duni/">Duni</a></li>
          <li class="item"><a href="/sejur-bulgaria/litoral/sunny-day/">Sunny Day</a></li>
		</ul>
	  </td>
	  <td class="sm_column">
		<ul>
          <li class="titlu"><a href="/sejur-grecia/" title="Litoral Grecia">Grecia</a></li>
          <li class="item"><a href="/sejur-grecia/creta/">Creta</a></li>
          <li class="item"><a href="/sejur-grecia/santorini/">Santorini</a></li>
          <li class="item"><a href="/sejur-grecia/corfu/">Corfu</a></li>
          <li class="item"><a href="/sejur-grecia/rodos/">Rodos</a></li>
          <li class="item"><a href="/sejur-grecia/kos/">Kos</a></li>
          <li class="item"><a href="/sejur-grecia/halkidiki/">Halkidiki</a></li>
		</ul>
	  </td>
	  <td class="sm_column">
		<ul>
          <li class="titlu"><a href="/sejur-turcia/" title="Litoral Turcia">Turcia</a></li>
          <li class="item"><a href="/sejur-turcia/antalya/alanya/">Antalya</a></li>
          <li class="item"><a href="/sejur-turcia/antalya/belek/">Belek</a></li>
          <li class="item"><a href="/sejur-turcia/antalya/kemer/">Kemer</a></li>
          <li class="item"><a href="/sejur-turcia/antalya/lara-kundu/">Lara-Kundu</a></li>
          <li class="item"><a href="/sejur-turcia/antalya/side/">Side</a></li>
          <li class="item"><a href="/sejur-turcia/kusadasi/">Kusadasi</a></li>
		</ul>
	  </td>
	  <td>
        <a href="/oferte-litoralul-pentru-toti/romania/litoral/" title="Litoralul pentru Toti"><img src="/images/header/litoralul-pentru-toti.jpg" alt="Litoralul pentru Toti"></a>
	  </td>
	</tr>
  </table>
</div>
';
?>