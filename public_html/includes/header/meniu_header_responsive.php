<?php if(isset($_COOKIE['cos_cumparaturi'])) {
		  $array_cos_cumparaturi = explode('+;++;+', $_COOKIE['cos_cumparaturi']);
		  $count_cos = ' ('.sizeof($array_cos_cumparaturi).')';
	  } else {
		  $info_cos = ' - 0 salvate';
	  } ?>

<div class="layout">
    <span class="tagline">Agenție de Turism. Verifici tariful pe site, primești răspunsul în timpul real</span>
    <div class="top-nav">
   
        <a href="https://www.ocaziituristice.ro/ofertele-mele/" rel="nofollow" title="Ofertele mele<?php echo $count_cos.$info_cos; ?>">
        <i class="icon-heart"></i>Ofertele mele <?php echo $count_cos; ?></a>
        <a href="https://www.ocaziituristice.ro/despre-noi.html"><i class="icon-group"></i>Despre noi</a>
    </div>
    <div class="" onclick="myFunction(this)">
        <nav role="navigation">
            <a href="/" class="logo"><img src="/images/logo.svg" class="svg"></a>
            <a href="https://www.ocaziituristice.ro/sejur-romania/litoral/" title="Litoral 2018" class="menu-item-litoral">
                <i class="fa fa-sun-o" aria-hidden="true"></i>
                Litoral 2018
            </a>
            <a href="https://www.ocaziituristice.ro/sejur-romania/" title="Sejur Romania" class="menu-item-sejur">
                <i class="fa fa-car" aria-hidden="true"></i>
                Sejur România
            </a>
            <a href="https://www.ocaziituristice.ro/sejur-romania/statiuni-balneare/" title="Statiuni balneare" class="menu-item-balneo">
                <i class="fa fa-thermometer-full" aria-hidden="true"></i>
                Stațiuni Balneare
            </a>
            <a href="https://www.ocaziituristice.ro/oferte-turism-extern/" title="Turism Extern" class="menu-item-plane">
                <i class="fa fa-plane" aria-hidden="true"></i>
                Turism Extern
            </a>
            <a href="https://www.ocaziituristice.ro/circuite/" title="Circuite Culturale" class="menu-item-map">
                <i class="fa fa-map-o" aria-hidden="true"></i>
                Circuite
            </a>
            <a href="https://www.ocaziituristice.ro/oferte-program-pentru-seniori/" title="Oferte pentru Seniori" class="menu-item-group">
                <i class="fa fa-users" aria-hidden="true"></i>
                Seniori
            </a>
            <a href="https://www.ocaziituristice.ro/circuite-pelerinaje/israel/" title="Pelerinaje Israel" class="menu-item-bell">
                <i class="fa fa-bell-o" aria-hidden="true"></i>
                Pelerinaje Israel
            </a>
            <a href="https://www.ocaziituristice.ro/contact.html" class="contact"><i
                        class="icon-volume-control-phone"></i>Contactază-ne</a>
            <a href="javascript:void(0);" class="menu">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </a>
        </nav>
    </div>
</div>
