
<!DOCTYPE html>

<html lang="ro">

<head>

    <meta charset="UTF-8" />

    <title>Ocaziituristice.ro</title>

    <meta name="description" content="" />

    <meta name="keywords" content="" />

    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />

    <link rel="shortcut icon" href="<?php echo PATH_IMAGES; ?>favicon.ico?<?php echo CACHE; ?>" />

    <link href="<?php echo $file_css; ?>" rel="stylesheet" type="text/css" />

     <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

<!--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>-->


    <script src="<?php echo $file_js; ?>"></script>
<!--        <link rel="stylesheet" href="jquery.ui.datepicker.mobile.css" />-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.5.3/datepicker.js"></script>-->
<!--        <script src="jquery.ui.datepicker.mobile.js"></script>-->
        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--    <link  rel="stylesheet" type="text/css" href="/js/classic.css">-->
<!--    <link  rel="stylesheet" type="text/css" href="/js/classic.date.css">-->
    <link  rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>daterangepicker.css">
<!--    <script src="--><?php //echo PATH_JS ?><!--picker.js"></script>-->
<!--    <script src="--><?php //echo PATH_JS ?><!--picker.date.js"></script>-->
<!--    <link rel="stylesheet"  href="http://code.jquery.com/mobile/git/jquery.mobile-git.css" />-->
<!--    <link rel="stylesheet" href="https://rawgithub.com/arschmitz/jquery-mobile-datepicker-wrapper/master/jquery.mobile.datepicker.css" />-->
<!--    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->






<!--    <script src="https://rawgithub.com/jquery/jquery-ui/1-10-stable/ui/jquery.ui.datepicker.js"></script>-->
<!--    <script src="http://code.jquery.com/mobile/git/jquery.mobile-git.js"></script>-->
<!--    <script src="https://rawgithub.com/arschmitz/jquery-mobile-datepicker-wrapper/master/jquery.mobile.datepicker.js"></script>-->
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery-2.2.4.min.js"></script>

   <link  rel="stylesheet" type="text/css"  href="<?php echo PATH_CSS ?>ocaziituristice.css">

    <link  rel="stylesheet" type="text/css"  href="<?php echo PATH_CSS ?>font-awesome.min.css">
    <link href="<?php echo PATH_JS ?>jquery_validate/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo PATH_JS ?>typeahead.bundle.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.masonery.min.js"></script>


    <script type="text/javascript">
        var tui = tui || {};
        tui.language = 'ro-RO';
    </script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript"  src="<?php echo PATH_JS ?>jquery.daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


    <script type="text/javascript" src="<?php echo PATH_JS ?>bootstrap-scrollspy-3.3.7.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>handlebars-v4.0.5.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery-deparam.min.js"></script>

    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.dotdotdot.min.js"></script>
    <script>
        dataLayer = [];
    </script>
    <script src="<?php echo PATH_JS ?>svginject.js?<?php echo CACHE ?>"></script>

        <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key_map ?>&language=ro"></script>

    
</head>

