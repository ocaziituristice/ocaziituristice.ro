   
   <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>
    <link rel="shortcut icon" href="<?php echo PATH_IMAGES; ?>favicon.ico?<?php echo CACHE; ?>"/>
    <link href="<?php echo $file_css; ?>" rel="stylesheet" type="text/css"/>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
  
   
      <?php /*?>  <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui/jquery-ui.structure.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui-custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>font-awesome.min.css">
    <link href="<?php echo PATH_JS ?>jquery_validate/validationEngine.jquery.css" rel="stylesheet" type="text/css"/>  <?php */?>
    
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>ocaziituristice.css">
    <?php /*?> <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/><?php */?>
    
    <link rel="stylesheet" type="text/css" href="https://www.ocaziituristice.ro/min/?g=css_ocazii_extra&f=more/ocazii_extra_invers.css">
    
    
    
<?php /*?>   <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.masonery.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.masonery.min.js"></script><?php */?>
 
    <?php /*?><script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script><?php */?>
  <?php /*?>  <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.daterangepicker.js"></script><?php */?>
<?php /*?>	  <script type="text/javascript" src="<?php echo PATH_JS ?>datepicker_ro.js"></script>
<?php */?>	
<?php /*?><script src="<?php echo $file_js; ?>"></script>
	  <script type="text/javascript" src="<?php echo PATH_JS ?>typeahead.bundle.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>bootstrap-scrollspy-3.3.7.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>handlebars-v4.0.5.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery-deparam.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.dotdotdot.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.jscroll.js"></script> 
	<script src="<?php echo PATH_JS ?>svginject.js?<?php echo CACHE ?>"></script><?php */?>
    <script src="https://www.ocaziituristice.ro/min/?g=js_ocazii_responsive&f=more/ocazii_responsive.js"></script>
	<script>
        dataLayer = [];
    </script>
   
<?php if($_SERVER['PHP_SELF']=="/detalii_hotel_new.php") {    ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key_map ?>&language=ro"></script>
      <script src="/js_responsive/jquery.popdown.js"></script>
   <link rel="stylesheet" type="text/css" href="/css_responsive/jquery.popdown.css">
    <?php }?>
<?php if($_SERVER['PHP_SELF']=="/detalii_circuit.php") {    ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
      <script src="/js_responsive/jquery.popdown.js"></script>
   <link rel="stylesheet" type="text/css" href="/css_responsive/jquery.popdown.css">
    <?php }?>    
