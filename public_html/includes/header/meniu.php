<div id="main-menu"><!-- Start menu -->
  <ul>
    <li class="first"><a href="/" title="Ocazii Turistice portal pentru agentii de turism" rel="nofollow">Home</a></li>
    <li class="firstNone"><a href="/last-minute/" title="Last minute,oferte last minute" style="color:#FF0;">Last Minute</a></li>
    <li class="firstNone"><a href="/early-booking/" title="Oferte Early booking" style="color:#0F0;">Early Booking</a></li>
    <li class="firstNone"><a href="/oferte-turism-extern/" title="Oferte Turism Extern">Turism Extern</a></li>
    <li class="firstNone"><a href="/oferte-turism-intern/" title="Oferte Turism Intern">Turism Intern</a></li>
    <li class="firstNone"><a href="/circuite/" title="Circuite">Circuite</a></li>
    <li class="firstNone"><a href="/hoteluri/" title="Cazare, Hoteluri">Hoteluri</a></li>
    <?php /*?><li class="firstNone"><a href="/pensiuni/pensiuni.html" title="Cazare pensiuni, cazare hoteluri">Cazari Romanesti</a></li><?php */?>
    <li style="border:0;"><a id="menu1" title="Tematici pentru sejururi" onmouseover="selectMenu('1');">Tematici</a></li>
  </ul>
</div><!-- End menu -->
<div id="submenu1" onmouseout="startDeselect('1');">
  <table width="100%" cellspacing="0" cellpadding="0" border="0" onmouseout="startDeselect('1');" onmouseover="cancelDeselect();">
    <tr>
      <td valign="top">
        <ul onmouseout="startDeselect('1');" onmouseover="cancelDeselect();">
          <li class="first" style="height:17px;">Tematici Sejururi</li>
          <li><a href="/oferte-revelion/" title="Oferte Revelion 2012">Revelion 2012</a></li>
          <li><a href="/oferte-paste/" title="Oferte Paste 2012">Paste</a></li>
          <li><a href="/oferte-sejur-ski/" title="Oferte Sejur Ski">Sejur Ski</a></li>
          <li><a href="/oferte-romantic/" title="Oferte Romantic">Romantic</a></li>
          <li><a href="/oferte-city-break/" title="Oferte City Break">City Break</a></li>
          <li><a href="/oferte-luna-de-miere/" title="Oferte Luna de Miere">Luna de Miere</a></li>
          <li><a href="/oferte-program-pentru-seniori/" title="Program pentru Seniori">Program pentru Seniori</a></li>
        </ul>
      </td>
    </tr>
  </table>
</div>
