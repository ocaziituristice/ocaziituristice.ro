<?php include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php"); ?>
<script type="text/javascript">
$(function () {
	$('marquee').marquee('pointer').mouseover(function () {
		$(this).trigger('stop');
	}).mouseout(function () {
		$(this).trigger('start');
	}).mousemove(function (event) {
		if ($(this).data('drag') == true) {
			this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
		}
	}).mousedown(function (event) {
		$(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
	}).mouseup(function () {
		$(this).data('drag', false);
	});
});
</script>
<marquee behavior="scroll" direction="left" scrollamount="1">

<img src="<?php echo $imgpath; ?>/header/plaja-turcia.png" alt="Sejur Turcia"  width="71" height="32"/> <a href="<?php echo $sitepath; ?>sejur-turcia/" title="Sejur Turcia, cazare turcia" class="link-blue">Charter Avion TURCIA</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<img src="<?php echo $imgpath; ?>/header/plaja-grecia.png" alt="sejur grecia, cazare grecia"  width="71" height="32"/> <a href="<?php echo $sitepath; ?>sejur-grecia/" title="Sejur Grecia" class="link-blue">Charter Avion GRECIA</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<img src="<?php echo $imgpath; ?>/header/plaja-bulgaria.png" alt="Litoral Bulgaria"  width="71" height="32"/> <a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/" title="Litoral Bulgaria" class="link-blue">Vara BULGARIA  </a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</marquee>