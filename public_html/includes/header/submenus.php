
<div id="sub_balneare_statiuni" class="submenu">
  <table border="0" cellpadding="0" cellspacing="0">
	<tr>
	  <td class="sm_column">
		<ul>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/baile-herculane/">Baile Herculane</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/baile-tusnad/">Baile Tusnad</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/covasna/">Covasna</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/slanic-moldova/">Slanic Moldova</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/sovata/">Sovata</a></li>
		</ul>
	  </td>
	  <td class="sm_column">
		<ul>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/baile-felix/">Baile Felix</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/baile-govora/">Baile Govora</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/baile-olanesti/">Baile Olanesti</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/calimanesti-caciulata/">Calimanesti Caciulata</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/amara/">Amara</a></li>
		</ul>
	  </td>
	  <td class="sm_column">
		<ul>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/baile-balvanyos/">Baile Balvanyos</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/geoagiu-bai/">Geoagiu Bai</a></li>
		  <li class="item"><a href="/sejur-romania/statiuni-balneare/vatra-dornei/">Vatra Dornei</a></li>
		</ul>
	  </td>
	</tr>
  </table>
</div>

<div id="sub_sejur_romania" class="submenu">
  <table border="0" cellpadding="0" cellspacing="0">
	<tr>
	  <td class="sm_column">
		<ul>
          <li class="titlu"><a href="/sejur-romania/litoral/" title="Litoral Romania">Litoral</a></li>
          <li class="item"><a href="/cazare-mamaia/">Mamaia</a></li>
          <li class="item"><a href="/cazare-eforie-nord/">Eforie Nord</a></li>
          <li class="item"><a href="/cazare-jupiter/">Jupiter</a></li>
          <li class="item"><a href="/cazare-neptun/">Neptun</a></li>
          <li class="item"><a href="/cazare-olimp/">Olimp</a></li>
          <li class="item"><a href="/cazare-saturn/">Saturn</a></li>
          <li class="item"><a href="/cazare-venus/">Venus</a></li>
		</ul>
	  </td>
	  <td class="sm_column">
		<ul>
          <li class="titlu"><a href="/sejur-romania/statiuni-balneare/" title="Statiuni Balneare Romania">Statiuni Balneare</a></li>
          <li class="item"><a href="/cazare-sovata/">Sovata</a></li>
          <li class="item"><a href="/cazare-calimanesti/">Calimanesti</a></li>
          <li class="item"><a href="/cazare-baile-olanesti/">Baile Olanesti</a></li>
          <li class="item"><a href="/cazare-baile-herculane/">Baile Herculane</a></li>
          <li class="item"><a href="/cazare-baile-felix/">Baile Felix</a></li>
          <li class="item"><a href="/cazare-baile-tusnad/">Baile Tusnad</a></li>
          <li class="item"><a href="/cazare-slanic-prahova/">Slanic Prahova</a></li>
		</ul>
	  </td>
	  <td class="sm_column">
		<ul>
          <li class="titlu">Munte</li>
          <li class="item"><a href="/sejur-romania/valea-prahovei/">Valea Prahovei</a></li>
          <li class="item"><a href="/sejur-romania/poiana-brasov/">Poiana Brasov</a></li>
          <li class="item"><a href="/sejur-romania/bran-moeciu-rucar/">Bran-Moeciu</a></li>
		  <li class="item"><a href="/cazare-sambata-de-sus/">Sambata de Sus</a></li>
		  <li class="item"><a href="/sejur-romania/parang-ranca/">Ranca - Straja</a></li>
		  <li class="item"><a href="/cazare-gura-humorului/">Gura humorului</a></li>
		</ul>
	  </td>
	</tr>
  </table>
</div>
