    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>
    <link rel="shortcut icon" href="<?php echo PATH_IMAGES; ?>favicon.ico?<?php echo CACHE; ?>"/>
    <link href="<?php echo $file_css; ?>" rel="stylesheet" type="text/css"/>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="<?php echo $file_js; ?>"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>ocaziituristice.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>font-awesome.min.css">
    <script src="<?php echo PATH_JS ?>svginject.js?<?php echo CACHE ?>"></script>
