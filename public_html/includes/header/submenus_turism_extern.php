<div id="sub_turism_extern" class="submenu clearfix">
<ul class="ext">
<li class="turism_extern"><a href="/sejur-turcia/" title="Sejur Turcia" class="country" style="background-image:url(/thumb_steag_tara/3378turcia1steag_tara.jpg)">Turcia</a></li>
<li class="turism_extern"><a href="/sejur-grecia/" title="Sejur Grecia" class="country" style="background-image:url(/thumb_steag_tara/5304grecia1steag_tara.jpg)">Grecia</a></li>
<li class="turism_extern"><a href="/sejur-bulgaria/" title="Sejur Bulgaria" class="country" style="background-image:url(/thumb_steag_tara/1090bulgaria1steag_tara.jpg)">Bulgaria</a></li>
<li class="turism_extern"><a href="/sejur-emiratele-arabe-unite/dubai/" title="Sejur Dubai" class="country" style="background-image:url(/thumb_steag_tara/5856emiratele_arabe_unite1steag_tara.jpg)">Dubai</a></li>
<li class="turism_extern"><a href="/sejur-spania/" title="Sejur Spania" class="country" style="background-image:url(/thumb_steag_tara/3490spania1steag_tara.jpg)">Spania</a></li>
<li class="turism_extern"><a href="/sejur-italia/" title="Sejur Italia" class="country" style="background-image:url(/thumb_steag_tara/4763italia1steag_tara.jpg)">Italia</a></li>
</ul>
<ul class="ext">
<li class="turism_extern"><a href="/sejur-cipru/" title="Sejur Cipru" class="country" style="background-image:url(/thumb_steag_tara/3244cipru1steag_tara.jpg)">Cipru</a></li>
<li class="turism_extern"><a href="/sejur-croatia/" title="Sejur Croatia" class="country" style="background-image:url(/thumb_steag_tara/1786croatia1steag_tara.jpg)">Croatia</a></li>
<li class="turism_extern"><a href="/sejur-portugalia/" title="Sejur Portugalia" class="country" style="background-image:url(/thumb_steag_tara/6733portugalia1steag_tara.jpg)">Portugalia</a></li>
<li class="turism_extern"><a href="/sejur-china/" title="Sejur China" class="country" style="background-image:url(/thumb_steag_tara/5900china1steag_tara.jpg)">China</a></li>
<li class="turism_extern"><a href="/sejur-india/" title="Sejur India" class="country" style="background-image:url(/thumb_steag_tara/)">India</a></li>
<li class="turism_extern"><a href="/sejur-maroc/" title="Sejur Maroc" class="country" style="background-image:url(/thumb_steag_tara/6528maroc1steag_tara.jpg)">Maroc</a></li>
</ul>
<ul class="ext">
<li class="turism_extern"><a href="/sejur-marea-britanie/" title="Sejur Marea Britanie" class="country" style="background-image:url(/thumb_steag_tara/6004marea_britanie1steag_tara.jpg)">Marea Britanie</a></li>
<li class="turism_extern"><a href="/sejur-thailanda/" title="Sejur Thailanda" class="country" style="background-image:url(/thumb_steag_tara/6880thailanda1steag_tara.jpg)">Thailanda</a></li>
<li class="turism_extern"><a href="/sejur-sua/" title="Sejur SUA" class="country" style="background-image:url(/thumb_steag_tara/5651sua211steag_tara.jpg)">SUA</a></li>
<li class="turism_extern"><a href="/sejur-suedia/" title="Sejur Suedia" class="country" style="background-image:url(/thumb_steag_tara/4081suedia1steag_tara.jpg)">Suedia</a></li>
<li class="turism_extern"><a href="/sejur-albania/" title="Sejur Albania" class="country" style="background-image:url(/thumb_steag_tara/)">Albania</a></li>
<li class="turism_extern"><a href="/sejur-tunisia/" title="Sejur Tunisia" class="country" style="background-image:url(/thumb_steag_tara/)">Tunisia</a></li>
</ul>
</div>
