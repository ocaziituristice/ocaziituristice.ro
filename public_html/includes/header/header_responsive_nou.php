    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>
    <link rel="shortcut icon" href="<?php echo PATH_IMAGES; ?>favicon.ico?<?php echo CACHE; ?>"/>
    <link href="<?php echo $file_css; ?>" rel="stylesheet" type="text/css"/>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="<?php echo $file_js; ?>"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui/jquery-ui.structure.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>jquery-ui-custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>ocaziituristice.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>font-awesome.min.css">
    <link href="<?php echo PATH_JS ?>jquery_validate/validationEngine.jquery.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo PATH_JS ?>typeahead.bundle.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.masonery.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.masonery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    
	<script type="text/javascript" src="<?php echo PATH_JS ?>jquery.daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>datepicker_ro.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS ?>daterangepicker.css">
    
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <script type="text/javascript" src="<?php echo PATH_JS ?>bootstrap-scrollspy-3.3.7.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>handlebars-v4.0.5.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery-deparam.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.dotdotdot.min.js"></script>
    <script>
        dataLayer = [];
    </script>
    <script src="<?php echo PATH_JS ?>svginject.js?<?php echo CACHE ?>"></script>

    
