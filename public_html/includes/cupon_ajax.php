<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/config_responsive.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/cupoane.php' );
$cupoane      = new CUPOANE();

if ( isset( $_REQUEST['cupon_code'] ) ) {
	$register_coupon = $cupoane->inregistreaza_cupon( $_REQUEST['cupon_code'] );
	if ( $register_coupon == 'eroare' ) { ?>
        <h2 class="red">EROARE! Codul introdus nu este corect sau a expirat!</h2>
        <h3 class="black">Va rugam sa reintroduceti codul sau sa ne <a href="/contact.html" target="_blank"><span
                        class="link-blue">contactati</span></a></h3>
        <div class="clearfix black">
            <div id="coupon-rezervare" class="coupon-rezervare">
                <strong class="red bigger-11em">Adaugă cupon:</strong>
                <input type="text" name="cod_cupon" id="cod_cupon">
                <div id="adauga-cupon"
                     class="button-blue"
                     style="padding:3px 8px; font-size:13px; -webkit-border-radius:4px; border-radius:4px;"
                     onclick="checkCoupon();">
                    Adauga
                </div>
            </div>
        </div>
	<?php } else if ( $coupon_valid == 'nu' ) { ?>
        <h2 class="red">EROARE! Acest cupon nu poate fi folosit pentru aceasta destinatie.</h2>
        <h3 class="black">Va rugam sa verificati destinatiile valide ale cuponului in mailul primit cu codul
            acestuia.</h3>
        <div class="clearfix black">
            <div id="coupon-rezervare" class="coupon-rezervare">
                <strong class="red bigger-11em">Adaugă cupon:</strong>
                <input type="text" name="cod_cupon" id="cod_cupon">
                <div id="adauga-cupon"
                     class="button-blue"
                     style="padding:3px 8px; font-size:13px; -webkit-border-radius:4px; border-radius:4px;"
                     onclick="checkCoupon();">
                    Adauga
                </div>
            </div>
        </div>
	<?php } else { ?>
		<?php if ( $register_coupon['tip_valoare'] == 'procent' ) {
			$register_coupon['valoare_campanie'] = round( $_REQUEST['total_price'] * $register_coupon['valoare_campanie'] / 100, 0 );
			$register_coupon['moneda_campanie']  = urldecode($_REQUEST['moneda']);
		}
		?>
        <div class="clearfix">
            <div class="title red new_check coupon-reservation-update">
                <input type="checkbox" name="coupon"
                       value="- Cupon de Reducere = -<?php echo $register_coupon['valoare_campanie'] . ' ' . moneda( $register_coupon['moneda_campanie'] ); ?>"
                       rel="- <?php echo $register_coupon['valoare_campanie']; ?>"
                       data-value="<?php echo $register_coupon['valoare_campanie'] ?>"
                       class="chOpt"
                       id="coupon-reservation">
                <label class="bold" for="coupon-reservation">
                    Reducere conform campaniei <span class="blue">CUPON DE REDUCERE</span>
                </label><br/>
                <span class="black italic smaller-09em">
                                                                Sunt de acord să folosesc CUPONUL DE REDUCERE, conform termenilor și condițiilor de folosire și am luat la cunoștință că pot să îl folosesc doar 1 dată. <strong>Detalii</strong> <a
                            href="/info-cupoane-reducere.html"
                            target="_blank" rel="nofollow"
                            onClick="ga('send', 'event', 'cupon reducere', 'citire mai multe detalii', '<?php echo substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI']; ?>');"><span
                                class="link-blue"><strong>cupoane de reducere</strong></span></a></span>
                <div class="value red"
                     style="font-size: 20px;margin-top: 10px;">
					<?php echo '- ' . $register_coupon['valoare_campanie'] . ' ' . moneda( $register_coupon['moneda_campanie'] ); ?>
                </div>
            </div>
        </div>
        <script>
            var li = '<li style="font-size: 18px;padding-bottom: 0;display: none;">';
            li += '<span class="red" style="float: left">Cupon</span>';
            li += '<span class="red" style="float: right"><span class="discount-value">';
            li += '<?php echo '- ' . $register_coupon['valoare_campanie'] . ' ' . moneda( $register_coupon['moneda_campanie'] ); ?>';
            li += '</span></span><div class="clear"></div></li>';
            $(document).find('.box_rez_sumar_top_in').append(li);
        </script>
		<?php
	}
}
?>