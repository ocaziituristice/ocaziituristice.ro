<ul class="trustme">
	<li>
		<i class="icon-volume-control-phone"></i>
		<p>
			<strong>Rezervări online sau telefonice</strong>
			<a href="tel:+40213117464" class="phone">021.311.7464</a>
		</p>
	</li>
	<li>
		<i class="icon-thumbs-up-alt"></i>
		<p><strong>Siguranță</strong>. Avem parteneri pe cei mai mari tour-operatori din țară și din afară (<a href="/info_parteneri_popup.php" target="_blank" rel="nofollow" class="popdown" title="Parteneri Ocaziituristice.ro"> <span class="blue">vezi »</span></a>
)</p>
	</li>
	<li>
		<i class="icon-thumbs-up-alt"></i>
		<p><strong>Transparență</strong>. Afișăm <strong>toate detaliile</strong> necesare la fiecare ofertă. <strong>Fără taxe ascunse</strong>!</p>
	</li>
	<li>
		<i class="icon-thumbs-up-alt"></i>
		<p>Multiple metode de plată (<strong><a href="/info_cum_platesc_popup.php" target="_blank" rel="nofollow" class="popdown" title="Modalitati de plata Ocaziituristice.ro"><span class="blue">vezi »</span></a></strong>) <strong>Comisioane bancare <span class="col-red">ZERO!</span></strong></p>
	</li>
	<li>
		<i class="icon-thumbs-up-alt"></i>
		<p>Afișăm disponibilitatea locurilor de cazare și în avion <strong class="col-red">ÎN TIMP REAL</strong>!</p>
	</li>
	<li>
		<i class="icon-thumbs-up-alt"></i>
		<p>Capital Social al Agenției de Turism <strong>50.000 Lei</strong>!<br><a href="/info_licenta_popup.php" title="Licenta de turism Ocaziituristice.ro" class="popdown">Licență de turism valabila</a> si <a href="/info_polita_popup.php" title="Polita de asigurare Ocaziituristice.ro" class="popdown">polita de asigurare de insolvabilitate</a></p>
	</li>


</ul>
<script>
$(document).ready(function(){
  $('.popdown').popdown();
});
</script>







