<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/seteri.php');
include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head_colorbox.php');
$id_hotel=$_REQUEST['id_hotel'];
$selH="select hoteluri.poza1, hoteluri.nume, hoteluri.latitudine, hoteluri.longitudine, hoteluri.stele, hoteluri.adresa, hoteluri.nr, tari.denumire as tara, localitati.denumire as localitate from hoteluri left join localitati on hoteluri.locatie_id = localitati.id_localitate left join zone on localitati.id_zona = zone.id_zona left join tari on zone.id_tara = tari.id_tara where id_hotel = '".$id_hotel."' ";
$queH=mysql_query($selH) or die(mysql_error());
$rowH=mysql_fetch_array($queH);
@mysql_free_result($queH); ?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ro"></script>
<script type="text/javascript">

  function initialize() {
	var mapDiv = document.getElementById('map-canvas');
	var map = new google.maps.Map(mapDiv, {
	  center: new google.maps.LatLng(<?php echo $rowH['latitudine']; ?>, <?php echo $rowH['longitudine']; ?>),
	  zoom: 15,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	});
  
	var content = '<h4 class="blue"><?php echo $rowH['nume']; ?> <img src="<?php echo $imgpath; ?>/spacer.gif" class="stele-mici-<?php echo $rowH['stele']; ?>" alt="" /></h4><img src="<?php echo $sitepath_parinte; ?>thumb_hotel/<?php echo $rowH['poza1']; ?>" style="float:left; margin-right:10px; width:60px;" /><?php echo $rowH['adresa'].' Nr. '. $rowH['nr'].', '.$rowH['localitate'].', '.$rowH['tara']; ?>';
  
	var infowindow = new google.maps.InfoWindow({
	  content: content
	});
  
	var image = '<?php echo $imgpath; ?>/icon_maps_hotel.png';
	var marker = new google.maps.Marker({
	  map: map,
	  position: map.getCenter(),
	  icon: image
	});
  
	infowindow.open(map, marker);
  }
  

  google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-canvas" class="map_canvas" style="width:100%; height:97%;">
</div>
<div class="text-center smaller-09em grey">Poziționarea hotelurilor pe hartă este cu titlu informativ.</div>
