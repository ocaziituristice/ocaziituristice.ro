          <ul >
            <li>După ce v-ați hotărât asupra ofertei și a perioadei de plecare, apăsaţi butonul <strong>&quot;Rezervă&quot;</strong>, completaţi detaliile cerute conform instrucțiunilor şi apăsaţi <strong>&quot;Continuă rezervarea&quot;</strong> sau <strong>&quot;Trimite&quot;</strong>.</li>
            <li>În scurt timp un operator Ocazii Turistice va prelua cererea dumneavoastră de rezervare și vă va contacta pentru a vă confirma disponibilitatea locurilor, sau pentru a vă recomanda altă ofertă asemănătoare, în cazul în care nu mai sunt locuri libere la hotelul care îl doriți dumneavoastră.</li>
            <li>După ce se obțin toate informațiile necesare, se va încheia un contract (<a href="/files/Contractul%20cu%20Turistul.pdf" target="_blank" rel="nofollow"><span class="blue">contractul cu turistul</span></a>), în baza căruia se va emite o factura proformă.</li>
            <li>Veți primi pe email factura proformă împreună cu contractul cu turistul, precum și instrucțiunile necesare. După ce efectuați plata aferentă facturii proforme (<a href="/info-cum-platesc.html" target="_blank" rel="nofollow" class="infos link-blue"><span class="blue">vezi aici modalitățile de plată</span></a>), vă rugăm să ne anunțați și, dacă aveți posibilitatea, să ne trimiteți pe email dovada plății, pentru a scurta timpul de emitere al restului de documente.<br />
            ATENȚIE! Factura proformă are valabilitate în general 72 de ore, după care tariful se poate modifica din diferite motive (schimbare curs valutar, încheiere ofertă promoțională, etc.), sau camerele pot să nu mai fie disponibile (cineva a achiziționat camerele după ce acestea au fost repuse în starea &quot;available&quot;).</li>
            <li>După înregistrarea plății se va emite factura fiscală aferentă plății înregistrate (puteți plăti întreaga sumă pentru pachetul rezervat sau doar un avans, conform instrucțiunilor primite de la operatorul Ocazii Turistice).</li>
            <li>După plata integrală a pachetului rezervat, se vor emite voucherele de călătorie.<br />
            ATENȚIE! În cazul pachetelor cu transport cu avionul și uneori și cele cu transport cu autocarul, biletele se vor emite cu câteva zile înainte de plecare (3-7 zile).</li>
            <li>Toate documentele de mai sus se vor emite în funcție de următoarele criterii:
              <ul style="margin-bottom:0; padding-bottom:0;">
                <li><strong class="blue bigger-12em">FIZIC</strong> - dacă clientul se prezintă la sediul agenției, sau dacă clientul nu se prezintă la sediul agenției însă face o solicitare specială pentru expedierea tuturor documentelor prin poștă</li>
                <li><strong class="blue bigger-12em">ELECTRONIC </strong>- dacă clientul nu se prezintă la sediul agenției</li>
              </ul>
            </li>
            <li>Rezervarea o puteți face de oriunde (intra sau extra comunitar) și se poate achita ori de pe teritoriul României, ori din afara acestuia.</li>
          </ul>