<?php
if($_SERVER['PHP_SELF']=='/index.php' or $_SERVER['PHP_SELF']=='/rezervarea_mea.php' or $_SERVER['PHP_SELF']=='/black-friday.php') {
	$show_news = 'nu';
	$clstl = 'style="border-top:1px solid #CCC;"';
} else {
	$show_news = 'da';
	$clstl = '';
}

if($show_news=='da') include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php");
?>
<div id="NEW-footer" <?php echo $clstl; ?>>
  <div class="wrap clearfix">
    
    <div id="footer_links" class="clearfix">
      <div class="column" style="width:175px;">
        <p class="titlu red"><?php echo $denumire_agentie; ?></p>
        <a href="/" title="Prima pagina" class="item">Prima pagina</a>
        <a href="/despre-noi.html" title="Despre noi" class="item" rel="nofollow">Despre noi</a>
        <a href="/termeni_si_conditii.html" title="Termeni si Conditii de utilizare" class="item" rel="nofollow">Termeni si Conditii</a>
        <a href="/contact.html" title="Contact" class="item" rel="nofollow">Contact</a>
        <a href="http://www.anpc.gov.ro/" title="Autoritatea Nationala pentru Protectia Consumatorilor" class="item" target="_blank" rel="nofollow">ANPC - 0219551</a>
      </div>
      
      <div class="column" style="width:150px;">
        <p class="titlu red">Utile</p>
        <a href="/info-cum-rezerv.html" title="Pasii ce trebuie parcursi pentru a efectua o rezervare" class="item" rel="nofollow">Cum rezerv</a>
        <a href="/info-cum-platesc.html" title="Modalitatile de plata disponibile" class="item" rel="nofollow">Cum platesc</a>
        <a href="/info-cookieuri.html" title="Informatii despre cookie-uri" class="item" target="_blank" rel="nofollow" onClick="setCookie('acceptCookies','da',3650);ga('send', 'event', 'cookie popup', 'citire mai multe detalii', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');">Despre cookie-uri</a>
        <a href="/info-asigurari-de-calatorie.html" title="Asigurari de calatorie" class="item" rel="nofollow">Asigurari de calatorie</a>
        <a href="/files/Regimul%20de%20vize%20pentru%20cetatenii%20romani.pdf" title="Regimul de vize pentru cetatenii romani" class="item" target="_blank" rel="nofollow">Regimul de vize</a>
        <?php /*?><a href="http://profitshare.ro/advertiseri/ocaziituristice" title="Devino partener Ocaziituristice.ro" class="item bold" target="_blank" rel="nofollow"><span class="blue">Afiliere Ocaziituristice.ro</span></a><?php */?>
      </div>
      <?php /*?><div class="column" style="width:175px;">
        <p class="titlu red">Link-uri utile</p>
        <a href="/files/Contractul%20cu%20Turistul.pdf" title="Contractul cu turistul" target="_blank" class="item" rel="nofollow">Contractul cu turistul</a>
      </div><?php */?>
      
      <?php /*?><div class="column" style="width:150px;">
        <p class="titlu red">Info</p>
        <a href="/files/licenta_turism.jpg" title="Licenta Turism" class="item" rel="nofollow" target="_blank">Licenta Turism</a>
        <a href="/files/asigurare.jpg" title="Polita Asigurare" class="item" rel="nofollow" target="_blank">Polita Asigurare</a>
      </div><?php */?>
      
<?php /*?>      <div class="column clearfix" style="width:240px;">
        <p class="titlu red">Cumparaturi online in siguranta</p>
        <div class="float-left text-center">
          <br>
	      <a href="https://secure2.plationline.ro/compliance.asp?f_login=www%2Eocaziituristice%2Ero&amp;f_lang=ro" target="_blank" rel="nofollow"><img src="https://secure2.plationline.ro/compliance/images_sigle_po/sigla_verificare_PlatiOnline.ro_170x43_2_RO.png" alt="Sigla PlatiOnline.ro" /></a><br />
	      <img src="https://secure2.plationline.ro/images/sigle/80px-Mastercard_SecureCode_Logo.png" alt="MasterCard SecureCode" />
	      <img src="https://secure2.plationline.ro/images/sigle/80px-VerifiedByVISA_Logo.png" alt="Verified by VISA" />
        </div>
        <div class="float-left">
	      <?php //include($_SERVER['DOCUMENT_ROOT']."/includes/marca_trusted.php"); ?>
        </div>
      </div><?php */?>
      <div class="column clearfix" style="width:240px;">
        <p class="titlu red">Cumparaturi online in siguranta</p>
        <div class="float-left text-center">
      <img src="/images/MobilPayLogo.png" alt="Plati prin Mobil Pay" width="210" height="205" />
      
     </div>
      </div>
      <div class="column" style="width:130px; text-align:center;">
		<?php $sel_curs_valutar = "SELECT * FROM curs_valutar ";
        $que_curs_valutar = mysql_query($sel_curs_valutar) or die(mysql_error());
        $row_curs_valutar = mysql_fetch_array($que_curs_valutar); ?>
        <p class="titlu red">Curs valutar BNR<br><span class="bigger-12em black"><?php echo date("d.m.Y",strtotime($row_curs_valutar['data'])); ?></span></p>
        <p class="bigger-13em" style="margin-top:15px;">EUR (euro):<br><strong class="black"><?php echo str_replace(".",",",$row_curs_valutar['EURO']); ?> Lei</strong></p>
      </div>
      
      
      <div class="column" style="float:right; width:175px;">
        <p class="titlu red">Ne gasesti pe</p>
        <a href="<?php echo $facebook_page; ?>" rel="nofollow" target="_blank" title="Facebook <?php echo $denumire_agentie; ?>"><img src="/images/social/facebook-32x32.png" alt="Facebook <?php echo $denumire_agentie; ?>"></a>
        <a href="https://plus.google.com/100372161073365189899?rel=author" target="_blank" title="Google Plus <?php echo $denumire_agentie; ?>"><img src="/images/social/google-plus-32x32.png" alt="Google Plus <?php echo $denumire_agentie; ?>"></a>
        <a href="https://twitter.com/ocaziituristice" rel="nofollow" target="_blank" title="Twitter <?php echo $denumire_agentie; ?>"><img src="/images/social/twitter-32x32.png" alt="Twitter <?php echo $denumire_agentie; ?>"></a>
        <a href="http://pinterest.com/ocaziituristice/" rel="nofollow" target="_blank" title="Pinterest <?php echo $denumire_agentie; ?>"><img src="/images/social/pinterest-32x32.png" alt="Pinterest <?php echo $denumire_agentie; ?>"></a>
        <a href="http://blog.ocaziituristice.ro" rel="nofollow" target="_blank" title="Blog <?php echo $denumire_agentie; ?>"><img src="/images/social/blog-32x32.png" alt="Twitter <?php echo $denumire_agentie; ?>"></a>
        <br class="clear"><br>
        <p><strong>Telefon:</strong> <span class="red" style="font-size:16px; font-weight:bold;"><?php echo $contact_telefon; ?></span></p>
        <p style="padding-bottom:0;"><strong>Adresa:</strong><br><?php echo $contact_adresa; ?></p>
      </div>
      
      <br class="clear" />
    </div>
    
    <div class="copyright clearfix">
      <div class="float-left">
		Copyright &copy; 2008 - <?php echo date('Y'); ?> Agentia de turism <a href="<?php echo $sitepath; ?>" title="Agentie de turism" class="link-blue">www.ocaziituristice.ro</a>.
		<br>
<?php /*?>		<div class="mar10-0">
          <a href="/early-booking/" title="Oferte Early Booking" class="link-blue">Early Booking</a>
          &nbsp; | &nbsp;
         <a href="/oferte-revelion/" title="Oferte Revelion 2015" class="link-blue">Revelion 2015</a>
          &nbsp; | &nbsp;
          <a href="/oferte-craciun/" title="Oferte Craciun 2014" class="link-blue">Craciun 2014</a>
          &nbsp; | &nbsp;
          <a href="/oferte-paste/" title="Oferte Paste 2015" class="link-blue">Paste 2015</a>
        </div> <?php */?>
	  </div>
      <div class="trafic">
        <img src="<?php echo $imgpath; ?>/logo_teztour.gif" alt="Tez Tour" class="float-left mar5" />
        
      </div>
    </div>
    
  </div>
</div>

<?php /*?><a href="<?php echo $sitepath; ?>cerere-oferta-personalizata.html" title="Cerere Oferta Personalizata" class="NEW-cerere-oferta">Cerere Oferta Personalizata</a><?php */?>

<?php /*?><?php if((!isset($_COOKIE['acceptCookies'])) or (isset($_COOKIE['cupon']))) { ?>
<div class="infos-right">
  <?php if(isset($_COOKIE['cupon'])) { ?>
  <div id="info-coupon">
    <p class="clearfix black"><strong>ATENȚIE !</strong></p>
    <p>Ai înregistrat un<br><strong>CUPON DE REDUCERE</strong>!</p>
    <p>Nu uita să îl folosești la <strong>rezervarea online</strong>!</p>
    <p>Mai multe detalii <a href="/info-cupoane-reducere.html" class="link-blue" rel="nofollow" target="_blank" onClick="ga('send', 'event', 'cupon reducere', 'citire mai multe detalii', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');">aici</a></p>
  </div>
  <?php } ?>
  <?php if(!isset($_COOKIE['acceptCookies']) and ($_COOKIE['acceptCookis']!='da')) { ?>
  <div id="info-cookies">
    <p class="clearfix black"><strong>INFO</strong><img src="/images/close.png" alt="close" class="float-right pointer" style="padding:5px 5px 5px 15px;" onClick="document.getElementById('info-cookies').style.display='none';setCookie('acceptCookies','da',3650);ga('send', 'event', 'cookie popup', 'inchidere din X', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');"></p>
    <p><?php echo $denumire_agentie; ?> folosește cookies pentru a-ți oferi o experiență cât mai plăcută. Navigând în continuare, îți exprimi acordul pentru folosirea acestora.<br>Mai multe detalii <a href="/info-cookieuri.html" class="link-blue" rel="nofollow" target="_blank" onClick="setCookie('acceptCookies','da',3650);ga('send', 'event', 'cookie popup', 'citire mai multe detalii', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');">aici</a></p>
    <span class="link-blue pointer" onClick="document.getElementById('info-cookies').style.display='none';setCookie('acceptCookies','da',3650);ga('send', 'event', 'cookie popup', 'inchidere din link', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');">Închide</span>
  </div>
  <?php } ?>
</div>
<?php } ?><?php */?>


<?php  if($id_tara==1 or $den_tara=="Romania") {
	
	if(isset($den_localitate)) {$text_adiacent=" pentru vacanta ta la ".$den_localitate;}
	if(isset($den_hotel)) {$text_adiacent=" pentru vacanta ta la ".$den_hotel." - ".$den_localitate;}
	?>

<footer_fix>
<div style=" width:100%; margin-right: auto;margin-left: auto;">
<a href="/info_tichete_vacanta.php" target="_blank" rel="nofollow" class="infos3" title="Informatii Tichete de vacanta">
<img src="/images/nou_verde.png"  alt="Nou Tichete de vacanta" width="59" height="60" align="left" hspace="20"/> 
</a>
<div style="padding-top:20px; padding-left:20px;">Primim tichete de vacanta<?php echo$text_adiacent;?>!</div>

</div>
</footer_fix>
<script type="application/javascript">
function footerAlign() {
  $('footer_fix').css('height', 'auto');
  var footerHeight = $('footer_fix').outerHeight();
  $('body').css('padding-bottom', footerHeight);
  $('footer_fix').css('height', footerHeight);
}


$(document).ready(function(){
  footerAlign();
});

$( window ).resize(function() {
  footerAlign();
});


</script>
<?php }?>