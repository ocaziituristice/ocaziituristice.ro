<?php
if ( isset( $_POST['submit-comment'] ) ) {
	if ( $_POST['rev_prenume'] == '' or $_POST['rev_nume'] == '' or $_POST['rev_email'] == '' ) {
		echo '<script>alert("Nu ați completat toate câmpurile obligatorii!");</script>';
	} else {
		if ( ! preg_match( "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $_POST['rev_comentarii_plusuri'] ) and ! preg_match( "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $_POST['rev_comentarii_minusuri'] ) ) {
			$note[1]    = $_POST['rev_personalul'];
			$note[2]    = $_POST['rev_facilitati'];
			$note[3]    = $_POST['rev_confort'];
			$note[4]    = $_POST['rev_curatenie'];
			$note[5]    = $_POST['rev_raport_pret_calitate'];
			$note[6]    = $_POST['rev_amplasare'];
			$nota_total = ( $note[1] + $note[2] + $note[3] + $note[4] + $note[5] + $note[6] ) / 6;

			$id_client = insert_user( '', $_POST['rev_nume'], $_POST['rev_prenume'], $_POST['rev_email'], '', '', 'postare review' );

			$ins_review = "INSERT INTO reviews (id_hotel, personalul, facilitati, confort, curatenie, raport_pret_calitate, amplasare, nota_total, comentarii_plusuri, comentarii_minusuri, recomand, tip_client, id_client, anonim, activ, data_adaugarii, ip) VALUES ('" . $id_hotel . "', '" . $note[1] . "', '" . $note[2] . "', '" . $note[3] . "', '" . $note[4] . "', '" . $note[5] . "', '" . $note[6] . "', '" . $nota_total . "', '" . inserare_frumos( $_POST['rev_comentarii_plusuri'] ) . "', '" . inserare_frumos( $_POST['rev_comentarii_minusuri'] ) . "', '" . $_POST['rev_recomand'] . "', '" . $_POST['rev_tip_client'] . "', '" . $id_client . "', '" . $_POST['rev_anonim'] . "', 'nu', NOW(), '" . $_SERVER['REMOTE_ADDR'] . "')";
			$que_review = mysql_query( $ins_review ) or die( mysql_error() );
			@mysql_free_result( $que_review );

			mail( $GLOBALS['email_contact'], 'Review nou', 'A fost adaugat un review nou la ' . $_SERVER['REMOTE_ADDR'] . '-' . $detalii_hotel['denumire'], 'FROM: ' . $GLOBALS['email_contact'] );

			redirect_php( $sitepath . 'thank-you-impresii.html?hotel=' . fa_link_oferta( $detalii_hotel['denumire'] ) . '&loc=' . fa_link_oferta( $detalii_hotel['localitate'] ) );
		} else {
			echo '<script>alert("Va rugam nu introduceti URL-uri in comentarii !");</script>';
		}
	}
} else {
	?>
    <div id="NEW-detaliiOferta">
        <div id="comentarii">
            <h2 class="blue underline expand icon" id="comentarii">
                Impresii<?php echo $detalii_hotel['denumire'] . " " . $den_loc; ?></h2>
            <div class="expandable ">
				<?php if ( mysql_num_rows( $que_comentarii ) ) { ?>
                    <div class="pad20">
						<?php
						foreach ( $comentarii['id_comentariu'] as $key_comentariu => $value_comentariu ) { ?>
                            <div class="comentariu">
                                <div class="nume">
                        <span style="font-size:16px;"
                              class="bigger-13em blue bold"><?php if ( $comentarii['anonim'][ $key_comentariu ] != 'da' ) {
		                        echo $comentarii['nume'][ $key_comentariu ];
	                        } else {
		                        echo 'Anonim';
	                        } ?></span><br>
                                    <span class="black"><?php echo $comentarii['tip_client'][ $key_comentariu ]; ?></span><br>
                                    <span class="smaller-08em"><?php echo denLuniRo( date( 'd F Y', strtotime( $comentarii['data_adaugarii'][ $key_comentariu ] ) ) ); ?></span>
                                </div>
                                <div class="nota text-center blue">
                                    <div class="nota_data">
										<?php echo $comentarii['nota_total'][ $key_comentariu ]; ?>
                                    </div>
                                    <div class="nota_img">
                                        <div class="inner"
                                             style="width:<?php echo $comentarii['nota_total'][ $key_comentariu ] * 10; ?>px;"></div>
                                    </div>
                                </div>
                                <div class="pareri">
									<?php if ( strlen( $comentarii['comentarii_plusuri'][ $key_comentariu ] ) > 0 ) { ?>
                                        <div class="plusuri"><?php echo afisare_frumos( $comentarii['comentarii_plusuri'][ $key_comentariu ] ); ?></div><?php } ?>
									<?php if ( strlen( $comentarii['comentarii_minusuri'][ $key_comentariu ] ) > 0 ) { ?>
                                        <div class="minusuri"><?php echo afisare_frumos( $comentarii['comentarii_minusuri'][ $key_comentariu ] ); ?></div><?php } ?>
                                </div>
                                <div class="clear"></div>
                            </div>
						<?php } ?>

                    </div>
                    <div class="reviews-header clearfix">
                        <div class="float-left black">
                    <span class="bigger-15em bold">Total: <span class="blue"
                                                                style="color:#31b0d5;font-size:36px;"><?php echo $nota_total ? $nota_total : 0; ?></span></span><br>
                            <p style="padding-bottom:10px;">Notă bazată pe
                                <strong><?php echo $nr_comentarii ? $nr_comentarii : 0; ?>
                                    comentarii</strong>.</p>
                            Comentariile turiștilor sunt scrise de clienții noștri după șederea lor la
                            <strong><?php echo $detalii_hotel['denumire'] . ', ' . $detalii_hotel['localitate']; ?></strong>.
                        </div>
                        <div class="float-left" style="background:#FFF; padding:10px 20px 0 30px;">
                            <div class="rating">
                                <div class="camp">Personalul</div>
                                <div class="nota_img">
                                    <div class="inner" style="width:<?php echo $personalul * 10; ?>px;"></div>
                                </div>
                                <div class="nota_txt"><?php echo number_format( $personalul, 1 ); ?></div>
                            </div>
                            <div class="rating">
                                <div class="camp">Facilități</div>
                                <div class="nota_img">
                                    <div class="inner" style="width:<?php echo $facilitati * 10; ?>px;"></div>
                                </div>
                                <div class="nota_txt"><?php echo number_format( $facilitati, 1 ); ?></div>
                            </div>
                            <div class="rating">
                                <div class="camp">Confort</div>
                                <div class="nota_img">
                                    <div class="inner" style="width:<?php echo $confort * 10; ?>px;"></div>
                                </div>
                                <div class="nota_txt"><?php echo number_format( $confort, 1 ); ?></div>
                            </div>
                            <div class="rating">
                                <div class="camp">Curățenie</div>
                                <div class="nota_img">
                                    <div class="inner" style="width:<?php echo $curatenie * 10; ?>px;"></div>
                                </div>
                                <div class="nota_txt"><?php echo number_format( $curatenie, 1 ); ?></div>
                            </div>
                            <div class="rating">
                                <div class="camp">Raport preț/calitate</div>
                                <div class="nota_img">
                                    <div class="inner" style="width:<?php echo $raport_pret_calitate * 10; ?>px;"></div>
                                </div>
                                <div class="nota_txt"><?php echo number_format( $raport_pret_calitate, 1 ); ?></div>
                            </div>
                            <div class="rating">
                                <div class="camp">Amplasare</div>
                                <div class="nota_img">
                                    <div class="inner" style="width:<?php echo $amplasare * 10; ?>px;"></div>
                                </div>
                                <div class="nota_txt"><?php echo number_format( $amplasare, 1 ); ?></div>
                            </div>
                        </div>
                        <div class="float-left" style="background:#FFF; padding-top:65px; margin-left:2px;">
                            <div id="add-review" class="button-blue" style="display:inline-block;">Adaugă comentariu
                            </div>
                        </div>
                    </div>
				<?php } else { ?>
                    <div class="float-left pad20" style="width:670px; height:130px;">
                        <div class="black bigger-14em">Adaugă comentariul tău despre
                            <strong><?php echo $detalii_hotel['denumire'] . ', ' . $detalii_hotel['localitate']; ?></strong>.
                        </div>
                        Dacă ai stat la <?php echo $detalii_hotel['denumire'] . ', ' . $detalii_hotel['localitate']; ?>
                        te rugăm
                        să ne impărtășești opiniile despre șederea ta. <span
                                class="black bold underline">Părerea ta contează!</span><br>
                        Încearcă să fii cât mai obiectiv, ajutând astfel ceilalți turiști să își facă o părere
                        despre <?php echo $detalii_hotel['denumire'] . ', ' . $detalii_hotel['localitate']; ?>.
                    </div>
                    <div class="float-left" style="background:#FFF; padding-top:65px; margin-left:2px;">
                        <div id="add-review" class="button-blue" style="display:inline-block;">Adaugă comentariu
                        </div>
                    </div>
                    <div class="clear"></div>
				<?php } ?>
                <div id="adauga-review" style="display:none;">
                    <div class="black pad20" style="background:#E3EFFF; margin-top:1px;">
                        <form style="padding:10px;"
                              action="<?php echo substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI']; ?>#comentarii"
                              method="post">
                            <h3 class="blue bigger-15em" style="margin:0; padding:0;">Evaluează șederea ta la <span
                                        class="black"><?php echo $detalii_hotel['denumire'] . ', ' . $detalii_hotel['localitate']; ?></span>
                            </h3>
                            <p class="red italic">Toate câmpurile sunt obligatorii.</p>

                            <p><span class="bigger-14em">Cum ai evalua hotelul la care ai stat din punct de vedere al următoarelor criterii:</span><br>
                                <span class="italic">Acordă o notă de la 1 la 10 pentru fiecare serviciu.</span></p>
                            <br>

                            <div class="float-left-comments" style="width:430px;">
                                <div class="review-line clearfix">
                                    <label class="float-left-comments">Personalul:</label>
                                    <div class="float-right clearfix">
										<?php for ( $i1 = 1; $i1 <= 10; $i1 ++ ) {
											echo '<input name="rev_personalul" type="radio" value="' . $i1 . '" class="star';
											if ( $i1 == 1 ) {
												echo ' required';
											}
											echo '"';
											if ( $_POST['rev_personalul'] == $i1 ) {
												echo ' checked="checked"';
											}
											echo '/>';
										} ?>
                                    </div>
                                </div>

                                <div class="review-line clearfix"><label class="float-left-comments">Facilități:</label>
                                    <div class="float-right clearfix">
										<?php for ( $i1 = 1; $i1 <= 10; $i1 ++ ) {
											echo '<input name="rev_facilitati" type="radio" value="' . $i1 . '" class="star';
											if ( $i1 == 1 ) {
												echo ' required';
											}
											echo '"';
											if ( $_POST['rev_facilitati'] == $i1 ) {
												echo ' checked="checked"';
											}
											echo '/>';
										} ?>
                                    </div>
                                </div>

                                <div class="review-line clearfix"><label class="float-left-comments">Confort:</label>
                                    <div class="float-right clearfix">
										<?php for ( $i1 = 1; $i1 <= 10; $i1 ++ ) {
											echo '<input name="rev_confort" type="radio" value="' . $i1 . '" class="star';
											if ( $i1 == 1 ) {
												echo ' required';
											}
											echo '"';
											if ( $_POST['rev_confort'] == $i1 ) {
												echo ' checked="checked"';
											}
											echo '>';
										} ?>
                                    </div>
                                </div>

                                <div class="review-line clearfix"><label class="float-left-comments">Curățenie:</label>
                                    <div class="float-right clearfix">
										<?php for ( $i1 = 1; $i1 <= 10; $i1 ++ ) {
											echo '<input name="rev_curatenie" type="radio" value="' . $i1 . '" class="star';
											if ( $i1 == 1 ) {
												echo ' required';
											}
											echo '"';
											if ( $_POST['rev_curatenie'] == $i1 ) {
												echo ' checked="checked"';
											}
											echo '>';
										} ?>
                                    </div>
                                </div>

                                <div class="review-line clearfix"><label class="float-left-comments">Raport
                                        preț/calitate:</label>
                                    <div class="float-right clearfix">
										<?php for ( $i1 = 1; $i1 <= 10; $i1 ++ ) {
											echo '<input name="rev_raport_pret_calitate" type="radio" value="' . $i1 . '" class="star';
											if ( $i1 == 1 ) {
												echo ' required';
											}
											echo '"';
											if ( $_POST['rev_raport_pret_calitate'] == $i1 ) {
												echo ' checked="checked"';
											}
											echo '>';
										} ?>
                                    </div>
                                </div>

                                <div class="review-line clearfix"><label class="float-left-comments">Amplasare:</label>
                                    <div class="float-right clearfix">
										<?php for ( $i1 = 1; $i1 <= 10; $i1 ++ ) {
											echo '<input name="rev_amplasare" type="radio" value="' . $i1 . '" class="star';
											if ( $i1 == 1 ) {
												echo ' required';
											}
											echo '"';
											if ( $_POST['rev_amplasare'] == $i1 ) {
												echo ' checked="checked"';
											}
											echo '>';
										} ?>
                                    </div>
                                </div>

                            </div>
                            <div class="prosicontra float-right" style="width:100%;max-width:500px;">

                                <div class="clearfix" style="margin-bottom:10px;">
                                    <label for="comentarii_plusuri" class="float-left-comments bigger-11em bold"
                                           style="width:200px;"><img src="/images/review_pos.gif" alt="pozitiv"> Ce ți-a
                                        plăcut la hotel?</label>
                                    <textarea id="comentarii_plusuri" name="rev_comentarii_plusuri"
                                              class="float-left-comments"
                                              style="width:100%;max-width:290px; height:50px;">
                                    <?php echo $_POST['rev_comentarii_plusuri']; ?>
                                </textarea>
                                </div>

                                <div class="clearfix" style="margin-bottom:10px;">
                                    <label for="comentarii_minusuri" class="float-left-comments bigger-11em bold"
                                           style="width:200px;"><img src="/images/review_neg.gif" alt="negativ"> Ce nu
                                        ți-a
                                        plăcut la hotel?</label>
                                    <textarea id="comentarii_minusuri" name="rev_comentarii_minusuri"
                                              class="float-left-comments"
                                              style="width:100%;max-width:290px; height:50px;">
                                    <?php echo $_POST['rev_comentarii_minusuri']; ?>
                                </textarea>
                                </div>

                                <div class="clearfix" style="margin-bottom:10px;">
                                    <label class="float-left-comments bigger-11em bold" style="width:320px;">Ai
                                        recomanda
                                        hotelul și altor persoane?</label>
                                    <label class="bold"><input type="radio" name="rev_recomand"
                                                               value="da"<?php $_POST['rev_recomand'] == 'da' ? ' checked="checked"' : ''; ?>>DA</label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="bold"><input type="radio" name="rev_recomand"
                                                               value="nu"<?php $_POST['rev_recomand'] == 'nu' ? ' checked="checked"' : ''; ?>>NU</label>
                                </div>

                            </div>

                            <br class="clear"><br>
                            <div class="user_data">
                                <p class="blue bold bigger-14em">Datele tale:</p>

                                <div class="clearfix" style="margin-bottom:10px;">
                                    <label for="prenume" class="float-left-comments bigger-11em bold"
                                           style="width:165px; padding-top:3px;">Prenumele tău:</label>
                                    <input type="text" id="prenume" name="rev_prenume"
                                           value="<?php echo $_POST['rev_prenume']; ?>" style="width:220px;"
                                           data-validation-engine="validate[required]">
                                </div>

                                <div class="clearfix" style="margin-bottom:10px;">
                                    <label for="nume" class="float-left-comments bigger-11em bold"
                                           style="width:165px; padding-top:3px;">Numele tău:</label>
                                    <input type="text" id="nume" name="rev_nume"
                                           value="<?php echo $_POST['rev_nume']; ?>"
                                           style="width:220px;"
                                           data-validation-engine="validate[required]"> <span class="italic">* acest câmp nu va fi afișat pe site</span>
                                </div>

                                <div class="clearfix" style="margin-bottom:10px;">
                                    <label for="email" class="float-left-comments bigger-11em bold"
                                           style="width:165px; padding-top:3px;">Email-ul tău:</label>
                                    <input type="email" id="email" name="rev_email"
                                           value="<?php echo $_POST['rev_email']; ?>" style="width:220px;"
                                           data-validation-engine="validate[required]"> <span class="italic">* acest câmp nu va fi afișat pe site</span>
                                </div>

                                <div class="clearfix" style="margin-bottom:10px;">
                                    <label for="tip_client" class="float-left-comments bigger-11em bold"
                                           style="width:165px; padding-top:3px;">Tip călători:</label>
                                    <select id="tip_client" name="rev_tip_client" style="width:223px;"
                                            data-validation-engine="validate[required]">
                                        <option disabled selected>- Selectează -</option>
                                        <option value="Calator singur">Călător singur</option>
                                        <option value="Cuplu tanar">Cuplu tânăr</option>
                                        <option value="Cuplu matur">Cuplu matur</option>
                                        <option value="Familie cu copii mici">Familie cu copii mici</option>
                                        <option value="Familie cu copii mai mari">Familie cu copii mai mari</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="margin-bottom:10px;">
                                <label class="float-left-comments bigger-11em bold" style="width:250px;">Dorești să
                                    apari ca
                                    "ANONIM"?</label>
                                <label class="bold"><input type="radio" name="rev_anonim"
                                                           value="da"<?php $_POST['rev_anonim'] == 'da' ? ' checked="checked"' : ''; ?>
                                                           data-validation-engine="validate[required]">DA</label>
                                <label class="bold"><input type="radio" name="rev_anonim"
                                                           value="nu"<?php $_POST['rev_anonim'] == 'nu' ? ' checked="checked"' : ''; ?>
                                                           data-validation-engine="validate[required]">NU</label>
                            </div>

                            <div><input style="background:#5cb85c;" type="submit" name="submit-comment"
                                        value="INREGISTREAZĂ PĂREREA TA" class="button-red"></div>
                        </form>

                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
<?php } ?>