<?php $luna=array(1=>'Ianuarie', 2=>'Februarie', 3=>'Martie', 4=>'Aprilie', 5=>'Mai', 6=>'Iunie', 7=>'Iulie', 8=>'August', 9=>'Septembrie', 10=>'Octombrie', 11=>'Noiembrie', 12=>'Decembrie');
if($_POST['trimite']) {
$err=0;
$err_nume='';
if(strlen(trim($_POST['nume']))<3) { ++$err; $err_nume='Campul nume este obligatoriu!'; }
if(strlen(trim($_POST['telefon']))<4) { ++$err; $err_tel='Campul telefon este obligatoriu!'; }
if(strlen(trim($_POST['email']))<5) { ++$err; $err_email='Campul E-mail este obligatoriu!'; }
if(strlen(trim($_POST['prenume']))<3) { ++$err; $err_prenume='Campul prenume este obligatoriu!'; }
elseif(validate_email($_POST['email'])==false) { ++$err; $err_email='Adresa de email completata nu este corecta!'; }
if(strlen(trim($_POST['perioada']))<4) { ++$err; $err_perioada='Nu a-ti completat data de incepere a sejurului!'; }
if(strlen(trim($_POST['nr_nopti']))<1) { ++$err; $err_nopti='Campul Nr. nopti este obligatoriu!'; }
/*$resp=recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
if(!$resp->is_valid) { ++$err; $err_cod='Codul este invalid'; }*/

if(!$err) {
  $date_time=date("Y-m-d G:i:s");
  $nume=$_POST['nume'];
  $prenume=$_POST['prenume'];
  $email=$_POST['email'];
  $nr_adulti=$_POST['nr_adulti'];
  $nr_copii=$_POST['nr_copii'];
  $mr_tel=$_POST['telefon'];
  $camera=$_POST['camera'];
  $nr_nopti=$_POST['nr_nopti'];
  $perioada=$_POST['perioada'];
  $observatii=$_POST['observatii'];
  $data_nasterii=$_POST['an'].'-'.$_POST['luna'].'-'.$_POST['ziua'];
  $data_nasterii_afisare=$_POST['ziua'].' '.$luna[$_POST['luna']].' '.$_POST['an'];
  if(sizeof($_POST['varsta'])>0) {
	foreach($_POST['varsta'] as $key=>$value) $varsta=$varsta.'Copil '.$key.' ani impliniti '.$value.'<br/>';  
  }
  foreach($_POST['asigurare'] as $key1=>$vale1) $asigurare=$asigurare.$vale1.',';
  $sol="insert into rezervari set id_oferta = '".$id_oferta."', camera = '".$camera."', sex = '".$_POST['sex']."', nume = '".$nume."', prenume = '".$prenume."', email = '".$email."', telefon = '".$mr_tel."', nr_adulti = '".$nr_adulti."', nr_copii = '".$nr_copii."', nr_nopti = '".$nr_nopti."', asigurare = '".$asigurare."', perioada = '".$perioada."', observatii = '".$observatii."', data_adaugarii = '".$date_time."', ip = '".$_SERVER['REMOTE_ADDR']."', copii_varsta = '".$varsta."', data_nasterii = '".$data_nasterii."', data_nasterii_afisare = '".$data_nasterii_afisare."' ";
  if($que=mysql_query($sol) or die(mysql_error())) {
	if($_POST['sex']=='m') $tit='Domnul '; else $tit='Doamna ';
	$GLOBALS['nume_expeditor2']=$prenume.' '.$nume;
	$GLOBALS['nume']=$tit.$nume_expeditor2;
	$GLOBALS['email']=$email;
	$GLOBALS['data_ora']=$date_time;  
	$GLOBALS['denumire_oferta']=$detalii['denumire'];
	$GLOBALS['data_nasterii_afisare']=$data_nasterii_afisare;
	$GLOBALS['link']=$sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link($detalii['denumire']).'-'.$id_oferta.'.html';
	$GLOBALS['perioada']=$perioada;
	$GLOBALS['nr_adulti']=$nr_adulti;
	if($nr_copii) { if($nr_copii>1) $t='copii'; else $t='copil'; $GLOBALS['nr_copii']=' si <b>'.$nr_copii.'</b> '.$t.'<br/>'.$varsta; } else $GLOBALS['nr_copii']='';
	$GLOBALS['telefon']=$mr_tel;
	$GLOBALS['observatii']=$observatii;
	$GLOBALS['sigla']=$GLOBALS['path_sigla'];
	//$GLOBALS['email_contact']='daniel@ocaziituristice.ro';
	
	$send_m= new SEND_EMAIL;
	
	//raspuns_client
	$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/rezervare.htm';
    $param['subject']='Rezervare '.$detalii['denumire'].' - '.$email_sitepath;
    $param['to_email']=$email;
    $param['to_nume']=$nume_expeditor2;
    $param['fr_email']=$GLOBALS['email_rezervare'];
    $param['fr_nume']=$GLOBALS['denumire_agentie'];
	$send_m->send($param);
	
	//email_agentie
	$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/mail_agentie_rezervare.htm';
    $param['subject']='Rezervare '.$detalii['denumire'];
    $param['to_email']=$GLOBALS['email_rezervare'];
    $param['to_nume']=$GLOBALS['denumire_agentie'];
    $param['fr_email']=$email;
    $param['fr_nume']=$nume_expeditor2;
	$send_m->send($param);
  }
 }
} ?>
<div id="NEW-detaliiOferta" <?php if($detalii['valabila']=='nu') { ?>style="background:#FFF url(/images/oferta_expirata.gif) top center repeat-y;"<?php } ?>>
<?php $link_cerere_detalii=$sitepath.'includes/sejururi/cere_mai_multe_detalii.php?hotel='.$id_hotel.'&oferta='.$id_oferta;
$valuta='nu'; ?>
  <h1 class="red">Rezervare <?php echo $detalii["denumire"]; ?> <?php //echo $detalii_hotel['denumire']; ?> <img src="/images/spacer.gif" class="stele-mari-<?php echo $detalii_hotel['stele']; ?>" alt="" /></h1>
  <div class="clearfix">
	<div class="NEW-adresa"><?php echo $detalii_hotel['adresa']; if($detalii_hotel['nr']) echo ' Nr. '.$detalii_hotel['nr']; ?>, <?php echo $detalii_hotel['localitate']; ?>, <?php echo $detalii_hotel['tara']; if($detalii_hotel['latitudine'] && $detalii_hotel['longitudine'] && $detalii_hotel['latitudine']<> 0 && $detalii_hotel['longitudine'] <> 0) { ?> (<a href="<?php echo $sitepath; ?>includes/harti/hotel.php?id_hotel=<?php echo $id_hotel; ?>" rel="harta" class="link"><strong>Arata harta</strong></a>)<?php } ?></div>
	<?php if($id_oferta) { ?><div class="cod-oferta"><strong class="grey">Cod Oferta:</strong> <span class="blue"><?php echo 'OSE'.$id_oferta; ?></span></div><?php } ?>
  </div>
    
  <?php /*?><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?><?php */?>
  
  <br class="clear" />
  
<div class="NEW-column-left1">
  <div class="NEW-gallery">
  <?php if($detalii_hotel['poza1']) { ?>
    <div class="single-img">
      <img src="<?php echo $sitepath_parinte; ?>img_mediu_hotel/<?php echo $detalii_hotel['poza1']; ?>" width="300" height="300" alt="" class="images" id="poza_principala" />
	  <?php if($detalii['last_minute']=='da') { echo '<div class="last-minute"></div>'; } ?>
	  <?php if(sizeof($detalii['early_time'])>0) { echo '<div class="early-booking"></div>'; } ?>
    </div>
  <?php } else { ?>
	<div class="single-img"><img src="<?php echo $sitepath_parinte; ?>images/no_photo.jpg" width="300" height="300" alt="" class="images" id="poza_principala" /></div>
  <?php } ?>
<?php
for($i=1;$i<=8;$i++) {
if($detalii_hotel['poza'.$i]) { ?>    
    <a href="<?php echo $sitepath_parinte; ?>img_prima_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" onmouseover="document.getElementById('poza_principala').src='<?php echo $sitepath_parinte; ?>img_mediu_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>'; document.getElementById('link_poza_principala').href='<?php echo $sitepath_parinte; ?>img_prima_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>';return false;" rel="gallery"><img src="<?php echo $sitepath_parinte; ?>thumb_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" width="75" alt="" class="images" /></a>
<?php }
} ?>
  </div>
<?php if($id_oferta) { ?>
  <div class="info-scurte">
	<?php if($preturi['minim']['pret']) { ?><div class="pret red">de la <span <?php if($preturi['minim']['moneda']=='RON') echo 'class="lei"'; ?>><?php echo $preturi['minim']['pret']." "; if($preturi['minim']['moneda']=='EURO') { $valuta='da'; echo '&euro;'; } elseif($preturi['minim']['moneda']=='USD') { $valuta='da'; echo '$'; } else echo $preturi['minim']['moneda']; ?></span> <?php echo $detalii['exprimare_pret']; ?></div><?php } ?>
    <div class="left">
      <p class="titlu">Transport:</p><p class="valoare"><?php echo $detalii['transport']; ?></p>
      <?php if($detalii['nr_zile']>2 || $detalii['nr_nopti']>1) { ?><p class="titlu">Durata:</p><p class="valoare"><?php if(isset($detalii['nr_zile'])) { if($detalii['nr_zile']>1) echo $detalii['nr_zile'].' zile'; else echo $detalii['nr_zile'].' zi'; } ?> / <?php if(isset($detalii['nr_nopti'])) { if($detalii['nr_nopti']>1) echo $detalii['nr_nopti'].' nopti'; else echo $detalii['nr_nopti'].' noapte'; } ?></p><?php } ?>
      <p class="titlu">Masa:</p><p class="valoare"><?php echo $detalii['masa']; ?></p>
    </div>
  </div>
<?php
} ?>
  <br class="clear" />
  
  <div class="NEW-detalii-oferta">

    <div class="expandable-hotel">
	  <?php afisare_frumos(schimba_caractere($detalii_hotel["descriere_scurta"].'<br/>'.$detalii_hotel["descriere"]), 'nl2p'); ?> 
      <br class="clear" />   
    </div>
  </div>
<?php //formular rezervare_________________________________ 
  if($detalii['valabila']=='da') { ?>
  <div class="cerere-detalii">
<?php if(!$_POST['trimite'] || $err>0) { ?>
<form action="" method="post" name="contactForm">
<h2 class="green">Date contact</h2>
<div>
      <label><input type="radio" name="sex" value="m" style="width:10px" <?php if(!$_POST['sex'] || $_POST['sex']=='m') { ?> checked="checked" <?php } ?>/>Domnul</label>&nbsp;&nbsp;&nbsp;<label><input type="radio" name="sex" value="f" style="width:10px" <?php if($_POST['sex']=='f') { ?> checked="checked" <?php } ?> />Doamna</label>
 </div>
<div>
  <label for="nume" class="titlu">Nume (obligatoriu)</label>
  <input name="nume" id="nume" type="text" value="<?php if($_POST['nume']) echo $_POST['nume']; ?>" class="input" style="background-image:url(<?php echo $imgpath; ?>/icon_contact_input_name.png);" />
  <?php if($err_nume) { ?><label class="error"><?php echo $err_nume; ?></label><?php } ?>
</div>
<div>
  <label for="prenume" class="titlu">Prenume (obligatoriu)</label>
  <input name="prenume" id="prenume" type="text" value="<?php if($_POST['prenume']) echo $_POST['prenume']; ?>" class="input" style="background-image:url(<?php echo $imgpath; ?>/icon_contact_input_name.png);" />
  <?php if($err_prenume) { ?><label class="error"><?php echo $err_prenume; ?></label><?php } ?>
</div>
<div>
  <label for="email" class="titlu">E-mail (obligatoriu)</label>
  <input name="email" id="email" type="text" value="<?php if($_POST['email']) echo $_POST['email']; ?>" class="input" style="background-image:url(<?php echo $imgpath; ?>/icon_contact_input_email.png);" />
  <?php if($err_email) { ?><label class="error"><?php echo $err_email; ?></label><?php } ?>
</div>
<div>
  <label for="telefon" class="titlu">Telefon (obligatoriu)</label>
  <input name="telefon" id="telefon" type="text" value="<?php if($_POST['telefon']) echo $_POST['telefon']; ?>" class="input" style="background-image:url(<?php echo $imgpath; ?>/icon_contact_input_phone.png);" />
  <?php if($err_tel) { ?><label class="error"><?php echo $err_tel; ?></label><?php } ?>
</div>
<div>
      <label class="titlu">Data nasterii</label>
      zi <select name="ziua">
<?php for($i=1;$i<=31;$i++) { echo "<option value='$i'"; if($_POST['ziua']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?>
</select> luna <?php ?> <select name="luna">
<?php for($i=1;$i<=12;$i++) { echo "<option value='$i'"; if($_POST['luna']==$i) echo "selected='selected'"; echo ">".$luna[$i]."</option>"; } ?>
</select> an <?php ?> <select name="an">
<?php for($i=date("Y");$i>=1900;$i--) { echo "<option value='$i'"; if($_POST['an']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?>
</select>
    </div>
<h2 class="green">Detalii rezervare</h2>
<div>
  <label class="titlu">Nr. persoane</label>
  Adulti: <select name="nr_adulti">
  <?php for($i=1;$i<=10;$i++) { ?>
  <option value="<?php echo $i; ?>" <?php if($_POST['nr_adulti']==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
  <?php } ?>
  </select>&nbsp;&nbsp;Copii: <select name="nr_copii" onchange="if(this.value>0) { var cont=''; for(var i=1; i<=this.value; i++) { cont=cont+'Copil '+i+' ani impliniti: <select name=\'varsta['+i+']\'>'; for(var j=1; j<=18; j++) cont=cont+'<option value=\''+j+'\'>'+j+'</option>'; cont=cont+'</select> <br/><br/>'; } document.getElementById('copii').innerHTML=cont; }">
  <?php for($i=0;$i<=10;$i++) { ?>
  <option value="<?php echo $i; ?>" <?php if($_POST['nr_copii']==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
  <?php } ?>
  </select>
 <div id="copii"><?php if(sizeof($_POST['varsta'])>0) {
foreach($_POST['varsta'] as $key=>$value) {  echo 'Copil '.$key.' ani impliniti: <select name="varsta['.$key.']">';
for($j=1;$j<=18;$j++) { echo '<option value="'.$j.'"'; if($j==$value) echo 'selected="selected"'; echo '>'.$j.'</option>'; } echo '</select><br/><br/>'; }
 } ?></div>
</div>

<?php if($preturi['camera']>0) { ?>
<div>
  <label class="titlu">Camera</label>
  <?php foreach($preturi['camera'] as $key_cam=>$value_cam) { ?>
  <label class="check"><input type="radio" name="camera" value="<?php echo $key_cam; ?>" <?php if($_POST['camera']==$key_cam) { ?> checked="checked" <?php } ?>/><?php echo $value_cam; ?></label>
  <?php } ?>
  <label class="titlu" for="observatii_camera">Observatii Camera</label>
  <input type="text" id="observatii_camera" name="observatii_camera" <?php if($_POST['observatii_camera']) echo $_POST['observatii_camera']; ?> class="input" />
</div>
<div>
  <label class="titlu" for="perioada">Data incepere a sejurului (obligatoriu)</label>
  <input type="text" name="perioada" id="perioada" value="<?php if($_POST['perioada']) echo $_POST['perioada']; ?>" class="input" />
  <?php if($err_perioada) { ?><label class="error"><?php echo $err_perioada; ?></label><?php } ?>
</div>
<?php } ?>
<div>
  <label class="titlu" for="nr_nopti">Nr. nopti (obligatoriu)</label>
  <input type="text" name="nr_nopti" id="nr_nopti" <?php if($detalii['nr_nopti']>=2) { ?> readonly="readonly" <?php } ?> value="<?php if($_POST['nr_nopti']) echo $_POST['nr_nopti']; elseif($detalii['nr_nopti']>=2) echo $detalii['nr_nopti']; ?>" class="input_mic" />
  <?php if($err_nopti) { ?><label class="error"><?php echo $err_nopti; ?></label><?php } ?>
</div>
<div>
  <label class="titlu">Asigurare de sanatate</label>
  <?php $loc="SELECT id_asigurare, denumire from asigurari Group by id_asigurare Order by denumire ";
$qury_loc=mysql_query($loc) or die(mysql_error()); ?>
  <label class="check"><input name="asigurare[]" type="checkbox" value="nu"<?php if(($_POST['asigurare'] && in_array('nu', $_POST['asigurare'])) || !$_POST['asigurare']) { ?> checked="checked" <?php } ?> /> Nu doresc asigurare</label>
    <?php while($row=mysql_fetch_array($qury_loc)) { ?>
    <label class="check"><input type="checkbox" name="asigurare[]" value="<?php echo $row['denumire']; ?>" <?php if($_POST['asigurare']) if(in_array($row['denumire'], $_POST['asigurare'])) { ?> checked="checked" <?php } ?>> <?php echo $row['denumire']; ?></label>
    <?php } @mysql_free_result($que); ?>
</div>
<div>
      <label for="observatii" class="titlu">Observatii</label>
      <textarea name="observatii" id="observatii"><?php if($_POST['observatii']) echo $_POST['observatii']; ?></textarea>
</div>
<?php /*?><div>
  <label class="titlu">Cod de siguranta</label>
  <?php echo recaptcha_get_html($publickey);
  if($err_cod) { ?><label class="error"><?php echo $err_cod; ?></label><?php } ?>
</div><?php */?>
<input name="trimite" type="hidden" value="trimite" />
<input type="image" src="<?php echo $imgpath; ?>/buton_trimite.gif" value="Trimite" class="send" />
</form>
<?php } else { ?>
Rezervarea dumneavoastra a fost inregistrata<br />
In cel mai scurt timp veti fi contactat in legatura cu rezervarea efectuata.
<br /><br />
Va multumim,<br />
Echipa ocaziituristice.ro
<?php } ?>
</div>
  <?php } else echo '<h2 align="center">Oferta este expirata</h2>'; ?>
  
</div>
<div class="NEW-column-right1">

  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/new_hotel.php"); ?>
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta_new.php"); ?>

</div>
<br class="clear" />
</div>
<br class="clear" />
