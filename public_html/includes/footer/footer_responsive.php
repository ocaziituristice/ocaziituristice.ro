<footer class="footer">
    <a href="#" class="top">Mergi Sus</a>

    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4">
                <h2>Ocaziituristice.ro</h2>
                <ul class="triangle">
                    <li><a href="#">Prima pagina</a></li>
                    <li><a href="/despre-noi.html" title="Despre noi" rel="nofollow">Despre noi</a></li>
                    <li><a href="/termeni_si_conditii.html" title="Termeni si Conditii de utilizare" rel="nofollow">Termeni si Conditii</a></li>
                    <li><a href="/contact.html" title="Contact" rel="nofollow">Contact</a></li>
                    
     
                    
                    <li><a href="http://www.anpc.gov.ro/" title="Autoritatea Nationala pentru Protectia Consumatorilor" target="_blank" rel="nofollow">ANPC - 0219551</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">
                <h2>Utile</h2>
                <ul class="triangle">
                    <li><a href="/info-cum-rezerv.html" title="Pasii ce trebuie parcursi pentru a efectua o rezervare" rel="nofollow">Cum rezerv</a></li>
                    <li><a href="/info-cum-platesc.html" title="Modalitatile de plata disponibile" rel="nofollow">Cum platesc</a></li>
                    <li><a href="/info-cookieuri.html" title="Informatii despre cookie-uri" target="_blank" rel="nofollow">Despre cookie-uri</a></li>
                    <li><a href="/info-asigurari-de-calatorie.html" title="Asigurari de calatorie" rel="nofollow">Asigurari de calatorie</a></li>
                    <li><a href="/files/Contractul%20cu%20Turistul.pdf"title="Contractul cu turistul" target="_blank"rel="nofollow">Contractul cu turistul</a></li>
                    <li><a href="/files/Regimul%20de%20vize%20pentru%20cetatenii%20romani.pdf" title="Regimul de vize pentru cetatenii romani" class="item" target="_blank" rel="nofollow">Regimul de vize</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">

				<?php $sel_curs_valutar = "SELECT * FROM curs_valutar ";
				$que_curs_valutar = mysql_query( $sel_curs_valutar ) or die( mysql_error() );
				$row_curs_valutar = mysql_fetch_array( $que_curs_valutar ); ?>


                <h2>Curs valutar BNR</h2>
                <div>
                    <h6><?php echo date( "d.m.Y", strtotime( $row_curs_valutar['data'] ) ); ?></h6>
                    <h6>1 EURO = <?php echo str_replace( ".", ",", $row_curs_valutar['EURO'] ); ?> Lei</h6>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <h2>Ne gasiti pe</h2>
                <a href=""><i id="facebook-icon" class="fa fa-facebook-square fa-3x social-icon"></i></a>
                <a href=""><i id="google-icon" class="fa fa-google-plus-square fa-3x social-icon"></i></a>
                <a href=""><i id="twitter-icon" class="fa fa-twitter-square fa-3x social-icon"></i></a>
                <a href=""><i id="pinterest-icon" class="fa fa-pinterest-square fa-3x social-icon"></i></a>
                <a href=""><i id="rss-icon" class="fa fa-rss-square fa-3x social-icon"></i></a>
                <address class="contact-details">
                    <span class="contact-phone"><i class="fa fa-phone"></i> 021.311.7464</span>
                    <span class="contact-address"><i class="fa fa-map-marker"></i> Bd-ul Gh. Magheru, Nr. 28-30, Sector 1, 010336, Bucuresti</span>
                </address>
            </div>
        </div>
        <div class="copyright">
            <span>Copyright © 2008 - 2018 Agentia de turism <a href="">www.ocaziituristice.ro.</a></span>
            <div class="pull-right">
                <img src="/images/teztour-logo.gif" alt="Tez Tour">
                <img src="/images/mobilepay-footer.png" alt="Plati prin Mobile Pay">
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#topic_title").autocomplete({
                source: "/includes/sejururi/jq_autocomplete_cauta.php?t=<?php echo $id_tara; ?>",
                minLength: 3,
                select: function (event, ui) {
                    var url = ui.item.id;
                    if (url != '#') {
                        location.href = url;
                    }
                },
                html: true,
                open: function (event, ui) {
                    $(".ui-autocomplete").css("z-index", 1000);
                }
            });
        });
        $(document).ready(function () {
            $("#topic_title").val("");
        });
    </script>


    <script type="text/javascript">

        $(document).ready(function () {
            var offset = 500, // At what pixels show Back to Top Button
                scrollDuration = 300; // Duration of scrolling to top
            $(window).scroll(function () {
                if ($(this).scrollTop() > offset) {
                    $('.top').fadeIn(500); // Time(in Milliseconds) of appearing of the Button when scrolling down.
                } else {
                    $('.top').fadeOut(500); // Time(in Milliseconds) of disappearing of Button when scrolling up.
                }
            });

            // Smooth animation when scrolling
            $('.top').click(function (event) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: 0
                }, scrollDuration);
            });


   

     


        });
    </script>
    <script>
        $(document).ready(function () {
            var windowWidth = $(window).width();
            if (windowWidth <= 1024) {
                $('.panel-collapse').removeClass('show')

            }


            var mq = window.matchMedia("(min-width: 300px)");
            if (mq.matches) {
//                $(".bottom").load("includes/trustme.php");
//
//                $(".trust").load("includes/trustme.php");
            }
        });
    </script>

</footer>
<?php  if($id_tara==1 or $den_tara=="Romania") {
	
	if(isset($den_localitate)) {$text_adiacent=" pentru vacanta ta la ".$den_localitate;}
	if(isset($den_hotel)) {$text_adiacent=" pentru vacanta ta la ".$den_hotel." - ".$den_localitate;}
	?>

<footer_fix>
<div style=" width:100%; margin-right: auto;margin-left: auto;">
<a href="/info_tichete_vacanta.php" target="_blank" rel="nofollow" title="Informatii Tichete de vacanta">
<img src="/images/nou_verde.png"  alt="Nou Tichete de vacanta" width="59" height="60" align="left" hspace="20"/> 
</a>
<div style="padding-top:20px; padding-left:20px;">Primim tichete de vacanta<?php echo$text_adiacent;?>!</div>

</div>
</footer_fix>
<script type="application/javascript">
function footerAlign() {
  $('footer_fix').css('height', 'auto');
  var footerHeight = $('footer_fix').outerHeight();
  $('body').css('padding-bottom', footerHeight);
  $('footer_fix').css('height', footerHeight);
}


$(document).ready(function(){
  footerAlign();
});

$( window ).resize(function() {
  footerAlign();
});


</script>
<?php }?>
<!-- END FOOTER SECTION -->
</body>

</html>