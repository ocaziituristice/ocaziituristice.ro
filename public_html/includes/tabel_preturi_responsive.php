<table class="table table-responsive offers_tables" style="margin-top: 20px; margin-bottom: 0;">
				<tbody>
				<tr>
					<th class="hide_mobile">Data plecării</th>
					<th class="hide_mobile">Durată</th>
					<th class="hide_mobile">Tip cameră</th>
					<th class="hide_mobile" style="text-align:center; padding: 0;">Tip masă</th>
					<th class="hide_mobile" style="text-align:center;">Preț / cameră</th>
					<th class="hide_mobile" style="width:20px;"></th>
					<th class="hide_mobile" style="width:192px;"></th>
				</tr>
				<tr>
					<td style="background-color: #efffe9 !important">
						<span class="hide_desktop">DATA PLECĂRII</span>
						<h6>31.05.2017</h6>
					</td>
					<td style="background-color: #efffe9 !important"><span class="hide_desktop">DURATĂ</span>7 nopți</td>
					<td style="background-color: #efffe9 !important">
						<span class="hide_desktop">TIP CAMERĂ </span>
						<h6>Studio</h6>
						<span class="text-danger">Early Booking 40% pana la 31.03.2017</span><br>
						<span class="text-success">Plecare din <strong>Otopeni-Henri Coanda</strong> - Aegean-Airlines</span><br>
						<a data-toggle="collapse" href="#additional-info" aria-expanded="false" aria-controls="additional-info" class="hide_mobile collapsed">
							Additional info
						</a>
						<a class="hide_desktop link_card_mobile" href="#">
							Additional info
						</a>
						<div class="hide_desktop card_mobile">
							<div class="show_card_mobile" style="background-color: #efffe9 !important; border: 0; border-radius: none;">
								<div class="row">
									<div class="col-lg-4 col-sm-12">
										<h5>Servicii Incluse</h5>
										<p>
											7 nopți cazare cu masă conform ofertei
											Bilet avion Bucuresti - Heraklion și retur
											<span class="text-success">Taxele de aeroport Agentia de turism</span>
											Bagaj de mână și bagaj de cală
											Transfer aeroport - hotel - aeroport
											Asistență turistică în limba română
											Cazare cu masa conform ofertei
										</p>
									</div>

									<div class="col-lg-8 col-sm-12">
										<table class="table table-responsive" style="background-color: #efffe9 !important;">
											<tbody>
											<tr class="hide_mobile">
												<th class="text-center">Companie</th>
												<th class="text-center">Nr. cursă</th>
												<th class="text-center">Plecare</th>
												<th class="text-center">Sosire</th>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4231</td>
												<td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
												<td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4230</td>
												<td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
												<td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</td>
					<td style="background-color: #efffe9 !important;text-align:center;padding: 0; width: 100px;"><span class="hide_desktop">TIP MASĂ</span>Fără masă</td>
					<td style="background-color: #efffe9 !important">
						<span class="hide_desktop">PREȚ / CAMERĂ</span>
                        <span style="text-align: center;" class="old-price text-primary">902 €</span>
                        <span style="text-align: center;" class="current-price text-success">615 €</span>
                        <span class="text-center hide_desktop"><i class="fa fa-check-circle text-success"></i></span>
                        <span class="badge badge-success hide_desktop">Disponibil</span>
                    </td>
                    <td style="background-color: #efffe9 !important">
                    </td>
                    <td style="background-color: #efffe9 !important; text-align:center;">
                        <span class="text-center hide_mobile"><i class="fa fa-check-circle text-success"></i></span>
                        <span class="badge badge-success hide_mobile">Disponibil</span><br>
                        <a href="" class="btn btn-primary full_width_mobile ">
                            Rezerva acum
                        </a>
                    </td>
				</tr>
				<tr>
					<td colspan="7" style="padding: 0; border: 0">
						<div class="collapse" id="additional-info" aria-expanded="true">
							<div class="card card-block" style="background-color: #efffe9 !important; border: 0; border-radius: none;">
								<div class="row">
									<div class="col-lg-4 col-sm-12">
										<h5>Servicii Incluse</h5>
										<p>
											7 nopți cazare cu masă conform ofertei
											Bilet avion Bucuresti - Heraklion și retur
											<span class="text-success">Taxele de aeroport Agentia de turism</span>
											Bagaj de mână și bagaj de cală
											Transfer aeroport - hotel - aeroport
											Asistență turistică în limba română
											Cazare cu masa conform ofertei
										</p>
									</div>
									<div class="col-lg-8 col-sm-12">
										<table class="table table-responsive" style="background-color: #efffe9 !important;">
											<tbody>
											<tr class="hide_mobile">
												<th class="text-center">Companie</th>
												<th class="text-center">Nr. cursă</th>
												<th class="text-center">Plecare</th>
												<th class="text-center">Sosire</th>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4231</td>
												<td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
												<td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4230</td>
												<td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
												<td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="background-color: #fafafa !important">
						<span class="hide_desktop">DATA PLECĂRII</span>
						<h6>31.05.2017</h6>
					</td>
					<td style="background-color: #fafafa !important"><span class="hide_desktop">DURATĂ</span>7 nopți</td>
					<td style="background-color: #fafafa !important">
						<h6>Studio</h6>
						<span class="text-danger">Early Booking 40% pana la 31.03.2017</span><br>
						<span class="text-success">Plecare din <strong>Otopeni-Henri Coanda</strong> - Aegean-Airlines</span><br>

						<a data-toggle="collapse" href="#additional-info-2" aria-expanded="false" aria-controls="additional-info" class="hide_mobile collapsed">
							Additional info
						</a>
						<a class="hide_desktop link_card_mobile" href="">
							Additional info
						</a>
						<div class="hide_desktop card_mobile">
							<div class="show_card_mobile" style="background-color: #efffe9 !important; border: 0; border-radius: none;">
								<div class="row">
									<div class="col-lg-4 col-sm-12">
										<h5>Servicii Incluse</h5>
										<p>
											7 nopți cazare cu masă conform ofertei
											Bilet avion Bucuresti - Heraklion și retur
											<span class="text-success">Taxele de aeroport Agentia de turism</span>
											Bagaj de mână și bagaj de cală
											Transfer aeroport - hotel - aeroport
											Asistență turistică în limba română
											Cazare cu masa conform ofertei
										</p>
									</div>

									<div class="col-lg-8 col-sm-12">
										<table class="table table-responsive" style="background-color: #efffe9 !important;">
											<tbody>
											<tr class="hide_mobile">
												<th class="text-center">Companie</th>
												<th class="text-center">Nr. cursă</th>
												<th class="text-center">Plecare</th>
												<th class="text-center">Sosire</th>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4231</td>
												<td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
												<td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4230</td>
												<td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
												<td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</td>
					<td style="background-color: #fafafa !important;text-align:center;padding: 0; width: 100px;"><span class="hide_desktop">TIP MASĂ</span>Fără masă</td>
					<td style="background-color: #fafafa !important">
						<span class="hide_desktop">PREȚ / CAMERĂ</span>
                        <span style="text-align: center;" class="old-price text-primary">902 €</span>
                        <span style="text-align: center;" class="current-price text-success">615 €</span>
                        <span class="text-center hide_desktop"><i class="fa fa-check-circle text-success"></i></span>
                        <span class="badge badge-success hide_desktop">Necesita Confirmare</span>
                    </td>
                    <td style="background-color: #fafafa !important">

                    </td>
                    <td style="background-color: #fafafa !important;text-align:center;">
                        <span class="text-center hide_mobile"><i class="fa fa-check-circle text-success"></i></span>
                        <span class="badge badge-success hide_mobile">Necesita Confirmare</span><br>
                        <a href="" class="btn btn-primary full_width_mobile">Rezerva Acum</a>
                    </td>
				</tr>
				<tr class="hide_mobile">
					<td colspan="7" style="padding: 0; border: 0">
						<div class="collapse" id="additional-info-2" aria-expanded="true">
							<div class="card card-block" style="background-color: #efffe9 !important; border: 0; border-radius: none;">
								<div class="row">
									<div class="col-lg-4 col-sm-12">
										<h5>Servicii Incluse</h5>
										<p>
											7 nopți cazare cu masă conform ofertei
											Bilet avion Bucuresti - Heraklion și retur
											<span class="text-success">Taxele de aeroport Agentia de turism</span>
											Bagaj de mână și bagaj de cală
											Transfer aeroport - hotel - aeroport
											Asistență turistică în limba română
											Cazare cu masa conform ofertei
										</p>
									</div>

									<div class="col-lg-8 col-sm-12">
										<table class="table table-responsive" style="background-color: #efffe9 !important;">
											<tbody>
											<tr class="hide_mobile">
												<th class="text-center">Companie</th>
												<th class="text-center">Nr. cursă</th>
												<th class="text-center">Plecare</th>
												<th class="text-center">Sosire</th>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4231</td>
												<td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
												<td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4230</td>
												<td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
												<td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<span class="hide_desktop">DATA PLECĂRII</span>
						<h6>31.05.2017</h6>
					</td>
					<td style="background-color: #fafafa !important"><span class="hide_desktop">DURATĂ</span>7 nopți</td>
					<td>
						<span class="hide_desktop">TIP CAMERĂ </span>
						<h6>Studio</h6>
						<span class="text-danger">Early Booking 40% pana la 31.03.2017</span><br>
						<span class="text-success">Plecare din <strong>Otopeni-Henri Coanda</strong> - Aegean-Airlines</span><br>
						<a class="hide_mobile collapsed" id="additional-info"  data-toggle="collapse" href="#additional-info-3" aria-expanded="false" aria-controls="additional-info">
							Additional info
						</a>
						<a class="hide_desktop link_card_mobile" href="#">
							Additional info
						</a>
						<div class="hide_desktop card_mobile">
							<div class="show_card_mobile" style="background-color: #efffe9 !important; border: 0; border-radius: none;">
								<div class="row">
									<div class="col-lg-4 col-sm-12">
										<h5>Servicii Incluse</h5>
										<p>
											7 nopți cazare cu masă conform ofertei
											Bilet avion Bucuresti - Heraklion și retur
											<span class="text-success">Taxele de aeroport Agentia de turism</span>
											Bagaj de mână și bagaj de cală
											Transfer aeroport - hotel - aeroport
											Asistență turistică în limba română
											Cazare cu masa conform ofertei
										</p>
									</div>

									<div class="col-lg-8 col-sm-12">
										<table class="table table-responsive" style="background-color: #efffe9 !important;">
											<tbody>
											<tr class="hide_mobile" >
												<th class="text-center ">Companie</th>
												<th class="text-center ">Nr. cursă</th>
												<th class="text-center ">Plecare</th>
												<th class="text-center ">Sosire</th>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4231</td>
												<td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
												<td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4230</td>
												<td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
												<td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</td>
					<td style="background-color: #fafafa !important;text-align:center;padding: 0; width: 100px;"><span class="hide_desktop">TIP MASĂ</span>Fără masă</td>
					<td>
						<span class="hide_desktop">PREȚ / CAMERĂ</span>
                        <span style="text-align: center;" class="old-price text-primary">902 €</span>
                        <span style="text-align: center;" class="current-price text-success">615 €</span>
                        <span class="text-center hide_desktop"><i class="fa fa-minus-circle text-danger"></i></span>
                        <span class="badge badge-danger hide_desktop">Indisponibil</span>
                    </td>
                    <td>

                    </td>
                    <td  style="text-align:center;" >
                        <span class="text-center hide_mobile"><i class="fa fa-minus-circle text-danger"></i></span>
                        <span class="badge badge-danger hide_mobile">Indisponibil</span><br>
                        <a href="" class="btn btn-primary red_change_data full_width_mobile">Schimba Datele</a>
                    </td>
				</tr>
				<tr class="hide_mobile">
					<td colspan="7" style="padding: 0; border: 0">
						<div class="collapse" id="additional-info-3" aria-expanded="true">
							<div class="card card-block" style="background-color: #efffe9 !important; border: 0; border-radius: none;">
								<div class="row">
									<div class="col-lg-4 col-sm-12">
										<h5>Servicii Incluse</h5>
										<p>
											7 nopți cazare cu masă conform ofertei
											Bilet avion Bucuresti - Heraklion și retur
											<span class="text-success">Taxele de aeroport Agentia de turism</span>
											Bagaj de mână și bagaj de cală
											Transfer aeroport - hotel - aeroport
											Asistență turistică în limba română
											Cazare cu masa conform ofertei
										</p>
									</div>

									<div class="col-lg-8 col-sm-12">
										<table class="table table-responsive" style="background-color: #efffe9 !important;">
											<tbody>
											<tr class="hide_mobile" >
												<th class="text-center ">Companie</th>
												<th class="text-center ">Nr. cursă</th>
												<th class="text-center ">Plecare</th>
												<th class="text-center ">Sosire</th>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4231</td>
												<td class="text-center"><strong>OTP</strong> - 31.05.2017 - 13:15</td>
												<td class="text-center"><strong>HER</strong> - 31.05.2017 - 15:15</td>
											</tr>
											<tr>
												<td class="text-center"><img src="https://www.ocaziituristice.ro/images/avion/20.jpg" alt=""></td>
												<td class="text-center">4230</td>
												<td class="text-center"><strong>HER</strong> - 07.06.2017 - 15:10</td>
												<td class="text-center"><strong>OTP</strong> - 07.06.2017 - 17:00</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
<div style="background-color: #fafafa; padding: 15px; font-size: 14px; margin-bottom: 20px">
				<strong>Legendă:</strong><br> <i class="fa fa-check-circle text-success"></i> Disponibil, <i class="fa fa-check-circle text-primary">

				</i> Disponibil, necesită confirmare, <i class="fa fa-minus-circle text-danger"></i> Indisponibil
				<br><br>
				<strong>OcaziiTuristice.ro</strong> se conecteaza in timp real la sistemele tour operatorilor din Romania si strainatate pentru a va oferi cele mai bune preturi disponibile.
				<br><br>
				<strong>Atenție!</strong> <i>Trimiterea unei rezervări <strong>nu implică</strong> nici o obligație financiară din partea dumneavoastră!</i>
			</div>            