<?php
$id_furnizor=$valoare_furnizor;
$plecdata=date('Y-m-d', strtotime($request_data_plecare));
$arivdata = date('Y-m-d', strtotime($plecdata.' + '.$pleczile.' days'));

$trip_Room_Chd_Age = '';
for($i=0; $i<$copii; $i++) {
	$trip_Room_Chd_Age .= '<Age>'.$copil_age[$i].'</Age>';
}

if($detalii['moneda']=='EURO') $trip_currency = 'EUR';
	else $trip_currency = $detalii['moneda'];

if($cazare=='nu') {
$trip_currency = 'EUR';
/*$sel_plecare = "SELECT
localitati.denumire AS loc_plecare,
localitati.id_localitate AS idloc_plecare
FROM oferte_transport_avion
LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
WHERE oferte_transport_avion.id_oferta = '".$id_oferta."'
AND oferte_transport_avion.tip = 'dus'
ORDER BY oferte_transport_avion.ordonare
LIMIT 0,1 ";
$que_plecare = mysql_query($sel_plecare) or die(mysql_error());
$row_plecare = mysql_fetch_array($que_plecare);*/

/* $sel_request = "SELECT
import_hoteluri.nume_hotel,
import_hoteluri.stele_hotel,
preturi_p45.hotel_destCode
FROM import_hoteluri
Inner Join preturi_p45 ON preturi_p45.hotel_code = import_hoteluri.id_hotel
WHERE import_hoteluri.id_hotel_corespondent = '".$id_hotel."'
AND import_hoteluri.id_furnizor = '".$id_furnizor."'
AND preturi_p45.period = '".$pleczile."'

AND preturi_p45.price_currency = '".$trip_currency."'
AND preturi_p45.trans_code <> ''
LIMIT 0,1 ";*/

/*$sel_request = "SELECT
import_hoteluri.nume_hotel,
import_hoteluri.stele_hotel,
localitati_corespondent.code_furnizor as hotel_destCode
FROM import_hoteluri
left join localitati_corespondent on import_hoteluri.city=localitati_corespondent.id
WHERE import_hoteluri.id_hotel_corespondent = '".$id_hotel."'
AND import_hoteluri.id_furnizor = '".$id_furnizor."'";
// AND preturi_p45.`date` = '".$plecdata."'
//echo print_r($depCityCode);
$que_request = mysql_query($sel_request) or die(mysql_error());
$row_request = mysql_fetch_array($que_request);*/
//$row_plecare['idloc_plecare'];
//$trip_CountryCode = $detalii_hotel['country_code'];
//$trip_CityCode = $row_request['hotel_destCode'];
$trip_DepCityCode = $depCityCodeZone[$id_furnizor][$valoare_loc_plecare];
$Destination_array=explode("-",$valoare_loc_sosire);
$Destination=$Destination_array[0];
$Destination_country=$Destination_array[1];
//$trip_DepCityCode=$depCityZone[$id_furnizor][$valoare_loc_plecare];


if($detalii['transport']=='Avion') $trip_Transport = 'plane';
$trip_ProductName = $row_request['nume_hotel'];
if($trip_ProductName=="Double Tree by Hilton")$trip_ProductName="Tree by Hilton";
$trip_ProductCategory = $row_request['stele_hotel'];
$trip_CurrencyCode = $trip_currency;


$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getPackageNVPriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>dreamvoyage</RequestUser>
    <RequestPass>travel</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
    <RequestLang>EN</RequestLang>
  </AuditInfo>
  <RequestDetails>
    <getPackageNVPriceRequest>
      <CountryCode>'.$Destination_country.'</CountryCode>
      <CityCode>'.$Destination.'</CityCode>
      <DepCountryCode>RO</DepCountryCode>
      <DepCityCode>'.$trip_DepCityCode.'</DepCityCode>
      <Transport>plane</Transport>
      <TourOpCode>P45</TourOpCode>
      <ProductName></ProductName>
      <ProductCategory></ProductCategory>
      <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
      <Language>RO</Language>
      <OfferType>TOATE</OfferType>
      <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
      <Days>0</Days>
      <Rooms>
        <Room Code="DB" NoAdults="'.$adulti.'" NoChildren="0">
          <Children>
            '.$trip_Room_Chd_Age.'
          </Children>
        </Room>
      </Rooms>
    </getPackageNVPriceRequest>
  </RequestDetails>
</Request>';
//if(!$err_logare_admin) echo $sXml;

$sUrl = 'http://rezervari.paralela45.ro/server_xml/server.php';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);
//if(!$err_logare_admin) echo $sResponse;

$prices = new SimpleXMLElement($sResponse);
//if(!$err_logare_admin) { echo '<pre>';print_r($prices);echo '</pre>'; }
if(count($rezcam)>0) $J = count($rezcam)+1; else $J=0;

if($prices->ResponseDetails->getPackageNVPriceResponse != '') {
	foreach($prices->ResponseDetails->getPackageNVPriceResponse->Hotel as $key => $value_1) {
	$hotel_code=(string)$value_1->Product->ProductCode;

	//foreach($hotel_code_1 as $key=>$value_h){$hotel_code=$value_h;}
	foreach($value_1->Offers->Offer as $key => $value) {
		//if(sizeof($value->BookingRoomTypes->Room)>0) {
		if(isset($value->BookingRoomTypes->Room)){
		$den_camera_1[$key] = (string)$value->BookingRoomTypes->Room;
		}
		$sel_roomtype = "SELECT tip_camera_corespondent.id_camera_corespondent, tip_camera.denumire
		FROM tip_camera_corespondent
		INNER JOIN tip_camera ON tip_camera.id_camera = tip_camera_corespondent.id_camera_corespondent
		WHERE tip_camera_corespondent.den_camera = '".$den_camera_1[$key]."' ";
		$que_roomtype = mysql_query($sel_roomtype) or die(mysql_error());
		$row_roomtype = mysql_fetch_array($que_roomtype);
	//}
		$rezcam[$J]['id_pret'] = 0;
		$rezcam[$J]['plecare'] = '1';
		$rezcam[$J]['id_camera'] = $row_roomtype['id_camera_corespondent'];
		if($row_roomtype['denumire']) $rezcam[$J]['denumire_camera'] = $row_roomtype['denumire']; else $rezcam[$J]['denumire_camera'] = schimba_caractere($den_camera_1[$key]);
		$rezcam[$J]['pret'] = (string) $value->Gross;
		$rezcam[$J]['comision'] = (string) $value->CommissionCed;
		$rezcam[$J]['id_oferta_furnizor'] = (string) $value->PackageId;
		$rezcam[$J]['variant_id'] = (string) $value->PackageVariantId;
	
                               
         
          

		$rezcam[$J]['moneda'] = (string) $value->attributes()->CurrencyCode;
		$rezcam[$J]['nr_adulti'] = $adulti;
		$rezcam[$J]['nr_copii'] = $copii;
		$rezcam[$J]['copil1'] = $copil_age[0];
		$rezcam[$J]['copil2'] = $copil_age[1];
		$rezcam[$J]['copil3'] = $copil_age[2];
		if(isset($value->OfferDescription) and ((string) $value->Gross != (string) $value->PriceNoRedd)) {
			$rezcam[$J]['oferta'] = schimba_caractere((string) $value->OfferDescription);
			$rezcam[$J]['oferta_pret'] = (string) $value->PriceNoRedd;
		} else {
			$rezcam[$J]['oferta'] = '';
			$rezcam[$J]['oferta_pret'] = '';
		}
		$rezcam[$J]['data_early_booking']=extrage_data($rezcam[$J]['oferta']);  
					$rezcam[$J]['PackageVariantId'] = (string) $value->PackageVariantId;
			foreach ($value->BookingRoomTypes->Room as $r_key => $r_valoare)
		  {
			 $rezcam[$J]['RoomCode']=(string)$r_valoare['Code'];
		  }
		
		$rezcam[$J]['masa'] = '';
		if(sizeof($value->Meals->Meal)>0) {
			foreach($value->Meals->Meal as $k_meal => $v_meal) {
				$rezcam[$J]['masa'] .= schimba_caractere(change_meal((string) $v_meal)).', ';
			}
			$rezcam[$J]['masa'] = substr($rezcam[$J]['masa'],0,-2);
		} else {
			$rezcam[$J]['masa'] .= 'Fără masă';
		}
		//if(((string) $value->Meals->Meal)=='') $rezcam[$J]['masa'] = 'Fara masa'; else $rezcam[$J]['masa'] = (string) $value->Meals->Meal;
		$rezcam[$J]['series_name'] = '';
		$rezcam[$J]['period'] = date("d.m",strtotime((string) $value->PeriodOfStay->CheckIn)).' - '.date("d.m.Y",strtotime((string) $value->PeriodOfStay->CheckOut));
		$rezcam[$J]['data_start'] = date("Y-m-d",strtotime((string) $value->PeriodOfStay->CheckIn));
		if((string) $value->Availability['Code'] == 'IM') $rezcam[$J]['disponibilitate'] = 'disponibil';
			elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'request';
			elseif((string) $value->Availability['Code'] == 'ST') $rezcam[$J]['disponibilitate'] = 'stopsales';
		$rezcam[$J]['nr_nights'] = dateDiff((string) $value->PeriodOfStay->CheckOut, (string) $value->PeriodOfStay->CheckIn)-1;
		
		/*foreach($value->PriceDetails->Services->Service as $k_service => $v_service) {
			if((string) $v_service->Type == '7' and (string) $v_service->Transport == 'plane') {
			
			if( date("Y-m-d",strtotime((string) $value->PeriodOfStay->CheckIn))==date("Y-m-d", strtotime($v_service->PeriodOfStay->CheckIn))){
			$flight[$id_oferta]['plecare'][0][0]['from'] = (string) $v_service->Departure['Code'];
			$flight[$id_oferta]['plecare'][0][0]['to'] = (string) $v_service->Arrival['Code'];
			$flight[$id_oferta]['plecare'][0][0]['data_plecare'] = date("Y-m-d H:i:s",strtotime((string) $v_service->PeriodOfStay->CheckIn));
			$flight[$id_oferta]['plecare'][0][0]['data_sosire'] =date("Y-m-d H:i:s",strtotime((string) $v_service->PeriodOfStay->CheckOut));
			$flight[$id_oferta]['plecare'][0][0]['companie']= schimba_caractere(strtolower((string) $v_service->Company));
			$flight[$id_oferta]['plecare'][0][0]['nr_cursa'] = $cursa[1];
			$flight[$id_oferta]['plecare'][0][0]['seats'] = (string) $v_service->Seats;
			}
			
			if( date("Y-m-d",strtotime((string) $value->PeriodOfStay->CheckOut))==date("Y-m-d", strtotime($v_service->PeriodOfStay->CheckIn))){
			$flight[$id_oferta]['intoarcere'][0][0]['from'] = (string) $v_service->Departure['Code'];
			$flight[$id_oferta]['intoarcere'][0][0]['to'] = (string) $v_service->Arrival['Code'];
			$flight[$id_oferta]['intoarcere'][0][0]['data_plecare'] = date("Y-m-d H:i:s",strtotime((string) $v_service->PeriodOfStay->CheckIn));
			$flight[$id_oferta]['intoarcere'][0][0]['data_sosire'] =date("Y-m-d H:i:s",strtotime((string) $v_service->PeriodOfStay->CheckOut));
			$flight[$id_oferta]['intoarcere'][0][0]['companie']= schimba_caractere(strtolower((string) $v_service->Company));
			$flight[$id_oferta]['intoarcere'][0][0]['nr_cursa'] = $cursa[1];
			$flight[$id_oferta]['intoarcere'][0][0]['seats'] = (string) $v_service->Seats;
			
			}
			
				
				
				if((string) $v_service->Availability['Code']=='IM') $flight[$id_oferta]['flight_bookable'] = 1; else $flight[$id_oferta]['flight_bookable'] = 0;
				$avion[]['seats'] = $v_service->Seats;
			}
		}*/
		
		if($flight[$id_oferta]['flight_bookable']==0) $rezcam[$J]['disponibilitate'] = 'stopsales';
		
/*		if(sizeof($avion)>0) {
			
			//echo "c=".$flight[$id_oferta]['plecare'][0][0]['companie']= (string) $value->PriceDetails->Service;
			
			$sel_plecare = "SELECT * FROM preturi_autoupdate WHERE id_oferta = '".$id_oferta."' ";
			$que_plecare = mysql_query($sel_plecare) or die(mysql_error());
			$row_plecare = mysql_fetch_array($que_plecare);
			
			$sel_flight = "SELECT * FROM preturi_p45 WHERE hotel_code = '".$row_plecare['hotel_code']."' AND date = '".$rezcam[$J]['data_start']."' AND period = '".$row_plecare['perioada']."' AND trans_dep_city = '".$row_plecare['plecare']."' ";
			$que_flight = mysql_query($sel_flight) or die(mysql_error());
			$row_flight = mysql_fetch_array($que_flight);
			
			$cursa = explode(" ", $row_flight['trans_code']);
			
			//$flight[$id_oferta]['plecare'][0][0]['from'] = $row_flight['trans_dep_city'];
			//$flight[$id_oferta]['plecare'][0][0]['to'] = $row_flight['trans_des_city'];
			//$flight[$id_oferta]['plecare'][0][0]['data_plecare'] = $row_flight['trans_dep_depDate'];
			//$flight[$id_oferta]['plecare'][0][0]['data_sosire'] = $row_flight['trans_dep_arrDate'];
			//$flight[$id_oferta]['plecare'][0][0]['companie'] = $row_flight['trans_airlineCode'];
			$flight[$id_oferta]['plecare'][0][0]['nr_cursa'] = $cursa[1];
			$flight[$id_oferta]['plecare'][0][0]['seats'] = $avion[0]['seats'];
			
			$flight[$id_oferta]['intoarcere'][1][0]['from'] = $row_flight['trans_des_city'];
			$flight[$id_oferta]['intoarcere'][1][0]['to'] = $row_flight['trans_dep_city'];
			$flight[$id_oferta]['intoarcere'][1][0]['data_plecare'] = $row_flight['trans_des_depDate'];
			$flight[$id_oferta]['intoarcere'][1][0]['data_sosire'] = $row_flight['trans_des_arrDate'];
			$flight[$id_oferta]['intoarcere'][1][0]['companie'] = $row_flight['trans_airlineCode'];
			$flight[$id_oferta]['intoarcere'][1][0]['nr_cursa'] = $cursa[1];
			$flight[$id_oferta]['intoarcere'][1][0]['seats'] = $avion[1]['seats'];
		}*/
		
		$rezcam[$J]['id_furnizor'] = $id_furnizor;
		$rezcam[$J]['furnizor_comision_procent'] = $furnizor['comision_procent'];
		if ($detalii['discount_tarif']>0) {$rezcam[$J]['furnizor_comision_procent']=$detalii['discount_tarif'];}

		$rezcam[$J]['id_hotel'] =$hotel_code;
		
		$rezcam[$J]['id_oferta'] = $hotel_cod_oferte[$rezcam[$J]['id_hotel']];
		
		
		//$rezcam[$J]['id_oferta'] = $id_oferta;
		
		$J++;
	}
}
}
//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; }


} //end if($detalii['cazare']=='nu')
else if($detalii['cazare']=='da') {

$sel_request = "SELECT
import_hoteluri.*,
localitati_corespondent.*,
import_hoteluri.nume_hotel,
import_hoteluri.stele_hotel,
localitati_corespondent.code_furnizor AS hotel_destCode
FROM import_hoteluri
INNER JOIN hoteluri ON hoteluri.id_hotel = import_hoteluri.id_hotel_corespondent
INNER JOIN localitati_corespondent ON localitati_corespondent.id_corespondent = hoteluri.locatie_id
WHERE import_hoteluri.id_hotel_corespondent = '".$id_hotel."'
AND import_hoteluri.id_furnizor = '".$id_furnizor."'
AND localitati_corespondent.id_furnizor = '".$id_furnizor."'
LIMIT 0,1 ";
$que_request = mysql_query($sel_request) or die(mysql_error());
$row_request = mysql_fetch_array($que_request);

$trip_CountryCode = $detalii_hotel['country_code'];
$trip_CityCode = $row_request['hotel_destCode'];
$trip_ProductName = $row_request['nume_hotel'];
$trip_ProductCategory = $row_request['stele_hotel'];
$trip_CurrencyCode = $trip_currency;


$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelPriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>dreamvoyage</RequestUser>
    <RequestPass>travel</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
    <RequestLang>EN</RequestLang>
  </AuditInfo>
  <RequestDetails>
    <getHotelPriceRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
      <TourOpCode>P45</TourOpCode>
      <ProductName>'.$trip_ProductName.'</ProductName>
      <ProductCategory>'.$trip_ProductCategory.'</ProductCategory>
      <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
      <Language>RO</Language>
      <OfferType>TOATE</OfferType>
      <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
      <Rooms>
        <Room Code="DB" NoAdults="'.$adulti.'" NoChildren="'.$copii.'">
          <Children>
            '.$trip_Room_Chd_Age.'
          </Children>
        </Room>
      </Rooms>
    </getHotelPriceRequest>
  </RequestDetails>
</Request>';

$sUrl = 'http://rezervari.paralela45.ro/server_xml/server.php';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);
//if(!$err_logare_admin) echo $sXml;

$prices = new SimpleXMLElement($sResponse);
//if(!$err_logare_admin) { echo '<pre>';print_r($prices);echo '</pre>'; }
if($prices->ResponseDetails->getHotelPriceResponse != '') {
	$J=0;
	foreach($prices->ResponseDetails->getHotelPriceResponse->Hotel->Offers->Offer as $key => $value) {
		$den_camera[$key] = (string) $value->BookingRoomTypes->Room;
		$checkIn[$key] = (string) $value->PeriodOfStay->CheckIn;
		$checkOut[$key] = (string) $value->PeriodOfStay->CheckOut;
		$numar_de_nopti[$key] = dateDiff($checkOut[$key], $checkIn[$key])-1;
		//if($numar_de_nopti[$key]==$pleczile) {
		//if((string) $value->Availability['Code'] == 'IM') {
			$sel_roomtype = "SELECT tip_camera_corespondent.id_camera_corespondent, tip_camera.denumire
			FROM tip_camera_corespondent
			INNER JOIN tip_camera ON tip_camera.id_camera = tip_camera_corespondent.id_camera_corespondent
			WHERE tip_camera_corespondent.den_camera = '".$den_camera[$key]."' ";
			$que_roomtype = mysql_query($sel_roomtype) or die(mysql_error());
			$row_roomtype = mysql_fetch_array($que_roomtype);
		
			$rezcam[$J]['id_pret'] = 0;
			$rezcam[$J]['plecare'] = '1';
			$rezcam[$J]['id_camera'] = $row_roomtype['id_camera_corespondent'];
			if($row_roomtype['denumire']) $rezcam[$J]['denumire_camera'] = $row_roomtype['denumire']; else $rezcam[$J]['denumire_camera'] = schimba_caractere($den_camera[$key]);
			$rezcam[$J]['pret'] = round((string) $value->Gross);
			$rezcam[$J]['comision'] = (string) $value->CommissionCed;
			$rezcam[$J]['moneda'] = $detalii['moneda'];
			$rezcam[$J]['nr_adulti'] = $adulti;
			$rezcam[$J]['nr_copii'] = $copii;
			$rezcam[$J]['copil1'] = $copil_age[0];
			$rezcam[$J]['copil2'] = $copil_age[1];
			$rezcam[$J]['copil3'] = $copil_age[2];
			if(isset($value->OfferDescription) and ((string) $value->Gross != (string) $value->PriceNoRedd)) {
				$rezcam[$J]['oferta'] = schimba_caractere((string) $value->OfferDescription);
				$rezcam[$J]['oferta_pret'] = round((string) $value->PriceNoRedd);
			} else {
				$rezcam[$J]['oferta'] = '';
				$rezcam[$J]['oferta_pret'] = '';
			}
			$rezcam[$J]['masa'] = '';
			if(sizeof($value->Meals->Meal)>0) {
				foreach($value->Meals->Meal as $k_meal => $v_meal) {
					$rezcam[$J]['masa'] .= schimba_caractere(change_meal((string) $v_meal)).', ';
				}
				$rezcam[$J]['masa'] = substr($rezcam[$J]['masa'],0,-2);
			} else {
				$rezcam[$J]['masa'] .= 'Fără masă';
			}
			//if(((string) $value->Meals->Meal)=='') $rezcam[$J]['masa'] = 'Fara masa'; else $rezcam[$J]['masa'] = (string) $value->Meals->Meal;
			//$rezcam[$J]['series_name'] = (string) $value->SeriesName;
			$rezcam[$J]['PackageVariantId'] = (string) $value->PackageVariantId;
			foreach ($value->BookingRoomTypes->Room as $r_key => $r_valoare)
		  {
			 $rezcam[$J]['RoomCode']=(string)$r_valoare['Code'];
		  }
			$rezcam[$J]['grila_name'] = schimba_caractere((string) $value->GrilaName);
			$rezcam[$J]['period'] = date("d.m",strtotime($checkIn[$key])).' - '.date("d.m.Y",strtotime($checkOut[$key]));
			$rezcam[$J]['data_start'] = date("Y-m-d",strtotime($checkIn[$key]));
			if((string) $value->Availability['Code'] == 'IM') $rezcam[$J]['disponibilitate'] = 'disponibil';
				elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'request';
				//elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'disponibil';
				elseif((string) $value->Availability['Code'] == 'ST') $rezcam[$J]['disponibilitate'] = 'stopsales';
			$rezcam[$J]['nr_nights'] = $numar_de_nopti[$key];
			
			$rezcam[$J]['id_furnizor'] = $id_furnizor;
			$rezcam[$J]['furnizor_comision_procent'] = $furnizor['comision_procent'];
			if ($detalii['discount_tarif']>0) {$rezcam[$J]['furnizor_comision_procent']=$detalii['discount_tarif'];}

			$J++;
		//}
	}
}



} //end if($detalii['cazare']=='da')

if($id_oferta_furnizor!='' and $cod_camera!='' and $pagina_curenta=='rezervare' ){
 $sXml ='<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getItemFeesRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>dreamvoyage</RequestUser>
    <RequestPass>travel</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
    <RequestLang>EN</RequestLang>
  </AuditInfo>
  <RequestDetails>
    <getItemFeesRequest CurrencyCode="EUR">
      <BookingItems>
        <BookingItem ProductType="hotel">
          <TourOpCode>P45</TourOpCode>
          <HotelItem>
           <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
             <ProductCode>'.$product_code.'</ProductCode>
            <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
            </PeriodOfStay>
              <PackageId>132</PackageId>
		    <VariantId>'.$id_oferta_furnizor.'</VariantId>
            <Rooms>
              <Room Code="'.$cod_camera.'" NoAdults="2">
                <PaxNames>
                  <PaxName PaxType="adult">ADULT 1</PaxName>
                  <PaxName PaxType="adult">ADULT 2</PaxName>
                </PaxNames>
              </Room>
            </Rooms>
          </HotelItem>
        </BookingItem>
      </BookingItems>
    </getItemFeesRequest>
  </RequestDetails>
</Request>';


$sUrl = 'http://rezervari.paralela45.ro/server_xml/server.php';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);
// echo $sXml;
//echo $sResponse;
$anulare = new SimpleXMLElement($sResponse);

//if(!$err_logare_admin) { echo '<pre>';print_r($anulare->ResponseDetails->getItemFeesResponse->ItemFees->ItemFee->Fees);echo '</pre>'; }

//$conditii_anulare = new SimpleXMLElement($anulare);


if($anulare->ResponseDetails->getItemFeesResponse->ItemFees->ItemFee->Fees != '') 
	{ 
		if(count($conditii_anulare)>0) $JA = count($anulare)+1; else $JA=0;
		foreach($anulare->ResponseDetails->getItemFeesResponse->ItemFees->ItemFee->Fees->Fee as $key => $value)
		{
			$conditii_anulare[$JA]['data_inceput']=(string)$value->FromDate;
			$conditii_anulare[$JA]['data_sfarsit']=(string)$value->ToDate;
			//$pret_total=round($total_pret);
			
			$conditii_anulare[$JA]['valoare']=round((int)$value->Value*100/round($total_pret));
			if (($conditii_anulare[$JA]['valoare'])>100) {$conditii_anulare[$JA]['valoare']=100;}
			
			foreach ($value->Value as $v_key => $v_valoare)
		  {
			 if((string)$v_valoare['Procent'])$conditii_anulare[$JA]['tip_valoare_anulare']='procent';
		  }
			$JA++;
		}
		
	}
//echo '<pre>';print_r($conditii_anulare);echo '</pre>'; 


}
//}

/* $pagina_1=$_SERVER['PHP_SELF'];;
if(!$err_logare_admin and  $pagina_1=='/includes/preturi_online_oferta.php') { 
$sXml1 = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getPackageNVRoutesRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>dreamvoyage</RequestUser>
    <RequestPass>travel</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
    <RequestLang>EN</RequestLang>
  </AuditInfo>
 
    <RequestDetails>
    <getPackageNVRoutesRequest>
      <Transport>plane</Transport>
	  	
	
	</getPackageNVRoutesRequest>
  </RequestDetails>



 
</Request>';

$sUrl = 'http://rezervari.paralela45.ro/server_xml/server.php';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);
//if(!$err_logare_admin) echo $sXml;

$date_plecare = new SimpleXMLElement($sResponse);
//if(!$err_logare_admin) { echo '<pre>';print_r($date_plecare);echo '</pre>'; } 


foreach($date_plecare->ResponseDetails->getPackageNVRoutesResponse->Country as $key => $value)
{
echo "<br/>".$value->CountryCode;
	if($value->CountryCode=='GR')
	{
		
		echo "aici e grecia";
		foreach($value->Destinations->Destination as $key_1=>$value_11)
		{
		echo '<pre>';print_r($value_11);echo '</pre>';
		if ($value_11->Citycode==$trip_CityCode) 
		{
			echo $trip_CityCode;
			
			}
					
			}
		}
	}



$data_plecare=rtrim($date_plecare,";");
$date_plecare_array=explode(";",$date_plecare);
$date_plecare_array=array_unique($date_plecare_array);
//$data_plecare_array=array_filter($data_plecare_array,''); 


echo $sel_oferta="Select min(pret) as pret_minim, data_start,tip_camera  from data_pret_oferta where id_oferta=".$id_oferta." group by data_start";
$que_oferta = mysql_query($sel_oferta) or die(mysql_error());
$a=0;
while ($row_oferta = mysql_fetch_array($que_oferta))
{
	$date_existente[$a]['pret_minim']=$row_oferta['pret_minim'];
	$date_existente[$a]['data_start']=$row_oferta['data_start'];
	$date_existente_m[$a]=$row_oferta['data_start'];
	$date_existente[$a]['tip_camera']=$row_oferta['tip_camera'];
	
	$a++;
	}
$tip_camera=$date_existente[0]['tip_camera'];
$pret_minim=$date_existente[0]['pret_minim'];
mysql_free_result ( $que_oferta  );
$date_existente=msort($date_existente, 'data_start', $sort_flags = SORT_REGULAR);
//echo '<pre>';print_r($date_existente_m);echo '</pre>'; 

asort($date_plecare_array);
$result = array_diff($date_plecare_array, $date_existente_m);
//echo '<pre>';print_r($date_plecare_array);echo '</pre>'; 

//echo '<pre>';print_r($result);echo '</pre>'; 
if(count($result)>1){echo "fa update";
//sterge din data pret oferta
//$del_preturi="delete from data_pret_oferta where id_oferta=".$id_oferta;
$que_del_preturi = mysql_query($del_preturi) or die(mysql_error());
//mysql_free_result ( $que_del_preturi  );

//adauga in data pret oferta
foreach ($date_plecare_array as $value_1)
{
if(e_data($value_1)){
$update_preturi="insert into data_pret_oferta (id_oferta, data_start,data_end,tip_camera,pret,moneda,id_hotel,of_logictour,id_masa)values('".$id_oferta."','".$value_1."','0000-00-00','".$tip_camera."','".$pret_minim."','EURO','0','0','0')";
//$que_update_preturi = mysql_query($update_preturi) or die(mysql_error());
//mysql_free_result ($que_update_preturi);
}
}

}
}*/







$one_time_only = 'da';
?>