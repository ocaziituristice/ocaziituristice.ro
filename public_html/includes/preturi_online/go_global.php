<?php

use App\Models\PreturiAutoupdate;
use App\Components\Supplier\Factory as SupplierFactory;
use App\Models\ImportHoteluri;
use App\Components\Supplier\Exception;
use Phalcon\Di\FactoryDefault;
use Carbon\Carbon;

$di = FactoryDefault::getDefault();

/** @var \Phalcon\Http\Request $request */
$request = $di->get('request');
/** @var \Phalcon\Config $config */
$config = $di->get('config');

$offerId = $request->getQuery('id_oferta', 'int');
$departureDate = $request->getQuery('plecdata');
$days = $request->getQuery('pleczile', 'int');
$adults = $request->getQuery('adulti', 'int');
$children = $request->getQuery('copii', 'int');
$child1 = $request->getQuery('copil1');
$child2 = $request->getQuery('copil2');
$child3 = $request->getQuery('copil3');
$transport = $request->getQuery('trans');
$from = $request->getQuery('orasplec');
$food = $request->getQuery('masa');

$startDateCarbon = Carbon::createFromFormat('d.m.Y', $departureDate);
$departureDate = $startDateCarbon->toDateString();

try {
    $supplierAdapter = SupplierFactory::load($config->application->suppliers->goGlobal);
} catch (Exception $exception) {
    d($exception->getMessage());
}

$preturiAutoUpdate = PreturiAutoupdate::findFirst([
    'conditions' => 'idOferta = :idOferta: AND idFurnizor = :idFurnizor:',
    'bind' => ['idOferta' => $id_oferta, 'idFurnizor' => $id_furnizor]
]);

$hotelId = ImportHoteluri::getHotelByIdAndSupplier($id_hotel, $id_furnizor, $detalii_hotel['country_code']);

$childAge = [];
if (!empty($child1)) {
    $childAge[] = $child1;
}

if (!empty($child2)) {
    $childAge[] = $child2;
}

if (!empty($child3)) {
    $childAge[] = $child3;
}

try {
    $hotelSearch = $supplierAdapter->getHotelSearch()
                                   ->setDateFrom($departureDate)
                                   ->setNights($days)
                                   ->addRoom($adults, $childAge)
                                   ->setHotelCode($preturiAutoUpdate->getHotelCode())
                                   ->setCity($hotelId['hotel_destCode'])
                                   ->setSort(1);
    //d($hotelSearch->toXml());
    $response = $hotelSearch->getResponse()->GetData();
    //d(var_export($response));
} catch (\Exception $exception) {
    d($exception->getMessage());
}
//
//$response = [
//    7076 => [
//        'hotel_code' => '7076',
//        'hotel_name' => 'MARIANO',
//        'country_id' => '37',
//        'category' => '3',
//        'location' => 'TERMINI RAILWAY STATION',
//        'thumbnail' => 'https://cdn.goglobal.travel/HotelsV3/7076/7076_1t.jpg',
//        'latitude' => null,
//        'longitude' => null,
//        'ta_rating' => null,
//        'ta_rating_url' => null,
//        'ta_review_count' => null,
//        'ta_review_url' => null,
//        'city_id' => 1661,
//        'rooms' => [
//            0 => [
//                'hotel_search_code' => '29515/1054727896710945437/5',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'RO',
//                'price' => '161',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            1 => [
//                'hotel_search_code' => '29515/1054727896710945437/97',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'RO',
//                'price' => '161',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            2 => [
//                'hotel_search_code' => '29515/1054727896710945437/189',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'RO',
//                'price' => '161',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            3 => [
//                'hotel_search_code' => '29515/1054727896710945437/281',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'RO',
//                'price' => '161',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            4 => [
//                'hotel_search_code' => '29515/1054727896710945437/6',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'RO',
//                'price' => '242',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            5 => [
//                'hotel_search_code' => '29515/1054727896710945437/98',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'RO',
//                'price' => '242',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            6 => [
//                'hotel_search_code' => '29515/1054727896710945437/190',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'RO',
//                'price' => '242',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            7 => [
//                'hotel_search_code' => '29515/1054727896710945437/282',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'RO',
//                'price' => '242',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            8 => [
//                'hotel_search_code' => '29515/1054727896710945437/7',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'TRIPLE STANDARD',
//                'board_id' => 'RO',
//                'price' => '322',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            9 => [
//                'hotel_search_code' => '29515/1054727896710945437/99',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'TRIPLE STANDARD',
//                'board_id' => 'RO',
//                'price' => '322',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            10 => [
//                'hotel_search_code' => '29515/1054727896710945437/191',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'TRIPLE STANDARD',
//                'board_id' => 'RO',
//                'price' => '322',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            11 => [
//                'hotel_search_code' => '29515/1054727896710945437/283',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'TRIPLE STANDARD',
//                'board_id' => 'RO',
//                'price' => '322',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            12 => [
//                'hotel_search_code' => '29515/1054727896710945437/8',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'SINGLE DELUXE',
//                'board_id' => 'RO',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            13 => [
//                'hotel_search_code' => '29515/1054727896710945437/9',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'BB',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            14 => [
//                'hotel_search_code' => '29515/1054727896710945437/100',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'SINGLE DELUXE',
//                'board_id' => 'RO',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            15 => [
//                'hotel_search_code' => '29515/1054727896710945437/101',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'BB',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            16 => [
//                'hotel_search_code' => '29515/1054727896710945437/192',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'SINGLE DELUXE',
//                'board_id' => 'RO',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            17 => [
//                'hotel_search_code' => '29515/1054727896710945437/193',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'BB',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            18 => [
//                'hotel_search_code' => '29515/1054727896710945437/284',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'SINGLE DELUXE',
//                'board_id' => 'RO',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            19 => [
//                'hotel_search_code' => '29515/1054727896710945437/285',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'BB',
//                'price' => '403',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            20 => [
//                'hotel_search_code' => '29515/1054727896710945437/10',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'DOUBLE SINGLE USE DELUXE',
//                'board_id' => 'RO',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            21 => [
//                'hotel_search_code' => '29515/1054727896710945437/11',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'BB',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            22 => [
//                'hotel_search_code' => '29515/1054727896710945437/102',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'DOUBLE SINGLE USE DELUXE',
//                'board_id' => 'RO',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            23 => [
//                'hotel_search_code' => '29515/1054727896710945437/103',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'BB',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            24 => [
//                'hotel_search_code' => '29515/1054727896710945437/194',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'DOUBLE SINGLE USE DELUXE',
//                'board_id' => 'RO',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            25 => [
//                'hotel_search_code' => '29515/1054727896710945437/195',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'BB',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            26 => [
//                'hotel_search_code' => '29515/1054727896710945437/286',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'DOUBLE SINGLE USE DELUXE',
//                'board_id' => 'RO',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            27 => [
//                'hotel_search_code' => '29515/1054727896710945437/287',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin STANDARD',
//                'board_id' => 'BB',
//                'price' => '483',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            28 => [
//                'hotel_search_code' => '29515/1054727896710945437/12',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'HB',
//                'price' => '523',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//            29 => [
//                'hotel_search_code' => '29515/1054727896710945437/104',
//                'date_cancel' => '2018-03-07',
//                'room_description' => 'Double or Twin DELUXE',
//                'board_id' => 'HB',
//                'price' => '523',
//                'currency_id' => 'EUR',
//                'comment' => 'NO AMENDMENTS POSSIBLE. IF NEEDED, PLEASE CANCEL AND REBOOK!!, PLEASE NOTE THAT A SEPARATE CITY TAX OF 2 EUR PER PERSON PER NIGHT WILL BE CHARGED AT THE HOTEL UPON ARRIVAL.',
//                'special_offer' => null,
//            ],
//        ],
//    ],
//];

$rooms = [];
foreach ($response as $item) {
    foreach ($item['rooms'] as $room) {
        $key = sha1(sprintf('%s-%s-%s-%s-%s', $room['date_cancel'], $room['board_id'], $room['room_description'], $room['price'], $room['currency_id']));
        $rooms[$key] = $room;
    }
}

$checkoutDate = Carbon::createFromFormat('Y-m-d', $departureDate)->addDays($days)->format('d.m.Y');
$period = sprintf('%s - %s', $startDateCarbon->format('d.m'), $checkoutDate);

foreach ($rooms as $room) {
    //cancel_date = 2018-03-07
    //daca cancel_date - today < 2 = nerambursabil

    $cancelDate = Carbon::createFromFormat('Y-m-d', $room['date_cancel'])->addDays(2);
    $cancelDateString = $cancelDate->isFuture() ? sprintf('Anulare gratuita pana la %s', $cancelDate->format('d.m.Y')) : 'nerambursabil';
    $booking = [
        'denumire_camera' => schimba_caractere($room['room_description']),
        'id_camera' => 1,
        'moneda' => $room['currency_id'],
        'pret' => $room['price'],
        'PackageVariantId' => $room['hotel_search_code'],
        'nr_adulti' => $adults,
        'nr_copii' => $children,
        'copil1' => $child1,
        'copil2' => $child2,
        'copil3' => $child3,
        'id_pret' => 0,
        'plecare' => 1,
        'comision' => 0,
        'id_pachet' => '',
        'oferta' => '',
        'oferta_pret' => '',
        'data_early_booking' => '',
        'comision_reducere' => '',
        'masa' => $room['board_id'],
        'tip_serviciu' => null,
        'id_masa' => $room['board_id'],
        'masa_neschimbata' => $room['board_id'],
        'series_name' => $cancelDateString, // daca anularea e cu mai putin de 2 zile inainte - nerambursabil
        'period' => $period, //'08.06 - 15.06.2018', // nr de zile plus data de start
        'check_in' => $startDateCarbon->format('d.m.Y'), //calcul date de intrare
        'check_out' => $checkoutDate, // calcul data de iesire
        'data_start' => $startDateCarbon->format('d.m.Y'), //data de check_in
        'disponibilitate' => 'disponibil',
        'nr_nights' => $days - 1,
        'RoomCode' => null,
        'id_furnizor' => $id_furnizor,
        'furnizor_comision_procent' => 0.980,
        'id_oferta' => $id_oferta,
        'oferta_comision_procent' => $detalii_online['discount_tarif'][$id_oferta]
    ];

    $rezcam[] = $booking;
}

//d($rezcam);
//d($rooms);