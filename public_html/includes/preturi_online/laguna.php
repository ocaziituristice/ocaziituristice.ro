<?php
$arivdata = date('Y-m-d', strtotime($plecdata.' + '.$pleczile.' days'));

$trip_Room_Chd_Age = '';
for($i=0; $i<$copii; $i++) {
	$trip_Room_Chd_Age .= '<Age>'.$copil_age[$i].'</Age>';
}

if($detalii['moneda']=='EURO') $trip_currency = 'EUR';
	else $trip_currency = $detalii['moneda'];

if($detalii['cazare']=='nu') {

$sel_plecare = "SELECT
localitati.denumire AS loc_plecare,
localitati.id_localitate AS idloc_plecare
FROM oferte_transport_avion
LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
WHERE oferte_transport_avion.id_oferta = '".$id_oferta."'
AND oferte_transport_avion.tip = 'dus'
ORDER BY oferte_transport_avion.ordonare
LIMIT 0,1 ";
$que_plecare = mysql_query($sel_plecare) or die(mysql_error());
$row_plecare = mysql_fetch_array($que_plecare);

$sel_request = "SELECT
import_hoteluri.nume_hotel,
import_hoteluri.stele_hotel,
preturi_laguna.hotel_destCode
FROM import_hoteluri
Inner Join preturi_laguna ON preturi_laguna.hotel_code = import_hoteluri.id_hotel
WHERE import_hoteluri.id_hotel_corespondent = '".$id_hotel."'
AND import_hoteluri.id_furnizor = '".$id_furnizor."'
AND preturi_laguna.period = '".$pleczile."'
AND preturi_laguna.`date` = '".$plecdata."'
AND preturi_laguna.price_currency = '".$trip_currency."'
AND preturi_laguna.trans_code <> ''
LIMIT 0,1 ";
$que_request = mysql_query($sel_request) or die(mysql_error());
$row_request = mysql_fetch_array($que_request);

$trip_CountryCode = $detalii_hotel['country_code'];
$trip_CityCode = $row_request['hotel_destCode'];
$trip_DepCityCode = $depCityCode[$id_furnizor][$row_plecare['idloc_plecare']];
if($detalii['transport']=='Avion') $trip_Transport = 'plane';
$trip_ProductName = $row_request['nume_hotel'];
$trip_ProductCategory = $row_request['stele_hotel'];
$trip_CurrencyCode = $trip_currency;


$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getPackageNVPriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>ocaziituristice</RequestUser>
    <RequestPass>turism5715</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
    <RequestLang>EN</RequestLang>
  </AuditInfo>
  <RequestDetails>
    <getPackageNVPriceRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
      <DepCountryCode>RO</DepCountryCode>
      <DepCityCode>'.$trip_DepCityCode.'</DepCityCode>
      <Transport>'.$trip_Transport.'</Transport>
      <TourOpCode>LA</TourOpCode>
      <ProductName>'.$trip_ProductName.'</ProductName>
      <ProductCategory>'.$trip_ProductCategory.'</ProductCategory>
      <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
      <Language>RO</Language>
      <OfferType>TOATE</OfferType>
      <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
      <Days>1</Days>
      <Rooms>
        <Room Code="DB" NoAdults="'.$adulti.'" NoChildren="'.$copii.'">
          <Children>
            '.$trip_Room_Chd_Age.'
          </Children>
        </Room>
      </Rooms>
    </getPackageNVPriceRequest>
  </RequestDetails>
</Request>';

$sUrl = 'http://rezervari.infosejur.ro/server_xml/server.php';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);
//if(!$err_logare_admin) echo $sResponse;

$prices = new SimpleXMLElement($sResponse);
//if(!$err_logare_admin) { echo '<pre>';print_r($prices);echo '</pre>'; }

if($prices->ResponseDetails->getPackageNVPriceResponse != '') {
	if(count($rezcam)>0) $J = count($rezcam)+1; else $J=0;
	
	foreach($prices->ResponseDetails->getPackageNVPriceResponse->Hotel->Offers->Offer as $key => $value) {
		$den_camera[$key] = (string) $value->BookingRoomTypes->Room;
	
		$sel_roomtype = "SELECT tip_camera_corespondent.id_camera_corespondent, tip_camera.denumire
		FROM tip_camera_corespondent
		INNER JOIN tip_camera ON tip_camera.id_camera = tip_camera_corespondent.id_camera_corespondent
		WHERE tip_camera_corespondent.den_camera = '".$den_camera[$key]."' ";
		$que_roomtype = mysql_query($sel_roomtype) or die(mysql_error());
		$row_roomtype = mysql_fetch_array($que_roomtype);
	
		$rezcam[$J]['id_pret'] = 0;
		$rezcam[$J]['plecare'] = '1';
		$rezcam[$J]['id_camera'] = $row_roomtype['id_camera_corespondent'];
		if($row_roomtype['denumire']) $rezcam[$J]['denumire_camera'] = $row_roomtype['denumire']; else $rezcam[$J]['denumire_camera'] = schimba_caractere($den_camera[$key]);
		$rezcam[$J]['pret'] = (string) $value->Gross;
		$rezcam[$J]['comision'] = (string) $value->CommissionCed;
		$rezcam[$J]['moneda'] = $detalii['moneda'];
		$rezcam[$J]['nr_adulti'] = $adulti;
		$rezcam[$J]['nr_copii'] = $copii;
		$rezcam[$J]['copil1'] = $copil_age[0];
		$rezcam[$J]['copil2'] = $copil_age[1];
		$rezcam[$J]['copil3'] = $copil_age[2];
		if(isset($value->OfferDescription) and ((string) $value->Gross != (string) $value->PriceNoRedd)) {
			$rezcam[$J]['oferta'] = schimba_caractere((string) $value->OfferDescription);
			$rezcam[$J]['oferta_pret'] = (string) $value->PriceNoRedd;
		} else {
			$rezcam[$J]['oferta'] = '';
			$rezcam[$J]['oferta_pret'] = '';
		}
		if(sizeof($value->Meals->Meal)>0) {
			foreach($value->Meals->Meal as $k_meal => $v_meal) {
				$rezcam[$J]['masa'] .= schimba_caractere(change_meal((string) $v_meal)).', ';
			}
			$rezcam[$J]['masa'] = substr($rezcam[$J]['masa'],0,-2);
		} else {
			$rezcam[$J]['masa'] .= 'Fără masă';
		}
		//if(((string) $value->Meals->Meal)=='') $rezcam[$J]['masa'] = 'Fara masa'; else $rezcam[$J]['masa'] = (string) $value->Meals->Meal;
		$rezcam[$J]['series_name'] = '';
		$rezcam[$J]['period'] = date("d.m",strtotime((string) $value->PeriodOfStay->CheckIn)).' - '.date("d.m.Y",strtotime((string) $value->PeriodOfStay->CheckOut));
		$rezcam[$J]['data_start'] = date("Y-m-d",strtotime((string) $value->PeriodOfStay->CheckIn));
		if((string) $value->Availability['Code'] == 'IM') $rezcam[$J]['disponibilitate'] = 'disponibil';
			elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'request';
			elseif((string) $value->Availability['Code'] == 'ST') $rezcam[$J]['disponibilitate'] = 'stopsales';
		$rezcam[$J]['nr_nights'] = dateDiff((string) $value->PeriodOfStay->CheckOut, (string) $value->PeriodOfStay->CheckIn)-1;
		
		foreach($value->PriceDetails->Services->Service as $k_service => $v_service) {
			if((string) $v_service->Type == '7' and (string) $v_service->Transport == 'plane') {
				if((string) $v_service->Availability['Code']=='IM') $flight[$id_oferta]['flight_bookable'] = 1; else $flight[$id_oferta]['flight_bookable'] = 0;
				$avion[]['seats'] = $v_service->Seats;
			}
		}
		
		if($flight[$id_oferta]['flight_bookable']==0) $rezcam[$J]['disponibilitate'] = 'stopsales';
		
		if(sizeof($avion)>0) {
			$sel_plecare = "SELECT * FROM preturi_autoupdate WHERE id_oferta = '".$id_oferta."' ";
			$que_plecare = mysql_query($sel_plecare) or die(mysql_error());
			$row_plecare = mysql_fetch_array($que_plecare);
			
			$sel_flight = "SELECT * FROM preturi_laguna WHERE hotel_code = '".$row_plecare['hotel_code']."' AND date = '".$rezcam[$J]['data_start']."' AND period = '".$row_plecare['perioada']."' AND trans_dep_city = '".$row_plecare['plecare']."' ";
			$que_flight = mysql_query($sel_flight) or die(mysql_error());
			$row_flight = mysql_fetch_array($que_flight);
			
			$cursa = explode(" ", $row_flight['trans_code']);
			
			if(isset($cursa[1])) {
				$flight[$id_oferta]['plecare'][0][0]['from'] = $row_flight['trans_dep_city'];
				$flight[$id_oferta]['plecare'][0][0]['to'] = $row_flight['trans_des_city'];
				$flight[$id_oferta]['plecare'][0][0]['data_plecare'] = $row_flight['trans_dep_depDate'];
				$flight[$id_oferta]['plecare'][0][0]['data_sosire'] = $row_flight['trans_dep_arrDate'];
				$flight[$id_oferta]['plecare'][0][0]['companie'] = $row_flight['trans_airlineCode'];
				$flight[$id_oferta]['plecare'][0][0]['nr_cursa'] = (string) $cursa[1];
				$flight[$id_oferta]['plecare'][0][0]['seats'] = (string) $avion[0]['seats'];
				
				$flight[$id_oferta]['intoarcere'][1][0]['from'] = $row_flight['trans_des_city'];
				$flight[$id_oferta]['intoarcere'][1][0]['to'] = $row_flight['trans_dep_city'];
				$flight[$id_oferta]['intoarcere'][1][0]['data_plecare'] = $row_flight['trans_des_depDate'];
				$flight[$id_oferta]['intoarcere'][1][0]['data_sosire'] = $row_flight['trans_des_arrDate'];
				$flight[$id_oferta]['intoarcere'][1][0]['companie'] = $row_flight['trans_airlineCode'];
				$flight[$id_oferta]['intoarcere'][1][0]['nr_cursa'] = (string) $cursa[1];
				$flight[$id_oferta]['intoarcere'][1][0]['seats'] = (string) $avion[1]['seats'];
			}
		}
		
		$rezcam[$J]['id_furnizor'] = $id_furnizor;
		$rezcam[$J]['furnizor_comision_procent'] = $furnizor['comision_procent'];

		$rezcam[$J]['id_oferta'] = $id_oferta;
		
		$J++;
	}
}

} //end if($detalii['cazare']=='nu')
else if($detalii['cazare']=='da') {

$sel_request = "SELECT
import_hoteluri.*,
localitati_corespondent.*,
import_hoteluri.nume_hotel,
import_hoteluri.stele_hotel,
localitati_corespondent.code_furnizor AS hotel_destCode
FROM import_hoteluri
INNER JOIN hoteluri ON hoteluri.id_hotel = import_hoteluri.id_hotel_corespondent
INNER JOIN localitati_corespondent ON localitati_corespondent.id_corespondent = hoteluri.locatie_id
WHERE import_hoteluri.id_hotel_corespondent = '".$id_hotel."'
AND import_hoteluri.id_furnizor = '".$id_furnizor."'
AND localitati_corespondent.id_furnizor = '".$id_furnizor."'
LIMIT 0,1 ";
$que_request = mysql_query($sel_request) or die(mysql_error());
$row_request = mysql_fetch_array($que_request);

$trip_CountryCode = $detalii_hotel['country_code'];
$trip_CityCode = $row_request['hotel_destCode'];
$trip_ProductName = $row_request['nume_hotel'];
$trip_ProductCategory = $row_request['stele_hotel'];
$trip_CurrencyCode = $trip_currency;


$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelPriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
    <RequestUser>ocaziituristice</RequestUser>
    <RequestPass>turism5715</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
  </AuditInfo>
  <RequestDetails>
    <getHotelPriceRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
      <TourOpCode>LA</TourOpCode>
      <ProductName>'.$trip_ProductName.'</ProductName>
      <ProductCategory>'.$trip_ProductCategory.'</ProductCategory>
      <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
      <Language>RO</Language>
      <OfferType>TOATE</OfferType>
      <PeriodOfStay>
        <CheckIn>'.$plecdata.'</CheckIn>
        <CheckOut>'.$arivdata.'</CheckOut>
      </PeriodOfStay>
      <Rooms>
        <Room Code="DB" NoAdults="'.$adulti.'" NoChildren="'.$copii.'">
          <Children>
            '.$trip_Room_Chd_Age.'
          </Children>
        </Room>
      </Rooms>
    </getHotelPriceRequest>
  </RequestDetails>
</Request>';

$sUrl = 'http://rezervari.infosejur.ro/server_xml/server.php';
$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sXml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$sResponse = curl_exec($ch);

//if(!$err_logare_admin) echo $sXml;
//if(!$err_logare_admin) echo $sResponse;


$prices = new SimpleXMLElement($sResponse);
//if(!$err_logare_admin) { echo '<pre>';print_r($prices);echo '</pre>'; }
if($prices->ResponseDetails->getHotelPriceResponse != '') {
	if(count($rezcam)>0) $J = count($rezcam)+1; else $J=0;
	foreach($prices->ResponseDetails->getHotelPriceResponse->Hotel->Offers->Offer as $key => $value) {
		$den_camera[$key] = (string) $value->BookingRoomTypes->Room;
		$checkIn[$key] = (string) $value->PeriodOfStay->CheckIn;
		$checkOut[$key] = (string) $value->PeriodOfStay->CheckOut;
		$numar_de_nopti[$key] = dateDiff($checkOut[$key], $checkIn[$key])-1;
		//if($numar_de_nopti[$key]==$pleczile) {
		//if((string) $value->Availability['Code'] == 'IM') {
			$sel_roomtype = "SELECT tip_camera_corespondent.id_camera_corespondent, tip_camera.denumire
			FROM tip_camera_corespondent
			INNER JOIN tip_camera ON tip_camera.id_camera = tip_camera_corespondent.id_camera_corespondent
			WHERE tip_camera_corespondent.den_camera = '".$den_camera[$key]."' ";
			$que_roomtype = mysql_query($sel_roomtype) or die(mysql_error());
			$row_roomtype = mysql_fetch_array($que_roomtype);
		
			$rezcam[$J]['id_pret'] = 0;
			$rezcam[$J]['plecare'] = '1';
			$rezcam[$J]['id_camera'] = $row_roomtype['id_camera_corespondent'];
			if($row_roomtype['denumire']) $rezcam[$J]['denumire_camera'] = $row_roomtype['denumire']; else $rezcam[$J]['denumire_camera'] = schimba_caractere($den_camera[$key]);
			$rezcam[$J]['pret'] = round((string) $value->Gross);
			$rezcam[$J]['comision'] = (string) $value->CommissionCed;
			$rezcam[$J]['moneda'] = $detalii['moneda'];
			$rezcam[$J]['nr_adulti'] = $adulti;
			$rezcam[$J]['nr_copii'] = $copii;
			$rezcam[$J]['copil1'] = $copil_age[0];
			$rezcam[$J]['copil2'] = $copil_age[1];
			$rezcam[$J]['copil3'] = $copil_age[2];
			if(isset($value->OfferDescription) and ((string) $value->Gross != (string) $value->PriceNoRedd)) {
				$rezcam[$J]['oferta'] = schimba_caractere((string) $value->OfferDescription);
				$rezcam[$J]['oferta_pret'] = round((string) $value->PriceNoRedd);
			} else {
				$rezcam[$J]['oferta'] = '';
				$rezcam[$J]['oferta_pret'] = '';
			}
			$rezcam[$J]['masa'] = '';
			if(sizeof($value->Meals->Meal)>0) {
				foreach($value->Meals->Meal as $k_meal => $v_meal) {
					$rezcam[$J]['masa'] .= schimba_caractere(change_meal((string) $v_meal)).', ';
				}
				$rezcam[$J]['masa'] = substr($rezcam[$J]['masa'],0,-2);
			} else {
				$rezcam[$J]['masa'] .= 'Fără masă';
			}
			if(((string) $value->Meals->Meal)=='') $rezcam[$J]['masa'] = 'Fara masa'; else $rezcam[$J]['masa'] = (string) $value->Meals->Meal;
			$rezcam[$J]['series_name'] = schimba_caractere((string) $value->SeriesName);
			$rezcam[$J]['data_early_booking']=extrage_data($rezcam[$J]['oferta']);
 $rezcam[$J]['comision_reducere']=extrage_procent( $rezcam[$J]['oferta']." ".$rezcam[$J]['grila_name']); 
			$rezcam[$J]['grila_name'] = schimba_caractere((string) $value->GrilaName);
			$rezcam[$J]['period'] = date("d.m",strtotime($checkIn[$key])).' - '.date("d.m.Y",strtotime($checkOut[$key]));
			$rezcam[$J]['data_start'] = date("Y-m-d",strtotime($checkIn[$key]));
			if((string) $value->Availability['Code'] == 'IM') $rezcam[$J]['disponibilitate'] = 'disponibil';
				elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'request';
				//elseif((string) $value->Availability['Code'] == 'OR') $rezcam[$J]['disponibilitate'] = 'disponibil';
				elseif((string) $value->Availability['Code'] == 'ST') $rezcam[$J]['disponibilitate'] = 'stopsales';
			$rezcam[$J]['nr_nights'] = $numar_de_nopti[$key];
			
			$rezcam[$J]['id_furnizor'] = $id_furnizor;
			$rezcam[$J]['furnizor_comision_procent'] = $furnizor['comision_procent'];

			$rezcam[$J]['id_oferta'] = $id_oferta;
			$rezcam[$J]['oferta_comision_procent'] =$detalii_online['discount_tarif'][$rezcam[$J]['id_oferta']];
			
			$J++;
		//}
	}
}

} //end if($detalii['cazare']=='da')

$one_time_only = 'da';
?>