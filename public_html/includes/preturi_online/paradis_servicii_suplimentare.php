<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);

?>

<?php $servicii_supliemtare_Xml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelServiceTypesRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId> 
     <RequestUser>'.$RequestUser.'</RequestUser>
    <RequestPass>'.$RequestPass.'</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
  </AuditInfo>
  <RequestDetails>
    <getHotelServiceTypesRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
       <TourOpCode>'.$TourOpCode.'</TourOpCode>
      <ProductCode>'.$trip_HotelCode.' </ProductCode>
      <VariantID></VariantID>    
      <PeriodOfStay>
        <CheckIn>2018-05-15</CheckIn>
        <CheckOut>2018-09-30</CheckOut>
      </PeriodOfStay>
  
    </getHotelServiceTypesRequest>
  </RequestDetails>
</Request>';
//if(!$err_logare_admin) echo "pppp". $servicii_supliemtare_Xml ;

$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $servicii_supliemtare_Xml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

$sResponse_servicii = curl_exec($ch);
$servicii_suplimetareXML = new SimpleXMLElement($sResponse_servicii);

//if(!$err_logare_admin) echo $servicii_suplimetareXML->ResponseDetails->getHotelServiceTypesResponse->Services->asXML();

$ss=0;
if($servicii_suplimetareXML->ResponseDetails->getHotelServiceTypesResponse != '') {
	foreach($servicii_suplimetareXML->ResponseDetails->getHotelServiceTypesResponse->Services->Service as $servicii_suplimentare_xml) {
		//echo (string)$servicii_suplimentare_xml->Service->HasPrice;
		if((string)$servicii_suplimentare_xml->HasPrice=='true') {
		//if($selectat['0']['pat_suplimentar']==1 and (string)$servicii_suplimentare_xml->Code!=$cod_pat_suplimentar )
		if($selectat['0']['pat_suplimentar']==1 and (string)$servicii_suplimentare_xml->Code!=$cod_pat_suplimentar and  (string)$servicii_suplimentare_xml->Type=='2')	
			{
		$servicii_suplimentare[$ss]['id_serviciu']=(string)$servicii_suplimentare_xml->Code;	
		$servicii_suplimentare[$ss]['tip']=(string)$servicii_suplimentare_xml->Type;
		$servicii_suplimentare[$ss]['tip_denumire']=(string)$servicii_suplimentare_xml->TypeName;
		$servicii_suplimentare[$ss]['denumire']=(string)$servicii_suplimentare_xml->Name;
		++$ss;
		}
		
		if($selectat['0']['pat_suplimentar']!=1)	{
		$servicii_suplimentare[$ss]['id_serviciu']=(string)$servicii_suplimentare_xml->Code;	
		$servicii_suplimentare[$ss]['tip']=(string)$servicii_suplimentare_xml->Type;
		$servicii_suplimentare[$ss]['tip_denumire']=(string)$servicii_suplimentare_xml->TypeName;
		$servicii_suplimentare[$ss]['denumire']=(string)$servicii_suplimentare_xml->Name;
		++$ss;
		}
		
		
		}
	}
	}
if(!$err_logare_admin) { echo '<pre>';print_r($servicii_suplimentare);echo '</pre>'; }

$servicii_suplimentare_masa = array_filter($servicii_suplimentare, function ($var) {
    return ($var['tip'] == '2');
});

//if(!$err_logare_admin) { echo '<pre>';print_r($servicii_suplimentare_masa);echo '</pre>'; }

for($i=0; $i<$adulti; $i++) {
	$PaxName .= '<PaxName PaxType="adult">Adult '.$i.'</PaxName>';
}
for($i=0; $i<$copii; $i++) {
	$PaxName .= '<PaxName PaxType="child" ChildAge=\"'.$copil_age[$i].'\">Copil '.$i.'</PaxName>';
}

echo 	$PaxName;
foreach($servicii_suplimentare_masa as $servicii_suplimentare_masa_value) {

$servicii_suplimentare_pret_Xml = '<?xml version="1.0" encoding="UTF-8"?>
<Request RequestType="getHotelServicePriceRequest">
  <AuditInfo>
    <RequestId>'.date("dmYHis").'</RequestId>
      <RequestUser>'.$RequestUser.'</RequestUser>
    <RequestPass>'.$RequestPass.'</RequestPass>
    <RequestTime>'.date("Y-m-d").'T'.date("H-i-s").'</RequestTime>
        <RequestLang>RO</RequestLang>
  </AuditInfo>
  <RequestDetails>
    <getHotelServicePriceRequest>
      <CountryCode>'.$trip_CountryCode.'</CountryCode>
      <CityCode>'.$trip_CityCode.'</CityCode>
      <TourOpCode>'.$TourOpCode.'</TourOpCode>
      <ProductCode>'.$product_code.'</ProductCode>
       <CurrencyCode>'.$trip_CurrencyCode.'</CurrencyCode>
      <VariantID>'.$rezcam[$J]['PackageVariantId'].'</VariantID>  
      <Language>RO</Language>
<Services>      
<Service>
          <ServiceType>'.$servicii_suplimentare_masa_value['tip'].'</ServiceType>
          <ServiceCode>'.$servicii_suplimentare_masa_value['id_serviciu'].'</ServiceCode>

          
          
          <PeriodOfStay>
      <CheckIn>'.date('Y-m-d',$rezcam[$J]['data_start']) .'</CheckIn>
        <CheckOut>'. date('Y-m-d', strtotime($rezcam[$J]['data_start'].' + '.$rezcam[$J]['nr_nights'].' days')).'</CheckOut>
      </PeriodOfStay>
      <PaxNames>
                  
				'.$PaxName. ' 
                				  
     </PaxNames>
       </Service>
</Services>
  
  </getHotelServicePriceRequest>
</RequestDetails>
</Request>';
echo "cerere servicii pret <br/>". $servicii_suplimentare_pret_Xml ;


$ch = curl_init($sUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$servicii_suplimentare_pret_Xml);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

$sResponse_servicii_pret = curl_exec($ch);
$servicii_suplimetareXML_pret = new SimpleXMLElement($sResponse_servicii_pret);
//echo $servicii_suplimetareXML_pret->asXML();

if($servicii_suplimetareXML_pret->ResponseDetails->getHotelServicePriceResponse != '') {

foreach($servicii_suplimetareXML_pret->ResponseDetails->getHotelServicePriceResponse->Services->Service as $servicii_suplimentare_xml){
	if($servicii_suplimentare_xml->Price>0) {
	

	if(!(in_array((string)$servicii_suplimentare_xml->Name, $array_masa))) {	
	for($a=1;$a<=$adulti;$a++) {	
	$extra_servicii_pret[$x]['denumire']=change_meal((string)$servicii_suplimentare_xml->Name);
	$extra_servicii_pret[$x]['pret']=(string)$servicii_suplimentare_xml->Gross;
	$extra_servicii_pret[$x]['tip_persoana']="Pentru 1 ADULT";
	$extra_servicii_pret[$x]['id']=(string)$servicii_suplimentare_xml->Code;
	$x++;
	}
	}
	}
}
}
}

if(!$err_logare_admin) { echo '<pre>';print_r($extra_servicii_pret);echo '</pre>'; }








?>