<h1 class="blue">Partii Ski in Austria</h1>
<?php
$partie = desfa_link($_REQUEST['partie']);

$sel_partii = "SELECT
partii.id,
partii.denumire,
partii.id_localitate,
partii.descriere,
partii.alt_min,
partii.alt_max,
partii.transport_pe_cablu,
partii.km_partie_incepatori,
partii.km_partie_avansati,
partii.km_partie_experimentati,
partii.km_partie_total,
partii.sezon,
partii.img_harta,
localitati.denumire AS localitatenume,
zone.denumire AS zonenume,
tari.denumire AS taranume
FROM
partii
LEFT JOIN localitati ON partii.id_localitate = localitati.id_localitate
LEFT JOIN zone ON localitati.id_zona = zone.id_zona
LEFT JOIN tari ON zone.id_tara = tari.id_tara
ORDER BY partii.denumire ASC";
$rez_partii = mysql_query($sel_partii) or die(mysql_error());

while ($row_partii = mysql_fetch_array($rez_partii)) {
?>
<div class="chnPartii">
  <div class="titlu"><a href="<?php echo $path; ?>/partii-austria/<?php echo fa_link($row_partii['denumire']); ?>.html" title="Partia <?php echo $row_partii['denumire']; ?>"><?php echo $row_partii['denumire']; ?></a></div>
  <img src="<?php echo $imgpath; ?>/icon_partie.jpg" alt="" width="60" height="48" style="float:left; margin-right:5px;" />
  <?php echo $row_partii['zonenume']; ?> / <?php echo $row_partii['localitatenume']; ?><br />
  Sezon: <span class="red bold"><?php echo $row_partii['sezon']; ?></span>
</div>
<?php } ?>

<br class="clear" /><br />
