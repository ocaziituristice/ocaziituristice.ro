<?php
/* vezi pagina root; am avut nevoie pentru titlu si text seo si sa nu fac 2 queryuri */
?>

<div id="detaliiOferta">
  <h1 class="blue">Partie ski <?php echo $detalii['denumire']; ?></h1>
  
  <div style="float:left; width:250px; padding:0 10px;">
    <h2 class="blue" style="font-size:15px;"><?php echo $detalii['alt_min']; ?>m - <?php echo $detalii['alt_max']; ?>m</h2>
    <?php echo $detalii['taranume']; ?> / <?php echo $detalii['localitatenume']; ?>
    
    <br class="clear" /><br />
    
    <div class="underline blue" style="margin:5px 0;">
      <img src="<?php echo $imgpath; ?>/spacer.gif" alt="" width="10" height="5" style="background:#065ABF; margin:0 5px 2px 0;" />
      <strong>Transport pe cablu</strong>
    </div>
    <?php echo nl2br($detalii['transport_pe_cablu']); ?>	

    <br class="clear" /><br />
    
    <div class="underline blue" style="margin:5px 0;">
      <img src="<?php echo $imgpath; ?>/spacer.gif" alt="" width="10" height="5" style="background:#065ABF; margin:0 5px 2px 0;" />
      <strong>Lungimi partii</strong>
    </div>
    
    <table cellpadding="2" cellspacing="0" border="0" width="190">
      <tr>
        <td width="25" align="left" valign="middle"><img src="<?php echo $imgpath; ?>/dificultate_partie_i.gif" alt="incepatori" width="18" height="18" /></td>
        <td align="left" valign="middle">Incepatori</td>
        <td width="60" align="right" valign="middle"><?php echo $detalii['km_partie_incepatori']; ?></td>
      </tr>
      <tr>
        <td align="left" valign="middle"><img src="<?php echo $imgpath; ?>/dificultate_partie_a.gif" alt="avansati" width="18" height="18" /></td>
        <td align="left" valign="middle">Avansati</td>
        <td align="right" valign="middle"><?php echo $detalii['km_partie_avansati']; ?></td>
      </tr>
      <tr>
        <td align="left" valign="middle"><img src="<?php echo $imgpath; ?>/dificultate_partie_e.gif" alt="experimentati" width="18" height="18" /></td>
        <td align="left" valign="middle">Experimentati</td>
        <td align="right" valign="middle"><?php echo $detalii['km_partie_experimentati']; ?></td>
      </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle" style="font-weight:bold;">Total lungime</td>
        <td align="right" valign="middle"><?php echo $detalii['km_partie_total']; ?></td>
      </tr>
    </table>

    <br class="clear" /><br />
    
    <div class="underline blue" style="margin:5px 0;">
      <img src="<?php echo $imgpath; ?>/spacer.gif" alt="" width="10" height="5" style="background:#065ABF; margin:0 5px 2px 0;" />
      <strong>Sezon</strong>
    </div>
    
    Partia este deschisa in perioada:<br />
    <span class="blue" style="font-weight:bold"><?php echo $detalii['sezon']; ?></span>

  </div>
  
  <div id="gallery" class="gallery">
	<a href="<?php echo $imgpath; ?>/partii_ski/mari/<?php echo $detalii['img_harta']; ?>" title="Vezi partia <?php echo $detalii['denumire']; ?> la dimensiuni mari"><img src="<?php echo $imgpath; ?>/partii_ski/mici/<?php echo $detalii['img_harta']; ?>" alt="<?php echo $detalii['denumire']; ?>" width="350" height="300" class="picture" /></a>
  </div>
  
  <br class="clear" /><br />
  
  <div class="descriereOferta">
	<?php echo $detalii['descriere']; ?>
  </div>
  
  <br class="clear" /><br />

</div>