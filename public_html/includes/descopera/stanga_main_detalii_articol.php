<div id="detaliiOferta">
  <div class="clear"><img src="/images/spacer.gif" width="1" height="10" /></div>
  <div class="descriereOferta">
    <?
$sel_art="
	SELECT
	articole.id_articol,
	articole.id_localitate,
	articole.id_tara,
	articole.id_hotel,
	articole.titlu_articol,
	articole.descriere_scurta,
	articole.descriere_articol,
	articole.data_publicare,
	articole.taguri,
	articole.imagine,
	articole.nume_imagine,
	articole.imagine_mare,
	articole.nume_imagine_mare,
	continente.nume_continent,
	tip_articol.denumire AS denumire_categorie,
	tari.denumire AS denumire_tara,
	localitati.denumire AS denumire_localitate,
	zone.denumire as denumire_zona
	FROM
	articole
	Left Join tip_articol ON articole.id_categorie = tip_articol.id_tip
	Left Join localitati ON articole.id_localitate = localitati.id_localitate
	Left join zone on localitati.id_zona = zone.id_zona
	Left Join tari ON articole.id_tara = tari.id_tara
	left join continente on tari.id_continent = continente.id_continent
	WHERE activ='da' AND
	id_articol='".mysql_real_escape_string($_GET['id'])."'
";

$rez=mysql_query($sel_art) or die (mysql_error());
$no=mysql_num_rows($rez);
if($no==0){
	echo "Articolul nu mai este disponibil!!!<br />";
}else{ 
    $row=mysql_fetch_array($rez); ?>
	<h1 class="normal"><?php echo $row['titlu_articol']; ?></h1>
    
    <div id="gallery" class="gallery" style="float:left;">
  <?php if($row['imagine']!=NULL) { ?>
  <a href="http://www.ocaziituristice.ro/img_prima_art/<?php echo $row['imagine']; ?>" title="<?php echo $row['titlu_articol']; ?>"><img src="http://www.ocaziituristice.ro/thumb_art/<?php echo $row['imagine']; ?>" class="imageDescriere" alt="<?php echo $row['nume_imagine']; ?>" title="<?php echo $row['nume_imagine']; ?>" /></a>
  <?php } ?>
</div>
<?php echo nl2p($row['descriere_articol']); ?>
<br class="clear" /><br /> <?php
} @mysql_free_result($rez);


?>
  </div>
  
  <br class="clear" />
  
<?php if($row['id_tara']!="0") { ?>
  <div style="padding:0 20px; font-size:14px;">
	<a href="<?php echo $path; ?>/descopera-<?php echo fa_link($row['denumire_tara']); ?>/" title="Descopera <?php echo $row['denumire_tara']; ?>">Descopera <?php echo $row['denumire_tara']; ?></a> <span style="padding:0 10px;">|</span> 
	<a href="<?php echo $path; ?>/descopera-<?php echo fa_link($row['denumire_tara']); ?>/harta-<?php echo fa_link($row['denumire_tara']); ?>.html" title="Harta <?php echo $row['denumire_tara']; ?>">Harta <?php echo $row['denumire_tara']; ?></a>
  </div>
  
  <br class="clear" /><br />
  
  <h2 class="underline green" style="margin-bottom:12px;">Descopera Oferte in <?php echo $row['denumire_tara']; ?></h2>
  <?php $test_of = " select
count(oferte.id_oferta) as oferte,
sum(if(oferte.id_transport = '1' and oferte.nr_zile <='2',1,0)) as cazari
FROM
oferte
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
inner join localitati on hoteluri.locatie_id = localitati.id_localitate
inner join zone on localitati.id_zona = zone.id_zona
inner join tari on zone.id_tara = tari.id_tara
where oferte.valabila = 'da' and tari.id_tara = '".$row['id_tara']."' ";
$que_test_of = mysql_query($test_of) or die(mysql_error());
$row_test_of = mysql_fetch_array($que_test_of);
@mysql_free_result($que_test_of); 
if($row_test_of['oferte']>'0') { ?>
  <img src="/images/icon_sejururi.gif" alt="Sejururi" style="vertical-align:middle;" /> <a href="/sejur-<?php echo fa_link($row['denumire_tara']); ?>/" title="Oferte <?php echo $row['denumire_tara']; ?>" class="link3"><strong>Oferte <?php echo $row['denumire_tara']; ?></strong></a>
  &nbsp;&nbsp;&nbsp;&nbsp;
<?php }
if($row_test_of['cazari']>'0') { ?>  
  <img src="/images/icon_transporturi.gif" alt="Cazare" style="vertical-align:middle;" /> <a href="/cazare/<?php echo fa_link($row['denumire_tara']); ?>/" title="Cazare <?php echo $row['denumire_tara']; ?>" class="link3"><strong>Cazare <?php echo $row['denumire_tara']; ?></strong></a>
  &nbsp;&nbsp;&nbsp;&nbsp;
<?php } 
 $test_cir = "SELECT
tari.denumire,
continente.nume_continent
FROM
circuite
Inner Join circuite_tari ON circuite.id_circuit = circuite_tari.id_circuit
Inner Join tari ON circuite_tari.id_tara = tari.id_tara
inner join continente on tari.id_continent = continente.id_continent
WHERE
circuite.valabila =  'Da' AND
tari.id_tara =  ".$row['id_tara']."
GROUP BY
tari.denumire
ORDER BY
tari.denumire ASC ";
$qeu_test_cir = mysql_query($test_cir) or die(mysql_error());
if(mysql_num_rows($qeu_test_cir)>'0') { 
$row_cont=mysql_fetch_array($qeu_test_cir); ?>  
  <img src="/images/icon_circuite.gif" alt="Circuite" style="vertical-align:middle;" /> <a href="/circuite/<?php echo fa_link($row_cont['nume_continent']); ?>/<?php echo fa_link($row['denumire_tara']); ?>/" title="Circuit <?php echo $row['denumire_tara']; ?>" class="link3"><strong>Circuit <?php echo $row['denumire_tara']; ?></strong></a>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <?php } @mysql_free_result($qeu_test_cir);
  if($row_test_of['oferte']>'0') { ?>
  <img src="/images/icon_transporturi.gif" alt="Hoteluri" style="vertical-align:middle;" /> <a href="/hoteluri/<?php echo fa_link($row['denumire_tara']); ?>/" title="Hoteluri <?php echo $row['denumire_tara']; ?>" class="link3"><strong>Hoteluri <?php echo $row['denumire_tara']; ?></strong></a>
  <?php } ?>
  <br class="clear" />
 <?php if($row_tara['afisare_tip']=='zona') {
$loc=new TARA();
$loc_sejur=$loc->load_zone_tara($row['id_tara'], '', ''); ?>
  <ul class="columnList4">
   <?php $c=0; 
   foreach($loc_sejur as $key => $value)
    { 
	  if(sizeof($value)>'0') {
      foreach($value as $key1 => $value1)
		{
		  foreach($value1 as $key2 => $value2)
			{ $c++;
			  if($c<='16') {	?>
               <li><a href="/sejur-<? echo fa_link($row['denumire_tara'])."/".fa_link($key2); ?>/"><? echo ucwords($key2); ?></a></li>
     <? } }
	 }
   }
 }?>
  </ul>
 <?php } else {
 $local=new TARI(); $j=0;
 $localitati=$local->oras_by_tara($row['id_tara'], '', '');  ?>
 <ul class="columnList4">
 <?php if(sizeof($localitati)>'0') {
     foreach($localitati as $key1 => $value1)
		{ 
		 foreach($value1 as $key2 => $value2)
		 { $j++; 
		 if($j<='16') { ?>
			 <li><a href="/sejur-<?php echo fa_link($row['denumire_tara']); ?>/<?php echo fa_link($value2); ?>/<?php echo fa_link($key2); ?>/"><? echo ucwords(strtolower($key2)); ?></a></li>
         <? } 
		 }
	  } 
  } ?>
  </ul>    
 <?php } ?> 
  <br class="clear" />
  <div align="right" class="black" style="padding-top:10px;">&raquo; <a href="/sejur-<?php echo fa_link($row['denumire_tara']); ?>/">Vezi toate destinatiile din <?php echo $row['denumire_tara']; ?></a></div>
  
  <br class="clear" /><br />
  
  <?php
  $sel_art = "select id_articol, titlu_articol, descriere_scurta from articole where activ='da' and id_tara = '".$row['id_tara']."' AND id_articol <> ".$row['id_articol']." group by id_articol ORDER BY RAND()";
  $que_art = mysql_query($sel_art) or die(mysql_error()); 
  if(mysql_num_rows($que_art)>'0') { ?>
  <h2 class="underline green">Ce au de spus Turistii si Specialistii despre <?php echo $row['denumire_tara']; ?></h2>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/descopera/carousel_articole.php'); ?>
  
  <br class="clear" /><br />
  <?php } ?>
  
<?php } ?>

</div>
<br class="clear" /><br />
<? 
if($row["denumire_zona"]){
	$zona=array();
	$zona[0]=$row["denumire_zona"];
	$afisare=new AFISARE();
	$afisare->setZone($zona);
	$afisare->setAfisare(1, 6);
	$afisare->setPromovata(0);
	$afisare->initializeaza_pagini($link);
	?>
<h1 class="blue">Oferte <?php echo $row['denumire_zona']; ?> &nbsp;<img src="/images/arrow_pointing_down.gif" style="vertical-align:middle;" /></h1>
<div class="spacer"></div>
<?
    $afisare->afiseaza_scorpy();
    ?>
<div class="spacer"></div>
<?

}elseif($row["denumire_tara"]){
	$tara=array();
	$tara[0]=$row["denumire_tara"];
	$afisare=new AFISARE();
	$afisare->setTari($tara);
	$afisare->setAfisare(1, 6);
	$afisare->setPromovata(0);
	$afisare->initializeaza_pagini($link);
	?>
<h1 class="blue">Oferte <?php echo $row['denumire_tara']; ?> &nbsp;<img src="/images/arrow_pointing_down.gif" style="vertical-align:middle;" /></h1>
<div class="spacer"></div>
<?
    $afisare->afiseaza_scorpy();
    ?>
<div class="spacer"></div>
<? } ?>
