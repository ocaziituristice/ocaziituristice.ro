<div id="NEW-detaliiOferta">
  <h1 class="red"><?php echo $detalii['denumire']; ?></h1>

  <?php /*?><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?><?php */?>
  
  <div class="NEW-adresa"><?php echo $detalii['tara']; ?></div>
    
  <br class="clear" />
  
<div class="NEW-column-left1">

  <div class="NEW-gallery">
  <?php if($detalii['poza1']) { ?>
    <a rel="gallery-single" href="<?php echo $sitepath_parinte; ?>img_prima_excursii/<?php echo $detalii['poza1']; ?>" id="link_poza_principala"><img src="<?php echo $sitepath_parinte; ?>img_prima_excursii/<?php echo $detalii['poza1']; ?>" width="300" height="300" alt="" class="images" id="poza_principala" /></a>
<?php } else { ?>
<a rel="gallery-single" href="<?php echo $sitepath_parinte; ?>images/no_photo.jpg" id="link_poza_principala"><img src="<?php echo $sitepath_parinte; ?>images/no_photo.jpg" width="300" height="300" alt="" class="images" id="poza_principala" /></a>
<?php }
for($i=1;$i<=3;$i++) {
if($detalii['poza'.$i]) { ?>    
    <a href="<?php echo $sitepath_parinte; ?>img_prima_excursii/<?php echo $detalii['poza'.$i]; ?>" onmouseover="document.getElementById('poza_principala').src='<?php echo $sitepath_parinte; ?>img_mediu_excursii/<?php echo $detalii['poza'.$i]; ?>'; document.getElementById('link_poza_principala').href='<?php echo $sitepath_parinte; ?>img_prima_excursii/<?php echo $detalii['poza'.$i]; ?>';return false;" rel="gallery"><img src="<?php echo $sitepath_parinte; ?>thumb_excursii/<?php echo $detalii['poza'.$i]; ?>" width="75" alt="" class="images" /></a>
<?php }
} ?>
  </div>

  <br class="clear" />
  
  <div class="NEW-detalii-oferta">
    <?php afisare_frumos($detalii["descriere"], 'nl2p'); ?>
    <br class="clear" />
  </div>
  <?php if(sizeof($detalii['pret'])>0) { ?>
  <div style="font-size:1.15em;">Tarife disponibile pentru <strong class="blue"><em><?php echo $detalii['denumire']; ?></em></strong> din <strong class="blue"><em><?php echo $detalii['tara']; ?></em></strong></div>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabel-preturi">
      <thead>
      <tr>
       <th align="center" valign="top">Varsta</th>
       <th align="center" valign="top">Tarif</th> 
      </tr>
      </thead>
    <tbody>
    <?php $nr_t=0; foreach($detalii['pret'] as $key_p=>$value_p) {
	if($nr_t%2==1) $class_tr='impar'; else $class_tr='par'; ?>
    <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
    <td align="center" valign="middle"><?php if($detalii['varsta_min'][$key_p] || $detalii['varsta_max'][$key_p]) { echo $detalii['varsta_min'][$key_p]; if($detalii['varsta_max'][$key_p]) echo ' - '.$detalii['varsta_max'][$key_p]; } else echo ' - '; ?></td>
    <td align="center" valign="middle" class="pret vtip last"><?php if($detalii['pret'][$key_p]) { echo $detalii['pret'][$key_p].' '; if($detalii['moneda'][$key_p]=='EURO') { echo '&euro;'; $valuta='da'; } elseif($detalii['moneda'][$key_p]=='USD') { echo '$'; $valuta='da'; } else echo  $detalii['moneda'][$key_p]; } ?></td>
    <?php } ?>
    </tbody>  
  </table>
  <?php } ?>    
</div>
<div class="NEW-column-right1">
  
  <br class="clear" /><br />

  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/new_hotel.php"); ?>  
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta_new.php"); ?>

</div>
<br class="clear" />
</div>
<br class="clear" />
