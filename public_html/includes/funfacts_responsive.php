<?php
?>

<div class="funfacts-section">
    <div class="homepage-boxes layout">
        <h3 class="subtitle">Mai mult decat o agentie de turism. Partenerul tau de incredere in alegerea vacantelor de
            vis!</h3>
    </div>
    <div class="layout">
        <div class="col-md-3 column">
            <div class="fact-wrapper">
                <div class="fact-content">
                    <div class="fact-icon">
                        <i class="fa fa-fast-forward" aria-hidden="true"></i>
                    </div>
                    <div class="fact-text">
                        Informatii in timp real, inca din 2012.
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 column">
            <div class="fact-wrapper">
                <div class="fact-content">
                    <div class="fact-icon">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                    </div>
                    <div class="fact-text">
                        10.000 de clienti, 30 de tour-operatori parteneri.
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 column">
            <div class="fact-wrapper">
                <div class="fact-content">
                    <div class="fact-icon">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                    </div>
                    <div class="fact-text">
                        Licenta de tour-operator si polita de asigurare valabila.
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 column">
            <div class="fact-wrapper">
                <div class="fact-content">
                    <div class="fact-icon">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                    </div>
                    <div class="fact-text">
                        Experienta de peste 6 ani.
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        resizeFactBox();
        window.addEventListener('resize', resizeFactBox);
    });

    function resizeFactBox() {
        if ($(window).width() > 767) {
            $('.fact-wrapper').height($('.fact-wrapper').width());
        }
    }
</script>
