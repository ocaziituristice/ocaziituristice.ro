  <div class="NEW-detalii-oferta">
<?php if($preturi['camera']>0) {
	$early_inclus='nu'; ?>  
 <?php if(sizeof($preturi['data_start'])<=10) { ?>
    <div style="overflow:auto;">
  <h3 class="green">  Tarife Sejur <span class="black"><?php echo $detalii['denumire_scurta']." -  ".$detalii_hotel['denumire']."/".$detalii_hotel['localitate'];?></span></h3>
    <table class="tabel-preturi" style="width:100%;">
      <?php $nr_t=0;
	  foreach($preturi['data_start'] as $key_d=>$data_start) {
		  $nr_t++;
		  if($nr_t%2==1) $class_tr='impar'; else $class_tr='par';
		  $numar_minim_nopti='';
		  if(sizeof($detalii['data_start_nr_min'])>'0') {
			  foreach($detalii['data_start_nr_min'] as $key_nm=>$value_nm) {
				  if($value_nm<=$preturi['data_start_normal'][$key_d]) {
					  if($preturi['data_end_normal'][$key_d] && $preturi['data_end_normal'][$key_d]<>'0000-00-00') {
						  if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_end_normal'][$key_d]) {
							  $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
						  }
					  } else {
						  if($detalii['data_end_nr_min'][$key_nm]>=$preturi['data_start_normal'][$key_d]) {
							  $numar_minim_nopti = '* minim '.$detalii['nr_min'][$key_nm].' nopti';
						  }
					  }
				  }
			  }
		  }
		  $stopsales[$key_d] = 0;
		  /*foreach($preturi['camera'] as $key_cam_ss=>$value_cam_ss) {
			  if(soldout($id_oferta, $key_cam_ss, $data_start)>0) $stopsales[$key_d] += 1;
		  }*/
		  if($stopsales[$key_d]==0) { ?>
	  <?php if($nr_t==1) { ?>
      <thead>
      <tr>
        <th class="text-center" style="width:150px;"><?php if($detalii['tip_preturi']=='plecari') echo 'Data Inceperii sejurului'; else echo 'Perioade'; ?></th>
        <?php /*?><th width="120" align="center" valign="middle">Tip masa</th><?php */?>
        <?php $g=0;
		foreach($preturi['camera'] as $key_cam=>$value_cam) {
			$g++;
			if(sizeof($preturi['camera'])==$g) {
				$class='text-center last';
				$g=0;
			} else $class='text-center'; ?>
        	<th class="<?php echo $class; ?>"><?php echo $value_cam; ?> - <?php echo $detalii['masa']; ?></th>
        <?php } ?>
      </tr>
      </thead>
      <tbody>
	  <?php } ?>
          <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
            <th class="text-left nowrap">
			<?php /*if(sizeof($preturi['include_tip'][$key_d])>0) {
				echo '<div class="text-right icon-tem">';
				foreach($preturi['include_tip'][$key_d] as $key_inc=>$include) echo '<a href="'.$sitepath.$_REQUEST['tara'].'/'.$_REQUEST['localitate'].'/'.fa_link($include['denumire']).'-'.$include['id_oferta'].'.html"><img src="'.$imgpath.'/oferte/icon_tematica_mica_'.$include['id_tip_oferta'].'.png" /></a>';
				echo '</div>';
			}*/
			echo $data_start;
			if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') echo ' - '.$preturi['data_end'][$key_d];
			?><br />
            <?php if(strlen($numar_minim_nopti)>4) echo '<em>'.$numar_minim_nopti.'</em><br />'; ?>
			<?php /*if(sizeof($preturi['secundar'][$preturi['data_start_normal'][$key_d]][$preturi['data_end_normal'][$key_d]])>0) {
				?><div class="red"><a onmousedown="if(document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display == 'none'){ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'block'; }else{ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'none'; }" style="cursor:pointer">toate preturile</a></div>
			<?php }*/ ?>
            </th>
            <?php /*?><td align="center" valign="middle"><?php
			foreach($preturi['camera'] as $key_cam=>$value_cam) {
				echo $preturi['tip_masa'][$key_d][$key_cam];
			}
			?></td><?php */?>
			<?php $g=0;
			foreach($preturi['camera'] as $key_cam=>$value_cam) {
				$g++;
				if(sizeof($preturi['camera'])==$g) {
					$class='text-center pret last';
					$g=0;
				} else $class='text-center pret';
				if(sizeof($preturi['camera'])=="1") $wdt='70%';
				elseif(sizeof($preturi['camera'])=="2") $wdt='35%';
				elseif(sizeof($preturi['camera'])=="3") $wdt='23%';
				elseif(sizeof($preturi['camera'])=="4") $wdt='17%';
				elseif(sizeof($preturi['camera'])=="5") $wdt='14%';
				elseif(sizeof($preturi['camera'])=="6") $wdt='11%';
			?>
            <td  <?php echo 'class="'.$class.'"'; ?> style="width:<?php echo $wdt; ?>">
			<?php if($preturi['pret'][$key_d][$key_cam]) {
				$pret_afisare = new_price($preturi['pret'][$key_d][$key_cam]).' ';
				if($preturi['moneda'][$key_d][$key_cam]=='EURO') {
					$moneda_afisare = '&euro;';
					$valuta='da';
				} elseif($preturi['moneda'][$key_d][$key_cam]=='USD') {
					$moneda_afisare = '$';
					$valuta='da';
				} else $moneda_afisare = $preturi['moneda'][$key_d][$key_cam];
				/*if((soldout($id_oferta, $key_cam, $data_start)>0) and !($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000')) {
					echo '<span class="soldout2 red" title="Aceasta perioada nu este disponibila. Camera se afla in STOP SALES.">STOP SALES<span class="pretz">'.$pret_afisare.$moneda_afisare.'</span></span>';
				} else {
					echo $pret_afisare.$moneda_afisare;
				}*/
				echo $pret_afisare.$moneda_afisare;
			} else echo '-'; ?>
            </td>
			<?php } ?>
          </tr>
          <?php } //else echo '<tr><td class="text-center pad10"><span class="red bold bigger-13em">Ne pare rau, momentan nu sunt preturi disponibile.</span></td></tr>'; ?>
      <?php } ?>
      </tbody>
    </table>
    </div>
    <?php } else { ?>
    <div id="tarife" style="overflow:auto;">&nbsp;</div>
    <?php }
	}
	?>
	
    <div class="grey2 bold" style="text-align:right; padding:0 5px 10px 5px;">* tarifele sunt exprimate in <span class="bigger-13em blue"><?php echo $detalii['moneda']; ?> pe <?php echo $detalii['exprimare_pret']; ?></span></div>