<?php $copii=$_GET['copii'];
      $age[0]=$_GET['age'][0]; 
      $age[1]=$_GET['age'][1];
      $age[2]=$_GET['age'][2]; 
?>
<form name="requestAvailability" action="/request-availability/" method="post" onsubmit="return validateForm();">
  <div class="chenar chn-color-green border-light-grey pad10-0" style="margin:5px 0 -1px 0 !important;">
    <div class="chapter-title red">Cauta cele mai bune preturi pentru <?php echo $den_loc?> folosind cautarea de mai jos</div>
     
    <div class="NEW-search-wide clearfix">
      <div class="field text-left" style="width:118px;">
        <label for="CheckIn">Data Check-in:</label>
      <?php if(isset($_GET['plecdata']) and $_GET['plecdata']!=''){$plecdata=$_GET['plecdata'];$detalii_online_min_data="01.06.2016";$detalii_online_max_data="15.09.205";}?>
        <input type="text" name="plecdata" id="CheckIn" value="<?php if(isset($plecdata) and $plecdata >= $detalii_online_min_data and $plecdata <= $detalii_online_max_data) echo date("d.m.Y", strtotime($plecdata)); ?>" class="NEW-round4px" style="width:106px;">
        
      </div>
      
      <div class="field text-left" style="width:90px;">
        <label for="calculeaza-nr-nopti-general">Nr. nopţi:</label>
        <select name="pleczile" id="calculeaza-nr-nopti-general" class="NEW-round4px">
           <?php for($nrnpt=1; $nrnpt<=15; $nrnpt++) { ?>
          
          <option value="<?php echo $nrnpt; ?>" <?php if($pleczile==$nrnpt) { echo 'selected'; }elseif(($nrnpt==7) and (!isset($pleczile))){echo 'selected'; } ?>><?php echo $nrnpt; ?></option>
          
		  <?php } ?>
        </select>
      </div>
      <div class="field text-left" style="width:90px;">
        <label for="calculeaza-adulti-general">Nr. adulţi:</label>
        <select name="adulti" id="calculeaza-adulti-general" class="NEW-round4px">
		  <?php for($i=1; $i<=3; $i++) { ?>
          <option value="<?php echo $i; ?>" <?php if(!is_integer($adulti) and $i==2) echo 'selected'; elseif($adulti==$i) echo 'selected'; ?>><?php echo $i; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="field text-left" style="width:90px;">
        <label for="calculeaza-copii-general">Nr. copii:</label>
        <select name="copii" id="calculeaza-copii-general" class="NEW-round4px" onchange="displayAges(this.value)">
		  <?php for($i=0; $i<=3; $i++) { ?>
          <option value="<?php echo $i; ?>" <?php if($_GET['copii']==$i) echo 'selected'; ?>><?php echo $i; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="field text-left" style="width:130px;">
        <label for="calculeaza-transport-general">Transport:</label>
        <select name="transport" id="calculeaza-transport-general" class="NEW-round4px" onchange="this.form.submit();return ray.ajax()">
            <option value="Fara transport" selected="selected"> Fara transport</option>
	 

 <?php asort($detalii_online['tip_transport']);
		  foreach($detalii_online['tip_transport'] as $k_tiptrans => $v_tiptrans) { ?>
          <option value="<?php echo $k_tiptrans; if($k_tiptrans==1) echo ';51'; ?>" <?php if($k_tiptrans!=1) echo 'disabled'; ?> 
		  <?php if($k_tiptrans==$tip_transport) echo 'selected'; ?>><?php echo $v_tiptrans; ?></option>
          <?php if($k_tiptrans!=1) {
		  foreach($detalii_online[$v_tiptrans] as $k_orasplec => $v_orasplec) { ?>
          <option value="<?php echo $k_tiptrans.';'.$k_orasplec; ?>" <?php if($k_tiptrans==$tip_transport and $k_orasplec==$orasplec) echo 'selected'; ?>><?php echo $v_tiptrans.'/'.$v_orasplec; ?></option>
		  <?php }
		  } ?>
          <?php } ?>
        </select>
      </div>
      <input type="submit" value="Vezi preț" class="button-green NEW-round6px float-right" style="font-size:16px; letter-spacing:1px; padding:4px 19px; margin:18px 10px 0 0;" >
      
      <br class="clear">
      
      <div class="chd-ages" style="margin:10px 0 0 360px; <?php if($copii<1) echo 'display:none;'; ?>">
		<?php for($t=0; $t<3; $t++) { ?>
        <div class="field text-left varste-copii-<?php echo $t; ?>" style="width:70px; <?php if($copii<($t+1)) echo 'display:none;'; ?>">
          <label for="varste-copii-<?php echo $t; ?>-<?php echo $id_hotel; ?>">Copil <?php echo $t+1; ?></label>
          <select name="age[<?php echo $t; ?>]" id="varste-copii-<?php echo $t; ?>" class="NEW-round4px varste-copii-select-<?php echo $t; ?>">
            <?php for($j1[$t]=1; $j1[$t]<=17; $j1[$t]++) { ?>
            <option value="<?php echo $j1[$t]; ?>" <?php if($age[$t]==$j1[$t] and ($t+1)<=$copii) echo 'selected'; ?>><?php echo $j1[$t]; ?> ani</option>
            <?php } ?>
          </select>
        </div>
        <?php } ?>
      </div>
	  <input name="parentLink" type="hidden" class="parentLink" value="">
	  <input name="prevdata" type="hidden" value="<?php echo $var[21]; ?>">
      <input name="id_hotel" type="hidden" value="<?php echo $id_hotel; ?>">
      <input name="tip_pagina" type="hidden" value="insiruire">
      <input name="search" type="hidden" value="da">
    </div>
  </div>
</form>

<script>
$(".parentLink").val(document.location.href);
//$(".parentLink").val("ocaziituristice.ro");
function displayAges(nVal) {
	if(nVal>0) {
		$(".chd-ages").show();
	} else {
		$(".chd-ages").hide();
	}
	
	for(var i=0; i<3; i++) {
		if(i<nVal) {
			$(".varste-copii-"+i).css("display", "inline-block");
		} else {
			$(".varste-copii-"+i).css("display", "none");
			$(".varste-copii-select-"+i).val(0);
		}
	}
}

	


function validateForm() {
	var date_field = document.getElementById("CheckIn").value;
	if (date_field==null || !date_field) {
		alert("Data plecării trebuie selectată!");
		document.getElementById("CheckIn").focus();	
		return false;
	}
	return ray.ajax();
}
</script>