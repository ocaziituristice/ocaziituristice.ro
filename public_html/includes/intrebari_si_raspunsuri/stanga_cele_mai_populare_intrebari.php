    <div id="detaliiFormulare" align="left">
        <div align="left">
            <?php
                if(!isset($_GET['pas']))
                {
                    ?>
                    	<h1>Intrebari si Raspunsuri</h1>
                        <div class="QAtabs">
                            <a href="<?php echo $path; ?>/intrebari_si_raspunsuri.php?pas=1" title="Propune o intrebare" class="propune"><span style="color:#FFF;">Propune o intrebare</span></a>
                            <a href="<?php $path; ?>/intrebari_si_raspunsuri.php" title="Cele mai recente intrebari" class="off"><span style="color:#777;">Cele mai recente</span></a>
                            <a href="<?php $path; ?>/cele_mai_populare_intrebari.php" title="Cele mai populare intrebari" class="on"><span style="color:#fff;">Cele mai populare</span></a>
                            <br class="clear" />
                        </div>
                        <div style="float:right;"></div>
                        <br class="clear" />
                        <?php
                            $query = "select sectiune_intrebari.ID, sectiune_intrebari.Titlu, CONCAT(sectiune_intrebari.Nume, ' ', sectiune_intrebari.Prenume) as nume, sectiune_intrebari.DataAdaugare from sectiune_intrebari inner join sectiune_raspunsuri on sectiune_intrebari.ID = sectiune_raspunsuri.IntrebareID where sectiune_intrebari.StareID = '1' and sectiune_raspunsuri.StareID = '1' group by sectiune_intrebari.ID order by count(sectiune_intrebari.ID) DESC";
                            $result = mysql_query($query) or die(mysql_error());
                            if(mysql_num_rows($result))
                            {
                                while($row = mysql_fetch_assoc($result))
                                {
                                    $numar_raspunsuri = "0 raspunsuri";
                                    $query2 = "select count(ID) as nr from sectiune_raspunsuri where IntrebareID = '".$row['ID']."' and StareID = '1'";
                                    $result2 = mysql_query($query2) or die(mysql_error());
                                    $row2 = mysql_fetch_assoc($result2);
                                    if($row2['nr'] > 1)
                                    {
                                        $numar_raspunsuri = $row2['nr']." raspunsuri";
                                    }
                                    //
                                    if($row2['nr'] == 1)
                                    {
                                        $numar_raspunsuri = "1 raspuns";
                                    }                                            
                                    //
                                    $tmp_data_adaugare = explode(" ", $row['DataAdaugare']);
                                    $tmp_an = explode("-", $tmp_data_adaugare[0]);
                                    $tmp_ora = explode(":", $tmp_data_adaugare[1]);
                                    //
                                    $epoch_1 = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
                                    $epoch_2 = mktime($tmp_ora[0], $tmp_ora[1], $tmp_ora[2], $tmp_an[1], $tmp_an[2], $tmp_an[0]);
                                    //
                                    $diff_seconds  = $epoch_1 - $epoch_2;
                                    $diff_weeks    = floor($diff_seconds/604800);
                                    $diff_seconds -= $diff_weeks   * 604800;
                                    $diff_days     = floor($diff_seconds/86400);
                                    $diff_seconds -= $diff_days    * 86400;
                                    $diff_hours    = floor($diff_seconds/3600);
                                    $diff_seconds -= $diff_hours   * 3600;
                                    $diff_minutes  = floor($diff_seconds/60);
                                    $diff_seconds -= $diff_minutes * 60;
                                    $propus_acum = '';
                                    //$diff_weeks." weeks, ".$diff_days." days, ".$diff_hours." hours,". $diff_minutes." minutes, and ".$diff_seconds
                                    if($diff_weeks > 0)
                                    {  
                                        if($diff_weeks == 1)
                                        {
                                            $propus_acum .= $diff_weeks.' saptamana ,';
                                        }
                                        else
                                        {
                                            $propus_acum .= $diff_weeks.' saptamani, ';    
                                        }                                                
                                    }
                                    if($diff_days > 0)
                                    { 
                                        if($diff_days == 1)
                                        {
                                            $propus_acum .= $diff_days.' zi, ';
                                        }
                                        else
                                        {
                                            $propus_acum .= $diff_days.' zile, ';    
                                        }                                                
                                    }
                                    if($diff_hours > 0)
                                    {
                                        if($diff_hours == 1)
                                        {
                                            $propus_acum .= $diff_hours.' ora';
                                        }
                                        else
                                        {
                                            $propus_acum .= $diff_hours.' ore';    
                                        }   
                                        $propus_acum .= ' si ';                                                
                                    }      
                                    if($diff_minutes > 0)
                                    {
                                        if($diff_minutes == 1)
                                        {
                                            $propus_acum .= $diff_minutes.' minut';
                                        }
                                        else
                                        {
                                            $propus_acum .= $diff_minutes.' minute';    
                                        }   
                                    } 
                                    /*
                                    if($diff_seconds > 0)
                                    {
                                        $propus_acum .= ' si '.$diff_seconds.' secunde';    
                                    } 
                                    */                                            
                                    //
                                    echo '<div style="border-bottom:1px solid #CCCCCC; padding:6px 0 3px 0;" align="left">
                                            <img src="'.$imgpath.'/avatar_QA.gif" width="40" height="40" alt="" style="float:left; padding:0 5px 0 0;" />
                                            <h3><a href="'.$path.'/vizualizare_intrebari_si_raspunsuri.php?id='.$row['ID'].'" title="'.$row['Titlu'].'">'.$row['Titlu'].'</a></h3>
                                            <div style="font-size:11px; color:#555;">A intrebat '.$row['nume'].' - <span class="QAraspunsuri">'.$numar_raspunsuri.'</span> - Propus acum <span class="QAraspunsuri">'.$propus_acum.'</span></div>
                                            <br class="clear" /> 
                                          </div>';
                                    
                                }
                            }
                        ?>                        
                    <?php                        
                }
                else
                {
                    if(isset($_GET['pas']) && $_GET['pas'] == 1)
                    {
                        include("includes/intrebari_si_raspunsuri/stanga_propune_o_intrebare.php");
                    }
                    //
                    if(isset($_GET['pas']) && $_GET['pas'] == 2)
                    {
                        include("includes/intrebari_si_raspunsuri/stanga_intrebari_si_raspunsuri_finalizare.php");
                    }   
                    //                         
                    if(isset($_GET['pas']) && $_GET['pas'] == 3)
                    {
                        include("includes/intrebari_si_raspunsuri/stanga_intrebari_si_raspunsuri_multumire.php");
                    }                               
                }
            ?>
        </div>            
    </div>