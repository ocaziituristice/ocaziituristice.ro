<?php $continent=desfa_link($_REQUEST['continente']); 
$tara=desfa_link($_REQUEST['tara']); ?>
<h1 class="title">Circuite <?php echo ucwords($continent)." - ".ucwords($tara); ?></h1>
<br class="clear" />

<div class="ordonare" align="right">
    <table cellspacing="0">
        <tr>
            <td><span class="red">Ordoneaza dupa:</span></td>
            <td>
                <form name="ord_pret" id="ord_pret" method="post" action=""> 
                  <input type="hidden" name="tip_pret" value="<?php if(!isset($_POST['tip_pret'])) echo 'desc'; elseif(isset($_POST['tip_pret']) && $_POST['tip_pret']=='desc') echo 'asc'; else echo 'desc'; ?>" />
                  <?php if(!isset($_POST['tip_pret'])) { ?>
                  <a href="#" onclick="$('#loader').show(); document.ord_pret.submit();" class="initial" title="Pret Descendent">Pret</a> 
				  <?php } else { ?> <a href="#" onclick="$('#loader').show(); document.ord_pret.submit();" class="selected" title="Pret <?php if(isset($_POST['tip_pret']) && $_POST['tip_pret']=='desc') echo 'Ascedent'; else echo 'Descendent'; ?>"><?php if(isset($_POST['tip_pret']) && $_POST['tip_pret']=='desc') { ?><img src="/images/spacer.gif" class="asc" /> <?php } else { ?><img src="/images/spacer.gif" class="desc" /><?php } ?> Pret</a><?php } ?>
<?php if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') { ?> <input type="hidden" name="transport" value="<?php echo $_REQUEST['transport']; ?>" /><?php } ?>
<?php if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') { ?> <input type="hidden" name="stele" value="<?php echo $_REQUEST['stele']; ?>" /><?php } ?>
<?php if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') { ?> <input type="hidden" name="masa" value="<?php echo $_REQUEST['masa']; ?>" /><?php } ?>
<?php if($_REQUEST['localitate_plecare']) { ?> <input type="hidden" name="localitate_plecare" value="<?php echo $_REQUEST['localitate_plecare']; ?>" /><?php } ?>
                </form>   
            </td>
            <td>
                <form name="ord_implicita" id="ord_implicita" method="post" action="">
                <?php if(isset($_POST['tip_relevanta']) || (isset($_POST['tip_stele']) && !$_REQUEST['stele']) || isset($_POST['tip_pret'])) { ?>
                <a onclick="$('#loader').show(); document.ord_implicita.submit();" class="initial" title="Ordonare implicita">Ordonare implicita</a>
                <?php } else { ?>
                <a onclick="$('#loader').show(); document.ord_implicita.submit();" class="selected" title="Ordonare implicita">Ordonare implicita</a>
                <?php } ?>
 <?php if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') { ?> <input type="hidden" name="transport" value="<?php echo $_REQUEST['transport']; ?>" /><?php } ?>
<?php if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') { ?> <input type="hidden" name="stele" value="<?php echo $_REQUEST['stele']; ?>" /><?php } ?>
<?php if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') { ?> <input type="hidden" name="masa" value="<?php echo $_REQUEST['masa']; ?>" /><?php } ?>
<?php if($_REQUEST['localitate_plecare']) { ?> <input type="hidden" name="localitate_plecare" value="<?php echo $_REQUEST['localitate_plecare']; ?>" /><?php } ?>               
                </form>            
            </td>            
        </tr>
    </table>
</div>

<?php  $afisare=new AFISARE_CIRCUITE();
$afisare->set_oferte_pagina(10);
$afisare->setContinent($_REQUEST['continente']);
$afisare->setTari($_REQUEST['tara']);
if($_POST['tip_pret']) $afisare->setOrdonarePret($_POST['tip_pret']);
$afisare->paginare(); ?>

<br class="clear" /><br />
<?php include($_SERVER['DOCUMENT_ROOT'].'/concurs/banner_concurs.php'); ?>
<div class="spacer"><img src="/images/spacer.gif" width="1" height="6" alt="" /></div>
