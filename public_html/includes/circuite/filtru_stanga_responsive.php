<div class="titlu blue" style="margin-bottom: 10px">Filtrare dupa:</div>

<?php if ( $id_transport || $nr_zile || $din_luna || $_GET['early-booking'] == 'da' ) { ?>
    <a href="<?php echo $link_p; ?>" class="delete" rel="nofollow"><span>X</span> Sterge toate filtrele</a>
    <ul class="delete-filters black">
		<?php
		if ( $checkin ) {
			echo '<li style="cursor: default">check-in <strong>' . date( "d.m.Y", strtotime( $checkin ) ) . '</strong></li>';
		}
		if ( $early ) {
			echo '<li style="cursor: default">early booking</li>';
		}
		if ( $din_luna ) {
			$month    = explode( '-', $din_luna );
			echo '<li style="cursor: default">' . $luna[ $month[0] ] . ' ' . $month[1] . '</li>';
		}
		if ( $id_transport ) {
			echo '<li style="cursor: default">' . $trans . '</li>';
		}
		if ( $nr_zile ) {
			echo '<li style="cursor: default">' . $nr_zile . ' zile</li>';
		}
		?>
    </ul>
    <br>
<?php } ?>

<?php
$tipuri = get_tiputi_circuite( '', $id_tara, '', '' );
$i      = 0;
if ( sizeof( $tipuri ) > 0 ) : ?>
    <div class="item">
        <div class="heading" data-toggle="collapse" data-target="#special-offer">Ocazii speciale</div>
        <div id="special-offer" class="show">
            <ul>
                <li class="label">
					<?php // TODO: invert the link ?>
					<?php /* <a<?php echo ! $_REQUEST['tip'] ? ' class="sel"' : '' ?> onclick="document.location.href='<?php echo $sitepath . 'circuite/' ?><?php echo $link_tara ? $link_tara . '/' : '' ?>'">
                            Toate
                        </a> */ ?>
                    <a<?php echo ! $_REQUEST['tip'] ? ' class="sel"' : '' ?>
                            onclick="document.location.href='<?php echo $sitepath . 'circuite-responsive/' ?><?php echo $link_tara ? $link_tara . '/' : '' ?>'">
                        Toate
                    </a>
                </li>
				<?php foreach ( $tipuri as $key_t => $value_t ) :
					$i ++;
					$valTip = fa_link( $value_t['denumire'] ); ?>
                    <li class="label">
                        <a<?php echo $valTip == $_REQUEST['tip'] ? ' class="sel"' : '' ?>
                                onclick="document.location.href='<?php echo $sitepath . 'circuite-' . $valTip . '/' ?><?php echo $link_tara ? $link_tara . '/' : '' ?>'">
							<?php echo $value_t['denumire']; ?>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>

<?php $data_link = $link_p1 = $link_p . '?optiuni=da'; ?>

<?php if ( $_REQUEST['ordonare'] ) {
	$data_link = $link_p1 = $link_p1 . '&ordonare=' . $_REQUEST['ordonare'];
} ?>

<?php if ( $_REQUEST['data-plecare'] ) {
	$link_p1 = $link_p1 . '&data-plecare=' . $_REQUEST['data-plecare'];
} ?>
<div class="item">
    <div class="heading" data-toggle="collapse" data-target="#month">Luna plecarii</div>
    <div id="month" class="show">
        <ul>
            <li class="label">
				<?php if ( sizeof( $luni_plecari ) > 0 ) : ?>
					<?php foreach ( $luni_plecari as $value ) : ?>
						<?php
						$lun    = explode( '-', $value );
						$link_z = $value;

						?>
                        <a<?php echo $link_z == $din_luna ? ' class="sel"' : '' ?>
                                onclick="document.location.href='<?php echo $data_link . '&data-plecare=' . $link_z; ?>'">
							<?php echo $luna[ $lun[0] ] . ' ' . $lun[1]; ?>
                        </a>
					<?php endforeach; ?>
				<?php endif; ?>
            </li>
        </ul>
    </div>
</div>

<?php if ( $_GET['early-booking'] ) {
	$link_p1 = $link_p1 . '&early-booking=da';
}
if ( sizeof( $filtruOf['trans'] ) > 0 ) { ?>
    <div class="item">
        <div class="heading" data-toggle="collapse" data-target="#transport">Transport</div>
		<?php krsort( $filtruOf['trans'] ); ?>
        <div id="transport" class="show">
            <ul>
                <li class="label">
                    <a href="<?php echo make_link_filters_circuite( $link_p1, 'toate', $nr_zile, $plecare_avion ); ?>"
                       rel="nofollow"<?php echo $transport == 'toate' ? ' class="sel"' : '' ?>>
                        Toate
                    </a>
                </li>
				<?php $i = 0;
				foreach ( $filtruOf['trans'] as $key => $value ) {
					$i ++;
					$lingTr = fa_link( $key );
					if ( $value ) {
						$nr = $value;
					} else {
						$nr = 0;
					}
					if ( $transport == $lingTr ) {
						$tr       = $lingTr;
						$selclass = 'class="sel"';
					} else {
						$tr       = $lingTr;
						$selclass = '';
					}
					?>
                    <li class="label">
                        <a href="<?php echo make_link_filters_circuite( $link_p1, $tr, $nr_zile, $plecare_avion ); ?>" <?php echo $selclass; ?>
                           rel="nofollow"><?php echo str_replace( " si ", " + ", $key );
							if ( $key == 'Avion' ) {
								echo ' + Autocar local';
							} ?>
							<?php echo '(' . $nr . ')'; ?>
                        </a>
						<?php if ( $key == 'Avion' or $key == 'Avion si Autocar local' ) { ?>
                            <select name="oras_plecare" onchange="filtrare_circuite(<?php echo "'" . $link_p1;
							echo "'"; ?>, '<?php echo fa_link( $key ); ?>', <?php echo "'" . $nr_zile . "'"; ?>, this.value);"
                                    class="oras-plecare">
                                <option value="toate" <?php if ( sizeof( $filtruActual['plecare_avion'] ) == 0 ) {
									echo 'disabled';
								}
								if ( $plecare_avion == 'toate' ) {
									echo 'selected';
								} ?>>Alegeti orasul plecarii
                                </option>
								<?php if ( sizeof( $filtruOf['plecare_avion'] ) ) {
									krsort( $filtruOf['plecare_avion'] );
									foreach ( $filtruOf['plecare_avion'] as $key => $value ) {
										$lingTr = fa_link( $key );
										?>
                                        <option value="<?php echo $lingTr; ?>" <?php if ( $lingTr == $plecare_avion || ( $transport == 'avion' && sizeof( $filtruOf['plecare_avion'] ) == 1 ) ) {
											echo 'selected';
										} ?>><?php echo $key; ?></option>
									<?php }
								} ?>
                            </select>
						<?php } ?>
                    </li>
				<?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>

<?php if ( sizeof( $filtruOf['nr_zile'] ) > 0 ) { ?>
    <div class="item">
        <div class="heading" data-toggle="collapse" data-target="#duration">Durata</div>
		<?php ksort( $filtruOf['nr_zile'] ); ?>
        <div id="duration" class="show">
            <ul>
                <li class="label">
                    <a href="<?php echo make_link_filters_circuite( $link_p1, $transport, 'toate', $plecare_avion ); ?>"
                       rel="nofollow" <?php echo $zile == 'toate' ? ' class="sel"' : '' ?>>
                        Toate
                    </a>
                </li>
				<?php foreach ( $filtruOf['nr_zile'] as $i => $nr_z ) {
					if ( $nr_z ) {
						$nrZ = $nr_z;
					} else {
						$nrS = 0;
					}
					if ( is_array( $zile ) ) {
						if ( in_array( $i, $zile ) ) {
							$zl       = remove_current_filter( $nr_zile, $i );
							$selclass = 'class="sel"';
						} else {
							$zl       = $nr_zile . ',' . $i;
							$selclass = '';
						}
					} else {
						$zl       = $i;
						$selclass = '';
					}
					?>
                    <li class="label">
                        <a href="<?php echo make_link_filters_circuite( $link_p1, $transport, $zl, $plecare_avion ); ?>" <?php echo $selclass; ?>
                           rel="nofollow">
							<?php echo $i;
							if ( $i > 1 ) {
								echo ' zile ';
							} else {
								echo ' zi ';
							} ?>
							<?php echo '(' . $nrZ . ')'; ?>
                        </a>
                    </li>
				<?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>

