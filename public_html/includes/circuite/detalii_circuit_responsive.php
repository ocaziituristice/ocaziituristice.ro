<div class="circuit"<?php echo $detalii['valabila'] == 'nu' ? ' style="background:#FFF url(\'/images/oferta_expirata.gif\') center 220px;"' : '' ?>>
	<?php
	$link_rezervare      = $sitepath . 'includes/rezervare_circuit.php?hotel=' . $id_hotel . '&amp;oferta=' . $id_oferta;
	$link_rezervare2     = $sitepath . 'rezervare_sejur_new.php?hotel=' . $id_hotel . '&amp;oferta=' . $id_oferta;
	$link_cerere_detalii = $sitepath . 'cerere_detalii_new.php?hotel=' . $id_hotel . '&amp;oferta=' . $id_oferta;
	$valuta              = 'nu';
	?>

    <div class="clear">
        <div class="left-column">
            <h1><?php echo $detalii_hotel["denumire"]; ?></h1>
			<?php if ( $detalii['valabila'] == 'nu' ) : ?>
                <h2 class="text-center red" style="font-size:2.5em;">ACEASTA OFERTA NU ESTE VALABILA !</h2>
			<?php endif ?>
            <div class="address">
				<?php $sel_cont = "SELECT
GROUP_CONCAT(DISTINCT tari.denumire Order by traseu_circuit.ordine SEPARATOR ', ') as tari,
GROUP_CONCAT(DISTINCT 
if(traseu_circuit.tara_principala='da',tari.denumire,'') Order by traseu_circuit.ordine SEPARATOR ',') as tari_principale,
GROUP_CONCAT(DISTINCT localitati.denumire Order by traseu_circuit.ordine SEPARATOR ', ') as localitati,
GROUP_CONCAT(DISTINCT traseu_circuit.id_hotel SEPARATOR ',') as hoteluri
FROM
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
Left join localitati on traseu_circuit.id_localitate = localitati.id_localitate
WHERE
hoteluri.tip_unitate = 'Circuit' and
oferte.id_oferta = '" . $id_oferta . "'
Order by traseu_circuit.ordine ";
				$que_cont = mysql_query( $sel_cont ) or die( mysql_error() );
				$row_of          = mysql_fetch_array( $que_cont );
				$tari_principale = $row_of['tari_principale'];
				@mysql_free_result( $que_cont );
				echo $row_of['tari'];
				if ( $row_of['localitati'] ) {
					echo '<br/><i class="fa fa-map-marker"></i> <em class="blue">Localitati traversate:</em> ' . $row_of['localitati'];
				} ?>
            </div>
        </div>

        <div class="right-column">
            <div class="tarif">
                Tarif de la
                <span class="pret red"><?php echo new_price( $preturi['minim']['pret'] ); ?></span>
                <span class="exprimare red"><?php echo moneda( $detalii['moneda'] ); ?></span>
                pe <?php echo $detalii['exprimare_pret']; ?>
            </div>
        </div>
    </div>

    <div class="clear">
        <div class="left-column images">
            <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-fit="cover" data-loop="true"
                 data-ratio="1.7">
				<?php for ( $i = 1; $i <= 8; $i ++ ) : ?>
					<?php if ( $detalii_hotel[ 'poza' . $i ] ) : ?>
                        <a href="https://www.ocaziituristice.ro/img_prima_hotel/<?php echo $detalii_hotel[ 'poza' . $i ]; ?>"
                           data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/<?php echo $detalii_hotel[ 'poza' . $i ]; ?>"></a>
					<?php endif ?>
				<?php endfor ?>
            </div>
			<?php if ( ( sizeof( $detalii_online['early_time'] ) > 0 or $detalii_hotel['data_early_booking'] > date( 'Y-m-d' ) ) and $detalii_online['last_minute'] < 1 ) { ?>
				<?php if ( is_array( $detalii_online['early_disc'] ) ) {
					$reduce_eb = max( $detalii_online['early_disc'] );
				}
				if ( $reduce_eb < $detalii_hotel['comision_reducere'] ) {
					$reduce_eb = $detalii_hotel['comision_reducere'];
				}
				if ( $reduce_eb > 1 ) {
					echo '<div class="discount eb"> <strong>Reducere <br />Early Booking <span>' . $reduce_eb . '% </span></strong></div>';
				}
			} ?>
			<?php if ( $detalii['spo_titlu'] ) {
				echo '<div class="oferta-speciala"></div>';
			};
			if ( $detalii_online['last_minute'] > 0 ) {
				echo '<div class="last-minute"></div>';
			} ?>


			<?php // TODO: de verificat pe versiunea live
			/*
			<div class="info-scurte-chn" style="padding-top:10px;">
				<br/>
				<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/socials_top.php" ); ?>
				<br class="clear"/>
			</div> */ ?>
        </div>
        <div class="trust right-column"></div>
    </div>
    <div class="clear"></div>
</div>

<?php if ( $preturi['camera'] > 0 ) : ?>
    <div class="circuit">
        <h2 class="expand icon">Preturi pe camera</h2>
        <div class="expandable">
			<?php if ( sizeof( $detalii['early_time'] ) > 0 ) : ?>
                <div class="detalii-oferta">
                    <a name="detalii-oferta"></a>
                    <div class="red">
						<?php foreach ( $detalii['early_time'] as $keyE => $valueE ) : ?>
                            <strong><?php echo $detalii['early_disc'][ $keyE ]; ?>%
                                REDUCERE</strong> pentru rezervarile confirmate si achitate integral pana la data de
                            <strong><?php echo strftime( '%d %B %Y', strtotime( $valueE ) ); ?></strong><br/>
						<?php endforeach; ?>
                    </div>
                </div>
			<?php endif ?>
			<?php if ( sizeof( $preturi['data_start'] ) > 0 ) : ?>
                <table class="tabel-preturi" style="width:100%;">
                    <thead>
                    <tr>
                        <th class="text-center" style="width:150px;">Plecare</th>
						<?php $g = 0;
						foreach ( $preturi['camera'] as $key_cam => $value_cam ) {
							$g ++;
							if ( sizeof( $preturi['camera'] ) == $g ) {
								$class = 'text-center last';
								$g     = 0;
							} else {
								$class = 'text-center';
							} ?>
                            <th class="<?php echo $class; ?>"><?php echo $value_cam; ?></th>
						<?php } ?>
                    </tr>
                    </thead>
                    <tbody>
					<?php $nr_t = 0;
					foreach ( $preturi['data_start'] as $key_d => $data_start ) {
						$nr_t ++;
						if ( $nr_t % 2 == 1 ) {
							$class_tr = 'impar';
						} else {
							$class_tr = 'par';
						}
						$numar_minim_nopti = ''; ?>
                        <tr onmouseover="this.className='hover';"
                            onmouseout="this.className='<?php echo $class_tr; ?>';"
                            class="<?php echo $class_tr; ?>">
                            <th class="text-center nowrap"><?php echo strftime( '%d %B %Y', strtotime( $data_start ) ); ?>
                                <br/>
								<?php if ( sizeof( $preturi['secundar'][ $data_start ][ $preturi['data_end'][ $key_d ] ] ) > 0 ) { ?>
                                    <div class="red"><a
                                            onmousedown="if(document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display == 'none'){ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'block'; }else{ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'none'; }"
                                            class="pointer">toate preturile</a></div><?php } ?>
                            </th>
							<?php $g = 0;
							foreach ( $preturi['camera'] as $key_cam => $value_cam ) {
								$g ++;
								if ( sizeof( $preturi['camera'] ) == $g ) {
									$class = 'text-center pret last';
									$g     = 0;
								} else {
									$class = 'text-center pret';
								} ?>
                                <td <?php echo 'class="' . $class . '"'; ?>><?php if ( $preturi['pret'][ $key_d ][ $key_cam ] ) {
										echo new_price( $preturi['pret'][ $key_d ][ $key_cam ] ) . ' ';
										if ( $preturi['moneda'][ $key_d ][ $key_cam ] == 'EURO' ) {
											echo '&euro;';
											$valuta = 'da';
										} elseif ( $preturi['moneda'][ $key_d ][ $key_cam ] == 'USD' ) {
											echo '$';
											$valuta = 'da';
										} else {
											echo $preturi['moneda'][ $key_d ][ $key_cam ];
										}
									} else {
										echo '-';
									} ?></td>
							<?php } ?>
                        </tr>
						<?php if ( sizeof( $preturi['secundar'][ $data_start ][ $preturi['data_end'][ $key_d ] ] ) > 0 ) { ?>
                            <tr>
                                <td colspan="5" class="text-left last" style="padding:0;">
                                    <div id="alte_preturi<?php echo $key_d; ?>"
                                         style="display:none; padding-left:164px; background:#d2f6c5;">
                                        <table class="tabel-preturi" style="width:100%; margin:0;">
                                            <tbody>
                                            <tr>
												<?php $sup_i = 0;
												$class_nr    = '';
												foreach ( $preturi['secundar'][ $data_start ][ $preturi['data_end'][ $key_d ] ] as $key_sup => $value_sup ) {
													$sup_i ++;
													if ( $sup_i == sizeof( $preturi['secundar'][ $data_start ][ $preturi['data_end'][ $key_d ] ] ) ) {
														$class_sup = 'text-center last';
													} else {
														$class_sup = 'text-center';
													} ?>
                                                    <th class="<?php echo $class_sup;
													if ( $class_nr ) {
														echo ' ' . $class_nr;
													} ?>"><?php echo $key_sup; ?></th>
												<?php } ?>
                                            </tr>
                                            <tr>
												<?php $sup_i = 0;
												foreach ( $preturi['secundar'][ $data_start ][ $preturi['data_end'][ $key_d ] ] as $key_sup => $value_sup ) {
													$sup_i ++;
													if ( $sup_i == sizeof( $preturi['secundar'][ $data_start ][ $preturi['data_end'][ $key_d ] ] ) ) {
														$class_sup = 'last';
													} else {
														$class_sup = '';
													} ?>
                                                    <td class="text-center pret <?php echo $class_sup;
													if ( $class_nr ) {
														echo ' ' . $class_nr;
													} ?>"><?php if ( $value_sup['pret'] ) {
															echo new_price( $value_sup['pret'] ) . ' ';
															if ( $value_sup['moneda'] == 'EURO' ) {
																echo '&euro;';
															} elseif ( $value_sup['moneda'] == 'USD' ) {
																echo '$';
															} else {
																echo $value_sup['moneda'];
															}
														} ?></td>
												<?php } ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
						<?php } ?>
					<?php } ?>
                    </tbody>
                </table>
                <br/>
			<?php endif ?>
            <div class="grey2 bold" style="text-align:right; padding:0 5px 10px 5px;">* tarifele sunt exprimate in <span
                        class="bigger-13em blue"><?php echo $detalii['moneda']; ?>
                    pe <?php echo $detalii['exprimare_pret']; ?></span>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="circuit">
	<?php if ( $detalii['rezervare_online'] == 'da' and $detalii['valabila'] == 'da' ) {
		include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/sejururi/calculare_pret_new_responsive.php" );
	} elseif ( $detalii['valabila'] == 'da' ) {

		include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/sejururi/cerere_detalii.php" );

		if ( ! is_bot() and isset( $err_logare_admin ) and $detalii['valabila'] == 'da' ) {
			erori_disponibilitate( $id_oferta, $id_hotel, $plecdata, $pleczile, $adulti, $copii );
		}

	} ?>
</div>

<?php if ( sizeof( $detalii['data_start_reduceri_speciale'] ) > '0' ) : ?>
    asdasd
    <div class="circuit">
        <h2 class="red underline" style="margin-bottom:5px;"><img
                    src="<?php echo $imgpath; ?>/icon_section_reduceri.png" style="vertical-align:middle;"
                    alt="Reduceri"/> Reduceri speciale <?php echo $detalii_hotel['denumire']; ?></h2>
		<?php foreach ( $detalii['data_start_reduceri_speciale'] as $jey_r => $value_r ) : ?>
            <p style="text-indent:10px;">Pentru sosirile cuprinse in perioada <strong
                        class="green"><?php echo $value_r . ' - ' . $detalii['data_end_reduceri_speciale'][ $jey_r ]; ?></strong>
                <span class="red">platesti <strong><?php echo $detalii['zile_deaplicare'][ $jey_r ] . ' nopti</strong> si stai <strong>' . $detalii['zile_aplicare'][ $jey_r ] . ' nopti</strong>'; ?></span>
            </p>
		<?php endforeach; ?>
    </div>
<?php endif; ?>

<?php if ( sizeof( $detalii['denumire_v3'] ) > '0' || sizeof( $detalii['denumire_v4'] ) > '0' || sizeof( $excursii ) > '0' ) : ?>
	<?php if ( sizeof( $detalii['denumire_v4'] ) > '0' ) : ?>
        <div class="circuit">
            <h3 class="red">Reduceri</h3>
            <div class="content">
                <ul>
					<?php foreach ( $detalii['denumire_v4'] as $key => $value ) : ?>
                        <li><?php echo ucfirst( $value );
							if ( $detalii['value_v4'][ $key ] ) : ?>
                                -
                                <strong><?php echo $detalii['value_v4'][ $key ] . ' ' . moneda( $detalii['moneda_v4'][ $key ] ) ?></strong>
							<?php endif; ?>
                        </li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
	<?php endif; ?>
	<?php $excursii = $det->get_excursii( $id_oferta );
	if ( sizeof( $excursii ) > 0 ) : ?>
        <div class="circuit">
            <h3 class="red">Excursii optionale</h3>
            <div class="content">
                <ul>
					<?php foreach ( $excursii as $key_e => $value_e ) : ?>
                        <li>
                            <a href="<?php echo $sitepath . 'excursii-optionale/' . fa_link( $detalii_hotel['tara'] ) . '/' . fa_link_oferta( $value_e['denumire'] ) . '-' . $value_e['id_excursie'] . '.html'; ?>"
                               rel="nofollow"
                               class="excursii_detalii link-blue"><?php echo $value_e['denumire'];
								if ( $value_e['pret'] ) {
									echo ' - <em class="red">' . $value_e['pret'] . ' ';
									echo moneda( $value_e['moneda'] );
									echo '</em> - ' . ucwords( $value_e['tip'] );
								} else {
									echo ' - <em class="red bold">' . ucwords( $value_e['tip'] ) . '</em>';
								} ?>
                            </a>
                        </li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
	<?php endif; ?>
<?php endif; ?>

<div class="circuit left-side">
    <h2 class="section-title expand icon">Intinerariu</h2>
    <div class="expandable article" style="font-size:14px; line-height:22px;">
		<?php afisare_frumos( schimba_caractere( $detalii_hotel["descriere"] ), 'nl2p' ); ?>
    </div>
</div>

<div class="circuit right-side expandable">
	<?php if ( sizeof( $detalii['denumire_v1'] ) > '0' ) : ?>
		<?php if ( $afisare_servicii == 'da' ) : ?>
            <div class="right-side-box">
                <h2 class="black expand icon">Tarifele includ</h2>
                <div class="expandable">
                    <ul>
						<?php foreach ( $detalii['denumire_v1'] as $key => $value ) : ?>
                            <li><?php echo schimba_caractere( ucfirst( $value ) );
								if ( $detalii['value_v1'][ $key ] ) {
									echo ' - <strong>' . $detalii['value_v1'][ $key ] . ' ' . moneda( $detalii['moneda_v1'][ $key ] ) . '</strong>';
								} ?>
                            </li>
						<?php endforeach; ?>
                    </ul>
                </div>
            </div>
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( sizeof( $detalii['denumire_v2'] ) > '0' or sizeof( $detalii['denumire_v3'] ) > '0' ) : ?>
        <div class="right-side-box">
            <h2 class="black expand icon">Neincluse in tarif</h2>
            <div class="expandable">
                <ul>
					<?php if ( sizeof( $detalii['denumire_v2'] ) > '0' ) {
						foreach ( $detalii['denumire_v2'] as $key => $value ) {
							if ( $afisare_servicii == 'da' or ( $afisare_servicii == 'nu' and $detalii['obligatoriu_v2'][ $key ] != 'da' ) ) { ?>
                                <li><?php echo schimba_caractere( ucfirst( $value ) );
									if ( $detalii['data_start_v2'][ $key ] ) {
										echo ' <em>(' . date( "d.m.Y", strtotime( $detalii['data_start_v2'][ $key ] ) ) . ' - ' . date( "d.m.Y", strtotime( $detalii['data_end_v2'][ $key ] ) ) . ')</em>';
									}
									if ( $detalii['value_v2'][ $key ] ) {
										echo ' - <strong>' . $detalii['value_v2'][ $key ] . ' ' . moneda( $detalii['moneda_v2'][ $key ] ) . ' ' . $detalii['exprimare_v2'][ $key ] . ' ' . $detalii['pasager_v2'][ $key ] . '</strong>';
									}
									if ( $detalii['obligatoriu_v2'][ $key ] == 'da' ) {
										echo ' - <em class="red bold">Obligatoriu</em>';
									} else {
										echo ' - <em class="blue">Optional</em>';
									} ?>
                                </li>
							<?php }
						}
					} ?>
					<?php if ( sizeof( $detalii['denumire_v3'] ) > '0' ) {
						foreach ( $detalii['denumire_v3'] as $key => $value ) {
							if ( $afisare_servicii == 'da' or ( $afisare_servicii == 'nu' and $detalii['obligatoriu_v3'][ $key ] != 'da' ) ) { ?>
                                <li><?php echo schimba_caractere( ucfirst( $value ) );
									if ( $detalii['data_start_v3'][ $key ] ) {
										echo ' <em>(' . date( "d.m.Y", strtotime( $detalii['data_start_v3'][ $key ] ) ) . ' - ' . date( "d.m.Y", strtotime( $detalii['data_end_v3'][ $key ] ) ) . ')</em>';
									}
									if ( $detalii['value_v3'][ $key ] ) {
										echo ' - <strong>' . $detalii['value_v3'][ $key ] . ' ' . moneda( $detalii['moneda_v3'][ $key ] ) . ' ' . $detalii['exprimare_v3'][ $key ] . ' ' . $detalii['pasager_v3'][ $key ] . '</strong>';
									}
									if ( $detalii['obligatoriu_v3'][ $key ] == 'da' ) {
										echo ' - <em class="red bold">Obligatoriu</em>';
									} else {
										echo ' - <em class="blue">Optional</em>';
									} ?>
                                </li>
							<?php }
						}
					} ?>
                </ul>
            </div>
        </div>
	<?php endif; ?>
	<?php if ( $detalii['conditii_plata'] ) : ?>
        <div class="right-side-box">
            <h2 class="black expand icon">Conditii de plata</h2>
            <div class="content expandable">
                <ul>
                    <li><?php echo afisare_frumos( $detalii['conditii_plata'], 'nl2br' ); ?></li>
                </ul>
            </div>
            <div class="clear-line"></div>
        </div>
	<?php endif; ?>
	<?php if ( $detalii['conditii_anulare'] ) : ?>
        <div class="right-side-box">
            <h2 class="black expand icon">Conditii de anulare</h2>
            <div class="content expandable">
                <ul>
                    <li><?php echo afisare_frumos( $detalii['conditii_anulare'], 'nl2br' ); ?></li>
                </ul>
            </div>
            <div class="clear-line"></div>
        </div>
	<?php endif; ?>
</div>

<div class="circuit left-side nota">
    <div class="content">* Continutul ofertei este valabil la data introducerii acesteia.
        Datorita specificului industriei de turism ofertele se pot schimba foarte des si nu pot fi actualizate in
        timp util.
    </div>
</div>

<div class="clear"></div>

<?php if ( $detalii["descriere_oferta"] || $row_of['hoteluri'] ) { ?>
    <div class="circuit bus-info">
        <h2 class="section-title expand icon open"><?php echo $detalii_hotel["denumire"]; ?></h2>
        <div class="expandable" style="display: none">
			<?php afisare_frumos( schimba_caractere( $detalii["descriere_oferta"] ), 'nl2p' );
			if ( $row_of['hoteluri'] ) {
				$selH = "select
hoteluri_viz.id_hotel,
hoteluri_viz.nume, 
hoteluri_viz.stele, 
hoteluri_viz.poza1,
hoteluri_viz.descriere,
tari.denumire as denumire_tara,
localitati.denumire as denumire_localitate
from 
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
inner join hoteluri as hoteluri_viz on traseu_circuit.id_hotel = hoteluri_viz.id_hotel
Inner Join localitati ON hoteluri_viz.locatie_id = localitati.id_localitate
inner join zone on localitati.id_zona = zone.id_zona
inner join tari on zone.id_tara = tari.id_tara
where
oferte.valabila =  'da' and
hoteluri.tip_unitate = 'Circuit' and
oferte.id_oferta = '" . $id_oferta . "'
Group by hoteluri_viz.id_hotel
Order by hoteluri_viz.nume ";
				$queH = mysql_query( $selH ) or die( mysql_error() );
				while ( $rowH = mysql_fetch_array( $queH ) ) {
					$link = $sitepath . 'hoteluri/' . fa_link( $rowH['denumire_tara'] ) . '/' . fa_link( $rowH['denumire_localitate'] ) . '/' . fa_link_oferta( $rowH['nume'] ) . '-' . $rowH['id_hotel'] . '.html'; ?>
                    <div class="NEW-chn-oferta NEW-chn-oferta-red">
                        <a href="<?php echo $link; ?>"><img
                                    src="<?php echo $sitepath . 'thumb_hotel/' . $rowH['poza1']; ?>" class="icon"
                                    alt="<?php echo $rowH['nume']; ?>"/></a>
                        <h2><a href="<?php echo $link; ?>" class="red"
                               title="<?php echo $rowH['nume']; ?>"><?php echo $rowH['nume']; ?> <img
                                        src="/images/spacer.gif" class="stele-mari-<?php echo $rowH['stele']; ?>"
                                        alt=""/></a></h2>
                        <p><?php echo $rowH['descriere']; ?></p>
                        <div class="clear"></div>
                    </div>
				<?php }
				@mysql_free_result( $queH );
			} ?>
        </div>
    </div>
    <div class="clear"></div>
<?php } ?>

<?php $autocar = array();
if ( $detalii['transport'] == 'Autocar' ) {
	$autocar = $det->get_autocar_sejur( $id_oferta );
}
if ( sizeof( $autocar ) > 0 ) { ?>
    <div class="circuit orar-transport">
        <h2 class="section-title expand icon open">Orar transport</h2>
        <div class="expandable" style="display: none;">
            <table class="tabel-transport" style="width:100%;">
                <thead>
                <tr>
                    <th class="text-left">Localitate Plecare</th>
                    <th class="text-center">Ora Plecare</th>
                    <th class="text-left">Detalii</th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ( $autocar as $key_a => $value_a ) { ?>
                    <tr>
                        <td class="text-left"><?php echo $value_a['denumire_localitate']; ?></td>
                        <td class="text-center"><?php echo $value_a['ora']; ?></td>
                        <td class="text-left"><?php echo $value_a['detalii']; ?></td>
                    </tr>
				<?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>

<?php if ( $detalii['nota'] ) : ?>
    <div class="circuit nota">
        <h2 class="black expand icon open">Nota</h2>
        <div class="content expandable" style="display: none;">
			<?php afisare_frumos( schimba_caractere( trim( $detalii['nota'] ) ), 'nl2br' ); ?>
        </div>
    </div>
<?php endif; ?>

<div class="circuit bottom trust right-column"><?php include( "includes/trustme.php" ) ?></div>

<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/dreapta/new_circuit.php" ); ?>
<?php if ( isset( $_GET['plecdata'] ) ) {
	include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/dreapta/addins_dreapta_new.php" );
} ?>
<div class="clear"></div>
<script>
        var mq = window.matchMedia("(min-width: 800px)");
        if (mq.matches) {
            $(".trust").load("/includes/trustme.php");
        } else {
            $(".second_mod").load("/includes/trustme.php");

        }
</script>