<div id="NEW-destinatie">

  <div class="float-left w620">
    <h1 class="blue"><?php echo $titlu_pag; ?></h1>
  </div>
  <div class="float-right"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?></div>
  
  <br class="clear">
  
<div class="NEW-column-right2 clearfix">

    <?php //include_once($_SERVER['DOCUMENT_ROOT'].'/includes/circuite/tematici.php'); ?>
    
    <div class="rez-afisare red">Afisare circuite <?php if($tip) echo ucwords($tip).' '; if($nr_zile) { ?> de <strong><?php echo $nr_zile.' zile'; ?></strong><?php } if($id_transport) { ?> cu transport <strong><?php echo ucwords($trans); ?></strong><?php } if($plecare_avion<>'toate') echo ', plecare din '.ucwords($plecare_avion); ?></div>
  
<?php $link_of_pag=$link; 
if($_REQUEST['ordonare']) $link=$link."&ordonare=".$_REQUEST['ordonare'];
if($link=='?optiuni=da') $link='';
$oferte=new AFISARE_SEJUR_NORMAL();
$oferte->setAfisare($from, $nr_pe_pagina);
$oferte->setCircuit('da');
//$oferte->setContinent($link_continent);
$oferte->setTariCircuit(array('tari'=>$tara));
if($early=='da') $oferte->setEarly('da');
if($id_tip) $oferte->setTipOferta($_REQUEST['tip']);
if($_REQUEST['ordonare']) {
$tipO=explode('-',$_REQUEST['ordonare']);
if($tipO[0]=='tip_pret') $oferte->setOrdonarePret($tipO[1]);
elseif($tipO[0]=='tip_numH') $oferte->setOrdonareNumeH($tipO[1]); }
if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $oferte->setTransport($_REQUEST['transport']);
if($nr_zile) $oferte->setDurata($nr_zile);
if($din_luna) $oferte->setLunaPlecare($din_luna);
if($id_loc_plecare_av) $oferte->setPlecareAvion($id_loc_plecare_av);

	$oferte->initializeaza_pagini($link_p, 'pag-###/', $link);
	$nr_hoteluri=$oferte->numar_oferte(); ?>
<div class="filter-pag clearfix" style="padding-bottom:15px; border-bottom:1px dashed #666;">
    <div class="filter">
      <strong>Ordoneaza dupa: &nbsp; </strong>
      <select name="ordonare" onchange="if(this.value) var ord='&amp;ordonare='+this.value; else var ord=''; document.location.href='<?php echo $link_of_pag; ?>'+ord;">
        <option value="data-introducere" <?php if(!$_REQUEST['ordonare']) { ?> selected="selected"<?php } ?>>Data introducere</option>
        <option value="tip_pret-asc" <?php if($tipO[0]=='tip_pret' && $tipO[1]=='asc') { ?> selected="selected" <?php } ?>>Pret crescator</option>
        <option value="tip_pret-desc" <?php if($tipO[0]=='tip_pret' && $tipO[1]=='desc') { ?> selected="selected" <?php } ?>>Pret descrescator</option>
        <option value="tip_numH-asc" <?php if($tipO[0]=='tip_numH' && $tipO[1]=='asc') { ?> selected="selected" <?php } ?>>Nume hotel crescator</option>
        <option value="tip_numH-desc" <?php if($tipO[0]=='tip_numH' && $tipO[1]=='desc') { ?> selected="selected" <?php } ?>>Nume hotel descrescator</option>
      </select>
      <br />Afisare <strong><?php echo $st=$nr_pe_pagina*($from-1)+1; $sf=$nr_pe_pagina*$from; if($nr_hoteluri<$sf) $sf=$nr_hoteluri; echo '-'.$sf; ?></strong> circuite <?php echo ucwords($tara); ?> din <strong><?php echo $nr_hoteluri; ?> in total</strong>
    </div>
    <div class="paginatie text-right"><?php $oferte->printPagini(); ?></div>
  <br class="clear" />
 </div>
 <br class="clear" /><br />  
<?php $oferte->afiseaza();
if($nr_hoteluri>0) {
$filtruOf=$oferte->get_filtru_mare();		
$filtruActual=$oferte->get_filtru(); 
} ?>
	
    <div class="filter-pag clearfix" style="padding-top:15px;">
      <div class="filter">
        Afisare <strong><?php echo $st=$nr_pe_pagina*($from-1)+1; $sf=$nr_pe_pagina*$from; if($nr_hoteluri<$sf) $sf=$nr_hoteluri; echo '-'.$sf; ?></strong> circuite <?php echo ucwords($tara); ?> din <strong><?php echo $nr_hoteluri; ?> in total</strong><br />
        <span class="blue"><strong>* arata</strong></span>
        <select style="width:50px;" onchange="document.cookie='nr_pe_pagina='+this.value; document.location.href='<?php echo $link; ?>';">
          <option <?php if($nr_pe_pagina==10) { ?> selected="selected" <?php } ?> value="10">10</option>
          <option <?php if($nr_pe_pagina==20) { ?> selected="selected" <?php } ?> value="20">20</option>
          <option <?php if($nr_pe_pagina==50) { ?> selected="selected" <?php } ?> value="50">50</option>
        </select>
        <span class="blue"><strong>circuite <?php echo ucwords($tara); ?> pe pagina</strong></span>
      </div>
      <div class="paginatie text-right"><?php $oferte->printPagini(); ?></div>
    </div>
    
    <br class="clear" /><br />

</div>
<div class="NEW-column-left2">
  <?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/circuite/filtru_stanga.php'); ?>
</div>
<br class="clear" />
</div>
<br class="clear" />