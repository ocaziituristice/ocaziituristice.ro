<div id="NEW-detaliiOferta" <?php if($detalii['valabila']=='nu') { ?>style="background:#FFF url(/images/oferta_expirata.gif) center 220px;"<?php } ?>>
<?php
$link_rezervare = $sitepath.'includes/rezervare_circuit.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$link_rezervare2 = $sitepath.'rezervare_sejur_new.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$link_cerere_detalii = $sitepath.'cerere_detalii_new.php?hotel='.$id_hotel.'&amp;oferta='.$id_oferta;
$valuta = 'nu';
?>

  <h1 class="red"><?php echo $detalii_hotel["denumire"]; ?></h1>

  <div class="Hline"></div>
<?php if($detalii['valabila']=='nu') {
	echo '<br /><br /><h2 class="text-center red" style="font-size:2.5em;">ACEASTA OFERTA NU ESTE VALABILA !</h2>';
	echo '<br /><br />';
} ?>

<div class="NEW-column-left1">

  <div class="NEW-adresa"><?php $sel_cont="SELECT
GROUP_CONCAT(DISTINCT tari.denumire Order by traseu_circuit.ordine SEPARATOR ', ') as tari,
GROUP_CONCAT(DISTINCT 
if(traseu_circuit.tara_principala='da',tari.denumire,'') Order by traseu_circuit.ordine SEPARATOR ',') as tari_principale,
GROUP_CONCAT(DISTINCT localitati.denumire Order by traseu_circuit.ordine SEPARATOR ', ') as localitati,
GROUP_CONCAT(DISTINCT traseu_circuit.id_hotel SEPARATOR ',') as hoteluri
FROM
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
Left join localitati on traseu_circuit.id_localitate = localitati.id_localitate
WHERE
hoteluri.tip_unitate = 'Circuit' and
oferte.id_oferta = '".$id_oferta."'
Order by traseu_circuit.ordine ";
$que_cont=mysql_query($sel_cont) or die(mysql_error());
$row_of=mysql_fetch_array($que_cont);
$tari_principale=$row_of['tari_principale'];
@mysql_free_result($que_cont);
echo $row_of['tari']; if($row_of['localitati']) echo '<br/><em class="blue">Localitati traversate:</em> '.$row_of['localitati']; ?></div>

  <div class="float-left" style="width:310px;">
    <div class="NEW-gallery">
    <?php if($detalii_hotel['poza1']) { ?>
      <div class="single-img">
        <img src="<?php echo $sitepath_parinte; ?>img_mediu_hotel/<?php echo $detalii_hotel['poza1']; ?>" width="300" height="300" alt="" class="images" id="poza_principala" />
        <?php if($detalii['last_minute']=='da') { echo '<div class="last-minute"></div>'; } ?>
        <?php if(sizeof($detalii['early_time'])>0) { echo '<div class="early-booking"></div>'; } ?>
      </div>
    <?php } else { ?>
      <div class="single-img"><img src="<?php echo $sitepath_parinte; ?>images/no_photo.jpg" width="300" height="300" alt="" class="images" id="poza_principala" />
      <?php if($detalii['last_minute']=='da') { echo '<div class="last-minute"></div>'; } ?>
        <?php if(sizeof($detalii['early_time'])>0) { echo '<div class="early-booking"></div>'; } ?></div>
    <?php } ?>
    </div>
  </div>
  
  <div class="float-left" style="width:358px;">
    <div class="NEW-gallery">
  <?php
  for($i=1;$i<=8;$i++) {
  if($detalii_hotel['poza'.$i]) { ?>    
      <a href="/img_prima_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" onmouseover="document.getElementById('poza_principala').src='/img_mediu_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>';return false;" class="gallery"><img src="/thumb_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" width="80" alt="" class="images" /></a>
  <?php }
  } ?>
    </div>

    <div class="info-scurte-chn" style="padding-top:10px;">
	  <br />
	  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
      <br class="clear" />
    </div>

  </div>

</div>
<div class="NEW-column-right1">
  
  <div class="info-scurte-new" style="margin-top:38px;">
    <ul>
      <li>Cod oferta: <strong><?php echo 'OCI'.$id_oferta; ?></strong></li>
      <?php if($detalii['nr_nopti']>1) { ?><li>Sejur: <strong><?php echo nopti($detalii['nr_nopti']); ?></strong></li><?php } ?>
      <li>Masa: <strong><?php echo $detalii['masa']; ?></strong></li>
      <li>Transport: <strong><?php echo $detalii['transport']; ?></strong></li>
      <li class="anchor"><a href="#detalii-oferta" class="link-grey" onclick="jQuery('#ofTabs').tabs('select', '#detalii-oferta' );" rel="nofollow">Vezi mai multe detalii</a></li>
    </ul>
    
    <div class="tarif">Tarif de la <span class="pret red"><?php echo new_price($preturi['minim']['pret']); ?></span> <span class="exprimare red"><?php echo moneda($detalii['moneda']); ?></span> pe <?php echo $detalii['exprimare_pret']; ?></div>
    
    <a href="#detalii-oferta" class="vezi-tarife" onclick="jQuery('#ofTabs').tabs('select', '#detalii-oferta' );" rel="nofollow">Vezi toate tarifele</a>
  </div>

</div>

<br class="clear" />

<div class="NEW-column-left1">
  
  <div class="NEW-detalii-oferta">

    <a name="detalii-oferta"></a>
	<?php if($preturi['camera']>0) { ?>  
	  <?php if(sizeof($detalii['early_time'])>0) { ?>
      <br />
      <div class="red">
      <?php foreach($detalii['early_time'] as $keyE=>$valueE) { ?>
        <strong><?php echo $detalii['early_disc'][$keyE]; ?>% REDUCERE</strong> pentru rezervarile confirmate si achitate integral pana la data de <strong><?php echo strftime('%d %B %Y',strtotime($valueE)); ?></strong><br />
      <?php } ?>
      </div>
      <?php } ?>
    </div>

    <?php if(sizeof($preturi['data_start'])>0) { ?>
    <table class="tabel-preturi" style="width:100%;">
      <thead>
      <tr>
        <th class="text-center" style="width:150px;">Plecare</th>
        <?php $g=0; foreach($preturi['camera'] as $key_cam=>$value_cam) { $g++;
		 if(sizeof($preturi['camera'])==$g) { $class='text-center last'; $g=0; } else $class='text-center'; ?>
        <th class="<?php echo $class; ?>"><?php echo $value_cam; ?></th>
        <?php } ?>
      </tr>
      </thead>
      <tbody>
      <?php $nr_t=0; foreach($preturi['data_start'] as $key_d=>$data_start) { $nr_t++;
	  if($nr_t%2==1) $class_tr='impar'; else $class_tr='par';
$numar_minim_nopti=''; ?>
      <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
        <th class="text-center nowrap"><?php echo strftime('%d %B %Y',strtotime($data_start)); ?><br />
        <?php if(sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])>0) { ?><div class="red"><a onmousedown="if(document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display == 'none'){ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'block'; }else{ document.getElementById('alte_preturi<?php echo $key_d; ?>').style.display = 'none'; }" class="pointer">toate preturile</a></div><?php } ?>
        </th>
         <?php $g=0; foreach($preturi['camera'] as $key_cam=>$value_cam) { $g++;
		 if(sizeof($preturi['camera'])==$g) { $class='text-center pret last'; $g=0; } else $class='text-center pret'; ?>
        <td <?php echo 'class="'.$class.'"'; ?>><?php if($preturi['pret'][$key_d][$key_cam]) { echo new_price($preturi['pret'][$key_d][$key_cam]).' '; if($preturi['moneda'][$key_d][$key_cam]=='EURO') { echo '&euro;'; $valuta='da'; } elseif($preturi['moneda'][$key_d][$key_cam]=='USD') { echo '$'; $valuta='da'; } else echo  $preturi['moneda'][$key_d][$key_cam]; } else echo '-'; ?></td>
        <?php } ?>
      </tr>
      <?php if(sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])>0) { ?>
      <tr>
        <td colspan="5" class="text-left last" style="padding:0;"><div id="alte_preturi<?php echo $key_d; ?>" style="display:none; padding-left:164px; background:#d2f6c5;">
        <table class="tabel-preturi" style="width:100%; margin:0;">
          <tbody>
          <tr>
			<?php $sup_i=0; $class_nr=''; foreach($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]] as $key_sup=>$value_sup) { $sup_i++; if($sup_i==sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])) $class_sup='text-center last'; else $class_sup='text-center';  ?>
            <th class="<?php echo $class_sup; if($class_nr) echo ' '.$class_nr; ?>"><?php echo $key_sup; ?></th>
			<?php } ?>
          </tr>
          <tr>
            <?php $sup_i=0; foreach($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]] as $key_sup=>$value_sup) { $sup_i++; if($sup_i==sizeof($preturi['secundar'][$data_start][$preturi['data_end'][$key_d]])) $class_sup='last'; else $class_sup=''; ?>
            <td class="text-center pret <?php echo $class_sup; if($class_nr) echo ' '.$class_nr; ?>"><?php if($value_sup['pret']) { echo new_price($value_sup['pret']).' '; if($value_sup['moneda']=='EURO') echo '&euro;'; elseif($value_sup['moneda']=='USD') echo '$'; else echo  $value_sup['moneda']; } ?></td>
			<?php } ?>
          </tr>
          </tbody>
        </table>
        </div></td>
      </tr>
		<?php } ?>
      <?php } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <div id="test">&nbsp;</div>
    <?php }
	}
	?>

    <div class="grey2 bold" style="text-align:right; padding:0 5px 10px 5px;">* tarifele sunt exprimate in <span class="bigger-13em blue"><?php echo $detalii['moneda']; ?> pe <?php echo $detalii['exprimare_pret']; ?></span></div>

    <?php /*?><div class="NEWrezervareLINE">
	  <div class="call-center">
    	<span class="tit black">Call center Rezervari</span>
		<span class="tel red"><?php echo $contact_telefon; ?></span>
		<span class="prg"><?php echo $contact_program; ?></span>
      </div>
      <?php if($detalii['valabila']=='da') { ?><a href="<?php if($detalii['rezervare_online']=='da') { echo $link_rezervare2; } else { echo $link_rezervare; } ?>" rel="nofollow" class="REZERVA-online-NEW2 float-right<?php if($detalii['rezervare_online']!='da') { echo ' rezervare'; } ?>" style="margin:11px 10px 0 0;"></a><?php } ?>
      <a href="<?php echo $link_cerere_detalii; ?>" rel="nofollow" class="CERE-detalii-NEW2 float-right" style="margin:11px 30px 0 0;"></a>
	  <br class="clear" />
    </div><?php */?>
    
	<?php if($detalii['rezervare_online']=='da' and $detalii['valabila']=='da') {
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/calculare_pret_new.php");
	} elseif($detalii['valabila']=='da') {
		
		include_once($_SERVER['DOCUMENT_ROOT']."/includes/sejururi/cerere_detalii.php");
		
		if(!is_bot() and isset($err_logare_admin) and $detalii['valabila']=='da') {
			erori_disponibilitate($id_oferta, $id_hotel, $plecdata, $pleczile, $adulti, $copii);
		}
		
	} ?>
    
    <br class="clear"><br>
    
	<?php if(sizeof($detalii['data_start_reduceri_speciale'])>'0') { ?>
    <div class="section">
      <h2 class="red underline" style="margin-bottom:5px;"><img src="<?php echo $imgpath; ?>/icon_section_reduceri.png" style="vertical-align:middle;" alt="Reduceri" /> Reduceri speciale <?php echo $detalii_hotel['denumire']; ?></h2>
      <?php foreach($detalii['data_start_reduceri_speciale'] as $jey_r=>$value_r) { ?>
      <p style="text-indent:10px;">Pentru sosirile cuprinse in perioada <strong class="green"><?php echo $value_r.' - '.$detalii['data_end_reduceri_speciale'][$jey_r]; ?></strong> <span class="red">platesti <strong><?php echo $detalii['zile_deaplicare'][$jey_r].' nopti</strong> si stai <strong>'.$detalii['zile_aplicare'][$jey_r].' nopti</strong>'; ?></span></p>
      <?php } ?>
    </div>
    <?php } ?>
    
    <?php if(sizeof($detalii['denumire_v3'])>'0' || sizeof($detalii['denumire_v4'])>'0' || sizeof($excursii)>'0') { ?>
    <div class="section">
      <?php /*?><h2 class="blue underline"><img src="<?php echo $imgpath; ?>/icon_section_oferta.png" style="vertical-align:middle;" alt="Descriere oferta" /> Informatii suplimentare oferta <?php echo $detalii_hotel['denumire']?></h2><?php */?>
      <?php /*?><?php if(sizeof($detalii['denumire_v3'])>'0') { ?>
      <h3 class="black">Suplimente</h3>
      <div class="content">
        <ul>
        <?php foreach($detalii['denumire_v3'] as $key=>$value) { ?>
        <li><?php echo ucfirst($value); if($detalii['value_v3'][$key]) { echo ' - <strong>'.$detalii['value_v3'][$key].' '.moneda($detalii['moneda_v3'][$key]).' '.$detalii['exprimare_v3'][$key].' '.$detalii['pasager_v3'][$key].'</strong>'; } if($detalii['obligatoriu_v3'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>'; else echo ' - <em class="blue">Optional</em>'; ?></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?><?php */?>
	  <?php if(sizeof($detalii['denumire_v4'])>'0') { ?>
      <h3 class="red">Reduceri</h3>
      <div class="content">
        <ul>
        <?php foreach($detalii['denumire_v4'] as $key=>$value) { ?>
        <li><?php echo ucfirst($value); if($detalii['value_v4'][$key]) { echo ' - <strong>'.$detalii['value_v4'][$key].' '.moneda($detalii['moneda_v4'][$key]).'</strong>'; } ?></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
	  <?php $excursii=$det->get_excursii($id_oferta);
	  if(sizeof($excursii)>0) { ?>
      <h3 class="red">Excursii optionale</h3>
      <div class="content">
        <ul>
        <?php foreach($excursii as $key_e=>$value_e) { ?>
        <li><a href="<?php echo $sitepath.'excursii-optionale/'.fa_link($detalii_hotel['tara']).'/'.fa_link_oferta($value_e['denumire']).'-'.$value_e['id_excursie'].'.html'; ?>" rel="nofollow" class="excursii_detalii link-blue"><?php echo $value_e['denumire']; if($value_e['pret']) { echo ' - <em class="red">'.$value_e['pret'].' '; echo moneda($value_e['moneda']); echo '</em> - '.ucwords($value_e['tip']); } else echo ' - <em class="red bold">'.ucwords($value_e['tip']).'</em>'; ?></a></li>
        <?php } ?>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
    </div>
	<?php } ?>

    <br class="clear" />

    <div style="font-size:14px; line-height:22px;"><?php afisare_frumos(schimba_caractere($detalii_hotel["descriere"]), 'nl2p'); ?></div>
    
	<br class="clear" />

    <?php if($detalii["descriere_oferta"] || $row_of['hoteluri']) { ?>
    <div class="section">
      <h2 class="section-title" style="margin-bottom:5px;"><?php echo $detalii_hotel["denumire"]; ?></h2>
      <?php afisare_frumos(schimba_caractere($detalii["descriere_oferta"]), 'nl2p');
if($row_of['hoteluri']) {
$selH="select
hoteluri_viz.id_hotel,
hoteluri_viz.nume, 
hoteluri_viz.stele, 
hoteluri_viz.poza1,
hoteluri_viz.descriere,
tari.denumire as denumire_tara,
localitati.denumire as denumire_localitate
from 
oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
inner join hoteluri as hoteluri_viz on traseu_circuit.id_hotel = hoteluri_viz.id_hotel
Inner Join localitati ON hoteluri_viz.locatie_id = localitati.id_localitate
inner join zone on localitati.id_zona = zone.id_zona
inner join tari on zone.id_tara = tari.id_tara
where
oferte.valabila =  'da' and
hoteluri.tip_unitate = 'Circuit' and
oferte.id_oferta = '".$id_oferta."'
Group by hoteluri_viz.id_hotel
Order by hoteluri_viz.nume ";
$queH=mysql_query($selH) or die(mysql_error());
while($rowH=mysql_fetch_array($queH)) {
$link=$sitepath.'hoteluri/'.fa_link($rowH['denumire_tara']).'/'.fa_link($rowH['denumire_localitate']).'/'.fa_link_oferta($rowH['nume']).'-'.$rowH['id_hotel'].'.html'; ?>
    <div class="NEW-chn-oferta NEW-chn-oferta-red">
      <a href="<?php echo $link; ?>"><img src="<?php echo $sitepath.'thumb_hotel/'.$rowH['poza1']; ?>" class="icon" alt="<?php echo $rowH['nume']; ?>" /></a>
      <h2><a href="<?php echo $link; ?>" class="red" title="<?php echo $rowH['nume']; ?>"><?php echo $rowH['nume']; ?> <img src="/images/spacer.gif" class="stele-mari-<?php echo $rowH['stele']; ?>" alt="" /></a></h2>
      <p><?php echo $rowH['descriere']; ?></p>
      <div class="clear"></div>
    </div>
<?php } @mysql_free_result($queH);
} ?>
    </div>
    <br class="clear" />
	<?php } ?>
  
    <div class="section">
	  <?php if($detalii['conditii_plata']) { ?>
      <h3 class="black">Conditii de plata</h3>
      <div class="content">
        <ul>
          <li><?php echo afisare_frumos($detalii['conditii_plata'], 'nl2br'); ?></li>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
	  <?php if($detalii['conditii_anulare']) { ?>
      <h3 class="black">Conditii de anulare</h3>
      <div class="content">
        <ul>
          <li><?php echo afisare_frumos($detalii['conditii_anulare'], 'nl2br'); ?></li>
        </ul>
      </div>
      <div class="clear-line"></div>
      <?php } ?>
    </div>
    
    <?php $autocar=array(); if($detalii['transport']=='Autocar') $autocar=$det->get_autocar_sejur($id_oferta);
	if(sizeof($autocar)>0) { ?>     
    <div class="section">
      <h2 class="section-title">Orar transport</h2>
      <table class="tabel-transport" style="width:100%;">
        <thead>
        <tr>
          <th class="text-center" style="width:180px;">Localitate Plecare</th>
          <th class="text-center" style="width:120px;">Ora Plecare</th>
          <th class="text-center">Detalii</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($autocar as $key_a=>$value_a) { ?>
        <tr>
          <td class="text-left"><?php echo $value_a['denumire_localitate']; ?></td>
          <td class="text-center"><?php echo $value_a['ora']; ?></td>
          <td class="text-left"><?php echo $value_a['detalii']; ?></td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>

    <br class="clear" />
    <?php } ?>    

    <br />

    <div class="chenar-info NEW-orange NEW-round8px">Continutul ofertei este valabil la data introducerii acesteia. Datorita specificului industriei de turism ofertele se pot schimba foarte des si nu pot fi actualizate in timp util.</div>

    <br class="clear" /><br />
    
 <?php /*  <?php $oferte=$det->get_oferte($id_hotel, $id_oferta);
	if(sizeof($oferte)>0)
	 { ?>
    <h2 class="section-title" style="margin-bottom:10px;">Alte oferte la <?php echo $detalii_hotel['denumire']; ?></h2>
    <br class="clear" />
		<?php foreach($oferte as $key_of=>$value_of) { $c++;
	$link=$sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link($value_of['denumire']).'-'.$value_of['id_oferta'].'.html';
	$tipuri=explode('+',$value_of['poza_tip']); ?>
  <div class="NEW-chn-oferta NEW-chn-oferta-<?php if($tipuri[1]) echo $tipuri[1]; else echo 'red'; ?>">
    <a href="<?php echo $link; ?>"><img src="<?php if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/oferte/icon_tematica_mare_'.$tipuri['0'].'.png')) echo $imgpath.'/oferte/icon_tematica_mare_'.$tipuri['0'].'.png'; else echo $imgpath.'/oferte/icon_tematica_mare_default.png'; ?>" class="icon" alt="<?php echo $value_of['denumire']; ?>" /></a>
    <div class="text-center right">
      <span><?php $pret=pret_minim_sejur($value_of['id_oferta'], '', '', ''); if($pret['pret']) { echo $pret['pret']; if($pret['moneda']=='EURO') echo '&euro;'; elseif($$pret['moneda']=='USD') echo '$'; else echo $pret['moneda']; } ?></span>
      <a href="<?php echo $link; ?>"><img src="<?php echo $imgpath; ?>/buton_vezi_detalii_oferta.gif" alt="vezi detalii Oferta <?php echo $value_of['denumire']; ?>" /></a>
    </div>
    <h2><a href="<?php echo $link; ?>" class="red" title="<?php echo $value_of['denumire']; ?>"><?php echo $value_of['denumire']; ?></a></h2>
    <p>Oferta cu <strong>Transport <?php echo $value_of['denumire_transport']; ?></strong> si <strong>Masa <?php echo $value_of['masa']; ?></strong> exprimata <strong><?php echo $value_of['exprimare_pret']; ?></strong></p>
    <img src="<?php echo $imgpath; ?>/oferte/icon_transport_<?php echo strtolower($value_of['denumire_transport']); ?>.gif" alt="Transport <?php echo $value_of['denumire_transport']; ?>" title="Transport <?php echo $value_of['denumire_transport']; ?>" class="transport vtip" />
    <div class="clear"></div>
  </div>

  <?php if($c==4) $c=0; }
  } ?>
*/?>
</div>
<div class="NEW-column-right1">

  <div class="right-serv" style="margin:7px 0 0 0;">

	<?php if(sizeof($detalii['denumire_v1'])>'0') { ?>
    <?php if($afisare_servicii=='da') { ?>
    <div>
      <h3 class="black underline">Tarifele includ</h3>
      <ul>
      <?php foreach($detalii['denumire_v1'] as $key=>$value) { ?>
      <li><?php echo schimba_caractere(ucfirst($value)); if($detalii['value_v1'][$key]) { echo ' - <strong>'.$detalii['value_v1'][$key].' '.moneda($detalii['moneda_v1'][$key]).'</strong>'; } ?></li>
      <?php } ?>
      </ul>
    </div>
    <?php } ?>
	<?php } ?>

	<?php if(sizeof($detalii['denumire_v2'])>'0' or sizeof($detalii['denumire_v3'])>'0') { ?>
    <div <?php if($afisare_servicii=='da') echo 'class="separator"'; ?>>
      <h3 class="black underline">Neincluse in tarif</h3>
      <ul>
      <?php if(sizeof($detalii['denumire_v2'])>'0') {
		  foreach($detalii['denumire_v2'] as $key=>$value) {
			  if($afisare_servicii=='da' or ($afisare_servicii=='nu' and $detalii['obligatoriu_v2'][$key]!='da')) { ?>
      <li><?php echo schimba_caractere(ucfirst($value));
          if($detalii['data_start_v2'][$key]) {
              echo ' <em>('.date("d.m.Y", strtotime($detalii['data_start_v2'][$key])).' - '.date("d.m.Y", strtotime($detalii['data_end_v2'][$key])).')</em>';
          }
          if($detalii['value_v2'][$key]) {
              echo ' - <strong>'.$detalii['value_v2'][$key].' '.moneda($detalii['moneda_v2'][$key]).' '.$detalii['exprimare_v2'][$key].' '.$detalii['pasager_v2'][$key].'</strong>';
          }
          if($detalii['obligatoriu_v2'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>';
              else echo ' - <em class="blue">Optional</em>'; ?></li>
      <?php }
		  }
	  } ?>
	  <?php if(sizeof($detalii['denumire_v3'])>'0') {
		  foreach($detalii['denumire_v3'] as $key=>$value) {
			  if($afisare_servicii=='da' or ($afisare_servicii=='nu' and $detalii['obligatoriu_v3'][$key]!='da')) { ?>
      <li><?php echo schimba_caractere(ucfirst($value));
          if($detalii['data_start_v3'][$key]) {
              echo ' <em>('.date("d.m.Y", strtotime($detalii['data_start_v3'][$key])).' - '.date("d.m.Y", strtotime($detalii['data_end_v3'][$key])).')</em>';
          }
          if($detalii['value_v3'][$key]) {
              echo ' - <strong>'.$detalii['value_v3'][$key].' '.moneda($detalii['moneda_v3'][$key]).' '.$detalii['exprimare_v3'][$key].' '.$detalii['pasager_v3'][$key].'</strong>';
          }
          if($detalii['obligatoriu_v3'][$key]=='da') echo ' - <em class="red bold">Obligatoriu</em>';
              else echo ' - <em class="blue">Optional</em>'; ?></li>
      <?php }
		  }
	  } ?>
      </ul>
    </div>
    <?php } ?>

	<?php $aeroport=array(); if($detalii['transport']=='Avion' or $detalii['transport']=='Avion si Autocar local') $aeroport=$det->get_avion_sejur($id_oferta);
    if(sizeof($aeroport)>0) { ?>
    <div class="separator">
      <h3 class="black underline">Orar de zbor</h3>
      <div style="padding:5px 0;">
        <img src="/images/avion/<?php echo $aeroport[0]['companie']; ?>.jpg" alt="<?php echo $aeroport[0]['denumire_companie']; ?>" style="margin:0 5px 0 13px; vertical-align:middle;"> <strong class="underline"><?php echo $aeroport[0]['denumire_companie']; ?></strong>
        <?php $tip_c=''; $tip_v=''; $i=0; foreach($aeroport as $key_a=>$value_a) { $i++; ?>
          <?php if($tip_c<>$value_a['tip']) { ?>
          <?php /*?><span class="black bigger-11em bold">- <?php echo strtoupper($value_a['tip']); ?></span><?php */?>
          <?php $tip_c=$value_a['tip']; $tip_v=$value_a['tip']; } ?>
          <ul style="margin-bottom:5px;">
            <li><strong>Plecare:</strong> <?php echo $value_a['denumire_aeroport_plecare'].' ('.$value_a['denumire_loc_plecare'].') - <strong>'.$value_a['ora_plecare'].'</strong>'; ?></li>
            <li><strong>Sosire:</strong> <?php echo $value_a['denumire_aeroport_sosire'].' ('.$value_a['denumire_loc_sosire'].') - <strong>'.$value_a['ora_sosire'].'</strong>'; ?></li>
          </ul>
          <?php if($i<sizeof($aeroport)) echo '&nbsp;&nbsp;&nbsp; --------'; ?>
        <?php } ?>
      </div>
    </div>
    <?php } ?>

	<?php if($detalii['nota']) { ?>
    <div class="separator">
      <h3 class="black underline">Nota</h3>
      <div class="italic" style="padding:5px 13px;">
        <?php afisare_frumos(schimba_caractere(trim($detalii['nota'])), 'nl2br'); ?>
      </div>
    </div>
    <?php } ?>
    
  </div>
  
  <br class="clear"><br>
  
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/new_circuit.php"); ?>
  <?php if(isset($_GET['plecdata'])) include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/addins_dreapta_new.php"); ?>
  
  <?php /* if(isset($id_oferta) and ($GLOBALS['tip_unitate']=='Circuit')) { ?>
  <div class="ofdreapta NEW-round8px">
	<?php if($tari_principale) { ?>
    <h3 class="blue">Alte circuite in <?php echo $tari_principale; ?></h3>
	<?php $tari=explode(',',$tari_principale);
	$template=$_SERVER['DOCUMENT_ROOT']."/templates/afisare_circuit_coloana_dreapta.tpl";
	$circuite_dr=new AFISARE_SEJUR();
	$circuite_dr->setCircuit('da');
	$circuite_dr->setOfertaSpeciala($template);
	$circuite_dr->set_oferte_pagina(5);
	$circuite_dr->setExceptie($id_oferta);
	$circuite_dr->setTariCircuit($tari);
	$circuite_dr->fara_paginare();
	unset($circuite_dr);
    } ?>
  </div>
  <br class="clear"><br>
  <?php } */?>


</div>

</div>
<br class="clear" />
