<div class="filters NEW-round6px">
  <?php /*?><div class="blue" align="justify">Pentru o navigare mai usoara, folositi <span class="bold">filtrele de mai jos</span>:</div><?php */?>
  
  <div class="titlu blue">Filtrare dupa:</div>

<?php if($id_transport || $nr_zile || $din_luna || $_GET['early-booking']=='da') { ?>
  <a href="<?php echo $link_p; ?>" class="delete" rel="nofollow"><span>X</span> Sterge toate filtrele</a>
  <ul class="delete-filters black">
    <?php
    if($checkin) echo '<li>check-in <strong>'.date("d.m.Y", strtotime($checkin)).'</strong></li>';
	if($early) echo '<li>early booking</li>';
    if($din_luna) echo '<li>'.$plecare.'</li>';
    if($id_transport) echo '<li>'.$trans.'</li>';
    if($nr_zile) echo '<li>'.$nr_zile.' zile</li>';
	?>
  </ul>
  <br>
<?php } ?>

  <div class="item">
    <div class="heading">Tara</div>
    <select name="localitate" id="localitate" onchange="filtrare_get_nou_circuite('<?php echo $indice; ?>', this.value);">
      <option value="">Selectati</option>
<?php
$sel_tara="SELECT
tari.denumire,
continente.nume_continent
FROM oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
INNER JOIN continente ON continente.id_continent = tari.id_continent ";
if($id_tip) $sel_tara=$sel_tara." inner join oferta_sejur_tip on (oferte.id_oferta = oferta_sejur_tip.id_oferta and oferta_sejur_tip.id_tip_oferta = '".$id_tip."') ";
$sel_tara=$sel_tara." WHERE
oferte.valabila =  'da' AND
hoteluri.tip_unitate = 'Circuit'
and traseu_circuit.tara_principala = 'da'
GROUP BY tari.denumire
ORDER BY tari.denumire ASC ";
$que_tara=mysql_query($sel_tara) or die(mysql_error());
while($row_tara=mysql_fetch_array($que_tara)) {
$link_z = /*fa_link($row_tara['nume_continent']).'/'.*/fa_link($row_tara['denumire']); ?>
	  <option value="<?php echo $link_z; ?>" <?php if($link_z==/*$link_continent.'/'.*/$link_tara) echo 'selected'; ?>><?php echo $row_tara['denumire']; ?></option>
<?php } @mysql_free_result($que_tara); ?>
	</select>
  </div>

<?php $tipuri=get_tiputi_circuite('', /*$id_continent,*/ $id_tara, '', ''); $i=0;
if(sizeof($tipuri)>0) { ?>
  <div class="item">
	<div class="heading">Ocazii speciale</div>
	<ul>
	  <li><input type="checkbox" onclick="document.location.href='<?php echo $sitepath.'circuite/'; /*if($link_continent) echo $link_continent.'/';*/ if($link_tara) echo $link_tara.'/'; ?>';" id="tip0" name="evenimente" value="toate" <?php if(!$_REQUEST['tip']) { ?> checked="checked" <?php } ?> /> <label for="tip0" <?php if(!$_REQUEST['tip']) echo 'class="sel"'; ?>>Toate</label></li>
	  <?php foreach($tipuri as $key_t=>$value_t) {
		  $i++;
		  $valTip=fa_link($value_t['denumire']); ?>
      <li><input type="checkbox" onclick="document.location.href='<?php echo $sitepath.'circuite-'.$valTip.'/'; /*if($link_continent) echo $link_continent.'/';*/ if($link_tara) echo $link_tara.'/'; ?>';" id="tip<?php echo $i; ?>" name="evenimente" value="<?php echo $valTip; ?>" <?php if($valTip==$_REQUEST['tip']) { ?> checked="checked" <?php } ?> /> <label for="tip<?php echo $i; ?>" <?php if($valTip==$_REQUEST['tip']) echo 'class="sel"'; ?>><?php echo $value_t['denumire']; ?></label></li>
       <?php } ?>
    </ul>
  </div>
<?php } ?>
  
<?php $link_p1=$link_p.'?optiuni=da'; ?>

<?php if($_REQUEST['data-plecare']) $link_p1=$link_p1.'&data-plecare='.$_REQUEST['data-plecare']; ?>
  <div class="item">
  <div class="heading">Luna plecarii</div>
  <ul>
    <li class="label"><select name="luni_plecari" id="luni_plecari" onchange="document.location.href='<?php echo $link_p1; ?>&data-plecare='+this.value;">
      <option value="">Alege inceperea circuitului</option>
      <?php if(sizeof($luni_plecari)>0) {
	   foreach($luni_plecari as $value) {
		$lun=explode('-', $value);
		$link_z=$value; ?>
      <option value="<?php echo $link_z; ?>" <?php if($link_z==$din_luna) { ?> selected="selected" <?php } ?>><?php echo $luna[$lun[0]].' '.$lun[1]; ?></option>
      <?php }
	  } ?>
    </select></li>
  </ul>
  </div>
  
  <?php /*?><div class="item">
	<div class="heading">Reduceri</div>
    <ul>
      <li><input type="checkbox" onclick="document.location.href='<?php echo $sitepath.'circuite/'; if($link_continent) echo $link_continent.'/'; if($link_tara) echo $link_tara.'/'; ?>';" id="tipE0" name="early" value="toate" <?php if(!$_GET['early-booking']) { ?> checked="checked" <?php } ?> /> <label for="tipE0" <?php if(!$_GET['early-booking']) echo 'class="sel"'; ?>>Toate</label></li>
      <li><input type="checkbox" onclick="document.location.href='<?php echo $link_p1.'&amp;early-booking=da'; ?>';" id="tipE1" name="erly1" value="da" <?php if($_GET['early-booking']=='da') { ?> checked="checked" <?php } ?> /> <label for="tipE1" <?php if($_GET['early-booking']=='da') echo 'class="sel"'; ?>>Early Booking</label></li>
    </ul>
  </div><?php */?>

<?php if($_GET['early-booking']) $link_p1=$link_p1.'&early-booking=da'; if(sizeof($filtruOf['trans'])>0) { ?>
  <?php /*?><div class="item">
    <div class="heading">Transport</div>
	<?php krsort($filtruOf['trans']); ?>
    <ul>
      <li <?php if(sizeof($filtruActual['trans'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['trans'])==0) echo 'disabled="disabled"'; ?> id="optiune0" name="transport[]" <?php if($transport=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_circuite(<?php echo "'".$link_p1; echo "'"; ?>, this.value, <?php echo "'".$nr_zile."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiune0" <?php if($transport=='toate') echo 'class="sel"'; ?>>Toate</label></li>
      <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) {
		  $i++;
		  $lingTr=fa_link($key);
		  if($value) $nr=$value; else $nr=0; ?>
      <li <?php if(!$filtruActual['trans'][$key]) echo 'class="disabled"'; ?>><input type="checkbox"  <?php if(!$filtruActual['trans'][$key]) echo 'disabled="disabled"'; ?> name="transport[]" id="optiune<?php echo $i; ?>" <?php if($lingTr==$transport) { ?> checked="checked" <?php } ?> value="<?php echo $lingTr; ?>" onclick="if(this.checked) var tr=this.value; else var tr='toate'; filtrare_circuite(<?php echo "'".$link_p1."'"; ?>, tr, <?php echo "'".$nr_zile."'"; ?>, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiune<?php echo $i; ?>" <?php if($lingTr==$transport) echo 'class="sel"'; ?>><?php echo $key; ?></label>
      <?php if($key=='Avion') { ?>
      <select name="oras_plecare" onchange="filtrare_circuite(<?php echo "'".$link_p1; echo "'"; ?>, 'avion', <?php echo "'".$nr_zile."'"; ?>, this.value);" class="oras-plecare">
		<option value="toate" <?php if(sizeof($filtruActual['plecare_avion'])==0) echo 'disabled="disabled"'; if($plecare_avion=='toate') { ?> selected="selected" <?php } ?>>Alegeti orasul plecarii</option>
		<?php if(sizeof($filtruOf['plecare_avion'])) {
			krsort($filtruOf['plecare_avion']);
			foreach($filtruOf['plecare_avion'] as $key=>$value) {
				$lingTr=fa_link($key); ?>
        <option value="<?php echo $lingTr; ?>" <?php if(!$filtruActual['plecare_avion'][$key]) echo 'disabled="disabled"'; if($lingTr==$plecare_avion || ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) { ?> selected="selected" <?php } ?>><?php echo $key; ?></option>
		<?php }
		} ?>
      </select>
	  <?php } ?>
      </li>
    <?php } ?>
    </ul>
  </div><?php */?>
  <div class="item">
  <div class="heading">Transport</div>
  <?php krsort($filtruOf['trans']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters_circuite($link_p1, 'toate', $nr_zile, $plecare_avion); ?>" rel="nofollow" <?php if($transport=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php $i=0; foreach($filtruOf['trans'] as $key=>$value) {
		$i++;
		$lingTr=fa_link($key);
		if($value) $nr=$value; else $nr=0;
		if($transport==$lingTr) {
			$tr=$lingTr;
			$selclass='class="sel"';
		} else {
			$tr=$lingTr;
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters_circuite($link_p1, $tr, $nr_zile, $plecare_avion); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo str_replace(" si ", " + ", $key); if($key=='Avion') echo ' + Autocar local'; ?> <?php echo '('.$nr.')'; ?></a>
      <?php if($key=='Avion' or $key=='Avion si Autocar local') { ?>
      <select name="oras_plecare" onchange="filtrare_circuite(<?php echo "'".$link_p1; echo "'"; ?>, '<?php echo fa_link($key); ?>', <?php echo "'".$nr_zile."'"; ?>, this.value);" class="oras-plecare">
        <option value="toate" <?php if(sizeof($filtruActual['plecare_avion'])==0) echo 'disabled'; if($plecare_avion=='toate') echo 'selected'; ?>>Alegeti orasul plecarii</option>
        <?php if(sizeof($filtruOf['plecare_avion'])) {
			krsort($filtruOf['plecare_avion']);
			foreach($filtruOf['plecare_avion'] as $key=>$value) {
				$lingTr=fa_link($key);
		?>
        <option value="<?php echo $lingTr; ?>" <?php if($lingTr==$plecare_avion || ($transport=='avion' && sizeof($filtruOf['plecare_avion'])==1)) echo 'selected'; ?>><?php echo $key; ?></option>
        <?php }
		} ?>
      </select>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>

<?php if(sizeof($filtruOf['nr_zile'])>0) { ?>
  <?php /*?><div class="item">
    <div class="heading">Durata</div>
    <?php ksort($filtruOf['nr_zile']); ?>
    <ul>
      <li <?php if(sizeof($filtruActual['nr_zile'])==0) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(sizeof($filtruActual['nr_zile'])==0) echo 'disabled="disabled"'; ?> id="optiunes0" name="nr_zile[]" <?php if($nr_zile=='toate') { ?> checked="checked" <?php } ?> value="toate" onclick="filtrare_circuite(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, this.value, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiunes0" <?php if($nr_zile=='toate') echo 'class="sel"'; ?>>Toate</label></li>
      <?php foreach($filtruOf['nr_zile'] as $i=>$nr_s) {
	  if($nr_s) $nrS=$nr_s; else $nrS=0; ?>
      <li <?php if(!$filtruActual['nr_zile'][$i]) echo 'class="disabled"'; ?>><input type="checkbox" <?php if(!$filtruActual['nr_zile'][$i]) echo 'disabled="disabled"'; ?> id="optiunes<?php echo $i; ?>" name="nr_zile[]" value="<?php echo $i; ?>" <?php if($i==$nr_zile) { ?> checked="checked" <?php } ?> onclick="if(this.checked) var st=this.value; else var st='toate'; filtrare_circuite(<?php echo "'".$link_p1."'"; ?>, <?php echo "'".$transport."'"; ?>, st, <?php echo "'".$plecare_avion."'"; ?>);" /> <label for="optiunes<?php echo $i; ?>" <?php if($i==$nr_zile) echo 'class="sel"'; ?>><?php echo $i; if($i>1) echo ' zile '; else echo ' zi '; ?></label></li>
	  <?php } ?>
    </ul>
  </div><?php */?>
  
  <div class="item">
  <div class="heading">Durata</div>
  <?php ksort($filtruOf['nr_zile']); ?>
  <ul>
    <li class="label"><a href="<?php echo make_link_filters_circuite($link_p1, $transport, 'toate', $plecare_avion); ?>" rel="nofollow" <?php if($zile=='toate') echo 'class="sel"'; ?>>Toate</a></li>
    <?php foreach($filtruOf['nr_zile'] as $i=>$nr_z) {
		if($nr_z) $nrZ=$nr_z; else $nrS=0;
		if(is_array($zile)) {
			if(in_array($i,$zile)) {
				$zl=remove_current_filter($nr_zile,$i);
				$selclass='class="sel"';
			} else {
				$zl=$nr_zile.','.$i;
				$selclass='';
			}
		} else {
			$zl=$i;
			$selclass='';
		}
	?>
    <li class="label"><a href="<?php echo make_link_filters_circuite($link_p1, $transport, $zl, $plecare_avion); ?>" <?php echo $selclass; ?> rel="nofollow"><?php echo $i; if($i>1) echo ' zile '; else echo ' zi '; ?> <?php echo '('.$nrZ.')'; ?></a></li>
    <?php } ?>
  </ul>
  </div>
<?php } ?>

</div>

<br class="clear"><br>

<?php /*?><div style="width:210px; height:430px; overflow:hidden; border:1px solid #CCC;" class="NEW-round6px"><div class="fb-like-box" data-href="<?php echo $facebook_page; ?>" data-width="210" data-height="457" data-show-faces="true" data-stream="false" data-show-border="false" data-header="true"></div></div>

<br class="clear"><br><?php */?>

<?php /*?><a href="<?php echo $sitepath; ?>newsletter.html" title="Abonare Newsletter" rel="infoboxes"><img src="<?php echo $imgpath; ?>/banner/abonare-newsletter-small.png" alt="Abonare Newsletter" /></a>

<p>&nbsp;</p><?php */?>
