<?php
$link_of_pag = $link;
if ( $_REQUEST['ordonare'] ) {
	$link = $link . "&ordonare=" . $_REQUEST['ordonare'];
}
if ( $link == '?optiuni=da' ) {
	$link = '';
}
$oferte = new AFISARE_SEJUR_NORMAL_RESPONSIVE();
$oferte->setAfisare( $from, $nr_pe_pagina );
$oferte->setCircuit( 'da' );
$oferte->setTariCircuit( array( 'tari' => $tara ) );
if ( $early == 'da' ) {
	$oferte->setEarly( 'da' );
}
if ( $id_tip ) {
	$oferte->setTipOferta( $_REQUEST['tip'] );
}
if ( $_REQUEST['ordonare'] ) {
	$tipO = explode( '-', $_REQUEST['ordonare'] );
	if ( $tipO[0] == 'tip_pret' ) {
		$oferte->setOrdonarePret( $tipO[1] );
	} elseif ( $tipO[0] == 'tip_numH' ) {
		$oferte->setOrdonareNumeH( $tipO[1] );
	}
}
if ( $_REQUEST['transport'] && $_REQUEST['transport'] <> 'toate' ) {
	$oferte->setTransport( $_REQUEST['transport'] );
}
if ( $nr_zile ) {
	$oferte->setDurata( $nr_zile );
}
if ( $din_luna ) {
	$oferte->setLunaPlecare( $din_luna );
}
if ( $id_loc_plecare_av ) {
	$oferte->setPlecareAvion( $id_loc_plecare_av );
}
$oferte->initializeaza_pagini( $link_p, 'pag-###/', $link );
$nr_hoteluri = $oferte->numar_oferte();
?>

<div class="circuit">
    <h1 class="blue"><?php echo $titlu_pag; ?></h1>
	<?php /* TODO: add share button
    <div class="float-right"><?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/socials_top.php" ); ?></div>
*/ ?>
</div>

<div class="sejururi show-mobile">
    <button class="show-filters">
        <span>Filtreaza</span>
        <span class="hide">Inchide filtre</span>
    </button>
    <script>
        $('.show-filters').on('click', function () {
            $(this).find('span').toggleClass('hide');
            $('.filter-wrapper').toggleClass('opened');
            $('.result-list').toggle('slow');
        });
    </script>
</div>

<div class="filter-wrapper">
    <div class="circuit no-margins-mobile">
        <div class="titlu blue">Ordoneaza dupa:</div>
        <div class="item">
            <ul>
                <li class="label">
                    <a<?php echo ! $_REQUEST['ordonare'] ? ' class="sel"' : '' ?>
                            onclick="document.location.href='<?php echo $link_of_pag . '&amp;ordonare=data-introducere' ?>';">
                        Data introducere
                    </a>
                </li>
                <li class="label">
                    <a<?php echo ( $tipO[0] == 'tip_pret' && $tipO[1] == 'asc' ) ? ' class="sel"' : '' ?>
                            onclick="document.location.href='<?php echo $link_of_pag . '&amp;ordonare=tip_pret-asc' ?>';">
                        Pret crescator
                    </a>
                </li>
                <li class="label">
                    <a<?php echo ( $tipO[0] == 'tip_pret' && $tipO[1] == 'desc' ) ? ' class="sel"' : '' ?>
                            onclick="document.location.href='<?php echo $link_of_pag . '&amp;ordonare=tip_pret-desc' ?>';">
                        Pret descrescator
                    </a>
                </li>
                <?php /*
                <li class="label">
                    <a<?php echo ( $tipO[0] == 'tip_numH' && $tipO[1] == 'asc' ) ? ' class="sel"' : '' ?>
                            onclick="document.location.href='<?php echo $link_of_pag . '&amp;ordonare=tip_numH-asc' ?>';">
                        Nume hotel crescator
                    </a>
                </li>
                <li class="label">
                    <a<?php echo ( $tipO[0] == 'tip_numH' && $tipO[1] == 'desc' ) ? ' class="sel"' : '' ?>
                            onclick="document.location.href='<?php echo $link_of_pag . '&amp;ordonare=  tip_numH-desc' ?>';">
                        Nume hotel descrescator
                    </a>
                </li> */ ?>
            </ul>
        </div>
    </div>
    <div class="circuit">
		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . '/includes/circuite/filtru_stanga_responsive.php' ); ?>
    </div>
</div>

<div class="circuit result-list">
    <div>
		<?php $oferte->afiseaza();
		if ( $nr_hoteluri > 0 ) {
			$filtruOf     = $oferte->get_filtru_mare();
			$filtruActual = $oferte->get_filtru();
		} ?>
    </div>
</div>
<div class="clear"></div>
<div class="circuit">
    <h2 class="black">Vezi alte circuite</h2>
    <div class="item">
		<?php
		$sel_tara = "SELECT
tari.denumire,
continente.nume_continent
FROM oferte
inner join hoteluri on oferte.id_hotel = hoteluri.id_hotel
Inner Join traseu_circuit ON hoteluri.id_hotel = traseu_circuit.id_hotel_parinte
Inner Join tari ON traseu_circuit.id_tara = tari.id_tara
INNER JOIN continente ON continente.id_continent = tari.id_continent ";
		if ( $id_tip ) {
			$sel_tara = $sel_tara . " inner join oferta_sejur_tip on (oferte.id_oferta = oferta_sejur_tip.id_oferta and oferta_sejur_tip.id_tip_oferta = '" . $id_tip . "') ";
		}
		$sel_tara = $sel_tara . " WHERE
oferte.valabila =  'da' AND
hoteluri.tip_unitate = 'Circuit'
and traseu_circuit.tara_principala = 'da'
GROUP BY tari.denumire
ORDER BY tari.denumire ASC ";
		$que_tara = mysql_query( $sel_tara ) or die( mysql_error() );
		?>
        <div class="location-filters">
            <ul>
				<?php while ( $row_tara = mysql_fetch_array( $que_tara ) ) : ?>
					<?php $link_z = fa_link( $row_tara['denumire'] ); ?>
                    <li class="<?php echo $link_z == $link_tara ? "selected" : '' ?>">
                        <a onclick="filtrare_get_nou_circuite('<?php echo $indice; ?>', '<?php echo $link_z ?>');"><?php echo $row_tara['denumire']; ?></a>
                    </li>
				<?php endwhile; ?>
				<?php @mysql_free_result( $que_tara ); ?>
            </ul>
        </div>
    </div>
</div>