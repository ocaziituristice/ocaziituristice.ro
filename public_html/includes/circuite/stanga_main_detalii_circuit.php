<script src="/js/functii.js"></script>
<?php
$id_circuit=$_REQUEST['id_circuit'];

$select="SELECT
continente.nume_continent,
circuite.id_utilizator,
circuite.denumire,
circuite.nr_zile,
circuite.nume_poza1,
circuite.poza1,
circuite.nume_poza2,
circuite.poza2,
circuite.nume_poza3,
circuite.poza3,
circuite.valabila,
info_circuite.descrierea_circuitului,
info_circuite.obiective_turistice,
info_circuite.serv_incluse,
info_circuite.serv_neincluse,
info_circuite.alte_detalii,
info_circuite.detalii_plecari,
info_circuite.detalii_preturi,
transport.denumire as denumire_transport,
usernames.sigla,
usernames.id as id_agentie,
usernames.denumire_utilizator as nume_agentie,
usernames.mail as mail_agentie
FROM
continente
Inner Join circuite ON continente.id_continent = circuite.id_continent
Inner Join info_circuite ON circuite.id_circuit = info_circuite.id_circuit
Inner Join usernames ON circuite.id_utilizator = usernames.id
left join transport on info_circuite.transport = transport.id_trans
WHERE
circuite.id_circuit =  '".$id_circuit."'
";
$rez=mysql_query($select);

$row=mysql_fetch_array($rez);
@mysql_free_result($rez);

$id_user=$row["id_utilizator"];
$denumire=$row["denumire"];
?>

<?php
$sel_date="
SELECT
usernames.denumire_utilizator,
usernames.mail,
usernames.tara,
usernames.adresa,
usernames.telefon,
usernames.fax,
usernames.site_web,
usernames.localitate,
judete.denumire_judet
FROM
usernames
Left Join judete ON usernames.id_judet = judete.id_judet
WHERE
usernames.id =  '$id_user'
";

$rez_date=mysql_query($sel_date);
$row_date=mysql_fetch_array($rez_date);
@mysql_free_result($rez_date);
?>    

<div id="detaliiOferta">
<? if($row['valabila']=='Nu') { ?><h2>Oferta este expirata!!</h2> <? } ?>
  <h1><?php echo $row["denumire"]; ?></h1>

  <div align="right" style="font-size:18px; font-weight:bold; padding:0 5px; letter-spacing:1px; color:#666;">COD OFERTA: <span class="blue">CIR-<?php echo $id_circuit; ?>-ZT</span></div>

  <?php if($detalii['valabila']=='nu') {} else { ?>
  <ul class="miscLINE">
    <li><a style="cursor: pointer;" onclick="openwindow('/recomanda_oferta.php?id=<?php echo $id_circuit; ?>&amp;nume=<? echo $denumire; ?>&amp;id_pers=<? echo $usuer_fizic; ?>&amp;oferta_speciala=Nu&amp;id_pers=<? echo $usuer_fizic; ?>&amp;recomanda=circuit', '620px', '450px'); return false;" title="Trimite pe mail" rel="nofollow"><span class="iconS10"></span>Trimite pe mail</a></li>
    <li><a href="/printeaza/circuit/<?php echo fa_link($row["denumire"])."-".$id_circuit; ?>.html" target="_blank" title="Printeaza" rel="nofollow"><span class="iconS12"></span>Printeaza</a></li>
    <li><a href="/circuit/<?php echo fa_link($row["denumire"]); ?>-<?php echo $id_circuit; ?>.pdf" target="_blank" title="Exporta PDF" rel="nofollow"><span class="iconS13"></span>Exporta PDF</a></li>
    <li><? if (efavorita($_SESSION['id_persoana_fizica'],$id_circuit,2)) { ?> <a style="cursor: pointer;" onclick="scoate_fav(<? echo $id_circuit; ?>,<? echo $_SESSION['id_persoana_fizica'];?>,2)" title="Sterge de la favorite"><span class="iconS11"></span>Sterge de la favorite</a><? } else { ?><a style="cursor: pointer;" onclick="<? if ($_SESSION['id_persoana_fizica']) {  ?> adauga_fav(<? echo $id_circuit; ?>,<?=$_SESSION['id_persoana_fizica'];?>,2)<? } else { ?> window.location='http://www.ocaziituristice.ro/login'  <? }?>" title="Adauga la favorite"><span class="iconS11"></span>Adauga la favorite</a> <? } ?></li>
  </ul>
  <?php } ?>

  <br class="spacer" />
  <div id="favorite"></div>

  <div style="width:300px; float:right; margin-top:3px;">
    <div class="contactAgentie" align="right">
      <div class="titlu">REZERVA ACUM!</div>
      <div style="font-size:13px;" align="left">Comunica agentiei de turism <strong class="red"><?php echo ucwords(strtolower($row_date["denumire_utilizator"])); ?></strong> ca ai vazut oferta pe <span class="blue">OcaziiTuristice.ro</span> pentru a obtine pretul cel mai bun</div>
      <div class="telefon" title="Contacteaza telefonic agentia de turism <?php echo ucwords(strtolower($row_date["denumire_utilizator"])); ?>"><img src="/images/phone_icon.jpg" alt="Contacteaza telefonic agentia" width="40" height="40" /> <?php echo substr($row_date["telefon"], 0, 12); ?></div>
      <br class="clear" />
      <a href="#contactAgentie" class="link">&raquo; Contactati agentia &laquo;</a> <a href="#contactAgentie" class="link"><img src="/sigle_agentii/<? echo $row['sigla']; ?>" width="100" height="30" style="border:1px solid #CCC; vertical-align:middle;" alt="AGENTIA: <? echo $row['nume_agentie']; ?>" title="AGENTIA: <? echo $row['nume_agentie']; ?>" /></a><br />
    </div>
    
    <div style="border:1px solid #CCC; -moz-border-radius:6px; -webkit-border-radius:6px; padding:5px 10px; margin:3px 4px; width:270px; font-size:16px; font-weight:bold;" align="right">
      <a style="cursor: pointer;" onclick="openwindow('/cere_mai_multe_detalii.php?id=<?php echo $id_circuit; ?>&amp;cere_info=circuit&amp;id_agentie=<? echo $row["id_agentie"]?>&amp;mail_agentie=<? echo $row["mail_agentie"]?>&amp;nume_agentie=<? echo $row["nume_agentie"]?>', '620px', '400px'); return false;" title="Cere mai multe informatii"><span class="red">Cere mai multe detalii</span></a>
    </div>
  </div>

  <div class="infoLocalizare">
    <strong class="blue">Continent:</strong> <?php echo $row["nume_continent"]; ?><br />
    <strong class="blue">Durata Circuit:</strong> <?php echo $row["nr_zile"]; ?> zile
    <?php if($row['denumire_transport']) { ?>
    <strong class="blue">Transport:</strong> <?php echo $row["denumire_transport"]; } ?>
    <br class="spacer" />
  </div>

  <div id="gallery" class="gallery">
  <?php
  $j=1;
  for($i=1;$i<=3 && $j<=3; $i++)
		{
			if($row["poza".$i]!=NULL) 
			{
				$j++;
				$nume_poza=$row["poza".$i];
				?>
                <a href="http://www.ocaziituristice.ro/img_prima_circuit/<?php echo $nume_poza; ?>" title="Poza marita"><img src="http://www.ocaziituristice.ro/thumb_circuit/<?php echo $nume_poza; ?>" class="imageDescriere" width="100" /></a>
				<?php
			}
		}
	?>
  </div>

  <div style="float:left; border:1px solid #CCC; -moz-border-radius:6px; -webkit-border-radius:6px; padding:5px 10px; margin:0 5px 5px 5px; width:320px; font-size:13px;" align="left">
    <strong>Orase vizitate:</strong> &nbsp; <?php $circuit=new CIRCUIT($id_circuit); $circuit->load_localitati_traversate(); echo $circuit->localitati_traversate; ?>
  </div>

  <br class="clear" /><br />

  <div style="border:1px solid #CCC; -moz-border-radius:6px; -webkit-border-radius:6px; padding:5px 10px; margin:0 5px;">
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
      <td width="40%">&nbsp;</td>
      <td align="left" valign="top"><a style="cursor: pointer;" onmousedown="if(document.getElementById('add_share').style.display == 'none'){ document.getElementById('add_share').style.display = 'block'; }else{ document.getElementById('add_share').style.display = 'none'; }" rel="nofollow"><span class="iconS9"></span>Share</a></td>
      <td width="40%">&nbsp;</td>
    </tr>
  </table>
  </div>
  
  <div id="add_share" style="display:none; border:1px solid #CCC; -moz-border-radius:6px; -webkit-border-radius:6px; padding:5px 10px; margin:2px 5px;">

<script type="text/javascript">
var addthis_config = {
     username: "ocaziituristice"
}
</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>

  <ul class="miscLINE" style="margin:0;">
    <li><a href="ymsgr:im?+&amp;msg=Iti+recomand+urmatoarea+oferta+de+pe+ocaziituristice.ro:+<?php echo "http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI']; ?>" target="_blank" title="Trimite pe Yahoo Messenger" rel="nofollow"><span class="iconS8"></span>Yahoo Messenger</a></li>
    <li><a class="addthis_button_twitter" rel="nofollow" style="cursor:pointer;"><span class="iconS7"></span>Twitter</a></li>
    <li><a class="addthis_button_facebook" rel="nofollow"><span class="iconS3"></span>Facebook</a></li>
    <li><a class="addthis_button_digg" rel="nofollow"><span class="iconS2"></span>Digg</a></li>
    <li><a class="addthis_button_myspace" rel="nofollow"><span class="iconS6"></span>My Space</a></li>
    <li><a class="addthis_button_delicious" rel="nofollow"><span class="iconS1"></span>Delicious</a></li>
    <li><a class="addthis_button_stumbleupon" rel="nofollow"><span class="iconS5"></span>StumbleUpon</a></li>
    <li><a class="addthis_button_reddit" rel="nofollow"><span class="iconS4"></span>Reddit</a></li>
  </ul>
  <br class="clear" />
  </div>
  
  <br class="clear" /><br />

  <div class="descriereOferta">
    
	<?php echo afiseaza_frumos(remove_special_characters(nl2p(strip_tags($row["descrierea_circuitului"])))); ?>
    
    <br class="clear" /><br />
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      	<? if(strlen(trim($row["serv_incluse"]))>0){?>
        <td width="45%" align="left" valign="top" class="tableServiciiBlue"><h3>&nbsp;&nbsp;&nbsp;Servicii incluse:</h3></td>
        <? }?>
        <? if(strlen(trim($row["serv_incluse"]))>0 && strlen(trim($row["serv_neincluse"]))>0){?>
        <td width="10%" rowspan="2" align="middle" valign="top" class="tableServiciiDespartitor"><img src="/images/spacer.gif" width="5" height="1" /></td>
        <? }?>
        <? if(strlen(trim($row["serv_neincluse"]))>0){?>
        <td width="45%" align="left" valign="top" class="tableServiciiRed"><h3>&nbsp;&nbsp;&nbsp;Servicii neincluse:</h3></td>
        <? }?>
      </tr>
      <tr>
      	<? if(strlen(trim($row["serv_incluse"]))>0){?>
        <td width="45%" align="left" valign="top" class="tableServiciiBlue"><ul><?php echo afiseaza_frumos(remove_special_characters(nl2li(strip_tags($row["serv_incluse"])))); ?></ul></td>
        <? }?>
        <? if(strlen(trim($row["serv_neincluse"]))>0){?>
        <td width="45%" align="left" valign="top" class="tableServiciiRed"><ul><?php echo afiseaza_frumos(remove_special_characters(nl2li(strip_tags($row["serv_neincluse"])))); ?></ul></td>
        <? }?>
      </tr>
    </table>

	<br class="clear" /><br />

<?php
$sel="
SELECT
plecari_circuit.id_plecare,
GROUP_CONCAT(date_format(plecari_circuit.data_plecare, '%d %b %Y') SEPARATOR ',<br />') AS date_plecare,
plecari_circuit.pret,
plecari_circuit.moneda,
plecari_circuit.pret_aditional,
plecari_circuit.moneda_aditional,
plecari_circuit.pret_last_minute
FROM
plecari_circuit
WHERE
plecari_circuit.id_circuit =  '$id_circuit'
and plecari_circuit.data_plecare >= '$azi'
GROUP BY
plecari_circuit.pret,
plecari_circuit.moneda,
plecari_circuit.pret_aditional,
plecari_circuit.moneda_aditional
ORDER BY plecari_circuit.data_plecare ASC
";
$rez=mysql_query($sel);
?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ofertaPreturi">
      <tr>
        <th width="120" align="left" valign="middle">Plecari</th>
        <th align="center" valign="middle">Pret</th>
        <th width="100" align="center" valign="middle">&nbsp;</th>
      </tr>
      
<?php $i=0; while($row_plecare=mysql_fetch_array($rez)) { ?>

      <?php if($row_plecare['pret_last_minute']) { ?><tr class="lastminute"> <?php } else { ?> <tr><?php } ?>
        <td width="120" align="left" valign="middle"><?php echo $row_plecare["date_plecare"]; ?></td>
        <td align="center" valign="middle" class="price">
        
        <?php  if($row_plecare['pret_last_minute']) { $pret=$row_plecare['pret_last_minute']; echo "<span class=\"oldprice\">".$row_plecare["pret"]." ".$row_plecare["moneda"]."</span> ".$row_plecare['pret_last_minute']." ".$row_plecare["moneda"];   } else {  $pret=$row_plecare['pret']; echo $row_plecare["pret"]." ".$row_plecare["moneda"]; if($row_plecare["pret_aditional"]) { echo " + ".$row_plecare["pret_aditional"]." ".$row_plecare["moneda_aditional"]; } }?>
        
        </td>
        <td width="100" align="center" valign="middle">
        
        <a style="cursor: pointer;" onclick="openwindow('/rezervare_circuit.php?id=<?php echo $id_circuit; ?>_<?php echo $pret; ?>', '650px', '460px'); return false;" title="Rezerva" rel="nofollow"><img src="/images/buton_rezerva.gif" /></a>
       
        </td>
      </tr>
<?php $i++; } ?>
    </table>
    
    <br class="clear" /><br />

    <?  $usuer_fizic = $_SESSION['id_persoana_fizica']; ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="45%" align="left" valign="top" class="tableServiciiBlue">
        <? if(strlen(trim($row["detalii_preturi"]))>0){?>
          <h3>&nbsp;&nbsp;&nbsp;Detalii preturi:</h3>
        <? }?>
        </td>
        <td width="10%" rowspan="2" align="middle" valign="top" class="tableServiciiDespartitor"><img src="/images/spacer.gif" width="5" height="1" /></td>
        <td width="45%" align="left" valign="top" class="tableServiciiRed">
        <? if(strlen(trim($row["detalii_plecari"]))>0){?>
          <h3>&nbsp;&nbsp;&nbsp;Detalii plecari:</h3>
        <? }?>
        </td>
      </tr>
      <tr>
        <td width="45%" align="left" valign="top">
        <? if(strlen(trim($row["detalii_preturi"]))>0){?>
		  <ul><?php echo afiseaza_frumos(remove_special_characters(nl2li(strip_tags($row["detalii_preturi"])))); ?></ul>
		<? }?>
        </td>
        <td width="45%" align="left" valign="top">
      	<? if(strlen(trim($row["detalii_plecari"]))>0){?>
		  <ul><?php echo afiseaza_frumos(remove_special_characters(nl2li(strip_tags($row["detalii_plecari"])))); ?></ul>
        <? }?>
        </td>
      </tr>
    </table>
    <? if(strlen(trim($row["alte_detalii"]))>0){?>
	<h3 class="green">&nbsp;&nbsp;&nbsp;Alte detalii:</h3> <br />
	<?php echo remove_special_characters(nl2br(strip_tags($row["alte_detalii"]))); ?>
    <? }?>
    
    <? $sel_plec="SELECT
			loc_plecari_circuit.detalii,
			localitati.denumire
			FROM
			loc_plecari_circuit
			Inner Join localitati ON loc_plecari_circuit.id_localitate = localitati.id_localitate
			WHERE
			loc_plecari_circuit.id_circuit =  '$id_circuit'";
$que_plec=mysql_query($sel_plec) or die(mysql_error());
if(mysql_num_rows($que_plec)>0){
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ofertaPreturi">
<tr>
  <th width="224" align="center" valign="middle">Oras plecare</th>
  <th width="847" align="center" valign="middle">Detalii</th>
</tr>
<? while($row_plec=mysql_fetch_assoc($que_plec)){ 
?>
<tr>
   <td><strong><? echo $row_plec['denumire'];?></strong></td>
   <td><? echo $row_plec['detalii'];?></td>
</tr>
<? } 
?> </table>
	<br />
<?
} 
@mysql_free_result($que_plec);
?>
    
<br class="clear" /><br />

<div id="detaliiAgentie">
<a name="contactAgentie"></a>
<div class="detaliiAgentie">
<div style="border:1px solid #d60808; background:url(/images/bkg_agentie_title.gif) repeat-x;">
	<div align="left" class="titleLeft">Oferta apartine Agentiei</div><div align="right" class="titleRight"><?php echo ucwords(strtolower($row_date["denumire_utilizator"])); ?></div>
    <div class="spacer"></div>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="2" style="border:1px solid #d60808; background:#FFF;">
  <tr>
    <td width="50%" align="left" valign="top"><strong>Telefon:</strong> <span style="font-size:16px; font-weight:bold;" class="blue"><?php echo $row_date["telefon"]; ?></span></td>
    <td width="1" rowspan="3" align="left" valign="top" style="border-left:1px solid #d60808;"><img src="/images/spacer.gif" width="1" height="1" /></td>
    <td width="50%" align="left" valign="top"><strong>Localitatea:</strong> <?php echo ucwords(strtolower($row_date["localitate"])); ?></td>
    </tr>
  <tr>
    <td width="50%" align="left" valign="top"><strong>E-mail:</strong> <?php echo $row_date["mail"]; ?></td>
    <td width="50%" align="left" valign="top"><strong>Adresa:</strong> <?php echo ucwords(strtolower($row_date["adresa"])); ?></td>
    </tr>
  <tr>
    <td width="50%" align="left" valign="top"><strong>Website:</strong> <a href="<?php echo $row_date["site_web"]; ?>" target="_blank" title="<?php echo str_replace('http://', '', $row_date["site_web"]); ?>" rel="nofollow"><?php echo str_replace('http://', '', $row_date["site_web"]); ?></a></td>
    <td width="50%" align="left" valign="top"><strong>Fax:</strong> <?php echo $row_date["fax"]; ?></td>
    </tr>
</table>
</div>
<?php /*?><div align="right" class="titleAgentieOferte" style="cursor:pointer; margin:0;" onclick="document.getElementById('hiding').style.display='block'">Vezi toate ofertele Agentiei</div><?php */?>
</div>
<div class="detaliiAgentie" <?php /*?>id="hiding"<?php */?> style="padding:0 15px; border:1px solid #d60808; background:#FFF;">
  <table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin:0;">
	<tr>
	<? $agentie=new AGENTIE($id_user);
	if($agentie->numar_sejururi()>'0') { ?>
		<td width="33%" align="left" valign="middle"><a href="/agentia/<?php echo fa_link($row_date["denumire_utilizator"]); ?>/sejururi/" title=" Sejururi agentia <?php echo ucwords($row_date["denumire_utilizator"]); ?> "><img src="/images/icon_sejururi.gif" width="30" height="30" style="vertical-align:middle;" alt="" /></a> <a href="/agentia/<?php echo fa_link($row_date["denumire_utilizator"]); ?>/sejururi/" title=" Sejururi agentia <?php echo ucwords($row_date["denumire_utilizator"]); ?> ">Sejururi (<?php echo $agentie->numar_sejururi();?>)</a></td><? } 
		//if($agentie->numar_cazari()>'0') { ?>
		<?php /*?><td width="33%" align="left" valign="middle"><a href="/agentia/<?php echo fa_link($row_date["denumire_utilizator"]); ?>/cazari.html" title=" Cazari agentia <?php echo ucwords($row_date["denumire_utilizator"]); ?> "><img src="/images/icon_cazari.gif" width="30" height="30" style="vertical-align:middle;" alt="" /></a> <a href="/agentia/<?php echo fa_link($row_date["denumire_utilizator"]); ?>/cazari.html" title=" Cazari agentia <?php echo ucwords($row_date["denumire_utilizator"]); ?> ">Cazari (<?php echo $agentie->numar_cazari(); ?>)</a></td><?php */?>
        <? //}
		if($agentie->numar_circuite()>'0') { ?>
		<td width="33%" align="left" valign="middle"><a href="/agentia/<?php echo fa_link($row_date["denumire_utilizator"]); ?>/circuite/" title=" Circuite agentia <?php echo ucwords($row_date["denumire_utilizator"]); ?> "><img src="/images/icon_circuite.gif" width="30" height="30" style="vertical-align:middle;" alt="" /></a> <a href="/agentia/<?php echo fa_link($row_date["denumire_utilizator"]); ?>/circuite/" title=" Circuite agentia <?php echo ucwords($row_date["denumire_utilizator"]); ?> ">Circuite (<?php echo $agentie->numar_circuite(); ?>)</a></td><? } ?>
	</tr>
  </table>
</div>

  </div>
  
  <br class="clear" /><br />
  
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/concurs/banner_concurs.php');

$circuit=new CIRCUIT($id_circuit); $loc=$circuit->make_localitati_traversate();
if(sizeof($loc)>'0') {
$template="".$_SERVER['DOCUMENT_ROOT']."/templates/afisare_sejur_patrat.tpl";
$link='http://www.ocaziituristice.ro';

$afisare =new AFISARE();
$afisare->setOfertaSpeciala($template);
$afisare->setAfisare(1, 4);
$afisare->setRandom('1');
$afisare->setOrase($loc);
$afisare->initializeaza_pagini($link);
if($afisare->numar_oferte>'0') { ?>
<h1 class="blue">Sejururi din: <span class="red"><?php foreach($loc as $key =>$value) echo ucwords($value).", "; ?></span> &nbsp;<img src="/images/arrow_pointing_down.gif" style="vertical-align:middle;" alt="" /></h1>
<?
$afisare->afiseaza_scorpy();
unset($afisare);
?>
<div class="spacer"></div>
<br/>
<?
  }
  
$afisare =new AFISARE();
$afisare->setOfertaSpeciala($template);
$afisare->setAfisare(1, 4);
$afisare->setRandom('1');
$afisare->set_cazare('da');
$afisare->setOrase($loc);
$afisare->initializeaza_pagini($link);
if($afisare->numar_oferte>'0') { ?>
<h1 class="blue">Cazari din: <span class="red"><?php foreach($loc as $key =>$value) echo ucwords($value).", "; ?></span> &nbsp;<img src="/images/arrow_pointing_down.gif" style="vertical-align:middle;" alt="" /></h1>
<?
$afisare->afiseaza_scorpy();
unset($afisare);
?>
<div class="spacer"></div>
<?
  } 
}
?>

<div class="clear"></div>
<br/>
