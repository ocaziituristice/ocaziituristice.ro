<?php
if ( $_COOKIE["oferte_vizit"] ) {
	$of              = $_COOKIE["oferte_vizit"];
	$of              = str_replace( ',,', ',', $of );
	$of              = str_replace( ',+,', ',', $of );
	$oferte          = explode( ',', $of );
	$id_hoteluri_viz = '';
	$id_oferte_viz   = '';
	$nr              = sizeof( $oferte );
	if ( $nr > 0 ) {
		for ( $i = $nr - 1; $i >= 0; $i -- ) {

			$of = explode( '+', $oferte[ $i ] );
			if ( $of[1] != '' ) {
				$id_oferte_viz = $id_oferte_viz . "" . $of[1] . ",";
			}

		}
		$id_oferte_viz = substr( $id_oferte_viz, 0, - 1 );
	}
	?>
    <div class="homepage-boxes last-seen-section">
        <div class="layout">
            <h3 class="subtitle">Ultimele oferte vazute de mine</h3>
            <div class="ofVizitate">
				<?php
				$selOfLast = "SELECT
oferte.denumire,
oferte.denumire_scurta,
oferte.id_oferta,
oferte.nr_zile,
oferte.nr_nopti,
oferte.exprimare_pret,
oferte.pret_minim,
oferte.moneda,
oferte.masa,
transport.denumire AS denumire_transport,
hoteluri.nume,
hoteluri.stele,
hoteluri.id_hotel,
hoteluri.tip_unitate,
hoteluri.poza1,
zone.denumire as denumire_zona,
localitati.denumire as denumire_localitate,
tari.denumire as denumire_tara,
continente.nume_continent
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
INNER JOIN transport ON transport.id_trans=oferte.id_transport
LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
WHERE oferte.valabila = 'da'
AND oferte.id_hotel IN (" . $id_oferte_viz . ")
group by oferte.id_hotel
ORDER BY FIND_IN_SET(oferte.id_hotel, '" . $id_oferte_viz . "')
LIMIT 0,4 ";
				$queOfLast = mysql_query( $selOfLast ) or die( mysql_error() );

				while ( $rowOfLast = mysql_fetch_array( $queOfLast ) ) {
//					echo '<pre>';
//					var_dump( $rowOfLast );
//					echo '</pre>';
					if ( $rowOfLast['poza1'] ) {
						$last_pic = '/img_hotel/' . $rowOfLast['poza1'];
					} else {
						$last_pic = '/images/no_photo.jpg';
					}

					if ( $rowOfLast['tip_unitate'] == 'Circuit' ) {
						$last_denumire   = $rowOfLast['nume'];
						$last_stele      = '';
						$last_localizare = $rowOfLast['nume_continent'] . ' / ' . $rowOfLast['nr_zile'] . ' zile';
						$last_link       = '/circuit/'/*.fa_link($rowOfLast['nume_continent']).'/'*/ . fa_link_oferta( $last_denumire ) . '-' . $rowOfLast['id_oferta'] . '.html';
					} else {
						$last_denumire   = $rowOfLast['nume'];
						$last_stele      = '<span class="stele-mici-' . $rowOfLast['stele'] . '"></span>';
						$last_localizare = $rowOfLast['denumire_zona'];
						if ( $rowOfLast['denumire_localitate'] <> $localizare ) {
							$localizare = $localizare . ' / ' . $rowOfLast['denumire_localitate'];
						}
						$last_localizare = $last_localizare . ' / ' . $rowOfLast['denumire_tara'];
						$last_link       = make_link_oferta( $rowOfLast['denumire_localitate'], $rowOfLast['nume'] );
					}
					?>
                    <div class="prod clearfix">
                        <div class="prod-bg" style="background-image:url(<?php echo $last_pic; ?>);"></div>
                        <div class="titlu">
							<?php echo $last_denumire; ?>
                        </div>
						<?php if ( $last_stele ) : ?>
                            <div class="hotel-stars">
								<?php echo $last_stele ?>
                            </div>
						<?php endif; ?>
                        <div class="prod-overlay">
                            <a href="<?php echo $last_link; ?>" class="link-blue">
                                <div class="overlay-text">
                                    <h3><?php echo $last_denumire; ?></h3>
                                    <h5><?php echo $rowOfLast['denumire_localitate'] . ' / ' . $rowOfLast['denumire_tara'] ?></h5>
                                    <div>de la</div>
                                    <div class="price">
                                        <span class="red"><?php echo $rowOfLast['pret_minim'] ?></span> <?php echo $rowOfLast['moneda'] ?>
                                    </div>
                                    <p><?php echo $rowOfLast['exprimare_pret'] ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php }
				@mysql_free_result( $queOfLast ); ?>
            </div>
        </div>
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function () {
        var prods = $('.last-seen-section').find('.ofVizitate').find('.prod').length;
        if (prods == 1) {
            $('.last-seen-section').find('.ofVizitate').addClass('single-column');
        } else if (prods == 2) {
            $('.last-seen-section').find('.ofVizitate').addClass('two-columns');
        } else if (prods == 3) {
            $('.last-seen-section').find('.ofVizitate').addClass('three-columns');
        } else {
            $('.last-seen-section').find('.ofVizitate').addClass('more-columns');
        }
    });

</script>
