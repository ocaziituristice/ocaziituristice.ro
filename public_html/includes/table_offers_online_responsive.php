<?php
include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/suppliers_xml.php');
	

$offers_hot = $_GET['offers'];
$var        = explode( ";;", base64_decode( $offers_hot ) );
$id_hotel   = $var[0];
?>
	<?php
	$timeout = time() + 60 * 60 * 24 * 30;
	if ( isset( $_POST['transport'] ) ) {
		$array_transport = explode( ";", $_POST['transport'] );
		$orasplec        = $array_transport['1'];
		$tip_transport   = $array_transport['0'];
	}
	$value_grad_ocupare = $_POST['plecdata'] . '*' . $_POST['pleczile'] . '*' . $_POST['adulti'] . '*' . $_POST['copii'] . '*' . $_POST['age'][0] . '*' . $_POST['age'][1] . '*' . $_POST['age'][2] . '*' . $_POST['tip_masa'] . '*' . $array_transport[0] . '*' . $array_transport[1];
	if ( $_POST['id_hotel'] == $id_hotel ) {
		setcookie( "grad_ocupare", $value_grad_ocupare, $timeout, "/" );
	}

	$cookvar       = explode( "*", $_COOKIE['grad_ocupare'] );
	$orasplec      = null;
	$tip_transport = null;

	/*** check login admin ***/
	$logare_admin     = new LOGIN( 'useri' );
	$err_logare_admin = $logare_admin->verifica_user();
	/*** check login admin ***/

	$det           = new DETALII_HOTEL();
	$detalii_hotel = $det->select_camp_hotel( $id_hotel );

	if ( $var[5] != '' ) {
		$orasplec = $var[5];
	} elseif ( $cookvar[9] != '' ) {
		$orasplec = $cookvar[9];
	} else {
		$orasplec = null;
	}

	if ( $var[6] != '' ) {
		$orasplec = $var[6];
	}
	if ( $var[15] != '' ) {
		$tip_transport = $var[15];
	}
	$tip_transport;
	if ( $tip_transport == 1 ) {
		$orasplec = null;
	}

	$orasplec1 = $orasplec;
	if ( isset( $_POST['transport'] ) ) {
		$array_transport = explode( ";", $_POST['transport'] );
		$orasplec        = $array_transport['1'];
		$tip_transport   = $array_transport['0'];
	}
	$pleczile = $cookvar[1];
	if ( $_POST['pleczile'] ) {
		$pleczile = $_POST['pleczile'];
	}
	$plecnopti           = $pleczile - 1;
	$GLOBALS['plecdata'] = date( "Y-m-d", strtotime( $cookvar[0] ) );
	if ( isset( $_POST['plecdata'] ) ) {
		$GLOBALS['plecdata'] = date( "Y-m-d", strtotime( $_POST['plecdata'] ) );
	}
	//echo "-------------data------------".$plecdata;
	$arivdata = date( 'Y-m-d', strtotime( $plecdata . ' + ' . $plecnopti . ' days' ) );
	$adulti   = $cookvar[2];
	if ( isset( $_POST['adulti'] ) ) {
		$adulti = $_POST['adulti'];
	}
	preg_match( '/\d+/', $adulti, $adulti_a );
	$adulti = $adulti_a['0'];
	$copii  = $cookvar[3];
	if ( isset( $_POST['copii'] ) ) {
		$copii = $_POST['copii'];
	}
	$copil1 = $cookvar[4];

	$copil2       = $cookvar[5];
	$copil3       = $cookvar[6];
	$copil_age[0] = $cookvar[4];
	if ( isset( $_POST['age'][0] ) ) {
		$copil1       = $_POST['age'][0];
		$copil_age[0] = $_POST['age'][0];
	}
	$copil_age[1] = $cookvar[5];
	if ( isset( $_POST['age'][1] ) ) {
		$copil2       = $_POST['age'][1];
		$copil_age[1] = $_POST['age'][1];
	}
	$copil_age[2] = $cookvar[6];
	if ( isset( $_POST['age'][2] ) ) {
		$copil2       = $_POST['age'][2];
		$copil_age[2] = $_POST['age'][2];
	}
	?>
    <?php
    if ( isset( $var[15] ) ) {
	    $detalii_online = $det->oferte_online( $id_hotel, $plecdata, $tip_transport, $orasplec );
    } else {
	    $detalii_online = $det->oferte_online( $id_hotel, $plecdata );
    }

    if ( ! array_key_exists( $tip_transport, $detalii_online['tip_transport'] ) ) {
	    $tip_transport = key( $detalii_online['tip_transport'] );
    }

    if ( $detalii_online['min_data'] > $date_now ) {
	    $dataplec_min = $detalii_online['min_data'];
    } else {
	    $dataplec_min = $date_now;
    }
    $dataplec_max = $detalii_online['max_data'];

    if ( ! array_key_exists( $orasplec, $detalii_online[ $detalii_online['tip_transport'][ $tip_transport ] ] ) ) {
	    $orasplec = key( $detalii_online[ $detalii_online['tip_transport'][ $tip_transport ] ] );
    }
    $detalii['moneda'] = $detalii_hotel['moneda'];

    $detalii_transport = get_details_transport( $tip_transport );

    if(isset($detalii_online['tip_preturi'])) {
	$var_dates = '';
	$var_dates_if = '';
	if((((in_array("Avion", $detalii_online['tip_transport'])) or in_array("Autocar", $detalii_online['tip_transport'])) AND (count($detalii_online['tip_transport'])==1) || $orasplec1!=NULL))
	 {
		if(sizeof($detalii_online['data_start'])>0) {
			$var_dates .= 'array_dates'.$id_hotel.' = [';
			foreach($detalii_online['data_start'] as $key_ds => $value_ds) {
				$var_dates .= '"'.$value_ds.'",';
			}
			 $var_dates = substr($var_dates,0,-1).']';
		}
	//if($_SESSION['mail']=='razvan@ocaziituristice.ro') 	{  echo '<pre>';print_r ($detalii_online['data_start']);echo '</pre>';}
		if(in_array(date("Y-m-d",strtotime($plecdata)),$detalii_online['data_start'])) $plecdata = $plecdata;

		elseif(in_array(date("Y-m-d",strtotime("+1 day",strtotime($plecdata))),$detalii_online['data_start'])) $plecdata = date("d.m.Y",strtotime("+1 day",strtotime($plecdata)));
		elseif(in_array(date("Y-m-d",strtotime("-1 day",strtotime($plecdata))),$detalii_online['data_start'])) {$plecdata = date("d.m.Y",strtotime("-1 day",strtotime($plecdata)));}
		elseif(in_array(date("Y-m-d",strtotime("+2 day",strtotime($plecdata))),$detalii_online['data_start'])) $plecdata = date("d.m.Y",strtotime("+2 day",strtotime($plecdata)));
		elseif(in_array(date("Y-m-d",strtotime("-2 day",strtotime($plecdata))),$detalii_online['data_start'])) $plecdata = date("d.m.Y",strtotime("-2 day",strtotime($plecdata)));
		elseif(in_array(date("Y-m-d",strtotime("+3 day",strtotime($plecdata))),$detalii_online['data_start'])) $plecdata = date("d.m.Y",strtotime("+3 day",strtotime($plecdata)));
		elseif(in_array(date("Y-m-d",strtotime("-3 day",strtotime($plecdata))),$detalii_online['data_start'])) $plecdata = date("d.m.Y",strtotime("-3 day",strtotime($plecdata)));
	} else if(count($detalii_online['tip_transport'])>1)
	//else if(in_array("perioade", $detalii_online['tip_preturi']))

	{
		if(sizeof($detalii_online['data_start'])>0) {

			foreach($detalii_online['data_start'] as $key_ds => $value_ds) {
				if($detalii_online['data_end'][$key_ds]!="0000-00-00") {

					$ds[$key_ds] = explode('-', $value_ds);
					$de[$key_ds] = explode('-', $detalii_online['data_end'][$key_ds]);
					$data_end_interval = $de[$key_ds][2];
					$var_dates .= 'ds_'.$key_ds.' = new Date('.$ds[$key_ds][0].', '.$ds[$key_ds][1].' - 1, '.$ds[$key_ds][2].'), de_'.$key_ds.' = new Date('.$de[$key_ds][0].', '.$de[$key_ds][1].' - 1, '.$data_end_interval.'), ';

					$var_dates_if .= '(date >= ds_'.$key_ds.' && date <= de_'.$key_ds.')||';
				}
			}
			$var_dates = substr($var_dates,0,-2);
			$var_dates_if = substr($var_dates_if,0,-2);
		}
	}
}

//echo"00".$var_dates_if;
$showerr = 'da';
$make_request = 'da';

    if ( ! isset( $_COOKIE['grad_ocupare'] ) ) {
	    $showerr      = 'da';
	    $make_request = 'da';
    }
    if ( ! in_array( $pleczile, $detalii_online['no_nights'] ) ) {
	    $showerr      = 'da';
	    $make_request = 'nu';
    }
    if ( date( "Y-m-d", strtotime( $plecdata ) ) < $detalii_online['min_data'] or date( "Y-m-d", strtotime( $plecdata ) ) > $detalii_online['max_data'] ) {
	    $showerr      = 'da';
	    $make_request = 'nu';
    }

//if(!$err_logare_admin) { echo '<pre>';print_r($var);echo '</pre>';}
?>



<form id="reg-form<?php echo $id_hotel?>" method="post">
  <div class="price-box chenar chn-color-green border-light-grey" style="margin:5px 0 -1px 0 !important;padding: 20px 0;">
    <div class="NEW-search-wide clearfix">
      <div class="form-group col-md-2">
        <label for="calc-dataplec<?php echo $id_hotel; ?>">Data Check-in:</label>
        <input type="text" name="plecdata" id="calc-dataplec<?php echo $id_hotel; ?>" value="<?php echo (isset($plecdata) and $plecdata >= $detalii_online['min_data'] and $plecdata <= $detalii_online['max_data']) ? date("d.m.Y", strtotime($plecdata)) : ''; ?>" class="NEW-round4px form-control">
      </div>

      <div class="form-group col-md-2">
        <label for="calculeaza-nr-nopti<?php echo $id_hotel; ?>">Nr. nopţi:</label>
        <select name="pleczile" id="calculeaza-nr-nopti<?php echo $id_hotel; ?>" class="NEW-round4px form-control">
          <?php foreach($detalii_online['no_nights'] as $nrnpt) { ?>
          <option value="<?php echo $nrnpt; ?>" <?php if($pleczile==$nrnpt) { echo 'selected'; }elseif(($nrnpt==7) and (!isset($pleczile))){echo 'selected'; } ?>><?php echo $nrnpt; ?></option>
		  <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-2">
        <label for="calculeaza-adulti<?php echo $id_hotel; ?>">Nr. adulţi:</label>
        <select name="adulti" id="calculeaza-adulti<?php echo $id_hotel; ?>" class="NEW-round4px form-control">
		  <?php for($i=1; $i<=3; $i++) { ?>
          <option value="<?php echo $i; ?>" <?php if(!is_integer($adulti) and $i==2 and $selected!='selected') echo 'selected'; elseif($adulti==$i) echo $selected='selected'; ?>><?php echo $i; ?></option>
          <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-2">
        <label for="calculeaza-copii<?php echo $id_hotel; ?>">Nr. copii:</label>
        <select name="copii" id="calculeaza-copii<?php echo $id_hotel; ?>" class="NEW-round4px form-control" onchange="displayAges(this.value)">
		  <?php for($i=0; $i<=3; $i++) { ?>
          <option value="<?php echo $i; ?>" <?php if($copii==$i) echo 'selected'; ?>><?php echo $i; ?></option>
          <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-2">
        <label for="calculeaza-transport<?php echo $id_hotel; ?>">Transport:</label>
        <select name="transport" id="calculeaza-transport<?php echo $id_hotel; ?>" class="NEW-round4px form-control">
		  <?php asort($detalii_online['tip_transport']);
		  foreach($detalii_online['tip_transport'] as $k_tiptrans => $v_tiptrans) { ?>
          <option value="<?php echo $k_tiptrans; if($k_tiptrans==1) echo ';51'; ?>" <?php if($k_tiptrans!=1) echo 'disabled'; ?>
		  <?php if($k_tiptrans==$tip_transport) echo 'selected'; ?>><?php echo $v_tiptrans; ?></option>
          <?php if($k_tiptrans!=1) {
		  foreach($detalii_online[$v_tiptrans] as $k_orasplec => $v_orasplec) { ?>
          <option value="<?php echo $k_tiptrans.';'.$k_orasplec; ?>" <?php if($k_tiptrans==$tip_transport and $k_orasplec==$orasplec) echo 'selected'; ?>><?php echo $v_tiptrans.'/'.$v_orasplec; ?></option>
		  <?php }
		  } ?>
          <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-2 hide_mobile">
          <a onclick="return validateForm<?php echo $id_hotel?>()" class="btn btn-success btn-block button-green NEW-round6px" style="text-transform: inherit;margin-top: 23px;">Vezi preț</a>
      </div>

      <br class="clear">

      <div class="chd-ages" style="margin:10px 0 0 360px; <?php if($copii==0) echo 'display:none;'; ?>">
		<?php for($t=0; $t<3; $t++) { ?>
        <div class="form-group col-md-4 varste-copii-<?php echo $t; ?>" style="<?php if($copii<($t+1)) echo 'display:none;'; ?>">
          <label for="varste-copii-<?php echo $t; ?>-<?php echo $id_hotel; ?>">Copil <?php echo $t+1; ?></label>
          <select name="age[<?php echo $t; ?>]" id="varste-copii-<?php echo $t; ?>-<?php echo $id_hotel; ?>" class="NEW-round4px varste-copii-select-<?php echo $t; ?> form-control">
            <?php for($j1[$t]=1; $j1[$t]<=17; $j1[$t]++) { ?>
            <option value="<?php echo $j1[$t]; ?>" <?php if($copil_age[$t]==$j1[$t] and ($t+1)<=$copii) echo 'selected'; ?>><?php echo $j1[$t]; ?> ani</option>
            <?php } ?>
          </select>
        </div>
        <?php } ?>
          <div class="clear"></div>
      </div>
      
      <div class="form-group col-md-2 hide_desktop">
		  
          <a onclick="return validateForm<?php echo $id_hotel?>()" class="btn btn-success btn-block button-green NEW-round6px" style="text-transform: inherit;margin-top: 23px;">Vezi preț</a>
      </div>

	  <input name="parentLink" type="hidden" class="parentLink" value="">
	  <input name="prevdata" type="hidden" value="<?php echo $var[21]; ?>">
      <input name="id_hotel" type="hidden" value="<?php echo $id_hotel; ?>">
      <input name="tip_pagina" type="hidden" value="insiruire">

    </div>
  </div>
</form>

<label><div id="loader<?php echo $id_hotel;?>" align="center" style="display: none;"><br>
 
<span class="bigger-11em bold black italic w620" style="text-align:center">Vă rugăm așteptați. Comparam preturile si verificam disponibilitatea locurilor</span><br><img src="/images/loader3.gif" alt=""></div></label>

 <?php
//echo 'ccc<pre>';print_r($_POST);echo '</pre>';?>

<script>
$(".parentLink").val(document.location.href);
//$(".parentLink").val("ocaziituristice.ro");
function displayAges(nVal) {
	if(nVal>0) {
		$(".chd-ages").show();
	} else {
		$(".chd-ages").hide();
	}

	for(var i=0; i<3; i++) {
		if(i<nVal) {
			$(".varste-copii-"+i).css("display", "inline-block");
		} else {
			$(".varste-copii-"+i).css("display", "none");
			$(".varste-copii-select-"+i).val(0);
		}
	}
}
<?php if($var_dates) { ?>var <?php echo $var_dates; ?>;<?php } ?>

 var windowWidth = $(window).width();
        if (windowWidth < 767) {
            var numberOfMonths = 1;
        } else {
            var numberOfMonths = 3;
        }

$("#calc-dataplec<?php echo $id_hotel; ?>").datepicker({
	numberOfMonths: numberOfMonths,
	dateFormat: "dd.mm.yy",
	showButtonPanel: true,
	<?php if($var_dates) { ?>
	beforeShowDay: function(date) {
		var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
		<?php if($var_dates_if) { ?>
		return [(<?php echo $var_dates_if; ?>), ''];
		<?php } else { ?>
		return [array_dates<?php echo $id_hotel; ?>.indexOf(string) !== -1];
		<?php } ?>
	},
	<?php } //end $var_dates ?>
	minDate: "<?php echo date("d.m.Y", strtotime($dataplec_min)); ?>",
	maxDate: "<?php echo date("d.m.Y", strtotime($dataplec_max)); ?>"
});
<?php
if((!isset($_POST['plecdata']) and !isset($_COOKIE['grad_ocupare'])) or $show_calendar=='da') {
	echo '$("#calc-dataplec'.$id_hotel.'").datepicker("show");'; 
}?>
$("#calc-dataplec<?php echo $id_hotel; ?>").datepicker().bind('click keyup', function() {
	if($('#ui-datepicker-div :last-child').is('table')) {
		$('#customText').append("<span class='inline-block pad5 bigger-11em italic'>Selectaţi data când doriţi sa plecaţi în concediu şi apoi apăsaţi <strong>\"Vezi preţ\"</strong></span>");
	}
});

function validateForm<?php echo $id_hotel; ?>() {
	var date_field<?php echo $id_hotel; ?> = document.getElementById("calc-dataplec<?php echo $id_hotel; ?>").value;
    $("#loader<?php echo $id_hotel?>").show();
    $("#tabelhotel<?php echo $id_hotel?>").hide();
	if (date_field<?php echo $id_hotel; ?> == null || !date_field<?php echo $id_hotel; ?>) {
		alert("Data plecării trebuie selectată!");
		document.getElementById("calc-dataplec<?php echo $id_hotel; ?>").focus();
	    $("#loader<?php echo $id_hotel?>").hide();
	    $("#tabelhotel<?php echo $id_hotel?>").show();
		return false;
	}
    searchHotels(<?php echo $id_hotel ?>);
}
</script>
<script type="text/javascript">

    $("#calculeaza-transport<?php echo $id_hotel; ?>").change(function () {
		$("#loader<?php echo $id_hotel?>").show();
        $("#tabelhotel<?php echo $id_hotel?>").hide();
        validateForm<?php echo $id_hotel; ?>();
    });

    function searchHotels(id) {
        var data = $('#reg-form' + id).serialize();
        $.ajax({
            type : 'POST',
            url  : '/includes/table_offers_online_responsive.php?offers=<?php echo $offers_hot;?>',
            data : data,
            success :  function(data) {
                $("#loader<?php echo $id_hotel?>").show();
                $("#tabelhotel<?php echo $id_hotel?>").hide();
                $("#reg-form<?php echo $id_hotel?>").fadeOut(500).hide(function() {
                    $(".result<?php echo $id_hotel?>").fadeIn(500).show(function() {
                        $(".result<?php echo $id_hotel?>").html(data);
                        $("#loader<?php echo $id_hotel?>").hide();
                        $("#tabelhotel<?php echo $id_hotel?>").show();
                    });
                });
            }
        });
    }
</script>
	<?php
	if ( $make_request == 'da' ) {
		$rezcam = array();
		if ( $copii == 0 ) {
			$copil1 = 0;
			$copil2 = 0;
			$copil3 = 0;
		}
		if ( $copii == "" or ! ( isset( $var ) ) ) {
			$copil1 = 0;
			$copil2 = 0;
			$copil3 = 0;
		}
		$ukey                    = date( "Y-m-d", strtotime( $plecdata ) ) . $pleczile . $adulti . $copii . $copil1 . $copil2 . $copil3 . $tip_transport . $orasplec;
		$cache_results['prices'] = cache_prices( $ukey, $id_hotel );
		$cache_results['planes'] = cache_planes( $ukey, $id_hotel );

		if ( count( $cache_results['prices'] ) > 0 ) {

			$rezcam = $cache_results['prices'];
			$flight = $cache_results['planes'];
		} else if ( isset( $detalii_online['furnizor'] ) ) {
			foreach ( $detalii_online['furnizor'] as $key_furnizor => $id_furnizor ) {
				$furnizor      = get_detalii_furnizor( $id_furnizor );
				$one_time_only = 'nu';
				foreach ( $detalii_online['id_oferta'][ $id_furnizor ] as $id_oferta ) {
					$skip_offer = 'nu';
					if ( isset( $orasplec ) ) {
						if ( $orasplec != $detalii_online['oras_plecare'][ $id_oferta ][0] ) {
							$skip_offer = 'da';
						}
					}
					$detalii['cazare']           = $detalii_online['cazare'][ $id_oferta ];
					$detalii['transport']        = $detalii_online['transport'][ $id_oferta ];
					$detalii['tip_preturi']      = $detalii_online['tip_preturi'][ $id_oferta ];
					$detalii['id_oferta_pivot']  = $detalii_online['id_oferta_pivot'][ $id_oferta ];
					$detalii['nr_formula_pivot'] = $detalii_online['nr_formula_pivot'][ $id_oferta ];
					$detalii['masa']             = $detalii_online['masa'][ $id_oferta ];
					if ( $skip_offer == 'nu' and $one_time_only == 'nu' ) {

						//if($_SESSION['mail']=='razvan@ocaziituristice.ro')  {echo "--".$plecdata;}
						if ( $detalii['transport'] == 'Avion' ) {
							$detalii['cazare'] = 'nu';
						}

						$stop_sales_hotel = $det->stop_sales( $id_hotel, $plecdata, $arivdata );

						switch ( $id_furnizor ) {

							case '2':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/p45.php' );
								break;
							case '52':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/malta.php' );
								break;
							case '1':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/teztour.php' );
								break;
							case '398':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/bibi_touring.php' );
								break;
							case '330':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/paradis.php' );
								break;
							case '471':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/eximtur.php' );
								break;
							case '397':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/aerotravel.php' );
								break;
							case '225':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/prestige.php' );
								break;
							case '15':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/accent.php' );
								break;
							case '22':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/cocktails.php' );
								break;
							case '6':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/christian.php' );
								break;
							case '216':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/melitours.php' );
								break;
							case '463':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/nova.php' );
								break;
							case '474':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/fibula.php' );
								break;
							case '480':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/romadria.php' );
								break;
							case '218':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/transilvania.php' );
								break;
							case '486':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/laguna.php' );
								break;
							case '490':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/peninsula.php' );
								break;
							case '491':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/mouzenidis.php' );
								break;
							case '493':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/trip_corporation.php' );
								break;
							case '32':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/solvex.php' );
								break;
							case '509':
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/holiday_office.php' );
								break;
							default:
								include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/z_preturi_pivot.php' );
						}
					}
				}
			}
		}

		foreach ( $rezcam as $key_rezcam => $value_rezcam ) {
			if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' and $stop_sales_hotel[ $rezcam[ $key_rezcam ]['id_camera'] ]['stop_sales'] == 'da' ) {
				$rezcam[ $key_rezcam ]['disponibilitate'] = 'stopsales';
			}

		}

		if ( $rezcam[ key( $rezcam ) ]['pret'] != '0' and $rezcam[ key( $rezcam ) ]['disponibilitate'] != '0' ) {

			if ( sizeof( $rezcam ) > 0 ) {
				$rezcam = optimize_rooms( $rezcam, $pleczile, date( "Y-m-d", strtotime( $plecdata ) ), 'scurt' );
			}

			$rezcam_date = find_items( $rezcam, 'data_start', $plecdata );

			if ( sizeof( $rezcam_date ) > 0 ) {
				$plecdata_new = $plecdata;
			} else {
				$plecdata_new = $rezcam[ key( $rezcam ) ]['data_start'];
			}

		    if ( sizeof( $rezcam ) > 0 ) {
			    echo '<div id="tabelhotel' . $id_hotel . '"><table class="search-rooms table table-responsive offers_tables" style="border-top:none;font-size: 1rem;">';
	
			     echo '<div id="tabelhotel' . $id_hotel . '"><table class="search-rooms table table-responsive offers_tables" style="border-top:none;font-size: 1rem;">';
			    echo '<thead><tr><th class="hide_mobile">Data plecării /durată</th><th class="hide_mobile">Tip cameră</th><th class="hide_mobile" style="text-align:center;">Tip masă</th><th class="hide_mobile" style="text-align:center;">Preț<br>sejur/cameră</th><th class="hide_mobile"></th></tr></thead>';
				$i = 0;
				
		        foreach ( $rezcam as $key_rezcam => $value_rezcam ) {
		            if ( ( count( $cache_results['prices'] ) == 0 ) and ( $rezcam[ $key_rezcam ]['nr_adulti'] > 0 ) and ( $rezcam[ $key_rezcam ]['pret'] > 0 ) ) {
		                $ins_cache[ $key_rezcam ] = "INSERT INTO pret_cache SET 
                                                        ukey = '" . $ukey . "',
                                                        id_hotel = '" . $id_hotel . "',
                                                        id_pret = '" . $rezcam[ $key_rezcam ]['id_pret'] . "',
                                                        plecare = '" . $rezcam[ $key_rezcam ]['plecare'] . "',
                                                        id_camera = '" . $rezcam[ $key_rezcam ]['id_camera'] . "',
                                                        denumire_camera = '" . $rezcam[ $key_rezcam ]['denumire_camera'] . "',
                                                        pret = '" . $rezcam[ $key_rezcam ]['pret'] . "',
                                                        comision = '" . $rezcam[ $key_rezcam ]['comision'] . "',
                                                        moneda = '" . $rezcam[ $key_rezcam ]['moneda'] . "',
                                                        nr_adulti = '" . $rezcam[ $key_rezcam ]['nr_adulti'] . "',
                                                        nr_copii = '" . $rezcam[ $key_rezcam ]['nr_copii'] . "',
                                                        copil1 = '" . $rezcam[ $key_rezcam ]['copil1'] . "',
                                                        copil2 = '" . $rezcam[ $key_rezcam ]['copil2'] . "',
                                                        copil3 = '" . $rezcam[ $key_rezcam ]['copil3'] . "',
                                                        oferta = '" . $rezcam[ $key_rezcam ]['oferta'] . "',
                                                        oferta_pret = '" . $rezcam[ $key_rezcam ]['oferta_pret'] . "',
                                                        masa = '" . $rezcam[ $key_rezcam ]['masa'] . "',
                                                        series_name = '" . $rezcam[ $key_rezcam ]['series_name'] . "',
                                                        grila_name = '" . $rezcam[ $key_rezcam ]['grila_name'] . "',
                                                        info_grad_ocupare = '" . $rezcam[ $key_rezcam ]['info_grad_ocupare'] . "',
                                                        period = '" . $rezcam[ $key_rezcam ]['period'] . "',
                                                        data_start = '" . $rezcam[ $key_rezcam ]['data_start'] . "',
                                                        data_end = '" . $rezcam[ $key_rezcam ]['data_end'] . "',
                                                        disponibilitate = '" . $rezcam[ $key_rezcam ]['disponibilitate'] . "',
                                                        nr_nights = '" . $rezcam[ $key_rezcam ]['nr_nights'] . "',
                                                        id_furnizor = '" . $rezcam[ $key_rezcam ]['id_furnizor'] . "',
                                                        furnizor_comision_procent = '" . $rezcam[ $key_rezcam ]['furnizor_comision_procent'] . "',
                                                        id_oferta = '" . $rezcam[ $key_rezcam ]['id_oferta'] . "',
                                                        data_early_booking = '" . date( 'Y-m-d', strtotime( $rezcam[ $key_rezcam ]['data_early_booking'] ) ) . "',
                                                        comision_reducere='" . $rezcam[ $key_rezcam ]['comision_reducere'] . "',
                                                        servicii_incluse='" . $rezcam[ $key_rezcam ]['servicii_incluse'] . "',
                                                        data_adaugarii = NOW()
                                                        ";
		                $que_cache[ $key_rezcam ] = mysql_query( $ins_cache[ $key_rezcam ] ) or die( mysql_error() );
		                @mysql_free_result( $que_cache[ $key_rezcam ] );
		            }
		            $i ++;
		            if ( strlen( $rezcam[ $key_rezcam ]['nr_nights'] ) > 0 ) {
		                $pleczile = $rezcam[ $key_rezcam ]['nr_nights'];
		            }
		            if ( isset( $detalii_online['discount_tarif'][ $rezcam[ $key_rezcam ]['id_oferta'] ] ) and $detalii_online['discount_tarif'][ $rezcam[ $key_rezcam ]['id_oferta'] ] > 0 ) {
		                $discount_tarif = $detalii_online['discount_tarif'][ $rezcam[ $key_rezcam ]['id_oferta'] ];
		            } else {
		                $discount_tarif = $rezcam[ $key_rezcam ]['furnizor_comision_procent'];
		            }
		            $discount_tarif                       = 1;
		            $rezcam[ $key_rezcam ]['oferta_pret'] = $rezcam[ $key_rezcam ]['oferta_pret'] * $discount_tarif;
		            $rezcam[ $key_rezcam ]['pret']        = $rezcam[ $key_rezcam ]['pret'] * $discount_tarif;

		            if ( $rezcam[ $key_rezcam ]['oferta_pret'] == 0 and isset( $detalii['spo_reducere'] ) ) {
		                $rezcam[ $key_rezcam ]['oferta_pret'] = $rezcam[ $key_rezcam ]['pret'] + ( $detalii['spo_reducere'] * 2 );
		            }
		            if ( $rezcam[ $key_rezcam ]['oferta'] == '' and isset( $detalii['spo_descriere'] ) ) {
		                $rezcam[ $key_rezcam ]['oferta'] = '<strong>' . $detalii['spo_titlu'] . '</strong> ' . $detalii['spo_descriere'];
		            }

		            echo '<tr class="';
		            if ( $i % 2 == 0 ) {
		                echo 'bkg-grey ';
		            } else {
		                echo 'bkg-white ';
		            }
	
		            echo 'fnt-smaller">';

		            echo '<td class="text-left nowrap"><span class="bold ';

		            if ( $plecdata == $rezcam[ $key_rezcam ]['data_start'] ) {
		                echo 'black';
		            } else {
                        echo 'green';
		            }

		            echo '">' . date( "d.m.Y", strtotime( $rezcam[ $key_rezcam ]['data_start'] ) ) .'<br />'.$pleczile .' nopți</span></td>';
		            echo '<td class="text-left"><span class="black bold">' . preg_replace( "/\{{([^\]]+)\}}/", "", $rezcam[ $key_rezcam ]['denumire_camera'] ) . '</span>';
		            if ( $rezcam[ $key_rezcam ]['oferta'] ) {
		                echo '<br><span class="red italic">' . $rezcam[ $key_rezcam ]['oferta'] . '</span>';
		            }
		            if ( $rezcam[ $key_rezcam ]['series_name'] ) {
		                echo '<br><span class="green italic">' . $rezcam[ $key_rezcam ]['series_name'] . '</span>';
		            }
		            if ( $rezcam[ $key_rezcam ]['grila_name'] ) {
		                echo '<br><span class="green italic">' . $rezcam[ $key_rezcam ]['grila_name'] . '</span>';
		            }
		            if ( $rezcam[ $key_rezcam ]['servicii_incluse'] ) {
		                echo '<br><span class="blue italic">' . $rezcam[ $key_rezcam ]['servicii_incluse'] . '</span>';
		            }
		            if ( $rezcam[ $key_rezcam ]['info_grad_ocupare'] ) {
		                echo '<br><span class="blue bold">' . $rezcam[ $key_rezcam ]['info_grad_ocupare'] . '</span>';
		            }

		            // detalii zbor am pus mai mic decat 0
                    if ( $flight[ $rezcam[ $key_rezcam ]['id_oferta'] ]['flight_bookable'] == 1 and count( $flight[ $rezcam[ $key_rezcam ]['id_oferta'] ]['plecare'] ) < 0 and $rezcam[ $key_rezcam ]['disponibilitate'] != 'stopsales' ) {
		                echo '<br><a href="#" class="flight_schedule link-blue smaller-09em" id="' . $rezcam[ $key_rezcam ]['id_oferta'] . '" title="Detalii despre companie, cursa si orar"><i class="icon-flight bigger-11em"></i> <span class="underline">detalii zbor - click aici</span></a>';
		            }
		            echo '</td>';
		            echo '<td class="text-left"><span class="bold black">' . change_meal( $rezcam[ $key_rezcam ]['masa'] ) . '</span>';
		            if ( $tip_transport != 1 ) {
		                echo '<div class="nowrap"><span class="icons transport" style="background-image:url(/images/oferte/' . $detalii_transport['icon'] . ')';
		                if ( $tip_transport == 5 ) {
		                    echo '; width:70px !important;';
		                }
		                echo '" title="Transport ' . strtoupper( $detalii_transport['denumire'] ) . '"></span>';
		                if ( $detalii_online['taxa_aeroport'][ $rezcam[ $key_rezcam ]['id_oferta'] ] == 'da' ) {
		                    echo '<span class="icons" style="background-image:url(/images/icon_tax_free.png)" title="Taxe aeroport INCLUSE"></span>' . '</div>';
		                }
		            }
		            echo '</td>';
		            echo '<td style="text-align: center;">';

		            if ( $rezcam[ $key_rezcam ]['oferta_pret'] ) {
		                $oldprice = round( $rezcam[ $key_rezcam ]['oferta_pret'], 0 );
		                echo '<div class="old-price bold blue">' . $oldprice . ' ' . moneda( $rezcam[ $key_rezcam ]['moneda'] ) . '</div>';
		            } else {
		                $oldprice = 0;
		            }
		            echo '<div class="current-price green">' . new_price( $rezcam[ $key_rezcam ]['pret'] ) . ' ' . moneda( $rezcam[ $key_rezcam ]['moneda'] ) . '</div></td>';
		            echo '<td style="text-align: center;position: relative;">';
		            if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'disponibil' ) {
			            echo '<i class="fa fa-check-circle text-success"></i> ';
			            echo '<span class="badge badge-success">Disponibil</span><br />';
		            } elseif ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' ) {
			            echo '<i class="fa fa-check-circle text-primary"></i> ';
			            echo '<span class="badge badge-success" style="background-color: #065abf">Necesita Confirmare</span><br />';
		            } elseif ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'stopsales' ) {
			            echo '<i class="fa fa-minus-circle text-danger"></i> ';
			            echo '<span class="badge badge-danger">Indisponibil</span><br />';
		            }

		            if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'disponibil' or $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' ) {
		                echo '<a href="' . make_link_oferta( $detalii_hotel['localitate'], $detalii_hotel['denumire'], null, null ) . '?&id_oferta=' . $rezcam[ $key_rezcam ]['id_oferta'] . '&plecdata=' . date( "d.m.Y", strtotime( $rezcam[ $key_rezcam ]['data_start'] ) ) . '&pleczile=' . $pleczile . '&adulti=' . $rezcam[ $key_rezcam ]['nr_adulti'] . '&copii=' . $rezcam[ $key_rezcam ]['nr_copii'] . '&age[0]=' . $rezcam[ $key_rezcam ]['copil1'] . '&age[1]=' . $rezcam[ $key_rezcam ]['copil2'] . '&age[2]=' . $rezcam[ $key_rezcam ]['copil3'] . '"  target="_blank">';
		                $link_detalii = make_link_oferta( $detalii_hotel['localitate'], $detalii_hotel['denumire'], null, null ) . '?&id_oferta=' . $rezcam[ $key_rezcam ]['id_oferta'] . '&plecdata=' . date( "d.m.Y", strtotime( $rezcam[ $key_rezcam ]['data_start'] ) ) . '&pleczile=' . $pleczile . '&adulti=' . $rezcam[ $key_rezcam ]['nr_adulti'] . '&copii=' . $rezcam[ $key_rezcam ]['nr_copii'] . '&age[0]=' . $rezcam[ $key_rezcam ]['copil1'] . '&age[1]=' . $rezcam[ $key_rezcam ]['copil2'] . '&age[2]=' . $rezcam[ $key_rezcam ]['copil3'];
                        echo '<input name="detalii" type="button" value="Vezi Detalii" class="btn btn-primary full_width_mobile" style="cursor: pointer;"/> </a>';
                        echo '<div class="clear" style="height: 5px;"></div>';
                        $basket[ $key_rezcam ] = check_basket( $rezcam[ $key_rezcam ]['id_oferta'], $rezcam[ $key_rezcam ]['id_pret'], $rezcam[ $key_rezcam ]['plecare'], $rezcam[ $key_rezcam ]['data_start'], $pleczile, $rezcam[ $key_rezcam ]['id_camera'], $rezcam[ $key_rezcam ]['denumire_camera'], $rezcam[ $key_rezcam ]['nr_adulti'], $rezcam[ $key_rezcam ]['nr_copii'], change_meal( $rezcam[ $key_rezcam ]['masa'] ), $rezcam[ $key_rezcam ]['pret'] );
                        if ( $basket[ $key_rezcam ] === true ) {
                            echo '<a href="/ofertele-mele/" class="smaller-09em link-blue" target="_blank" title="Deschide lista cu ofertele salvate"><i class="icon-heart green"></i> <span class="underline">Ofertă salvată</span></a>';
                        } else {
                            echo '<span id="save' . $key_rezcam . $id_hotel . '" style="cursor: pointer;"><span class="smaller-09em blue underline pointer">Salvează si COMPARA</span></span>';
                            echo '<script>
                                    $("#save' . $key_rezcam . $id_hotel . '").on("click", function() {
                                        $("#save' . $key_rezcam . $id_hotel . '").load("/salveaza-oferta/?data=' . base64_encode( $rezcam[ $key_rezcam ]['id_oferta'] . '##' . $rezcam[ $key_rezcam ]['id_pret'] . '##' . $rezcam[ $key_rezcam ]['plecare'] . '##' . $rezcam[ $key_rezcam ]['data_start'] . '##' . $pleczile . '##' . $rezcam[ $key_rezcam ]['pret'] . '##' . $rezcam[ $key_rezcam ]['id_camera'] . '##' . $rezcam[ $key_rezcam ]['nr_adulti'] . '##' . $rezcam[ $key_rezcam ]['nr_copii'] . '##' . $rezcam[ $key_rezcam ]['copil1'] . '##' . $rezcam[ $key_rezcam ]['copil2'] . '##' . $rezcam[ $key_rezcam ]['copil3'] . '##' . change_meal( $rezcam[ $key_rezcam ]['masa'] ) . '##' . $rezcam[ $key_rezcam ]['denumire_camera'] ) . '");
                                    });
                                  </script>';
                        }
		            } else {
		                echo '<p class="text-center red bold" style="padding-bottom:10px;">Indisponibil</p><a href="#" class="button-blue smaller-09em pad5"><label for="calc-dataplec' . $id_hotel . '" class="white">Schimbă data</label></a>';
		            }
		            if ( ! $err_logare_admin ) {
		                $detalii_furnizor = get_detalii_furnizor( $rezcam[ $key_rezcam ]['id_furnizor'] );
		                echo '<div style="position:absolute; top:15px; right:-130px; width:130px;"><a href="/adm/editare_furnizor.php?pas=2&furnizor=' . $detalii_furnizor['id_furnizor'] . '" target="_blank" class="link-blue">' . $detalii_furnizor['denumire'] . '</a><br><a href="' . $detalii_furnizor['link_admin'] . '" target="_blank" class="link-green"><strong class="green">&laquo; Login &raquo;</strong></a></div>';
		            }
		            echo '</td>';
		            echo '</tr>';
		        }
		        echo '</table></div>';

	
		} else if ( $showerr == 'da' ) {
			if ( count( $cache_results['prices'] ) == 0 ) {
				$ins_cache = "INSERT INTO pret_cache SET
		ukey = '" . $ukey . "',
		id_hotel = '" . $id_hotel . "',
		id_pret = '0',
		plecare = '0',
		id_camera = '0',
		denumire_camera = '0',
		pret = '0',
		comision = '0',
		moneda = '0',
		nr_adulti = '0',
		nr_copii = '0',
		copil1 = '0',
		copil2 = '0',
		copil3 = '0',
		oferta = '0',
		oferta_pret = '0',
		masa = '0',
		series_name = '0',
		grila_name = '0',
		info_grad_ocupare = '0',
		period = '0',
		data_start = '0',
		data_end = '0',
		disponibilitate = '0',
		nr_nights = '0',
		id_furnizor = '0',
		furnizor_comision_procent = '0',
		id_oferta = '0',
		data_adaugarii = NOW()
		";
				//$que_cache = mysql_query($ins_cache) or die(mysql_error());
				@mysql_free_result( $que_cache );
			}

			$fara_rezervare = 'da';
			echo '<p class="black bigger-12em pad0-10">';
			echo 'Pentru <span class="blue bold">' . $adulti . ' adul';
			if ( $adulti > 1 ) {
				echo 'ți';
			} else {
				echo 't';
			}
			if ( $copii > 0 ) {
				echo '</span> și <span class="blue bold">' . $copii . ' copi';
				if ( $copii == 1 ) {
					echo 'l';
				} else {
					echo 'i';
				}
				if ( $detalii['online_prices'] == 'da' ) {
					$ages_copii = '';
					for ( $ic = 0; $ic < $copii; $ic ++ ) {
						$ages_copii .= $_REQUEST[ 'copil' . ( $ic + 1 ) ] . ' ani, ';
					}
					$ages_copii = substr( $ages_copii, 0, - 2 );
					echo ' (' . $ages_copii . ') ';
				}
			}
			echo '</span> în cameră începând cu <span class="blue bold">' . date( "d.m.Y", strtotime( $plecdata ) ) . '</span>, <span class="blue bold">' . $pleczile . ' nopți</span> nu am găsit disponibilitate.<br>';
			if ( $adulti + $copii >= 4 ) {
				echo 'Vă rugăm să încercaţi <span class="underline bold">o altă repartizare în cameră</span> (mai puţine persoane).';
			} else {
				echo 'Vă rugăm să modificați <span class="underline bold">data de plecare</span>.';
			}
			echo '</p>';

			if ( ! is_bot() and isset( $err_logare_admin ) ) {
				erori_disponibilitate( null, $id_hotel, $plecdata, $pleczile, $adulti, $copii );
			}
		}
		?>
           <?php /*?> <script>
                $(document).ready(function () {
                    $(".flight_schedule").each(function () {
                        var $id = $(this).attr('id');
                        var $dialog = $('#flight' + $id)
                            .dialog({
                                modal: true,
                                autoOpen: false,
                                resizable: false,
                                title: "Orar de zbor",
                                width: 750,
                                open: function () {
                                    jQuery('.ui-widget-overlay').bind('click', function () {
                                        jQuery('#flight' + $id).dialog('close');
                                    })
                                }
                            });
                        $(this).click(function () {
                            $dialog.dialog('open');
                            return false;
                        });
                    });
                });
            </script><?php */?>
			<?php
		} //end if($rezcam[0]['pret']!=0 and $rezcam[0]['disponibilitate']!=0)

	} //end if(isset($_COOKIE['grad_ocupare']))


	?>
