<?php $id_hotel = $_REQUEST['hid'];
    //print_r($_POST);
$sel_hot=" Select hoteluri.*,
localitati.denumire AS denumire_localitate,
zone.denumire AS denumire_zona,
tari.denumire AS denumire_tara,
tari.id_tara,
zone.id_zona
from
hoteluri
Left Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Left Join zone ON localitati.id_zona = zone.id_zona
left Join tari ON zone.id_tara = tari.id_tara
where hoteluri.id_hotel = '$id_hotel' ";
$rez_oferta=mysql_query($sel_hot) or die(mysql_error());
$detalii=@mysql_fetch_array($rez_oferta);
$id_zona=$detalii['id_zona'];
$id_tara=$detalii['id_tara'];
@mysql_free_result($rez_oferta);
?>
<div id="detaliiFormulare" align="left">

<h1>Adauga review la <?php echo $nume_hotel; ?> <img src="/images/spacer.gif" class="stele-mari-<? echo $detalii["stele"]; ?>" alt="" /></h1>

  <div id="gallery" class="gallery" style="float:left; margin-right:15px;">
  <?php
  $j=1;
  for($i=1;$i<=8 && $j<=1; $i++)
		{
		
			if($detalii["poza".$i]!=NULL) 
			{
				$j++;
				$nume_poza=$detalii["poza".$i];
				$titlu_poza=$detalii["numa_poza".$i];
				?>
                <img src="http://www.ocaziituristice.ro/thumb_hotel/<?php echo $nume_poza; ?>" class="imageDescriere" alt="<?php echo ucwords(strtolower($detalii["nume"])); ?>-<?php echo $detalii["denumire_tara"]; ?>" title="<?php echo ucwords(strtolower($detalii["nume"])); ?>- <?php echo $detalii["denumire_tara"]; ?>" />
                
				<?php
			}
		}
	?>
  </div>

  <p style="float:left;"><?php echo afiseaza_frumos(remove_special_characters(nl2p(strip_tags($detalii["descriere"])))); ?></p>    

  <br class="clear" />
  
  <div style="border:1px solid #CCC; -moz-border-radius:6px; -webkit-border-radius:6px; border-radius:6px; padding:5px 10px; margin:5px; font-size:13px; line-height:140%;">
	<a href="#reviews" style="display:block; width:300px; float:left;">&raquo; Vezi toate review-urile acestui hotel</a>
	<a href="<?php echo $path."/hoteluri/".fa_link($row_h['denumire_tara'])."/".fa_link($row_h['denumire_zona'])."/".fa_link($nume_hotel)."-".$id_hotel.".html"; ?>" style="display:block; width:300px; float:left;">&raquo; Vezi toate ofertele de la <?php echo $nume_hotel; ?></a>
    <br class="clear" />
  </div>
  
  <br class="clear" /><br />
  
  <div style="border-bottom:1px solid #CCC"></div>
  
  <br class="clear" /><br />

<form name="review_hotel" action="" method="post" class="reviewForm">
<label><span class="small"><strong>Tara:</strong><span style="color: #ff0000;">*</span></span> 
  <input type="text" name="tara" style="width: 350px;<?php if(isset($_POST['tara']) && empty($_POST['tara'])) { echo ' border-color: #ff0000;'; } ?>" value="<?php if(isset($_POST['tara']) && !empty($_POST['tara'])) { echo $_POST['tara']; } else { echo $nume_tara; } ?>" /><?php if(isset($_POST['tara']) && empty($_POST['tara'])) { echo '&nbsp;<span style="color: #ff0000;">Completati campul TARA.</span>'; } ?></label>
<label><span class="small"><strong>Localitate:</strong></span> 
  <input type="text" name="localitate" style="width: 350px;" value="<?php if(isset($_POST['localitate']) && !empty($_POST['localitate'])) { echo $_POST['localitate']; } else { echo $nume_localitate; } ?>" /></label> 
<label><span class="small"><strong>Hotel:</strong></span> <input type="text" name="hotel" style="width: 350px;" value="<?php if(isset($_POST['hotel']) && !empty($_POST['hotel'])) { echo $_POST['hotel']; } else { echo $nume_hotel; } ?>" /></label>
<br />
<br />
<div class="blue" style="padding-bottom:6px;">Acordati o nota de la 1 la 10 pentru fiecare serviciu de la <strong><?php echo $nume_hotel; ?></strong>:</div>
<?php
if(isset($_POST['btnReviewHotelContinuare']) && (!isset($_POST['servicii']) || !isset($_POST['locatie']) || !isset($_POST['comfort']) || !isset($_POST['personal']) || !isset($_POST['curatenie'])))
{
    echo '<div style="margin-left: 200px;"';
    if(!isset($_POST['servicii']))
    {
        echo '<span style="color: #ff0000;">&raquo;&nbsp;Alegeti nota pentru SERVCII.</span><br />';    
    }
    if(!isset($_POST['locatie']))
    {
        echo '<span style="color: #ff0000;">&raquo;&nbsp;Alegeti nota pentru LOCATIE.</span><br />';    
    }
    if(!isset($_POST['comfort']))
    {
        echo '<span style="color: #ff0000;">&raquo;&nbsp;Alegeti nota pentru COMFORT.</span><br />';    
    }
    if(!isset($_POST['personal']))
    {
        echo '<span style="color: #ff0000;">&raquo;&nbsp;Alegeti nota pentru PERSONAL.</span><br />';    
    }
    if(!isset($_POST['curatenie']))
    {
        echo '<span style="color: #ff0000;">&raquo;&nbsp;Alegeti nota pentru CURATENIE.</span><br />';    
    }
    echo '</div></br />';
}
?>
<table width="100%">
    <tr>
        <td width="50%">
          <div style="float:left; width:75px;"><strong>Servicii:</strong><span style="color: #ff0000;">*</span></div>  
          <div class="multiField" id="ratings1">
            <label for="vote1" class="blockLabel"><input type="radio" name="servicii" id="vote1" value="1"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 1) { echo ' checked="checked"'; } ?> /> 1</label>
            <label for="vote2" class="blockLabel"><input type="radio" name="servicii" id="vote2" value="2"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 2) { echo ' checked="checked"'; } ?> /> 2</label>
            <label for="vote3" class="blockLabel"><input type="radio" name="servicii" id="vote3" value="3"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 3) { echo ' checked="checked"'; } ?> /> 3</label>
            <label for="vote4" class="blockLabel"><input type="radio" name="servicii" id="vote4" value="4"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 4) { echo ' checked="checked"'; } ?> /> 4</label>
            <label for="vote5" class="blockLabel"><input type="radio" name="servicii" id="vote5" value="5"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 5) { echo ' checked="checked"'; } ?> /> 5</label>
            <label for="vote6" class="blockLabel"><input type="radio" name="servicii" id="vote6" value="6"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 6) { echo ' checked="checked"'; } ?> /> 6</label>
            <label for="vote7" class="blockLabel"><input type="radio" name="servicii" id="vote7" value="7"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 7) { echo ' checked="checked"'; } ?> /> 7</label>
            <label for="vote8" class="blockLabel"><input type="radio" name="servicii" id="vote8" value="8"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 8) { echo ' checked="checked"'; } ?> /> 8</label>
            <label for="vote9" class="blockLabel"><input type="radio" name="servicii" id="vote9" value="9"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 9) { echo ' checked="checked"'; } ?> /> 9</label>
            <label for="vote10" class="blockLabel"><input type="radio" name="servicii" id="vote10" value="10"<?php if(isset($_POST['servicii']) && $_POST['servicii'] == 10) { echo ' checked="checked"'; } ?> /> 10</label>
          </div>
          <br class="clear" />
          
          <div style="float:left; width:75px;"><strong>Locatie:</strong><span style="color: #ff0000;">*</span></div>
          <div class="multiField" id="ratings2">
            <label for="vote1" class="blockLabel"><input type="radio" name="locatie" id="vote1" value="1"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 1) { echo ' checked="checked"'; } ?> /> 1</label>
            <label for="vote2" class="blockLabel"><input type="radio" name="locatie" id="vote2" value="2"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 2) { echo ' checked="checked"'; } ?> /> 2</label>
            <label for="vote3" class="blockLabel"><input type="radio" name="locatie" id="vote3" value="3"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 3) { echo ' checked="checked"'; } ?> /> 3</label>
            <label for="vote4" class="blockLabel"><input type="radio" name="locatie" id="vote4" value="4"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 4) { echo ' checked="checked"'; } ?> /> 4</label>
            <label for="vote5" class="blockLabel"><input type="radio" name="locatie" id="vote5" value="5"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 5) { echo ' checked="checked"'; } ?> /> 5</label>
            <label for="vote6" class="blockLabel"><input type="radio" name="locatie" id="vote6" value="6"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 6) { echo ' checked="checked"'; } ?> /> 6</label>
            <label for="vote7" class="blockLabel"><input type="radio" name="locatie" id="vote7" value="7"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 7) { echo ' checked="checked"'; } ?> /> 7</label>
            <label for="vote8" class="blockLabel"><input type="radio" name="locatie" id="vote8" value="8"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 8) { echo ' checked="checked"'; } ?> /> 8</label>
            <label for="vote9" class="blockLabel"><input type="radio" name="locatie" id="vote9" value="9"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 9) { echo ' checked="checked"'; } ?> /> 9</label>
            <label for="vote10" class="blockLabel"><input type="radio" name="locatie" id="vote10" value="10"<?php if(isset($_POST['locatie']) && $_POST['locatie'] == 10) { echo ' checked="checked"'; } ?> /> 10</label>
          </div>
          <br class="clear" />
          
          <div style="float:left; width:75px;"><strong>Confort:</strong><span style="color: #ff0000;">*</span></div>
          <div class="multiField" id="ratings3">
            <label for="vote1" class="blockLabel"><input type="radio" name="comfort" id="vote1" value="1"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 1) { echo ' checked="checked"'; } ?> /> 1</label>
            <label for="vote2" class="blockLabel"><input type="radio" name="comfort" id="vote2" value="2"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 2) { echo ' checked="checked"'; } ?> /> 2</label>
            <label for="vote3" class="blockLabel"><input type="radio" name="comfort" id="vote3" value="3"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 3) { echo ' checked="checked"'; } ?> /> 3</label>
            <label for="vote4" class="blockLabel"><input type="radio" name="comfort" id="vote4" value="4"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 4) { echo ' checked="checked"'; } ?> /> 4</label>
            <label for="vote5" class="blockLabel"><input type="radio" name="comfort" id="vote5" value="5"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 5) { echo ' checked="checked"'; } ?> /> 5</label>
            <label for="vote6" class="blockLabel"><input type="radio" name="comfort" id="vote6" value="6"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 6) { echo ' checked="checked"'; } ?> /> 6</label>
            <label for="vote7" class="blockLabel"><input type="radio" name="comfort" id="vote7" value="7"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 7) { echo ' checked="checked"'; } ?> /> 7</label>
            <label for="vote8" class="blockLabel"><input type="radio" name="comfort" id="vote8" value="8"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 8) { echo ' checked="checked"'; } ?> /> 8</label>
            <label for="vote9" class="blockLabel"><input type="radio" name="comfort" id="vote9" value="9"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 9) { echo ' checked="checked"'; } ?> /> 9</label>
            <label for="vote10" class="blockLabel"><input type="radio" name="comfort" id="vote10" value="10"<?php if(isset($_POST['comfort']) && $_POST['comfort'] == 10) { echo ' checked="checked"'; } ?> /> 10</label>
          </div>
          <br class="clear" />
        </td>
        <td width="50%" valign="top">
          <div style="float:left; width:75px;"><strong>Personal:</strong><span style="color: #ff0000;">*</span></div>
          <div class="multiField" id="ratings4">
            <label for="vote1" class="blockLabel"><input type="radio" name="personal" id="vote1" value="1"<?php if(isset($_POST['personal']) && $_POST['personal'] == 1) { echo ' checked="checked"'; } ?> /> 1</label>
            <label for="vote2" class="blockLabel"><input type="radio" name="personal" id="vote2" value="2"<?php if(isset($_POST['personal']) && $_POST['personal'] == 2) { echo ' checked="checked"'; } ?> /> 2</label>
            <label for="vote3" class="blockLabel"><input type="radio" name="personal" id="vote3" value="3"<?php if(isset($_POST['personal']) && $_POST['personal'] == 3) { echo ' checked="checked"'; } ?> /> 3</label>
            <label for="vote4" class="blockLabel"><input type="radio" name="personal" id="vote4" value="4"<?php if(isset($_POST['personal']) && $_POST['personal'] == 4) { echo ' checked="checked"'; } ?> /> 4</label>
            <label for="vote5" class="blockLabel"><input type="radio" name="personal" id="vote5" value="5"<?php if(isset($_POST['personal']) && $_POST['personal'] == 5) { echo ' checked="checked"'; } ?> /> 5</label>
            <label for="vote6" class="blockLabel"><input type="radio" name="personal" id="vote6" value="6"<?php if(isset($_POST['personal']) && $_POST['personal'] == 6) { echo ' checked="checked"'; } ?> /> 6</label>
            <label for="vote7" class="blockLabel"><input type="radio" name="personal" id="vote7" value="7"<?php if(isset($_POST['personal']) && $_POST['personal'] == 7) { echo ' checked="checked"'; } ?> /> 7</label>
            <label for="vote8" class="blockLabel"><input type="radio" name="personal" id="vote8" value="8"<?php if(isset($_POST['personal']) && $_POST['personal'] == 8) { echo ' checked="checked"'; } ?> /> 8</label>
            <label for="vote9" class="blockLabel"><input type="radio" name="personal" id="vote9" value="9"<?php if(isset($_POST['personal']) && $_POST['personal'] == 9) { echo ' checked="checked"'; } ?> /> 9</label>
            <label for="vote10" class="blockLabel"><input type="radio" name="personal" id="vote10" value="10"<?php if(isset($_POST['personal']) && $_POST['personal'] == 10) { echo ' checked="checked"'; } ?> /> 10</label>
          </div>
          <br class="clear" />
          
          <div style="float:left; width:75px;"><strong>Curatenie:</strong><span style="color: #ff0000;">*</span></div>
          <div class="multiField" id="ratings5">
            <label for="vote1" class="blockLabel"><input type="radio" name="curatenie" id="vote1" value="1"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 1) { echo ' checked="checked"'; } ?> /> 1</label>
            <label for="vote2" class="blockLabel"><input type="radio" name="curatenie" id="vote2" value="2"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 2) { echo ' checked="checked"'; } ?> /> 2</label>
            <label for="vote3" class="blockLabel"><input type="radio" name="curatenie" id="vote3" value="3"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 3) { echo ' checked="checked"'; } ?> /> 3</label>
            <label for="vote4" class="blockLabel"><input type="radio" name="curatenie" id="vote4" value="4"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 4) { echo ' checked="checked"'; } ?> /> 4</label>
            <label for="vote5" class="blockLabel"><input type="radio" name="curatenie" id="vote5" value="5"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 5) { echo ' checked="checked"'; } ?> /> 5</label>
            <label for="vote6" class="blockLabel"><input type="radio" name="curatenie" id="vote6" value="6"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 6) { echo ' checked="checked"'; } ?> /> 6</label>
            <label for="vote7" class="blockLabel"><input type="radio" name="curatenie" id="vote7" value="7"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 7) { echo ' checked="checked"'; } ?> /> 7</label>
            <label for="vote8" class="blockLabel"><input type="radio" name="curatenie" id="vote8" value="8"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 8) { echo ' checked="checked"'; } ?> /> 8</label>
            <label for="vote9" class="blockLabel"><input type="radio" name="curatenie" id="vote9" value="9"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 9) { echo ' checked="checked"'; } ?> /> 9</label>
            <label for="vote10" class="blockLabel"><input type="radio" name="curatenie" id="vote10" value="10"<?php if(isset($_POST['curatenie']) && $_POST['curatenie'] == 10) { echo ' checked="checked"'; } ?> /> 10</label>
          </div>
          <br class="clear" />        
        </td>
    </tr>
    <tr>
        <td>
            <em style="font-size:12px; color:#666;">*) pentru a selecta nota faceti clic pe steaua dorita</em>
        </td>
    </tr>
</table>
<br />
<span class="blue"><strong>Detaliaza experienta ta la acest hotel:</strong></span><span style="color: #ff0000;">*</span>
<?php if(isset($_POST['descriere_vacanta']) && empty($_POST['descriere_vacanta'])) { echo '<span style="color: #ff0000;">&nbsp;Completati descrierea excursiei.</span>'; } if(isset($_POST['descriere_vacanta']) && !empty($_POST['descriere_vacanta']) && $prea_scurt == 1) { echo '<span style="color: #ff0000;">&nbsp;Descrierea trebuie sa contina cel putin 50 de caractere.</span>'; } ?>
<br />
<textarea name="descriere_vacanta" style="width:630px; height:250px;<?php if((isset($_POST['descriere_vacanta']) && empty($_POST['descriere_vacanta'])) || (isset($_POST['descriere_vacanta']) && !empty($_POST['descriere_vacanta']) && $prea_scurt == 1)) { echo ' border-color: #ff0000;'; } ?>"><?php if(isset($_POST['descriere_vacanta']) && !empty($_POST['descriere_vacanta'])) { echo $_POST['descriere_vacanta']; } ?></textarea>
<br/>
<br />
<p><strong><em>Campurile marcate cu <span style="color: #ff0000;">*</span> sunt obligatorii.</em></strong></p>
<br />
<div align="center">
    <input type="image" name="btnReviewHotelContinuare" src="/images/buton_continuare.jpg" width="154" height="30" style="border: 0;" value="Continuare" />
    <input type="hidden" name="contor" value="<?php echo $contor; ?>" />
</div>
<br />
</form>

<br class="clear" />

</div>

<div class="opinii-Vizitatori">
  <div style="float:right; padding:3px 15px; color:#666;"><?php echo ucwords(strtolower($detalii["nume"])); ?></div>
  <a name="reviews"></a>
  <h2 class="blue underline">Parerile Turistilor</h2>
  <?php
    $username = "";
    $query = "select ID, Review, DataAdaugare, Email, Nume, Prenume, Nota_Servicii, Nota_Locatie, Nota_Comfort, Nota_Personal, Nota_Curatenie, ReviewUtil, ReviewInutil from reviews where HotelID = '".$detalii['id_hotel']."' and StareID = '1' ORDER BY DataAdaugare DESC";
    $result = mysql_query($query) or die(mysql_error());
    if(mysql_num_rows($result))
    {
        while($row = mysql_fetch_assoc($result))
        {
            if(!empty($row['Email']))
            {
                /*
                $query2 = "SELECT users_profil.user_profil as username FROM usernames_fizice Inner Join users_profil ON usernames_fizice.id = users_profil.id_utilizator where usernames_fizice.mail = '".$row['Email']."'";
                $result2 = mysql_query($query2) or die(mysql_error());
                if(mysql_num_rows($result2))
                {
                    while($row2 = mysql_fetch_assoc($result2))
                    {
                        if(!empty($row2['username']))
                        {
                            $username = $row2['username'];
                        }
                    }
                }
                else
                {
                 */
                    $username = $row['Nume']." ".$row['Prenume'];
                //}
            }
            else
            {
                $username = "Anonim";
            }
            ?>
              <table cellpadding="0" cellspacing="0" border="0" width="100%" class="reviews">
                <tr>
                  <td align="left" valign="top" colspan="3">
                    <div class="titlu">
                        <div style="float: left;"><?php echo $username; ?> | <span class="data"><?php echo substr($row['DataAdaugare'], 8, 2).".".substr($row['DataAdaugare'], 5, 2).".".substr($row['DataAdaugare'], 0, 4); ?></span></div>
                        <div style="float:right; font-size:12px; font-weight:normal;" class="blue">
                        <?php
                            if($row['ReviewUtil'] > 0 && $row['ReviewInutil'] > 0)
                            {
                                echo '<span>'.$row['ReviewUtil'].' din '.($row['ReviewUtil']+$row['ReviewInutil']).' utilizatori au considerat util acest review</span>';    
                            }
                            //
                            if($row['ReviewUtil'] > 0 && $row['ReviewInutil'] == 0)
                            {
                                echo '<span>'.$row['ReviewUtil'].' din '.$row['ReviewUtil'].' utilizatori au considerat util acest review</span>';
                            }
                        ?>                    
                        </div>
                        <br class="clear" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td align="left" valign="top" width="210">
                    <div style="float:left; width:65px;">Servicii:</div>
                    <span class="votingStars<?php echo $row['Nota_Servicii']; ?>"><?php echo $row[6]; ?></span>
                    <br class="clear" />
                    
                    <div style="float:left; width:65px;">Locatie:</div>
                    <span class="votingStars<?php echo $row['Nota_Locatie']; ?>"><?php echo $row[7]; ?></span>
                    <br class="clear" />
                    
                    <div style="float:left; width:65px;">Comfort:</div>
                    <span class="votingStars<?php echo $row['Nota_Comfort']; ?>"><?php echo $row[8]; ?></span>
                    <br class="clear" />
                    
                    <div style="float:left; width:65px;">Personal:</div>
                    <span class="votingStars<?php echo $row['Nota_Personal']; ?>"><?php echo $row[9]; ?></span>
                    <br class="clear" />
                    
                    <div style="float:left; width:65px;">Curatenie:</div>
                    <span class="votingStars<?php echo $row['Nota_Curatenie']; ?>"><?php echo $row[10]; ?></span>
                    <br class="clear" />
                  </td>
                  <td align="left" valign="top" style="background:url(/images/despartitor_servicii.gif) top center repeat-y;"><img src="<?php echo $imgpath; ?>/spacer.gif" width="5" height="1" alt="" /></td>
                  <td align="justify" valign="top">
                    <?php echo str_replace("\n", "<br />", $row['Review']); ?>
                    <br class="clear" />
                    <div class="util" align="right">
                      A fost util acest review?
                       <form action="" method="post">
                        <input type="submit" name="btnReviewUtil" value="Da" />
                        <input type="submit" name="btnReviewInutil" value="Nu" />
                        <input type="hidden" name="rid" value="<?php echo $row['ID']; ?>" />
                      </form>
                    </div>
                  </td>
                </tr>
              </table>
            <?php
        }
    }
    else {}
  ?>
  <br class="clear" /><br />
</div>
