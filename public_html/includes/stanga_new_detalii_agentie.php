<?php $agentia=desfa_link($_REQUEST['agentia']);
$id_agentie=get_id_agentie($agentia);
if($id_agentie) {
$agentie=new AGENTIE($id_agentie);
$agentie->load_date(); ?>	

<div id="detaliiOferta" style="padding:10px;">

<?php //afiseaza denumirea campurilor de intrare
/*$agentie->afiseaza_denumire_camp();*/
/*echo "<br/><br/>";*/
//EXEMPLU:
//afisati valuearea unui camp dorit
/*echo $agentie->afiseaza_camp('denumire_utilizator');*/ ?>

  <h1 class="normal"><?php echo $agentie->afiseaza_camp('denumire_utilizator'); ?></h1>
  <img src="<?php echo $sitepath."sigle_agentii/".$agentie->afiseaza_camp('sigla'); ?>" style="float:left;" class="image-border" />
  <?php echo $agentie->afiseaza_camp('prezentare_agentie'); ?>
  
  <div class="clear"></div>
  <br />

  <h2 class="blue" style="border-bottom:2px solid;">Date de contact</h2>
  <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="date-contact">
    <tr>
      <td width="140" align="left" valign="top"><strong>Localitate:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('localitate'); ?></td>
    </tr>
    <tr class="grey">
      <td align="left" valign="top"><strong>Tara:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('tara'); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top"><strong>Adresa:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('adresa'); ?></td>
    </tr>
    <tr class="grey">
      <td align="left" valign="top"><strong>Persoana contact:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('persoana_contact'); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top"><strong>Telefon:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('telefon'); ?> <?php echo $agentie->afiseaza_camp('alt_telefon'); ?> </td>
    </tr>
    <tr class="grey">
      <td align="left" valign="top"><strong>Fax:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('fax'); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top"><strong>Website:</strong></td>
      <td align="left" valign="top"><a href="<?php echo $agentie->afiseaza_camp('site_web'); ?>" target="_blank"><?php echo str_replace("http://","",$agentie->afiseaza_camp('site_web')); ?></a></td>
    </tr>
    <tr class="grey">
      <td align="left" valign="top"><strong>E-mail:</strong></td>
      <td align="left" valign="top"><a href="mailto:<?php echo $agentie->afiseaza_camp('mail'); ?>" target="_blank"><?php echo $agentie->afiseaza_camp('mail'); ?></a></td>
    </tr>
    <tr>
      <td align="left" valign="top"><strong>Program:</strong></td>
      <td align="left" valign="top"><?php echo nl2br($agentie->afiseaza_camp('program_lucru')); ?></td>
    </tr>
  </table>

  <br />
  
  <h2 class="blue" style="border-bottom:2px solid;">Date despre firma</h2>
  <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="date-contact">
    <tr>
      <td width="140" align="left" valign="top"><strong>Nume firma:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('denumire_firma'); ?></td>
    </tr>
    <tr class="grey">
      <td align="left" valign="top"><strong>Tip firma:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('tip'); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top"><strong>Cod unic de inregistrare:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('cod_fiscal'); ?></td>
    </tr>
    <tr class="grey">
      <td align="left" valign="top"><strong>Nr. registrul comertului:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('reg_comertului'); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top"><strong>Adresa:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('adresa_firma'); ?></td>
    </tr>
    <tr class="grey">
      <td align="left" valign="top"><strong>Banca:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('banca'); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top"><strong>Cont bancar:</strong></td>
      <td align="left" valign="top"><?php echo $agentie->afiseaza_camp('cont_bancar'); ?></td>
    </tr>
  </table>
  
  <div class="clear"></div>
</div>

<?php } ?>