<?php include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php"); ?>
<?php /*?>
<div class="info-alege NEW-round8px">
  <h2 class="black underline" style="font-size:2em;"><img src="<?php echo $imgpath; ?>/icon_thumbs_up.png" alt="" style="vertical-align:middle;" /> Avantajele Ocaziituristice.ro</h2>
  <ul class="clearfix">
	<li class="NEW-round6px"><span class="red">Siguranță!</span> Folosim <strong>cei mai mari</strong> tour-operatori din țară și de peste hotare.<br /><a href="<?php echo $sitepath; ?>info-parteneri.html" target="_blank" rel="infos" title="Parteneri Ocaziituristice.ro" class="link-blue">Click aici</a> pentru lista partenerilor noștri.</li>
	<li class="NEW-round6px"><span class="red">Comoditate!</span> Faci o rezervare din fața calculatorului sau a dispozitivului tău mobil fără a te mai deplasa până la sediul agenției.</li>
	<li class="NEW-round6px"><span class="red">Transparență!</span> Afișăm <strong>toate detaliile</strong> necesare la fiecare oferta. <strong>Fără taxe sau comisioane ascunse.</strong></li>
	<li class="NEW-round6px"><span class="red">Ieftin!</span> Comparăm ofertele partenerilor noștri pentru a alege pentru dumneavoastră <strong>cel mai mic preț</strong> posibil.</li>
	<li class="NEW-round6px"><span class="red">Asistență online!</span> Suntem la dispoziția dumneavoastră prin intermediul internetului, ca să nu vă mai incărcați factura de la telefon.</li>
	<li class="NEW-round6px"><span class="red">Fără taxe de rezervare!</span> <strong>Nu percepem taxe de rezervare</strong> și nici alte comisioane, nici măcar la plata cu cardul!</li>
  </ul>
</div>
<?php */?>

<div class="column" style="width:170px;">
  <p class="titlu red">Link-uri utile</p>
  <a href="<?php echo $sitepath; ?>" title="Prima pagina" class="item" rel="nofollow">Prima pagina</a>
  <a href="<?php echo $sitepath; ?>termeni_si_conditii.html" title="Termeni si Conditii de utilizare" class="item" rel="nofollow">Termeni si Conditii</a>
  <a href="<?php echo $sitepath; ?>files/Contractul cu Turistul.pdf" title="Contractul cu turistul" target="_blank" class="item" rel="nofollow">Contractul cu turistul</a>
  <a href="<?php echo $sitepath; ?>contact.html" title="Contact" class="item" rel="nofollow">Contact</a>
  <?php /*?><a href="<?php echo $sitepath; ?>parteneri_ocaziituristice.html" title="Parteneri Ocazii Turistice" class="item">Devino partener OcaziiTuristice</a>
  <a href="<?php echo $sitepath; ?>files/cui.jpg" title="Certificat Unic de Inregistrare" target="_blank" class="item">Certificat de inregistrare</a>
  <a href="<?php echo $sitepath; ?>files/licenta_turism.jpg" title="Licenta de Turism" target="_blank" class="item">Licenta de turism</a>
  <a href="<?php echo $sitepath; ?>files/brevet_turism.jpg" title="Brevet Turism" target="_blank" class="item">Brevet de turism</a>
  <a href="<?php echo $sitepath; ?>files/asigurare.jpg" title="Polita de asigurare" target="_blank" class="item">Polita de asigurare</a><?php */?>
  <a href="http://www.anpc.gov.ro/" title="Autoritatea Nationala pentru Protectia Consumatorilor" target="_blank" rel="nofollow" class="item">Protectia Consumatorului</a>
</div>

<?php /*?><div class="column" style="width:120px;">
  <p class="titlu red">Ne gasiti pe</p>
  <a href="<?php echo $facebook_page; ?>" target="_blank" title="Facebook Ocazii Turistice" class="item">Facebook</a>
  <a href="https://twitter.com/ocaziituristice" target="_blank" title="Twitter Ocazii Turistice" class="item">Twitter</a>
  <a href="http://blog.ocaziituristice.ro/" target="_blank" title="Blog Ocazii Turistice" class="item">Blog</a>
</div><?php */?>

<div class="column" style="width:120px;">
  <p class="titlu red">Info</p>
  <a href="<?php echo $sitepath; ?>info-cum-cumpar.html" title="Cum cumpar" class="item" rel="nofollow">Cum cumpar</a>
  <a href="<?php echo $sitepath; ?>info-cum-platesc.html" title="Cum platesc" class="item" rel="nofollow">Cum platesc</a>
</div>

<?php /*<div class="column" style="width:150px;">
  <p class="titlu red">Parteneri</p>
  <a href="http://www.felicitari.afix.ro" target="_blank" title="Felicitari aniversare La multi ani !" class="item">Felicitari</a>
  <a title="Cazare Vatra Dornei, Oferte cazare pensiuni, hotel, cabane, Revelion Vatra Dornei" href="http://www.vatra-dornei.info/cazare-vatra-dornei.html" target="_blank" class="item">Cazare Vatra Dornei</a>
  <a href="http://www.macazez.ro" target="_blank" title="Cazare Litoral" class="item">Cazare Litoral</a>
</div>*/
?>

<div class="column" align="center">
  <p class="titlu red">Cumparaturi online in siguranta</p>
  <a href="https://secure2.plationline.ro/compliance.asp?f_login=www%2Eocaziituristice%2Ero&f_lang=ro" target="_blank"><img src="https://secure2.plationline.ro/compliance/images_sigle_po/sigla_verificare_PlatiOnline.ro_170x43_2_RO.png" alt="Sigla PlatiOnline.ro" /></a><br />
  <img src="https://secure2.plationline.ro/images/sigle/80px-Mastercard_SecureCode_Logo.png" alt="MasterCard SecureCode" />
  <img src="https://secure2.plationline.ro/images/sigle/80px-VerifiedByVISA_Logo.png" alt="Verified by VISA" />
</div>

<div class="column" style="width:120px;">
  <a href="https://plus.google.com/u/0/b/100372161073365189899/100372161073365189899?rel=author">Google</a>
</div>

<div class="column" style="float:right; width:200px;" align="justify">
  <p><strong>Telefon:</strong> <span class="red" style="font-size:16px; font-weight:bold;"><?php echo $contact_telefon; ?></span></p>
  
  <p><strong>Adresa:</strong> <?php echo $contact_adresa; ?></p>
</div>

<br class="clear" />
