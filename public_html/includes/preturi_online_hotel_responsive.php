<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );

include( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/suppliers_xml.php' );

/*** check login admin ***/
$logare_admin     = new LOGIN( 'useri' );
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/
//$det_oferta = new DETALII_SEJUR();

$id_hotel      = $_REQUEST['idh'];
$det           = new DETALII_HOTEL();
$detalii_hotel = $det->select_camp_hotel( $id_hotel );
if ( isset( $_REQUEST['trans'] ) ) {
    $detalii_online = $det->oferte_online( $id_hotel, date( "Y-m-d", strtotime( $_REQUEST['plecdata'] ) ), $_REQUEST['trans'] );
} else {
    $detalii_online = $det->oferte_online( $id_hotel, date( "Y-m-d", strtotime( $_REQUEST['plecdata'] ) ) );
}

$pleczile            = $_REQUEST['pleczile'];
$plecnopti           = $pleczile - 1;
$GLOBALS['plecdata'] = date( "Y-m-d", strtotime( $_REQUEST['plecdata'] ) );
$arivdata            = date( 'Y-m-d', strtotime( $plecdata . ' + ' . $plecnopti . ' days' ) );
$adulti              = $_REQUEST['adulti'];
$copii               = $_REQUEST['copii'];
$copil1              = $_REQUEST['copil1'];
$copil2              = $_REQUEST['copil2'];
$copil3              = $_REQUEST['copil3'];
$copil_age[0]        = $copil1;
$copil_age[1]        = $copil2;
$copil_age[2]        = $copil3;
$orasplec            = $_REQUEST['orasplec'];
$tip_transport       = $_REQUEST['trans'];
if ( $orasplec == '' ) {
    $orasplec = 51;
}
$detalii['moneda'] = $_REQUEST['moneda'];

$showerr          = $_REQUEST['showerr'];
$stop_sales_hotel = $det->stop_sales( $id_hotel, $plecdata, $arivdata );
if ( ! $err_logare_admin ) {
    //echo "rrrrrrrrrrrrrrrrrrrrr".$showerr ;

    //$servicii=$det->offer_services($id_hotel, $id_transport, $nr_nopti=NULL, $oras_plecare=NULL, $oras_destinatie=NULL, $id_oferta=5740,$e_circuit=false);
    //echo '<pre>';print_r($servicii);echo '</pre>';
}
$rezcam = array();
if ( $copii == 0 ) {
    $copil1 = 0;
    $copil2 = 0;
    $copil3 = 0;
}

$ukey                    = date( "Y-m-d", strtotime( $plecdata ) ) . $pleczile . $adulti . $copii . $copil1 . $copil2 . $copil3 . $tip_transport . $orasplec;
$cache_results['prices'] = cache_prices( $ukey, $id_hotel );
$cache_results['planes'] = cache_planes( $ukey, $id_hotel, $id_zbor );

if ( count( $cache_results['prices'] ) > 0 ) {

    //$rezcam = $cache_results['prices'];
    //$flight = $cache_results['planes'];
}


if ( isset( $detalii_online['furnizor'] ) ) {
    foreach ( $detalii_online['furnizor'] as $key_furnizor => $id_furnizor ) {
        $furnizor      = get_detalii_furnizor( $id_furnizor );
        $one_time_only = 'nu';
        foreach ( $detalii_online['id_oferta'][ $id_furnizor ] as $id_oferta ) {
            $skip_offer = 'nu';
            if ( isset( $orasplec ) ) {
                if ( $orasplec != $detalii_online['oras_plecare'][ $id_oferta ][0] ) {
                    $skip_offer = 'da';
                }
            }
            if ( $detalii_online['cazare'][ $id_oferta ] == 'nu' and $pleczile != $detalii_online['nr_nopti'][ $id_oferta ] ) {
                //echo "zileeee".$pleczile;
                $skip_offer = 'da';
            }

            // generare div

            $oras_destinatie = array_unique( $detalii_online['oras_destinatie'] );
            //$services = $det->offer_services($id_hotel, $tip_transport, $pleczile, get_den_localitate($orasplec), $oras_destinatie[key($oras_destinatie)][1], reset($detalii_online['id_oferta'][key($detalii_online['id_oferta'])]));


            $detalii['cazare'] = $detalii_online['cazare'][ $id_oferta ];

            $detalii['transport'] = $detalii_online['transport'][ $id_oferta ];
            if ( $detalii['transport'] == 'Avion' ) {
                $detalii['cazare'] = 'nu';
            }
            $detalii['tip_preturi']      = $detalii_online['tip_preturi'][ $id_oferta ];
            $detalii['id_oferta_pivot']  = $detalii_online['id_oferta_pivot'][ $id_oferta ];
            $detalii['nr_formula_pivot'] = $detalii_online['nr_formula_pivot'][ $id_oferta ];
            $detalii['masa']             = $detalii_online['masa'][ $id_oferta ];

            if ( $skip_offer == 'nu' and $one_time_only == 'nu' ) {
                switch ( $id_furnizor ) {
                    case '2':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/p45.php' );
                        break;
                    case '52':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/malta.php' );
                        break;
                    case '1':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/teztour.php' );
                        break;
                    case '398':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/bibi_touring.php' );
                        break;
                    case '330':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/paradis.php' );
                        break;
                    case '397':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/aerotravel.php' );
                        break;
                    case '225':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/prestige.php' );
                        break;
                    case '15':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/accent.php' );
                        break;
                    case '22':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/cocktails.php' );
                        break;
                    case '6':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/christian.php' );
                        break;
                    case '216':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/melitours.php' );
                        break;

                    //	if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; } break;
                    case '474':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/fibula.php' );
                        break;
                    case '480':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/romadria.php' );
                        break;
                    case '218':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/transilvania.php' );
                        break;
                    case '486':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/laguna.php' );
                        break;
                    case '490':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/peninsula.php' );
                        break;
                    case '491':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/mouzenidis.php' );
                        break;
                    //case '493': include($_SERVER['DOCUMENT_ROOT'].'/includes/preturi_online/trip_corporation.php');break;
                    //if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; } break;
                    case '463':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/nova.php' );
                        break;
                    case '471':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/eximtur.php' );
                        break;
                    case '32':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/solvex.php' );
                        break;
                    case '509':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/holiday_office.php' );
                        break;
                    //if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; } break;
                    case '557':
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/filos.php' );
                        break;
                    case '349': include($_SERVER['DOCUMENT_ROOT'].'/includes/preturi_online/karpaten.php'); break;


                    default:
                        include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/z_preturi_pivot.php' );
                }
            }
        }
    }
}
//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; }

//if(!$err_logare_admin) { echo '<pre>';print_r($flight);echo '</pre>';}
foreach ( $rezcam as $key_rezcam => $value_rezcam ) {


    //if(!$err_logare_admin)  echo "-------".$rezcam[$key_rezcam]['disponibilitate']."<br />";

    if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' and $stop_sales_hotel[ $rezcam[ $key_rezcam ]['id_camera'] ]['stop_sales'] == 'da' ) {
        $rezcam[ $key_rezcam ]['disponibilitate'] = 'stopsales';
    }

}

//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; }
if ( sizeof( $rezcam ) > 0 ) {
    $rezcam = optimize_rooms( $rezcam, $pleczile, date( "Y-m-d", strtotime( $plecdata ) ) );
}

//if(!$err_logare_admin) { echo '<pre>';print_r($rezcam);echo '</pre>'; }
//if(!$err_logare_admin) { echo '<pre>';print_r($_REQUEST);echo '</pre>'; }


$rezcam_date = find_items( $rezcam, 'data_start', $plecdata );
if ( sizeof( $rezcam_date ) > 0 ) {
    $plecdata_new = $plecdata;
} else {
    $plecdata_new = $rezcam[ key( $rezcam ) ]['data_start'];
}

if ( sizeof( $rezcam ) > 0 ) {
    echo '<p class="black bigger-13em" style="padding: 10px 0;margin: 10px 0 0;font-size: 16px;">Pentru <strong class="blue">' . $adulti . ' adul';
    if ( $adulti > 1 ) {
        echo 'ți';
    } else {
        echo 't';
    }
    if ( $copii > 0 ) {
        echo '</strong> și <strong class="blue">' . $copii . ' copi';
        if ( $copii == 1 ) {
            echo 'l';
        } else {
            echo 'i';
        }

        $ages_copii = '';


        for ( $ic = 0; $ic < $copii; $ic ++ ) {
            $ages_copii .= $_REQUEST[ 'copil' . ( $ic + 1 ) ] . ' ani, ';
        }
        $ages_copii = substr( $ages_copii, 0, - 2 );
        echo ' (' . $ages_copii . ') ';

    }
    echo '</strong> în cameră începând cu <strong class="blue">' . date( "d.m.Y", strtotime( $plecdata ) ) . '</strong>, <strong class="blue">' . $pleczile . ' nopți -<span class="textatentie">&nbsp;Toate taxele INCLUSE&nbsp;</span></strong> ';


    if ( $plecdata_new == date( "Y-m-d", strtotime( $plecdata ) ) ) {
        echo 'avem următoarele tarife:';
    } else {
        echo 'nu am găsit nici o ofertă disponibilă. Mai jos regăsiți rezultate asemănătoare căutării dumneavoastră:';
    }
    echo '</p>';
    echo '<table class="table table-responsive offers_tables" style="margin-bottom: 0;font-size: 1rem;">';
    echo '<tr><th class="hide_mobile">Data check-in</th><th class="hide_mobile">Durată</th><th class="hide_mobile" style="text-align: center">Tip cameră</th><th class="hide_mobile" style="text-align:center; padding: 0;">Tip masă</th><th class="hide_mobile" style="text-align:center;">Preț / cameră</th><th class="hide_mobile" style="width:20px;"></th><th class="hide_mobile" style="width:192px;"></th></tr>';

    $i = 0;

    $stergere_sql = "delete from pret_cache where
			ukey = '" . $ukey . "' and
			id_hotel = '" . $id_hotel . "'";
    $stergere_int = mysql_query( $stergere_sql ) or die( mysql_error() );
    @mysql_free_result( $stergere_int );

    foreach ( $rezcam as $key_rezcam => $value_rezcam ) {

        if ( ( $rezcam[ $key_rezcam ]['nr_adulti'] > 0 ) and ( $rezcam[ $key_rezcam ]['pret'] > 0 ) ) {

            $ins_cache[ $key_rezcam ] = "INSERT INTO pret_cache SET
			ukey = '" . $ukey . "',
			id_hotel = '" . $id_hotel . "',
			id_pret = '" . $rezcam[ $key_rezcam ]['id_pret'] . "',
			plecare = '" . $rezcam[ $key_rezcam ]['plecare'] . "',
			id_camera = '" . $rezcam[ $key_rezcam ]['id_camera'] . "',
			denumire_camera = '" . $rezcam[ $key_rezcam ]['denumire_camera'] . "',
			pret = '" . $rezcam[ $key_rezcam ]['pret'] . "',
			comision = '" . $rezcam[ $key_rezcam ]['comision'] . "',
			moneda = '" . $rezcam[ $key_rezcam ]['moneda'] . "',
			nr_adulti = '" . $rezcam[ $key_rezcam ]['nr_adulti'] . "',
			nr_copii = '" . $rezcam[ $key_rezcam ]['nr_copii'] . "',
			copil1 = '" . $rezcam[ $key_rezcam ]['copil1'] . "',
			copil2 = '" . $rezcam[ $key_rezcam ]['copil2'] . "',
			copil3 = '" . $rezcam[ $key_rezcam ]['copil3'] . "',
			oferta = '" . $rezcam[ $key_rezcam ]['oferta'] . "',
			oferta_pret = '" . $rezcam[ $key_rezcam ]['oferta_pret'] . "',
			masa = '" . $rezcam[ $key_rezcam ]['masa'] . "',
			series_name = '" . $rezcam[ $key_rezcam ]['series_name'] . "',
			grila_name = '" . $rezcam[ $key_rezcam ]['grila_name'] . "',
			info_grad_ocupare = '" . $rezcam[ $key_rezcam ]['info_grad_ocupare'] . "',
			period = '" . $rezcam[ $key_rezcam ]['period'] . "',
			data_start = '" . $rezcam[ $key_rezcam ]['data_start'] . "',
			data_end = '" . $rezcam[ $key_rezcam ]['data_end'] . "',
			disponibilitate = '" . $rezcam[ $key_rezcam ]['disponibilitate'] . "',
			nr_nights = '" . $rezcam[ $key_rezcam ]['nr_nights'] . "',
			id_furnizor = '" . $rezcam[ $key_rezcam ]['id_furnizor'] . "',
			furnizor_comision_procent = '" . $rezcam[ $key_rezcam ]['furnizor_comision_procent'] . "',
			id_oferta = '" . $rezcam[ $key_rezcam ]['id_oferta'] . "',
			data_early_booking = '" . date( 'Y-m-d', strtotime( $rezcam[ $key_rezcam ]['data_early_booking'] ) ) . "',
			comision_reducere='" . $rezcam[ $key_rezcam ]['comision_reducere'] . "',
			servicii_incluse='" . addslashes( $rezcam[ $key_rezcam ]['servicii_incluse'] ) . "',
			id_zbor='" . $rezcam[ $key_rezcam ]['id_zbor'] . "',
			data_adaugarii = NOW()
			";
            $que_cache[ $key_rezcam ] = mysql_query( $ins_cache[ $key_rezcam ] ) or die( mysql_error() );
            @mysql_free_result( $que_cache[ $key_rezcam ] );
        } else {
            $update_cache[ $key_rezcam ] = "Update pret_cache SET

			id_pret = '" . $rezcam[ $key_rezcam ]['id_pret'] . "',
			plecare = '" . $rezcam[ $key_rezcam ]['plecare'] . "',
			id_camera = '" . $rezcam[ $key_rezcam ]['id_camera'] . "',
			denumire_camera = '" . $rezcam[ $key_rezcam ]['denumire_camera'] . "',
			pret = '" . $rezcam[ $key_rezcam ]['pret'] . "',
			comision = '" . $rezcam[ $key_rezcam ]['comision'] . "',
			moneda = '" . $rezcam[ $key_rezcam ]['moneda'] . "',
			nr_adulti = '" . $rezcam[ $key_rezcam ]['nr_adulti'] . "',
			nr_copii = '" . $rezcam[ $key_rezcam ]['nr_copii'] . "',
			copil1 = '" . $rezcam[ $key_rezcam ]['copil1'] . "',
			copil2 = '" . $rezcam[ $key_rezcam ]['copil2'] . "',
			copil3 = '" . $rezcam[ $key_rezcam ]['copil3'] . "',
			oferta = '" . $rezcam[ $key_rezcam ]['oferta'] . "',
			oferta_pret = '" . $rezcam[ $key_rezcam ]['oferta_pret'] . "',
			masa = '" . $rezcam[ $key_rezcam ]['masa'] . "',
			series_name = '" . $rezcam[ $key_rezcam ]['series_name'] . "',
			grila_name = '" . $rezcam[ $key_rezcam ]['grila_name'] . "',
			info_grad_ocupare = '" . $rezcam[ $key_rezcam ]['info_grad_ocupare'] . "',
			period = '" . $rezcam[ $key_rezcam ]['period'] . "',
			data_start = '" . $rezcam[ $key_rezcam ]['data_start'] . "',
			data_end = '" . $rezcam[ $key_rezcam ]['data_end'] . "',
			disponibilitate = '" . $rezcam[ $key_rezcam ]['disponibilitate'] . "',
			nr_nights = '" . $rezcam[ $key_rezcam ]['nr_nights'] . "',
			id_furnizor = '" . $rezcam[ $key_rezcam ]['id_furnizor'] . "',
			furnizor_comision_procent = '" . $rezcam[ $key_rezcam ]['furnizor_comision_procent'] . "',
			id_oferta = '" . $rezcam[ $key_rezcam ]['id_oferta'] . "',
			data_early_booking = '" . date( 'Y-m-d', strtotime( $rezcam[ $key_rezcam ]['data_early_booking'] ) ) . "',
			comision_reducere='" . $rezcam[ $key_rezcam ]['comision_reducere'] . "',
			id_zbor='" . $rezcam[ $key_rezcam ]['id_zbor'] . "',
			data_adaugarii = NOW() where
			ukey = '" . $ukey . "' and
			id_hotel = '" . $id_hotel . "'
			";
            $que_cache[ $key_rezcam ] = mysql_query( $update_cache[ $key_rezcam ] ) or die( mysql_error() );
            @mysql_free_result( $que_cache[ $key_rezcam ] );

        }


        $oferte_id[] = $rezcam[ $key_rezcam ]['id_oferta'];


        $i ++;

        if ( strlen( $rezcam[ $key_rezcam ]['nr_nights'] ) > 0 ) {
            $pleczile = $rezcam[ $key_rezcam ]['nr_nights'];
        }
        $detalii_online['discount_tarif'][ $rezcam[ $key_rezcam ]['id_oferta'] ];
        if ( $rezcam[ $key_rezcam ]['oferta_pret'] == 0 and isset( $detalii['spo_reducere'] ) ) {
            $rezcam[ $key_rezcam ]['oferta_pret'] = $rezcam[ $key_rezcam ]['pret'] + ( $detalii['spo_reducere'] * 2 );
        }
        if ( $rezcam[ $key_rezcam ]['oferta'] == '' and isset( $detalii['spo_descriere'] ) ) {
            $rezcam[ $key_rezcam ]['oferta'] = '<strong>' . $detalii['spo_titlu'] . '</strong> ' . $detalii['spo_descriere'];
        }

        if ( $rezcam[ $key_rezcam ]['pret'] > 0 ) {  // daca e pretul mai amre decat 0
            echo '<tr style="border-bottom-style:hidden; background-color: ';
            if ( $i % 2 == 0 ) {
                echo ' #f2f2f2 !important"';
                $background_div = ' #f2f2f2 !important"';
            } else {
                echo ' #ffffff !important"';
                $background_div = ' #ffffff !important"';
            }
            echo '" class="noborderjos">';
            echo '<td class="text-left nowrap"><span class="hide_desktop">Data Plecării</span>
		 <span class="';
            if ( $plecdata == $rezcam[ $key_rezcam ]['data_start'] ) {
                echo 'black';
            } else {
                echo 'green';
            }
            echo '"><strong>' . date( "d.m.Y", strtotime( $rezcam[ $key_rezcam ]['data_start'] ) ) . '</strong></span>';
            '</span>';
            echo '</td>';
            echo '<td class="text-left"><span class="hide_desktop">Durată</span><spam style=" white-space: nowrap;">' . $pleczile . ' nopti</spam></td>';
            echo '<td style="text-align: center";><span class="hide_desktop">Tip Cameră</span><div class="blue text-center"><strong>' . $rezcam[ $key_rezcam ]['denumire_camera'] . '</strong></div>';
            if ( $rezcam[ $key_rezcam ]['oferta'] ) {
                echo '<div class="red italic">' . $rezcam[ $key_rezcam ]['oferta'] . '</div>';
            }

            if ( $rezcam[ $key_rezcam ]['series_name'] ) {
                echo '<div class="green italic">' . $rezcam[ $key_rezcam ]['series_name'] . '</div>';
            }
            if ( $rezcam[ $key_rezcam ]['grila_name'] ) {
                echo '<div class="green italic">' . $rezcam[ $key_rezcam ]['grila_name'] . '</div>';
            }

            $id_zbor = str_replace( '|', '-', $rezcam[ $key_rezcam ]['PackageVariantId'] . '-' . $rezcam[ $key_rezcam ]['data_start'] . '-' . $rezcam[ $key_rezcam ]['id_oferta'] . '-' . $rezcam[ $key_rezcam ]['nr_nights'] );

            //		if ( ( $flight[ $id_zbor ]['flight_bookable'] == 1 and count( $flight[ $id_zbor ]['plecare'] ) > 0 ) or $tip_transport == 4 ) {
            echo '<a data-toggle="collapse" href="#additional-info-' . $i . '-' . $id_hotel . '" aria-expanded="false" aria-controls="additional-info" class="hide_mobile collapsed"><i class="fa fa-info-circle" aria-hidden="true"></i> Informaţii suplimentare</a>';

            echo '<a data-toggle="collapse" href="#additional-mobile-info-' . $i . '-' . $id_hotel . '" aria-expanded="false" aria-controls="additional-info" class="hide_desktop link_card_mobile" ><i class="fa fa-info-circle" aria-hidden="true"></i> Informaţii suplimentare</a>';
            echo '<div class="collapse" id="additional-mobile-info-' . $i . '-' . $id_hotel . '" aria-expanded="true">';
            echo '<div class="card card-block" style="background-color:' . $background_div . '; border: 0; border-radius: 0;">';
            echo '<div class="row">';
            echo '<div class="col-lg-4 col-sm-12">';
            $included_services = '';
            echo '---';
            include( $_SERVER['DOCUMENT_ROOT'] . "/includes/hoteluri/info_servicii_incluse.php" );
            echo '</div>';
            echo '<div class="col-lg-8 col-sm-12">---';
            include( $_SERVER['DOCUMENT_ROOT'] . "/includes/hoteluri/info_servicii.php" );
            echo '</div>';
            if ( sizeof( $rezcam[ $key_rezcam ]['conditii_anulare'] ) > 0 ) {
                echo '<div class="col-lg-4 col-xs-12">';
                echo '<div class="optional-box"><h5>Condiţii de plată</h5>';
                echo 'Avans minim 30%';
                echo '</div></div>';
                echo '<div class="col-lg-4 col-xs-12">';
                echo '<div class="optional-box"><h5>Condiţii de anulare</h5>';
                echo 'Pentru anulare in perioada ';
                echo '</div></div>';
            }
            echo '</div></div></div>';
            //		}

            echo '</td>';
            echo '<td style="text-align: center;" width="15%"><span class="hide_desktop">TIP MASĂ</span><span>' . change_meal_name($rezcam[ $key_rezcam ]['masa']) . '</span></td>';

            if ( ! $err_logare_admin ) {
                ////				echo preg_replace_callback( "/([+])+([ ])+([a-zA-Z \ă \Ă \â \Â \î \Î \ş \Ş \ţ \Ţ]*\w+([ ])+([\d]+))/", "change_meal_new", $rezcam[ $key_rezcam ]['masa'] );
                //				echo preg_replace_callback( "/[\+]+[ ]+([a-zA-Z \ă\Ă\â\Â\î\Î\ş\Ş\ţ\Ţ]*)+([ ])+([\d]+)/", "change_meal_new", $rezcam[ $key_rezcam ]['masa'] );
                //                echo $rezcam[ $key_rezcam ]['masa'] . ' | ' .change_meal_name($rezcam[ $key_rezcam ]['masa']).'<br>';
            }

            echo '<td style="text-align: center;"><span class="hide_desktop">PREȚ / CAMERĂ</span>';
            if ( $rezcam[ $key_rezcam ]['oferta_pret'] ) {
                $oldprice = round( $rezcam[ $key_rezcam ]['oferta_pret'], 0 );
                echo '<span class="old-price text-primary blue" style=" white-space: nowrap;">' . $oldprice . ' ' . moneda( $rezcam[ $key_rezcam ]['moneda'] ) . '</span>';
            } else {
                $oldprice = 0;
            }
            echo '<span class="current-price text-success pret green" style=" white-space: nowrap;">' . new_price( $rezcam[ $key_rezcam ]['pret'] ) . ' ' . moneda( $rezcam[ $key_rezcam ]['moneda'] ) . '</span>';
            $array_pret[ $i ] = new_price( $rezcam[ $key_rezcam ]['pret'] );
            echo '</td>';
            echo '<td class="hide_mobile"></td>';
            echo '<td style="text-align: center; position:relative;" >';
            if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'disponibil' ) {
                echo '<i class="fa fa-check-circle text-success"></i> ';
                echo '<span class="badge badge-success">Disponibil</span><br />';
            } elseif ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' ) {
                echo '<i class="fa fa-check-circle text-primary"></i> ';
                echo '<span class="badge badge-success" style="background-color: #065abf">Necesita Confirmare</span><br />';
            } elseif ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'stopsales' ) {
                echo '<i class="fa fa-minus-circle text-danger"></i> ';
                echo '<span class="badge badge-danger">Indisponibil</span><br />';
            }
            if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'disponibil' or $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' ) {
                echo '<a href="' . make_link_oferta( $detalii_hotel['localitate'], $detalii_hotel['denumire'], null, null ) . 'rrezervare-' . $rezcam[ $key_rezcam ]['id_oferta'] . '_' . base64_encode( $rezcam[ $key_rezcam ]['id_pret'] . '_' . $rezcam[ $key_rezcam ]['plecare'] . '_' . $rezcam[ $key_rezcam ]['data_start'] . '_' . $pleczile . '_' . $rezcam[ $key_rezcam ]['pret'] . '_' . str_replace( '_', '*', $rezcam[ $key_rezcam ]['id_camera'] ) . '_' . $rezcam[ $key_rezcam ]['nr_adulti'] . '_' . $rezcam[ $key_rezcam ]['nr_copii'] . '_' . $rezcam[ $key_rezcam ]['copil1'] . '_' . $rezcam[ $key_rezcam ]['copil2'] . '_' . $rezcam[ $key_rezcam ]['copil3'] . '_' . $rezcam[ $key_rezcam ]['masa'] . '_' . $rezcam[ $key_rezcam ]['denumire_camera'] . '_' . $rezcam[ $key_rezcam ]['comision'] . '_' . $oldprice . '_' . $rezcam[ $key_rezcam ]['disponibilitate'] . '_' . $rezcam[ $key_rezcam ]['data_early_booking'] . '_' . str_replace( '_', '*', $rezcam[ $key_rezcam ]['PackageVariantId'] ) . '_' . $rezcam[ $key_rezcam ]['RoomCode'] . '_' . time() ) . '" onClick="ga(\'send\', \'event\', \'pagina oferta\', \'rezerva oferta\', \'' . $detalii['denumire'] . '\');" class="btn btn-primary full_width_mobile">Rezervă acum</a>';
                echo '<br class="clear">';
                $basket[ $key_rezcam ] = check_basket( $rezcam[ $key_rezcam ]['id_oferta'], $rezcam[ $key_rezcam ]['id_pret'], $rezcam[ $key_rezcam ]['plecare'], $rezcam[ $key_rezcam ]['data_start'], $pleczile, $rezcam[ $key_rezcam ]['id_camera'], $rezcam[ $key_rezcam ]['denumire_camera'], $rezcam[ $key_rezcam ]['nr_adulti'], $rezcam[ $key_rezcam ]['nr_copii'], $rezcam[ $key_rezcam ]['masa'], $rezcam[ $key_rezcam ]['pret'] );
                if ( $basket[ $key_rezcam ] === true ) {
                    echo '<a href="/ofertele-mele/" class="smaller-09em link-blue" target="_blank" title="Deschide lista cu ofertele salvate"><i class="icon-heart green"></i> <span class="underline">Ofertă salvată</span></a>';
                } else {
                    echo '<span id="save' . $key_rezcam . '"><span class="smaller-09em blue underline pointer" style="cursor: pointer;">Salvează oferta</span></span>';
                    echo '<script>
					$("#save' . $key_rezcam . '").on("click", function() {
						$("#save' . $key_rezcam . '").load("/salveaza-oferta/?data=' . base64_encode( $rezcam[ $key_rezcam ]['id_oferta'] . '##' . $rezcam[ $key_rezcam ]['id_pret'] . '##' . $rezcam[ $key_rezcam ]['plecare'] . '##' . $rezcam[ $key_rezcam ]['data_start'] . '##' . $pleczile . '##' . $rezcam[ $key_rezcam ]['pret'] . '##' . $rezcam[ $key_rezcam ]['id_camera'] . '##' . $rezcam[ $key_rezcam ]['nr_adulti'] . '##' . $rezcam[ $key_rezcam ]['nr_copii'] . '##' . $rezcam[ $key_rezcam ]['copil1'] . '##' . $rezcam[ $key_rezcam ]['copil2'] . '##' . $rezcam[ $key_rezcam ]['copil3'] . '##' . change_meal( $rezcam[ $key_rezcam ]['masa'] ) . '##' . $rezcam[ $key_rezcam ]['denumire_camera'] . '##' . $rezcam[ $key_rezcam ]['data_early_booking'] . '##' . $rezcam[ $key_rezcam ]['comision_reducere'] ) . '");
					});
				  </script>';
                }
            } else {
                echo '<a href="#" class="btn btn-primary red_change_data full_width_mobile">Schimba datele</a>';
            }
            if ( ! $err_logare_admin ) {
                $detalii_furnizor = get_detalii_furnizor( $rezcam[ $key_rezcam ]['id_furnizor'] );
                echo '<div style="position:absolute; top:25px; right:-180px; width:150px;">
			  <strong>' . round( $rezcam[ $key_rezcam ]['comision'], 0 ) . ' ' . $rezcam[ $key_rezcam ]['moneda'] . '</strong> <br />
			  <a href="/adm/editare_furnizor.php?pas=2&furnizor=' . $detalii_furnizor['id_furnizor'] . '" target="_blank" class="link-blue">' . $detalii_furnizor['denumire'] . '</a><br><a href="' . $detalii_furnizor['link_admin'] . '" target="_blank" class="link-green"><strong>&laquo; Login &raquo;</strong></a></div>';
            }
            if ( $i == 1 ) {
                $primul_pret = new_price( $rezcam[ $key_rezcam ]['pret'] );
            }
            echo '</td>';
            echo '</tr>';
        }// end pret mic
        //		if ( ( $flight[ $id_zbor ]['flight_bookable'] == 1 and count( $flight[ $id_zbor ]['plecare'] ) > 0 ) or $tip_transport == 4 ) {
        echo '<tr style="border-top-style:hidden">';
        echo '<td colspan="7" style="padding: 0; ">';
        echo '<div class="collapse" id="additional-info-' . $i . '-' . $id_hotel . '" aria-expanded="true">';
        echo '<div class="card card-block" style="background-color:' . $background_div . '; border: 0; border-radius: 0;">';

        echo '<div class="row">';
        echo '<div class="col-lg-4 col-sm-12">';

        include( $_SERVER['DOCUMENT_ROOT'] . "/includes/hoteluri/info_servicii_incluse.php" );
        echo '</div>';
        echo '<div class="col-lg-8 col-sm-12">';

        include( $_SERVER['DOCUMENT_ROOT'] . "/includes/hoteluri/info_servicii.php" );
        // echo '<pre>';print_r($rezcam[ $key_rezcam ]['conditii_anulare']);echo '</pre>';

        echo '</div>';

        if ( sizeof( $conditii_anulare[ $rezcam[ $key_rezcam ]['PackageVariantId'] ] ) > 0 ) {
            echo '<div class="col-lg-4 col-xs-12">';

            echo '<div><h5>Conditii de anulare</h5>'; ?>

            <ul class="taxe-neincluse">
                <?php foreach ( $conditii_anulare[ $rezcam[ $key_rezcam ]['PackageVariantId'] ] as $a_key => $a_value ) {


                    if ( $a_value['valoare'] == '0' ) {
                        ?>
                        <li>
                            <span class="bkg-dark-green underline bigger-13em white pad5"> Anulare <strong>GRATUITA</strong> pana la data de <?php echo denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ); ?></span>
                        </li>
                        <?php $camp_anulare = 'Anulare GRATUITA pana la data de ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ) . '\n'; ?>

                    <?php } else { ?>
                        <?php if ( $a_value['tip_valoare_anulare'] == 'numar_zile_penalizare' ) {
                            $valoare_anualare = round( $rezcam[ $key_rezcam ]['pret'] );
                        } ?>
                        <li>Pentru anularea rezervarii in perioada <strong>
                                <?php echo denLuniRo( date( 'd F Y', strtotime( $a_value['data_inceput'] ) ) ) . ' - ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ); ?></strong>
                            penalizarea este de <strong><?php echo $a_value['valoare'];

                                if ( $a_value['tip_valoare_anulare'] == 'procent' ) {
                                    echo '% din valoare totala a sejurului';
                                }
                                if ( $a_value['tip_valoare_anulare'] == 'suma_fixa' ) {
                                    echo '  ';
                                } ?>
                            </strong>
                        </li>
                        <?php //$camp_anulare .= '- Pentru anularea rezervarii in perioada ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_inceput'] ) ) ) . ' - ' . denLuniRo( date( 'd F Y', strtotime( $a_value['data_sfarsit'] ) ) ) . ' penalizarea este de <strong>' . $a_value['valoare'] . '</strong> \n'; ?>

                    <?php } ?>

                <?php } ?>

            </ul>


            <?php echo '</div>
			
			</div>';
        }
        if ( sizeof( $rezcam[ $key_rezcam ]['conditii_plata'] ) > 0 ) {
            echo '<div class="col-lg-4 col-xs-12">';
            echo '<div class="optional-box"><h5>Title box</h5>';
            echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum lacinia nunc et eros mattis gravida. Vestibulum congue dolor vitae purus gravida sollicitudin. Aliquam nec urna eget velit fermentum scelerisque. Integer a est in orci auctor bibendum sed quis magna. Integer vel quam sed justo mollis sagittis quis in turpis. Mauris sed felis non ex finibus congue. Proin imperdiet ullamcorper tellus, et feugiat ante finibus in. Fusce semper ipsum vel sem semper dictum. Nulla id tellus vitae est pharetra fringilla et et mi. Sed ac malesuada ipsum.';
            echo '</div></div>';
        }

        echo '	</div></div></div>';
        //		}
    }
    echo '</table>';
    echo '<div class="search-rooms" style="background-color: #fafafa; padding: 5px; font-size: 14px; margin-bottom: 1px">
            <strong>Legendă:</strong><br/><i class="fa fa-check-circle text-success"></i> Disponibil, &nbsp; <i class="fa fa-check-circle text-primary"></i> Disponibil, necesită confirmare, &nbsp; <i class="fa fa-minus-circle text-danger"></i> Indisponibil
	<br /><br />
	
	<strong>OcaziiTuristice.ro</strong> se conecteaza in timp real la sistemele tour operatorilor din Romania si strainatate pentru a va oferi cele mai bune preturi disponibile.
	<br /><br />
	<strong>Atenție!</strong> <i>Trimiterea unei rezervări <strong>nu implică</strong> nici o obligație financiară din partea dumneavoastră!</i> 
	

	</div>';

    //start div optiuni oferta


    $nr_nopti                = $pleczile;
    $detalii['id_transport'] = $_REQUEST['trans'];

    //$detalii_oferta = $det_oferta->select_det_sejur($id_oferta);
    $services = $det->offer_services( $id_hotel, $detalii['id_transport'], $nr_nopti, get_den_localitate( $orasplec ), $detalii_online['oras_destinatie'][ $id_oferta ][1], $id_oferta );
    //if(!$err_logare_admin) { echo '<pre>';print_r($services);echo '</pre>';}

    //echo $detalii_hotel['cautare_live'];

    if ( $detalii['id_transport'] == 1 ) {
        unset( $oras_plecare );
    }


    $included_services = '';
    if ( $detalii_hotel['cautare_live'] == 'da' ) {
        $preturi_online = 'da';
        if ( sizeof( $services['servicii_incluse'] ) > '0' ) {
            foreach ( $services['servicii_incluse'] as $key => $value ) {
                $included_services .= '- ' . ucfirst( strip_tags( $value ) ) . '<br>';
            }
        }

    } else {
        $preturi_online = "nu";
        foreach ( $detalii['denumire_v1'] as $key => $value ) {
            $included_services .= '- ' . ucfirst( $value ) . '<br>';
        }

    }

    ?>

    <?php //if(!$err_logare_admin) { echo '<pre>';print_r(array_unique ($oferte_id));echo '</pre>'; }?>

    <?php $oferte_id = array_unique( $oferte_id );
    foreach ( $oferte_id as $key => $detalii_id_oferta ) {

        $services = $det->offer_services( $id_hotel, $detalii['id_transport'], $nr_nopti, get_den_localitate( $orasplec ), $detalii_online['oras_destinatie'][ $id_oferta ][1], $detalii_id_oferta );
        //if(!$err_logare_admin) { echo '<pre>';print_r($services);echo '</pre>';}

        //echo $detalii_hotel['cautare_live'];

        if ( $detalii['id_transport'] == 1 ) {
            unset( $oras_plecare );
        }


        $included_services = '';
        if ( $detalii_hotel['cautare_live'] == 'da' ) {
            $preturi_online = 'da';
            if ( sizeof( $services['servicii_incluse'] ) > '0' ) {
                foreach ( $services['servicii_incluse'] as $key => $value ) {
                    $included_services .= '- ' . ucfirst( strip_tags( $value ) ) . '<br>';
                }
            }
        } else {
            $preturi_online = "nu";
            foreach ( $detalii['denumire_v1'] as $key => $value ) {
                $included_services .= '- ' . ucfirst( $value ) . '<br>';
            }
        }
        $included_services .= '- ' . $rezcam[ $key_rezcam ]['servicii_incluse'];

        ?>


        <?php echo "<div id=\"detaliis" . $detalii_id_oferta . "\" style=\"display:none;\">
			<div style=\"display: inline-block;width: 400px;height: 100px;margin: 1em;\">
<span class=\" bigger-12em blue bold\">Servicii Incluse 2 </span><br /><br />" . schimba_caractere( $included_services );
        $tip_masa = change_meal( $rezcam[ $key_rezcam ]['masa'] );

    }


    //end optiuni oferta
    //if(!$err_logare_admin) { echo '<pre>';print_r($flight);echo '</pre>';}


    if ( sizeof( $flight ) > 0 ) {
        foreach ( $flight as $id_zbor => $zbor ) {


            if ( $zbor['flight_bookable'] == 1 and sizeof( $zbor['plecare'] ) > 0 ) {

                echo '<div id="flight' . $id_zbor . '" style="display:none;">';
                echo '<table class="search-rooms mar10-0"><tr class="bkg-white"><th class="text-center">Companie</td><th class="text-center">Nr. cursă</th><th class="text-center">Plecare</th><th class="text-center">Sosire</th>';
                foreach ( $zbor['plecare'] as $k_av_out => $v_av_out ) {


                    foreach ( $v_av_out as $k_cursa_out => $v_cursa_out ) {

                        if ( count( $cache_results['planes'] ) == 0 ) {
                            $ins_cache['plecare'][ $k_av_out ][ $k_cursa_out ] = "INSERT INTO pret_planes_cache SET
							ukey = '" . $ukey . "',
							id_hotel = '" . $id_hotel . "',
							id_oferta = '" . $id_oferta . "',
							flight_bookable = '" . $zbor['flight_bookable'] . "',
							tip_cursa = 'plecare',
							key_flight = '" . $k_av_out . "',
							key_curse = '" . $k_cursa_out . "',
							`from` = '" . $v_cursa_out['from'] . "',
							`to` = '" . $v_cursa_out['to'] . "',
							data_plecare = '" . $v_cursa_out['data_plecare'] . "',
							data_sosire = '" . $v_cursa_out['data_sosire'] . "',
							companie = '" . $v_cursa_out['companie'] . "',
							nr_cursa = '" . $v_cursa_out['nr_cursa'] . "',
							seats = '" . $v_cursa_out['seats'] . "',
							id_zbor='" . $id_zbor . "',
							data_adaugarii = NOW()
							";
                            $que_cache['plecare'][ $k_av_out ][ $k_cursa_out ] = mysql_query( $ins_cache['plecare'][ $k_av_out ][ $k_cursa_out ] ) or die( mysql_error() );
                            @mysql_free_result( $que_cache['plecare'][ $k_av_out ][ $k_cursa_out ] );
                        } else {

                            $update_cache['plecare'][ $k_av_out ][ $k_cursa_out ] = "Update pret_planes_cache SET
							ukey = '" . $ukey . "',
							id_hotel = '" . $id_hotel . "',
							id_oferta = '" . $id_oferta . "',
							flight_bookable = '" . $zbor['flight_bookable'] . "',
							tip_cursa = 'plecare',
							key_flight = '" . $k_av_out . "',
							key_curse = '" . $k_cursa_out . "',
							`from` = '" . $v_cursa_out['from'] . "',
							`to` = '" . $v_cursa_out['to'] . "',
							data_plecare = '" . $v_cursa_out['data_plecare'] . "',
							data_sosire = '" . $v_cursa_out['data_sosire'] . "',
							companie = '" . $v_cursa_out['companie'] . "',
							nr_cursa = '" . $v_cursa_out['nr_cursa'] . "',
							seats = '" . $v_cursa_out['seats'] . "',
							id_zbor='" . $id_zbor . "',
							data_adaugarii = NOW()
							where
							ukey = '" . $ukey . "' and
							id_hotel = '" . $id_hotel . "' and
							id_zbor='" . $id_zbor . "'";
                            //$que_cache['plecare'][ $k_av_out ][ $k_cursa_out ] = mysql_query( $ins_cache['plecare'][ $k_av_out ][ $k_cursa_out ] ) or die( mysql_error() );
                            //@mysql_free_result( $que_cache['plecare'][ $k_av_out ][ $k_cursa_out ] );


                            // fa update la avion
                        }


                        echo '<tr class="bkg-grey">';
                        echo '<td class="text-center">';
                        if ( strlen( get_airline_by_iata( $v_cursa_out['companie'] ) ) > 0 ) {
                            echo '<br class="clear"><img src="/images/avion/' . get_airline_by_iata( $v_cursa_out['companie'] ) . '.jpg" alt=""> ';
                        }
                        echo '</td>';
                        echo '<td class="text-center">' . $v_cursa_out['nr_cursa'] . '</td>';


                        echo '<td class="text-center"><strong>' . $v_cursa_out['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_out['data_plecare'] ) . '</td>';

                        echo '<td class="text-center"><strong>' . $v_cursa_out['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_out['data_sosire'] ) . '</td>';

                        //if($v_cursa_out['seats']) echo ' - '.$v_cursa_out['seats'].' locuri';
                        echo '</tr>';
                    }
                }
                foreach ( $zbor['intoarcere'] as $k_av_in => $v_av_in ) {
                    foreach ( $v_av_in as $k_cursa_in => $v_cursa_in ) {
                        if ( count( $cache_results['planes'] ) == 0 ) {
                            $ins_cache['intoarcere'][ $k_av_in ][ $k_cursa_in ] = "INSERT INTO pret_planes_cache SET
							ukey = '" . $ukey . "',
							id_hotel = '" . $id_hotel . "',
							id_oferta = '" . $id_oferta . "',
							flight_bookable = '" . $zbor['flight_bookable'] . "',
							tip_cursa = 'intoarcere',
							key_flight = '" . $k_av_in . "',
							key_curse = '" . $k_cursa_in . "',
							`from` = '" . $v_cursa_in['from'] . "',
							`to` = '" . $v_cursa_in['to'] . "',
							data_plecare = '" . $v_cursa_in['data_plecare'] . "',
							data_sosire = '" . $v_cursa_in['data_sosire'] . "',
							companie = '" . $v_cursa_in['companie'] . "',
							nr_cursa = '" . $v_cursa_in['nr_cursa'] . "',
							seats = '" . $v_cursa_in['seats'] . "',
							id_zbor='" . $id_zbor . "',
							data_adaugarii = NOW()
							";
                            $que_cache['intoarcere'][ $k_av_in ][ $k_cursa_in ] = mysql_query( $ins_cache['intoarcere'][ $k_av_in ][ $k_cursa_in ] ) or die( mysql_error() );
                            @mysql_free_result( $que_cache['intoarcere'][ $k_av_in ][ $k_cursa_in ] );
                        } else {
                            // fa update la avion
                        }


                        echo '<tr class="bkg-white">';
                        echo '<td class="text-center">';
                        if ( strlen( get_airline_by_iata( $v_cursa_in['companie'] ) ) > 0 ) {
                            echo '<br class="clear"><img src="/images/avion/' . get_airline_by_iata( $v_cursa_in['companie'] ) . '.jpg" alt=""> ';
                        }
                        echo '</td>';
                        echo '<td class="text-center">' . $v_cursa_in['nr_cursa'] . '</td>';
                        echo '<td class="text-center"><strong>' . $v_cursa_in['from'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_in['data_plecare'] ) . '</td>';
                        echo '<td class="text-center"><strong>' . $v_cursa_in['to'] . '</strong> - ' . afiseaza_orar_zbor( $v_cursa_in['data_sosire'] ) . '</td>';
                        //if($v_cursa_in['seats']) echo ' - '.$v_cursa_in['seats'].' locuri';
                        echo '</tr>';

                    }
                }
                echo '</table>';
                echo '</div>';
            }
        }
    }

} elseif ( $showerr == 'da' ) {
    $fara_rezervare = 'da';
    echo '<br><div class="float-left w520"><p class="black bigger-11em" style="padding-bottom:10px;">';
    echo 'Pentru <span class="blue bold">' . $adulti . ' adul';
    if ( $adulti > 1 ) {
        echo 'ți';
    } else {
        echo 't';
    }
    if ( $copii > 0 ) {
        echo '</span> și <span class="blue bold">' . $copii . ' copi';
        if ( $copii == 1 ) {
            echo 'l';
        } else {
            echo 'i';
        }

        $ages_copii = '';
        for ( $ic = 0; $ic < $copii; $ic ++ ) {
            $ages_copii .= $_REQUEST[ 'copil' . ( $ic + 1 ) ] . ' ani, ';
        }
        $ages_copii = substr( $ages_copii, 0, - 2 );
        echo '(' . $ages_copii . ') ';
    }
    echo '</span> în cameră începând cu <span class="blue bold">' . date( "d.m.Y", strtotime( $plecdata ) ) . '</span>, <span class="blue bold">' . $pleczile . ' nopți</span> ';
    if ( $adulti + $copii >= 4 ) {
        echo ' vă rugăm să încercaţi <span class="underline bold">o altă repartizare în cameră</span> (mai puţine persoane) sau puteţi să completați o <span class="underline bold">cerere pentru mai multe detalii</span> <span class="italic smaller-09em">(click pe butonul alăturat)</span>.';
    } else {
        echo ' vă rugăm să ne <span class="underline bold">contactați telefonic</span> ori să completați o <span class="underline bold">cerere pentru mai multe detalii</span> <span class="italic smaller-09em">(click pe butonul alăturat)</span>.';
    }
    echo '</p></div>';
}

if ( $fara_rezervare == 'da' ) {
    $id_oferta = null;
    echo '<div class="float-right">';
    include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/sejururi/cerere_detalii_responsive.php" );
    echo '</div>';

    if ( ! is_bot() and isset( $err_logare_admin ) ) {
        erori_disponibilitate( null, $id_hotel, $plecdata, $pleczile, $adulti, $copii );
    }

}

//if(!$err_logare_admin) { echo '<pre>';print_r( $array_pret);echo '</pre>'; }

if ( is_array( $array_pret ) ) {
    $pret_minim = min( $array_pret );
}


?>
<?php /*?><script type="text/javascript">

var _ra = _ra || {};
	_ra.sendProductInfo = {
		"id": <?php echo $id_hotel;?>,
		"name": "<?php echo $detalii_hotel['denumire'];?>",
		"url": "<?php echo $sitepath.'cazare-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/'; ?>",
		"img": "<?php echo $sitepath.'img_mediu_hotel/'.$detalii_hotel['poza1']; ?>",
		"price": <?php echo $pret_minim;?>,
		"stock": 1,
		"brand": false,
		"category": {
			"id": <?php echo $detalii_hotel['id_zona'];?>,
			"name": "<?php echo $detalii_hotel['zona'];?>",
			"parent": false
		},
		"category_breadcrumb": []
	}

	if (_ra.ready !== undefined) {
		_ra.sendProduct(_ra.sendProductInfo);
	}

</script><?php */ ?>
<script>
    $(document).ready(function () {
        $(".detalii_oferta").each(function () {
            var $id = $(this).attr('id');
            var $dialog = $('#detalii' + $id)
                .dialog({
                    modal: true,
                    autoOpen: false,
                    resizable: false,
                    title: "Detalii oferta",
                    width: 750,
                    open: function () {
                        jQuery('.ui-widget-overlay').bind('click', function () {
                            jQuery('#detalii' + $id).dialog('close');
                        })
                    }
                });
            $(this).click(function () {
                $dialog.dialog('open');
                return false;
            });
        });
    });


</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-27001687-1)', 'auto');
    ga('send', 'pageview');

    function gaTrack(path, title) {
        ga('set', {page: path, title: title});
        ga('send', 'pageview');
    }

    gaTrack('<?php echo currentPageURL(); ?>', 'Pagina Preturi <?php echo( $detalii_hotel['denumire'] )?>');

    ga('send', 'event', 'pagina hotel', 'incarcare preturi', 'incarcare preturi', 'Incarcare perturi <?php echo( $detalii_hotel['denumire'] )?>');
</script>


