<?php

$destinatii = [
	[
		'nume'           => 'Circuit Israel',
		'continent'      => 'Asia',
		'tara'           => 'Israel',
		'pret_minim'     => '495',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/circuite/israel/',
		'imagine'        => '/circuite/images/israel.jpg'
	],
	[
		'nume'           => 'Circuit Japonia',
		'continent'      => 'Asia',
		'tara'           => 'Japonia',
		'pret_minim'     => '3090',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/circuite/japonia/',
		'imagine'        => '/circuite/images/japonia.jpg'
	],
	[
		'nume'           => 'Circuit Thailanda',
		'continent'      => 'Asia',
		'tara'           => 'Thailanda',
		'pret_minim'     => '999',
		'moneda'         => 'EURO',
		'exprimare_pret' => 'sejur/pers',
		'link'           => 'https://www.ocaziituristice.ro/circuite/thailanda/',
		'imagine'        => '/circuite/images/thailanda.jpg'
	]
];
?>
<?php if ( $destinatii ) : ?>
    <div class="homepage-boxes circuite">
        <div class="layout">
            <h3 class="subtitle">Circuite</h3>
            <div class="ofVizitate <?php echo 'columns-' . count( $destinatii ) ?>">
				<?php foreach ( $destinatii as $destinatie ) : ?>
                    <div class="prod">
                        <div class="prod-bg" style="background-image:url(<?php echo $destinatie['imagine'] ?>);"></div>
                        <div class="titlu"><?php echo $destinatie['nume'] ?></div>
                        <div class="prod-overlay">
                            <a href="<?php echo $destinatie['link'] ?>" class="link-blue">
                                <div class="overlay-text">
                                    <h3><?php echo $destinatie['nume'] ?></h3>
                                    <h5>
										<?php echo $destinatie['tara'] ? $destinatie['tara'] : '' ?>
										<?php echo $destinatie['tara'] && $destinatie['continent'] ? ' / ' : '' ?>
										<?php echo $destinatie['continent'] ? $destinatie['continent'] : '' ?>
                                    </h5>
                                    <div>de la</div>
                                    <div class="price">
                                        <span class="red"><?php echo $destinatie['pret_minim'] ?></span> <?php echo $destinatie['moneda'] ?>
                                    </div>
                                    <p><?php echo $destinatie['exprimare_pret'] ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            resizeBoxes();
            window.addEventListener('resize', resizeBoxes);
        });

        function resizeBoxes() {
            if ( $('.circuite .prod').width() < 410 ) {
                $('.circuite .prod').height($('.circuite .prod').width() + 10);
                $('.circuite .prod .prod-bg').height($('.circuite .prod').width());
            } else {
                $('.circuite .prod').height(410);
                $('.circuite .prod .prod-bg').height(400);
            }

        }
    </script>
<?php endif; ?>