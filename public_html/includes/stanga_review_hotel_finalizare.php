<div id="detaliiFormulare" align="left">
    <h1>Impresii de vacanta <?php echo $nume_din_tara; ?></h1>
    <form name="review_hotel" action="" method="post" class="reviewForm">
        <br />
        <br />
        <table align="center">
            <tr>
                <td width="450" align="justify">
                    Pentru o mai buna credibilitate a review-urilor, va rugam frumos completati urmatoarele date.
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                    
                </td>
            </tr>
            <tr>
                <td>
                    <label><span class="small">Nume:<span style="color: #ff0000;">*</span></span> <input type="text" name="nume" style="width: 350px;<?php if(isset($_POST['nume']) && empty($_POST['nume'])) { echo ' border-color: #ff0000;'; } ?>" value="<?php if(isset($_POST['nume']) && !empty($_POST['nume'])) { echo $_POST['nume']; } else { echo $nume; } ?>" /></label>
                </td>
                <?php
                    if($eroare > 0)
                    {
                        if(isset($_POST['nume']) && empty($_POST['nume']))
                        {
                            echo "<td style=\"color: #ff0000;\">Completati campul NUME.</td>";
                        }                        
                    }
                    else
                    {
                        echo "<td>&nbsp;</td>";
                    }                    
                ?>
            </tr>
            <tr>
                <td>
                    <label><span class="small">Prenume:<span style="color: #ff0000;">*</span></span> <input type="text" name="prenume" style="width: 350px;<?php if(isset($_POST['prenume']) && empty($_POST['prenume'])) { echo ' border-color: #ff0000;'; } ?>" value="<?php if(isset($_POST['prenume']) && !empty($_POST['prenume'])) { echo $_POST['prenume']; } else { echo $prenume; } ?>" /></label>
                </td>
                <?php
                    if($eroare > 0)
                    {
                        if(isset($_POST['prenume']) && empty($_POST['prenume']))
                        {
                            echo "<td style=\"color: #ff0000;\">Completati campul PRENUME.</td>";
                        }                        
                    }
                    else
                    {
                        echo "<td>&nbsp;</td>";
                    }                    
                ?>                
            </tr>
            <tr>
                <td>
                    <label><span class="small">E-mail:<span style="color: #ff0000;">*</span></span> <input type="text" name="email" style="width: 350px;<?php if((isset($_POST['email']) && empty($_POST['email'])) || (isset($_POST['email']) && !empty($_POST['email']) && $eroare_email == 2)) { echo ' border-color: #ff0000;'; } ?>" value="<?php if(isset($_POST['email']) && !empty($_POST['email'])) { echo $_POST['email']; } else { echo $email; } ?>" /></label>
                    <span style="font-size: 10px; margin-left: 85px;"><strong><em>*) adresa de email nu va aparea pe site.</em></strong></span>
                </td>
                <?php
                    if($eroare > 0)
                    {
                        if(isset($_POST['email']) && empty($_POST['email']))
                        {
                            echo "<td style=\"color: #ff0000;\">Completati campul EMAIL.</td>";
                        }                        
                        else
                        {
                            if($eroare_email == 2)
                            {
                                echo "<td style=\"color: #ff0000;\">Adresa de email invalida.</td>";    
                            }
                            else
                            {
                                if($email_existent == 2)
                                {
                                    echo "<td style=\"color: #ff0000;\" valign=\"top\">Adresa de email existenta.</td>";
                                }
                                else
                                {
                                    echo "<td>&nbsp;</td>";
                                }                                
                            }                                                    
                        }
                    }
                    else
                    {
                        if(isset($_POST['email']) && !empty($_POST['email']) && $email_existent == 1)
                        {
                            echo "<td style=\"color: #ff0000;\" valign=\"top\">Adresa de email existenta.</td>";
                        }
                        else
                        {
                            echo "<td>&nbsp;</td>";
                        }                        
                    }
                ?>                
            </tr>
            <tr>
                <td>&nbsp;
                    
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle"<?php if($eroare > 0) { echo ' colspan="2"'; } ?>>
                    <span>Doriti sa va creati cont pe site-ul <strong>www.ocaziituristice.ro</strong>: </span><input type="radio" name="se_creaza_cont" value="1" <?php if((!isset($_POST['se_creaza_cont'])) || (isset($_POST['se_creaza_cont']) && $_POST['se_creaza_cont'] == 1)) { echo 'checked="checked"'; } ?> style="width: 0px;" />Da&nbsp;&nbsp;&nbsp;<input type="radio" name="se_creaza_cont" value="2" <?php if(isset($_POST['se_creaza_cont']) && $_POST['se_creaza_cont'] == 2) { echo 'checked="checked"'; } ?> style="width: 0px;" />Nu
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                    
                </td>
            </tr>            
            <tr>
                <td align="center" valign="middle"<?php if($eroare > 0) { echo ' colspan="2"'; } ?>>
                    <span>Daca nu doriti sa va creati  cont pe site-ul <strong>www.ocaziituristice.ro</strong>, review-ul dumneavoastra va fi afisat cu numele si prenumele introduse mai sus.
                </td>
            </tr>             
            <tr>
                <td<?php if($_SESSION['rv_eroare'] > 0) { echo ' colspan="2"'; } ?>>&nbsp;
                    
                </td>
            </tr>                       
            <tr>
                <td<?php if($_SESSION['rv_eroare'] > 0) { echo ' colspan="2"'; } ?>>
                    <strong><em>Campurile marcate cu <span style="color: #ff0000;">*</span> sunt obligatorii.</em></strong>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <div align="center">
            <input type="image" name="btnReviewHotelTrimite" src="/images/buton_trimite.gif" width="154" height="30" value="Trimitere" style="border: 0;" />
        </div>
        <br />
    </form>
    <br class="clear" />
</div>