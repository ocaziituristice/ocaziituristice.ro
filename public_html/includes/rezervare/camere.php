<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php'); ?>

<?php for ($i=1; $i<=$_POST['id_nr_camere']; $i++) { ?>
<div class="ln-camera NEW-round8px">
  <div class="titlu NEW-round8px clearfix">
    <div class="left1">Camera <?php echo $i; ?></div>
    <div class="right1">
      <select name="tip_camera" id="tip_camera">
        <option selected="selected" disabled="disabled">- Alege tipul camerei -</option>
        <?php foreach($preturi['camera'] as $key_cam=>$value_cam) { ?>
        <option value="<?php echo $key_cam; ?>"><?php echo $value_cam; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div id="tip_camera_inp"></div>
</div>
<?php } ?>

<script type="text/javascript">
$("#tip_camera").change(function(){
	var id=$(this).val();
	var dataString = 'id_tip_camera='+ id;
	
	$.ajax({
		type: "POST",
		url: "<?php echo $sitepath; ?>includes/rezervare/tip_camera.php",
		data: dataString,
		cache: false,
		success: function(html){
			$("#tip_camera_inp").html(html);
		}
	});
});
</script>
