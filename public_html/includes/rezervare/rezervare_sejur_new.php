<?php
$sel_nr_min="SELECT nr_min FROM nr_nopti_minim WHERE id_oferta='$id_oferta' ORDER BY ordonare DESC LIMIT 0,1";
$que_nr_min=mysql_query($sel_nr_min) or die(mysql_error());
$row_nr_min=mysql_fetch_array($que_nr_min);
@mysql_free_result($que_nr_min);

if($row_nr_min['nr_min']) {
	$nr_minim_nopti = $row_nr_min['nr_min'];
} else {
	$nr_minim_nopti = '2';
}
?>

<form action="" method="post" name="contactForm">
<h2 class="green">Rezervare - <span class="blue">Pasul 1</span></h2>

<div style="padding-left:15px;">Toate campurile marcate cu <strong class="red">*</strong> sunt obligatorii!</div>

<div class="NEW-rezervare NEW-round8px clearfix">
  <a href="<?php echo $link_oferta_return; ?>" title="Inapoi la oferta"><span class="cancel float-right"></span></a>
  <div class="item clearfix">
    <div class="left">&nbsp;</div>
    <div class="right"><label><input type="radio" name="sex" value="m" <?php if(!$_POST['sex'] || $_POST['sex']=='m') { ?> checked="checked" <?php } ?>/> <strong class="red">Domnul</strong></label>
    &nbsp;&nbsp;&nbsp;<label><input type="radio" name="sex" value="f" <?php if($_POST['sex']=='f') { ?> checked="checked" <?php } ?> /> <strong class="red">Doamna</strong></label></div>
  </div>
  <div class="item clearfix">
    <div class="left"><label for="prenume" class="titlu">* Prenume</label></div>
    <div class="right"><input name="prenume" id="prenume" type="text" value="<?php if($_POST['prenume']) echo $_POST['prenume']; ?>" class="big" />
    <?php if($err_prenume) { ?><label class="error"><?php echo $err_prenume; ?></label><?php } ?></div>
  </div>
  <div class="item clearfix">
    <div class="left"><label for="nume" class="titlu">* Nume</label></div>
    <div class="right"><input name="nume" id="nume" type="text" value="<?php if($_POST['nume']) echo $_POST['nume']; ?>" class="big" />
    <?php if($err_nume) { ?><label class="error"><?php echo $err_nume; ?></label><?php } ?></div>
  </div>
  <div class="item clearfix">
    <div class="left"><label for="email" class="titlu">* E-mail</label></div>
    <div class="right"><input name="email" id="email" type="text" value="<?php if($_POST['email']) echo $_POST['email']; ?>" class="big" />
    <?php if($err_email) { ?><label class="error"><?php echo $err_email; ?></label><?php } ?></div>
  </div>
  <div class="item clearfix">
    <div class="left"><label for="telefon" class="titlu">* Telefon</label></div>
    <div class="right"><input name="telefon" id="telefon" type="text" value="<?php if($_POST['telefon']) echo $_POST['telefon']; ?>" class="big" />
    <?php if($err_tel) { ?><label class="error"><?php echo $err_tel; ?></label><?php } ?></div>
  </div>
  <div class="item clearfix">
    <div class="left"><label class="titlu">* Data nasterii</label></div>
    <div class="right">
      &nbsp;&nbsp; Zi <select name="ziua"><option value="" selected="selected">--</option><?php for($i=1;$i<=31;$i++) { echo "<option value='$i'"; if($_POST['ziua']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
      Luna <select name="luna"><option value="" selected="selected">--</option><?php for($i=1;$i<=12;$i++) { echo "<option value='$i'"; if($_POST['luna']==$i) echo "selected='selected'"; echo ">".$luna[$i]."</option>"; } ?></select>
      An <select name="an"><option value="" selected="selected">--</option><?php for($i=date("Y")-18;$i>=1930;$i--) { echo "<option value='$i'"; if($_POST['an']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
      <?php if($err_birth) { ?><br /><label class="error"><?php echo $err_birth; ?></label><?php } ?>
    </div>
  </div>
  <div class="item clearfix">
    <div class="left"><label for="data_start1" class="titlu">* Data inceperii sejurului</label></div>
    <div class="right">
<?php
$data_calendar='';
foreach($preturi['data_start_normal'] as $key_d=>$data_start) {
  $data_calendar .= '"'.date('j-n-Y',strtotime($data_start)).'",';
}
$data_calendar=substr($data_calendar,0,-1);
$st = explode('-', $preturi['min_start']);
$en = explode('-', $preturi['max_end']);
?>
      <input type="text" id="data_start1" name="data_start1" value="<?php if($_POST['data_start1']) echo $_POST['data_start1']; ?>" readonly="readonly" />
      <?php if($err_data) { ?><br /><label class="error"><?php echo $err_data; ?></label><?php } ?>
    </div>
  </div>
  <div class="item clearfix">
    <div class="left"><label for="nr_nopti" class="titlu">* Nr. nopti</label></div>
    <div class="right">
      <?php if($preturi['data_end'][$key_d] && $preturi['data_end'][$key_d]<>'00.00.0000') { ?>
      <select name="nr_nopti" id="nr_nopti">
        <option selected="selected" value="">Nr nopti</option>
        <?php for($i=$nr_minim_nopti; $i<=15; $i++) { ?>
        <option value="<?php echo $i; ?>" <?php if($_POST['nr_nopti']==$i) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
        <?php } ?>
      </select>
      <?php } else { ?>
      <input type="text" name="nr_nopti" id="nr_nopti" class="small" value="<?php echo $detalii['nr_nopti']; ?>" readonly="readonly" />
      <?php } ?>
      <?php if($err_nopti) { ?><br /><label class="error"><?php echo $err_nopti; ?></label><?php } ?>
    </div>
  </div>
</div>

<input name="trimite" type="hidden" value="pas1" />
<p style="padding-left:25px;"><input type="submit" value="Continua rezervarea - Pasul 2" class="button-green" /></p>
<br class="clear" /><br />
</form>
