  <div class="item2 clearfix">
    <div class="left"><label for="denumire" class="titlu">* Prenume si Nume</label></div>
    <div class="right"><input name="denumire" id="denumire" type="text" value="<?php if($_POST['denumire']) echo $_POST['denumire']; ?>" class="big" />
    <?php if($err_denumire) { ?><label class="error"><?php echo $err_denumire; ?></label><?php } ?></div>
  </div>
  <div class="item2 clearfix">
    <div class="left"><label for="cui_cnp" class="titlu">CNP</label></div>
    <div class="right"><input name="cui_cnp" id="cui_cnp" type="text" value="<?php if($_POST['cui_cnp']) echo $_POST['cui_cnp']; ?>" class="normal" /></div>
  </div>
  <div class="item2 clearfix">
    <div class="left"><label for="adresa" class="titlu">* Adresa</label></div>
    <div class="right"><textarea name="adresa" id="adresa" class="big"><?php if($_POST['adresa']) echo $_POST['adresa']; ?></textarea>
    <?php if($err_adresa) { ?><label class="error"><?php echo $err_adresa; ?></label><?php } ?></div>
  </div>
  <div class="item2 clearfix">
    <div class="left"><label for="oras" class="titlu">* Oras</label></div>
    <div class="right"><input name="oras" id="oras" type="text" value="<?php if($_POST['oras']) echo $_POST['oras']; ?>" class="normal" />
    <?php if($err_oras) { ?><label class="error"><?php echo $err_oras; ?></label><?php } ?></div>
  </div>
