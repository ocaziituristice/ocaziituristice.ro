<div class="NEW-rezervare NEW-round8px clearfix">
  <a href="<?php echo $link_oferta_return; ?>" title="Inapoi la oferta"><span class="cancel float-right"></span></a>
    <br /><br />
    <h2 class="green" style="text-align:center; font-size:1.8em;">Rezervarea dumneavoastră a fost efectuată cu succes!</h2>
    <br /><br />
    <h3 class="black" style="text-align:center;">În scurt timp un operator <?php echo $denumire_agentie; ?> vă va contacta pentru confirmarea rezervării și pentru a vă cere mai multe informații dacă este cazul.</h3>
    <br /><br />
    <h2 class="blue" style="text-align:center;">Vă mulțumim și vă dorim o zi plăcută în continuare!</h2>
    <br /><br />
</div>
