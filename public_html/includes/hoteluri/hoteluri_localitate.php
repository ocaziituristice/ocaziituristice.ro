<h1 class="blue">Hoteluri <?php echo ucwords(strtolower($localitati)); ?></h1>
<div class="ordonare" align="right">
    <table cellspacing="0">
        <tr>
            <td><span class="red">Ordoneaza dupa:</span></td>
            <td>
                <form name="ord_nume" id="ord_nume" method="post" action=""> 
                  <input type="hidden" name="tip_nume" value="<?php if(!isset($_POST['tip_nume'])) echo 'desc'; elseif(isset($_POST['tip_nume']) && $_POST['tip_nume']=='desc') echo 'asc'; else echo 'desc'; ?>" />
                  <?php if(!isset($_POST['tip_nume'])) { ?>
                  <a href="#" onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_nume.submit();" class="initial" title="Nume Descendent">Nume</a> 
				  <?php } else { ?> <a href="#" onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_nume.submit();" class="selected" title="Nume <?php if(isset($_POST['tip_nume']) && $_POST['tip_nume']=='desc') echo 'Ascedent'; else echo 'Descendent'; ?>"><?php if(isset($_POST['tip_nume']) && $_POST['tip_nume']=='desc') { ?><img src="/images/spacer.gif" class="asc" /> <?php } else { ?><img src="/images/spacer.gif" class="desc" /><?php } ?> Nume</a><?php } ?>
<?php if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') { ?> <input type="hidden" name="stele" value="<?php echo $_REQUEST['stele']; ?>" /><?php } ?>
                </form>   
            </td>
            <?php if(!$_REQUEST['stele']) { ?><td> | </td>
            <td>
                <form name="ord_stele" id="ord_stele" method="post" action="">
                  <input type="hidden" name="tip_stele" value="<?php if(!isset($_POST['tip_stele'])) echo 'desc'; elseif(isset($_POST['tip_stele']) && $_POST['tip_stele']=='desc') echo 'asc'; else echo 'desc'; ?>" />
                  <?php if(!isset($_POST['tip_stele'])) { ?>
                  <a onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_stele.submit();" class="initial" title="Numar stele Descendent">Numar stele</a>
                  <?php } else { ?> <a href="#" onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_stele.submit();" class="selected" title="Numar stele <?php if(isset($_POST['tip_stele']) && $_POST['tip_stele']=='desc') echo 'Ascedent'; else echo 'Descendent'; ?>"><?php if(isset($_POST['tip_stele']) && $_POST['tip_stele']=='desc') { ?><img src="/images/spacer.gif" class="asc" /> <?php } else { ?><img src="/images/spacer.gif" class="desc" /><?php } ?> Numar stele</a><?php } ?>
<?php if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') { ?> <input type="hidden" name="stele" value="<?php echo $_REQUEST['stele']; ?>" /><?php } ?>
                </form>            
            </td>
            <?php } ?>
            <td> | </td>
            <td>
                <form name="ord_implicita" id="ord_implicita" method="post" action="">
                <?php if(isset($_POST['tip_relevanta']) || (isset($_POST['tip_stele']) && !$_REQUEST['stele']) || isset($_POST['tip_pret'])) { ?>
                <a onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_implicita.submit();" class="initial" title="Ordonare implicita">Ordonare implicita</a>
                <?php } else { ?>
                <a onclick="<?php /*?>$('#loader').show();<?php */?> document.ord_implicita.submit();" class="selected" title="Ordonare implicita">Ordonare implicita</a>
                <?php } ?>
 <?php if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') { ?> <input type="hidden" name="stele" value="<?php echo $_REQUEST['stele']; ?>" /><?php } ?>
                </form>            
            </td>            
        </tr>
    </table>
</div>
<?php $afisare =new AFISARE_HOTELURI();
$afisare->set_oferte_pagina('20');
$afisare->setTari($_REQUEST['tara']);
$afisare->setZone($_REQUEST['zona']);
$afisare->setOrase($_REQUEST['oras']);
if(eregi('fara_oferte', $_SERVER['REQUEST_URI'])) $afisare->setFaraOferte('da');
if($_REQUEST['stele']) $afisare->setStele($_REQUEST['stele']);
if($_POST['tip_nume']) $afisare->setOrdonareNume($_POST['tip_nume']);
if($_POST['tip_stele'] && !$_REQUEST['stele']) $afisare->setOrdonareStele($_POST['tip_stele']);
$afisare->paginare(); ?>
<br class="clear" /><br />