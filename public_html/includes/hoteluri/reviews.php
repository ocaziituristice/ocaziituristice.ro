<div class="reviews-header clearfix">
  <?php if(mysql_num_rows($que_comentarii)>0) { ?>
  <div class="float-left black" style="width:330px; height:135px; padding:25px 20px 0 20px;">
    <span class="bigger-15em bold">Total: <span class="blue" style="font-size:36px;"><?php echo $nota_total; ?></span></span><br><br>
    <p style="padding-bottom:10px;">Notă bazată pe <strong><?php echo $nr_comentarii; ?> comentarii</strong>.</p>
    Comentariile turiștilor sunt scrise de clienții noștri după șederea lor la <strong><?php echo $detalii_hotel['denumire'].', '.$detalii_hotel['localitate']; ?></strong>.
  </div>
  <div class="float-left" style="background:#FFF; width:290px; height:160px; padding:10px 20px 0 30px;">
    <div class="rating">
      <div class="camp">Personalul</div>
      <div class="nota_img"><div class="inner" style="width:<?php echo $personalul*10; ?>px;"></div></div>
      <div class="nota_txt"><?php echo number_format($personalul,1); ?></div>
    </div>
    <div class="rating">
      <div class="camp">Facilități</div>
      <div class="nota_img"><div class="inner" style="width:<?php echo $facilitati*10; ?>px;"></div></div>
      <div class="nota_txt"><?php echo number_format($facilitati,1); ?></div>
    </div>
    <div class="rating">
      <div class="camp">Confort</div>
      <div class="nota_img"><div class="inner" style="width:<?php echo $confort*10; ?>px;"></div></div>
      <div class="nota_txt"><?php echo number_format($confort,1); ?></div>
    </div>
    <div class="rating">
      <div class="camp">Curățenie</div>
      <div class="nota_img"><div class="inner" style="width:<?php echo $curatenie*10; ?>px;"></div></div>
      <div class="nota_txt"><?php echo number_format($curatenie,1); ?></div>
    </div>
    <div class="rating">
      <div class="camp">Raport preț/calitate</div>
      <div class="nota_img"><div class="inner" style="width:<?php echo $raport_pret_calitate*10; ?>px;"></div></div>
      <div class="nota_txt"><?php echo number_format($raport_pret_calitate,1); ?></div>
    </div>
    <div class="rating">
      <div class="camp">Amplasare</div>
      <div class="nota_img"><div class="inner" style="width:<?php echo $amplasare*10; ?>px;"></div></div>
      <div class="nota_txt"><?php echo number_format($amplasare,1); ?></div>
    </div>
  </div>
  <div class="float-left" style="background:#FFF; width:260px; height:105px; padding-top:65px; margin-left:2px;">
  <?php } else { ?>
  <div class="float-left pad20" style="width:670px; height:130px;">
    <div class="black bigger-14em">Adaugă comentariul tău despre <strong><?php echo $detalii_hotel['denumire'].', '.$detalii_hotel['localitate']; ?></strong>.</div>
    Dacă ai stat la <?php echo $detalii_hotel['denumire'].', '.$detalii_hotel['localitate']; ?> te rugăm să ne impărtășești opiniile despre șederea ta. <span class="black bold underline">Părerea ta contează!</span><br>
    Încearcă să fii cât mai obiectiv, ajutând astfel ceilalți turiști să își facă o părere despre <?php echo $detalii_hotel['denumire'].', '.$detalii_hotel['localitate']; ?>.
  </div>
  <div class="float-left" style="background:#FFF; width:260px; height:125px; padding-top:45px; margin-left:2px;">
  <?php } ?>
	<div id="add-review" class="button-blue" style="display:inline-block; margin:0 0 0 55px;">Adaugă comentariu</div>
  </div>
</div>

<div id="adauga-review" style="display:none;"><?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri/reviews_add.php"); ?></div>

<br>

<div class="pad20">

<?php if(mysql_num_rows($que_comentarii)) {
	foreach($comentarii['id_comentariu'] as $key_comentariu => $value_comentariu) { ?>
<div class="comentariu">
  <div class="nume">
    <span class="bigger-13em blue bold"><?php if($comentarii['anonim'][$key_comentariu]!='da') echo $comentarii['nume'][$key_comentariu]; else echo 'Anonim'; ?></span><br>
    <span class="black"><?php echo $comentarii['tip_client'][$key_comentariu]; ?></span><br>
    <span class="smaller-08em"><?php echo denLuniRo(date('d F Y',strtotime($comentarii['data_adaugarii'][$key_comentariu]))); ?></span>
  </div>
  <div class="pareri">
    <?php if(strlen($comentarii['comentarii_plusuri'][$key_comentariu])>0) { ?><div class="plusuri"><?php echo afisare_frumos($comentarii['comentarii_plusuri'][$key_comentariu]); ?></div><?php } ?>
    <?php if(strlen($comentarii['comentarii_minusuri'][$key_comentariu])>0) { ?><div class="minusuri"><?php echo afisare_frumos($comentarii['comentarii_minusuri'][$key_comentariu]); ?></div><?php } ?>
    <?php if(strlen($comentarii['comentarii_minusuri'][$key_comentariu])==0 and strlen($comentarii['comentarii_minusuri'][$key_comentariu])==0) { ?>Ai recomanda hotelul si altor persoane? <span class="bold blue bigger-13em"><?php echo $comentarii['recomand'][$key_comentariu]; ?></span><?php } ?>
  </div>
  <div class="nota text-center blue">
    <?php echo $comentarii['nota_total'][$key_comentariu]; ?>
    <div class="nota_img"><div class="inner" style="width:<?php echo $comentarii['nota_total'][$key_comentariu]*10; ?>px;"></div></div>
  </div>
  <div class="clear"></div>
</div>
<?php }
} ?>

</div>
