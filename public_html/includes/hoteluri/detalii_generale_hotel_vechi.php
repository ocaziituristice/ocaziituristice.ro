<?php
$den_hotel           = $detalii_hotel['denumire'];
$den_loc             = $detalii_hotel['localitate'];
$id_tara             = $detalii_hotel['id_tara'];
$hoteluri_localitate = hoteluri_localitate( $detalii_hotel['id_localitate'], $id_hotel, $detalii_hotel['latitudine'], $detalii_hotel['longitudine'] );
for ( $stea = 1; $stea <= $detalii_hotel['stele']; $stea ++ ) {
	$stars .= '<i class="icon-star yellow"></i>';
}
$link_cerere_detalii = $sitepath . 'includes/sejururi/cere_mai_multe_detalii.php?hotel=' . $id_hotel;
$asigurare           = 'nu';
$detalii_online      = $det->oferte_online( $id_hotel );
if ( ! isset($_COOKIE['grad_ocupare'] ) ) {
	$detalii_online = $det->oferte_online( $id_hotel );
} else {
	$grdocup = explode( "*", $_COOKIE['grad_ocupare'] );

	//setare data plecare
	if ( $grdocup[0] == '' ) {
		$data_plecare = '';
	} else {
		$data_plecare = date( "Y-m-d", strtotime( $grdocup[0] ) );
	}

	// daca exista transport
	if ( isset( $_GET['plecare-avion'] ) ) {
		//$tip_transport_trimis=$trasport_trimis[0];
		$orasplec_denumire = $_GET['plecare-avion'];
		$orasplec          = get_id_localitate( $orasplec_denumire );
	} else {
		$tip_transport = $grdocup[8];
		$orasplec      = $grdocup[9];
	}

	$detalii_online = $det->oferte_online( $id_hotel, $data_plecare, $tip_transport, $orasplec );
}

if ( isset( $_COOKIE['grad_ocupare'] ) ) {
	$grad_ocupare  = explode( "*", $_COOKIE['grad_ocupare'] );
	$plecdata      = $grad_ocupare[0];
	$pleczile      = $grad_ocupare[1];
	$adulti        = $grad_ocupare[2];
	$copii         = $grad_ocupare[3];
	$copil_age[0]  = $grad_ocupare[4];
	$copil_age[1]  = $grad_ocupare[5];
	$copil_age[2]  = $grad_ocupare[6];
	$tip_masa      = $grad_ocupare[7];
	$tip_transport = $grad_ocupare[8];
	if ( $grad_ocupare[9] != '' and ! isset( $_GET['plecare-avion'] ) ) {
		$orasplec = $grad_ocupare[9];
	}
	if ( count( $detalii_online['tip_transport'] ) == 1 and key( $detalii_online['tip_transport'] ) != $tip_transport ) {
		
		setcookie( "grad_ocupare", $plecdata . '*' . $pleczile . '*' . $adulti . '*' . $copii . '*' . $copil_age[0] . '*' . $copil_age[1] . '*' . $copil_age[2] . '*' . $tip_masa . '*' . key( $detalii_online['tip_transport'] ) . '*' . $orasplec, time() + 60 * 60 * 24 * 30, "/" );
		$tip_transport = key( $detalii_online['tip_transport'] );
	}
}


if(isset($_REQUEST['plecdata'])) {

    $plecdata = $_REQUEST['plecdata'];
	$pleczile = $_REQUEST['pleczile'];
	$adulti = $_REQUEST['adulti'];
	$copii = $_REQUEST['copii'];
	$copil_age[0] = $_REQUEST['age'][0];
	$copil_age[1] = $_REQUEST['age'][1];
	$copil_age[2] = $_REQUEST['age'][2];
	if(isset($_REQUEST['tip_masa'])) $tip_masa = $_REQUEST['tip_masa'];
	if(isset($_REQUEST['tip_transport'])) $tip_transport = $_REQUEST['tip_transport'];
	if(isset($_REQUEST['showerr'])) $showerr = $_REQUEST['showerr'];
}

?>
<div style="display: none"><?php var_dump($_SERVER['REMOTE_ADDR']) ?></div>
<section class="hotel">
    <div class="clear">
        <div class="left-column">

			<?php // Hotel name and rating ?>
            <h1><?php echo $detalii_hotel['denumire'] ?>
                <span class="stars">
                       <?php echo $stars; ?>
                    </span>
            </h1>

			<?php // Hotel address ?>
            <p class="address" data-scroll="map"><i
                        class="icon-location"></i> <?php echo $detalii_hotel['adresa'] . ' ';
				if ( $detalii_hotel['nr'] ) {
					echo 'nr. ' . $detalii_hotel['nr'] . ', ';
				}
				echo $detalii_hotel['localitate'] . ', ';
				echo $detalii_hotel['tara'];
				if ( $detalii_hotel['distanta'] ) {
					echo ', <strong>' . $detalii_hotel['distanta'] . 'm de ' . $detalii_hotel['distanta_fata_de'] . '</strong>, ';
				}
				if ( $detalii_hotel['latitudine'] && $detalii_hotel['longitudine'] && $detalii_hotel['latitudine'] <> 0 && $detalii_hotel['longitudine'] <> 0 ) { ?>
				<?php } ?>
            </p>
        </div>
        <div class="right-column">
            <div class="recommended">
                <i class="icon-thumbs-up-alt"></i>
                <p>Recomandat pentru: <span><?php echo $detalii_hotel['concept']?></span></p>
            </div>
        </div>
    </div>
    <div class="clear">
        <div class="left-column images">
            <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-fit="cover"
                 data-loop="true" data-ratio="1.7">
				<?php for ( $i = 1; $i <= 20; $i ++ ) {
					if ( $detalii_hotel[ 'poza' . $i ] ) { ?>
                        <a href="https://www.ocaziituristice.ro/img_prima_hotel/<?php echo $detalii_hotel[ 'poza' . $i ]; ?>"
                           data-thumb="https://www.ocaziituristice.ro/img_prima_hotel/<?php echo $detalii_hotel[ 'poza' . $i ]; ?>"></a>
					<?php }
				} ?>
            </div>
			<?php if ( ( sizeof( $detalii_online['early_time'] ) > 0 or $detalii_hotel['data_early_booking'] > date( 'Y-m-d' ) ) and $detalii_online['last_minute'] < 1 ) { ?>
				<?php if ( is_array( $detalii_online['early_disc'] ) ) {
					$reduce_eb = max( $detalii_online['early_disc'] );
				}
				if ( $reduce_eb < $detalii_hotel['comision_reducere'] ) {
					$reduce_eb = $detalii_hotel['comision_reducere'];
				}
				if ( $reduce_eb > 1 ) {
					echo '<div class="discount eb"> <strong>Reducere <br />Early Booking <span>' . $reduce_eb . '% </span></strong></div>';
				}
			} ?>
			<?php if ( $detalii['spo_titlu'] ) {
				echo '<div class="oferta-speciala"></div>';
			};
			if ( $detalii_online['last_minute'] > 0 ) {
				echo '<div class="last-minute"></div>';
			} ?>
        </div>
        <div class="trust right-column"></div>
    </div>
	<?php include_once( "includes/price_form.php" ) ?>
    <div id="preturiOnline"></div>
    <script>
        var id_hotel = <?php echo $id_hotel ?>;
        $("#preturiOnline").empty().html('<br><span class="bigger-13em bold">Vă rugăm așteptați. Verificăm preţurile şi disponibilitatea în timp real.</span><br><img src="/images/loader3.gif" alt="loading">');
        setTimeout(function() { $("#preturiOnline").load("/includes/preturi_online_hotel_responsive.php?<?php echo 'idh='.$id_hotel.'&moneda='.$detalii_hotel['moneda'].'&plecdata='.$plecdata.'&pleczile='.$pleczile.'&adulti='.$adulti.'&copii='.$copii.'&copil1='.$copil_age[0].'&copil2='.$copil_age[1].'&copil3='.$copil_age[2].'&masa='.$tip_masa.'&trans='.$tip_transport.'&orasplec='.$orasplec.'&showerr='.$showerr; ?>"); }, 10);
        $("#button-prices").click(function() {
            $('html, body').animate({
                scrollTop: $("#preturi").offset().top
            });
        });
    </script>
    <br class="clear">
	
       <h2 class="expand icon">Descriere <?php echo $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate']; ?></h2>
    <div class="expandable description">  
		
			<?php 
		$textdescriere.=nl2p(htmlspecialchars_decode($detalii_hotel["descriere_scurta"]));
		$textdescriere.=nl2p(htmlspecialchars_decode($detalii_hotel["descriere"]));
		      if ( $detalii_hotel["general"] ) : 
					$textdescriere.='<h3>General</h3>';
					$textdescriere.='<p>'.str_replace('<li>-','<li>',nl2li($detalii_hotel["general"])).'</p>';
				 endif;
       
	
				if ( $detalii_hotel["servicii"] ) :
					$textdescriere.='<p><h3>Servicii</h3>';
					
					$textdescriere.=str_replace('<li>-','<li>',nl2li($detalii_hotel["servicii"])).'</p>';
					
				endif;
	 if ( $detalii_hotel["internet"] ) :
				$textdescriere.='<h3>Internet</h3>';
				$textdescriere.=nl2p(htmlspecialchars_decode($detalii_hotel["internet"]));
			     endif;
				if ( $detalii_hotel["parcare"] ) : 
			$textdescriere.='<h3>Parcare</h3>';
			$textdescriere.=nl2p(htmlspecialchars_decode($detalii_hotel["parcare"]));
			 endif; 
			 
			  if( ( $detalii_hotel["check_in"] ) or ( $detalii_hotel["check_out"] ) ) :
			$textdescriere.='<h3>Check-in / check-out</h3>';
			$textdescriere.=$detalii_hotel["check_in"].'/'.$detalii_hotel["check_out"];
			 endif; 
			 
			 if ( $detalii_hotel["accepta_animale"] ) : 
			 $textdescriere.='<h3>Animale companie</h3>';
			$textdescriere.=nl2p(htmlspecialchars_decode($detalii_hotel["accepta_animale"]));
			 endif;
			  if ($detalii_hotel["plaja"] ) : 
			 $textdescriere.='<h3>Plaja</h3>';
			$textdescriere.=nl2p(htmlspecialchars_decode($detalii_hotel["plaja"]));
			 endif;
			  if ($detalii_hotel["allinclusive"] ) : 
			 $textdescriere.='<h3>All Inclusive</h3>';
			$textdescriere.=nl2p(htmlspecialchars_decode($detalii_hotel["allinclusive"]));
			 endif;
			 
	 
			  ?>



<?php
$Columns = SplitIntoColumns ($textdescriere, 3, "<p>");
echo '<div class="details">'.($Columns[0]).'</div>';
echo '<div class="facilities">'.($Columns[1]).'</div>';	
echo '<div class="facilities-extra">'.($Columns[2]).'</div>';	
//if($_SESSION['mail']=='razvan@ocaziituristice.ro') echo '<pre>';print_r ($detalii);echo '</pre>';

?>

</div>

<?php 	if($id_tara=='9') 
		{echo
			'<div class="info"><h3>Info taxe Grecia</h3>
			Incepand cu 1 ianuarie 2018, autoritatile de resort din Grecia au introdus o taxa turistica (de oras). Aceasta taxa se va aplica si pentru sejururile care au fost rezervate inainte de 1 ianuarie 2018, dar care se desfasoara dupa aceasta data. <br />

Taxa nu este inclusa in tarifele cu care sunt vandute sejururile in Grecia oferite de Aerovacante. Aceasta taxa se va achita de catre turisti la receptia vilei sau hotelului.<br />

Valoarea aproximativa a acestei taxe, care difera in functie de categoria spatiilor de cazare este:<br />
- 0,5 Euro / camera / noapte pentru vile si hoteluri de 1* si 2*<br />
- 1,5 Euro / camera / noapte pentru hoteluri 3*<br />
- 3 Euro / camera / noapte pentru hoteluri 4*<br />
- 4 Euro / camera / noapte pentru hoteluri 5*</div>';

			}?>
	
	<?php
	$sel_detcam = "SELECT camere_hotel.detalii, tip_camera.denumire FROM camere_hotel INNER JOIN tip_camera ON tip_camera.id_camera = camere_hotel.id_camera WHERE id_hotel='".$id_hotel."' AND (detalii<>NULL OR detalii<>'')";
	$que_detcam = mysql_query($sel_detcam) or die(mysql_error());
	if ( mysql_num_rows($que_detcam) > '0' ) :
		?>
        <h2 class="expand icon">Detalii camere <?php echo $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate']; ?></h2>
        <div class="expandable description">
            <ul class="room-details">
				<?php while($row_detcam = mysql_fetch_array($que_detcam)) {
					echo '<li>';
					echo '<h3>'.$row_detcam['denumire'].'</h3>';
					echo '<span>'.nl2br($row_detcam['detalii']).'</span>';
					echo '</li>';
				} ?>
            </ul>
        </div>
	<?php endif; ?>
	<?php include_once( "includes/impresii_hotel.php" ) ?>
   <?php if(intval($detalii_hotel['latitudine'])>1 and intval($detalii_hotel['longitudine'])>1) {?> 
    <h2 class="expand icon">Localizare pe harta <?php echo $detalii_hotel['denumire'] . ' ' . $detalii_hotel['localitate']; ?></h2>
    <div class="map">
        <div class="expandable" id="map"></div>
        <div class="nearby">
            <h3>Hoteluri din <?php echo $detalii_hotel['localitate'] ?> in apropiere</h3>
			<?php $hoteluri_localitate_1 = multid_sort($hoteluri_localitate, 'distanta' ); ?>
			<?php if ( sizeof( $hoteluri_localitate_1 ) > 3 ) { ?>
				<?php for ( $id = 1; $id <= 5; $id ++ ) {
					echo "
					<div class=\"descriere\">
					<a href=\"".fa_link_hotel($detalii_hotel['localitate'],$hoteluri_localitate_1[ $id ]['denumire'] )."\" title=\"".$hoteluri_localitate_1[ $id ]['denumire'] . " " . $detalii_hotel['localitate'] ."\">
                    <img src=\"" . $sitepath . "thumb_hotel/" . $hoteluri_localitate_1[ $id ]['poza1'] . "\" alt=\"" . $hoteluri_localitate_1[ $id ]['denumire'] . " " . $detalii_hotel['localitate'] . "\">
					<span class=\"title\">" . $hoteluri_localitate_1[ $id ]['denumire'] . "</span>
					<span class=\"stars\">" . afiseaza_stele( $hoteluri_localitate_1[ $id ]['stele'] ) . "</span></a>
					<span >la " . $hoteluri_localitate_1[ $id ]['distanta'] . " metri</span> 
					</div><br />
				";
				}
			} ?>
        </div>
    </div>
<?php // final harta 
   }
//if(!$err_logare_admin) {echo '<pre>';print_r($detalii_online['id_oferta']);echo '</pre>';}
//echo (intval($detalii_hotel['latitudine'])>1 and intval($detalii_hotel['longitutine'])>1);}

?>
	<?php include_once( "includes/hoteluri_vizualizate_altii_responsive.php" ) ?>
    <section class="bottom second_mod"></section>
    <script>
        var locations = [
			<?php
			foreach ( $hoteluri_localitate_1 as $hoteluri_pe_harta ) {
				if ( $hoteluri_pe_harta['id_hotel'] == $id_hotel ) {
					echo "[\"" . $hoteluri_pe_harta['denumire'] . "- hotelul curent\"," . $hoteluri_pe_harta['latitudine'] . "," . $hoteluri_pe_harta['longitudine'] . ", \"https://www.ocaziituristice.ro/images/icon_maps_hotel.png\"],";
				} else {
					echo "[\"<a href='".fa_link_hotel($detalii_hotel['localitate'],$hoteluri_pe_harta['denumire'])."'>" . $hoteluri_pe_harta['denumire']." - ".$detalii_hotel['localitate'] . "</a>\"," . $hoteluri_pe_harta['latitudine'] . "," . $hoteluri_pe_harta['longitudine'] . ", \"https://www.ocaziituristice.ro/images/icon_maps_hotel_normal.png\"],";
				}
			}
			?>
        ];

        function initGoogleMap() {
            var infowindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: new google.maps.LatLng(<?php  echo $detalii_hotel['latitudine']?>,   <?php echo $detalii_hotel['longitudine']?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            function placeMarker(loc) {
                var latLng = new google.maps.LatLng(loc[1], loc[2]);
                var markerIcon = loc[3];
                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: markerIcon,
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.close();
                    infowindow.setContent("<div id='infowindow'>" + loc[0] + "</div>");
                    infowindow.open(map, marker);
                });
            }

            for (var i = 0; i < locations.length; i++) {
                placeMarker(locations[i]);
            }
        }

        if (isDevice) {
            $("#map").html('<span class="static" style="background-image: url(\'https://maps.googleapis.com/maps/api/staticmap?center=<?php  echo $detalii_hotel['latitudine']?>,<?php  echo $detalii_hotel['longitudine']?>&zoom=16&size=300x500&maptype=roadmap&markers=icon:https://www.ocaziituristice.ro/images/icon_maps_hotel.png|<?php  echo $detalii_hotel['latitudine']?>,<?php  echo $detalii_hotel['longitudine']?>=<?php echo $google_api_key_static ?>\'"></span>');
        } else {
            google.maps.event.addDomListener(window, 'load', initGoogleMap);
        }
    </script>
    <script>
        $(function () {
            $(".address").click(function (e) {
                e.preventDefault();
                goToByScroll($(this).data("scroll"), 'h2');
            });
        });

        $(window).load(function () {
            $(".visited ul li p").dotdotdot();
        });

        $(window).resize(function () {
            $(".visited ul li p").dotdotdot();
        });
    </script>
    <script>

        //Show Age
        function displayAges(nVal) {
            if (nVal > 0) {
                $(".chd-ages").show();
            } else {
                $(".chd-ages").hide();
            }

            for (var i = 0; i < 3; i++) {
                if (i < nVal) {
                    $(".varste-copii-" + i).css("display", "inline-block");
                } else {
                    $(".varste-copii-" + i).css("display", "none");
                    $(".varste-copii-select-" + i).val(0);
                }
            }
        }

        $('.link_card_mobile').click(function (e) {
            e.preventDefault();
            $(this).next('.card_mobile').toggle('slow');
        });

        //Include modal different media
        var mq = window.matchMedia("(min-width: 800px)");
        if (mq.matches) {
            $(".trust").load("/includes/trustme.php");
        } else {
            $(".second_mod").load("/includes/trustme.php");

        }
    </script>
</section>


<script>
    $("#button-comentarii").click(function () {
        $('html, body').animate({
            scrollTop: $("#comentarii").offset().top - 220
        });
    });
    $("#button-harta").click(function () {
        $('html, body').animate({
            scrollTop: $("#harta").offset().top - 220
        });
    });
</script>
<script>

    function checkHELLO(field, rules, i, options) {
        if (field.val() != "HELLO") {
            return options.allrules.validate2fields.alertText;
        }
    }
</script>
<!--
<script>
    $(document).ready(function() {

    })
</script>
-->