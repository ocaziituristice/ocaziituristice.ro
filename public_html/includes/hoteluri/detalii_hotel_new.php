<?php $link_cerere_detalii=$sitepath.'includes/sejururi/cere_mai_multe_detalii.php?hotel='.$id_hotel; ?>
<div id="NEW-detaliiOferta">
<div class="NEW-column-left1">

  <h1 class="red"><?php echo $detalii_hotel['denumire']; ?> <span class="black"><?php echo $detalii_hotel['localitate']; ?></span> <img src="/images/spacer.gif" class="stele-mari-<?php echo $detalii_hotel['stele']; ?>" alt="" /></h1>

  <br class="clear" />
  
  <div class="NEW-adresa bigger-11em">
    <?php echo $detalii_hotel['adresa'].' ';
	if($detalii_hotel['nr']) echo 'nr. '.$detalii_hotel['nr'].', ';
	echo $detalii_hotel['localitate'].', ';
	echo $detalii_hotel['tara'];
	if($detalii_hotel['distanta']) {
		echo ', <strong>'.$detalii_hotel['distanta'].'m de '.$detalii_hotel['distanta_fata_de'].'</strong>, ';
    }
	if($detalii_hotel['latitudine'] && $detalii_hotel['longitudine'] && $detalii_hotel['latitudine']<> 0 && $detalii_hotel['longitudine'] <> 0) { ?>
		(<a href="#harta" class="link" onclick="jQuery('#ofTabs').tabs('option', 'active', 1 );"><strong>Arata harta</strong></a>)
	<?php } ?>
  </div>
  
  <div class="clearfix pad5">
    <?php if($nota_total>0) { ?>
    <div class="rating">
      <div class="nota_img" style="margin:2px 7px 0 0;"><div class="inner" style="width:<?php echo $nota_total*10; ?>px;"></div></div>
      <span class="bold blue" style="font-size:20px;"><?php echo $nota_total; ?></span>
      <span class="black smaller-09em">(scor calculat din <strong><?php echo $nr_comentarii; ?></strong> comentarii)</span>
      <a href="#comentarii" class="link-red bold" onclick="jQuery('#ofTabs').tabs('option', 'active', 2 );">Vezi toate comentariile</a>
    </div>
    <?php } else { ?>
    <img src="/images/icon_nou_small.png" alt="nou" class="float-left pad5">
    <div>
      <span class="black">La acest hotel nu exista comentarii</span>
      <a href="#comentarii" class="link-red bold" onclick="jQuery('#ofTabs').tabs('option', 'active', 2 );">Adauga comentariu</a>
    </div>
    <? } ?>
  </div>
    
  <br class="clear" />
  
  <div class="float-left" style="width:310px;">
    <div class="NEW-gallery">
    <?php if($detalii_hotel['poza1']) { ?>
      <div class="single-img">
        <img src="/img_mediu_hotel/<?php echo $detalii_hotel['poza1']; ?>" width="300" height="300" alt="<?php echo $detalii_hotel['denumire']." imagine principala"; ?>" class="images" id="poza_principala" />
      </div>
    <?php } else { ?>
      <div class="single-img"><img src="/images/no_photo.jpg" width="300" height="300" alt="" class="images" id="poza_principala" /></div>
    <?php } ?>
    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
  </div>

  <div class="float-left" style="width:358px;">
    <div class="NEW-gallery">
<?php
for($i=1;$i<=8;$i++) {
if($detalii_hotel['poza'.$i]) { ?>    
    <a href="/img_prima_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" onmouseover="document.getElementById('poza_principala').src='/img_mediu_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>';return false;" class="gallery"><img src="/thumb_hotel/<?php echo $detalii_hotel['poza'.$i]; ?>" width="80" alt="<?php echo $detalii_hotel['denumire']." imagine ".$i; ?>" class="images" /></a>
<?php }
} ?>
	</div>

    <div class="info-scurte-chn" style="padding-top:10px;">
      <?php if($detalii_hotel['concept']) { ?><div class="bigger-11em black bold" style="padding:0 0 10px 10px;">Recomandat pentru: <strong class="bigger-13em red"><?php echo $detalii_hotel['concept']; ?></strong></div><?php } ?>
      <?php if($detalii_hotel['detalii_concept']) { echo '<div class="detalii-concept bigger-11em bold"><em>'.$detalii_hotel['detalii_concept'].'</em></div>'; } ?>
    </div>
  </div>
  
  <br class="clear" /><br />

</div>
<div class="NEW-column-right1">
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/incredere_dece_ocazii.php"); ?>
  <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/dreapta/alte_oferte_hotel.php"); ?>
</div>

<br class="clear" />

<div class="NEW-column-full2">
  
  <?php $oferte=$det->get_oferte($id_hotel, $id_oferta);
  if(sizeof($oferte)>0) { ?>
  <div class="ofDisp NEW-round8px">
    <h3 class="black">Oferte disponibile la <span class="blue"><?php echo $detalii_hotel['denumire'].' '.$detalii_hotel['stele'].'*'; ?></span></h3>
    <ul class="inner clearfix">
      <?php foreach($oferte as $key_of=>$value_of) {
	  $c++;
	  if($c==4) $c=1;
	  $link = substr($sitepath,0,-1).make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $value_of['denumire'], $value_of['id_oferta']);
      $tipuri=explode('+',$value_of['poza_tip']);
	  $class = 'class="';
	  if($value_of['oferta_speciala']=='da') $class .= 'special ';
	  if(sizeof($oferte)<=3) {
		  if(sizeof($oferte)%3==0) {
			  $class .= 'cols-3';
			  if($c==3) $class .= ' last-right';
			  $class .= '"';
		  } else if(sizeof($oferte)%2==0) {
			  $class .= 'cols-2';
			  if($c==2) $class .= ' last-right';
			  $class .= '"';
		  } else {
			  $class .= 'cols-1';
			  if($c==1) $class .= ' last-right';
			  $class .= '"';
		  }
	  } else {
		  $class .= 'cols-3';
		  if($c==3) $class .= ' last-right';
		  $class .= '"';
	  }
	  ?>
      <li <?php echo $class; ?>><a href="<?php echo $link; ?>" class="clearfix">
        <span class="titlu"><?php echo $value_of["denumire_scurta"]; ?></span>
        <span class="pret">de la <?php echo '<span class="green">'.$value_of['pret_minim'].' '.moneda($value_of['moneda']).'</span> /'.$value_of['exprimare_pret']; ?></span>
        <?php if($value_of['nr_nopti']>1) { ?><span class="durata">Sejur: <strong><?php echo $value_of['nr_nopti'].' nopti'; ?></strong></span><?php } ?>
        <span class="masa">Masa: <strong><?php echo $value_of['masa']; ?></strong></span>
        <span class="transport">Transport: <strong><?php echo $value_of['denumire_transport']; ?></strong></span>
        <span class="detalii"></span>
      </a></li>
      <?php } ?>
    </ul>
  </div>
  <br class="clear"><br>
  <?php } ?>
  
  <div id="ofTabs">
    <ul>
      <li><a href="#detalii-hotel">Detalii Hotel</a></li>
      <li><a href="#harta">Harta</a></li>
      <li><a href="#comentarii" onclick="ga('send', 'event', 'reviews', 'citeste reviews hotel', '<?php echo substr($sitepath,0,-1).$_SERVER['REQUEST_URI']; ?>');"><img src="/images/icon_nou_small.png" alt="nou"> Comentarii clienti</a></li>
	  <?php /*$oferte=$det->get_oferte($id_hotel, $id_oferta); if(sizeof($oferte)>0) { ?><li><a href="#alte-oferte">Oferte disponibile la <?php echo $detalii_hotel['denumire']; ?></a></li><?php }*/ ?>
    </ul>
    <div id="detalii-hotel">
  
      <br class="clear" /><br>
    
	  <?php
      if($detalii_hotel['distanta']) {
          echo '<div class="bold bigger-13em" style="padding:0 0 0 10px;">';
          echo '<img src="'.$imgpath.'/icon_dist_'.$detalii_hotel['distanta_fata_de'].'.jpg" alt="" title="'.$detalii_hotel['distanta'].'m de '.$detalii_hotel['distanta_fata_de'].'" class="NEW-round6px float-left" style="margin:0 10px 0 0;" />';
          echo '<span class="blue" style="line-height:32px;">'.$detalii_hotel['denumire'].'</span> este situat la <span class="bigger-11em red">'.$detalii_hotel['distanta'].'m de '.$detalii_hotel['distanta_fata_de'].'</span>';
          echo '</div><br class="clear" />';
      }
      ?>
      
      <br class="clear" /><br>
    
	  <?php if($detalii_hotel['fisier_upload']) { ?><p style="padding:5px 10px 0 10px;"><span class="blue icon-coffee"></span> <a href="/uploads/hotel/<?php echo $detalii_hotel['fisier_upload']; ?>" target="_blank" class="link-blue"><strong class="blue">Concept <?php echo $detalii_hotel['denumire']; ?></strong></a></p><?php } ?>
	  
	  <?php if($detalii_hotel['website'] and $detalii_hotel['tara']!='Romania') { ?><p style="padding:5px 10px 0 10px;"><span class="blue icon-link"></span> <a href="<?php echo hotel_link($detalii_hotel['website']); ?>" target="_blank" class="link-blue"><strong class="blue">Adresa website <?php echo $detalii_hotel['denumire']; ?></strong>: <?php echo hotel_link($detalii_hotel['website']); ?></a></p><?php } ?>
	  
      <?php if($detalii_hotel['new_descriere']) { ?>
  
      <div class="section" style="margin:0;">
        <?php if($detalii_hotel["comentariul_nostru"]) { ?>
        <p class="black"><strong><u>Comentariul nostru</u>:</strong> <?php afisare_frumos(schimba_caractere($detalii_hotel["comentariul_nostru"]), ''); ?></p>
        <br />
        <?php } ?>
        <?php afisare_frumos(schimba_caractere($detalii_hotel["new_descriere"]), 'nl2p'); ?>
        <div id="detHotNew-container">
          <?php if($detalii_hotel["new_camera"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Camera</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_camera"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_teritoriu"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Teritoriu</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_teritoriu"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_relaxare"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Relaxare si sport</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_relaxare"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_pentru_copii"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Pentru copii</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_pentru_copii"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
          <?php if($detalii_hotel["new_plaja"]) { ?>
          <div class="detHotelNew2">
            <div class="titlu">Plaja</div>
            <?php afisare_frumos(schimba_caractere($detalii_hotel["new_plaja"]), 'NEW_nl2li'); ?>
          </div>
          <?php } ?>
        </div>
      </div>
  
      <?php } else { ?>
      
      <div class="section" style="margin:0;">
        <div class="pad10"><?php afisare_frumos(schimba_caractere($detalii_hotel["descriere_scurta"]), 'nl2p'); afisare_frumos(schimba_caractere($detalii_hotel["descriere"]), 'nl2p'); ?></div>
  
      <?php if($detalii_hotel["general"] || $detalii_hotel["servicii"] || $detalii_hotel["internet"] || $detalii_hotel["parcare"]) { ?>
        <?php if($detalii_hotel["general"]) { ?>
        <h3 class="black">General</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["general"], 'nl2br'); ?></div>
        <div class="clear-line"></div> 
        <?php }
        if($detalii_hotel["servicii"]) { ?>
        <h3 class="black">Servicii</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["servicii"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["internet"]) { ?>
        <h3 class="black">Internet</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["internet"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["parcare"]) { ?>
        <h3 class="black">Parcare</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["parcare"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php } ?>
        <?php if( ($detalii_hotel["check_in"]) or ($detalii_hotel["check_out"]) ) { ?>
        <h3 class="black">Check-in / check-out</h3>
        <div class="content"><?php echo $detalii_hotel["check_in"]; ?> / <?php echo $detalii_hotel["check_out"]; ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["info_copii"]) { ?>
        <h3 class="black">Copii si paturi suplimentare</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["info_copii"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php }
        if($detalii_hotel["accepta_animale"]) { ?>
        <h3 class="black">Animale companie</h3>
        <div class="content"><?php afisare_frumos($detalii_hotel["accepta_animale"], 'nl2br'); ?></div>
        <div class="clear-line"></div>
        <?php } ?>
        <br class="clear" /><br />
        <div class="chenar-info NEW-orange NEW-round8px">Clasificarea pe stele a unitatilor de cazare din program este cea atribuita in mod oficial de Ministerul Turismului din <?php echo $detalii_hotel['tara']; ?> conform standardelor proprii.</div>
      <?php } ?>
      </div>
    
      <?php } ?>
  
    </div>
    <div id="harta">
      <div id="harta-localizare" style="width:100%; height:500px;"></div>
      <div class="text-center smaller-09em grey">Poziționarea hotelurilor pe hartă este cu titlu informativ.</div>
    </div>
    <div id="comentarii">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/hoteluri/reviews.php"); ?>
    </div>
	<?php /*?><?php $oferte=$det->get_oferte($id_hotel, $id_oferta);
    if(sizeof($oferte)>0) { ?>
    <div id="alte-oferte" class="afisare-oferta">
      <table class="tabel-oferte" style="width:100%;">
        <thead>
          <tr>
            <th class="text-center">&nbsp;</th>
            <th class="text-center" style="width:80px;">Nr. nopti</th>
            <th class="text-center" style="width:120px;">Masa</th>
            <th class="text-center" style="width:170px;">Transport</th>
            <th class="text-center" style="width:170px;">Tarif de la</th>
            <th class="text-center" style="width:80px;">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
		  <?php foreach($oferte as $key_of=>$value_of) { $c++;
          if($c%2==1) $class_tr='impar'; else $class_tr='par';
          $link=make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $value_of['denumire_scurta'], $value_of['id_oferta']);
          $tipuri=explode('+',$value_of['poza_tip']);
          if($value_of['taxa_aeroport']=='da') {
              $taxa_aeroport = ' - <span class="red" title="Taxe aeroport INCLUSE">Taxe <strong>INCLUSE</strong></span>';
              $taxa_aeroport2 = ' <img src="/images/dificultate_partie_i.gif" alt="Taxe aeroport INCLUSE" title="Taxe aeroport INCLUSE" style="vertical-align:middle;" width="14" />';
          } else {
              $taxa_aeroport = '';
              $taxa_aeroport2 = '';
          }
		  if($value_of['nr_nopti']>0) {
			  $zile=$value_of['nr_nopti'];
			  if($value_of['nr_nopti']>1) $zile=''.$zile.' nopti';
			  else $zile='Pe noapte';
		  }
          ?>
          <tr onmouseover="this.className='hover';" onmouseout="this.className='<?php echo $class_tr; ?>';" class="<?php echo $class_tr; ?>">
            <td class="text-center"><a href="<?php echo $link; ?>" title="<?php echo $value_of['denumire']; ?>"><?php echo '<strong class="bigger-11em">'.$value_of['denumire_scurta'].'</strong> '.$taxa_aeroport; ?></a></td>
            <td class="text-center"><?php echo $zile; ?></td>
            <td class="text-center"><?php echo $value_of['masa']; ?></td>
            <td class="text-center"><?php echo transport($value_of['denumire_transport']).' '.$taxa_aeroport2; ?></td>
            <td class="text-center"><span class="pret"><?php if(!$value_of['pret_minim']) { $pret=pret_minim_sejur($value_of['id_oferta'], '', '', ''); if($pret['pret']) { echo $pret['pret'].' '; echo moneda($pret['moneda']); } } else { echo $value_of['pret_minim'].' '; echo moneda($value_of['moneda']); } ?></span> <?php echo $value_of['exprimare_pret']; ?></td>
            <td class="text-center last"><a href="<?php echo $link; ?>" rel="nofollow"><img src="/images/button_detalii.png" alt="<?php echo $value_of['denumire']; ?>" /></a></td>
          </tr>
		  <?php } ?>
        </tbody>
      </table>
    </div>
    <?php } ?><?php */?>
  </div>

  <br class="clear" /><br />

</div>

<br class="clear" />
</div>
<br class="clear" />
