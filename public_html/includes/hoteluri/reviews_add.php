<?php
if(isset($_POST['submit-comment'])) {
	if($_POST['rev_prenume']=='' or $_POST['rev_nume']=='' or $_POST['rev_email']=='') {
		echo '<script>alert("Nu ați completat toate câmpurile obligatorii!");</script>';
	} else {
	  if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$_POST['rev_comentarii_plusuri']) and !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$_POST['rev_comentarii_minusuri'])) {
		$note[1] = $_POST['rev_personalul'];
		$note[2] = $_POST['rev_facilitati'];
		$note[3] = $_POST['rev_confort'];
		$note[4] = $_POST['rev_curatenie'];
		$note[5] = $_POST['rev_raport_pret_calitate'];
		$note[6] = $_POST['rev_amplasare'];
		$nota_total = ( $note[1] + $note[2] + $note[3] + $note[4] + $note[5] + $note[6] ) / 6;
		
		/*$sel_users = "SELECT id_useri_fizice FROM useri_fizice WHERE email = '".$_POST['rev_email']."' ";
		$que_users = mysql_query($sel_users) or die(mysql_error());
		$row_users = mysql_fetch_array($que_users);
		if(mysql_num_rows($que_users)==0) {
			$ins_user = "INSERT INTO useri_fizice SET prenume = '".ucwords(strtolower($_POST['rev_prenume']))."', nume = '".ucwords(strtolower($_POST['rev_nume']))."', email = '".strtolower($_POST['rev_email'])."', data_adaugarii = NOW(), session_id = '".session_id()."', tip_adaugare = 'postare review' ";
			$que_user = mysql_query($ins_user) or die(mysql_error());
			@mysql_free_result($que_user);
			
			$sel_iuzar = "SELECT id_useri_fizice FROM useri_fizice WHERE session_id = '".session_id()."' ORDER BY id_useri_fizice DESC ";
			$que_iuzar = mysql_query($sel_iuzar) or die(mysql_error());
			$row_iuzar = mysql_fetch_array($que_iuzar);
			$id_client = $row_iuzar['id_useri_fizice'];
			@mysql_free_result($que_iuzar);
		} else {
			$id_client = $row_users['id_useri_fizice'];
		}*/
		$id_client = insert_user('', $_POST['rev_nume'], $_POST['rev_prenume'], $_POST['rev_email'], '', '', 'postare review');
		
		$ins_review = "INSERT INTO reviews (id_hotel, personalul, facilitati, confort, curatenie, raport_pret_calitate, amplasare, nota_total, comentarii_plusuri, comentarii_minusuri, recomand, tip_client, id_client, anonim, activ, data_adaugarii, ip) VALUES ('".$id_hotel."', '".$note[1]."', '".$note[2]."', '".$note[3]."', '".$note[4]."', '".$note[5]."', '".$note[6]."', '".$nota_total."', '".inserare_frumos($_POST['rev_comentarii_plusuri'])."', '".inserare_frumos($_POST['rev_comentarii_minusuri'])."', '".$_POST['rev_recomand']."', '".$_POST['rev_tip_client']."', '".$id_client."', '".$_POST['rev_anonim']."', 'nu', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$que_review = mysql_query($ins_review) or die(mysql_error());
		@mysql_free_result($que_review);
		
		mail($GLOBALS['email_contact'], 'Review nou', 'A fost adaugat un review nou la '.$_SERVER['REMOTE_ADDR'].'-'.$detalii_hotel['denumire'], 'FROM: '.$GLOBALS['email_contact']);
		
		redirect_php($sitepath.'thank-you-impresii.html?hotel='.fa_link_oferta($detalii_hotel['denumire']).'&loc='.fa_link_oferta($detalii_hotel['localitate']));
	  } else {
		  echo '<script>alert("Va rugam nu introduceti URL-uri in comentarii !");</script>';
	  }
	}
} else {
?>

<div class="black pad20" style="background:#E3EFFF; margin-top:1px;">

<form action="<?php echo substr($sitepath, 0, -1).$_SERVER['REQUEST_URI']; ?>#comentarii" method="post">
<h3 class="blue bigger-15em" style="margin:0; padding:0;">Evaluează șederea ta la <span class="black"><?php echo $detalii_hotel['denumire']; ?></span></h3>
<p class="red italic">Toate câmpurile sunt obligatorii.</p>

<p><span class="bigger-14em">Cum ai evalua hotelul la care ai stat din punct de vedere al următoarelor criterii:</span><br>
<span class="italic">Acordă o notă de la 1 la 10 pentru fiecare serviciu.</span></p>
<br>

<div class="float-left" style="width:430px;">

  <div class="review-line clearfix"><label class="float-left">Personalul:</label>
    <div class="float-right clearfix">
	<?php for($i1=1; $i1<=10; $i1++) {
        echo '<input name="rev_personalul" type="radio" value="'.$i1.'" class="star';
        if($i1==1) echo ' required';
        echo '"';
        if($_POST['rev_personalul']==$i1) echo ' checked="checked"';
        echo '>';
    } ?>
    </div>
  </div>

  <div class="review-line clearfix"><label class="float-left">Facilități:</label>
    <div class="float-right clearfix">
	<?php for($i1=1; $i1<=10; $i1++) {
        echo '<input name="rev_facilitati" type="radio" value="'.$i1.'" class="star';
        if($i1==1) echo ' required';
        echo '"';
        if($_POST['rev_facilitati']==$i1) echo ' checked="checked"';
        echo '>';
    } ?>
    </div>
  </div>
  
  <div class="review-line clearfix"><label class="float-left">Confort:</label>
    <div class="float-right clearfix">
	<?php for($i1=1; $i1<=10; $i1++) {
        echo '<input name="rev_confort" type="radio" value="'.$i1.'" class="star';
        if($i1==1) echo ' required';
        echo '"';
        if($_POST['rev_confort']==$i1) echo ' checked="checked"';
        echo '>';
    } ?>
    </div>
  </div>
  
  <div class="review-line clearfix"><label class="float-left">Curățenie:</label>
    <div class="float-right clearfix">
	<?php for($i1=1; $i1<=10; $i1++) {
        echo '<input name="rev_curatenie" type="radio" value="'.$i1.'" class="star';
        if($i1==1) echo ' required';
        echo '"';
        if($_POST['rev_curatenie']==$i1) echo ' checked="checked"';
        echo '>';
    } ?>
    </div>
  </div>
  
  <div class="review-line clearfix"><label class="float-left">Raport preț/calitate:</label>
    <div class="float-right clearfix">
	<?php for($i1=1; $i1<=10; $i1++) {
        echo '<input name="rev_raport_pret_calitate" type="radio" value="'.$i1.'" class="star';
        if($i1==1) echo ' required';
        echo '"';
        if($_POST['rev_raport_pret_calitate']==$i1) echo ' checked="checked"';
        echo '>';
    } ?>
    </div>
  </div>
  
  <div class="review-line clearfix"><label class="float-left">Amplasare:</label>
    <div class="float-right clearfix">
	<?php for($i1=1; $i1<=10; $i1++) {
        echo '<input name="rev_amplasare" type="radio" value="'.$i1.'" class="star';
        if($i1==1) echo ' required';
        echo '"';
        if($_POST['rev_amplasare']==$i1) echo ' checked="checked"';
        echo '>';
    } ?>
    </div>
  </div>

</div>
<div class="float-right" style="width:500px;">
  
  <div class="clearfix" style="margin-bottom:10px;">
    <label for="comentarii_plusuri" class="float-left bigger-11em bold" style="width:200px;"><img src="/images/review_pos.gif" alt="pozitiv"> Ce ți-a plăcut la hotel?</label>
    <textarea id="comentarii_plusuri" name="rev_comentarii_plusuri" class="float-left" style="width:290px; height:50px;"><?php echo $_POST['rev_comentarii_plusuri']; ?></textarea>
  </div>
  
  <div class="clearfix" style="margin-bottom:10px;">
    <label for="comentarii_minusuri" class="float-left bigger-11em bold" style="width:200px;"><img src="/images/review_neg.gif" alt="negativ"> Ce nu ți-a plăcut la hotel?</label>
    <textarea id="comentarii_minusuri" name="rev_comentarii_minusuri" class="float-left" style="width:290px; height:50px;"><?php echo $_POST['rev_comentarii_minusuri']; ?></textarea>
  </div>
  
  <div class="clearfix" style="margin-bottom:10px;">
    <label class="float-left bigger-11em bold" style="width:320px;">Ai recomanda hotelul și altor persoane?</label>
    <label class="bold"><input type="radio" name="rev_recomand" value="da" <?php if($_POST['rev_recomand']=='da') echo 'checked="checked"'; ?>>DA</label>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <label class="bold"><input type="radio" name="rev_recomand" value="nu" <?php if($_POST['rev_recomand']=='nu') echo 'checked="checked"'; ?>>NU</label>
  </div>
  
</div>

<br class="clear"><br>

<p class="blue bold bigger-14em">Datele tale:</p>

<div class="clearfix" style="margin-bottom:10px;">
  <label for="prenume" class="float-left bigger-11em bold" style="width:165px; padding-top:3px;">Prenumele tău:</label>
  <input type="text" id="prenume" name="rev_prenume" value="<?php echo $_POST['rev_prenume']; ?>" style="width:220px;" data-validation-engine="validate[required]">
</div>

<div class="clearfix" style="margin-bottom:10px;">
  <label for="nume" class="float-left bigger-11em bold" style="width:165px; padding-top:3px;">Numele tău:</label>
  <input type="text" id="nume" name="rev_nume" value="<?php echo $_POST['rev_nume']; ?>" style="width:220px;" data-validation-engine="validate[required]"> <span class="italic">* acest câmp nu va fi afișat pe site</span>
</div>

<div class="clearfix" style="margin-bottom:10px;">
  <label for="email" class="float-left bigger-11em bold" style="width:165px; padding-top:3px;">Email-ul tău:</label>
  <input type="email" id="email" name="rev_email" value="<?php echo $_POST['rev_email']; ?>" style="width:220px;" data-validation-engine="validate[required]"> <span class="italic">* acest câmp nu va fi afișat pe site</span>
</div>

<div class="clearfix" style="margin-bottom:10px;">
  <label for="tip_client" class="float-left bigger-11em bold" style="width:165px; padding-top:3px;">Tip călători:</label>
  <select id="tip_client" name="rev_tip_client" style="width:223px;" data-validation-engine="validate[required]">
    <option disabled selected>- Selectează -</option>
    <option value="Calator singur">Călător singur</option>
    <option value="Cuplu tanar">Cuplu tânăr</option>
    <option value="Cuplu matur">Cuplu matur</option>
    <option value="Familie cu copii mici">Familie cu copii mici</option>
    <option value="Familie cu copii mai mari">Familie cu copii mai mari</option>
  </select>
</div>

<div class="clearfix" style="margin-bottom:10px;">
  <label class="float-left bigger-11em bold" style="width:250px;">Dorești să apari ca "ANONIM"?</label>
  <label class="bold"><input type="radio" name="rev_anonim" value="da" <?php if($_POST['rev_anonim']=='da') echo 'checked="checked"'; ?> data-validation-engine="validate[required]">DA</label>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <label class="bold"><input type="radio" name="rev_anonim" value="nu" <?php if($_POST['rev_anonim']=='nu') echo 'checked="checked"'; ?> data-validation-engine="validate[required]">NU</label>
</div>

<div style="margin:10px 0 0 165px;"><input type="submit" name="submit-comment" value="INREGISTREAZĂ PĂREREA TA" class="button-red"></div>
</form>

</div>
<?php } ?>
