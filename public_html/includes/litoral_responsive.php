<?php

$destinatii = [
	[
		'nume'           => 'Mamaia',
		'tara'           => 'Romania',
		'localitate'     => 'Mamaia',
		'pret_minim'     => '1568',
		'moneda'         => 'LEI',
		'exprimare_pret' => 'sejur/camera',
		'link'           => 'https://www.ocaziituristice.ro/cazare-mamaia/',
		'imagine'        => '/images/localitati/cazare-mamaia.jpg'
	],
    [
		'nume'           => 'Eforie Nord',
		'tara'           => 'Romania',
		'localitate'     => 'Eforie Nord',
		'pret_minim'     => '680',
		'moneda'         => 'LEI',
		'exprimare_pret' => 'sejur/camera',
		'link'           => 'https://www.ocaziituristice.ro/cazare-eforie-nord/',
		'imagine'        => '/images/localitati/cazare-eforie-nord.jpg'
	],
    [
		'nume'           => 'Neptun',
		'tara'           => 'Romania',
		'localitate'     => 'Neptun',
		'pret_minim'     => '1860',
		'moneda'         => 'LEI',
		'exprimare_pret' => 'sejur/camera',
		'link'           => 'https://www.ocaziituristice.ro/cazare-neptun/',
		'imagine'        => '/images/localitati/cazare-neptun.jpg'
	],
    [
		'nume'           => 'Saturn',
		'tara'           => 'Romania',
		'localitate'     => 'Saturn',
		'pret_minim'     => '739',
		'moneda'         => 'LEI',
		'exprimare_pret' => 'sejur/camera',
		'link'           => 'https://www.ocaziituristice.ro/cazare-saturn/',
		'imagine'        => '/images/localitati/cazare-saturn.jpg'
	],
    [
		'nume'           => 'Venus',
		'tara'           => 'Romania',
		'localitate'     => 'Venus',
		'pret_minim'     => '1094',
		'moneda'         => 'LEI',
		'exprimare_pret' => 'sejur/camera',
		'link'           => 'https://www.ocaziituristice.ro/cazare-venus/',
		'imagine'        => '/images/localitati/cazare-venus.jpg'
	],
];
?>
<?php if ( $destinatii ) : ?>
    <div class="homepage-boxes litoral">
        <div class="layout">
            <h3 class="subtitle">Litoral Romania</h3>
            <div class="ofVizitate <?php echo 'columns-' . count( $destinatii ) ?>">
				<?php foreach ( $destinatii as $destinatie ) : ?>
                    <div class="prod">
                        <div class="prod-bg" style="background-image:url(<?php echo $destinatie['imagine'] ?>);"></div>
                        <div class="titlu"><?php echo $destinatie['nume'] ?></div>
                        <div class="prod-overlay">
                            <a href="<?php echo $destinatie['link'] ?>" class="link-blue">
                                <div class="overlay-text">
                                    <h3><?php echo $destinatie['nume'] ?></h3>
                                    <h5>
                                        <?php echo $destinatie['localitate'] ? $destinatie['localitate'] : '' ?>
                                        <?php echo $destinatie['localitate'] && $destinatie['tara'] ? ' / ' : '' ?>
                                        <?php echo $destinatie['tara'] ? $destinatie['tara'] : '' ?>
                                    </h5>
                                    <div>de la</div>
                                    <div class="price">
                                        <span class="red"><?php echo $destinatie['pret_minim'] ?></span> <?php echo $destinatie['moneda'] ?>
                                    </div>
                                    <p><?php echo $destinatie['exprimare_pret'] ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<?php endif; ?>