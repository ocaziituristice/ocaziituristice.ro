<div id="localitate_plecare" style="display:none;"></div>
<ul>
<?php
if($_REQUEST['tip']) {
	$link_tip = '/oferte-'.$_REQUEST['tip'].'/';
	$den_tipF = 'Oferte '.ucwords(desfa_link($_REQUEST['tip']));
} else {
	$link_tip='/sejur-';
}

if($_REQUEST['tari']) {
	$den_taraF = get_den_tara($id_tara);
} elseif($_REQUEST['tara']) {
	$den_taraF = get_den_tara($id_tara);
} elseif($id_tara!=''){$den_taraF = get_den_tara($id_tara);}
$den_zonaF = get_den_zona($id_zona);
$den_loc = get_den_localitate($id_localitate);

echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" title="Ocazii Turistice" itemprop="url"><span itemprop="title">Ocazii Turistice</span></a></li>';

if($_SERVER['PHP_SELF']=="/turism_intern.php") { echo '<li class="sel">Sejur Romania</li>'; }
if($_SERVER['PHP_SELF']=="/turism_extern.php" or $_SERVER['PHP_SELF']=="/oferte-turism-extern/index.php") { echo '<li class="sel">Turism Extern</li>'; }
/*elseif($_REQUEST['tari'] && $_REQUEST['tari']<>'romania' && !$_REQUEST['tip']) { echo '<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></div> › '; }*/
if($_SERVER['PHP_SELF']=="/turism_extern_tematica.php") echo '<li class="sel">'.$den_tipF.'</li>';

if($_SERVER['PHP_SELF']=='/oferte-program-pentru-seniori/index.php') echo '<li class="sel">Program Pentru Seniori</li>';
if($_SERVER['PHP_SELF']=="/oferte-city-break/index.php") { echo '<li class="sel">City Break</li>'; }
if($_SERVER['PHP_SELF']=="/oferte-paste/index.php") { echo '<li class="sel">Paste</li>'; }
if($_SERVER['PHP_SELF']=="/oferte-litoral/index.php") { echo '<li class="sel">Litoral '.date("Y").'</li>'; }
if($_SERVER['PHP_SELF']=="/oferte-craciun/index.php") { echo '<li class="sel">Oferte Craciun</li>'; }
if($_SERVER['PHP_SELF']=="/oferte-revelion/index.php") { echo '<li class="sel">Oferte Revelion</li>'; }
if($_SERVER['PHP_SELF']=="/early-booking/index.php") { echo '<li class="sel">Early Booking</li>'; }
if($_SERVER['PHP_SELF']=="/sejur-romania/index.php") { echo '<li class="sel">Sejur Romania</li>'; }
if($_SERVER['PHP_SELF']=="/sejur-bulgaria/index.php") { echo '<li class="sel">Sejur Bulgaria</li>'; }
if($_SERVER['PHP_SELF']=="/sejur-bulgaria/litoral-bulgaria/index.php") { 
echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/sejur-bulgaria/" title="Sejur Bulgaria" itemprop="url"><span itemprop="title">Sejur Bulgaria</span></a></li>';
echo '<li class="sel">Litoral Bulgaria</li>'; }

if($_SERVER['PHP_SELF']=="/sejur-romania/statiuni-balneare/index.php") {
	echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/sejur-romania/" title="Sejur Romania" itemprop="url"><span itemprop="title">Sejur Romania</span></a></li>';
	echo '<li class="sel">Statiuni Balneare</li>';
}
if($_SERVER['PHP_SELF']=="/sejur-romania/litoral/index.php") {
	echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/sejur-romania/" title="Sejur Romania" itemprop="url"><span itemprop="title">Sejur Romania</span></a></li>';
	echo '<li class="sel">Litoral</li>';
}
if($_SERVER['PHP_SELF']=="/sejur-grecia/index.php") { echo '<li class="sel">Sejur Grecia</li>'; }
if($_SERVER['PHP_SELF']=="/sejur-egipt/index.php") { echo '<li class="sel">Sejur Egipt</li>'; }

if($_SERVER['PHP_SELF']=="/sejur-turcia/index.php") { echo '<li class="sel">Sejur Turcia</li>'; }
if($_SERVER['PHP_SELF']=="/sejur-spania/index.php") { echo '<li class="sel">Sejur Spania</li>'; }
if($_SERVER['PHP_SELF']=="/transport_bulgaria.php") { echo '<li class="sel">Transport Bulgaria</li>'; }
if($_SERVER['PHP_SELF']=="/circuite/index.php") { echo '<li class="sel">Circuite</li>'; }
if($_SERVER['PHP_SELF']=="/rezervarea_mea.php") { echo '<li class="sel">Rezervarea mea</li>'; }

//--- oferte speciale
if($_SERVER['PHP_SELF']=="/oferte-speciale/index.php") { echo '<li class="sel">Oferte Speciale</li>'; }
if($_SERVER['PHP_SELF']=="/zone_new1_ofsp.php") {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-speciale/" title="Oferte Speciale" itemprop="url"><span itemprop="title">Oferte Speciale</span></a></li>';
	echo '<li class="sel">'.$den_zona.'</li>';
}

//--- diverse
if($_SERVER['PHP_SELF']=="/termeni_si_conditii.php" ) { echo '<li class="sel">Termeni si Conditii</li>'; }
if($_SERVER['PHP_SELF']=="/tipuri_de_abonamente.php" ) { echo '<li class="sel">Tipuri de Abonamente</li>'; }
if($_SERVER['PHP_SELF']=="/contact.php" ) { echo '<li class="sel">Contact</li>'; }
if($_SERVER['PHP_SELF']=="/cos_cumparaturi_save.php" ) { echo '<li class="sel">Ofertele mele</li>'; }
if($_SERVER['PHP_SELF']=="/info_cookieuri.php" ) { echo '<li class="sel">Despre cookie-uri</li>'; }
if($_SERVER['PHP_SELF']=="/cupoane_adauga.php" ) { echo '<li class="sel">Cupoane Reducere</li>'; }
if($_SERVER['PHP_SELF']=="/info_despre_noi.php" ) { echo '<li class="sel">Despre noi</li>'; }

//--- last minute
if($_SERVER['PHP_SELF']=="/last_minute.php") {
	echo '<li class="sel">Last Minute</li>';
}
if($_SERVER['PHP_SELF']=="/last_minute_tara.php") {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/last-minute/" title="Oferte last minute" itemprop="url"><span itemprop="title">Last Minute</span></a></li>';
	echo '<li class="sel">'.$den_taraF.'</li>';
}
/*if($_SERVER['PHP_SELF']=="/detalii_last_minute.php") {
	echo '<a href="/last-minute/" title="Oferte Last Minute">Oferte Last Minute</a> › '.ucwords(desfa_link($_REQUEST['denumire']));
}*/

//--- tari
if($_SERVER['PHP_SELF']=="/tara_new1.php" or $_SERVER['PHP_SELF']=="/tara_new1_noindex.php") {
	if($den_taraF!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
	echo '<li class="sel">Sejur '.$den_taraF.'</li>';
}
if($_SERVER['PHP_SELF']=="/tematica_tara.php") {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.'" title="'.$den_tipF.'" itemprop="url"><span itemprop="title">'.$den_tipF.'</span></a></li>';
	echo '<li class="sel">'.$den_taraF.'</li>';
}

//--- zone
if($_SERVER['PHP_SELF']=="/zone_new1.php" or $_SERVER['PHP_SELF']=="/zone_new1_noindex.php") {
	//if($den_taraF!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($den_taraF).'/" title="Sejur '.$den_taraF.'" itemprop="url"><span itemprop="title">Sejur '.$den_taraF.'</span></a></li>';
	echo '<li class="sel">'.$den_zonaF.'</li>';
}
if($_SERVER['PHP_SELF']=="/tematica_zona.php") {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.'" title="'.$den_tipF.'" itemprop="url"><span itemprop="title">'.$den_tipF.'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($den_taraF).'/" title="'.$den_taraF.'" itemprop="url"><span itemprop="title">'.$den_taraF.'</span></a></li>';
	echo '<li class="sel">'.$den_zonaF.'</li>';
}

//--- localitati
if($_SERVER['PHP_SELF']=="/localitate_new1.php" or $_SERVER['PHP_SELF']=="/localitate_new1_noindex.php") {
	//if($den_taraF!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($den_taraF).'/" title="Sejur '.$den_taraF.'" itemprop="url"><span itemprop="title">Sejur '.$den_taraF.'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($den_taraF).'/'.fa_link($den_zonaF).'/" title="'.$den_zonaF.'" itemprop="url"><span itemprop="title">'.$den_zonaF.'</span></a></li>';
	echo '<li class="sel">'.$den_loc.'</li>';
}
if($_SERVER['PHP_SELF']=="/tematica_localitate.php") {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.'" title="'.$den_tipF.'" itemprop="url"><span itemprop="title">'.$den_tipF.'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($den_taraF).'/" title="'.$den_taraF.'" itemprop="url"><span itemprop="title">'.$den_taraF.'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($den_taraF).'/'.fa_link($den_zonaF).'/" title="'.$den_zonaF.'" itemprop="url"><span itemprop="title">'.$den_zonaF.'</span></a></li>';
	echo '<li class="sel">'.$den_loc.'</li>';
}

//--- detalii oferta
if($_SERVER['PHP_SELF']=="/detalii_oferta_new.php" ) {
	//if($detalii_hotel['tara']!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
//if(!$err_logare_admin) { echo '<pre>';print_r($detalii['tip_oferta']);echo '</pre>';}
	
if(sizeof($detalii['tip_oferta'])=='1' and ($detalii['tip_oferta']['0']==33)){$denumire_tip_oferta='Revelion';$link_tip='/oferte-revelion/';	

	
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/" title="'.$denumire_tip_oferta.' '.$detalii_hotel['tara'].'" itemprop="url"><span itemprop="title">'.$denumire_tip_oferta.' '.$detalii_hotel['tara'].'</span></a></li>';
	
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" title="'.$denumire_tip_oferta.' '.$detalii_hotel['zona'].'" itemprop="url"><span itemprop="title">'.$denumire_tip_oferta.' '.$detalii_hotel['zona'].'</span></a></li>';
	
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/'.fa_link($detalii_hotel['localitate']).'/" title="'.$denumire_tip_oferta.' '.$detalii_hotel['localitate'].'" itemprop="url"><span itemprop="title">'.$denumire_tip_oferta.' '.$detalii_hotel['localitate'].'</span></a></li>';
	
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
	<a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'" itemprop="url" title="'.$detalii_hotel['denumire'].'"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
	
	echo '<li class="sel">'.$detalii['denumire_scurta'].'</li>'; 
	} else 
	{
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/" title="Sejur '.$detalii_hotel['tara'].'" itemprop="url"><span itemprop="title">Sejur '.$detalii_hotel['tara'].'</span></a></li>';
	
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" title="'.$detalii_hotel['zona'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['zona'].'</span></a></li>';
	
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.'/cazare-'.fa_link($detalii_hotel['localitate']).'/" title="Cazare '.$detalii_hotel['localitate'].'" itemprop="url"><span itemprop="title">Cazare '.$detalii_hotel['localitate'].'</span></a></li>';
	
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
	<a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'" itemprop="url" title="'.$detalii_hotel['denumire'].'"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
	
	echo '<li class="sel">'.$detalii['denumire_scurta'].'</li>'; 
		}
}







//--- rezervare oferta
if($_SERVER['PHP_SELF']=="/rezervare_sejur_new2.php" ) {
	if($detalii_hotel['tip_unitate']=='Circuit') {
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/" title="Circuite" itemprop="url"><span itemprop="title">Circuite</span></a></li>';
		/*echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/'.fa_link_circuit($detalii_hotel['nume_continent']).'/" title="Circuit '.$detalii_hotel['nume_continent'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['nume_continent'].'</span></a></li>';*/
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_circuit($detalii_hotel['denumire'], $id_oferta).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
		echo '<li class="sel">Rezervare</li>';
	} else {
		//if($detalii_hotel['tara']!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
		//echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/" title="Sejur '.$detalii_hotel['tara'].'" itemprop="url"><span itemprop="title">Sejur '.$detalii_hotel['tara'].'</span></a></li>';
		//echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" title="'.$detalii_hotel['zona'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['zona'].'</span></a></li>';
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/cazare-'.fa_link($detalii_hotel['localitate']).'/" title="Cazare '.$detalii_hotel['localitate'].'" itemprop="url"><span itemprop="title">Cazare'.$detalii_hotel['localitate'].'</span></a></li>';
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
		//echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $detalii['denumire_scurta'], $id_oferta).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii['denumire_scurta'].'</span></a></li>';
		echo '<li class="sel">Rezervare</li>';
	}
}

//--- rezervare succes oferta
if($_SERVER['PHP_SELF']=="/rezervare_sejur_succes.php" ) {
	//if($den_taraF!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/" title="Sejur '.$detalii_hotel['tara'].'" itemprop="url"><span itemprop="title">Sejur '.$detalii_hotel['tara'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" title="'.$detalii_hotel['zona'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['zona'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/'.fa_link($detalii_hotel['localitate']).'/" title="'.$detalii_hotel['localitate'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['localitate'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
	//echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $detalii['denumire_scurta'], $id_oferta).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii['denumire_scurta'].'</span></a></li>';
	echo '<li class="sel">Rezervare finalizata</li>';
}

//--- rezervare succes circuit
if($_SERVER['PHP_SELF']=="/rezervare_circuit_succes.php" ) {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/" title="Circuite" itemprop="url"><span itemprop="title">Circuite</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_circuit($detalii_hotel['denumire'], $id_oferta).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
	echo '<li class="sel">Rezervare finalizata</li>';
}

//--- cerere succes oferta
if($_SERVER['PHP_SELF']=="/cerere_sejur_succes.php" ) {
	//if($den_taraF!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/" title="Sejur '.$detalii_hotel['tara'].'" itemprop="url"><span itemprop="title">Sejur '.$detalii_hotel['tara'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" title="'.$detalii_hotel['zona'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['zona'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/'.fa_link($detalii_hotel['localitate']).'/" title="'.$detalii_hotel['localitate'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['localitate'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
	if($id_oferta!=0) echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], $detalii['denumire_scurta'], $id_oferta).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii['denumire_scurta'].'</span></a></li>';
	echo '<li class="sel">Cerere finalizata</li>';
}

//--- cerere succes circuit
if($_SERVER['PHP_SELF']=="/cerere_circuit_succes.php" ) {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/" title="Circuite" itemprop="url"><span itemprop="title">Circuite</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.make_link_circuit($detalii_hotel['denumire'], $id_oferta).'" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
	echo '<li class="sel">Cerere finalizata</li>';
}

//--- detalii hotel
if($_SERVER['PHP_SELF']=="/detalii_hotel_new.php") {
	//if($den_taraF!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/" title="Sejur '.$detalii_hotel['tara'].'" itemprop="url"><span itemprop="title">Sejur '.$detalii_hotel['tara'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" title="'.$detalii_hotel['zona'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['zona'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.'/'.'cazare-'.fa_link($detalii_hotel['localitate']).'/" title="Cazare '.$detalii_hotel['localitate'].'" itemprop="url"><span itemprop="title">'.'Cazare '.$detalii_hotel['localitate'].'</span></a></li>';
	echo '<li class="sel">'.$detalii_hotel['denumire'].'</li>';
}

//--- review hotel
if($_SERVER['PHP_SELF']=="/review_thank_you.php") {
	//if($den_taraF!='Romania') echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/oferte-turism-extern/" title="Turism Extern" itemprop="url"><span itemprop="title">Turism Extern</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/" title="Sejur '.$detalii_hotel['tara'].'" itemprop="url"><span itemprop="title">Sejur '.$detalii_hotel['tara'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" title="'.$detalii_hotel['zona'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['zona'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$link_tip.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/'.fa_link($detalii_hotel['localitate']).'/" title="'.$detalii_hotel['localitate'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['localitate'].'</span></a></li>';
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/hoteluri-'.fa_link($detalii_hotel['localitate']).'/'.fa_link_oferta($detalii_hotel['denumire']).'/" title="'.$detalii_hotel['denumire'].'" itemprop="url"><span itemprop="title">'.$detalii_hotel['denumire'].'</span></a></li>';
	echo '<li class="sel">Review-uri / Parerile Turistilor</li>';
}

//--- circuite
if($_SERVER['PHP_SELF']=="/circuite.php") {
	echo '<li class="sel">Circuite</li>';
}
if($_SERVER['PHP_SELF']=="/circuite_continent.php") {
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/" title="Circuite" itemprop="url"><span itemprop="title">Circuite</span></a></li>';
	echo '<li class="sel">'.ucwords(desfa_link($_REQUEST['continent'])).'<li>';
}

if($_SERVER['PHP_SELF']=="/circuite_tara.php" or $_SERVER['PHP_SELF']=="/circuite_tara_noindex.php") {
	if(strtolower($tip)=='pelerinaje') {
echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite-pelerinaje/" title="Pelerinaje" itemprop="url"><span itemprop="title">Pelerinaje</span></a></li>';
	echo '<li class="sel">'.ucwords($tara).'<li>';		
		}
		else
{	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/" title="Circuite" itemprop="url"><span itemprop="title">Circuite</span></a></li>';
	echo '<li class="sel">'.ucwords($tara).'<li>';
}
}

if($_SERVER['PHP_SELF']=="/detalii_circuit.php") {
	if($detalii['tip_oferta_denumire']['0']=='pelerinaje') 
	{ echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/" title="Circuite" itemprop="url"><span itemprop="title">Pelerinaje</span></a></li>';
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite/'.fa_link(str_replace(',','',$tari_principale)).'/" title="Pelerinaje '.$tari_principale.'" itemprop="url"><span itemprop="title">Pelerinaje '.str_replace(',','',$tari_principale).'</span></a></li>';

	echo '<li class="sel">'.$detalii_hotel['denumire'].'<li>';
		
		
		} else
	
	{ echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite-pelerinaje/" title="Pelerinaje" itemprop="url"><span itemprop="title">Pelerinaje</span></a></li>';
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/circuite-pelerinaje/'.fa_link(str_replace(',','',$tari_principale)).'/" title="Pelerinaje '.$tari_principale.'" itemprop="url"><span itemprop="title">Pelerinaje '.str_replace(',','',$tari_principale).'</span></a></li>';

	echo '<li class="sel">'.$detalii_hotel['denumire'].'<li>';
	
} }
//if(!$err_logare_admin) { echo $sResponse; }


//--- articles
if($_SERVER['PHP_SELF']=="/article_area.php") {
	echo '<li class="sel">Ghid turistic '.$den_destinatie.'</li>';
}
if($_SERVER['PHP_SELF']=="/article.php") {
	if($id_tara==1)
	{
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/ghid-turistic-'.fa_link($den_destinatie).'/" title="Ghid Turistic '.$den_destinatie.'" itemprop="url"><span itemprop="title">Ghid Turistic '.$den_destinatie.'</span></a></li>';
	echo '<li class="sel">'.$denumire .'</li>';	
	}
	 else
	{
	echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/ghid-turistic-'.fa_link(get_den_tara($id_tara)).'/" title="Ghid Turistic '.get_den_tara($id_tara).'" itemprop="url"><span itemprop="title">Ghid Turistic '.get_den_tara($id_tara).'</span></a></li>';
	echo '<li class="sel">'.$den_destinatie.'</li>';
	}
}

//--- articles
if($_SERVER['PHP_SELF']=="/vremea.php") {
	echo '<li class="sel">Vremea in '.$den_destinatie.'</li>';
}
?>
</ul>