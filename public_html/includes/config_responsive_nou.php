<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
// ======================================= //

// =============== DEFINES =============== //

// ======================================= //

define("CACHE", 3);

define("PATH_IMAGES", "/images/");

define("PATH_CSS", "/css_responsive/");

define("PATH_JS", "/js_responsive/");



// ========================================= //

// =============== VARIABLES =============== //

// ========================================= //

$file_css = PATH_CSS."test_1.css"."?ocz=".CACHE;

// $file_js = PATH_JS."ocaziituristice.min.js"."?ocz=".CACHE;

$file_js = PATH_JS."ocaziituristice.js"."?ocz=".CACHE;







?>