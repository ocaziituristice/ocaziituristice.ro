<?php //include('concurs/banner_concurs.php'); ?>
<div class="textTopIndex">
<h1 class="blue">Agentii de Turism, portal de turism Ocazii Turistice</h1>
<img src="<?php echo $imgpath; ?>/agentii_de_turism.jpg" width="160" height="130" alt="Agentii de Turism" style="float:left; margin-right:10px;" />
<p>care cuprinde oferte turistice pentru toate buzunarele de la <strong>agentii de turism romanesti</strong>, adevarate <strong>ocazii turistice</strong>. Daca stiti din timp unde vreti sa va petreceti sejururi, sau circuite, pe portal gasiti <strong>oferte early booking</strong>. <br/>In cazul in care va hotarati in ultima clipa, gasiti si <strong>oferte last minute</strong>. <a href="http://www.ocaziituristice.ro/hoteluri/" class="link">Hoteluri</a>, pensiuni cu oferta completa, cazari romanesti in orase, zone sau statiuni preferate asteapta sa le descoperiti.</p>
<p>Vreti sa aflati mai multe despre destinatia dumneavoastra? Stiri din turism, impresii despre hoteluri, ghiduri de calatorie va stau la dispozitie pe ocaziituristice.</p>
</div>

<div class="spacer"><img src="<?php echo $imgpath; ?>/spacer.gif" width="1" height="6" alt="" /></div>

<?php //include('seo_blogline.php'); ?>

<?php

$link="/";

?>

<div class="tematiciIndex">
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
      <td align="center" valign="top">
        <a href="/oferte-revelion/" title="Oferte Revelion 2012" class="title">Oferte Revelion 2012</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-revelion.jpg" alt="Revelion 2012" width="200" height="130" onclick="location.href='/oferte-revelion/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td>
      <td align="center" valign="top">
        <a href="/oferte-craciun/" title="Oferte Craciun" class="title">Oferte Craciun</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-craciun.jpg" alt="Craciun" width="200" height="130" onclick="location.href='/oferte-craciun/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td>
      <td align="center" valign="top">
        <a href="/oferte-sejur-ski/" title="Oferte Sejur Ski" class="title">Oferte Sejur Ski</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-ski.jpg" alt="Sejur Ski" width="200" height="130" onclick="location.href='/oferte-sejur-ski/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td>
      <?php /*?><td align="center" valign="top">
        <a href="/oferte-valentines-day/" title="Oferte Valentines Day" class="title">Oferte Valentines Day</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-valentines-day.jpg" alt="Valentines Day" width="200" height="130" onclick="location.href='/oferte-valentines-day/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td><?php */?>
      <?php /*?><td align="center" valign="top">
        <a href="/oferte-paste/" title="Oferte Paste" class="title">Oferte Paste</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-paste.jpg" alt="Paste" width="200" height="130" onclick="location.href='/oferte-paste/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td><?php */?>
      <?php /*?><td align="center" valign="top">
        <a href="/oferte-vara/" title="Oferte Vara" class="title">Oferte Vara</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-vara.jpg" alt="Vara" width="200" height="130" onclick="location.href='/oferte-vara/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td><?php */?>
      <?php /*?><td align="center" valign="top">
        <a href="/oferte-litoral/" title="Oferte Litoral" class="title">Oferte Litoral</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-litoral.jpg" alt="Oferte Litoral" width="200" height="130" onclick="location.href='/oferte-litoral/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td><?php */?>
      <?php /*?><td align="center" valign="top">
        <a href="/oferte-romantic/" title="Oferte Romantic" class="title">Oferte Romantic</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-romantic.jpg" alt="Oferte Romantic" width="200" height="130" onclick="location.href='/oferte-romantic/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td><?php */?>
    </tr>
    <tr>
      <td align="center" valign="top">
        <a href="/oferte-exotic/" title="Oferte Exotic" class="title">Oferte Exotic</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-exotic.jpg" alt="Exotic" width="200" height="130" onclick="location.href='/oferte-exotic/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td>
      <td align="center" valign="top">
        <a href="/oferte-city-break/" title="Oferte City Break" class="title">City Break</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-city-break.jpg" alt="Oferte City Break" width="200" height="130" onclick="location.href='/oferte-city-break/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td>
      <td align="center" valign="top">
        <a href="/oferte-luna-de-miere/" title="Luna de Miere" class="title">Luna de Miere</a>
        <img src="<?php echo $imgpath; ?>/tematici/tematica-luna-de-miere.jpg" alt="Luna de Miere" width="200" height="130" onclick="location.href='/oferte-luna-de-miere/';" class="normal" onmouseout="this.className='normal';" onmouseover="this.className='hover';" />
      </td>
    </tr>
  </table>
</div>

<div class="spacer"><img src="<?php echo $imgpath; ?>/spacer.gif" width="1" height="6" alt="" /></div>

<div class="chenarIndexBlue">
        
  <h2 class="blue">Oferte sejur, cazare de la agentii de turism</h2>
  <div class="spacer"></div>
<?php
$template="".$_SERVER['DOCUMENT_ROOT']."/templates/sejur_index.tpl";

$tipuri="prima_pagina_ocazii";

$afisare=new AFISARE_SEJUR();
$afisare->setOfertaSpeciala($template);
$afisare->set_oferte_pagina(4);
$afisare->setTipOferta($tipuri);
$afisare->setRandom('1');
$afisare->fara_paginare();
unset($afisare);
?>
  <div class="spacer"></div>
  <?php /*?><div align="right" style="margin:3px 0 1px 0;"><a href="/oferte_litoral/" title="oferte litoral 2010" class="more">&raquo; afiseaza toate ofertele de litoral</a></div><?php */?>
</div>

<div class="spacer"><img src="<?php echo $imgpath; ?>/spacer.gif" width="1" height="6" alt="" /></div>

<div class="rssBlog" style="height:300px;">
  <?php include('readrss.php'); ?>
</div>
<div class="chenarIndexRed" style="float:left; width:250px; height:125px; margin-bottom:5px;">
  <h3>&raquo;<a href="/oferte-sejur-ski/" title="Oferte Sejur Ski">Oferte Sejur Ski</a></h3>
  <div class="indexOneColumn">
    <img src="<?php echo $imgpath; ?>/flags/austria.small.gif" alt="Austria" class="flag" width="32" height="20" /> <a href="/oferte-sejur-ski/austria/" title="Sejur Ski Austria">Sejur Ski Austria</a>
  </div>
  <div class="indexOneColumn">
    <img src="<?php echo $imgpath; ?>/flags/bulgaria.small.gif" alt="Bulgaria" class="flag" width="32" height="20" /> <a href="/oferte-sejur-ski/bulgaria/" title="Sejur Ski Bulgaria">Sejur Ski Bulgaria</a>
  </div>
  <div class="indexOneColumn">
    <img src="<?php echo $imgpath; ?>/flags/finland.small.gif" alt="Finlanda" class="flag" width="32" height="20" /> <a href="/oferte-sejur-ski/finlanda/" title="Sejur Ski Finlanda">Sejur Ski Finlanda</a>
  </div>
</div>

<div class="chenarIndexGreen" style="float:left; width:250px; height:160px;">
  <h3>&raquo;<a href="/program-pentru-seniori/" title="Program pentru Seniori">Program pentru Seniori</a></h3>
  <div class="indexOneColumn">
    <img src="<?php echo $imgpath; ?>/flags/spain.small.gif" alt="Spania" class="flag" width="32" height="20" /> <a href="/oferte-program-pentru-seniori/spania/" title="Program pentru Seniori Spania">Seniori Spania</a>
  </div>
  <div class="indexOneColumn">
    <img src="<?php echo $imgpath; ?>/flags/israel.small.gif" alt="Israel" class="flag" width="32" height="20" /> <a href="/oferte-program-pentru-seniori/israel/" title="Program pentru Seniori Israel">Seniori Israel</a>
  </div>
  <div class="indexOneColumn">
    <img src="<?php echo $imgpath; ?>/flags/italy.small.gif" alt="Italia" class="flag" width="32" height="20" /> <a href="/oferte-program-pentru-seniori/italia/" title="Program pentru Seniori Italia">Seniori Italia</a>
  </div>
  <div class="indexOneColumn">
    <img src="<?php echo $imgpath; ?>/flags/bulgaria.small.gif" alt="Bulgaria" class="flag" width="32" height="20" /> <a href="/oferte-program-pentru-seniori/bulgaria/" title="Program pentru Seniori Bulgaria">Seniori Bulgaria</a>
  </div>
</div>

<div class="spacer"><img src="<?php echo $imgpath; ?>/spacer.gif" width="1" height="6" alt="" /></div>

<div class="chenarIndexGreen">
  <h2 class="blue">Circuite de la agentii de turism</h2>
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
      <td align="center" valign="top">
        <div class="titluCircuite"><a href="/circuite/europa/" title="Circuite Europa">Circuit Europa</a></div>
        <img src="<?php echo $imgpath; ?>/map_europe.gif" width="150" height="150" alt="harta Europa" />
      </td>
      <td align="center" valign="top">
        <div class="titluCircuite"><a href="/circuite/america/" title="Circuite America">Circuit America</a></div>
        <img src="<?php echo $imgpath; ?>/map_america.gif" width="150" height="150" alt="harta America" />
      </td>
      <td align="center" valign="top">
        <div class="titluCircuite"><a href="/circuite/asia/" title="Circuite Asia">Circuit Asia</a></div>
        <img src="<?php echo $imgpath; ?>/map_asia.gif" width="150" height="150" alt="harta Asia" />
      </td>
      <td align="center" valign="top">
        <div class="titluCircuite"><a href="/circuite/africa/" title="Circuit Africa">Circuit Africa</a></div>
        <img src="<?php echo $imgpath; ?>/map_africa.gif" width="150" height="150" alt="harta Africa" />
      </td>
    </tr>
  </table>

  <div align="right" style="margin:3px 0 1px 0;">&raquo; afiseaza toate <a href="/circuite/" title="Circuite" >circuitele</a> disponibile</div>
</div>

<div class="spacer"><img src="<?php echo $imgpath; ?>/spacer.gif" width="1" height="6" alt="" /></div>

<div class="chenarIndexBlue seoLinkuri">
  <?php /*?><h2 class="blue">Oferte last minute</h2>
  <div class="spacer"></div>
<?php
$template="".$_SERVER['DOCUMENT_ROOT']."/templates/last_minute_index.tpl";


$afisare=new AFISARE_LAST_MINUTE();
$afisare->setOfertaSpeciala($template);
$afisare->set_oferte_pagina('4');
$afisare->setRandom('1');
$afisare->afisare(1);
unset($afisare);
?>
  <div class="spacer"></div>
  <div align="right" style="margin:3px 0 1px 0;"><a href="/last_minute/" title="Oferte last minute" class="more">&raquo; afiseaza toate ofertele last minute</a></div><?php */?>
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
      <td colspan="2" align="left" valign="top"><h2 class="blue" style="font-size:18px;">Oferte Revelion Turism Extern</h2></td>
      <td align="left" valign="top"><h3 class="blue" style="font-size:18px;">Oferte Revelion Romania</h3></td>
    </tr>
    <tr>
      <td align="left" valign="top">
        <ul>
          <li><a href="<?php echo $path; ?>/oferte-revelion/bulgaria/" title="Oferte Revelion Bulgaria">Revelion Bulgaria</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/austria/" title="Oferte Revelion Austria">Revelion Austria</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/turcia/" title="Oferte Revelion Turcia">Revelion Turcia</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/egipt/" title="Oferte Revelion Egipt">Revelion Egipt</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/franta/" title="Oferte Revelion Franta">Revelion Franta</a></li>
        </ul>
      </td>
      <td align="left" valign="top">
        <ul>
          <li><a href="<?php echo $path; ?>/oferte-revelion/emiratele-arabe-unite/" title="Oferte Revelion Emiratele Arabe Unite">Revelion Emiratele Arabe Unite</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/spania/" title="Oferte Revelion Spania">Revelion Spania</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/cipru/" title="Oferte Revelion Cipru">Revelion Cipru</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/italia/" title="Oferte Revelion Italia">Revelion Italia</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/grecia/" title="Oferte Revelion Grecia">Revelion Grecia</a></li>
        </ul>
      </td>
      <td align="left" valign="top" width="260">
        <ul>
          <li><a href="<?php echo $path; ?>/oferte-revelion/romania/valea-prahovei/sinaia/" title="Oferte Revelion Sinaia">Revelion Sinaia</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/romania/valea-prahovei/busteni/" title="Oferte Revelion Busteni">Revelion Busteni</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/romania/valea-prahovei/predeal/" title="Oferte Revelion Predeal">Revelion Predeal</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/romania/valea-prahovei/azuga/" title="Oferte Revelion Azuga">Revelion Azuga</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/romania/brasov_d_poiana-brasov/poiana-brasov/" title="Oferte Revelion Poiana Brasov">Revelion Poiana Brasov</a></li>
          <li><a href="<?php echo $path; ?>/oferte-revelion/romania/bran_d_moeciu_d_rucar/bran/" title="Oferte Revelion Bran">Revelion Bran</a></li>
        </ul>
      </td>
    </tr>
  </table>
</div>
<br class="clear" />
<img src="<?php echo $imgpath; ?>/banner/banner_maxholidays.jpg" alt="" style="margin-top:6px;" />
<br class="clear" /><br />
