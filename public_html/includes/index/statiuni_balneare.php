<?php
$sel_col1 = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND zone.id_zona = '1497'
AND oferte.nr_nopti > '2'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_col1 = mysql_query($sel_col1) or die(mysql_error());

if(mysql_num_rows($que_col1)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Statiuni Balneare Romania</h3>
          <a href="/sejur-romania/statiuni-balneare/" rel="nofollow noindex"><img src="/images/index/banner_statiuni-balneare-index_25-06-2013.jpg" alt="Statiuni Balneare Romania" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_col1 = mysql_fetch_array($que_col1)) {
        if($row_col1['oferte_moneda']=='EURO') $min_price_col1 = final_price_euro($row_col1['min_price']); else $min_price_col1 = $row_col1['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/cazare-'.fa_link($row_col1['den_localitate']).'/'; ?>" title="Cazare <?php echo $row_col1['den_localitate']; ?> - Tratament <?php echo $row_col1['den_localitate']; ?>"><?php echo $row_col1['den_localitate']; ?></a>
              <span><span class="value"><?php echo $row_col1['numar']; ?></span> oferte</span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-romania/statiuni-balneare/" title="Oferte Statiuni Balneare" class="link-black">Statiuni Balneare Romania</a></div>
        </div></div>
<?php } ?>