<?php
$sel_individual_grecia = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND id_transport = '1'
AND tari.denumire = 'Grecia'
GROUP BY zone.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_individual_grecia = mysql_query($sel_individual_grecia) or die(mysql_error());

if(mysql_num_rows($que_individual_grecia)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Cazare Grecia</h3>
          <a href="/sejururi/9/?optiuni=da&transport=fara-transport" rel="nofollow noindex"><img src="/images/index/banner_individual_grecia_index.jpg" alt="Grecia cazare" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_individual_grecia = mysql_fetch_array($que_individual_grecia)) {
        if($row_individual_grecia['oferte_moneda']=='EURO') $min_price_individual_grecia = final_price_euro($row_individual_grecia['min_price']); else $min_price_individual_grecia = $row_individual_grecia['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/sejur-grecia/'.fa_link($row_individual_grecia['den_zona']).'/'; ?>" title="Cazare <?php echo $row_individual_grecia['den_zona']; ?>"><?php echo $row_individual_grecia['den_zona']; ?></a>
              <span class="value"><?php echo $row_individual_grecia['numar']; ?></span> hoteluri
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-grecia/?optiuni=da&transport=fara-transport" title="Cazare Grecia" class="link-black">Cazare Grecia</a></div>
        </div></div>
<?php } ?>