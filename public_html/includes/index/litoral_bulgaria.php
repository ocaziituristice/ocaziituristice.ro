<?php
$sel_litoral_bulgaria = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND tari.denumire = 'Bulgaria'
AND zone.denumire = 'Litoral'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_litoral_bulgaria = mysql_query($sel_litoral_bulgaria) or die(mysql_error());

if(mysql_num_rows($que_litoral_bulgaria)>0) {
?>
        <div class="chenar chn-color-blue NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Litoral Bulgaria</h3>
          <a href="/sejur-bulgaria/litoral/" title="Sejur Bulgaria"><img src="/images/index/banner_litoral_bulgaria_index.jpg" alt="Litoral Bulgaria" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_litoral_bulgaria= mysql_fetch_array($que_litoral_bulgaria)) {
        if($row_litoral_bulgaria['oferte_moneda']=='EURO') $min_price_litoral_bulgaria = final_price_euro($row_litoral_bulgaria['min_price']); else $min_price_litoral_bulgaria = $row_litoral_bulgaria['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/cazare-'.fa_link($row_litoral_bulgaria['den_localitate']).'/"'; ?>" title="Cazare <?php echo $row_litoral_bulgaria['den_localitate']; ?>"><?php echo $row_litoral_bulgaria['den_localitate']; ?></a>
              <span class="value"><?php echo $row_litoral_bulgaria['numar']; ?> </span> hoteluri 
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-bulgaria/litoral/" title="Sejur Bulgaria Litoral" class="link-black">Sejur Bulgaria Litoral</a></div>
        </div></div>
<?php } ?>