<?php
$sel_autocar_grecia = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND id_transport = '2'
AND tari.denumire = 'Grecia'
GROUP BY zone.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_autocar_grecia = mysql_query($sel_autocar_grecia) or die(mysql_error());

if(mysql_num_rows($que_autocar_grecia)>0) {
?>
        <div class="chenar chn-color-blue NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Autocar Grecia</h3>
          <a href="/sejururi/9/?optiuni=da&transport=autocar" rel="nofollow noindex"><img src="/images/index/banner_autocar_grecia_index.jpg" alt="Grecia autocar" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_autocar_grecia = mysql_fetch_array($que_autocar_grecia)) {
        if($row_autocar_grecia['oferte_moneda']=='EURO') $min_price_autocar_grecia = final_price_euro($row_autocar_grecia['min_price']); else $min_price_autocar_grecia = $row_autocar_grecia['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/sejur-grecia/'.fa_link($row_autocar_grecia['den_zona']).'/?optiuni=da&transport=autocar'; ?>" title="Sejur <?php echo $row_autocar_grecia['den_zona']; ?> autocar"><?php echo $row_autocar_grecia['den_zona']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_autocar_grecia; ?></span> <?php echo moneda(substr($row_autocar_grecia['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-grecia/?optiuni=da&transport=autocar" title="Oferte autocar Grecia" class="link-black">Grecia autocar</a></div>
        </div></div>
<?php } ?>