<?php
$link_lm = '/last-minute/';

$sel_lm = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join continente ON continente.id_continent = tari.id_continent
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND oferte.last_minute = 'da'
AND oferte.nr_nopti > '2'
GROUP BY tari.denumire
ORDER BY numar DESC, tari.denumire ASC
LIMIT 0,6 ";
$que_lm = mysql_query($sel_lm) or die(mysql_error());

if(mysql_num_rows($que_lm)>0) {
?>
        <div class="chenar chn-color-orange NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Last Minute</h3>
          <a href="<?php echo $link_lm; ?>" rel="nofollow"><img src="/images/index/banner_last-minute-index_10-09-2013.jpg" alt="Last Minute" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_lm = mysql_fetch_array($que_lm)) {
		if($row_lm['oferte_moneda']=='EURO') $min_price_lm = final_price_euro($row_lm['min_price']); else $min_price_lm = $row_lm['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_lm.fa_link($row_lm['den_tara']).'/'; ?>" title="Last Minute <?php echo $row_lm['den_tara']; ?>"><?php echo $row_lm['den_tara']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_lm; ?></span> <?php echo moneda(substr($row_lm['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
          </div>
		  <div class="text-right"><a href="<?php echo $link_lm; ?>" rel="nofollow" class="link-black">vezi toate ofertele Last Minute</a></div>
        </div></div>
<?php } ?>
        