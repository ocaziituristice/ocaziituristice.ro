<?php
$link_seniori = "/oferte-program-pentru-seniori/";

$sel_seniori = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND oferta_sejur_tip.id_tip_oferta IN ('66','72','74','75')
AND oferte.nr_nopti > '2'
GROUP BY tari.denumire
ORDER BY numar DESC, tari.denumire ASC
LIMIT 0,6 ";
$que_seniori = mysql_query($sel_seniori) or die(mysql_error());

if(mysql_num_rows($que_seniori)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Programe Seniori</h3>
          <a href="/oferte-program-pentru-seniori/"><img src="/images/index/banner_seniori-index_04-01-2012.jpg" alt="Programe Seniori" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_seniori = mysql_fetch_array($que_seniori)) {
        if($row_seniori['oferte_moneda']=='EURO') $min_price_seniori = final_price_euro($row_seniori['min_price']); else $min_price_seniori = $row_seniori['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_seniori.fa_link($row_seniori['den_tara']).'/'; ?>" title="Program Seniori <?php echo $row_seniori['den_tara']; ?>"><?php echo $row_seniori['den_tara']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_seniori; ?></span> <?php echo moneda(substr($row_seniori['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
          </div>
		  <div class="text-right"><a href="/oferte-program-pentru-seniori/" title="Oferte Seniori" class="link-black">Oferte Programe Seniori</a></div>
        </div></div>
<?php } ?>