<?php
$sel_sejur_grecia = "SELECT
tari.denumire AS den_tara,
count(DISTINCT(hoteluri.id_hotel)) as numar_hoteluri,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,

COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND tari.denumire = 'Grecia'
GROUP BY zone.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_sejur_grecia = mysql_query($sel_sejur_grecia) or die(mysql_error());

if(mysql_num_rows($que_sejur_grecia)>0) {
?>
        <div class="chenar chn-color-orange NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Sejur Grecia</h3>
          <a href="/sejur-grecia/" title="Sejur Grecia" ><img src="/images/index/banner_sejur_grecia_index.jpg" alt="Sejur Grecia" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_sejur_grecia = mysql_fetch_array($que_sejur_grecia)) {
        if($row_sejur_grecia['oferte_moneda']=='EURO') $min_price_sejur_grecia = final_price_euro($row_sejur_grecia['min_price']); else $min_price_sejur_grecia = $row_sejur_grecia['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/sejur-grecia/'.fa_link($row_sejur_grecia['den_zona']).'/'; ?>" title="Sejur <?php echo $row_sejur_grecia['den_zona']; ?> avion"><?php echo $row_sejur_grecia['den_zona']; ?></a>
              <span ><span class="value"><?php echo $row_sejur_grecia['numar_hoteluri']; ?></span> hoteluri</span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right">vezi toate ofertele pentru <a href="/sejur-grecia/" title="Sejur Grecia" class="link-black">Sejur Grecia</a></div>
        </div></div>
<?php } ?>