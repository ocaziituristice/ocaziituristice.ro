<?php
$link_reveu = '/oferte-piata-de-craciun/';

$sel_reveu = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join continente ON continente.id_continent = tari.id_continent
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND oferta_sejur_tip.id_tip_oferta IN ('80')
AND oferte.nr_nopti > '1'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_reveu = mysql_query($sel_reveu) or die(mysql_error());

if(mysql_num_rows($que_reveu)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="green">Piata de Craciun</h3>
          <a href="<?php echo $link_reveu; ?>" rel="nofollow"><img src="/images/index/banner_piata-de-craciun-index_10-09-2013.jpg" alt="Piata de Craciun" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_reveu = mysql_fetch_array($que_reveu)) {
        if($row_reveu['oferte_moneda']=='EURO') $min_price_reveu = final_price_euro($row_reveu['min_price']); else $min_price_reveu = $row_reveu['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_reveu.fa_link($row_reveu['den_tara']).'/'.fa_link($row_reveu['den_zona']).'/'.fa_link($row_reveu['den_localitate']).'/'; ?>" title="Piata de Craciun <?php echo $row_reveu['den_localitate']; ?>"><?php echo $row_reveu['den_localitate']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_reveu; ?></span> <?php echo moneda(substr($row_reveu['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
          </div>
		  <div class="text-right"><a href="<?php echo $link_reveu; ?>" rel="nofollow" class="link-black">vezi toate ofertele Piata de Craciun</a></div>
        </div></div>
<?php } ?>