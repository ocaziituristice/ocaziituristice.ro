<?php
$link_revro = '/oferte-revelion/romania/';

$sel_revro = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND oferta_sejur_tip.id_tip_oferta IN ('69','33','72')
AND tari.denumire = 'Romania'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_revro = mysql_query($sel_revro) or die(mysql_error());

if(mysql_num_rows($que_revro)>0) {
?>
        <div class="chenar chn-color-orange NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Revelion Romania</h3>
          <a href="<?php echo $link_revro; ?>" rel="nofollow"><img src="/images/index/banner_revelion-romania-index_10-09-2013.jpg" alt="Revelion Romania" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_revro = mysql_fetch_array($que_revro)) {
        if($row_revro['oferte_moneda']=='EURO') $min_price_revro = final_price_euro($row_revro['min_price']); else $min_price_revro = $row_revro['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_revro.fa_link($row_revro['den_zona']).'/'.fa_link($row_revro['den_localitate']).'/'; ?>" title="Revelion <?php echo $row_revro['den_localitate']; ?>"><?php echo $row_revro['den_localitate']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_revro; ?></span> <?php echo moneda(substr($row_revro['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="<?php echo $link_revro; ?>" rel="nofollow" class="link-black">vezi toate ofertele Revelion Romania</a></div>
        </div></div>
<?php } ?>