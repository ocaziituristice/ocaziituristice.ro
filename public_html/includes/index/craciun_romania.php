<?php
$link_craro = '/oferte-craciun/romania/';

$sel_craro = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND oferta_sejur_tip.id_tip_oferta IN ('34','75','70')
AND tari.denumire = 'Romania'
AND oferte.nr_nopti > '1'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_craro = mysql_query($sel_craro) or die(mysql_error());

if(mysql_num_rows($que_craro)>0) {
?>
        <div class="chenar chn-color-blue NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="blue">Craciun Romania</h3>
          <a href="<?php echo $link_craro; ?>" rel="nofollow"><img src="/images/index/banner_craciun-romania-index_10-09-2013.jpg" alt="Craciun Romania" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_craro = mysql_fetch_array($que_craro)) {
        if($row_craro['oferte_moneda']=='EURO') $min_price_craro = final_price_euro($row_craro['min_price']); else $min_price_craro = $row_craro['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_craro.fa_link($row_craro['den_zona']).'/'.fa_link($row_craro['den_localitate']).'/'; ?>" title="Craciun <?php echo $row_craro['den_localitate']; ?>"><?php echo $row_craro['den_localitate']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_craro; ?></span> <?php echo moneda(substr($row_craro['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="<?php echo $link_craro; ?>" rel="nofollow" class="link-black">vezi toate ofertele Craciun Romania</a></div>
        </div></div>
<?php } ?>