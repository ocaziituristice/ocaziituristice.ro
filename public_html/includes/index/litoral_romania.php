<?php
$sel_litoral_romania = "SELECT
tari.denumire AS den_tara,
count(DISTINCT(hoteluri.id_hotel)) as numar_hoteluri,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND tari.denumire = 'Romania'
AND zone.denumire = 'Litoral'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_litoral_romania = mysql_query($sel_litoral_romania) or die(mysql_error());

if(mysql_num_rows($que_litoral_romania)>0) {
?>
        <div class="chenar chn-color-orange NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Litoral Romania</h3>
          <a href="/sejur-romania/litoral/" title="Litoral Romania"><img src="/images/index/banner_litoral_romania_index.jpg" alt="Litoral Romania" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_litoral_romania = mysql_fetch_array($que_litoral_romania)) {
        $min_price_litoral_romania = final_price_lei($row_litoral_romania['min_price'], $row_litoral_romania['oferte_moneda']);
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/cazare-'.fa_link($row_litoral_romania['den_localitate']).'/"'; ?>" title="Cazare <?php echo $row_litoral_romania['den_localitate']; ?>"><?php echo $row_litoral_romania['den_localitate']; ?></a>
              <span><span class="value"><?php echo  $row_litoral_romania['numar_hoteluri']; ?></span> hoteluri</span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right">Vezi toate ofertele pentru <a href="/sejur-romania/litoral/" title="Cazare Litoral Romania" class="link-black">Litoral Romania</a></div>
        </div></div>
<?php } ?>