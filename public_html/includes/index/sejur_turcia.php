<?php
$sel_charter_turcia = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND tari.denumire = 'Turcia'
GROUP BY localitati.denumire
ORDER BY numar DESC
LIMIT 0,6 ";
$que_charter_turcia = mysql_query($sel_charter_turcia) or die(mysql_error());

if(mysql_num_rows($que_charter_turcia)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Sejur Turcia</h3>
          <a href="/sejur-turcia/" ><img src="/images/index/banner_litoral_turcia_index.jpg" alt="Charter turcia Avion" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_charter_turcia = mysql_fetch_array($que_charter_turcia)) {
        if($row_charter_turcia['oferte_moneda']=='EURO') $min_price_charter_turcia = final_price_euro($row_charter_turcia['min_price']); else $min_price_charter_turcia = $row_charter_turcia['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/cazare-'.fa_link($row_charter_turcia['den_localitate']).'/?optiuni=da&transport=avion'; ?>" title="Cazare  <?php echo $row_charter_turcia['den_localitate']; ?> "><?php echo $row_charter_turcia['den_localitate']; ?></a>
              <span class="value"><?php echo $row_charter_turcia['numar']; ?></span> Oferte
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-turcia/" title="Sejur Turcia" class="link-black">Sejur turcia</a></div>
        </div></div>
<?php } ?>