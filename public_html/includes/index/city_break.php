<?php
$sel_city_break = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND oferta_sejur_tip.id_tip_oferta IN ('14')
AND tari.denumire <> 'Romania'
AND oferte.nr_nopti > '2'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_city_break = mysql_query($sel_city_break) or die(mysql_error());

if(mysql_num_rows($que_city_break)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h3 class="green">City Break</h3>
          <a href="/oferte-city-break/"><img src="/images/index/banner_city-break-index_04-01-2012.jpg" alt="City Break" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_city_break = mysql_fetch_array($que_city_break)) {
		if($row_city_break['oferte_moneda']=='EURO') $min_price_city_break = final_price_euro($row_city_break['min_price']); else $min_price_city_break = $row_city_break['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_city_break.fa_link($row_city_break['den_tara']).'/'.fa_link($row_city_break['den_zona']).'/'.fa_link($row_city_break['den_localitate']).'/'; ?>" title="City Break <?php echo $row_city_break['den_localitate']; ?>"><?php echo $row_city_break['den_localitate']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_city_break; ?></span> <?php echo moneda(substr($row_city_break['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
          </div>
		  <div class="text-right"><a href="/oferte-city-break/" title="Oferte City Break" class="link-black">Oferte City Break</a></div>
        </div></div>
<?php } ?>