<?php
$sel_sejur_spania = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim / oferte.nr_nopti) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND tari.denumire = 'Spania'
GROUP BY zone.denumire
ORDER BY numar DESC, zone.denumire ASC
LIMIT 0,6 ";
$que_sejur_spania = mysql_query($sel_sejur_spania) or die(mysql_error());

if(mysql_num_rows($que_sejur_spania)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Sejur Spania</h3>
          <a href="/sejur-spania/" title="Sejur Spania"><img src="/images/index/banner_sejur_spania_index.jpg" alt="Sejur Spania" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_sejur_spania = mysql_fetch_array($que_sejur_spania)) {
        if($row_sejur_spania['oferte_moneda']=='EURO') $min_price_sejur_spania = final_price_euro($row_sejur_spania['min_price']); else $min_price_sejur_spania = $row_sejur_spania['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/sejur-spania/'.fa_link($row_sejur_spania['den_zona']).'/"'; ?>" title="Sejur <?php echo $row_sejur_spania['den_zona']; ?>"><?php echo $row_sejur_spania['den_zona']; ?></a>
              <span class="value"><?php echo  $row_sejur_spania['numar']; ?></span> oferte
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-spania/" title="Sejur Spania" class="link-black">Sejur Spania</a></div>
        </div></div>
<?php } ?>