<?php
$link_revex = '/oferte-revelion/';

$sel_revex = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
Inner Join continente ON continente.id_continent = tari.id_continent
Inner Join oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND oferta_sejur_tip.id_tip_oferta IN ('69','33','72')
AND continente.nume_continent <> 'Europa'
AND oferte.nr_nopti > '2'
GROUP BY tari.denumire
ORDER BY numar DESC, tari.denumire ASC
LIMIT 0,6 ";
$que_revex = mysql_query($sel_revex) or die(mysql_error());

if(mysql_num_rows($que_revex)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h3 class="green">Revelion Exotic</h3>
          <a href="<?php echo $link_revex; ?>" rel="nofollow"><img src="/images/index/banner_revelion-exotic-index_10-09-2013.jpg" alt="Revelion Exotic" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_revex = mysql_fetch_array($que_revex)) {
		if($row_revex['oferte_moneda']=='EURO') $min_price_revex = final_price_euro($row_revex['min_price']); else $min_price_revex = $row_revex['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo $link_revex.fa_link($row_revex['den_tara']).'/'; ?>" title="Revelion Exotic <?php echo $row_revex['den_tara']; ?>"><?php echo $row_revex['den_tara']; ?></a>
              <span class="tarif"><span class="value"><?php echo $min_price_revex; ?></span> <?php echo moneda(substr($row_revex['moneda'], 0, 3)); ?></span>
            </div>
	<?php } ?>
          </div>
		  <div class="text-right"><a href="<?php echo $link_revex; ?>" rel="nofollow" class="link-black">vezi toate ofertele Revelion Exotic</a></div>
        </div></div>
<?php } ?>