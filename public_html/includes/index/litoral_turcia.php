<?php
$sel_litoral_turcia = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
count(DISTINCT(hoteluri.id_hotel)) as numar_hoteluri,
localitati.denumire AS den_localitate,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND tari.denumire = 'Turcia'
GROUP BY localitati.denumire
ORDER BY numar DESC, localitati.denumire ASC
LIMIT 0,6 ";
$que_litoral_turcia = mysql_query($sel_litoral_turcia) or die(mysql_error());

if(mysql_num_rows($que_litoral_turcia)>0) {
?>
        <div class="chenar chn-color-blue NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Sejur Turcia</h3>
          <a href="/sejur-turcia/" rel="nofollow noindex"><img src="/images/index/banner_litoral_turcia_index.jpg" alt="Sejur Turcia" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_litoral_turcia = mysql_fetch_array($que_litoral_turcia)) {
        if($row_litoral_turcia['oferte_moneda']=='EURO') $min_price_litoral_turcia = final_price_euro($row_litoral_turcia['min_price']); else $min_price_litoral_turcia = $row_litoral_turcia['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/sejur-turcia/'.fa_link($row_litoral_turcia['den_zona']).'/'.fa_link($row_litoral_turcia['den_localitate']).'/"'; ?>" title="Sejur <?php echo $row_litoral_turcia['den_localitate']; ?>"><?php echo $row_litoral_turcia['den_localitate']; ?></a>
              <span><span class="value"><?php echo $row_litoral_turcia['numar_hoteluri'];   ?></span> hoteluri</span>
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-turcia/" title="Sejur Turcia" class="link-black">Sejur Turcia</a></div>
        </div></div>
<?php } ?>