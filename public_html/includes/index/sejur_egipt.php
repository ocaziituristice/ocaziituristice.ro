<?php
$sel_charter_egipt = "SELECT
tari.denumire AS den_tara,
zone.denumire AS den_zona,
localitati.denumire AS den_localitate,
MIN(oferte.pret_minim) AS min_price,
oferte.moneda,
COUNT(oferte.id_oferta) AS numar
FROM oferte
Inner Join hoteluri ON hoteluri.id_hotel = oferte.id_hotel
Inner Join localitati ON localitati.id_localitate = hoteluri.locatie_id
Inner Join zone ON zone.id_zona = localitati.id_zona
Inner Join tari ON tari.id_tara = zone.id_tara
WHERE oferte.valabila = 'da'
AND hoteluri.tip_unitate = 'Hotel'
AND tari.denumire = 'Egipt'
GROUP BY localitati.denumire
ORDER BY numar DESC
LIMIT 0,6 ";
$que_charter_egipt = mysql_query($sel_charter_egipt) or die(mysql_error());

if(mysql_num_rows($que_charter_egipt)>0) {
?>
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h3 class="red">Sejur Egipt</h3>
          <a href="/sejur-turcia/" ><img src="/images/index/banner_egipt_index.jpg" alt="Sejur Egipt" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0; height:186px;">
	<?php while($row_charter_egipt = mysql_fetch_array($que_charter_egipt)) {
        if($row_charter_egipt['oferte_moneda']=='EURO') $min_price_charter_egipt = final_price_euro($row_charter_egipt['min_price']); else $min_price_charter_egipt = $row_charter_egipt['min_price'];
	?>
            <div class="item2 clearfix">
              <a href="<?php echo '/cazare-'.fa_link($row_charter_egipt['den_localitate']).'/?optiuni=da&transport=avion'; ?>" title="Cazare  <?php echo $row_charter_egipt['den_localitate']; ?> "><?php echo $row_charter_egipt['den_localitate']; ?></a>
              <span class="value"><?php echo $row_charter_egipt['numar']; ?></span> Oferte
            </div>
	<?php } ?>
		  </div>
		  <div class="text-right"><a href="/sejur-egipt/" title="Sejur Egipt" class="link-black">Sejur Egipt</a></div>
        </div></div>
<?php } ?>