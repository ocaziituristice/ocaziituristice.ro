<?php include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/functii.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/mysql.php' );

//var_dump( $_GET );

if ( $_GET['tip_oferta'] ) {
	$id_tip_sejur = $_GET['tip_oferta'];
	$tip_sejur    = get_tip_sejur( $id_tip_sejur );
	$tip_fii      = get_id_tip_sejur_fii( $id_tip_sejur );
	$iduri        = "'" . $id_tip_sejur . "'";
	if ( $tip_fii['id_tipuri'] ) {
		$iduri = $iduri . ',' . $tip_fii['id_tipuri'];
	}

	$linkPag = $sitepath . 'oferte-' . fa_link( $tip_sejur ) . '/';
} else {
	$iduri   = '';
	$linkPag = $sitepath . 'sejur-';
}

if ( $_GET['tara'] ) {
	$tara    = desfa_link( $_GET['tara'] );
	$id_tara = get_id_tara( $tara );
	$linkPag = $linkPag . $_GET['tara'] . '/';
}
if ( $_REQUEST['zone'] ) {
	$zona    = desfa_link( $_REQUEST['zone'] );
	$id_zona = get_id_zona( $zona, $id_tara );
	$linkPag = $linkPag . $_GET['zone'] . '/';
}
if ( $_REQUEST['oras'] ) {
	$oras          = desfa_link( $_REQUEST['oras'] );
	$id_localitate = get_id_localitate( $oras, $id_zona );
	$linkPag       = $linkPag . $_GET['oras'] . '/';
}
if ( $_GET['transport'] || $_GET['stele'] || $_GET['masa'] ) {
	$linkPag = $linkPag . '?optiuni=da';
}
if ( $_GET['transport'] ) {
	$transport    = desfa_link( $_GET['transport'] );
	$id_transport = get_id_transport( $transport );
	$linkPag      = $linkPag . '&transport=' . $_GET['transport'];
}
if ( $_GET['oras_plecare_avion'] ) {
	$plecare           = desfa_link( $_GET['oras_plecare_avion'] );
	$id_loc_plecare_av = get_id_localitate( $plecare, '' );
	$linkPag           = $linkPag . '&plecare-avion=' . $_GET['oras_plecare_avion'];
}
if ( $_GET['stele'] ) {
	$stele   = $_GET['stele'];
	$linkPag = $linkPag . '&stele=' . $_GET['stele'];
}
if ( $_GET['masa'] ) {
	$masa    = desfa_link( $_GET['masa'] );
	$linkPag = $linkPag . '&masa=' . $_GET['masa'];
}
?>

<?php // tara ?>
<div class="form-group col-md-3">
	<?php $tari_f = get_tari( '', '', '', '', '' ); ?>
	<?php
	if ( sizeof( $tari_f ) > 0 ) {
		foreach ( $tari_f as $key_tf => $value_tf ) {
			if ( desfa_link( $value_tf ) == $tara ) {
				$nume_tara = $value_tf;
			}
		}
	}
	?>
    <label class="white">Țara:</label>
    <div id="tara-placeholder" class="form-control input-replace">
		<?php echo $nume_tara ? $nume_tara : '<span class="placeholder">Alege țara</span>' ?>
    </div>
    <input id="tara" type="hidden" name="tara" value="<?php echo $tara ? $tara : '' ?>"/>
</div>

<?php // destinatia ?>
<?php
if ( $id_tara ) {
	$zone      = get_zone( $iduri, '', '', $id_tara, '' );
	$linkValue = '';
	if ( sizeof( $zone ) > 0 ) {
		foreach ( $zone as $id_zona1 => $value ) {
			$link_z = fa_link( $value );
			$loc_f  = get_localitate( $iduri, '', $id_zona1, $id_tara, '' );
			if ( $link_z == $_GET['zone'] ) {
				$location  = $value;
				$linkValue = '&zone=' . $link_z;
			}
			if ( sizeof( $loc_f ) > 0 ) {
				foreach ( $loc_f as $kloc_tf => $loc_tf ) {
					$link_lo = fa_link( $loc_tf['denumire'] );
					if ( $link_lo == $_GET['oras'] ) {
						$location  = $loc_tf['denumire'];
						$linkValue = '&zone=' . $link_z . '&oras=' . $link_lo;
					}
				}
			}

		}
	}
}
?>
<div class="form-group col-md-3">
    <label class="white">Destinația:</label>
    <div id="localitate-placeholder" class="form-control input-replace<?php echo $id_tara && ! $location ? ' active' : '' ?>">
		<?php echo $location ? $location : '<span class="placeholder">Alege destinația</span>' ?>
    </div>
    <input id="localitate" type="hidden" name="localitate" value="<?php echo $linkValue; ?>"/>
</div>

<?php // transport ?>
<div class="form-group col-md-2">
    <label class="white">Transport:</label>
	<?php
	$selTr = "SELECT transport.denumire
              FROM oferte
              INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
              INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
              INNER JOIN zone ON localitati.id_zona = zone.id_zona
              INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
	if ( $id_tip_sejur ) {
		$selTr .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
	}
	$selTr .= " WHERE oferte.valabila = 'da'
                  AND hoteluri.tip_unitate <> 'Circuit'
                  AND zone.id_tara = '" . $id_tara . "' ";
	if ( $id_zona ) {
		$selTr .= "AND zone.id_zona = '" . $id_zona . "' ";
	}
	if ( $id_localitate ) {
		$selTr .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
	}
	if ( $id_tip_sejur ) {
		$selTr .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
	}
	if ( $stele ) {
		$selTr .= "AND hoteluri.stele = '" . $stele . "' ";
	}
	if ( $masa ) {
		$selTr .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
	}
	$selTr .= "GROUP BY transport.denumire
                ORDER BY transport.denumire ";
	$queTr = mysql_query( $selTr ) or die( mysql_error() );
	?>
	<?php
	$i = 0;
	while ( $value = mysql_fetch_array( $queTr ) ) :
		$lingTr = fa_link( $value['denumire'] );
		if ( $lingTr == $_GET['transport'] ) {
			$textTransport = $value['denumire'];
			$linkTransport = '&transport=' . $lingTr;
		}
		if ( $value['denumire'] == 'Avion' ) :
			$selFTA = "SELECT loc_plecare.denumire 
                          FROM oferte 
                          INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
                          INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
                          INNER JOIN zone ON localitati.id_zona = zone.id_zona
                          INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
                          INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
                          INNER JOIN localitati AS loc_plecare ON aeroport.id_localitate = loc_plecare.id_localitate ";
			if ( $id_tip_sejur ) {
				$selFTA .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
			}
			$selFTA .= "WHERE oferte.valabila = 'da' 
							            AND hoteluri.tip_unitate <> 'Circuit'
                                        AND zone.id_tara = '" . $id_tara . "'
                                        AND oferte_transport_avion.tip = 'dus' ";
			if ( $id_zona ) {
				$selFTA .= "AND zone.id_zona = '" . $id_zona . "' ";
			}
			if ( $id_localitate ) {
				$selFTA .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
			}
			if ( $id_tip_sejur ) {
				$selFTA .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
			}
			if ( $stele ) {
				$selFTA .= "AND hoteluri.stele = '" . $stele . "' ";
			}
			if ( $masa ) {
				$selFTA .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
			}
			$selFTA .= "GROUP BY loc_plecare.denumire
	                                    ORDER BY loc_plecare.denumire ";
			$queFTA = mysql_query( $selFTA ) or die( mysql_error() ); ?>
			<?php
			while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
				$lingTr = fa_link( $loc_FTA['denumire'] );
				if ( $lingTr == $_GET['oras_plecare_avion'] && $transport == 'avion' ) {
					$linkTransport = '&transport=avion&oras_plecare_avion=' . $lingTr;
					$textTransport = $loc_FTA['denumire'];
				}
			}
			?>
			<?php @mysql_free_result( $queFTA ); ?>
		<?php
		endif; ?>
		<?php
		$i ++;
	endwhile;
	@mysql_free_result( $queTr );
	?>
    <div id="transport-placeholder" class="form-control input-replace<?php echo $id_tara && $location && ! $transport ? ' active' : '' ?>">
		<?php echo $textTransport ? $textTransport : '<span class="placeholder">Alege transport</span>' ?>
    </div>
    <input id="transport" type="hidden" name="transport" value="<?php echo $linkTransport ?>"/>
</div>

<?php // stele ?>
<?php $selSt = "SELECT hoteluri.stele
                                FROM oferte
                                INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
                                INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
                                INNER JOIN zone ON localitati.id_zona = zone.id_zona
                                INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
if ( $id_loc_plecare_av ) {
	$selSt .= "INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
                                INNER JOIN aeroport ON (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport AND aeroport.id_localitate = '" . $id_loc_plecare_av . "') ";
}
if ( $id_tip_sejur ) {
	$selSt .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
}
$selSt .= "WHERE oferte.valabila = 'da'
                            AND hoteluri.tip_unitate <> 'Circuit'
                            AND hoteluri.stele > '0'
                            AND zone.id_tara = '" . $id_tara . "' ";
if ( $id_zona ) {
	$selSt .= "AND zone.id_zona = '" . $id_zona . "' ";
}
if ( $id_localitate ) {
	$selSt .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
}
if ( $id_tip_sejur ) {
	$selSt .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
}
if ( $masa ) {
	$selSt .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
}
$selSt .= "GROUP BY hoteluri.stele
                            ORDER BY hoteluri.stele ";
$queSt = mysql_query( $selSt ) or die( mysql_error() ); ?>
<?php
while ( $nr_s = mysql_fetch_array( $queSt ) ) {
	$i = $nr_s['stele'];
    if ( $i == $stele ) {
        $valueStars = $i;
	    $textStars = $i > 1 ? $i . ' stele' : $i . 'stea';
    }
} ?>
<div class="form-group col-md-2">
    <label class="white">Număr stele:</label>
    <div id="stele-placeholder" class="form-control input-replace<?php echo $id_tara && $location && $transport && ! $stele ? ' active' : '' ?>">
        <?php echo $textStars ? $textStars : '<span class="placeholder">Alege număr stele</span>' ?>
    </div>
    <input id="stele" type="hidden" name="stele" value="<?php echo $valueStars ?>"/>
</div>

<div class="form-group col-md-2">
    <a href="<?php echo $linkPag; ?>" class="btn"
       onclick="ga('send', 'event', 'form cautare', 'motor cautare index', '<?php echo substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI']; ?>');">
        Caută
    </a>
</div>
<br class="clear">

<div class="clear"></div>

<div class="col-md-12">
    <div class="advanced-search-options">

		<?php // continut tara ?>
        <div class="tara-search search-box-wrapper" style="display: none;">
			<?php if ( sizeof( $tari_f ) > 0 ) : ?>
				<?php foreach ( $tari_f as $key_tf => $value_tf ) : ?>
                    <div class="option-column">
                        <div class="js-option option<?php echo desfa_link( $value_tf ) == $tara ? ' active' : '' ?>"
                             data-value="<?php echo fa_link( $value_tf ) ?>"
                             data-type="tara"><?php echo $value_tf; ?></div>
                    </div>
				<?php endforeach; ?>
			<?php endif; ?>
            <div class="clear"></div>
        </div>

		<?php // continut localitate ?>
        <div class="localitate-search search-box-wrapper"<?php echo ! $id_tara || $location ? ' style="display: none;"' : '' ?>>
			<?php if ( $id_tara ) : ?>
				<?php
				$zone = get_zone( $iduri, '', '', $id_tara, '' );
				if ( sizeof( $zone ) > 0 ) : ?>
                    <div class="nav nav-tabs tabs-left">
						<?php
						$i         = 0;
						foreach ( $zone as $id_zona1 => $value ) :
							$link_z = fa_link( $value );
							$loc_f = get_localitate( $iduri, '', $id_zona1, $id_tara, '' );
							?>
                            <div class="option-column">
                                <div class="js-option option<?php echo $link_z == $_GET['zone'] || ( ! $_GET['zone'] && $i == 0 ) ? ' active' : '' ?>"
									<?php echo $loc_f > 0 ? ' data-toggle="location-' . $link_z . '"' : ' data-value="&zone=' . $link_z . '" data-type="localitate"'; ?>>
									<?php echo $value; ?>
                                </div>
                            </div>
                            <div class="clear"></div>
							<?php
							$i ++;
						endforeach; ?>
                    </div>
                    <div class="tab-content">
						<?php
						$i         = 0;
						foreach ( $zone as $id_zona1 => $value ) :
							$link_z = fa_link( $value );
							$loc_f = get_localitate( $iduri, '', $id_zona1, $id_tara, '' );
							?>
							<?php if ( sizeof( $loc_f ) > 0 ) : ?>
                            <div class="tab-pane<?php echo $link_z == $_GET['zone'] || ( ! $_GET['zone'] && $i == 0 ) ? ' active' : '' ?>"
                                 id="<?php echo 'location-' . $link_z; ?>">
                                <div class="tab-pane-wrapper">
                                    <div class="option-column">
                                        <div class="js-option option<?php echo $link_z == $_GET['zone'] && ! $_GET['oras'] ? ' active' : '' ?>"
                                             data-value="<?php echo '&zone=' . $link_z; ?>" data-type="localitate"
                                             data-title="<?php echo $value ?>">
                                            Toate localitățile din <?php echo $value ?>
                                        </div>
                                    </div>
									<?php foreach ( $loc_f as $kloc_tf => $loc_tf ) :
										$link_lo = fa_link( $loc_tf['denumire'] ); ?>
                                        <div class="option-column">
                                            <div class="js-option option<?php echo $link_lo == $_GET['oras'] ? ' active' : '' ?>"
                                                 data-value="<?php echo '&zone=' . $link_z . '&oras=' . $link_lo; ?>"
                                                 data-type="localitate">
												<?php echo $loc_tf['denumire']; ?>
                                            </div>
                                        </div>
									<?php endforeach; ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
						<?php endif; ?>
							<?php $i ++; ?>
						<?php endforeach; ?>
                    </div>
                    <div class="clear"></div>
				<?php endif; ?>
			<?php else : ?>
                <div class="option-column" style="width: 100%;">
                    <div class="option active" onclick="$('#tara-placeholder').trigger('click');">
                        Te rugam sa selectezi tara inainte
                    </div>
                </div>
                <div class="clear"></div>
			<?php endif; ?>
        </div>

		<?php // continut transport ?>
        <div class="transport-search search-box-wrapper"<?php echo ! $id_tara || ! $location || $transport ? ' style="display: none;"' : '' ?>>
			<?php if ( $id_tara ) : ?>
                <div class="nav nav-tabs tabs-left">
					<?php
					$selTr = "SELECT transport.denumire
                              FROM oferte
                              INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
                              INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
                              INNER JOIN zone ON localitati.id_zona = zone.id_zona
                              INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
					if ( $id_tip_sejur ) {
						$selTr .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
					}
					$selTr .= " WHERE oferte.valabila = 'da'
                                AND hoteluri.tip_unitate <> 'Circuit'
                                AND zone.id_tara = '" . $id_tara . "' ";
					if ( $id_zona ) {
						$selTr .= "AND zone.id_zona = '" . $id_zona . "' ";
					}
					if ( $id_localitate ) {
						$selTr .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
					}
					if ( $id_tip_sejur ) {
						$selTr .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
					}
					if ( $stele ) {
						$selTr .= "AND hoteluri.stele = '" . $stele . "' ";
					}
					if ( $masa ) {
						$selTr .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
					}
					$selTr .= "GROUP BY transport.denumire
                               ORDER BY transport.denumire ";
					$queTr = mysql_query( $selTr ) or die( mysql_error() );
					?>
					<?php
					$i = 0;
					while ( $value = mysql_fetch_array( $queTr ) ) :
						$lingTr = fa_link( $value['denumire'] ); ?>
                        <div class="option-column">
                            <div class="js-option option<?php echo $lingTr == $_GET['transport'] || ( ! $_GET['transport'] && $i == 0 ) ? ' active' : '' ?>"
                                 data-value="<?php echo '&transport=' . $lingTr; ?>" data-type="transport">
								<?php echo $value['denumire']; ?>
                            </div>
                        </div>
                        <div class="clear"></div>
						<?php
						$i ++;
					endwhile; ?>
                </div>
                <div class="tab-content">
					<?php $selTr = "SELECT transport.denumire 
                                    FROM oferte
                                    INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
                                    INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
                                    INNER JOIN zone ON localitati.id_zona = zone.id_zona
                                    INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
					if ( $id_tip_sejur ) {
						$selTr .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
					}
					$selTr .= " WHERE oferte.valabila = 'da'
                                AND hoteluri.tip_unitate <> 'Circuit'
                                AND zone.id_tara = '" . $id_tara . "' ";
					if ( $id_zona ) {
						$selTr .= "AND zone.id_zona = '" . $id_zona . "' ";
					}
					if ( $id_localitate ) {
						$selTr .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
					}
					if ( $id_tip_sejur ) {
						$selTr .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
					}
					if ( $stele ) {
						$selTr .= "AND hoteluri.stele = '" . $stele . "' ";
					}
					if ( $masa ) {
						$selTr .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
					}
					$selTr .= "GROUP BY transport.denumire
                                ORDER BY transport.denumire ";
					$queTr = mysql_query( $selTr ) or die( mysql_error() ); ?>
					<?php
					$i = 0;
					while ( $value = mysql_fetch_array( $queTr ) ) :
						$lingTr = fa_link( $value['denumire'] );
						if ( $value['denumire'] == 'Avion' ) :
							$selFTA = "SELECT loc_plecare.denumire 
                                      FROM oferte 
                                      INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
                                      INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
                                      INNER JOIN zone ON localitati.id_zona = zone.id_zona
                                      INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
                                      INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
                                      INNER JOIN localitati AS loc_plecare ON aeroport.id_localitate = loc_plecare.id_localitate ";
							if ( $id_tip_sejur ) {
								$selFTA .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
							}
							$selFTA .= "WHERE oferte.valabila = 'da' 
							            AND hoteluri.tip_unitate <> 'Circuit'
                                        AND zone.id_tara = '" . $id_tara . "'
                                        AND oferte_transport_avion.tip = 'dus' ";
							if ( $id_zona ) {
								$selFTA .= "AND zone.id_zona = '" . $id_zona . "' ";
							}
							if ( $id_localitate ) {
								$selFTA .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
							}
							if ( $id_tip_sejur ) {
								$selFTA .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
							}
							if ( $stele ) {
								$selFTA .= "AND hoteluri.stele = '" . $stele . "' ";
							}
							if ( $masa ) {
								$selFTA .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
							}
							$selFTA .= "GROUP BY loc_plecare.denumire
	                                    ORDER BY loc_plecare.denumire ";
							$queFTA = mysql_query( $selFTA ) or die( mysql_error() ); ?>
                            <div class="tab-pane<?php echo $lingTr == $_GET['transport'] || ( ! $_GET['transport'] && $i == 0 ) ? ' active' : '' ?>">
                                <div class="tab-pane-wrapper">
                                    <h4>Orasul de plecare</h4>
                                    <div class="option-column">
                                        <div class="js-option option<?php echo $lingTr == $_GET['transport'] && ! $_GET['oras_plecare_avion'] ? ' active' : '' ?>"
                                             data-value="<?php echo '&transport=' . $lingTr; ?>"
                                             data-type="transport"
                                             data-title="<?php echo $value['denumire'] ?>">
                                            Toate orasele
                                        </div>
                                    </div>
									<?php while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) :
										$lingTr = fa_link( $loc_FTA['denumire'] );
										?>
                                        <div class="option-column">
                                            <div class="js-option option<?php echo $lingTr == $_GET['oras_plecare_avion'] && $transport == 'avion' ? ' active' : '' ?>"
                                                 data-value="<?php echo '&transport=avion&oras_plecare_avion=' . $lingTr; ?>"
                                                 data-type="transport">
												<?php echo $loc_FTA['denumire']; ?>
                                            </div>
                                        </div>
									<?php endwhile; ?>
									<?php @mysql_free_result( $queFTA ); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
						<?php
						endif; ?>
						<?php
						$i ++;
					endwhile;
					@mysql_free_result( $queTr );
					?>
                </div>
                <div class="clear"></div>
			<?php else : ?>
                <div class="option-column" style="width: 100%;">
                    <div class="option active" onclick="$('#tara-placeholder').trigger('click');">
                        Te rugam sa selectezi tara inainte
                    </div>
                </div>
                <div class="clear"></div>
			<?php endif; ?>
        </div>

        <?php // continut stele ?>
        <div class="stele-search search-box-wrapper"<?php echo ! $id_tara || ! $location || ! $transport || $stele ? ' style="display: none;"' : '' ?>>
			<?php if ( $id_tara ) : ?>
				<?php $selSt = "SELECT hoteluri.stele
                                FROM oferte
                                INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
                                INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
                                INNER JOIN zone ON localitati.id_zona = zone.id_zona
                                INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
				if ( $id_loc_plecare_av ) {
					$selSt .= "INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
                                INNER JOIN aeroport ON (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport AND aeroport.id_localitate = '" . $id_loc_plecare_av . "') ";
				}
				if ( $id_tip_sejur ) {
					$selSt .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
				}
				$selSt .= "WHERE oferte.valabila = 'da'
                            AND hoteluri.tip_unitate <> 'Circuit'
                            AND hoteluri.stele > '0'
                            AND zone.id_tara = '" . $id_tara . "' ";
				if ( $id_zona ) {
					$selSt .= "AND zone.id_zona = '" . $id_zona . "' ";
				}
				if ( $id_localitate ) {
					$selSt .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
				}
				if ( $id_tip_sejur ) {
					$selSt .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
				}
				if ( $masa ) {
					$selSt .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
				}
				$selSt .= "GROUP BY hoteluri.stele
                            ORDER BY hoteluri.stele ";
				$queSt = mysql_query( $selSt ) or die( mysql_error() ); ?>
				<?php while ( $nr_s = mysql_fetch_array( $queSt ) ) :
					$i = $nr_s['stele']; ?>
                    <div class="option-column">
                        <div class="js-option option<?php echo $i == $stele ? ' active' : '' ?>"
                             data-value="<?php echo $i; ?>"
                             data-type="stele"><?php echo $i > 1 ? $i . ' stele' : $i . 'stea'; ?></div>
                    </div>
				<?php endwhile; ?>
                <div class="clear"></div>
			<?php else : ?>
                <div class="option-column" style="width: 100%;">
                    <div class="option active" onclick="$('#tara-placeholder').trigger('click');">
                        Te rugam sa selectezi tara inainte
                    </div>
                </div>
                <div class="clear"></div>
			<?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.input-replace').click(function () {
            if ($(this).hasClass('active')) {
                $('.input-replace').removeClass('active');
                $('.search-box-wrapper').hide();
            } else {
                var type = ($(this).attr('id')).replace('-placeholder', '');
                $('.input-replace').removeClass('active');
                $(this).addClass('active');
                $('.search-box-wrapper').hide();
                $('.' + type + '-search').show();
            }
        });

        $('.js-option').click(function () {
            if ($(this).closest('.nav-tabs').length > 0) {
                var parent = $(this).closest('.nav-tabs');
            }

            if (parent) {
                parent.find('.js-option').removeClass('active');
                $(this).addClass('active');
            }

            var triggeredId = $(this).attr('data-toggle');
            if (triggeredId && parent) {
                parent.siblings('.tab-content').find('.tab-pane').removeClass('active');
                parent.siblings('.tab-content').find('#' + triggeredId).addClass('active');
                return false;
            }
            var type = $(this).attr('data-type');
            var value = $(this).attr('data-value');
            var text = $(this).html();
            if ($(this).attr('data-title')) {
                text = $(this).attr('data-title');
            }
            $('#' + type + '-placeholder').html(text);
            $('#' + type).val(value);
            if (type == 'tara' || type == 'localitate') {
                af_filtru_responsive(1);
            }
            if (type == 'transport' || type == 'stele') {
                af_filtru_responsive(2);
            }
        });
    });
</script>
