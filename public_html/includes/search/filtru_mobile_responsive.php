<?php include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/functii.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/mysql.php' );

if ( $_GET['tip_oferta'] ) {
	$id_tip_sejur = $_GET['tip_oferta'];
	$tip_sejur    = get_tip_sejur( $id_tip_sejur );
	$tip_fii      = get_id_tip_sejur_fii( $id_tip_sejur );
	$iduri        = "'" . $id_tip_sejur . "'";
	if ( $tip_fii['id_tipuri'] ) {
		$iduri = $iduri . ',' . $tip_fii['id_tipuri'];
	}

	$linkPag = $sitepath . 'oferte-' . fa_link( $tip_sejur ) . '/';
} else {
	$iduri   = '';
	$linkPag = $sitepath . 'sejur-';
}

if ( $_GET['tara'] ) {
	$tara    = desfa_link( $_GET['tara'] );
	$id_tara = get_id_tara( $tara );
	$linkPag = $linkPag . $_GET['tara'] . '/';
}
if ( $_REQUEST['zone'] ) {
	$zona    = desfa_link( $_REQUEST['zone'] );
	$id_zona = get_id_zona( $zona, $id_tara );
	$linkPag = $linkPag . $_GET['zone'] . '/';
}
if ( $_REQUEST['oras'] ) {
	$oras          = desfa_link( $_REQUEST['oras'] );
	$id_localitate = get_id_localitate( $oras, $id_zona );
	$linkPag       = $linkPag . $_GET['oras'] . '/';
}
if ( $_GET['transport'] || $_GET['stele'] || $_GET['masa'] ) {
	$linkPag = $linkPag . '?optiuni=da';
}
if ( $_GET['transport'] ) {
	$transport    = desfa_link( $_GET['transport'] );
	$id_transport = get_id_transport( $transport );
	$linkPag      = $linkPag . '&transport=' . $_GET['transport'];
}
if ( $_GET['oras_plecare_avion'] ) {
	$plecare           = desfa_link( $_GET['oras_plecare_avion'] );
	$id_loc_plecare_av = get_id_localitate( $plecare, '' );
	$linkPag           = $linkPag . '&plecare-avion=' . $_GET['oras_plecare_avion'];
}
if ( $_GET['stele'] ) {
	$stele   = $_GET['stele'];
	$linkPag = $linkPag . '&stele=' . $_GET['stele'];
}
if ( $_GET['masa'] ) {
	$masa    = desfa_link( $_GET['masa'] );
	$linkPag = $linkPag . '&masa=' . $_GET['masa'];
}
?>
<div class="form-group col-md-3">
    <label class="white">Țara:</label>
	<?php $tari_f = get_tari( $iduri, '', '', '', '' ); ?>
    <select name="tara" id="tara" onchange="af_filtru_mobile_responsive(1<?php if ( $id_tip_sejur ) {
		echo ',' . $id_tip_sejur;
	} ?>);" class="form-control input-replace">
        <option value="">Alege țara</option>
		<?php if ( sizeof( $tari_f ) > 0 ) {
			foreach ( $tari_f as $key_tf => $value_tf ) {
				?>
                <option value="<?php echo fa_link( $value_tf ); ?>" <?php if ( desfa_link( $value_tf ) == $tara ) {
					echo 'selected';
				}
				if ( isset( $_GET['turism-extern'] ) and $value_tf == 'Romania' ) {
					echo 'hidden';
				} ?>><?php echo $value_tf; ?></option>
			<?php }
		} ?>
    </select>
</div>
<div class="form-group col-md-3">
    <label class="white">Destinația:</label>
    <select name="localitate" id="localitate" onchange="af_filtru_mobile_responsive(1<?php if ( $id_tip_sejur ) {
		echo ',' . $id_tip_sejur;
	} ?>);" class="form-control input-replace">
        <option value="">Alege destinația</option>
		<?php if ( ! $id_tara ) { ?>
            <option value="" disabled="disabled">Selectați țara întâi</option><?php } ?>
		<?php if ( $id_tara ) {
			$zone = get_zone( $iduri, '', '', $id_tara, '' );
			if ( sizeof( $zone ) > 0 ) {
				foreach ( $zone as $id_zona1 => $value ) {
					$link_z = fa_link( $value ); ?>
                    <option value="<?php echo '&zone=' . $link_z; ?>" <?php if ( $link_z == $_GET['zone'] ) {
						echo 'selected';
					} ?>><?php echo strtoupper( $value ); ?></option>
					<?php $loc_f = get_localitate( $iduri, '', $id_zona1, $id_tara, '' );
					if ( sizeof( $loc_f ) > 0 ) {
						foreach ( $loc_f as $kloc_tf => $loc_tf ) {
							$link_lo = fa_link( $loc_tf['denumire'] ); ?>
                            <option value="<?php echo '&zone=' . $link_z . '&oras=' . $link_lo; ?>" <?php if ( $link_lo == $_GET['oras'] ) {
								echo 'selected';
							} ?>>&nbsp;&nbsp;&nbsp;<?php echo $loc_tf['denumire']; ?></option>
						<?php }
					}
				}
			}
		} ?>
    </select>
</div>
<div class="form-group col-md-2">
    <label class="white">Transport:</label>
	<?php $selTr = "SELECT transport.denumire
  FROM oferte
  INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
  INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
  INNER JOIN zone ON localitati.id_zona = zone.id_zona
  INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
	if ( $id_tip_sejur ) {
		$selTr .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
	}
	$selTr .= " WHERE oferte.valabila = 'da'
  AND hoteluri.tip_unitate <> 'Circuit'
  AND zone.id_tara = '" . $id_tara . "' ";
	if ( $id_zona ) {
		$selTr .= "AND zone.id_zona = '" . $id_zona . "' ";
	}
	if ( $id_localitate ) {
		$selTr .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
	}
	if ( $id_tip_sejur ) {
		$selTr .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
	}
	if ( $stele ) {
		$selTr .= "AND hoteluri.stele = '" . $stele . "' ";
	}
	if ( $masa ) {
		$selTr .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
	}
	$selTr .= "GROUP BY transport.denumire
  ORDER BY transport.denumire ";
	$queTr = mysql_query( $selTr ) or die( mysql_error() ); ?>
    <select name="transport" id="transport" onchange="af_filtru_mobile_responsive(2<?php if ( $id_tip_sejur ) {
		echo ',' . $id_tip_sejur;
	} ?>);" class="form-control input-replace">
        <option value="" <?php if ( ! $id_transport ) {
			echo 'selected';
		} ?>>
            Alege tipul transportului
        </option>
		<?php if ( ! $id_tara ) { ?>
            <option value="" disabled="disabled">Selectați țara întâi</option><?php } ?>
		<?php $i = 0;
		while ( $value = mysql_fetch_array( $queTr ) ) {
			$i ++;
			$lingTr = fa_link( $value['denumire'] ); ?>
            <option value="<?php echo '&transport=' . $lingTr; ?>" <?php if ( $lingTr == $_GET['transport'] ) {
				echo 'selected';
			} ?>><?php echo strtoupper( $value['denumire'] ); ?></option>
			<?php if ( $value['denumire'] == 'Avion' ) {
				$selFTA = "SELECT loc_plecare.denumire
    FROM oferte
    INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
    INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
    INNER JOIN zone ON localitati.id_zona = zone.id_zona
    INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
    INNER JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
    INNER JOIN localitati AS loc_plecare ON aeroport.id_localitate = loc_plecare.id_localitate ";
				if ( $id_tip_sejur ) {
					$selFTA .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
				}
				$selFTA .= "WHERE oferte.valabila = 'da'
    AND hoteluri.tip_unitate <> 'Circuit'
    AND zone.id_tara = '" . $id_tara . "'
    AND oferte_transport_avion.tip = 'dus' ";
				if ( $id_zona ) {
					$selFTA .= "AND zone.id_zona = '" . $id_zona . "' ";
				}
				if ( $id_localitate ) {
					$selFTA .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
				}
				if ( $id_tip_sejur ) {
					$selFTA .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
				}
				if ( $stele ) {
					$selFTA .= "AND hoteluri.stele = '" . $stele . "' ";
				}
				if ( $masa ) {
					$selFTA .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
				}
				$selFTA .= "
	GROUP BY loc_plecare.denumire
	ORDER BY loc_plecare.denumire ";
				$queFTA = mysql_query( $selFTA ) or die( mysql_error() ); ?>
                <optgroup label="Orasul plecarii cu avionul">
					<?php while ( $loc_FTA = mysql_fetch_array( $queFTA ) ) {
						$lingTr = fa_link( $loc_FTA['denumire'] ); ?>
                        <option value="<?php echo '&transport=avion&oras_plecare_avion=' . $lingTr; ?>" <?php if ( $lingTr == $_GET['oras_plecare_avion'] || ( $transport == 'avion' && mysql_num_rows( $queFTA ) == 1 ) ) {
							echo 'selected';
						} ?>>&nbsp;&nbsp;&nbsp;<?php echo $loc_FTA['denumire']; ?></option>
					<?php }
					@mysql_free_result( $queFTA ); ?></optgroup>
			<?php } ?>
		<?php }
		@mysql_free_result( $queTr ); ?>
    </select>
</div>
<div class="form-group col-md-2">
    <label class="white">Număr stele:</label>
	<?php $selSt = "SELECT hoteluri.stele
    FROM oferte
    INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
    INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
    INNER JOIN zone ON localitati.id_zona = zone.id_zona
    INNER JOIN transport ON oferte.id_transport = transport.id_trans ";
	if ( $id_loc_plecare_av ) {
		$selSt .= "INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
    INNER JOIN aeroport ON (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport AND aeroport.id_localitate = '" . $id_loc_plecare_av . "') ";
	}
	if ( $id_tip_sejur ) {
		$selSt .= "INNER JOIN oferta_sejur_tip ON oferta_sejur_tip.id_oferta = oferte.id_oferta ";
	}
	$selSt .= "WHERE oferte.valabila = 'da'
    AND hoteluri.tip_unitate <> 'Circuit'
    AND hoteluri.stele > '0'
    AND zone.id_tara = '" . $id_tara . "' ";
	if ( $id_zona ) {
		$selSt .= "AND zone.id_zona = '" . $id_zona . "' ";
	}
	if ( $id_localitate ) {
		$selSt .= "AND localitati.id_localitate = '" . $id_localitate . "' ";
	}
	if ( $id_tip_sejur ) {
		$selSt .= "AND oferta_sejur_tip.id_tip_oferta IN (" . $iduri . ") ";
	}
	if ( $masa ) {
		$selSt .= "AND LOWER(oferte.masa) = '" . $masa . "' ";
	}
	$selSt .= "GROUP BY hoteluri.stele
    ORDER BY hoteluri.stele ";
	$queSt = mysql_query( $selSt ) or die( mysql_error() ); ?>
    <select name="stele" id="stele" onchange="af_filtru_mobile_responsive(2<?php if ( $id_tip_sejur ) {
		echo ',' . $id_tip_sejur;
	} ?>);" class="form-control input-replace">
        <option value="">Alege număr stele</option>
		<?php if ( ! $id_tara ) { ?>
            <option value="" disabled="disabled">Selectați țara întâi</option><?php } ?>
		<?php while ( $nr_s = mysql_fetch_array( $queSt ) ) {
			$i = $nr_s['stele']; ?>
            <option value="<?php echo $i; ?>" <?php if ( $i == $stele ) {
				echo 'selected';
			} ?>><?php echo $i;
				if ( $i > 1 ) {
					echo ' stele ';
				} else {
					echo ' stea ';
				} ?></option>
		<?php } ?>
    </select>
</div>

<div class="form-group col-md-2">
    <a href="<?php echo $linkPag; ?>" class="btn"
       onclick="ga('send', 'event', 'form cautare', 'motor cautare index', '<?php echo substr( $sitepath, 0, - 1 ) . $_SERVER['REQUEST_URI']; ?>');">
        Caută
    </a>
</div>

<div class="clear"></div>
