<?php
$loc_f = get_localitate( $iduri, '', $id_zona, $id_tara, $early );
$loc_f = array_orderby( $loc_f, 'count_hotel', SORT_DESC );
?>

<div class="chenar chn-color-grey NEW-round8px most-important-locations">
    <div class="inner NEW-round6px clearfix">
        <h3 class="blue underline">
			<?php if ( $id_tara != 1 ) {
				echo "Cele mai importante localitati din ";
			} ?>
			<?php echo ucwords( $den_zona ) . " - " . ucwords( $den_tara ); ?>
        </h3>
        <div class="ofVizitate">
			<?php
			foreach ( $loc_f as $kloc_tf => $localitate_zona ) {
				if ( $tmp ++ < 8 ) {
					if ( $localitate_zona['poza1'] ) {
						$last_pic = '/thumb_localitate/' . $localitate_zona['poza1'];
					} else {
						$last_pic = '/images/no_photo.jpg';
					}
					?>
                    <div class="prod city-airports">
                        <a href="<?php echo "/cazare-" . fa_link( $localitate_zona['denumire'] ) . "/"; ?>"
                           class="link-blue"
                           title="<?php echo $localitate_zona['denumire'] . " - " . $localitate_zona['zona']; ?>">
                            <div class="titlu">
								<?php echo $localitate_zona['denumire']; ?>
                                <span>(<?php echo $localitate_zona['count_hotel']; ?> <?php echo $localitate_zona['count_hotel'] > 1 ? 'Hoteluri' : 'Hotel' ?>)</span>
                            </div>
                            <img class="image"
                                 src="<?php echo $last_pic; ?>"
                                 alt="<?php echo $localitate_zona['denumire'] . " " . $localitate_zona['zona'] ?>"/>
							<?php if ( $localitate_zona['descriere_localitate'] ) : ?>
                                <div class="description"><?php echo $localitate_zona['descriere_localitate']; ?></div>
							<?php endif ?>
                        </a>
                    </div>
				<?php }
			} ?>
            <div class="clear"></div>
        </div>
    </div>
</div>