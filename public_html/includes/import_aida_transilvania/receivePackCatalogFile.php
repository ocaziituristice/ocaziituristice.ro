<?php
if(isset($_FILES["catalogFile"]["name"])) {
	$allowedExts = "zip";
	$temp = explode(".", $_FILES["catalogFile"]["name"]);
	$extension = end($temp);
	if($extension==$allowedExts) {
		if($_FILES["catalogFile"]["error"] > 0) {
			echo "Return Code: " . $_FILES["catalogFile"]["error"] . "<br>";
		} else {
			echo "Upload: " . $_FILES["catalogFile"]["name"] . "<br>";
			echo "Size: " . ($_FILES["catalogFile"]["size"] / (1024*1024)) . " MB"."<br>";
			echo "<br>";
			
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/includes/import_aida_transilvania/".$_FILES["catalogFile"]["name"])) {
				echo $_FILES["catalogFile"]["name"]." already exists. ";
			} else {
				move_uploaded_file($_FILES["catalogFile"]["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/includes/import_aida_transilvania/".$_FILES["catalogFile"]["name"]);
				echo "File ".$_FILES["file"]["name"]." successfully uploaded";
				
				mail("contact@ocaziituristice.ro", "Primire fisier Transilvania Travel", "File ".$_FILES["file"]["name"]." successfully uploaded", "From: System Ocaziituristice.ro <no-reply@ocaziituristice.ro>");
			}
		}
	} else {
		echo "Invalid file";
	}
}
?>

<?php /*?><form action="" method="post"
enctype="multipart/form-data">
<label for="file">Filename:</label>
<input type="file" name="catalogFile" id="catalogFile"><br>
<input type="submit" name="submit" value="Submit">
</form><?php */?>
