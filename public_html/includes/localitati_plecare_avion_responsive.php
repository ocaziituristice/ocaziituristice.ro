<?php
$localitati_plecare_avion_array = explode( ',', $zona_detalii['localitati_plecare_avion'] );
?>
<?php if ( sizeof( $localitati_plecare_avion_array ) > 0 && ! empty( $localitati_plecare_avion_array[0] ) ) { ?>
    <br>
    <div class="NEW-round8px">
        <div class="inner NEW-round6px clearfix">
            <h3 class="blue underline"><?php if ( $id_tara != 1 ) {
					echo "Localitati de plecare pentru ";
				} ?><?php echo ucwords( $den_zona ) . " - " . ucwords( $den_tara ); ?></h3>
            <div class="ofVizitate clearfix">
				<?php
				foreach ( $localitati_plecare_avion_array as $k_localitati_plecare_avion_array => $v_localitati_plecare_avion_array ) :
					$last_pic = '/images/aeroporturi/' . fa_link( $v_localitati_plecare_avion_array ) . '.jpg';
					?>
                    <div class="prod city-airports">
                        <a href="<?php echo "/sejur-" . fa_link( $zona_detalii['denumire_tara'] ) . "/" . fa_link( $zona_detalii['denumire_zona'] ) . "/?optiuni=da&transport=avion&plecare-avion=" . fa_link( $v_localitati_plecare_avion_array ); ?>"
                           class="poza_aeroport NEW-round4px" rel="nofollow">
                            <div class="titlu">
								<?php echo $v_localitati_plecare_avion_array; ?>
                            </div>
                            <img class="image" src="<?php echo $last_pic; ?>"
                                 alt="<?php echo $v_localitati_plecare_avion_array ?>">
							<?php if ( $zona_detalii['denumire_zona'] ) : ?>
                                <div class="description">
                                    Sejur <?php echo $zona_detalii['denumire_zona'] ?><br/> plecare din
                                    <strong><?php echo $v_localitati_plecare_avion_array ?></strong>
                                </div>
							<?php endif; ?>
                        </a>
                    </div>
					<?php
				endforeach;
				?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<?php } ?>