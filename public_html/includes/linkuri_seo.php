<div class="seo-links">
<?php
if(($tara) and !($zona)) {
	$tari_lnks=get_tari($iduri,'','','','',$id_tara);
	if(sizeof($tari_lnks)>0) {
		foreach($tari_lnks as $key_lnks=>$value_lnks) {
			echo '<a href="'.$sitepath.'sejur-'.fa_link($tara).'/" class="link-blue" title="Oferte '.$value_lnks.'"><strong>Oferte '.$value_lnks.'</strong></a>';
		}
	}
	$zona_lnks=get_zone($iduri,'','',$id_tara,'',$id_zona);
	if(sizeof($zona_lnks)>0) {
		foreach($zona_lnks as $key_lnks=>$value_lnks) {
			echo '<a href="'.$sitepath.'sejur-'.fa_link($tara).'/'.fa_link($value_lnks).'/" class="link-blue" title="Oferte '.$value_lnks.'" style="margin-left:10px;">Oferte '.$value_lnks.'</a>';
		}
	}
}
elseif(($tara) and ($zona)) {
	$tari_lnks=get_tari($iduri,'','','','',$id_tara);
	if(sizeof($tari_lnks)>0) {
		foreach($tari_lnks as $key_lnks=>$value_lnks) {
			echo '<a href="'.$sitepath.'sejur-'.fa_link($tara).'/" class="link-blue" title="Oferte '.$value_lnks.'"><strong>Oferte '.$value_lnks.'</strong></a>';
		}
	}
}

if($zona) {
	$zona_lnks=get_zone($iduri,'','',$id_tara,'',$id_zona);
	if(sizeof($zona_lnks)>0) {
		foreach($zona_lnks as $key_lnks=>$value_lnks) {
			echo '<a href="'.$sitepath.'sejur-'.fa_link($tara).'/'.fa_link($value_lnks).'/" class="link-blue" title="Oferte '.$value_lnks.'" style="margin-left:10px;">Oferte '.$value_lnks.'</a>';
		}
	}
}

if($localitate) {
	echo '<a href="'.$sitepath.'sejur-'.fa_link($tara).'/'.fa_link($zona).'/" class="link-blue" title="Oferte '.ucwords($zona).'" style="margin-left:10px;">Oferte '.ucwords($zona).'</a>';
	$localitate_lnks=get_localitate($iduri,'',$id_zona,$id_tara,'',$id_localitate);
	if(sizeof($localitate_lnks)>0) {
		foreach($localitate_lnks as $key_lnks=>$value_lnks) {
			echo '<a href="'.$sitepath.'sejur-'.fa_link($tara).'/'.fa_link($zona).'/'.fa_link($value_lnks['denumire']).'/" class="link-blue" title="Oferte '.$value_lnks['denumire'].'" style="margin-left:10px;">Oferte '.$value_lnks['denumire'].'</a>';
		}
	}
}
?>
</div>
