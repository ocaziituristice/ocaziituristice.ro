 <p>Site-ul <?php echo $denumire_agentie; ?> utilizeaza cookie-uri.</p>
		  <p>Informatiile prezentate in continuare au scopul de a aduce la cunostinta utilizatorului mai multe detalii despre plasarea, utilizarea si administrarea &quot;cookie&quot;-urilor utilizate de site-ul <?php echo $denumire_agentie; ?>.</p>
		  <p>De asemenea, sunt prezente si cateva linkuri utile legate de acest subiect. In cazul in care aveti nevoie de mai multe informatii, si ele nu se regasesc mai jos, ne puteti contacta la: <?php echo $email_contact; ?>.</p>
		  <p>Va rugam sa cititi cu atentie informatiile care urmeaza:</li>
		  <p>Acest website foloseste cookie-uri (atat proprii cat si de la terti) pentru a furniza vizitatorilor o experienta mult mai buna de navigare si servicii adaptate nevoilor si interesului fiecaruia.</p>
		  <p>In ceea ce numim &quot;web 2.0&quot;, &quot;cookie&quot;-urile joaca un rol important in facilitarea accesului si livrarii multiplelor servicii de care utilizatorul se bucura pe internet, cum ar fi:</p>
		  
          <ul>
		    <li>Personalizarea anumitor setari precum: ofertele salvate in cos, ofertele vizitate, numarul de oferte pe pagina, pastrarea diverselor optiuni;</li>
		    <li>Cookie-urile ofera detinatorilor de site-uri un feedback valoros asupra modului cum sunt utilizate site-urile lor de catre utilizatori, astfel incat sa le poata face si mai eficiente si mai accesibile pentru utilizatori;</li>
		    <li>Permit aplicatiilor multimedia sau de alt tip de pe alte site-uri sa fie incluse intr-un anumit site pentru a crea o experienta de navigare mai valoroasa, mai utila si mai placuta;</li>
		    <li>Imbunatatesc eficienta publicitatii online.</li>
	      </ul>
		  <h2>Ce este un &quot;cookie&quot;?</h2>
          
		  <p>Un &quot;Internet Cookie&quot; (termen cunoscut si ca &quot;browser cookie&quot; sau &quot;HTTP cookie&quot; sau pur si simplu &quot;cookie&quot;) este un fisier de mici dimensiuni, format din litere si numere, care va fi stocat pe computerul, terminalul mobil sau alte echipamente ale unui utilizator de pe care se acceseaza Internetul.</p>
		  <p>Cookie-ul este instalat prin solicitarea emisa de catre un web-server unui browser (ex: Internet Explorer, Firefox, Chrome, Opera, Safari, etc.) si este complet &quot;pasiv&quot; (nu contine programe software, virusi sau spyware si nu poate accesa informatiile de pe hard driveul utilizatorului).</p>
		  <p>Un cookie este format din 2 parti: numele si continutul sau valoarea cookie-ului. Mai mult, durata de existenta a unui cookie este determinata; tehnic, doar webserverul care a trimis cookie-ul il poate accesa din nou in momentul in care un utilizator se intoarce pe website-ul asociat webserverului respectiv.</p>
		  <p>Cookie-urile in sine nu solicita informatii cu caracter personal pentru a putea fi utilizate si, in cele mai multe cazuri, nu identifica personal utilizatorii de internet.</p> 
         
          <h2>Exista 2 categorii mari de cookie-uri:</h2>
		  
          <p><strong>Cookieuri de sesiune</strong> - acestea sunt stocate temporar in dosarul de cookie-uri al browserului web pentru ca acesta sa le memoreze pana cand utilizatorul iese de pe web-siteul respectiv sau inchide fereastra browserului (ex: in momentul logarii/delogarii pe un cont de webmail sau pe retele de socializare).</p>
		  <p><strong>Cookieuri Persistente</strong> - acestea sunt stocate pe hard-drive-ul unui computer sau echipament (si in general depind de durata de viata prestabilita pentru cookie). Cookie-urile persistente le includ si pe cele plasate de un alt website decat cel pe care il viziteaza utilizatorul la momentul respectiv - cunoscute sub numele de 'third party cookies' (cookieuri plasate de terti) - care pot fi folosite in mod anonim pentru a memora interesele unui utilizator, astfel incat sa fie livrata publicitate cat mai relevanta pentru utilizatori.</p>
          
		  <h2>Care sunt avantajele cookie-urilor?</h3>
         
		  <p>Un cookie contine informatii care fac legatura intre un web-browser (utilizatorul) si un web-server anume (website-ul). Daca un browser acceseaza acel web-server din nou, acesta poate citi informatia deja stocata si reactiona in consecinta.</p>
		  <p>Cookie-urile asigura userilor o experienta placuta de navigare si sustin eforturile multor websiteuri pentru a oferi servicii confortabile utilizatorillor: ex - preferintele in materie de confidentialitate online, cosuri de cumparaturi sau publicitate relevanta.</p>
          
		  <h2>Care este durata de viata a unui cookie?</h2>
		  <p>Cookieurile sunt administrate de webservere. Durata de viata a unui cookie poate varia semnificativ, depinzand de scopul pentru care este plasat. Unele cookie-uri sunt folosite exclusive pentru o singura sesiune (session cookies) si nu mai sunt retinute odata de utilizatorul a parasite website-ul si unele cookie-uri sunt retinute si refolosite de fiecare data cand utilizatorul revine pe acel website ('cookie-uri permanente'). Cu toate acestea, cookie-urile pot fi sterse de un utilizator in orice moment prin intermediul setarilor browserului.</p>
          
		  <h2>Ce sunt cookie-urile plasate de terti?</h2>
          <ul>
		  <p>Anumite sectiuni de continut de pe unele site-uri pot fi furnizate prin intermediul unor terte parti/furnizori (ex: news box, un video sau o reclama). Aceste terte parti pot plasa de asemenea cookieuri prin intermediul site-ului si ele se numesc &quot;third party cookies&quot; pentru ca nu sunt plasate de proprietarul website-ului respectiv. Furnizorii terti trebuie sa respecte de asemenea legea in vigoare si politicile de confidentialitate ale detinatorului site-ului.</p>
          </ul>
		  <h2>Cum sunt folosite cookie-urile de catre acest site</h2>
		  <p>O vizita pe acest site poate plasa cookie-uri in scopuri de:</p>
		  <ul>
		    <li>Cookie-uri de performanta a site-ului</li>
		    <li>Cookie-uri de analiza a vizitatorilor</li>
		    <li>Cookie-uri pentru geotargetting</li>
		    <li>Cookie-uri de inregistrare</li>
		    <li>Cookie-uri pentru publicitate</li>
		    <li>Cookie-uri ale furnizorilor de publicitate</li>
		    <li>Aceste cookie-uri pot proveni de la urmatorii terti: Google Analytics, Facebook, AdOcean		  </li>
		  </ul>
		  <h2>Cookie-uri de performanta:</h2>
		  
          <p>Acest tip de cookie retine preferintele utilizatorului pe acest site, asa incat nu mai este nevoie de setarea lor la fiecare vizitare a site-ului.</p>
		  <h2>Cookie-uri pentru analiza vizitatorilor</h2>
		  <p>De fiecare data cand un utilizator viziteaza acest site softul de analytics furnizat de o terta parte genereaza un cookie de analiza a utilizatorului. Acest cookie ne spune daca ati mai vizitat acest site pana acum. Browserul ne va spune daca aveti acest cookie, iar daca nu, vom genera unul. Acesta permite monitorizarea utilizatorilor unici care ne viziteaza si cat de des o fac.</p>
		  <p>Atat timp cat nu sunteti inregistrat pe acest site, acest cookie nu poate fi folosit pentru a identifica persoanele fizice, ele sunt folosite doar in scop statistic. Daca sunteti inregistrat putem sti, de asemenea, detaliile pe care ni le-ati furnizat, cum ar fi adresa de e-mail si username-ul - acestea fiind supuse confidentialitatii si prevederilor din Termeni si Conditii, Politica de confidentialitate precum si prevederilor legislatiei in vigoare cu privire la protejarea datelor cu caracter personal. <br>
	      </p>
		  <h2>Cookie-uri pentru geotargetting</h2>
		  <p>Aceste cookie-uri sunt utilizate de catre un soft care stabileste din ce tara proveniti. Este complet anonim si este folosit doar pentru a targeta continutul.</p>
		  <h2>Cookie-uri pentru inregistrare</h2>
		  <p>Atunci cand va inregistrati pe acest site, generam un cookie care ne anunta daca sunteti inregistrat sau nu. Serverele noastre folosesc aceste cookie-uri pentru a ne arata contul cu care sunteti inregistrat si daca aveti permisiunea pentru un serviciu anume. De asemenea, ne permite sa asociem orice comentariu pe care il postati pe site-ul nostru cu username-ul dvs. Daca nu ati selectat &quot;pastreaza-ma inregistrat&quot;, acest cookie se va sterge automat cand veti inchide browserul sau calculatorul.</p>
		  <h2>Cookie-uri pentru publicitate</h2>
		  <p>Aceste cookie-uri ne permit sa aflam daca ati vizualizat sau nu o reclama online, care este tipul acesteia si cat timp a trecut de cand ati vazut mesajul publicitar.</p>
		  <p>Aceste cookie-uri le folosim si pentru a targeta publicitatea online. Putem folosi, de asemenea, cookieuri apartinand unei terte parti, pentu o mai buna targetare a publicitatii, pentru a arata de exemplu reclame despre vacante, daca utilizatorul a vizitat recent un articol pe site despre vacante. Aceste cookie-uri sunt anonime, ele stocheaza informatii despre contentul vizualizat, nu despre utilizatori.</p>
		  <p>De asemenea, setam cookie-uri anonime si prin alte site-uri pe care avem publicitate. Primindu-le, astfel, noi le putem folosi pentru a va recunoaste ca vizitator al acelui site daca ulterior veti vizita site-ul nostru, va vom putea livra publicitatea bazata pe aceasta informatie.</p>
		  <h2>Alte cookie-uri ale tertelor parti</h2>
		  <p>Pe unele pagini, tertii pot seta propriile cookie-uri anonime, in scopul de a urmari succesul unei aplicatii, sau pentru a customiza o aplicatie. Datorita modului de utilizare, acest site nu poate accesa aceste cookie-uri, la fel cum tertele parti nu pot accesa cookie-urile detinute de acest site.</p>
		  <p>De exemplu, cand distribuiti un articol folosind butonul pentru retelele sociale aflat pe acest site, acea retea sociala va inregistra activitatea dvs.</p>
		  <h2>Ce tip de informatii sunt stocate si accesate prin intermediul cookie-urilor?</h2>
		  <p>Cookie-urile pastreaza informatii intr-un fisier text de mici dimensiuni care permit unui website sa recunoasca un browser. Webserverul va recunoaste browserul pana cand cookie-ul expira sau este sters.</p>
		  <p>Cookie-ul stocheaza informatii importante care imbunatatesc experienta de navigare pe Internet (ex: setarile limbii in care se doreste accesarea unui site; pastrarea unui user logat in contul de webmail; securitatea online banking; pastrarea produselor in cosul de cumparaturi).</p>
		  <h2>De ce sunt cookie-urile importante pentru Internet?</h2>
		  <p>Cookie-urile reprezinta punctul central al functionarii eficiente a Internetului, ajutand la generarea unei experiente de navigare prietenoase si adaptata preferintelor si intereselor fiecarui utilizator. Refuzarea sau dezactivarea cookieurilor poate face unele site-uri imposibil de folosit.</p>
		  <p>Refuzarea sau dezactivarea cookie-urilor nu inseamna ca nu veti mai primi publicitate online - ci doar ca aceasta nu va mai putea tine cont de preferintele si interesele dvs, evidentiate prin comportamentul de navigare.</p>
		  <p>Cookieurile NU sunt virusi! Ele folosesc formate tip plain text. Nu sunt alcatuite din bucati de cod asa ca nu pot fi executate nici nu pot auto-rula. In consecinta, nu se pot duplica sau replica pe alte retele pentru a se rula sau replica din nou. Deoarece nu pot indeplini aceste functii, nu pot fi considerate virusi.</p>
		  <p>Cookie-urile pot fi totusi folosite pentru scopuri negative. Deoarece stocheaza informatii despre preferintele si istoricul de navigare al utilizatorilor, atat pe un anume site cat si pe mai multe alte siteuri, cookieurile pot fi folosite ca o forma de Spyware. Multe produse anti-spyware sunt constiente de acest fapt si in mod constant marcheaza cookie-urile pentru a fi sterse in cadrul procedurilor de stergere/scanare anti-virus/anti-spyware.</p>
		  <p>In general browserele au integrate setari de confidentialitate care furnizeaza diferite nivele de acceptare a cookieurilor, perioada de valabilitate si stergere automata dupa ce utilizatorul a vizitat un anumit site.</p>
		  <h2>Alte aspecte de securitate legate de cookie-uri</h2>
		  <p>Deoarece protectia identitatii este foarte valoroasa si reprezinta dreptul fiecarui utilizator de internet, este indicat sa se stie ce eventuale probleme pot crea cookieurile. Pentru ca prin intermediul lor se transmit in mod constant in ambele sensuri informatii intre browser si website, daca un atacator sau persoana neautorizata intervine in parcursul de transmitere a datelor, informatiile continute de cookie pot fi interceptate. Desi foarte rar, acest lucru se poate intampla daca browserul se conecteaza la server folosind o retea necriptata (ex: o retea WiFi nesecurizata).</p>
		  <p>Alte atacuri bazate pe cookie implica setari gresite ale cookieurilor pe servere. Daca un website nu solicita browserului sa foloseasca doar canale criptate, atacatorii pot folosi aceasta vulnerabilitate pentru a pacali browserele in a trimite informatii prin intermediul canalelor nesecurizate. Atacatorii utilizeaza apoi informatiile in scopuri de a accesa neautorizat anumite site-uri. Este foarte important sa fiti atenti in alegerea metodei celei mai potrivite de protectie a informatiilor personale.</p>
		  <h3>Sfaturi pentru o navigare sigura si responsabila, bazata pe cookies.</h3>
		  <p>Datorita flexibilitatii lor si a faptului ca majoritatea dintre cele mai vizitate site-uri si cele mai mari folosesc cookieuri, acestea sunt aproape inevitabile. Dezactivarea cookie-urilor nu va permite accesul utilizatorului pe site-urile cele mai raspandite si utilizate printre care Youtube, Gmail, Yahoo si altele.</p>
		  <p>Iata cateva sfaturi care va pot asigura ca navigati fara griji insa cu ajutorul cookieurilor:</p>
		  <p><strong>Particularizati-va setarile browserului in ceea ce priveste cookie-urile pentru a reflecta un nivel confortabil pentru voi al securitatii utilizarii cookie-urilor.</strong></p>
		  <p>Daca nu va deranjeaza cookie-urile si sunteti singura persoana care utilizeaza computerul, puteti seta termene lungi de expirare pentru stocarea istoricului de navigare si al datelor personale de acces.</p>
		  <p>Daca impartiti accesul la calculator, puteti lua in considerare setarea browserului pentru a sterge datele individuale de navigare de fiecare data cand inchideti browserul. Aceasta este o varianta de a accesa site-urile care plaseaza cookieuri si de a sterge orice informatie de vizitare la inchiderea sesiunii navigare.</p>
		  <p>Instalati-va si updatati-va constant aplicatii antispyware.</p>
		  <p>Multe dintre aplicatiile de detectare si prevenire a spyware-ului includ detectarea atacurilor pe site-uri.</p>
		  <p>Astfel, impiedica browserul de la a accesa website-uri care ar putea sa exploateze vulnerabilitatile browserului sau sa descarce software periculos. Asigurati-va ca aveti browserul mereu updatat. Multe dintre atacurile bazate pe cookies se realizeaza exploatand punctele slabe ale versiunilor vechi ale browserelor.</p>
		  <p>Cookie-urile sunt pretutindeni si nu pot fi evitate daca doriti sa va bucurati de acces pe cele mai bune si cele mai mari site-uri de pe Internet - locale sau internationale. Cu o intelegere clara a modului lor de operare si a beneficiilor pe care le aduc, puteti lua masurile necesare de securitate astel incat sa puteti naviga cu incredere pe internet.</p>
		  <h2>Cum pot opri cookie-urile?</h2>
		  <p>Dezactivarea si refuzul de a primi cookie-uri pot face anumite site-uri impracticabile sau dificil de vizitat si folosit. De asemenea, refuzul de a accepta cookie-uri nu inseamna ca nu veti mai primi/vedea publicitate online.</p>
		  <p>Este posibila setarea din browser pentru ca aceste cookie-uri sa nu mai fie acceptate sau poti seta browserul sa accepte cookie-uri de la un site anume. Dar, de exemplu, daca nu esti inregistat folosind cookie-urile, nu vei putea lasa comentarii.</p>
		  <p>Toate browserele moderne ofera posibilitatea de a schimba setarile cookie-urilor. Aceste setari se gasesc de regula in &quot;optiuni&quot; sau in meniul de &quot;preferinte&quot; al browserului tau.</p>
		  <p>Pentru a intelege aceste setari, urmatoarele linkuri pot fi folositoare, altfel puteti folosi optiunea &quot;ajutor&quot;
a browserului pentru mai multe detalii.</p>
		  <ul>
		    <li><a href="http://support.microsoft.com/kb/196955" class="link-blue" target="_blank">Cookie settings in Internet Explorer</a></li>
		    <li><a href="http://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer?redirectlocale=en-US&redirectslug=Cookies" class="link-blue" target="_blank">Cookie settings in Firefox</a></li>
		    <li><a href="http://support.google.com/chrome/bin/answer.py?hl=en&answer=95647" class="link-blue" target="_blank">Cookie settings in Chrome</a></li>
		    <li><a href="http://support.apple.com/kb/PH5042" class="link-blue" target="_blank">Cookie settings in Safari</a></li>
	      </ul>
		  <p>Pentru setarile cookie-urilor generate de terti, puteti consulta si site-ul <a href="http://www.youronlinechoices.com/ro/" class="link-blue" target="_blank">http://www.youronlinechoices.com/ro/</a> unde gasiti mai multe informatii privind confidentialitatea legata de publicitatea online.</p>