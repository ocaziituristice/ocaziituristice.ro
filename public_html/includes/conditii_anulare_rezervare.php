<?php 
if(count($conditii_anulare)>0)
{?>
<div class="bkg-white">
              <ul class="taxe-neincluse">
             
		<li class="first bigger-15em bold blue">Conditii de anulare :</li>
<?php foreach ($conditii_anulare as $a_key=>$a_value) 
	{
  if($a_value['valoare']=='0'){?>
	<span class="bkg-dark-green underline bigger-13em white pad5 w540"> Anulare <strong>GRATUITA</strong> pana la data de <?php echo denLuniRo(date('d F Y', strtotime($a_value['data_sfarsit'])));?></span> <br />
     <?php $camp_anulare='Anulare GRATUITA pana la data de '.denLuniRo(date('d F Y', strtotime($a_value['data_sfarsit']))).'\n';?>
	<?php   } else {
    ?>
	Pentru anularea rezervarii in perioada <strong><?php echo denLuniRo(date('d F Y', strtotime($a_value['data_inceput']))).' - '.denLuniRo(date('d F Y', strtotime($a_value['data_sfarsit'])));?></strong> penalizarea este de <strong><?php echo $a_value['valoare']; if($a_value['tip_valoare_anulare']=='procent'){echo '% din valoare totala a sejurului';}
    if($a_value['tip_valoare_anulare']=='suma_fixa'){echo '  ';} ?>
    
    
    </strong>
    <br/>
	 <?php $camp_anulare.='- Pentru anularea rezervarii in perioada '.denLuniRo(date('d F Y', strtotime($a_value['data_inceput']))).' - '.denLuniRo(date('d F Y', strtotime($a_value['data_sfarsit']))).' penalizarea este de <strong>'.	$a_value['valoare'].'% </strong> din valoare totala a sejurului \n';?>
	 
	<?php }?>
	
<?php	}?>

</ul>
           </div>  
<?php }

?>