<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/peste_tot.php" );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/functii_pt_afisare.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_hotel.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/afisare_sejur.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/class/class_sejururi/class_sejur.php' );
include_once( $_SERVER['DOCUMENT_ROOT'] . '/config/includes/suppliers_xml.php' );


/*** check login admin ***/
$logare_admin     = new LOGIN( 'useri' );
$err_logare_admin = $logare_admin->verifica_user();
/*** check login admin ***/

$id_oferta     = $_REQUEST['id_oferta'];
$det           = new DETALII_SEJUR();
$detalii       = $det->select_det_sejur( $id_oferta );
$preturi       = $det->select_preturi_sejur( $id_oferta, '', '' );
$id_hotel      = $detalii['id_hotel'];
$detalii_hotel = $det->select_camp_hotel( $id_hotel );

$furnizor    = get_detalii_furnizor( $detalii['furnizor'] );
$id_furnizor = $detalii['furnizor'];

$pleczile     = $_REQUEST['pleczile'];
$plecnopti    = $pleczile - 1;
$plecdata     = $_REQUEST['plecdata'];
$arivdata     = date( 'Y-m-d', strtotime( $plecdata . ' + ' . $plecnopti . ' days' ) );
$adulti       = $_REQUEST['adulti'];
$copii        = $_REQUEST['copii'];
$copil1       = $_REQUEST['copil1'];
$copil2       = $_REQUEST['copil2'];
$copil3       = $_REQUEST['copil3'];
$copil_age[0] = $copil1;
$copil_age[1] = $copil2;
$copil_age[2] = $copil3;
$orasplec     = $_REQUEST['orasplec'];
if ( $detalii['transport'] == 'Avion' ) {
	$detalii['cazare'] = 'nu';
}

$rezcam = array();

switch ( $id_furnizor ) {
	case '2':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/p45.php' );
		break;
	case '52':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/malta.php' );
		break;
	case '1':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/teztour.php' );
		break;
	case '398':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/bibi_touring.php' );
		break;
	case '330':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/paradis.php' );
		break;
	case '397':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/aerotravel.php' );
		break;
	case '225':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/prestige.php' );
		break;
	case '15':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/accent.php' );
		break;
	case '22':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/cocktails.php' );
		break;
	case '6':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/christian.php' );
		break;
	
	
case '349':
		include( $_SERVER['DOCUMENT_ROOT'] . '/includes/preturi_online/karpaten.php' );	
	break;		
}
if ( ! $err_logare_admin ) {
//	echo '<pre>';
//	print_r( $rezcam );
//	echo '</pre>';
}

//array_multisort($rezcam['pret']);
if ( sizeof( $rezcam ) > 0 ) {
	/*$rezcam = multid_sort($rezcam, 'pret');
	$rezcam = array_map('unserialize', array_unique(array_map('serialize', $rezcam)));*/

	$sort = array();
	foreach ( $rezcam as $key_sort => $value_sort ) {
		$sort['disponibilitate'][ $key_sort ] = $value_sort['disponibilitate'];
		$sort['pret'][ $key_sort ]            = $value_sort['pret'];
	}
	array_multisort( $sort['disponibilitate'], SORT_ASC, $sort['pret'], SORT_ASC, $rezcam );
}
//if($_SESSION['mail']=="daniel@ocaziituristice.ro") { echo '<pre>';print_r($rezcam);echo '</pre>'; }

echo '<div class="camere bkg-green">';
if ( sizeof( $rezcam ) > 0 and $arivdata <= $preturi['max_end'] ) {

	foreach ( $rezcam as $key_rezcam => $value_rezcam ) {
		if ( strlen( $rezcam[ $key_rezcam ]['nr_nights'] ) > 0 ) {
			$pleczile = $rezcam[ $key_rezcam ]['nr_nights'];
		}

		if ( isset( $detalii['discount_tarif'] ) ) {
			$discount_tarif = $detalii['discount_tarif'];
		} else {
			$discount_tarif = $furnizor['comision_procent'];
		}
		$rezcam[ $key_rezcam ]['oferta_pret'] = $rezcam[ $key_rezcam ]['oferta_pret'] * $discount_tarif;
		$rezcam[ $key_rezcam ]['pret']        = $rezcam[ $key_rezcam ]['pret'] * $discount_tarif;

		echo '<div class="clearfix">';
		echo '<div class="detalii col-md-5"';
		if ( moneda( $rezcam[ $key_rezcam ]['moneda'] ) == 'RON' ) {
			echo ' style="width:300px;"';
		}
		echo '>';
		echo '<div class="ocupanti">';
		for ( $i1 = 0; $i1 < $rezcam[ $key_rezcam ]['nr_adulti']; $i1 ++ ) {
			echo '<img src="/images/people_01_m.png" alt="adult">';
		}
		for ( $i2 = 0; $i2 < $rezcam[ $key_rezcam ]['nr_copii']; $i2 ++ ) {
			$copii = array(
				'0-2',
				'0-2.00',
				'0.00-2',
				'0.00-2.00',
				'0-1,99',
				'0.00-1,99',
				'0-1.99',
				'0.00-1.99',
				'0',
				'1'
			);
			if ( array_search( $rezcam[ $key_rezcam ][ 'copil' . ( $i2 + 1 ) ], $copii ) != false ) {
				echo '<img src="/images/people_03_m.png" alt="infant">';
			} else {
				echo '<img src="/images/people_02_m.png" alt="copil">';
			}
		}
		echo '</div>';
		echo '<div class="bigger-12em bold denumire-camera">' . preg_replace( "/\{{([^\]]+)\}}/", "", $rezcam[ $key_rezcam ]['denumire_camera'] ) . ' - <span class="blue">' . change_meal( $rezcam[ $key_rezcam ]['masa'] ) . '</span></div>';
		echo $rezcam[ $key_rezcam ]['nr_adulti'];
		echo $rezcam[ $key_rezcam ]['nr_adulti'] < 2 ? ' adult' : ' adulti';
		if ( $rezcam[ $key_rezcam ]['copil1'] ) {
			echo ' + copil 1 (<strong>' . $rezcam[ $key_rezcam ]['copil1'] . ' ani</strong>)';
		}
		if ( $rezcam[ $key_rezcam ]['copil2'] ) {
			echo ' + copil 2 (<strong>' . $rezcam[ $key_rezcam ]['copil2'] . ' ani</strong>)';
		}
		if ( $rezcam[ $key_rezcam ]['copil3'] ) {
			echo ' + copil 3 (<strong>' . $rezcam[ $key_rezcam ]['copil3'] . ' ani</strong>)';
		}
		if ( strlen( $rezcam[ $key_rezcam ]['oferta'] ) > 0 ) {
			echo '<div class="red italic bold">' . $rezcam[ $key_rezcam ]['oferta'] . '</div>';
		}
		if ( strlen( $rezcam[ $key_rezcam ]['series_name'] ) > 0 ) {
			echo '<div class="italic">' . $rezcam[ $key_rezcam ]['series_name'] . '</div>';
		}
		if ( strlen( $rezcam[ $key_rezcam ]['period'] ) > 0 ) {
			echo '<div class="black">Perioada: <span class="blue bold bigger-12em">' . $rezcam[ $key_rezcam ]['period'] . '</span></div>';
		}
		echo '</div>';
		echo '<div class="col-md-1" style="float: left;"></div>';
		echo '<div class="pret col-md-3">';
		echo 'Tarif/camera ' . $pleczile . ' nopti';
		if ( $rezcam[ $key_rezcam ]['oferta_pret'] ) {
			$oldprice = round( $rezcam[ $key_rezcam ]['oferta_pret'], 2 );
			echo '<span class="old-price blue">' . $oldprice . ' ' . moneda( $rezcam[ $key_rezcam ]['moneda'] ) . '</span>';
		} else {
			$oldprice = 0;
		}
		echo '<span class="val red">' . new_price( $rezcam[ $key_rezcam ]['pret'] ) . ' ' . moneda( $rezcam[ $key_rezcam ]['moneda'] ) . '</span>';
		echo '</div>';
		echo '<div class="col-md-3 float-left">';
		echo '<div class="float-right" style="text-align: center">';
		echo '<div class="avl clearfix">';
		if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'disponibil' ) {
			echo '<i class="fa fa-check-circle text-success"></i> ';
			echo '<span class="badge badge-success">Disponibil</span>';
		} elseif ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' ) {
			echo '<i class="fa fa-check-circle text-primary"></i> ';
			echo '<span class="badge badge-success" style="background-color: #065abf">Necesita confirmare</span>';
		} elseif ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'stopsales' ) {
			echo '<i class="fa fa-minus-circle text-danger"></i> ';
			echo '<span class="badge badge-danger">Indisponibil</span><br />';
		}
		echo '</div>';
		if ( $rezcam[ $key_rezcam ]['disponibilitate'] == 'disponibil' or $rezcam[ $key_rezcam ]['disponibilitate'] == 'request' ) {
			echo '<a href="' . make_link_oferta( $detalii_hotel['localitate'], $detalii_hotel['denumire'], null, null ) . 'rrezervare-' . $id_oferta . '_' . base64_encode( $rezcam[ $key_rezcam ]['id_pret'] . '_' . $rezcam[ $key_rezcam ]['plecare'] . '_' . $_REQUEST['plecdata'] . '_' . $_REQUEST['pleczile'] . '_' . $rezcam[ $key_rezcam ]['pret'] . '_' . $rezcam[ $key_rezcam ]['id_camera'] . '_' . $rezcam[ $key_rezcam ]['nr_adulti'] . '_' . $rezcam[ $key_rezcam ]['nr_copii'] . '_' . $rezcam[ $key_rezcam ]['copil1'] . '_' . $rezcam[ $key_rezcam ]['copil2'] . '_' . $rezcam[ $key_rezcam ]['copil3'] . '_' . $detalii['masa'] . '_' . $rezcam[ $key_rezcam ]['denumire_camera'] . '_' . $rezcam[ $key_rezcam ]['comision'] . '_' . $oldprice . '_' . $rezcam[ $key_rezcam ]['disponibilitate'] . '_' . $rezcam[ $key_rezcam ]['data_early_booking'] . '_' . str_replace( '_', '*', $rezcam[ $key_rezcam ]['PackageVariantId'] ) . '_' . $rezcam[ $key_rezcam ]['RoomCode'] . '_' . time() ) . '" class="btn btn-primary full_width_mobile" onclick="ga(\'send\', \'event\', \'pagina oferta\', \'rezerva oferta\', \'' . $detalii['denumire'] . '\');">rezerva acum</a>';


			echo '<a href="/ofertele-mele/?data=' . base64_encode( $id_oferta . '_' . $rezcam[ $key_rezcam ]['id_pret'] . '_' . $rezcam[ $key_rezcam ]['plecare'] . '_' . $rezcam[ $key_rezcam ]['data_start'] . '_' . $pleczile . '_' . $rezcam[ $key_rezcam ]['pret'] . '_' . $rezcam[ $key_rezcam ]['id_camera'] . '_' . $rezcam[ $key_rezcam ]['nr_adulti'] . '_' . $rezcam[ $key_rezcam ]['nr_copii'] . '_' . $rezcam[ $key_rezcam ]['copil1'] . '_' . $rezcam[ $key_rezcam ]['copil2'] . '_' . $rezcam[ $key_rezcam ]['copil3'] . '_' . change_meal( $rezcam[ $key_rezcam ]['masa'] ) . '_' . $rezcam[ $key_rezcam ]['denumire_camera'] ) . '" rel="nofollow" class="addToCart" title="Adauga oferta pentru comparare" onClick="ga(\'send\', \'event\', \'pagina oferta\', \'adauga in cos\', \'' . $detalii['denumire'] . '\');">Adauga la comparatie</a>';
		}
		echo '</div>';

		echo '</div>';
		echo '</div>';

	}



	foreach ( $flight as $fly ) {
		if ( $fly['flight_bookable'] == 1 and sizeof( $fly['plecare'] ) > 0 ) {
			echo '<table class="search-rooms mar10-0"><tr class="bkg-white"><th></th><th class="text-center">Companie</td><th class="text-center">Nr. cursa</th><th class="text-center">Plecare</th><th class="text-center">Sosire</th></tr>';
			foreach ( $fly['plecare'][0] as $k_cursa_out => $v_cursa_out ) {
				echo '<tr class="bkg-grey">';
				echo '<td class="text-center"><img src="/images/oferte/icon_small_transport_avion.png" alt="Autocar"></td>';
				echo '<td class="text-center">';
				if ( strlen( get_airline_by_iata( $v_cursa_out['companie'] ) ) > 0 ) {
					echo '<img src="/images/avion/' . get_airline_by_iata( $v_cursa_out['companie'] ) . '.jpg" alt=""> ';
				}
				echo '</td>';
				echo '<td class="text-center">' . $v_cursa_out['nr_cursa'] . '</td>';
				echo '<td class="text-center"><strong>' . $v_cursa_out['from'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_out['data_plecare'] ) ) . '</td>';
				echo '<td class="text-center"><strong>' . $v_cursa_out['to'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_out['data_sosire'] ) ) . '</td>';
				//if($v_cursa_out['seats']) echo ' - '.$v_cursa_out['seats'].' locuri';
				echo '</tr>';
			}
			foreach ( $fly['intoarcere'][0] as $k_cursa_in => $v_cursa_in ) {
				echo '<tr class="bkg-white">';
				echo '<td class="text-center"><img src="/images/oferte/icon_small_transport_avion.png" alt="Autocar"></td>';
				echo '<td class="text-center">';
				if ( strlen( get_airline_by_iata( $v_cursa_in['companie'] ) ) > 0 ) {
					echo '<img src="/images/avion/' . get_airline_by_iata( $v_cursa_in['companie'] ) . '.jpg" alt=""> ';
				}
				echo '</td>';
				echo '<td class="text-center">' . $v_cursa_in['nr_cursa'] . '</td>';
				echo '<td class="text-center"><strong>' . $v_cursa_in['from'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_in['data_plecare'] ) ) . '</td>';
				echo '<td class="text-center"><strong>' . $v_cursa_in['to'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_in['data_sosire'] ) ) . '</td>';
				//if($v_cursa_in['seats']) echo ' - '.$v_cursa_in['seats'].' locuri';
				echo '</tr>';
			}
			echo '</table>';
		}
	}

	if ( $bus['bus_bookable'] == 1 and sizeof( $bus['plecare'] ) > 0 ) {
		echo '<table class="search-rooms mar10-0"><tr class="bkg-white"><th></th><th class="text-center">Plecare</th><th class="text-center">Sosire</th><th class="text-center">Locuri</th></tr>';
		foreach ( $bus['plecare'] as $k_av_out => $v_av_out ) {
			foreach ( $v_av_out as $k_cursa_out => $v_cursa_out ) {
				echo '<tr class="bkg-grey">';
				echo '<td class="text-center"><img src="/images/oferte/icon_small_transport_autocar.png" alt="Autocar"></td>';
				echo '<td class="text-center"><strong>' . $v_cursa_out['from'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_out['data_plecare'] ) ) . '</td>';
				echo '<td class="text-center"><strong>' . $v_cursa_out['to'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_out['data_sosire'] ) ) . '</td>';
				if ( $v_cursa_out['seats'] ) {
					echo '<td class="text-center"><strong>' . $v_cursa_out['seats'] . '</strong> locuri</td>';
				}
				echo '</tr>';
			}
		}
		foreach ( $bus['intoarcere'] as $k_av_in => $v_av_in ) {
			foreach ( $v_av_in as $k_cursa_in => $v_cursa_in ) {
				echo '<tr class="bkg-white">';
				echo '<td class="text-center"><img src="/images/oferte/icon_small_transport_autocar.png" alt="Autocar"></td>';
				echo '<td class="text-center"><strong>' . $v_cursa_in['from'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_in['data_plecare'] ) ) . '</td>';
				echo '<td class="text-center"><strong>' . $v_cursa_in['to'] . '</strong> - ' . date( "d.m.Y - H:i", strtotime( $v_cursa_in['data_sosire'] ) ) . '</td>';
				if ( $v_cursa_in['seats'] ) {
					echo '<td class="text-center"><strong>' . $v_cursa_in['seats'] . '</strong> locuri</td>';
				}
				echo '</tr>';
			}
		}
		echo '</table>';
	}

} else {
	//include_once($_SERVER['DOCUMENT_ROOT']."/includes/track_failed_selections.php");
	$fara_rezervare = 'da';
	echo '<div class="clearfix">';
	echo '<div class="bkg-white pad10">';
	echo '<span class="bigger-11em bold">Pentru ' . $adulti . ' adult';
	if ( $adulti > 1 ) {
		echo 'i';
	}
	if ( $copii > 0 ) {
		echo ' si ' . $copii . ' copi';
		if ( $copii == 1 ) {
			echo 'l';
		} else {
			echo 'i';
		}
	}
	if ( $adulti + $copii >= 4 ) {
		echo ' va rugam sa incercati <span class="blue">o alta repartizare in camera</span> (mai putine persoane) sau puteti sa completati o <span class="blue">cerere pentru mai multe detalii</span> <span class="italic smaller-09em">(click pe butonul de mai jos)</span>.';
	} else {
		echo ' va rugam sa ne <span class="blue">contactati telefonic</span> ori sa completati o <span class="blue">cerere pentru mai multe detalii</span></span> <span class="italic">(click pe butonul de mai jos)</span>.';
	}
	/*echo '<strong class="bigger-12em">Nu am gasit nici o camera disponibila pentru aceasta combinatie.</strong><br><br>';
	echo '- Va rugam sa incercati o <strong>alta combinatie</strong> sau sa ne <strong>contactati telefonic</strong> ori sa completati o <strong>cerere pentru mai multe detalii</strong>.';*/
	echo '</div>';
	echo '</div>';
}
echo '</div>';
?>
<?php if ( $fara_rezervare != 'da' ) { ?>
	<?php if ( sizeof( $detalii['denumire_v1'] ) > '0' ) { ?>
        <div class="right-serv clearfix">
            <h3 class="black underline">Servicii incluse </h3>
            <ul>
				<?php foreach ( $detalii['denumire_v1'] as $key => $value ) { ?>
                    <li><?php echo schimba_caractere( ucfirst( $value ) );
						if ( $detalii['value_v1'][ $key ] ) {
							echo ' - <strong>' . $detalii['value_v1'][ $key ] . ' ' . moneda( $detalii['moneda_v1'][ $key ] ) . '</strong>';
						} ?></li>
				<?php } ?>
            </ul>
        </div>
	<?php } ?>
<?php } ?>

<?php if ( $fara_rezervare == 'da' ) {
	//include_once($_SERVER['DOCUMENT_ROOT']."/includes/track_failed_selections.php");
	echo '<br>';
	include_once( $_SERVER['DOCUMENT_ROOT'] . "/includes/sejururi/cerere_detalii.php" );

	if ( ! is_bot() and isset( $err_logare_admin ) and $detalii['valabila'] == 'da' ) {
		erori_disponibilitate( $id_oferta, $id_hotel, $plecdata, $pleczile, $adulti, $copii );
	}

} ?>
