<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii.php');
//require_once($_SERVER['DOCUMENT_ROOT'].'/config/recaptchalib.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/send_mail.php');
$id_oferta=$_GET['oferta'];
$id_hotel=$_GET['hotel'];
$GLOBALS['make_vizualizata']='nu';
$det= new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$detalii_hotel=$det->select_camp_hotel($id_hotel);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');
$luna=array(1=>'Ianuarie', 2=>'Februarie', 3=>'Martie', 4=>'Aprilie', 5=>'Mai', 6=>'Iunie', 7=>'Iulie', 8=>'August', 9=>'Septembrie', 10=>'Octombrie', 11=>'Noiembrie', 12=>'Decembrie');
if($_POST['trimite']) {
$err=0;
$err_nume='';
if(strlen(trim($_POST['nume']))<3) { ++$err; $err_nume='Campul nume este obligatoriu!'; }
if(strlen(trim($_POST['telefon']))<4) { ++$err; $err_tel='Campul telefon este obligatoriu!'; }
if(strlen(trim($_POST['email']))<5) { ++$err; $err_email='Campul E-mail este obligatoriu!'; }
if(strlen(trim($_POST['prenume']))<3) { ++$err; $err_prenume='Campul prenume este obligatoriu!'; }
elseif(validate_email($_POST['email'])==false) { ++$err; $err_email='Adresa de email completata nu este corecta!'; }
if(strlen(trim($_POST['perioada']))<4) { ++$err; $err_perioada='Nu a-ti completat data de incepere a circuitului!'; }
if(strlen(trim($_POST['nr_nopti']))<1) { ++$err; $err_nopti='Campul Nr. nopti este obligatoriu!'; }
/*$resp=recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
if(!$resp->is_valid) { ++$err; $err_cod='Codul este invalid'; }*/

if(!$err) {
  $date_time=date("Y-m-d G:i:s");	
  $nume=$_POST['nume'];
  $email=$_POST['email'];
  $nr_adulti=$_POST['nr_adulti'];
  $nr_copii=$_POST['nr_copii'];
  $mr_tel=$_POST['telefon'];
  $camera=$_POST['camera'];
  $nr_nopti=$_POST['nr_nopti'];
  $perioada=$_POST['perioada'];
  $observatii=$_POST['observatii'];
  $prenume=$_POST['prenume'];
  $data_nasterii=$_POST['an'].'-'.$_POST['luna'].'-'.$_POST['ziua'];
  $data_nasterii_afisare=$_POST['ziua'].' '.$luna[$_POST['luna']].' '.$_POST['an'];
  if(sizeof($_POST['varsta'])>0) {
	foreach($_POST['varsta'] as $key=>$value) $varsta=$varsta.'Copil '.$key.' ani impliniti '.$value.'<br/>';  
  }
  foreach($_POST['asigurare'] as $key1=>$vale1) $asigurare=$asigurare.$vale1.',';
  $sol="insert into rezervari set id_circuit = '".$id_oferta."', camera = '".$camera."', sex = '".$_POST['sex']."', nume = '".$nume."', prenume = '".$prenume."', email = '".$email."', telefon = '".$mr_tel."', nr_adulti = '".$nr_adulti."', nr_copii = '".$nr_copii."', nr_nopti = '".$nr_nopti."', asigurare = '".$asigurare."', perioada = '".$perioada."', observatii = '".$observatii."', data_adaugarii = '".$date_time."', ip = '".$_SERVER['REMOTE_ADDR']."', copii_varsta = '".$varsta."', data_nasterii = '".$data_nasterii."', data_nasterii_afisare = '".$data_nasterii_afisare."' ";
  if($que=mysql_query($sol) or die(mysql_error())) {
	if($_POST['sex']=='m') $tit='Domnul '; else $tit='Doamna ';
	$GLOBALS['nume']=$tit.$nume.' '.$_POST['prenume'];
	$GLOBALS['email']=$email;
	$GLOBALS['data_ora']=$date_time;  
	$GLOBALS['denumire_oferta']=$detalii['denumire'];
	$GLOBALS['data_nasterii_afisare']=$data_nasterii_afisare;
	$GLOBALS['link']=$sitepath.'circuit/'/*.fa_link($detalii_hotel['nume_continent']).'/'*/.fa_link($detalii_hotel['denumire']).'-'.$id_oferta.'.html';
	$GLOBALS['perioada']=$perioada;
	$GLOBALS['nr_adulti']=$nr_adulti;
	if($nr_copii) { if($nr_copii>1) $t='copii'; else $t='copil'; $GLOBALS['nr_copii']=' si <b>'.$nr_copii.'</b> '.$t.'<br/>'.$varsta; } else $GLOBALS['nr_copii']='';
	$GLOBALS['telefon']=$mr_tel;
	$GLOBALS['observatii']=$observatii;
	$GLOBALS['sigla']=$GLOBALS['path_sigla'];
	$send_m= new SEND_EMAIL;
	//raspuns_client
	$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/rezervare.htm';
    $param['subject']='Rezervare '.$detalii['denumire'];
    $param['to_email']=$email;
    $param['to_nume']=$nume.' '.$_POST['prenume'];
    $param['fr_email']=$GLOBALS['email_rezervare'];
    $param['fr_nume']=$GLOBALS['denumire_agentie'];
	$send_m->send($param);
	//email_agentie
	$param['templates']=$_SERVER['DOCUMENT_ROOT'].'/mail/templates/mail_agentie_rezervare.htm';
    $param['subject']='Rezervare '.$detalii['denumire'];
    $param['to_email']=$GLOBALS['email_rezervare'];
    $param['to_nume']=$GLOBALS['denumire_agentie'];
    $param['fr_email']=$email;
    $param['fr_nume']=$nume.' '.$_POST['prenume'];
	$send_m->send($param);
  }
 }
} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Rezervare sejur <?php echo $detalii['denumire']; ?></title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>
<body>
<h2 class="green">Rezervare <?php echo $detalii_hotel['denumire']; ?></h2>
<div>
&nbsp;&nbsp; 
<strong>Transport:</strong> <?php echo $detalii['transport']; ?>
&nbsp;&nbsp; | &nbsp;&nbsp; 
<strong>Continent:</strong> <?php echo $detalii_hotel['nume_continent']; ?>
</div>
<div class="cerere-detalii NEW-round8px clearfix" style="width:540px;">
<?php if(!$_POST['trimite'] || $err>0) { ?>
<form action="" method="post" name="contactForm">
<h2 class="green">Date contact</h2>
<div class="item clearfix">
  <div class="left">&nbsp;</div>
  <div class="right"><label><input type="radio" name="sex" value="m" style="width:10px" <?php if(!$_POST['sex'] || $_POST['sex']=='m') { ?> checked="checked" <?php } ?>/>Domnul</label>&nbsp;&nbsp;&nbsp;<label><input type="radio" name="sex" value="f" style="width:10px" <?php if($_POST['sex']=='f') { ?> checked="checked" <?php } ?> />Doamna</label></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="nume" class="titlu">* Nume</label></div>
  <div class="right"><input name="nume" id="nume" type="text" value="<?php if($_POST['nume']) echo $_POST['nume']; ?>" class="big" />
  <?php if($err_nume) { ?><label class="error"><?php echo $err_nume; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="prenume" class="titlu">* Prenume</label></div>
  <div class="right"><input name="prenume" id="prenume" type="text" value="<?php if($_POST['prenume']) echo $_POST['prenume']; ?>" class="big" />
  <?php if($err_prenume) { ?><label class="error"><?php echo $err_prenume; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="email" class="titlu">* E-mail</label></div>
  <div class="right"><input name="email" id="email" type="text" value="<?php if($_POST['email']) echo $_POST['email']; ?>" class="big" />
  <?php if($err_email) { ?><label class="error"><?php echo $err_email; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label for="telefon" class="titlu">* Telefon</label></div>
  <div class="right"><input name="telefon" id="telefon" type="text" value="<?php if($_POST['telefon']) echo $_POST['telefon']; ?>" class="big" />
  <?php if($err_tel) { ?><label class="error"><?php echo $err_tel; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label class="titlu">Data nasterii</label></div>
  <div class="right" style="width:330px;">
    &nbsp;&nbsp; Zi <select name="ziua"><?php for($i=1;$i<=31;$i++) { echo "<option value='$i'"; if($_POST['ziua']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
    Luna <?php ?> <select name="luna"><?php for($i=1;$i<=12;$i++) { echo "<option value='$i'"; if($_POST['luna']==$i) echo "selected='selected'"; echo ">".$luna[$i]."</option>"; } ?></select>
    An <?php ?> <select name="an"><?php for($i=date("Y");$i>=1900;$i--) { echo "<option value='$i'"; if($_POST['an']==$i) echo "selected='selected'"; echo ">$i</option>"; } ?></select>
  </div>
</div>
<h2 class="green">Detalii rezervare</h2>
<div class="item clearfix">
  <div class="left"><label class="titlu">Nr. persoane</label></div>
  <div class="right">
    &nbsp;&nbsp; Adulti: <select name="nr_adulti"><?php for($i=1;$i<=10;$i++) { ?><option value="<?php echo $i; ?>" <?php if($_POST['nr_adulti']==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option><?php } ?></select>
    &nbsp;&nbsp;Copii: <select name="nr_copii" onchange="if(this.value>0) { var cont=''; for(var i=1; i<=this.value; i++) { cont=cont+'<p>Copil '+i+' ani impliniti: <select name=\'varsta['+i+']\'>'; for(var j=1; j<=18; j++) cont=cont+'<option value=\''+j+'\'>'+j+'</option>'; cont=cont+'</select></p>'; } document.getElementById('copii').innerHTML=cont; }"><?php for($i=0;$i<=10;$i++) { ?><option value="<?php echo $i; ?>" <?php if($_POST['nr_copii']==$i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option><?php } ?></select>
    <div id="copii"><?php if(sizeof($_POST['varsta'])>0) {
foreach($_POST['varsta'] as $key=>$value) {  echo '<p>Copil '.$key.' ani impliniti: <select name="varsta['.$key.']">';
for($j=1;$j<=18;$j++) { echo '<option value="'.$j.'"'; if($j==$value) echo 'selected="selected"'; echo '>'.$j.'</option>'; } echo '</select></p>'; }
 } ?></div>
  </div>
</div>

<?php if($preturi['camera']>0) { ?>
<div class="item clearfix">
  <div class="left"><label class="titlu">Camera</label></div>
  <div class="right" style="width:330px;">
	<?php foreach($preturi['camera'] as $key_cam=>$value_cam) { ?>
    <label class="check"><input type="radio" name="camera" value="<?php echo $key_cam; ?>" <?php if($_POST['camera']==$key_cam) { ?> checked="checked" <?php } ?>/><?php echo $value_cam; ?></label>
    <?php } ?>
  </div>
</div>
<div class="item clearfix">
  <div class="left"><label class="titlu" for="observatii_camera">Observatii Camera</label></div>
  <div class="right"><input type="text" id="observatii_camera" name="observatii_camera" <?php if($_POST['observatii_camera']) echo $_POST['observatii_camera']; ?> class="big" /></div>
</div>
<div class="item clearfix">
  <div class="left"><label class="titlu" for="perioada">* Data inceput circuit</label></div>
  <div class="right"><input type="text" name="perioada" id="perioada" value="<?php if($_POST['perioada']) echo $_POST['perioada']; ?>" class="small" />
  <?php if($err_perioada) { ?><label class="error"><?php echo $err_perioada; ?></label><?php } ?></div>
</div>
<?php } ?>
<div class="item clearfix">
  <div class="left"><label class="titlu" for="nr_nopti">* Nr. nopti</label></div>
  <div class="right"><input type="text" name="nr_nopti" id="nr_nopti" <?php if($detalii['nr_nopti']>=2) { ?> readonly="readonly" <?php } ?> value="<?php if($_POST['nr_nopti']) echo $_POST['nr_nopti']; elseif($detalii['nr_nopti']>=2) echo $detalii['nr_nopti']; ?>" class="small" />
  <?php if($err_nopti) { ?><label class="error"><?php echo $err_nopti; ?></label><?php } ?></div>
</div>
<div class="item clearfix">
  <div class="left"><label class="titlu">Asigurare de sanatate</label></div>
  <div class="right">
	<?php $loc="SELECT id_asigurare, denumire from asigurari Group by id_asigurare Order by denumire ";
    $qury_loc=mysql_query($loc) or die(mysql_error()); ?>
    <label class="check"><input name="asigurare[]" type="checkbox" value="nu"<?php if(($_POST['asigurare'] && in_array('nu', $_POST['asigurare'])) || !$_POST['asigurare']) { ?> checked="checked" <?php } ?> /> Nu doresc asigurare</label><br />
    <?php while($row=mysql_fetch_array($qury_loc)) { ?>
    <label class="check"><input type="checkbox" name="asigurare[]" value="<?php echo $row['denumire']; ?>" <?php if($_POST['asigurare']) if(in_array($row['denumire'], $_POST['asigurare'])) { ?> checked="checked" <?php } ?>> <?php echo $row['denumire']; ?></label><br />
    <?php } @mysql_free_result($que); ?>
  </div>
</div>
<div class="item clearfix">
  <div class="left"><label for="observatii" class="titlu">Observatii</label></div>
  <div class="right"><textarea name="observatii" id="observatii" class="big"><?php if($_POST['observatii']) echo $_POST['observatii']; ?></textarea></div>
</div>
<?php /*?><div>
  <label class="titlu">Cod de siguranta</label>
  <?php echo recaptcha_get_html($publickey);
  if($err_cod) { ?><label class="error"><?php echo $err_cod; ?></label><?php } ?>
</div><?php */?>
<div class="item clearfix">
  <div class="left"><input name="trimite" type="hidden" value="trimite" /></div>
  <div class="right"><input type="image" src="<?php echo $imgpath; ?>/buton_trimite.gif" value="Trimite" class="send" /></div>
</div>
</form>
<?php } else { ?>
Rezervarea dumneavoastra a fost inregistrata<br />
In cel mai scurt timp veti fi contactat in legatura cu rezervarea efectuata.
<br /><br />
Va multumim,<br />
Echipa ocaziituristice.ro
<?php } ?>
</div>

</body>
</html>