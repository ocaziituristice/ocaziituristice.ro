<?php

require __DIR__ . '/../../phalcon/app/legacy.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

ob_start();
header('Content-type: text/html; charset=UTF-8');

if ($_SERVER['PHP_SELF'] <> '/erori/404.php') {
    $_SESSION['ultima_pagina'] = $_SERVER['REQUEST_URI'];
}

$timeout = time() + (60 * 60 * 24 * 5);
//if(!$_COOKIE['nr_pe_pagina']) setcookie('nr_pe_pagina', 10, $timeout);
if ($_SERVER['PHP_SELF'] == '/index.php') {
    if ($_SERVER['REQUEST_URI'] <> '/') {
        //header("HTTP/1.1 301 Moved Permanently");
        header(sprintf("Location: %", $_SERVER['HTTP_HOST']));
        exit();
    }
}

include_once __DIR__ . '/../config/functii.php';
include_once __DIR__ . '/../config/includes/class/mysql.php';
include_once __DIR__ . '/../config/includes/class/send_mail.php';
include_once __DIR__ . '/../config/includes/class/login.php';

$logare_admin = new LOGIN('useri');
$err_logare_admin = $logare_admin->verifica_user();
