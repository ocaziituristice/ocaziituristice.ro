<table style="width:100%; border:none; border-collapse:collapse;">
  <tr>
    <td class="text-left">
      <br />
      <ul>
        <li>Navigaţi pe site folosind meniul, sistemul de filtrare din coloana stângă, sau alte referinţe utile din site, până ajungeţi la ceea ce doriţi dvs.</li>
        <li>După ce v-aţi decis asupra unei oferte, apăsaţi butonul <strong>&quot;Rezervă&quot;</strong> ce se găseşte în dreptul fiecărei oferte, completaţi detaliile cerute şi apăsaţi <strong>&quot;Trimite&quot;</strong>. Apoi, veţi primi imediat pe email o copie a rezervării dvs.</li>
        <li>Rezervarea trimisă de dvs. va fi preluată în cel mai scurt timp de către unul dintre operatorii Ocazii Turistice care vă va contacta pentru a vă confirma rezervarea sau a vă oferi o alternativă în cazul în care nu mai sunt locuri la hotelul unde aţi trimis cererea de rezervare.</li>
        <li>După stabilirea detaliilor cu operatorul Ocazii Turistice, se va încheia un contract în baza căruia se va emite o factură proformă.</li>
      </ul>
    </td>
    <td class="text-left nowrap">
      <h2 class="blue underline">Modalități de plată</h2>
      <img src="<?php echo $imgpath; ?>/img_pay_transfer.jpg" alt="Plata prin transfer bancar" title="Plata prin transfer bancar" width="80" style="margin:10px 0;" />
      <img src="<?php echo $imgpath; ?>/img_pay_pos.jpg" alt="Plata cu cardul la sediul agentiei" title="Plata cu cardul la sediul agentiei" width="80" style="margin:10px 0;" /><br />
      <img src="<?php echo $imgpath; ?>/img_pay_cash.jpg" alt="Plata in numerar" title="Plata in numerar" width="80" style="margin:10px 0;" />
      <img src="<?php echo $imgpath; ?>/img_pay_creditcard.jpg" alt="Plata cu cardul online" title="Plata cu cardul online" width="80" style="margin:10px 0;" />
    </td>
  </tr>
</table>
