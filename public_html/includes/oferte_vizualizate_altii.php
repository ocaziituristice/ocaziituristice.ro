<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_hotel.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/afisare_sejur.php');
include($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');

$id_oferta = $_GET['idOf'];

$det = new DETALII_SEJUR();
$detalii=$det->select_det_sejur($id_oferta);
$preturi=$det->select_preturi_sejur($id_oferta, '', '');
$id_hotel=$detalii['id_hotel'];
$detalii_hotel=$det->select_camp_hotel($id_hotel);

$viewers = '';
$sel_views1 = "SELECT id_sesiune_vizitator FROM oferte_vizitate WHERE id_oferta = '".$id_oferta."' ";
$que_views1 = mysql_query($sel_views1) or die(mysql_error());
if(mysql_num_rows($que_views1)>0) {
while($row_views1 = mysql_fetch_array($que_views1)) {
	$viewers .= "'".$row_views1['id_sesiune_vizitator']."',";
}
$viewers = substr($viewers,0,-1);

$sel_views = "SELECT *
FROM oferte_vizitate
WHERE id_sesiune_vizitator IN (".$viewers.")
AND id_oferta <> '".$id_oferta."'
AND id_sesiune_vizitator <> '".session_id()."'
";
$que_views = mysql_query($sel_views) or die(mysql_error());

if(mysql_num_rows($que_views)>0) {
$id_oferte_viz = '';
while($row_views = mysql_fetch_array($que_views)) {
	$id_oferte_viz .= $row_views['id_oferta'].',';
}
$id_oferte_viz = substr($id_oferte_viz,0,-1);

$selOf="SELECT
oferte.id_oferta,
oferte.denumire,
oferte.denumire_scurta,
oferte.id_oferta,
oferte.nr_zile,
oferte.nr_nopti,
oferte.exprimare_pret,
oferte.pret_minim,
oferte.moneda,
oferte.masa,
transport.denumire AS denumire_transport,
hoteluri.nume,
hoteluri.stele,
hoteluri.id_hotel,
hoteluri.tip_unitate,
hoteluri.poza1,
zone.denumire as denumire_zona,
localitati.denumire as denumire_localitate,
tari.denumire as denumire_tara,
continente.nume_continent
FROM oferte
INNER JOIN hoteluri ON oferte.id_hotel = hoteluri.id_hotel
INNER JOIN localitati ON hoteluri.locatie_id = localitati.id_localitate
INNER JOIN zone ON localitati.id_zona = zone.id_zona
INNER JOIN tari ON zone.id_tara = tari.id_tara
INNER JOIN transport ON transport.id_trans=oferte.id_transport
LEFT JOIN continente ON hoteluri.id_continent = continente.id_continent
WHERE oferte.valabila = 'da'
AND oferte.id_oferta IN (".$id_oferte_viz.")
ORDER BY rand()
LIMIT 0,3 ";
$queOf=mysql_query($selOf) or die(mysql_error());

if(mysql_num_rows($queOf)) {
?>
<div class="NEW-column-full" style="margin-top:10px; padding:0;">
  <div class="other-views NEW-round8px clearfix">
    <div class="titlu">Clienții care au văzut <span class="red bold"><?php echo $detalii_hotel['denumire']; ?></span>
    <?php if($detalii_hotel['tip_unitate']=='Circuit') {} else {
		echo 'din <a href="/sejur-'.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/" class="link-blue">'.$detalii_hotel['zona'].'</a> / <a href="/sejur-'.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['zona']).'/'.fa_link($detalii_hotel['localitate']).'/" class="link-blue">'.$detalii_hotel['localitate'].'</a>';
	} ?>
    au fost interesați și de:</div>
<?php
while($rowOf = mysql_fetch_array($queOf)) {
	if($rowOf['poza1']) $poza = '/img_mediu_hotel/'.$rowOf['poza1'];
	else $poza = '/images/no_photo.jpg';
	
	if($rowOf['tip_unitate']=='Circuit') {
		$denumire = $rowOf['nume'];
		$stele = '';
		$localizare = $rowOf['nume_continent'].' / '.$rowOf['nr_zile'].' zile';
		$link = '/circuit/'/*.fa_link($rowOf['nume_continent']).'/'*/.fa_link_oferta($denumire).'-'.$rowOf['id_oferta'].'.html';
	} else {
		$denumire = $rowOf['nume'];
		$stele = '<span class="stele-mici-'.$rowOf['stele'].'"></span>';
		$localizare = $rowOf['denumire_zona']; if($rowOf['denumire_localitate']<>$localizare) $localizare = $localizare.' / '.$rowOf['denumire_localitate'];
		$link = make_link_oferta($rowOf['denumire_localitate'], $rowOf['nume'], $rowOf['denumire_scurta'], $rowOf['id_oferta']);
	}

	$sel_plecare = "SELECT GROUP_CONCAT(DISTINCT localitati.denumire ORDER BY oferte_transport_avion.ordonare SEPARATOR ', ') AS loc_plecare
	FROM oferte_transport_avion
	LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
	LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
	WHERE oferte_transport_avion.id_oferta = '".$rowOf['id_oferta']."'
	AND oferte_transport_avion.tip = 'dus' ";
	$que_plecare = mysql_query($sel_plecare) or die(mysql_error());
	$row_plecare = mysql_fetch_array($que_plecare);
?>
    <div class="oferta NEW-round8px clearfix">
      <div class="titlul"><a href="<?php echo $link; ?>" title="<?php echo $denumire; ?>" class="link-blue"><?php echo $denumire; ?></a> <?php echo $stele; ?></div>
      <a href="<?php echo $link; ?>" class="image NEW-round8px" style="background-image:url(<?php echo $poza; ?>);" rel="nofollow"></a>
      <div class="left">
	    <p><?php echo $localizare; ?></p>
        <!--<p><?php //echo '<strong>'.$rowOf['denumire_transport'].'</strong>';
		//if($row_plecare['loc_plecare']) echo ' - plecare '.$row_plecare['loc_plecare']; ?>
        </p>-->
      </div>
 
      <br class="clear">
      <a href="<?php echo $link; ?>" class="btn-detalii" rel="nofollow"><img src="/images/but_nou_detalii.png" alt="vezi detalii"></a>
    </div>
<?php } @mysql_free_result($queOf); ?>
  </div>
</div>
<?php }
}
} ?>