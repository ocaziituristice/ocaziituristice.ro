<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" align="justify">
      <br />
      <ul>
        <li><strong>Reducerile</strong> de tarif de pe site-ul  www.ocaziituristice.ro se aplica doar rezervarilor efectuate prin intermediul  formularului de rezervare online de pe site si care urmeaza a fi achitate in  totalitate.</li>
        <li>Dupa achitarea in totalitate a facturii proforme emise de unul dintre operatorii nostri, veti primi un cupon de <strong>REDUCERE in valoare de 3%</strong> din valoarea incasata de noi (nu sunt incluse taxele suplimentare: taxa de aeroport, taxa de statiune, taxa de reprezentare etc).</li>
        <li><strong>Reducerile</strong> nu se acorda biletelor de avion achizitionate  de pe sectiunea <a href="http://www.bileteavion.ocaziituristice.ro" target="_blank">www.bileteavion.ocaziituristice.ro</a> si nici biletelor de avion  ale curselor de linie.</li>
        <li><strong>Cuponul de reducere</strong> apartine persoanei pe numele  careia se va face factura si care este si titulara contractului.</li>
        <li><strong>Reducerile</strong> primite in urma folosirii unui cupon  de reducere se aplica de catre operatorii nostri dupa validarea manuala a  acestora si a conditiilor de aplicare.</li>
        </ul>
    </td>
    <td align="left" valign="top" class="nowrap">
     
      <img src="<?php echo $imgpath; ?>/reduceri.jpg" alt="Reduceri preturi" hspace="20" title="Plata prin transfer bancar" width="150" />
    
    </td>
  </tr>
</table>
