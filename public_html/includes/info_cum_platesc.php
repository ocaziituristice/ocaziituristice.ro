<div class="popdown-content" style="padding:20px">
<p class="bigger-13em"><strong>Puteţi plăti serviciile comandate de dvs. prin următoarele modalităţi:</strong></p>
<ul>
  <li><strong class="blue bigger-12em">Numerar</strong><br />
    Puteţi plăti în numerar în <strong>RON</strong> sau <strong>EUR</strong> la sediul Agenţiei de turism Dream Voyage International, <?php echo $contact_adresa; ?><br />
    <strong>Program de lucru:</strong> <?php echo $contact_program; ?>
  </li>
  <li><strong class="blue bigger-12em">Transfer bancar</strong><br />
    Mai jos aveţi datele necesare pentru efectuarea transferului bancar:<br />
    <br />
    <strong><?php echo $contact_legal_den_firma; ?></strong><br /><br />
    Cod fiscal: <?php echo $contact_legal_cui; ?><br /><br />
    Adresa: <?php echo $contact_legal_adresa; ?><br />
    <br />
    Banca <?php echo $contact_legal_banca; ?><br />
    RON: <?php echo $contact_legal_cont_lei; ?><br />
    EUR: <?php echo $contact_legal_cont_euro; ?>
  </li>
  <li><strong class="blue bigger-12em">Cu cardul online</strong><br />
    Plata se va face prin intermediul site-ului <a href="http://www.mobilpay.ro/" target="_blank" class="link-blue">www.mobilpay.ro</a> - dacă optaţi pentru această variantă, veţi primi un link securizat şi personalizat pentru achitarea facturii<br />
    <strong>IMPORTANT!</strong> Pe extrasul de cont va apărea ca operator <strong>PlatiOnline</strong> sau <strong>OnlinePay</strong> şi nu Ocazii Turistice sau Dream Voyage International
  </li>
  <li><strong class="blue bigger-12em">Cu cardul la sediul agenţiei</strong><br />
    Puteţi efectua plata şi la sediul agenţiei Dream Voyage International prin POS<br />
    <strong>Program de lucru:</strong> <?php echo $contact_program; ?>
  </li>
</ul>
</div>
