<?php if(($detalii['cazare']=='da' and $detalii['online_prices']=='da') or ($_SERVER['PHP_SELF']=="/detalii_hotel_new.php") or ($detalii['new_layout']=='da')) { ?>
<script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
<script>
function initialize() {
	<?php if($rowH['latitudine']!='') { ?>
		var mapDiv = document.getElementById('harta-localizare');
		var map = new google.maps.Map(mapDiv, {
			center: new google.maps.LatLng(<?php echo $rowH['latitudine']; ?>, <?php echo $rowH['longitudine']; ?>),
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		
		var content = '<a href="<?php echo make_link_oferta($detalii_hotel['localitate'], $detalii_hotel['denumire'], NULL, NULL); ?>" title="<?php echo $detalii_hotel['denumire']; ?>" class="info-window" style="background-image:url(/img_mediu_hotel/<?php echo $detalii_hotel['poza1']; ?>);"><span class="bkg"><span><?php echo $detalii_hotel['denumire'].' '.$stars; ?></span></span></a>';
		
		var infowindow = new google.maps.InfoWindow({
			content: content
		});
		
		var marker = new google.maps.Marker({
			map: map,
			position: map.getCenter()
		});
		
		infowindow.open(map, marker);
		
		google.maps.event.addListener(map, "idle", function(){
			marker.setMap(map);
		});
	<?php } ?>
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php } else { ?>
<script>
$(function(){
	$('#ofTabs').tabs({
		collapsible: false,
		<?php /*?>selected: <?php echo $uitabs_selected; ?>,<?php */?>
		activate: function(event, ui){
			<?php if($rowH['latitudine']!='') { ?>
			if (ui.newPanel.attr('id') == 'harta' && $('#harta-localizare').is(':empty')) {
				var mapDiv = document.getElementById('harta-localizare');
				var map = new google.maps.Map(mapDiv, {
					center: new google.maps.LatLng(<?php echo $rowH['latitudine']; ?>, <?php echo $rowH['longitudine']; ?>),
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				
				var content = '<h4 class="blue"><?php echo $rowH['nume']; ?> <img src="/images/spacer.gif" class="stele-mici-<?php echo $rowH['stele']; ?>" alt="" /></h4><img src="<?php echo $sitepath_parinte; ?>thumb_hotel/<?php echo $rowH['poza1']; ?>" style="float:left; margin-right:10px; width:60px;" /><?php echo $rowH['adresa'].' Nr. '. $rowH['nr'].', '.$rowH['localitate'].', '.$rowH['tara']; ?>';
				
				var infowindow = new google.maps.InfoWindow({
					content: content
				});
				
				var image = '/images/icon_maps_hotel.png';
				var marker = new google.maps.Marker({
					map: map,
					position: map.getCenter(),
					icon: image
				});
				
				infowindow.open(map, marker);
				
				google.maps.event.addListener(map, "idle", function(){
					marker.setMap(map);
				});
			}
			<?php } ?>
		},
		selected: (location.hash && parseInt(location.hash.substr(1)) != 'NaN') ? parseInt(location.hash.substr(1)) : 0
	});
});
</script>
<?php } //end cazare, online_prices ?>
