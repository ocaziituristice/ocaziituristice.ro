<div id="NEW-destinatie">

<h1 class="red">Rezervare <?php echo $detalii['denumire']; ?></h1>
  
<div class="NEW-column-left1">

  <div class="clearfix" style="padding:0 10px;">
    <img src="<?php echo $sitepath; ?>thumb_hotel/<?php echo $detalii_hotel['poza1']; ?>" width="100" alt="" class="images" style="margin-right:10px;" />
    <h2 class="blue"><?php echo $detalii_hotel['denumire']; ?> <img src="<?php echo $imgpath; ?>/spacer.gif" class="stele-mici-<?php echo $detalii_hotel['stele']; ?>" alt="" /></h2>
    <div class="float-right"><a href="" class="link-blue">&laquo; inapoi la oferta</a></div>
    <p><?php echo $detalii_hotel['tara']; ?> / <?php echo $detalii_hotel['zona']; ?> / <?php echo $detalii_hotel['localitate']; ?></p>
      <?php if($detalii['nr_zile']>2 || $detalii['nr_nopti']>1) { ?><p><strong>Durata <?php if(isset($detalii['nr_zile'])) { if($detalii['nr_zile']>1) echo $detalii['nr_zile'].' zile'; else echo $detalii['nr_zile'].' zi'; } ?> / <?php if(isset($detalii['nr_nopti'])) { if($detalii['nr_nopti']>1) echo $detalii['nr_nopti'].' nopti'; else echo $detalii['nr_nopti'].' noapte'; } ?></strong></p><?php } ?>
    <p><strong>Transport <?php echo $detalii['transport']; ?></strong></p>
    <div class="cod-oferta"><strong class="grey">Cod Oferta:</strong> <span class="blue"><?php echo 'OSE'.$id_oferta; ?></span></div>
    <p><strong>Masa <?php echo $detalii['masa']; ?></strong></p>
  </div>
  
  <br class="clear" /><br />

<?php
$sel1_useri_fizice = "SELECT * FROM useri_fizice WHERE id_useri_fizice = '".$_GET['usr']."'";
$que1_useri_fizice = mysql_query($sel1_useri_fizice) or die(mysql_error());
$row1_useri_fizice = mysql_fetch_array($que1_useri_fizice);

$sel1_cerere_rezervare = "SELECT * FROM cerere_rezervare WHERE id_cerere = '".$_GET['rez']."'";
$que1_cerere_rezervare = mysql_query($sel1_cerere_rezervare) or die(mysql_error());
$row1_cerere_rezervare = mysql_fetch_array($que1_cerere_rezervare);
?>
  
  <form action="" method="post" name="contactForm">
  <h2 class="green">Rezervare - Pasul 1</h2>
  
  <div style="padding-left:15px;">Toate campurile marcate cu <strong class="red">*</strong> sunt obligatorii!</div>

  <div class="NEW-rezervare NEW-round8px clearfix">
    <div class="item bigger-13em clearfix" style="font-weight:bold;">
      <h3 class="red underline" style="margin:0 0 10px 0;">Detalii client</h3>
      <p><?php if($row1_useri_fizice['sex']=='m') echo 'Domnul'; elseif($row1_useri_fizice['sex']=='f') echo 'Doamna'; ?> <span class="blue"><?php echo $row1_useri_fizice['prenume']; ?> <?php echo $row1_useri_fizice['nume']; ?></span> <em>(<?php echo $row1_useri_fizice['email']; ?>)</em></p>
      <p>Telefon: <?php echo $row1_useri_fizice['telefon']; ?><br />
      Data nasterii: <?php echo date("d.m.Y",strtotime($row1_useri_fizice['data_nasterii'])); ?></p>
      <br />
      <h3 class="red underline" style="margin:0 0 10px 0;">Detalii oferta</h3>
      <p>Data inceperii sejurului: <span class="blue"><?php echo date("d.m.Y",strtotime($row1_cerere_rezervare['data'])); ?></span></p>
      <p>Nr. nopti: <span class="blue"><?php echo $row1_cerere_rezervare['nr_nopti'].' nopti'; ?></span></p>
    </div>
  </div>
  
  <a name="pas2"></a>
  <h2 class="green">Rezervare - <span class="blue">Pasul 2</span></h2>

  <div class="NEW-rezervare NEW-round8px clearfix">
    <div class="item clearfix">
    </div>
  </div>
  
  <p style="padding-left:25px;"><label><input name="termeni_conditii" type="checkbox" value="da" <?php if ($_POST['termeni_conditii']=='da') echo " checked=\"checked\"" ?>/> Sunt de acord cu <a href="<?php echo $sitepath; ?>termeni_si_conditii.html" target="_blank" class="infos2 link-blue" rel="nofollow">termenii si conditiile</a> de utilizare a site-ului.</label><?php if($err_termeni) { ?><br/><label class="error"><?php echo $err_termeni; ?></label><?php } ?></p>
  <input name="trimite" type="hidden" value="pas2" />
  <p style="padding-left:25px;"><input type="submit" value="Continua rezervarea - Pasul 3" /></p>
  <br class="clear" /><br />
  </form>
</div>

<div class="NEW-column-right1">
</div>

<br class="clear" />

</div>