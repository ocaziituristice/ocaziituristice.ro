<script type="text/javascript">
//<![CDATA[
 function filtrare(tip)
 {
    <!--$('#loader').show();-->
	var paglink = '<?php if($_REQUEST['tip']) echo "/oferte-".$_REQUEST['tip']."/"; else echo "/sejur-"; ?>';
	var tara = document.getElementById('ftara').value;
	if(tip=='tara') { if(tara!='') paglink=paglink+tara+'/'; else paglink='/oferte-turism-extern/';
	} else if(tip=='zona') {
	var zona = document.getElementById('fzona').value;
	if(zona!='') paglink=paglink+tara+'/'+zona+'/'; else paglink=paglink+tara+'/';
	} else {
	var zona = document.getElementById('fzona').value;	
	var localitate = document.getElementById('flocalitate').value;
	if(localitate!='') paglink=paglink+tara+'/'+zona+'/'+localitate+'/'; else paglink=paglink+tara+'/'+zona+'/';
	}
	
	document.location.href=paglink;
 }
//]]>
</script> 
<?php 
if($_REQUEST['tari']) $tara=desfa_link($_REQUEST['tari']);
if($_REQUEST['zone']) $zona=desfa_link($_REQUEST['zone']);
if($_REQUEST['oras']) $oras=desfa_link($_REQUEST['oras']);
if($_REQUEST['tip']) { $tip=desfa_link($_REQUEST['tip']); $id_tip=get_id_tip_sejur($tip); } 
?>
<div class="filtrare">
  <h3 class="titlu-mare"><?php if($tip) echo ucwords($tip)." ";
   if($oras) echo ucwords($oras); elseif($zona) echo ucwords($zona); else echo ucwords($tara); if(!$tip) echo " cautare avansata:"; else echo ":"; ?></h3>
<?php if($_REQUEST['tari']<>'romania') { ?>
	Pentru o navigare mai usoara folositi sistemul de filtrare de mai jos:
  <div class="new-filter filtNormal" onmouseout="this.className='new-filter filtNormal';" onmouseover="this.className='new-filter filtHover';">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="left" align="left" valign="middle">Tara:</td>
        <td class="right">
        <?php $sel_tara="select
		tari.denumire
        FROM
		oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		WHERE
		oferte.valabila =  'da' and tari.denumire is not null and tari.id_tara <> '1' and hoteluri.tip_unitate <> 'Circuit' ";
		if($_REQUEST['tip'])  $sel_tara=$sel_tara." and oferta_sejur_tip.id_tip_oferta in (".$iduri.") and oferte.last_minute = 'nu' ";
		$sel_tara=$sel_tara."
		Group by tari.id_tara
		Order by tari.denumire ";
		$que_tara=mysql_query($sel_tara) or die(mysql_error()); ?>
          <select name="ftara" id="ftara" onchange="filtrare('tara');">
            <option value="">Selecteaza</option>
            <?php while($row_tara=mysql_fetch_array($que_tara)) { ?>
            <option value="<?php echo fa_link($row_tara['denumire']); ?>" <?php if($_REQUEST['tari']==fa_link($row_tara['denumire'])) { ?> selected="selected" <?php } ?>><?php echo $row_tara['denumire']; ?></option>
            <?php } @mysql_free_result($que_tara); ?>
          </select>
        </td>
      </tr>
    </table>
  </div>
<?php } else { ?>
<input type="hidden" name="ftara" id="ftara" value="<?php echo $_REQUEST['tari']; ?>" />
<?php }
if($tara) { ?>
  <div class="new-filter filtNormal" onmouseout="this.className='new-filter filtNormal';" onmouseover="this.className='new-filter filtHover';">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="left" align="left" valign="middle">Zona:</td>
        <td class="right">
        <?php $sel_zona="select
		zone.denumire
        FROM
		oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		WHERE
		oferte.valabila =  'da' and zone.denumire is not null and hoteluri.tip_unitate <> 'Circuit' and TRIM(LOWER(tari.denumire)) = '".$tara."' and oferte.last_minute = 'nu' ";
		if($_REQUEST['tip'])  $sel_zona=$sel_zona." and oferta_sejur_tip.id_tip_oferta in (".$iduri.") ";
		$sel_zona=$sel_zona."
		Group by zone.id_zona
		Order by zone.denumire ";
		$que_zone = mysql_query($sel_zona) or die(mysql_error()); ?>
          <select name="fzona" id="fzona" onchange="filtrare('zona');">
            <option value="">Selecteaza</option>
            <?php while($row_zone=mysql_fetch_array($que_zone)) { ?>
            <option value="<?php echo fa_link($row_zone['denumire']); ?>" <?php if($zona==trim(strtolower($row_zone['denumire']))) { ?> selected="selected" <?php } ?>><?php echo $row_zone['denumire']; ?></option>
            <?php } @mysql_free_result($que_zone); ?>
          </select>
        </td>
      </tr>
    </table>
  </div>
<?php } else { ?> <input type="hidden" name="fzona" id="fzona" value="" /><?php } ?>
<?php if($zona) { ?>
  <div class="new-filter filtNormal" onmouseout="this.className='new-filter filtNormal';" onmouseover="this.className='new-filter filtHover';">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="left" align="left" valign="middle">Localitate:</td>
        <td class="right">
        <?php $sel_localitate="select
		localitati.denumire
        FROM
		oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		WHERE
		oferte.valabila =  'da' and localitati.denumire is not null and TRIM(LOWER(tari.denumire)) = '".$tara."' and hoteluri.tip_unitate <> 'Circuit' and TRIM(LOWER(zone.denumire)) = '".$zona."' and oferte.last_minute = 'nu' ";
		if($_REQUEST['tip'])  $sel_localitate=$sel_localitate." and oferta_sejur_tip.id_tip_oferta in (".$iduri.") ";
		$sel_localitate=$sel_localitate."
		Group by localitati.id_localitate
		Order by localitati.denumire ";
		$que_localitate = mysql_query($sel_localitate) or die(mysql_error()); ?>
          <select name="flocalitate" id="flocalitate" onchange="filtrare('localitate');">
            <?php if(mysql_num_rows($que_localitate)>'1') { ?><option value="">Selecteaza</option><?php } ?>
            <?php while($row_localitate=mysql_fetch_array($que_localitate)) { ?>
            <option value="<?php echo fa_link($row_localitate['denumire']); ?>" <?php if($oras==trim(strtolower($row_localitate['denumire']))) { ?> selected="selected" <?php } ?>><?php echo $row_localitate['denumire']; ?></option>
            <?php } @mysql_free_result($que_localitate); ?>
          </select>
        </td>
      </tr>
    </table>
  </div>
<?php } else { ?> <input type="hidden" name="flocalitate" id="flocalitate" value="" /><?php } ?>
<form method="post" name="filtreaza" action="">
<?php if($_POST['tip_pret']) { ?> <input type="hidden" name="tip_pret" value="<?php echo $_POST['tip_pret']; ?>" /> <?php } ?>
<?php if($_POST['tip_stele'] && !$_POST['stele']) { ?> <input type="hidden" name="tip_stele" value="<?php echo $_POST['tip_stele']; ?>" /> <?php } ?>
<?php if($_POST['tip_relevanta']) { ?> <input type="hidden" name="tip_relevanta" value="<?php echo $_POST['tip_relevanta']; ?>" /> <?php } ?>
<input type="hidden" name="den_ord" value="<?php echo $_POST['den_ord']; ?>" />

  <div class="new-filter filtNormal" onmouseout="this.className='new-filter filtNormal';" onmouseover="this.className='new-filter filtHover';">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="left" align="left" valign="middle">Transport:</td>
        <td class="right">
        <?php $sel_transport="SELECT
		transport.denumire
		FROM
		oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		Left Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
		Inner Join transport ON oferte.id_transport = transport.id_trans ";
		if($_REQUEST['localitate_plecare']) $sel_transport=$sel_transport." left join oferta_sejur_orase on oferte.id_oferta = oferta_sejur_orase.id_oferta
		left join localitati as localitati_plecare on oferta_sejur_orase.id_oras = localitati_plecare.id_localitate ";
		$sel_transport=$sel_transport."
		WHERE
		oferte.valabila =  'da' and hoteluri.tip_unitate <> 'Circuit' and oferte.last_minute = 'nu' ";
		if($_REQUEST['tip'])  $sel_transport=$sel_transport." and oferta_sejur_tip.id_tip_oferta in (".$iduri.") ";
		if($tara) $sel_transport=$sel_transport." and TRIM(LOWER(tari.denumire)) = '".$tara."' ";
		if($zona) $sel_transport=$sel_transport." and TRIM(LOWER(zone.denumire)) = '".$zona."' ";
		if($oras) $sel_transport=$sel_transport." and TRIM(LOWER(localitati.denumire)) = '".$oras."' ";
		if($_REQUEST['localitate_plecare']) $sel_transport=$sel_transport." and TRIM(LOWER(localitati_plecare.denumire)) = '".desfa_link($_REQUEST['localitate_plecare'])."' ";
		if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') $sel_transport=$sel_transport." and oferte.masa = '".desfa_link($_REQUEST['masa'])."' ";
		if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') $sel_transport=$sel_transport." and hoteluri.stele = '".$_REQUEST['stele']."' ";
		$sel_transport=$sel_transport."
		GROUP BY transport.id_trans
		ORDER BY transport.denumire ASC ";
	$que_transport=mysql_query($sel_transport) or die(mysql_error()); ?>
          <select name="transport" onchange="$('#loader').show(); document.filtreaza.submit();">
           <?php if(mysql_num_rows($que_transport)>'1') { ?><option value="">Selecteaza</option><?php } ?>
            <?php while($row_transport=mysql_fetch_array($que_transport)) { ?>
            <option value="<?php echo fa_link($row_transport['denumire']); ?>" <?php if($_REQUEST['transport']==fa_link($row_transport['denumire'])) { ?> selected="selected" <?php } ?>><?php echo $row_transport['denumire']; ?></option>
          <?php } @mysql_free_result($que_transport); ?>
          </select>
        </td>
      </tr>
    </table>
  </div>

  <div class="new-filter filtNormal" onmouseout="this.className='new-filter filtNormal';" onmouseover="this.className='new-filter filtHover';">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="left" align="left" valign="middle">Tip masa:</td>
        <td class="right">
        <?php  $sel_masa="SELECT
		oferte.masa
		FROM
		oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta 
		left Join transport ON oferte.id_transport = transport.id_trans ";
		if($_REQUEST['localitate_plecare']) $sel_masa=$sel_masa."
		left join oferta_sejur_orase on oferte.id_oferta = oferta_sejur_orase.id_oferta
		left join localitati as localitati_plecare on oferta_sejur_orase.id_oras = localitati_plecare.id_localitate ";
		$sel_masa=$sel_masa."
		WHERE
		oferte.valabila =  'da' and length(oferte.masa)>'3' and hoteluri.tip_unitate <> 'Circuit' and oferte.last_minute = 'nu' ";
		if($_REQUEST['tip'])  $sel_masa=$sel_masa." and oferta_sejur_tip.id_tip_oferta in (".$iduri.") ";
		if($tara) $sel_masa=$sel_masa." and TRIM(LOWER(tari.denumire)) = '".$tara."' ";
		if($zona) $sel_masa=$sel_masa." and TRIM(LOWER(zone.denumire)) = '".$zona."' ";
		if($oras) $sel_masa=$sel_masa." and TRIM(LOWER(localitati.denumire)) = '".$oras."' ";
		if($_REQUEST['localitate_plecare']) $sel_masa=$sel_masa." and TRIM(LOWER(localitati_plecare.denumire)) = '".desfa_link($_REQUEST['localitate_plecare'])."' ";
		if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') $sel_masa=$sel_masa." and hoteluri.stele = '".$_REQUEST['stele']."' ";
		if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $sel_masa=$sel_masa." and TRIM(LOWER(transport.denumire)) = '".desfa_link($_REQUEST['transport'])."' ";
		$sel_masa=$sel_masa."
		GROUP BY oferte.masa
		ORDER BY oferte.masa ASC ";
	$que_masa=mysql_query($sel_masa) or die(mysql_error()); ?>
          <select name="masa" onchange="$('#loader').show(); document.filtreaza.submit();">
          <?php if(mysql_num_rows($que_masa)>'1') { ?><option value="">Selecteaza</option><?php } ?>
            <?php while($row_masa=mysql_fetch_array($que_masa)) { ?>
            <option value="<?php echo fa_link($row_masa['masa']); ?>" <?php if(desfa_link($_REQUEST['masa'])==desfa_link($row_masa['masa'])) { ?> selected="selected" <?php } ?>><?php echo $row_masa['masa']; ?></option>
            <?php } @mysql_free_result($que_masa); ?>
          </select>
        </td>
      </tr>
    </table>
  </div>

  <div class="new-filter filtNormal" onmouseout="this.className='new-filter filtNormal';" onmouseover="this.className='new-filter filtHover';">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="left" align="left" valign="middle">Nr. stele hotel:</td>
        <td class="right">
       <?php $sel_stele="SELECT
		hoteluri.stele
		FROM
		oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		left Join transport ON oferte.id_transport = transport.id_trans ";
		if($_REQUEST['localitate_plecare']) $sel_stele=$sel_stele."
		left join oferta_sejur_orase on oferte.id_oferta = oferta_sejur_orase.id_oferta
		left join localitati as localitati_plecare on oferta_sejur_orase.id_oras = localitati_plecare.id_localitate ";
		$sel_stele=$sel_stele."
		WHERE
		oferte.valabila =  'da' and hoteluri.stele > '0' and hoteluri.tip_unitate <> 'Circuit' and oferte.last_minute = 'nu' ";
		if($_REQUEST['tip'])  $sel_stele=$sel_stele." and oferta_sejur_tip.id_tip_oferta in (".$iduri.") ";
		if($tara) $sel_stele=$sel_stele." and TRIM(LOWER(tari.denumire)) = '".$tara."' ";
		if($zona) $sel_stele=$sel_stele." and TRIM(LOWER(zone.denumire)) = '".$zona."' ";
		if($oras) $sel_stele=$sel_stele." and TRIM(LOWER(localitati.denumire)) = '".$oras."' ";
		if($_REQUEST['localitate_plecare']) $sel_stele=$sel_stele." and TRIM(LOWER(localitati_plecare.denumire)) = '".desfa_link($_REQUEST['localitate_plecare'])."' ";
		if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') $sel_stele=$sel_stele." and oferte.masa = '".desfa_link($_REQUEST['masa'])."' ";
		if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $sel_stele=$sel_stele." and TRIM(LOWER(transport.denumire)) = '".desfa_link($_REQUEST['transport'])."' ";
		$sel_stele=$sel_stele."
		GROUP BY hoteluri.stele
		ORDER BY hoteluri.stele ASC ";
	$que_stele=mysql_query($sel_stele) or die(mysql_error()); ?>
          <select name="stele" onchange="$('#loader').show(); document.filtreaza.submit();">
           <?php if(mysql_num_rows($que_stele)>'1') { ?><option value="">Selecteaza</option><?php } ?>
            <?php while($row_stele=mysql_fetch_array($que_stele)) { ?>
            <option value="<?php echo $row_stele['stele']; ?>" <?php if($_REQUEST['stele']==$row_stele['stele']) { ?> selected="selected" <?php } ?>><?php echo $row_stele['stele']; if($row_stele['stele']<>'1') echo " stele"; else echo " stea"; ?></option>
            <?php } @mysql_free_result($que_stele); ?>
          </select>
        </td>
      </tr>
    </table>
  </div>

<?php /*?><?php if($_REQUEST['transport']=='autocar' || $_REQUEST['transport']=='avion') { ?>
  <div class="new-filter filtNormal" onmouseout="this.className='new-filter filtNormal';" onmouseover="this.className='new-filter filtHover';">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="left" align="left" valign="middle">Oras plecare:</td>
        <td class="right">
          <select name="localitate_plecare" onchange="$('#loader').show(); document.filtreaza.submit();">
          <?php $sel_orase="SELECT
		localitati_plecare.denumire
		FROM
		oferte
		Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
		Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
		Inner Join zone ON localitati.id_zona = zone.id_zona
		Inner Join tari ON zone.id_tara = tari.id_tara
		Left Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
		Left Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
		left Join transport ON oferte.id_transport = transport.id_trans
		left join oferta_sejur_orase on oferte.id_oferta = oferta_sejur_orase.id_oferta
		left join localitati as localitati_plecare on oferta_sejur_orase.id_oras = localitati_plecare.id_localitate
		WHERE
		oferte.valabila =  'da' and localitati_plecare.denumire is not null ";
		if($_REQUEST['tip'])  $sel_orase=$sel_orase." and oferta_sejur_tip.id_tip_oferta in (".$iduri.") ";
		if($tara) $sel_orase=$sel_orase." and TRIM(LOWER(tari.denumire)) = '".$tara."' ";
		if($zona) $sel_orase=$sel_orase." and TRIM(LOWER(zone.denumire)) = '".$zona."' ";
		if($oras) $sel_orase=$sel_orase." and TRIM(LOWER(localitati.denumire)) = '".$oras."' ";
		if($_REQUEST['masa'] && $_REQUEST['masa']<>'toate') $sel_orase=$sel_orase." and oferte.masa = '".desfa_link($_REQUEST['masa'])."' ";
		if($_REQUEST['transport'] && $_REQUEST['transport']<>'toate') $sel_orase=$sel_orase." and TRIM(LOWER(transport.denumire)) = '".desfa_link($_REQUEST['transport'])."' ";
		if($_REQUEST['stele'] && $_REQUEST['stele']<>'toate') $sel_orase=$sel_orase." and hoteluri.stele = '".$_REQUEST['stele']."' ";
		$sel_orase=$sel_orase."
		GROUP BY localitati_plecare.id_localitate
		ORDER BY localitati_plecare.denumire ASC ";
	$que_orase=mysql_query($sel_orase) or die(mysql_error()); ?>
    <?php if(mysql_num_rows($que_orase)>'1') { ?><option value="">Selecteaza</option><?php } ?>
    <?php while($row_orase=mysql_fetch_array($que_orase)) { ?>
    <option value="<?php echo desfa_link($row_orase['denumire']); ?>" <?php if(desfa_link($_REQUEST['localitate_plecare'])==desfa_link($row_orase['denumire'])) { ?> selected="selected" <?php } ?>><?php echo $row_orase['denumire']; ?></option>
    <?php } @mysql_free_result($que_orase); ?>
          </select>
        </td>
      </tr>
    </table>
  </div>
<?php } ?><?php */?>  
</form>

<?php if($_POST && !$_POST['reset']) { ?>
<form method="post" name="reset" action="">
  <p align="right"><input type="submit" onclick="$('#loader').show();" name="reset" value="X Reseteaza filtre" class="resetFilters" /></p>
</form>         
<?php } ?>
</div>