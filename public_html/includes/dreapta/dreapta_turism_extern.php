<?php $tip="SELECT
tip_oferta.denumire_tip_oferta,
tip_oferta.descriere_scurta,
tip_oferta.culoare
FROM oferte
Inner Join oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
Inner Join tip_oferta ON oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta
Inner Join hoteluri ON oferte.id_hotel = hoteluri.id_hotel
Inner Join localitati ON hoteluri.locatie_id = localitati.id_localitate
Inner Join zone ON localitati.id_zona = zone.id_zona
Inner Join tari ON zone.id_tara = tari.id_tara
WHERE oferte.valabila = 'Da'
AND hoteluri.tip_unitate <> 'Circuit'
AND tip_oferta.activ = 'da'
AND tip_oferta.apare_site = 'da'
AND tari.id_tara <> '1'
AND tip_oferta.tip IS NOT NULL
AND oferte.last_minute = 'nu' ";
if($trans) $tip.="AND oferte.id_transport = '$trans' ";
if($masa) $tip.="AND oferte.masa = '$masa' ";
$tip.="
GROUP BY tip_oferta.denumire_tip_oferta
ORDER BY tip_oferta.ordine ASC, tip_oferta.denumire_tip_oferta ASC ";
$que_tip=mysql_query($tip) or die(mysql_error());
if(mysql_num_rows($que_tip)>0) {
	$i=0;
	while($row=@mysql_fetch_array($que_tip)) {
		$i++;
?>
<div class="chnTematici tem_<?php echo $i; ?>">
  <div class="titlu"><a href="/oferte-<?php echo fa_link($row['denumire_tip_oferta']); ?>/" title="<?php echo ucwords($row['denumire_tip_oferta'])." ".desfa_link($_REQUEST['tari']); ?>"><?php echo ucwords($row['denumire_tip_oferta']); ?></a></div>
  <a href="/oferte-<?php echo fa_link($row['denumire_tip_oferta']); ?>/" title="<?php echo ucwords($row['denumire_tip_oferta'])." ".desfa_link($_REQUEST['tari']); ?>"><img src="/images/tematici/<?php echo fa_link($row['denumire_tip_oferta']); ?>.jpg" width="286" height="80" alt="" /></a><br />
  <p class="txt"><?php echo $row['descriere_scurta']; ?></p>
</div>
<?php if($i==3) $i=0;
	}
} @mysql_free_result($que_tip);
?>

