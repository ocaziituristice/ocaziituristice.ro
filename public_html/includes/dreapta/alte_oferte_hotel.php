<?php $oferte=$det->get_oferte($id_hotel, $id_oferta);
if(sizeof($oferte)>0) { ?>
<div class="alte-oferte NEW-round8px">
  <div <?php /*?>style="background:url(<?php echo $sitepath_parinte; ?>img_mediu_hotel/<?php echo $detalii_hotel['poza1']; ?>) -9px 8px no-repeat;"<?php */?>>
  <h3 align="center">Oferte disponibile la <span class="blue"><?php echo $detalii_hotel['denumire']; ?></span></h3>
  <?php /*?><div style="height:200px; display:block; border:1px solid #87B1F1; border-top:none; border-bottom:none;"></div><?php */?>
  <ul class="inner">
	<?php foreach($oferte as $key_of=>$value_of) { $c++;
    $link=$sitepath.fa_link($detalii_hotel['tara']).'/'.fa_link($detalii_hotel['localitate']).'/'.fa_link($value_of['denumire']).'-'.$value_of['id_oferta'].'.html';
    $tipuri=explode('+',$value_of['poza_tip']); ?>
    <li class="clearfix"><a href="<?php echo $link; ?>" title="<?php echo $value_of['denumire']; ?>" class="clearfix">
      <span class="titlu"><?php echo $value_of["denumire_scurta"]; ?></span>
      <span class="detalii"></span>
      <?php /*?><span class="pret black">
        de la<br />
        <span class="red"><?php if(!$value_of['pret_minim']) { $pret=pret_minim_sejur($value_of['id_oferta'], '', '', ''); if($pret['pret']) { echo $pret['pret'].' '; if($pret['moneda']=='EURO') echo '&euro;'; elseif($$pret['moneda']=='USD') echo '$'; else echo $pret['moneda']; } } else { echo $value_of['pret_minim'].' '; if($value_of['moneda']=='EURO') echo '&euro;'; elseif($value_of['moneda']=='USD') echo '$'; else echo $value_of['moneda']; } ?></span><br />
        <?php echo $value_of['exprimare_pret']; ?>
      </span><?php */?>
      <?php if($value_of['nr_zile']>2 || $value_of['nr_nopti']>1) { ?><span class="durata">Sejur: <strong><?php echo $value_of['nr_nopti'].' nopti'; ?></strong></span><?php } ?>
      <span class="masa">Masa: <strong><?php echo $value_of['masa']; ?></strong></span>
      <span class="transport">Transport: <strong><?php echo $value_of['denumire_transport']; ?></strong></span>
    </a></li>
    <?php if($c==4) $c=0; } ?>
  </ul>
  </div>
</div>
<br class="clear"><br>
<?php } ?>
