<?php $selOf = "SELECT
			oferte.id_oferta,
			oferte.denumire,
			oferte.nr_zile,
			oferte.nr_nopti,
			oferte.masa,
			oferte.pret_minim,
			oferte.moneda,
			oferte.exprimare_pret,
			oferte.servicii_manual,
			oferte.cazare,
			oferte.taxa_aeroport,
			oferte.denumire_scurta,
			oferte.oferta_speciala,
			oferte.tip_preturi,
			oferte.last_minute,
			transport.denumire as denumire_transport, ";
			if($var[1]) $selOf .= " new_prices.data_start AS new_data_start,
			new_prices.pret AS new_price, ";
			$selOf .= " tip_oferta.denumire_tip_oferta,
			oferte_speciale.denumire_tip_oferta AS ofsp_denumire,
			oferte_speciale.descriere_scurta AS ofsp_descriere,
			oferte_speciale.reducere AS ofsp_reducere,
			oferte_speciale.data_inceput_eveniment AS ofsp_start,
			oferte_speciale.data_sfarsit_eveniment AS ofsp_end
			FROM oferte
			INNER JOIN transport ON oferte.id_transport = transport.id_trans
			INNER JOIN hotel_meal ON oferte.id_hotel = hotel_meal.id_hotel
			LEFT JOIN oferta_sejur_tip ON oferte.id_oferta = oferta_sejur_tip.id_oferta
			LEFT JOIN tip_oferta ON (oferta_sejur_tip.id_tip_oferta = tip_oferta.id_tip_oferta AND tip_oferta.apare_site = 'da')
			LEFT JOIN tip_oferta AS oferte_speciale ON (oferta_sejur_tip.id_tip_oferta = oferte_speciale.id_tip_oferta AND oferte_speciale.tip = 'oferte_speciale') ";
			if ( $var[2] ) $selOf = $selOf . " INNER JOIN data_pret_oferta ON (oferte.id_oferta = data_pret_oferta.id_oferta AND ((oferte.tip_preturi = 'plecari' AND date_format(data_pret_oferta.data_start, '%m-%Y')  = '" . $var[2] . "') OR (oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <='" . $var[3] . "' AND data_pret_oferta.data_end >= '" . $var[4] . "'))) ";
			if ( $var[5] and $var[15]==4) $selOf = $selOf . " INNER JOIN oferte_transport_avion ON oferte.id_oferta = oferte_transport_avion.id_oferta
			INNER JOIN aeroport ON (oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport AND aeroport.id_localitate = '" . $var[5] . "') ";
			if ( $var[6] ) $selOf = $selOf . " INNER JOIN oferte_transport_autocar ON (oferte.id_oferta = oferte_transport_autocar.id_oferta AND oferte_transport_autocar.id_localitate = '".$var[6]."') ";
			if ( $var[7] ) $selOf = $selOf . " INNER JOIN early_booking ON (oferte.id_oferta = early_booking.id_oferta AND early_booking.end_date >= now() and early_booking.tip = 'sejur') ";
			if($var[1]) $selOf = $selOf . " INNER JOIN (SELECT oferte.id_oferta, data_pret_oferta.data_start, data_pret_oferta.pret FROM oferte INNER JOIN data_pret_oferta ON data_pret_oferta.id_oferta = oferte.id_oferta WHERE oferte.valabila = 'da' AND oferte.id_hotel = '".$var[0]."' AND ( ( oferte.tip_preturi = 'perioade' AND data_pret_oferta.data_start <= '".$var[1]."' AND data_pret_oferta.data_end >= '".$var[1]."' ) OR ( oferte.tip_preturi = 'plecari' AND data_pret_oferta.data_start > NOW() AND data_pret_oferta.data_start >= '".$var[8]."' AND data_pret_oferta.data_start <= '".$var[9]."' ) ) ORDER BY ABS(DATEDIFF(data_pret_oferta.data_start, '".$var[1]."')) ASC, data_pret_oferta.pret ASC) AS new_prices ON new_prices.id_oferta = oferte.id_oferta ";
			$selOf = $selOf . " WHERE oferte.valabila = 'da' ";
			$selOf = $selOf . " AND oferte.id_hotel = '" . $var[0] . "' ";
			if ( $var[10] == 'da' ) $selOf = $selOf . " AND oferte.recomandata = 'da' ";
			if ( $var[11] ) $selOf = $selOf . " AND oferte.last_minute = 'da' ";
			if ( $var[12]==1 ) $selOf = $selOf . " AND oferte_speciale.tip = 'oferte_speciale' ";
			if ( $var[13] ) $selOf = $selOf . " AND oferta_sejur_tip.id_tip_oferta in (" . $var[13] . ") ";
			if ( $var[14] ) $selOf = $selOf . " AND LOWER(hotel_meal.masa) IN (".$var[14].") ";
			if ( $var[15] ) $selOf = $selOf . " AND oferte.id_transport = '" . $var[15] . "' ";
			$selOf = $selOf . " GROUP BY oferte.id_oferta
			ORDER BY oferte.oferta_speciala DESC, oferte.pret_minim ASC ";
			//echo $selOf;
			  // if($_SESSION['mail']=='razvan@ocaziituristice.ro'){echo $selOf;}
			$queOf = mysql_query( $selOf ) or die( mysql_error() );
			
			$nr_t = 0;
			$i = 0;
			$nr_oferte = mysql_num_rows( $queOf );
			$Trans['avion'] = 0;
			$Trans['autocar'] = 0;
			$Trans['individual'] = 0;
			
			while ( $rowOf = mysql_fetch_array( $queOf ) ) {
				$nr_t++;
				$loc = '';
				$cod_plecare = '';
				$oferta_speciala = '';
				$transport = $rowOf['denumire_transport'];
				if($transport == 'Avion') {
					$icon_transport = 'icon_small_transport_avion.png';
					if(cautare_live($var[0])=='da') $Trans['avion'] += 1;
				}
				if($transport == 'Autocar') {
					$icon_transport = 'icon_small_transport_autocar.png';
					if(cautare_live($var[0])=='da') $Trans['autocar'] += 1;
				}
				if($transport == 'Fara transport') {
					$icon_transport = 'icon_small_transport_individual.png';
					if(cautare_live($var[0])=='da') $Trans['individual'] += 1;
				}
				if($transport == 'Avion si Autocar local') {
					$icon_transport = 'icon_small_transport_avion_si_autocar_local.png';
					if(cautare_live($var[0])=='da') $Trans['avion'] += 1;
				}
				
			  if($Trans['avion'] <= 1 and $Trans['autocar'] <= 1 and $Trans['individual'] <= 1) {
				
				if ( $transport == 'Avion' ) {
					$sel = "SELECT
					GROUP_CONCAT(DISTINCT localitati.denumire ORDER BY oferte_transport_avion.ordonare SEPARATOR ', ') AS loc_plecare,
					GROUP_CONCAT(DISTINCT aeroport.cod ORDER BY oferte_transport_avion.ordonare SEPARATOR ', ') AS cod_plecare
					FROM oferte_transport_avion
					LEFT JOIN aeroport ON oferte_transport_avion.aeroport_plecare = aeroport.id_aeroport
					LEFT JOIN localitati ON aeroport.id_localitate = localitati.id_localitate
					WHERE oferte_transport_avion.id_oferta = '" . $rowOf['id_oferta'] . "'
					AND oferte_transport_avion.tip = 'dus' ";
					$que = mysql_query( $sel ) or die( mysql_error() );
					$loc_plecare = mysql_fetch_array( $que );
					@mysql_free_result( $que );
					if($loc_plecare['loc_plecare']) $loc = '<span class="bold black">'.$loc_plecare['loc_plecare'].'</span>';
					if($loc_plecare['cod_plecare']) $cod_plecare = '<span class="plecare bold black" id="av'.$var[0].'" title="Plecare din '.strtoupper($loc_plecare['loc_plecare']).'">'.strtoupper($loc_plecare['cod_plecare']).'</span>';
				}
				
				// de aici vin servicii incluse
				//offer_services($id_hotel, $id_transport, $nr_nopti=NULL, $oras_plecare=NULL, $oras_destinatie=NULL, $id_oferta=NULL) 
	
				
				if ( $nr_t % 2 == 1 ) $class_tr = 'impar'; else $class_tr = 'par';
				$numar_minim_nopti = '';

				if($var[1]) {
					if($rowOf['tip_preturi']=='plecari') {
						$new_pret = new_price($rowOf['new_price']);
						if($rowOf['new_data_start']) $date_checkin = date("d.m", strtotime($rowOf['new_data_start']));
					} else if($rowOf['tip_preturi']=='perioade') {
						$new_pret = new_price($rowOf['new_price']);
						$date_checkin = date("d.m", strtotime($var[1]));
					}
				}
				
				if($new_pret=='') {
					if ( $rowOf['pret_minim'] ) {
						$pret = new_price($rowOf['pret_minim']).' '.moneda($rowOf['moneda']);
					} else {
						$pret = pret_minim_sejur( $rowOf['id_oferta'], '', '', '' );
						$pret = new_price($pret[0])." ".moneda($pret[1]);
					}
				} else {
					$pret = $new_pret.' '.moneda($rowOf['moneda']);
				}
				
				if($rowOf['ofsp_start']<=date("Y-m-d") and $rowOf['ofsp_end']>=date("Y-m-d")) {
					$pret_nou = explode(" ",$pret);
					$price_old = $pret_nou[0]+$rowOf['ofsp_reducere'].' '.$pret_nou[1];
					$oferta_speciala = '<span class="red bold italic bigger-12em" title="'.$rowOf['ofsp_descriere'].'">Oferta Speciala <i class="icon-info-circled green bigger-11em"></i></span>';
				}
				
				if ( $rowOf['cazare'] == 'nu' ) {
					$den = trim( $rowOf['denumire_scurta'] );
				} else {
					$den = trim( $rowOf['denumire_scurta'] );
				}
				if ( $rowOf['nr_nopti'] > 1 ) {
					$durata = $rowOf['nr_nopti'].' nopti';
				}
				if ( $var[16] == 'nu' ) {
					$linkul = make_link_oferta($var[17], $var[19], $rowOf['denumire_scurta'], $rowOf['id_oferta']);
					if(/*$var[20]==1414 and*/ cautare_live($var[0])=='da') $linkul = make_link_oferta($var[17], $var[19]);
				} else {
					$link = make_link_circuit($var[19], $rowOf['id_oferta']);
				}

				
				if($rowOf['taxa_aeroport']=='da') {
					$taxa_aeroport = '<span class="red" title="Taxe aeroport INCLUSE">Taxe INCLUSE</span>';
					$taxa_aeroport2 = '<span class="icons" style="background-image:url(/images/icon_tax_free.png)" title="Taxe aeroport INCLUSE"></span>';
				} else {
					$taxa_aeroport = '';
					$taxa_aeroport2 = '';
				}
				if(!$date_checkin and $var[1]) {
					$oferte .= '';
					$loader .= '';
					$nr_t = $nr_t - 1;
					$nr_oferte = $nr_oferte - 1;
				} else {
					$oferte .= '<tr class="';
					if($oferta_speciala) $oferte .= 'bkg-promo';
						else { if($nr_t%2==0) $oferte .= 'bkg-grey'; else $oferte .= 'bkg-grey'; }
						
					$oferte .= '" title="'.$rowOf['denumire'].'">';
					$oferte .= '<td class="text-center w120 nowrap"><a href="'.$linkul.'" target="_blank"><span class="bold blue">';
					if($oferta_speciala) $oferte .= $oferta_speciala.'<br>';
					if($rowOf['cazare']=='da') {
						$durata='';
						$oferte .= /*'Tarif pe noapte'*/$rowOf['denumire_scurta'].' ';
					} else if($rowOf['last_minute']=='da') {
						$oferte .= '<span class="red">LAST MINUTE</span><br>';
					} else {
						$oferte .= 'Sejur ';
						if($date_checkin) $oferte .= '<span class="green bigger-12em">'.$date_checkin.'</span> - ';
					}
					
					$oferte .= $durata.'</span></a>';
					
					if($rowOf['denumire_tip_oferta']) $oferte .= '<span class="block smaller-09em italic bold green">'.$rowOf['denumire_tip_oferta'].'</span>';
					$oferte .= '</td>';
					$oferte .= '<td class="text-center">';
					//if($rowOf['cazare']=='da') $oferte .= $var[18]; else $oferte .= $rowOf['masa'];
					if($rowOf['cazare']=='da') $oferte .= $rowOf['masa'];
		
					$oferte .= '</td>';
					$oferte .= '<td class="text-center nowrap">';
					$oferte .= '<span class="icons transport" style="background-image:url(/images/oferte/'.$icon_transport.')';
					if($transport == 'Avion si Autocar local') $oferte .= '; width:70px !important;';
					$oferte .= '" title="Transport '.strtoupper(str_replace("Fara transport", "Individual", $transport)).'"></span>';
					$oferte .= $cod_plecare.''.$taxa_aeroport2;
					$oferte .= '</td>';
					$oferte .= '<td class="text-center w140 nowrap">';
					if($price_old > $pret) $oferte .= '<span class="pret-old bold">&nbsp;'.$price_old.'&nbsp;</span>';
					$oferte .= '<span class="pret red">'.$pret.'</span>'.$rowOf['exprimare_pret'].'</td>';
					$oferte .= '<td class="text-center w120"><a href="'.$linkul.'" target="_blank"><img src="/images/but_nou_vezi_detalii.png" alt="Vezi detalii" style="height:25px;"></a></td>';
				
					$oferte .= '</tr>';
					
	//	if(!$err_logare_admin) {
$oferte .= '<tr><td colspan="5">';
//echo $rowOf['servicii_manual'];
		$servicii=$det->offer_services($id_hotel, $id_transport, $nr_nopti=NULL, $oras_plecare=NULL, $oras_destinatie=NULL, $id_oferta=$rowOf['id_oferta'],$e_circuit=false, $servicii_manual=$rowOf['servicii_manual']) ;
		
		$servicii_string='<span class="bigger-12em black bold">Servicii Incluse</span></strong><br />';
		if(sizeof($servicii['servicii_incluse'])>0) {
		foreach ($servicii['servicii_incluse'] as $key => $value)
	{
		$servicii_string.='-'.$value.'<br />';
		
		}  
		}
$oferte.=$servicii_string;
$oferte .= '</td></tr>';

		//	}	// end admin				
					
				}
			  }
				//if(/*$var[20]==1414 and*/ cautare_live($var[0])=='da' and $nr_t>0) break;
			
			}
			
			@mysql_free_result( $queOf );
			
			echo '<table class="search-rooms mar5-0">';
			echo $oferte;
			echo '</table>';
?>