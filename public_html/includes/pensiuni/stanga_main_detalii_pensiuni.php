<?php
 $id_hotel=$_REQUEST['id_hotel'];
 $judet=$_REQUEST['judet'];
 $zona=$_REQUEST['zona'];
 $usuer_fizic=$_SESSION['id_persoana_fizica'];
 $localitate=$_REQUEST['localitati'];

$sel_pensiune="
SELECT
pensiuni.id_pachet,
pensiuni.denumire_hotel,
pensiuni.adresa,
pensiuni.descriere_hotel,
pensiuni.tip,
pensiuni.categorie,
pensiuni.telefon,
pensiuni.fax,
pensiuni.website,
pensiuni.email,
pensiuni.nr_camere as cam_hotel,
pensiuni.facilitati_hotel,
pensiuni.facilitati_camera,
pensiuni.poza1,
pensiuni.poza2,
pensiuni.poza3,
pensiuni.desc_poza1,
pensiuni.desc_poza2,
pensiuni.desc_poza3,
tip_hotel.nume
FROM
pensiuni
Inner Join tip_hotel ON pensiuni.tip = tip_hotel.id
WHERE
pensiuni.id_hotel =  '$id_hotel'
";

$rez_pens=mysql_query($sel_pensiune) or die(mysql_error());
$row_pens=mysql_fetch_assoc($rez_pens);

?>

<div id="detaliiOferta">
  <h1><?php echo ucwords(strtolower($row_pens["nume"])); ?></h1>

  <div class="titluHotel"><?php echo ucfirst(strtolower($row_pens["denumire_hotel"])); ?>
    <img src="/images/spacer.gif" class="stele-mari-<? echo $row_pens["categorie"]; ?>" alt="" /> 
  </div>

<div class="socialIcons">
<div class="addthis_toolbox" style="float:left;">   
	<div class="custom_images">
		<a href="#" onclick="openwindow('/recomanda_pensiuni.php?id=<?php echo $id_hotel; ?>&id_pers=<? echo $usuer_fizic; ?>&recomanda=pensiune', '620px', '450px'); return false;" title="Recomanda oferta" rel="nofollow"><img src="/images/social/email-32x32.png" alt="Recomanda oferta" /></a>
		<a href="/printeaza/pensiune/<?php echo fa_link($row_pens["denumire_hotel"])."-".$id_hotel; ?>.html" target="_blank" title="Printeaza aceasta oferta" rel="nofollow"><img src="/images/social/print-32x32.png" alt="Printeaza aceasta oferta" /></a>
		<a href="/pensiuni/<?php echo fa_link($row_pens["denumire_hotel"]); ?>-<?php echo $id_hotel; ?>.pdf" target="_blank" title="Salveaza ca PDF aceasta oferta" rel="nofollow"><img src="/images/social/pdf-32x32.png" alt="Salveaza ca PDF aceasta oferta" /></a>
		<?php /*?><? if (efavorita($_SESSION['id_persoana_fizica'],$id_circuit,2)) { ?> <a href="javascript: scoate_fav(<? echo $id_circuit; ?>,<? echo $_SESSION['id_persoana_fizica'];?>,2)" title="Sterge de la favorite"><img src="/images/social/remove-save-32x32.png" alt="Sterge de la favorite" /></a><? } else { ?><a href="javascript: <? if ($_SESSION['id_persoana_fizica']) {  ?> adauga_fav(<? echo $id_circuit; ?>,<?=$_SESSION['id_persoana_fizica'];?>,2)<? } else { ?> window.location='http://www.ocaziituristice.ro/login'  <? }?>" title="Salveaza la favorite"><img src="/images/social/save-32x32.png" alt="Salveaza la favorite" /></a> <? } ?><?php */?>
		<a href="#" onclick="openwindow('/cere_mai_multe_detalii.php?id=<?php echo $id_oferta; ?>&cere_info=sejur&id_agentie=<? echo $detalii["id_agentie"]?>&mail_agentie=<? echo $detalii["mail_agentie"]?>&nume_agentie=<? echo $detalii["nume_agentie"]?>', '620px', '400px'); return false;" title="Cere mai multe informatii"><img src="/images/social/info-32x32.png" alt="Cere mai multe informatii" /></a>
    </div>
</div>
<?php include('add_to_social.php'); ?>
<div class="spacer"></div>
</div>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="left" valign="top"><div class="localizare"><span class="red"><?php echo ucwords($judet); ?></span> >> <span class="blue"><?php echo ucwords($zona); ?></span> >> <span class="green"><?php echo ucwords($localitate); ?></span></div></td>
      <td align="right" valign="top" style="padding:0 10px 0 0;">&nbsp;</td>
    </tr>
  </table>
  <div id="gallery" class="gallery">
    <?php
  $j=1;
  for($i=1;$i<=8 && $j<=3; $i++)
		{
		
			if($row_pens["poza".$i]!=NULL) 
			{
				$j++;
				$nume_poza=$row_pens["poza".$i];
				?>
    <a href="http://www.ocaziituristice.ro/foto-pensiuni/imagini/<?php echo $nume_poza; ?>" title="Poza marita"><img src="http://www.ocaziituristice.ro/foto-pensiuni/thumb/<?php echo $nume_poza; ?>" class="imageDescriere" /></a>
    <?php
			}
		}
	?>
    <?php
  $j=1;
  for($i=4;$i<=8 && $j<=8; $i++)
		{
		
			if($row_pens["poza".$i]!=NULL) 
			{
				$j++;
				$nume_poza=$row_pens["poza".$i];
				?>
    <a href="http://www.ocaziituristice.ro/foto_pensiuni/imagini/<?php echo $nume_poza; ?>"></a>
    <?php
			}
		}
	?>
  </div>
  <div class="descriereOferta"> <?php echo remove_special_characters(nl2br($row_pens["descriere_hotel"])); ?> <br />
    <br />
<? if(strlen($row_pens['cam_hotel'])>0){  ?>
        <span class="blue"><strong>Detalii Camere: </strong></span> <br/>
        <?php echo $row_pens['cam_hotel'];  } ?>
    
    <div class="clear"></div>
  <br />

  <table width="100%" border="0">
    <tr>
      <? if(strlen(trim($row_pens["facilitati_hotel"]))>0){?>
      <td width="45%" align="left" valign="top" class="tableServiciiBlue"><h3>&nbsp;&nbsp;&nbsp;Facilitati <?php echo ucwords(strtolower($row_pens["nume"])); ?>:</h3></td>
        <td width="10%" rowspan="2" align="middle" valign="top" class="tableServiciiDespartitor"><img src="/images/spacer.gif" width="5" height="1" /></td>
      <? }?>
      <? if(strlen(trim($row_pens["facilitati_hotel"]))>0 && strlen(trim($row_pens["facilitati_camera"]))>0){?>
      <? }?>
      <? if(strlen(trim($row_pens["facilitati_camera"]))>0){?>
      <td width="45%" align="left" valign="top" class="tableServiciiRed"><h3>&nbsp;&nbsp;&nbsp;Facilitati camera:</h3></td>
      <? }?>
    </tr>
    <tr>
      <? if(strlen(trim($row_pens["facilitati_hotel"]))>0){?>
      <td width="45%" align="left" valign="top" class="tableServiciiBlue"><?php echo remove_special_characters(nl2br($row_pens["facilitati_hotel"])); ?></td>
      <? }?>
      <? if(strlen(trim($row_pens["facilitati_camera"]))>0){?>
      <td width="45%" align="left" valign="top" class="tableServiciiRed"><?php echo remove_special_characters(nl2br($row_pens["facilitati_camera"])); ?></td>
      <? }?>
    </tr>
  </table>
  <br />


<div id="detaliiAgentie">
<div class="detaliiAgentie">
<div style="border:1px solid #d60808; background:url(/images/bkg_agentie_title.gif) repeat-x;">
	<div align="left" class="titleLeft">Date de contact:</div>
    <div class="spacer"></div>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="2" style="border:1px solid #d60808; background:#FFF;">
  <tr>
    <td width="50%" align="left" valign="top"><strong>Telefon:</strong> <span style="font-size:16px; font-weight:bold;" class="blue"><?php echo $row_pens['telefon']; ?></span></td>
    <td width="1" rowspan="3" align="left" valign="top" style="border-left:1px solid #d60808;"><img src="/images/spacer.gif" width="1" height="1" /></td>
    <td width="50%" align="left" valign="top"><strong>Localitatea:</strong> <?php echo ucwords($localitate); ?></td>
    </tr>
  <tr>
    <td width="50%" align="left" valign="top"><strong>E-mail:</strong> <?php echo $row_pens['email']; ?></td>
    <td width="50%" align="left" valign="top"><strong>Adresa:</strong> <?php echo ucwords(strtolower($row_pens['adresa'])); ?></td>
    </tr>
  <tr>
    <td width="50%" align="left" valign="top"><strong>Website:</strong> <a href="<? if (eregi("http://",$row_pens['website'])) echo $row_pens['website']; else echo "http://".$row_pens['website']; ?>" target="_blank" title="<?php echo $row_pens['website']; ?>" rel="nofollow"><?php echo $row_pens['website']; ?></a></td>
    <td width="50%" align="left" valign="top"><strong>Fax:</strong> <?php echo $row_pens['fax']; ?></td>
    </tr>
</table>
</div>
</div>

<br />

  <? $sel_pret="Select 
                preturi.id_pret,
				GROUP_CONCAT(DISTINCT date_format(preturi.perioada_inceput, '%d %b %Y'),' - ', date_format(preturi.perioada_terminat, '%d %b %Y') ORDER BY preturi.perioada_inceput SEPARATOR ',<br /> ') AS perioade,
				preturi.id_hotel,
				preturi.tip_camera,
				preturi.nr_camere,
				preturi.observatii,
				preturi.data_cuvinte,
				preturi.pret,
				preturi.moneda,
                 tip_camera.nume
				  from preturi
				  Inner Join tip_camera ON tip_camera.id= preturi.tip_camera
				   Where id_hotel= '$id_hotel'
				   group by preturi.pret,
				   preturi.tip_camera,
				   preturi.observatii,
				   preturi.moneda ";
   $rez_pret=mysql_query($sel_pret) or die(mysql_error());
   ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ofertaPreturi">
    <tr>
      <th width="314" align="center" valign="middle">Perioade</th>
      <th width="453" align="center" valign="middle">Tip camera</th>
      <th width="177" align="center" valign="middle">Pret</th>
      <th width="106" align="center" valign="middle">&nbsp;</th>
    </tr>
    <?php $i=0; while($row_pret=mysql_fetch_array($rez_pret)) { ?>
    <tr>
      <td width="314" align="center" valign="middle"><?php echo $row_pret["perioade"]; ?></td>
      <?php if ($row_pret["observatii"]!=NULL) { ?>
      <td align="left" valign="middle" style="padding:3px 5px;" id="sprytrigger<?php echo $i+3; ?>"><strong><?php echo $row_pret["nume"]; ?></strong><?php /*?><img src="/images/info_vsmall.gif" width="17" height="17" /><?php */?>
        <div class="tooltipContent2" id="sprytooltip<?php echo $i+3; ?>"><?php echo $row_pret["observatii"]; ?></div>
        <?php /*?><script type="text/javascript">
<!--
var sprytooltip<?php echo $i+3; ?> = new Spry.Widget.Tooltip("sprytooltip<?php echo $i+3; ?>", "#sprytrigger<?php echo $i+3; ?>", {useEffect:"fade", followMouse:true, showDelay:0, hideDelay:0});
//-->
</script><?php */?></td>
      <?php } else { ?>
       <td align="left" valign="middle" style="padding:3px 5px;"><?php echo $row_pret["nume"]; ?></td><? } ?>
      <td align="left" valign="middle" style="padding:3px 5px;"><strong class="red"><?php echo $row_pret['pret']; ?>&nbsp;<?php echo $row_pret['moneda']; ?></strong></td>
      <td width="106" align="center" valign="middle"><?php $i++; ?></td>
    </tr>
    <?php } ?>
  </table>
  <? $sel_ofer="Select *
				  from oferte_pensiuni
				   Where cazare= '$id_hotel' and
				   current_date() <= data_sfarsit
				   GROUP BY id ";
   $rez_ofer=mysql_query($sel_ofer) or die(mysql_error());
   if(mysql_num_rows($rez_ofer)>0) {
   ?>
  <br/>
  <br/>
  <h1>Oferte speciale:</h1>
  <? 
   while($row_ofer=mysql_fetch_array($rez_ofer)) { ?>
   <div style="padding:5px; border-bottom:1px solid #065abf;">
	<div class="titluHotel" style="font-size:18px;"><? echo $row_ofer['titlu']; ?></div>
	<div align="justify"><? echo $row_ofer['descriere']; ?></div>
	<? if ($row_ofer['pret']>0) { ?>
    <div style="float:right; font-size:16px; font-weight:bold;" class="red"><? echo $row_ofer['pret']." ".$row_ofer['moneda']; ?></div>
    <? } ?>
    <div class="spacer"></div>
   </div>
  <br/>
  <? } }?>

<div class="socialIcons">
<div class="addthis_toolbox" style="float:left;">   
	<div class="custom_images">
		<a href="#" href="#" onclick="openwindow('/recomanda_pensiuni.php?id=<?php echo $id_hotel; ?>&id_pers=<? echo $usuer_fizic; ?>&recomanda=pensiune', '620px', '450px'); return false;" title="Recomanda oferta" rel="nofollow"><img src="/images/social/email-32x32.png" alt="Recomanda oferta" /></a>
		<a href="/printeaza/pensiune/<?php echo fa_link($row_pens["denumire_hotel"])."-".$id_hotel; ?>.html" target="_blank" title="Printeaza aceasta oferta" rel="nofollow"><img src="/images/social/print-32x32.png" alt="Printeaza aceasta oferta" /></a>
		<a href="/pensiuni/<?php echo fa_link($row_pens["denumire_hotel"]); ?>-<?php echo $id_hotel; ?>.pdf" target="_blank" title="Salveaza ca PDF aceasta oferta" rel="nofollow"><img src="/images/social/pdf-32x32.png" alt="Salveaza ca PDF aceasta oferta" /></a>
		<?php /*?><? if (efavorita($_SESSION['id_persoana_fizica'],$id_circuit,2)) { ?> <a href="javascript: scoate_fav(<? echo $id_circuit; ?>,<? echo $_SESSION['id_persoana_fizica'];?>,2)" title="Sterge de la favorite"><img src="/images/social/remove-save-32x32.png" alt="Sterge de la favorite" /></a><? } else { ?><a href="javascript: <? if ($_SESSION['id_persoana_fizica']) {  ?> adauga_fav(<? echo $id_circuit; ?>,<?=$_SESSION['id_persoana_fizica'];?>,2)<? } else { ?> window.location='http://www.ocaziituristice.ro/login'  <? }?>" title="Salveaza la favorite"><img src="/images/social/save-32x32.png" alt="Salveaza la favorite" /></a> <? } ?><?php */?>
		<a href="#" onclick="openwindow('/cere_mai_multe_detalii.php?id=<?php echo $id_oferta; ?>&cere_info=sejur&id_agentie=<? echo $detalii["id_agentie"]?>&mail_agentie=<? echo $detalii["mail_agentie"]?>&nume_agentie=<? echo $detalii["nume_agentie"]?>', '620px', '400px'); return false;" title="Cere mai multe informatii"><img src="/images/social/info-32x32.png" alt="Cere mai multe informatii" /></a>
    </div>
</div>
<?php include('add_to_social.php'); ?>
<div class="spacer"></div>
</div>

<?php /*?>  <br/>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="5" style="border:1px solid #279500; background:#e9fddc;">
    <tr>
      <td align="left" valign="top"><img src="/images/trimite_recomandare.gif" style="float:left;" />&nbsp;<a href="#" onclick="openwindow('/recomanda_pensiuni.php?id=<?php echo $id_hotel; ?>&id_pers=<? echo $usuer_fizic; ?>&recomanda=pensiune', '620px', '500px'); return false;" title="Recomanda oferta">Recomanda aceasta oferta</a></td>
      <td align="left" valign="top"><img src="/images/trimite_print.gif" style="float:left;" />&nbsp;<a href="/printeaza/pensiune/<?php echo fa_link($row_pens["denumire_hotel"])."-".$id_hotel; ?>.html" target="_blank" rel="nofollow" title="Printeaza aceasta oferta">Printeaza aceasta oferta</a></td>
      <td align="left" valign="top"><img src="/images/trimite_pdf.gif" style="float:left;" />&nbsp;<a href="/pensiuni/<?php echo fa_link($row_pens["denumire_hotel"]); ?>-<?php echo $id_hotel; ?>.pdf" target="_blank" title="PDF aceasta oferta">PDF aceasta oferta</a></td>
    </tr>
    <tr>
      <td align="left" valign="top"><!-- NICI O OPINIE -->
        <?php
 $sel_opinii="
	SELECT
	COUNT(distinct pensiuni_opinia.id_opinie) AS numar,
	AVG(pensiuni_opinia.nota) AS media
	FROM
	pensiuni_opinia
	WHERE
	pensiuni_opinia.aprobat_admin =  '1' AND
	pensiuni_opinia.id_pens =  '$id_hotel'
	GROUP BY pensiuni_opinia.id_opinie
	";
	$rez1=mysql_query($sel_opinii);
	$row_op=mysql_fetch_array($rez1);
	@mysql_free_result($rez1);
	if(!$row_op["numar"]) { ?>
        <div class="opiniiVizitatoriTop"><img src="/images/icon_editeaza_profil.gif" style="float:left;" />&nbsp;<a href="#" onclick="openwindow('/pareri_vizitatori_pensiuni.php?id=<?php echo $id_hotel; ?>', '620px', '500px'); return false;" title="Exprima parerea">Fii primul care isi exprima parerea</a></div>
        <?php } else { ?>
        <!-- CEL PUTIN O OPINIE -->
        <div class="opiniiVizitatoriTop"><img src="/images/icon_editeaza_profil.gif" style="float:left;" />&nbsp;<a href="#opinii" title="Opinii">Parerile Vizitatorilor/Turistilor</a>
        <br />
          <?php
	$media=$row_op["media"];
	if(!$row_op["numar"]) $row_op["numar"]=0;
	$media=round($media);
	if(!$media) $media=0;
	?>
          <?php for($i=1; $i<=5; $i++) { if($i<=$media) { ?>
          <img src="/images/stea_mica_on.gif" />
          <?php } else { ?>
          <img src="/images/stea_mica_off.gif" />
          <?php } } ?>
          &nbsp;&nbsp;&nbsp; (<a href="#opinii" title="Opinii"><?php echo $row_op["numar"]; ?> pareri</a>)</div>
        <?php } ?></td>
      <td align="left" valign="top" colspan="2"><!--<img src="/images/salveaza_oferta.gif" style="float:left;" />&nbsp;<a href="#">Salveaza oferta</a>--></td>
      </tr>
      <tr>
      <td align="right" valign="top" colspan="3"><img src="/images/cere_informatii.gif" style="float:right;" />&nbsp;<a href="#" onclick="openwindow('/cere_mai_multe_detalii.php?id=<?php echo $id_oferta; ?>&cere_info=sejur&id_agentie=<? echo $detalii["id_agentie"]?>&mail_agentie=<? echo $detalii["mail_agentie"]?>&nume_agentie=<? echo $detalii["nume_agentie"]?>', '620px', '370px'); return false;" title="Cere mai multe informatii">Cere mai multe informatii</a></td>
    </tr>
  </table>
</div>
<br />
<div class="opiniiVizitatori">
  <?php 
$sel_opinii="
SELECT
pensiuni_opinia.nume,
pensiuni_opinia.localitate,
pensiuni_opinia.nota,
pensiuni_opinia.comentariu,
pensiuni_opinia.data_adaugarii
FROM
pensiuni_opinia
WHERE
pensiuni_opinia.aprobat_admin =  '1' AND
pensiuni_opinia.id_pens =  '$id_hotel'
";
$rez_opinii=mysql_query($sel_opinii);
if(mysql_num_rows($rez_opinii)) {


while($row_opinii=mysql_fetch_array($rez_opinii))
{
?>
  <fieldset style="border:1px solid #00722e; padding:3px 6px;">
    <legend style="color:#00722e;"><strong><strong><?php echo $row_opinii["nume"]; ?></strong> - <?php echo $row_opinii["localitate"]; ?></strong>&nbsp;&nbsp;&nbsp;
    <?php for($i=1; $i<=$row_optinii["nota"]; $i++) { ?>
    <img src="/images/stea_mica_on.gif" />
    <?php } ?>
    </legend>
    <?php echo nl2br($row_opinii["comentariu"]); ?>
  </fieldset>
  <br />
  <?php
}				

}
@mysql_free_result($rez_opinii);
?><?php */?>
</div>
</div>
<? @mysql_free_result($rez_pens);
   @mysql_free_result($rez_foto);
   @mysql_free_result($sel_fac);
   @mysql_free_result($rez_pret);
   @mysql_free_result($rez_ofer); ?>

<br />
<h1 class="aceeasiZona">Pensiuni si Hoteluri <span class="red"><?php echo ucwords(desfa_link($zona)); ?></span> &nbsp;<img src="/images/arrow_pointing_down.gif" style="vertical-align:middle;" /></h1>

<div class="chenarIndexBlue">
  <?php
$template="".$_SERVER['DOCUMENT_ROOT']."/templates/afisare_pensiune_links.tpl";
$zone[0] = $zona;
$afisare=new AFISARE_PENSIUNI();
$afisare->setAfisare(1,16);
$afisare->set_pensiune_special($template);
$afisare->setZone($zone);
$afisare->initializeaza_pagini($link);
$afisare->afiseaza();
unset($afisare);
?>
  <div class="spacer"></div>
</div>

<div class="clear"></div>
