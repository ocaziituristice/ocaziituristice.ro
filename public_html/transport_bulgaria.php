<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Transport Bulgaria | <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Transport Bulgaria pe litoral si la munte prin <?php echo $denumire_agentie; ?> gasesti cele mai bune oferte de transport in Bulgaria" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <span class="titlu_modala">Transport Bulgaria</span>
        
        <div class="Hline"></div>
        
        <div class="text-justify article">
          
          <ul class="chn-linkuri float-right">
            <li class="primul black">Linkuri utile Bulgaria</li>
            <li class="second"><a href="/sejur-bulgaria/">Sejur Bulgaria</a></li>
            <?php /*?><li class="second"><a href="/oferte-paste/bulgaria/">Paste Bulgaria</a></li><?php */?>
            <li class="second"><a href="/sejur-bulgaria/litoral/">Litoral Bulgaria</a></li>
          </ul>
               
          <p class="bigger-12em">Agenția noastră vă pune la dispoziție mai multe oferte de transport cu autocarul în Bulgaria prin partenerii noștri</p>
          <?php /*?><ul class="bold bigger-12em" style="margin:5px 0 0 35px;">
            <li><a href="#filadelfia-turism" class="green">FILADELFIA TURISM</a></li>
          </ul><?php */?>
          
          <br class="clear">
          
          <div id="filadelfia-turism" class="NEW-chn-oferta-green NEW-round8px mar10 pad10 clearfix">
            <h2 class="section-title-green underline blue" style="margin-bottom:10px;">Oferta Transport cu Autocarul - <span class="black">Bulgaria Litoral</span> - <span class="red">Vară 2014</span></h2>
            <p class="bigger-15em bold">Plecări zilnice în perioada <span class="underline">01.06.2014 - 15.09.2014</span></p>
            <table width="100%" border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td width="50%" align="left" valign="top">
                <p class="bigger-15em bold">PREȚ dus-întors: <span class="red">30 €/adult</span></p>
                <p><strong>Tarifele de mai sus includ transport DUS - ÎNTORS, taxele vamale și de pod.</strong></p>
                <p class="bigger-13em bold">Copii 0-3 ani - <span class="red">GRATUIT</span></p>
                <p class="bigger-13em bold">Copii 3-7 ani - <span class="red">20 €/copil</span></p>
                <p class="bigger-13em bold">Copii peste 7 ani - <span class="red">30 €/copil</span></p>
                <p>Turiștii trebuie să se prezinte la locul de îmbarcare cu <strong>minim 20 de minute înaintea orei anunțate</strong>.</p>
                <p class="bigger-12em"><strong>OBSERVAȚII!</strong> În Varna pasagerii care merg spre Sunny Beach vor fi transferați cu un microbus/autocar, iar ceialalți vor continua călătoria către stațiunile din Nord.</p></td>
                <td width="50%" align="left" valign="top"><table width="100%" border="1" cellspacing="0" cellpadding="4">
                  <tr>
                    <th align="center" valign="top">STAȚIA</th>
                    <th align="center" valign="top">DUS</th>
                    <th align="center" valign="top">ÎNTORS</th>
                  </tr>
                  <tr>
                    <td align="left" valign="top">BUCUREȘTI - Gara de Nord, vis-a-vis de coloane</td>
                    <td align="center" valign="top">06.00</td>
                    <td align="center" valign="top">20.00</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">GIURGIU - Rompetrol</td>
                    <td align="center" valign="top">08.00</td>
                    <td align="center" valign="top">18.30</td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">VARNA - Catedrala</td>
                    <td align="center" valign="top">11.15</td>
                    <td align="center" valign="top">15.00</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">C-TIN ȘI ELENA - Hotel Romance</td>
                    <td align="center" valign="top">11.30</td>
                    <td align="center" valign="top">14.45</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">SUNNY DAY - Poarta de lângă șoseaua principală</td>
                    <td align="center" valign="top">11.40</td>
                    <td align="center" valign="top">14.40</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">NISIPURILE DE AUR - Hotel Melia Grand Hermitage</td>
                    <td align="center" valign="top">11.45</td>
                    <td align="center" valign="top">14.30</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">ALBENA - Centrul de informare turistică</td>
                    <td align="center" valign="top">12.00</td>
                    <td align="center" valign="top">14.15</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">BALCIC - Hotel Bisser</td>
                    <td align="center" valign="top">12.15</td>
                    <td align="center" valign="top">14.00</td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left" valign="top">&nbsp;</td>
                    </tr>
                  <tr>
                    <td align="left" valign="top">OBZOR - Hotel Miramar</td>
                    <td align="center" valign="top">12.00</td>
                    <td align="center" valign="top">13.45</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">SUNNY BEACH - vis-a-vis de Hotel Kuban</td>
                    <td align="center" valign="top">12.30</td>
                    <td align="center" valign="top">13.00</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">NESSEBAR - Hotel Sol Palace</td>
                    <td align="center" valign="top">12.45</td>
                    <td align="center" valign="top">12.45</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            <?php /*?><div class="float-right black uppercase smaller-09em underline">Transport asigurat de <strong>Tiladelfia Turism</strong></div><?php */?>
          </div>
          
          <br class="clear"><br>

        </div>
        
      </div>
    </div>
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>