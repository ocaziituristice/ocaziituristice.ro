<a name="hotel_{id_hotel}"></a>
<div class="afisare-oferta {class_selected_hotel}">
    <div class="normal">
        <div id="leftdiv_hotel" class="hotel-name">
            <a href="{link_h}" class="link-blue" title="{denumire_hotel} {titlu_dreapta}" target="_blank">
                <span class="green">{denumire_tip_oferta}</span> {denumire_hotel} - <span
                        class="grey2 smaller-09em">{titlu_dreapta}</span>
            </a> {stele}
            <div id="rightdiv_hotel_all">{masa_all_inclusive}</div>
        </div>
        <div class="float-left">
            <a href="{link_h}" class="poza NEW-round6px" target="_blank">
                {early}{afisare_comision_reducere}
                <img src="{poza1_mare}" alt="{denumire_hotel}" class="hotel-image">
                <div class="mobile-all-inclusive">{masa_all_inclusive}</div>
            </a>
        </div>
        <div class="float-right">
            <div class="text-justify" style="line-height:1.4em; margin:10px 0;">{comentariu}</div>
            <div>
                <div id="rightdiv_hotel">{localitati_plecare_avion}</div>
                <div id="leftdiv_hotel">{recomandat_pentru}</div>
                <div id="rightdiv_hotel" class="bigger-12em ">{afisare_distanta}</div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="show-prices-button  ">
            <a href="#hotel_{id_hotel}" id="vezipret{id_hotel}">
                <h3 class="bigger-15em ui-state-default ui-accordion-header-active  ui-corner-top" style="text-align: center;">
                    Vezi preturi
                </h3>
            </a>
        </div>
    </div>
    <input type="hidden" value="{loader_link}" class="loader_link"/>
    <input type="hidden" value="{cautare_live}" class="cautare_live"/>
    <div id="hotel{id_hotel}O"></div>
</div>