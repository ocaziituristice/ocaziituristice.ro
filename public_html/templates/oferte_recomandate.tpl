<div class="inner NEW-round8px clearfix" style="cursor:pointer;">
  <h3 class="blue" style="margin:3px; padding:0;"><a href="{link}" title="{den_oferta}">{den_oferta}</a> {stele}</h3>
  <a href="{link}"><img src="{poza1}" alt="" class="images NEW-round8px" style="margin-right:6px;" /></a>
  <p style="font-weight:bold; font-style:italic;"><a href="{link_localitate}" title="{denumire_localitate}" class="link-green">{denumire_localitate}</a> / <a href="{link_tara}" title="{denumire_tara}" class="link-green">{denumire_tara}</a></p>
  <p>{denumire_transport}, {nr_nopti}, {masa}</p>
  <p class="blue" style="font-size:1.2em;">{plecare}</p>
  <p align="right" style="font-size:1.2em;" class="red"><strong>de la</strong> <span style="font-size:1.5em;"><strong>{pret_minim}</strong></span></p>
  <br class="clear" /><br />
  <p align="center"><a href="{link}" class="buton-blue">vezi detalii oferta</a></p>
</div>