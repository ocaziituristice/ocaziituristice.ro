<div class="afisare-oferta NEW-round8px clearfix">
  <div class="poza"><a href="{link}"><img src="{poza1}" alt="{denumire_hotel}" /></a></div>
  <h2 class="float-left"><a href="{link}" title="{denumire_oferta}">{denumire_oferta}</a></h2>
  <p class="float-right green"><strong>{continent}</strong></p>
  <div class="infos">
    <p class="titlu">Transport:</p><p class="valoare">{denumire_transport}</p>
    <p class="titlu">Durata:</p><p class="valoare">{nr_zile_nou}</p>
    <p class="titlu">Masa:</p><p class="valoare">{masa}</p>
  </div>
  <div class="price red">de la <span>{pret_minim}</span> {exprimare_pret}</div>
  <a href="{link}"><span class="vezi-detalii NEW-round8px">vezi detalii</span></a>
</div>
