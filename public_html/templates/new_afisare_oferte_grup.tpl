<div class="afisare-oferta {class_selected_hotel}">
  <div class="normal NEW-round8px clearfix">
   
<div id="leftdiv_hotel" class="hotel-name">
            <a href="{link_h}" class="link-blue" title="{denumire_hotel} {titlu_dreapta}" target="_blank">
                <span class="green">{denumire_tip_oferta}</span> {denumire_hotel} - <span
                        class="grey2 smaller-09em">{titlu_dreapta}</span>
            </a> {stele}
            <div id="rightdiv_hotel_all">{masa_all_inclusive}</div>
        </div>
        <div class="float-left">
            <a href="{link_h}" class="poza NEW-round6px" target="_blank">
                {early}{afisare_comision_reducere}
                <img src="{poza1_mare}" alt="{denumire_hotel}" class="hotel-image">
                <div class="mobile-all-inclusive">{masa_all_inclusive}</div>
            </a>
        </div>
        <div class="float-right">
            <div class="text-justify" style="line-height:1.4em; margin:10px 0;">{comentariu}</div>
            <div>
                <div id="rightdiv_hotel">{localitati_plecare_avion}</div>
                <div id="leftdiv_hotel">{recomandat_pentru}</div>
                <div id="rightdiv_hotel" class="bigger-12em ">{afisare_distanta}</div>
            </div>
        </div>
  </div>
  <div id="H{id_hotel}O"></div>
</div>
{loader}