<div class="afisare-oferta"><div class="recomandata NEW-round8px">
  <span class="title white">Va recomandam</span>
  <div class="inner NEW-round6px clearfix">
  <div class="float-left" style="margin-right:8px;">
    <div class="poza"><a href="{link_h}"><img src="{poza1}" alt="{denumire_hotel}" />{early}</a></div>
  </div>
  <h2 class="float-left">{link} {stele}</h2>
  <p class="float-right green"><strong>{titlu_dreapta}</strong></p>
  <table width="618" cellpadding="2" cellspacing="1" class="tabel-oferte">
    <thead>
      <tr>
        <th align="center" valign="middle">&nbsp;</th>
        <th align="center" valign="middle" width="90">Masa</th>
        <th align="center" valign="middle" width="100">Transport</th>
        <th align="center" valign="middle" width="90">Tarif de la</th>
        <th align="center" valign="middle" width="80">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
       {oferte}
    </tbody>
  </table>
</div></div></div>
