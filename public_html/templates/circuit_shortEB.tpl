<div class="oferta NEW-round8px clearfix">
  <a href="{link}"><img width="100" height="100" class="images NEW-round6px" alt="{denumire_hotel}" src="{poza1}" /></a>
  <div class="titlu"><a href="{link}" class="link-blue" title="{titlu_link}">{denumire_hotel}</a></div>
  <div class="pret red">de la <span>{pret_minim}</span><br />{exprimare_pret}</div>
  <div class="transport">{denumire_transport}</div>
  <div class="place">{nr_zile_nou}</div>
  <div class="destinatie"><strong>{denumire_continent}:</strong> {tari_vizitate}</div>
</div>
