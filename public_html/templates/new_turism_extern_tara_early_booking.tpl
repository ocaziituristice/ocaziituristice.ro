<div class="afisare_oferta_lunga_early">
  <img class="thumbnail" src="{poza1}" alt="{denumire_hotel}" width="100" height="100"  />
  <div class="titlu">
    <a href="{link}" title="{titlu_link}" style="padding-left:8px;">{denumire_hotel} {stele}</a>
  </div>
  <div class="clear-right"></div>
  <div class="linkedPrice" style="float:right; width:260px;">de la <font style="font-size:18px;">{pret_minim}</font> {exprimare_pret}</div>
  <div class="area">{denumire_zona} {denumire_localitate}</div>
  <div class="area">Transport: <strong>{denumire_transport}</strong></div>
  <div class="durata">{nr_zile}</span></div>
  <br class="clear" />
  <div class="early">
	<div align="right" class="veziDetaliiBlue" onclick="location.href='{link}';" style="float:right;"><i>{denumire_oferta}</i></div>
    <strong>ECONOMISESTI <span>{procent_early}</span></strong> daca rezervi pana la data de <strong>{data_early}</strong>
  </div>
</div>
