<div class="oferta NEW-round8px clearfix">
  <a href="{link}"><img width="50" height="50" class="images NEW-round6px" alt="{denumire_hotel}" src="{poza1}" /></a>
  <div class="titlu"><a href="{link}" class="link-blue" title="{denumire_hotel}">{denumire_hotel}</a></div>
  <div class="float-left">
	<div class="local">Continent: {denumire_continent}</div>
  </div>
  <div class="pret">de la <span class="red">{pret_minim}</span><br />{exprimare_pret}</div>
</div>
