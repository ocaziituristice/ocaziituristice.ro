<div class="afisare-oferta"><div class="normal NEW-round8px clearfix">
  <div class="float-left" style="margin-right:8px;">
	<div class="poza"><a href="{link_h}" title="{denumire_hotel} {titlu_dreapta}"><img src="{poza1}" alt="{denumire_hotel} {titlu_dreapta}" />{early}</a></div>
    {nota_review}
  </div>
  <div style="float:right; width:618px;">
    <div class="hotel grey2"><a href="{link_h}" class="link-blue" title="{denumire_hotel} {titlu_dreapta}">{denumire_hotel} - <span class="grey2 smaller-09em">{titlu_dreapta}</span></a> {stele}</div>
    <table class="tabel-oferte" style="width:618px;">
      <thead>
        <tr>
          <th class="text-center">&nbsp;</th>
          <th class="text-center" style="width:110px;">Masa</th>
          <th class="text-center" style="width:100px;">Transport</th>
          <th class="text-center" style="width:90px;">Tarif de la</th>
          <th class="text-center" style="width:80px;">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        {oferte}
      </tbody>
    </table>
    {comentariu}
  </div>
</div></div>
