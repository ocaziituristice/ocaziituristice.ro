
<a name="hotel_{id_hotel}"></a>
<div class="afisare-oferta {class_selected_hotel}">
  <div class="normal NEW-round8px clearfix">
    <div class="float-left" style="margin-right:8px;">
      <a href="{link_h}" class="poza NEW-round6px" target="_blank">{early}{afisare_comision_reducere}<img src="{poza1}" alt="{denumire_hotel}" class="NEW-round6px"></a>
    </div>
    <div class="float-right w620">
    
     <div id="rightdiv_hotel_all" >  {masa_all_inclusive}</div>
     
      <div id="leftdiv_hotel" class="hotel grey2 mar5-0">
      <a href="{link_h}" class="link-blue" title="{denumire_hotel} {titlu_dreapta}" target="_blank"><span class="green">{denumire_tip_oferta}</span> {denumire_hotel} - <span class="grey2 smaller-09em">{titlu_dreapta}</span></a> {stele}
      </div>
  
      
      <div class="text-justify" style="line-height:1.4em; margin:3px 0 0 0;">{comentariu}</div> 
      
     <div>
     <div id="rightdiv_hotel" >  {localitati_plecare_avion}</div>
      
         
     
     
      <!-- {nota_review}--><div id="leftdiv_hotel">{recomandat_pentru}</div>
      <div id="rightdiv_hotel" style="line-height:1.4em; margin:4px 0 0 0;" class="bigger-12em bold">{afisare_distanta}</div>
   <!--{pret_marketing}-->
   </div>
   <br class="clear">
   
   <a href="#hotel_{id_hotel}" id="vezipret{id_hotel}"><h3 class="bigger-15em ui-state-default ui-accordion-header-active  ui-corner-top" style="text-align: center;">Vezi prețurile la <strong>{denumire_hotel}</strong> din <strong>{denumire_localitate}</strong></h3></a>
   
    </div> 
  
  
   
  </div>
 <div id="hotel{id_hotel}O"></div> 
</div>
{loader}
