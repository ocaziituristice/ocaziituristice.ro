<div class="afisare-oferta NEW-round8px clearfix">
  <div class="poza"><a href="{link}"><img src="{poza1}" alt="{denumire_hotel}" /></a></div>
  <h2 class="blue"><a href="{link}" title="{denumire_oferta}">{denumire_oferta}</a></h2>
  <div class="infos">
    <p><strong>{denumire_zona} {denumire_localitate}</strong></p>
    <p>{stele}</p>
    <p class="titlu">Transport:</p><p class="valoare">{denumire_transport}</p>
    {nr_zile_nou}
    <p class="titlu">Masa:</p><p class="valoare">{masa}</p>
  </div>
  <div class="price red">de la <span>{pret_minim}</span> {exprimare_pret}</div>
  <a href="{link}"><span class="vezi-detalii NEW-round8px">vezi detalii</span></a>
</div>
