<div class="afisare-oferta">
    <div class="normal">
        <div id="leftdiv_hotel" class="hotel-name">
            <a href="{link}" class="link-blue" title="{denumire_hotel}" target="_blank">
                {denumire_hotel}
            </a> {stele}
        </div>
        <div class="float-left">
            <a href="{link}" class="poza NEW-round6px" target="_blank">
                {early}
                <img src="{poza1_mare}" alt="{denumire_hotel}" class="hotel-image">
            </a>
        </div>
        <div class="float-right">
            <div class="circuit-details">
                <p class="titlu">
                    <span class="name">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <span class="bigger-12em bold">Continent:</span>
                    </span>
                    <span class="bigger-12em green bold result">{denumire_continent}</span>
                </p>
                <p class="titlu">
                    <span class="name">
                        <i class="fa fa-map" aria-hidden="true"></i>
                        <span class="bigger-12em bold">Tari vizitate:</span>
                    </span>
                    <span class="bigger-12em green bold result">{tari_vizitate}</span>
                </p>
                <p class="titlu">
                    <span class="name">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <span class="bigger-12em bold">Plecari:</span>
                    </span>
                    <span class="bigger-12em blue bold result">{plecare}</span>
                </p>
                <p class="titlu">
                    <span class="name">
                        <i class="fa fa-bus" aria-hidden="true"></i>
                        <span class="bigger-12em bold">Transport:</span>
                    </span>
                    <span class="bigger-12em blue bold result">{denumire_transport}</span>
                </p>
                <p class="titlu">
                    <span class="name">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span class="bigger-12em bold">Durata:</span>
                    </span>
                    <span class="bigger-12em blue bold result">{nr_zile_nou}</span>
                </p>
            </div>
        </div>
        <div class="clear"></div>
        <div class="show-prices-button">
            <div class="price">de la <span>{pret_minim}</span> {exprimare_pret}</div>
            <a href="{link}">
                <h3 class="bigger-15em ui-state-default ui-accordion-header-active ui-corner-top">
                    Vezi detalii
                </h3>
            </a>
        </div>
    </div>
</div>