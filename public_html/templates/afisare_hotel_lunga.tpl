<div class="afisare_oferta_lunga">
  <a onclick="location.href='{link}';" style="cursor:pointer;" title="{denumire_hotel}"><img class="thumbnail" src="{poza1}" alt="{denumire_hotel}" width="100" height="100" /></a>
  <div class="titlu"><a href="{link}" title="{denumire_hotel}" style="padding-left:8px;">{denumire_hotel}</a> {stele}</div>
  <div class="clear-right"></div>
  <div class="area blue">{denumire_tara} / {denumire_zona} / {denumire_localitate}</div>
  <div class="descriere_hotel" align="justify">{descriere_hotel}</div>
  <br class="clear-right" />
  <div align="right" class="veziDetaliiBlue" onclick="location.href='{link}';"><i>{denumire_hotel}</i></div>
</div>
