<div class="afisare-oferta"><div class="normal NEW-round8px clearfix">
  <div class="float-left" style="margin-right:8px;">
	<div class="poza"><img src="{poza1}" alt="{denumire_hotel}" class="NEW-round6px">{early}</div>
  </div>
  <div style="float:right; width:618px;">
    <div class="hotel grey2"><a href="{link_h}" class="link-blue" title="{denumire_hotel} {titlu_dreapta}" target="_blank">{denumire_hotel} - <span class="grey2 smaller-09em">{titlu_dreapta}</span></a> {stele}</div>
    <div class="LMbox">
      <p class="black">Plecare: <strong class="blue bigger-13em">{data_plecare}</strong></p>
      <span class="black">{denumire_transport}</span> | <span class="black">{nr_zile_nou}</span> | <span class="black">{masa}</span>
    </div>
    <div class="price red">
      <span>{pret_minim}</span> {exprimare_pret}
	  <a href="{link_h}" style="margin-top:6px; display:block;" target="_blank"><img src="/images/button_detalii.png" alt="detalii" /></a>
    </div>
  </div>
</div></div>
