<div class="afisare-lastminute-index NEW-round8px clearfix">
  <a href="{link}"><img width="50" height="50" class="images NEW-round6px" alt="{denumire_hotel}" src="{poza1}" /></a>
  <div class="float-left">
	<div class="titlu"><a href="{link}" class="link-blue" title="{denumire_hotel}">{denumire_hotel}</a> - <span class="black">{denumire_zona} {denumire_tara}</span></div>
    <div class="clearfix">
	  <span class="col-left bold">{stele}</span>
      <span class="col-middle">{nr_zile_nou}</span>
      <span class="col-right bold blue bigger-11em">{data_plecare}</span>
    </div>
    <div class="clearfix">
      <span class="col-left">{denumire_transport}</span>
      <span class="col-middle">{masa}</span>
      <span class="col-right"><span class="pret red">{pret_minim}</span> {exprimare_pret}</span>
    </div>
  </div>
</div>
