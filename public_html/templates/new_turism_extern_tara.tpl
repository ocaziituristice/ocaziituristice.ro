<div class="afisare_oferta_lunga">
  <img class="thumbnail" src="{poza1}" alt="{denumire_hotel}" width="100" height="100" onclick="location.href='{link}';" style="cursor:pointer;" />
  <div class="titlu">
    <a href="{link}" title="{denumire_oferta}" style="padding-left:8px;"> {denumire_oferta} {stele}</a>
    <span class="blue" style="float:right"><strong>{titlu_dreapta}</strong></span>
  </div>
  <div class="clear-right"></div>
  <div style="float:right; padding:0 10px 0 20px; width:60px; display:block;">{poza_fiu}</div>
  <div class="linkedPrice" style="float:right; width:200px;">de la <font style="font-size:18px;">{pret_minim}</font> {exprimare_pret}</div>
  <div class="area">{titlu_localizare}</div>
  <div class="area">Transport: <strong>{denumire_transport}</strong></div>
  <div class="area">Tip masa: <strong>{masa}</strong></div>
  <div align="right" class="veziDetaliiBlue" onclick="location.href='{link}';" style="float:right;"><i>{denumire_oferta}</i></div>
  <div class="durata">{nr_zile}</div>
  <br class="clear" />
</div>
