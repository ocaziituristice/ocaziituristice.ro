<div class="afisare-oferta"><div class="normal NEW-round8px clearfix">
  <div class="float-left" style="margin-right:8px;">
	<div class="poza"><img src="{poza1}" alt="{denumire_hotel}" />{early}</div>
  </div>
  <h2 class="blue"><a href="{link}" title="{denumire_hotel}">{denumire_hotel}</a></h2>
  <div class="infos">
    <p class="titlu">Continent:</p><p class="valoare">{denumire_continent}</p>
    <p class="titlu">Tari vizitate:</p><p class="valoare">{tari_vizitate}</p>
    <p class="titlu">Plecari</p><p class="valoare blue bold">{plecare}</p>
  </div>
  <div class="infos2">
    <p>{denumire_transport}</p>
    <p>{nr_zile_nou}</p>
  </div>
  <div class="price red">de la <span>{pret_minim}</span> {exprimare_pret}</div>
  <a href="{link}" class="vezi-detalii"><img src="http://www.ocaziituristice.ro/images/button_vezi_detalii_green.png" alt="vezi detalii" /></a>
</div></div>
