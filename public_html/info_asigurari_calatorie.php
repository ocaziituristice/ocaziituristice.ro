<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); 
$meta_index = "noindex,follow";
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Asigurari de calatorie - <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Asigurari de calatorie oferite de <?php echo $denumire_agentie; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">

        <span class="titlu_modala">Asigurări de călătorie</span>
        
        <div class="Hline"></div>
        
        <div class="text-justify pad20 article">
        
          <div class="asigurare-title white" style="background:#00B2EF;"><a href="/files/travel.pdf" target="_blank" rel="nofollow">Pachetul <strong>TRAVEL</strong></a></div>
          <a href="/files/travel.pdf" target="_blank" class="pad15 link-blue bigger-12em" rel="nofollow">Vezi condițiile și tarifele pachetului <img src="/images/link_extern.png" alt="deschide link"></a>
          <div class="block bigger-13em bold red">* Recomandarea Agenției</div>
          <p class="pad10-0">Indiferent de destinaţia pe care o alegi, pachetul Travel este alegerea perfectă asigurându-ţi protecţie atât în cazul anulării călătoriei, cât şi în situaţia unei îmbolnăviri sau accidentări pe parcursul călătoriei. Pachetul Travel include protecţie simultană la anulare pentru până la 7 persoane.</p>
          
          <br><br>
        
          <div class="asigurare-title white" style="background:#ED1C24;"><a href="/files/clasic.pdf" target="_blank" rel="nofollow">Pachetul <strong>CLASIC</strong></a></div>
          <a href="/files/clasic.pdf" target="_blank" class="pad15 link-blue bigger-12em" rel="nofollow">Vezi condițiile și tarifele pachetului <img src="/images/link_extern.png" alt="deschide link"></a>
          <p class="pad10-0">Plecaţi într-un city break sau într-o călătorie de afaceri?</p>
          <p>Nu uitaţi că puteţi încheia asigurarea de călătorie Clasic, pentru a putea conta pe ajutorul Mondial Assistance, în cazul unei îmbolnăviri, a pierderii sau deteriorării bagajului de călătorie sau în cazul în care aţi provocat fără voie daune persoanelor sau obiectelor.</p>
          
          <br><br>
        
          <div class="asigurare-title white" style="background:#0078BF;"><a href="/files/premium.pdf" target="_blank" rel="nofollow">Pachetul <strong>PREMIUM</strong></a></div>
          <a href="/files/premium.pdf" target="_blank" class="pad15 link-blue bigger-12em" rel="nofollow">Vezi condițiile și tarifele pachetului <img src="/images/link_extern.png" alt="deschide link"></a>
          <p class="pad10-0">Vă doriţi o călătorie de vis într-o destinaţie exotică, plecaţi în luna de miere, călătoriţi împreună cu copiii sau sunteţi o persoana în vârstă ?</p>
          <p>Pachetul Premium vă poate oferi siguranţa necesară pentru o vacanţă reuşită. Asigurarea medicală, protecţia în cazul acutizării unei boli cronice, asigurarea de bagaje, protecţia în cazul întreruperii călătoriei sau sosirii cu întârziere la destinaţia de vacanţă, costurile suplimentare (biletul de avion de întoarcere, costurile de transport pentru deplasarea unei rude în cazul unei spitalizări), asigurarea de răspundere civilă privată sunt doar câteva argumente pentru alegerea unui pachet de protecţie complex.</p>
          
          <br><br>
        
          <div class="asigurare-title white" style="background:#F57B20;"><a href="/files/storno.pdf" target="_blank" rel="nofollow">Pachetul <strong>STORNO</strong></a></div>
          <a href="/files/storno.pdf" target="_blank" class="pad15 link-blue bigger-12em" rel="nofollow">Vezi condițiile și tarifele pachetului <img src="/images/link_extern.png" alt="deschide link"></a>
          <p class="pad10-0">Nu ezitaţi să încheiaţi asigurarea Storno pentru a vă proteja investiţia în pachetul turistic, atât înainte de plecare în cazul anulării călătoriei, cât şi după plecare în cazul în care o situaţie neprevăzută conform Condiţiilor Generale de Asigurare vă obligă să vă întrerupeţi călătoria.</p>
          
          <br><br>
        
          <div class="asigurare-title white" style="background:#009B82;"><a href="/files/wintersports.pdf" target="_blank" rel="nofollow">Pachetul <strong>WINTER SPORTS</strong></a></div>
          <a href="/files/wintersports.pdf" target="_blank" class="pad15 link-blue bigger-12em" rel="nofollow">Vezi condițiile și tarifele pachetului <img src="/images/link_extern.png" alt="deschide link"></a>
          <p class="pad10-0">Iubiţi iarna şi vă relaxează practicarea sporturilor de iarnă?</p>
          <p>Se poate întâmpla să primiţi cu întârziere bagajul, să pierdeţi sau să deterioraţi echipamentul de schi închiriat, să suferiţi o accidentare sau să accidentaţi fară voie o altă persoană. Pentru toate aceste situaţii, pachetul cu protecţii multiple Winter Sports este garanţia pentru o vacanţă relaxantă la schi.</p>
          
          <br><br>
        
          <div class="asigurare-title white" style="background:#9C5708;"><a href="/files/auto asistenta.pdf" target="_blank" rel="nofollow">Pachetul <strong>AUTO ASISTENŢĂ</strong></a></div>
          <a href="/files/auto asistenta.pdf" target="_blank" class="pad15 link-blue bigger-12em" rel="nofollow">Vezi condițiile și tarifele pachetului <img src="/images/link_extern.png" alt="deschide link"></a>
          <p class="pad10-0">Pentru a putea beneficia de servicii este obligatorie informarea centralei pentru apeluri de urgență cu program de lucru de 24 de ore în caz de pană, avarie sau furtul autovehiculului.</p>
          
          <br><br>
        
          <div class="asigurare-title white" style="background:#B60050;"><a href="/files/multitrip.pdf" target="_blank" rel="nofollow">Pachetul <strong>MULTITRIP</strong></a></div>
          <a href="/files/multitrip.pdf" target="_blank" class="pad15 link-blue bigger-12em" rel="nofollow">Vezi condițiile și tarifele pachetului <img src="/images/link_extern.png" alt="deschide link"></a>
          <p class="pad10-0">Asistenţă de urgenţă la nivel mondial 24 de ore pe zi, 365 de zile pe an. Vă garantăm ajutor competent oriunde în lume, în caz de: îmbolnăvire, accident, avocat, translator, pierderea documentelor de călătorie, pierderea mijloacelor de plată a călătoriei.</p>
          
        </div>
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>