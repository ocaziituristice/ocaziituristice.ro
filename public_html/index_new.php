<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur_normal.php');
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Agentie de turism online | Ocaziituristice.ro</title>
<meta name="description" content="Mergi la sigur cu Ocaziituristice.ro. Agentie de turism din Bucuresti cu o oferta variata pentru pachete turistice in Romania si externe. Profita acum de reduceri" />
<meta name="keywords" content="agentie de turism, sejururi, last minute, early booking, circuite, oferte revelion, city break, oferte balneo" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner" style="padding:5px 0;">
    <div class="NEW-column-full">
      <div id="NEW-index" class="clearfix">
      
        <div class="tmain blue"><h1>Agentia de turism Ocaziituristice.ro</h1> va pune la dispozitie o gama variata de pachete turistice.</div>
        
        <div class="search NEW-round8px"><div class="inner NEW-round6px">
		  <div class="chapter-title black">Cauta vacanta:</div>
          <div id="afis_filtru"></div>
        </div></div>
        
        <div id="jflowSlider" class="float-right">
          <div id="mySlides">
            <div id="slide1" class="slide">
              <a href="/sejur-romania/litoral/"><img src="/images/index/banner_litoral_romania_03-01-2013.gif" alt="Litoral Romania" class="NEW-round8px" /></a>
            </div>   
            <div id="slide2" class="slide">    
              <a href="/oferte-paste/bulgaria/"><img src="/images/index/banner_paste-bulgaria_03-01-2012.jpg" alt="Paste Bulgaria" class="NEW-round8px" /></a>
              <div class="slideContent">
                <h3>Super Oferte de Paste !</h3>
                <p>Petrece Sarbatorile Pascale pe litoralul Bulgaresc.</p>
              </div>
            </div>    	
          </div>
          <div id="myController">
            <span class="jFlowControl">1</span>
            <span class="jFlowControl">2</span>
          </div>
          <div class="jFlowPrev"></div>
          <div class="jFlowNext"></div>
        </div>
        
        <br class="clear">
        
        <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix">
          <h2 class="blue float-left"><a href="/oferte-litoral/" title="Oferte Litoral 2013">Litoral 2013</a></h2>
          <a href="/oferte-litoral/" class="float-right link-black" rel="nofollow">vezi toate ofertele Litoral 2013</a>
          <br class="clear">
          <div class="item">
            <a href="/sejur-romania/litoral/mamaia/" rel="nofollow"><img src="/images/index/litoral-mamaia.jpg" alt="Litoral Mamaia" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/mamaia/" class="link-black">Litoral 2013 Mamaia</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/eforie-nord/" rel="nofollow"><img src="/images/index/litoral-eforie-nord.jpg" alt="Litoral Eforie Nord" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/eforie-nord/" class="link-black">Litoral 2013 Eforie Nord</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/neptun/" rel="nofollow"><img src="/images/index/litoral-neptun.jpg" alt="Litoral Neptun" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/neptun/" class="link-black">Litoral 2013 Neptun</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/jupiter/" rel="nofollow"><img src="/images/index/litoral-jupiter.jpg" alt="Litoral Jupiter" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/jupiter/" class="link-black">Litoral 2013 Jupiter</a>
          </div>
          <div class="item">
            <a href="/sejur-romania/litoral/saturn/" rel="nofollow"><img src="/images/index/litoral-saturn.jpg" alt="Litoral Saturn" class="NEW-round8px"></a>
            <a href="/sejur-romania/litoral/saturn/" class="link-black">Litoral 2013 Saturn</a>
          </div>
          <br class="clear">
        </div></div>
        
        <br class="clear">
        
        <div class="chenar chn-color-orange NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h2 class="red"><a href="/oferte-paste/" title="Oferte Paste 2013">Paste 2013</a></h2>
          <a href="/oferte-paste/" rel="nofollow"><img src="/images/index/banner_paste-index_03-01-2012.jpg" alt="Paste 2013" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0;">
<?php
$c1_link[1]='/oferte-paste/bulgaria/litoral/';
$c1_title[1]='Paste 2013 Litoral Bulgaria';
$c1_titlu[1]='Litoral - Bulgaria';
$c1_pret[1]='80';
$c1_moneda[1]='EUR';

$c1_link[2]='/oferte-paste/romania/valea-prahovei/';
$c1_title[2]='Paste 2013 Valea Prahovei Romania';
$c1_titlu[2]='Valea Prahovei - Romania';
$c1_pret[2]='365';
$c1_moneda[2]='RON';

$c1_link[3]='/oferte-paste/romania/litoral/';
$c1_title[3]='Paste 2013 Litoral Romania';
$c1_titlu[3]='Litoral - Romania';
$c1_pret[3]='224';
$c1_moneda[3]='RON';

$c1_link[4]='/oferte-paste/romania/statiuni-balneare/';
$c1_title[4]='Paste 2013 Statiuni Balneare Romania';
$c1_titlu[4]='Statiuni Balneare - Romania';
$c1_pret[4]='160';
$c1_moneda[4]='RON';

$c1_link[5]='/oferte-paste/italia/';
$c1_title[5]='Paste 2013 Italia';
$c1_titlu[5]='Italia';
$c1_pret[5]='288';
$c1_moneda[5]='EUR';

$c1_link[6]='/oferte-paste/spania/costa-del-sol/';
$c1_title[6]='Paste 2013 Costa del Sol Spania';
$c1_titlu[6]='Costa del Sol - Spania';
$c1_pret[6]='369';
$c1_moneda[6]='EUR';

for($i=1; $i<=6; $i++) {
?>
            <div class="item2 clearfix">
              <a href="<?php echo $c1_link[$i]; ?>" title="<?php echo $c1_title[$i]; ?>"><?php echo $c1_titlu[$i]; ?></a>
              <span class="tarif"><span class="value"><?php echo $c1_pret[$i]; ?></span> <?php echo $c1_moneda[$i]; ?></span>
            </div>
<?php } ?>
		  </div>
		  <div class="text-right"><a href="/oferte-paste/" rel="nofollow" class="link-black">vezi toate ofertele Paste 2013</a></div>
        </div></div>
        
        <div class="chenar chn-color-blue NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h2 class="blue"><a href="/oferte-program-pentru-seniori/" title="Oferte Programe Seniori">Programe Seniori</a></h2>
          <a href="/oferte-program-pentru-seniori/" rel="nofollow"><img src="/images/index/banner_seniori-index_04-01-2012.jpg" alt="Programe Seniori" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0;">
<?php
$c2_link[1]='/oferte-program-pentru-seniori/spania/';
$c2_title[1]='Program Seniori Spania';
$c2_titlu[1]='Spania';
$c2_pret[1]='339';
$c2_moneda[1]='EUR';

$c2_link[2]='/oferte-program-pentru-seniori/italia/';
$c2_title[2]='Program Seniori Italia';
$c2_titlu[2]='Italia';
$c2_pret[2]='288';
$c2_moneda[2]='EUR';

$c2_link[3]='/oferte-program-pentru-seniori/cipru/';
$c2_title[3]='Program Seniori Cipru';
$c2_titlu[3]='Cipru';
$c2_pret[3]='259';
$c2_moneda[3]='EUR';

$c2_link[4]='/oferte-program-pentru-seniori/portugalia/';
$c2_title[4]='Program Seniori Portugalia';
$c2_titlu[4]='Portugalia';
$c2_pret[4]='344';
$c2_moneda[4]='EUR';

$c2_link[5]='/oferte-program-pentru-seniori/israel/';
$c2_title[5]='Program Seniori Israel';
$c2_titlu[5]='Israel';
$c2_pret[5]='375';
$c2_moneda[5]='EUR';

$c2_link[6]='/oferte-program-pentru-seniori/maroc/';
$c2_title[6]='Program Seniori Maroc';
$c2_titlu[6]='Maroc';
$c2_pret[6]='595';
$c2_moneda[6]='EUR';

for($i=1; $i<=6; $i++) {
?>
            <div class="item2 clearfix">
              <a href="<?php echo $c2_link[$i]; ?>" title="<?php echo $c2_title[$i]; ?>"><?php echo $c2_titlu[$i]; ?></a>
              <span class="tarif"><span class="value"><?php echo $c2_pret[$i]; ?></span> <?php echo $c2_moneda[$i]; ?></span>
            </div>
<?php } ?>
          </div>
		  <div class="text-right"><a href="/oferte-program-pentru-seniori/" rel="nofollow" class="link-black">vezi toate ofertele Programe Seniori</a></div>
        </div></div>
        
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px;"><div class="inner NEW-round6px clearfix">
          <h2 class="green"><a href="/oferte-city-break/" title="Oferte City Break">City Break</a></h2>
          <a href="/oferte-city-break/" rel="nofollow"><img src="/images/index/banner_city-break-index_04-01-2012.jpg" alt="City Break" class="NEW-round8px"></a>
          <div style="padding:5px 0 10px 0;">
<?php
$c3_link[1]='/oferte-city-break/turcia/istanbul/istanbul/';
$c3_title[1]='City Break Istanbul';
$c3_titlu[1]='Istanbul';
$c3_pret[1]='99';
$c3_moneda[1]='EUR';

$c3_link[2]='/oferte-city-break/franta/paris/paris/';
$c3_title[2]='City Break Paris';
$c3_titlu[2]='Paris';
$c3_pret[2]='399';
$c3_moneda[2]='EUR';

$c3_link[3]='/oferte-city-break/austria/viena/viena/';
$c3_title[3]='City Break Viena';
$c3_titlu[3]='Viena';
$c3_pret[3]='207';
$c3_moneda[3]='EUR';

$c3_link[4]='/oferte-city-break/spania/catalonia/barcelona/';
$c3_title[4]='City Break Barcelona';
$c3_titlu[4]='Barcelona';
$c3_pret[4]='525';
$c3_moneda[4]='EUR';

$c3_link[5]='/oferte-city-break/cehia/praga/praga/';
$c3_title[5]='City Break Praga';
$c3_titlu[5]='Praga';
$c3_pret[5]='258';
$c3_moneda[5]='EUR';

$c3_link[6]='/oferte-city-break/italia/roma/roma/';
$c3_title[6]='City Break Roma';
$c3_titlu[6]='Roma';
$c3_pret[6]='329';
$c3_moneda[6]='EUR';

for($i=1; $i<=6; $i++) {
?>
            <div class="item2 clearfix">
              <a href="<?php echo $c3_link[$i]; ?>" title="<?php echo $c3_title[$i]; ?>"><?php echo $c3_titlu[$i]; ?></a>
              <span class="tarif"><span class="value"><?php echo $c3_pret[$i]; ?></span> <?php echo $c3_moneda[$i]; ?></span>
            </div>
<?php } ?>
          </div>
		  <div class="text-right"><a href="/oferte-city-break/" rel="nofollow" class="link-black">vezi toate ofertele City Break</a></div>
        </div></div>
        
        <br class="clear">
        
        <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix">
          <h2 class="blue float-left"><a href="/circuite/" title="Circuite">Circuite</a></h2>
          <a href="/circuite/" class="float-right link-black" rel="nofollow">vezi toate Circuitele</a>
          <br class="clear">
<?php
$of2_link[1]='/hoteluri-bethlehem/seniori-in-israel-2012-2013/seniori-in-israel-2012-2013-5527.html';
$of2_titlu[1]='Seniori in Israel 2012 - 2013';
$of2_poza[1]='/images/index/001.jpg';
$of2_tarif[1]='de la <span>375 &euro;</span> sejur/pers';
$of2_transport[1]='Avion';
$of2_durata[1]='7 zile / 6 nopti';
$of2_masa[1]='Demipensiune';

$of2_link[2]='/circuit/europa/circuit-in-10-capitale-europene-6876.html';
$of2_titlu[2]='Circuit in 10 capitale europene';
$of2_poza[2]='/images/index/002.jpg';
$of2_tarif[2]='de la <span>720 &euro;</span> sejur/pers';
$of2_transport[2]='Autocar';
$of2_durata[2]='16 zile / 15 nopti';
$of2_masa[2]='Mic Dejun';

$of2_link[3]='/circuit/europa/circuit-serbia-croatia-muntenegru-2013-6853.html';
$of2_titlu[3]='Circuit Serbia - Croatia - Muntenegru 2013';
$of2_poza[3]='/images/index/003.jpg';
$of2_tarif[3]='de la <span>379 &euro;</span> sejur/pers';
$of2_transport[3]='Autocar';
$of2_durata[3]='10 zile / 9 nopti';
$of2_masa[3]='Mic Dejun';

$of2_link[4]='/circuit/asia/turcia-2013-capadokia-6795.html';
$of2_titlu[4]='Circuit Turcia - Cappadokia 2013';
$of2_poza[4]='/images/index/004.jpg';
$of2_tarif[4]='de la <span>269 &euro;</span> sejur/pers';
$of2_transport[4]='Autocar';
$of2_durata[4]='8 zile / 7 nopti';
$of2_masa[4]='Mic Dejun';

for($i=1; $i<=4; $i++) {
?>
          <div class="of2cols NEW-round8px clearfix">
            <a href="<?php echo $of2_link[$i]; ?>" rel="nofollow"><img src="<?php echo $of2_poza[$i]; ?>" alt="<?php echo strip_tags($of2_titlu[$i]); ?>" width="130" height="90" class="image" /></a>
            <div class="titlu"><a href="<?php echo $of2_link[$i]; ?>" class="link-blue" title="<?php echo strip_tags($of2_titlu[$i]); ?>"><?php echo $of2_titlu[$i]; ?></a></div>
            <div class="pret blue"><?php echo $of2_tarif[$i]; ?></div>
            <?php if($of2_transport[$i]!='') { ?><div class="field"><p class="camp">Transport:</p><p class="valoare"><?php echo $of2_transport[$i]; ?></p></div><?php } ?>
            <?php if($of2_durata[$i]!='') { ?><div class="field"><p class="camp">Durata:</p><p class="valoare"><?php echo $of2_durata[$i]; ?></p></div><?php } ?>
            <?php if($of2_masa[$i]!='') { ?><div class="field"><p class="camp">Masa:</p><p class="valoare"><?php echo $of2_masa[$i]; ?></p></div><?php } ?>
          </div>
<?php } ?>
          <br class="clear">
        </div></div>
        
        <br class="clear">
        
        <div class="chenar chn-color-green NEW-round8px float-left" style="width:304px; margin-right:10px;"><div class="inner NEW-round6px clearfix">
          <h2 class="green"><a href="/early-booking/" title="Early Booking">Early Booking</a></h2>

          <div class="item4 clearfix">
            <a href="/sejur-romania/?optiuni=da&early-booking=da" title="Early Booking Romania" class="stanga"><img src="/images/index/romania-logo.jpg" alt="Early Booking Romania"></a>
            <div class="dreapta big-procent blue">30%</div>
          </div>

          <div class="item4 clearfix">
            <a href="/sejur-bulgaria/?optiuni=da&early-booking=da" title="Early Booking Bulgaria" class="stanga"><img src="/images/index/bulgaria-logo.jpg" alt="Early Booking Bulgaria"></a>
            <div class="dreapta big-procent blue">25%</div>
          </div>

          <div class="item4 clearfix">
            <a href="/sejur-turcia/?optiuni=da&early-booking=da" title="Early Booking Turcia" class="stanga"><img src="/images/index/turcia-logo.jpg" alt="Early Booking Turcia"></a>
            <div class="dreapta big-procent blue">25%</div>
          </div>

          <div class="item4 clearfix">
            <a href="/sejur-grecia/?optiuni=da&early-booking=da" title="Early Booking Grecia" class="stanga"><img src="/images/index/grecia-logo.jpg" alt="Early Booking Grecia"></a>
            <div class="dreapta big-procent blue">25%</div>
          </div>

        </div></div>
        
        <div class="fb-like-box float-left" style="width:320px; margin-right:10px;" data-href="http://www.facebook.com/OcaziiTuristice.ro" data-width="320" data-height="435" data-show-faces="true" data-stream="false" data-header="true"></div>
        
        <div class="float-left" style="width:320px;">

          <div class="chenar chn-color-blue NEW-round8px"><div class="inner NEW-round6px clearfix">
            <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_form.php"); ?>
          </div></div>

          <div class="chenar chn-color-grey NEW-round8px"><div class="inner NEW-round6px clearfix">
            <h2 class="black"><a href="http://blog.ocaziituristice.ro">Blog Ocaziituristice.ro</a></h2>
<?php 
require_once('magpierss/rss_fetch.inc');
$url = 'http://blog.ocaziituristice.ro/feed';
$rss = fetch_rss($url);
//echo "Site: ", $rss->channel['title'], "<br>";

$sf=sizeof($rss->items);
for($i=0;$i<$sf-8;$i++) {

    $item = $rss->items[$i];
	//print_r($item);
	$title = $item[title];
	$url = $item[link];
	$desc = $item[description];
	$date = date('d M Y, H:i',$item[date_timestamp]);
?>
<div class="item3">
  <a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="link-blue bold" target="_blank"><?php echo $title; ?></a>
  <div class="desc"><?php echo truncate_str($desc, 120).' [...]'; ?></div>
  <div class="date"><?php echo $date; ?></div>
</div>
<?php } ?>
          </div></div>

        </div>
        
        <br class="clear">
        
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
<script type="text/javascript">
	$("#afis_filtru").load("<?php echo $sitepath; ?>filtru.php");
</script>

<script src="/js/jflow.plus.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $("#myController").jFlow({
			controller: ".jFlowControl", // must be class, use . sign
			slideWrapper : "#jFlowSlider", // must be id, use # sign
			slides: "#mySlides",  // the div where all your sliding divs are nested in
			selectedWrapper: "jFlowSelected",  // just pure text, no sign
			effect: "flow", //this is the slide effect (rewind or flow)
			width: "630px",  // this is the width for the content-slider
			height: "250px",  // this is the height for the content-slider
			duration: 400,  // time in milliseconds to transition one slide
			pause: 4000, //time between transitions
			prev: ".jFlowPrev", // must be class, use . sign
			next: ".jFlowNext", // must be class, use . sign
			auto: true	
    });
});
</script>
</body>
</html>