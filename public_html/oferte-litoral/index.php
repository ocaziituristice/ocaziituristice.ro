<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
$tip = "litoral";
$linktip = "oferte-litoral";
$dentip = "Litoral";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'";
if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri'];
?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Litoral <?php echo date("Y"); ?> | ocaziituristice.ro</title>
<meta name="description" content="Oferte pentru litoral <?php echo date("Y"); ?> in Romania, Bulgaria, Grecia, Turcia" />
<meta name="keywords" content="Litoral, Oferte Litoral, Sejururi Litoral, Cazare Litoral" />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Oferte Litoral <?php echo date("Y"); ?></h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="litoral-tari NEW-round8px article">
          <a href="<?php echo $sitepath; ?>sejur-romania/litoral/" rel="nofollow"><img src="<?php echo $imgpath; ?>/index/romania-logo.jpg" alt="Litoral Romania" class="img-logo" /></a>
          <h2>Litoral Romania</h2>
          <p class="text-justify">Litoralul romanesc la Marea Neagra, cea mai exploatata zona turistica din Romania, se intinde de la granita cu Ucraina (Nord) pana la cea cu Bulgaria (Sud). 
Tarmul este deosebit de variat, format din forme usor ondulate, cu capuri accentuate si golfuri prelungite adanc pe vaile dobrogene, cu faleze, plaje si cordoane de nisip. Marea, in actiunea ei erodanta, a facut ca tarmul sa se retraga continuu, fie prin transformarea golfurilor in limane si lagune, fie prin abraziunea in dreptul promotoriilor. Falezele, care reprezinta doua treimi din lungimea litoralului, au inaltimi ce variaza intre 20 si 40 m.

</p>
          <br class="clear" />
          <div><img src="<?php echo $sitepath; ?>oferte-litoral/images/img_romania.jpg" alt="Litoral Romania" class="NEW-round8px" /></div><br />
          <p class="text-justify">Stralucirea soarelui pe litoralul romanesc are o medie anuala de 2.500 de ore, identica cu cea de pe plajele coastei dalmatine (Croatia) si apropiata de cea a coastelor mediteraneene. In zona Agigea se afla unica rezervatie de dune marine din Romania.
<?php /*?>Cele mai importante statiuni turistice sunt:
Mamaia, considerata o perla a litoralului romanesc, este situata pe cordonul de nisip ce separa apele sarate ale marii de apele dulci ale lacului Siutghiol. Plaja Mamaia este cea mai mare plaja a litoralului romanesc.
A doua statiune ca marime de pe litoral in ceea ce priveste numarul de locuri este Eforie Nord.
Sora ei, Eforie Sud este situata pe un pinten calcaros ce constituie legatura malului drept al vaii Techirghiol cu faleza Marii Negre.
Costinesti, capitala vacantelor estivale ale tineretului, se afla in perimetrul satului cu acelasi nume.
Olimp  este prima dintre cele sase statiuni ale litoralului sirag de statiuni cunoscute sub numele de Mangalia-Nord, apoi Neptunul, gradina litoralului, situat intre Olimp si Jupiter, Aurora, cea mai mica statiune din salba de statiuni Mangalia-Nord.Saturn este cea mai sudica statiune sezoniera a litoralului, in prelungirea orasului Mangalia.
Mangalia, vechea cetate antica Callatis este orasul turistic romanesc cel mai sudic.
2 Mai este un sat situat pe malul marii, la sud de municipiul Mangalia, iar la 3 km de 2 Mai se afla Vama Veche, localitate de frontiera, amplasata pe marginea unui larg golf maritim.<?php */?></p>
		  <?php /*?><div class="links clearfix">
	
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-romania/litoral/mamaia/" class="link-blue" title="Cazare Mamaia">Cazare Mamaia</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-romania/litoral/eforie-nord/" class="link-blue" title="Cazare Eforie Nord">Cazare Eforie Nord</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-romania/litoral/costinesti/" class="link-blue" title="Cazare Costinesti">Cazare Costinesti</a></div>
          </div><?php */?>
		
          <div class="text-right">vezi <a href="<?php echo $sitepath; ?>sejur-romania/litoral/" class="link-black" title="oferte litoral Romania"><strong>ofertele pentru litoralul Romanesc</strong></a> &raquo;</div>
        </div>
        
        <div class="litoral-tari NEW-round8px article">
          <a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/" rel="nofollow"><img src="<?php echo $imgpath; ?>/index/bulgaria-logo.jpg" alt="Logo Bulgaria" class="img-logo" /></a>
          <h2>Litoral Bulgaria</h2>
          <p class="text-justify">Popularitatea litoralului Bulgaresc al Marii Negre a fost in continua crestere in ultimii ani.
Industria turistica din Bulgaria a cunoscut si ea o crestere robusta, ajungand una din principalele surse de venit ale bugetului de stat. Oferta de litoral din Bulgaria este orientata catre clientii cu bugete de vacanta mai reduse, oferind o alternativa ieftina statiunilor din vestul Europei insa cu o calitate a serviciilor ce nu scade mult sub cele vest Europene in statiunile de top din Bulgaria, cum ar fi Albena, Nisipurile de Aur, Sunny Beach, Duni.</p>
          <br class="clear" /><br />
          <div><img src="<?php echo $sitepath; ?>oferte-litoral/images/img_bulgaria.jpg" alt="Litoral Bulgaria" class="NEW-round8px" /></div><br />
          <p class="text-justify">Plaja este asemanatoare celei de pe litoralul Romanesc avand nisipul fin, iar intrarea in apa este lina in majoritatea statiunilor de pe litoralul Bulgaresc.
Clima este si ea asemanatoare celei de pe litoralul romanesc, cu temperaturi medii in timpul verii de : 27 ° C in aer si 24 ° C in apa de mare. <?php /*?>In 2010, conform publicatiei Scotsman, Bulgaria s-a aflat pe locul 4 in topul destinatiilor turistice preferate ale britanicilor. Cele mai solicitate statiuni sunt: Albena, una dintre cele mai frumoase statiuni de pe coasta Marii Negre, beneficiind de o locatie excelenta, ideala pentru serviciile de calitate superioara puse la dispozitie de catre gazde. Albena este o incantare pentru sejururile pe litoral pentru toate varstele; Nisipurile de Aur, una din statiunile specifice litoralului bulgaresc, unde ploua putin, iar caderile abundente de zapada sunt extrem de rare. Combinatia dintre nisipul fin si auriu, apa limpede precum cristalul si vegetatia abundenta constituie punctul de atractie pentru turistii din toata Europa. Statiunea este la 17 km de Varna; Statiunea Konstantin si Elena, prima statiune de vara din Bulgaria, situata la 10 km de Varna, inconjurata de un minunat parc. Hotelurile noi, de o calitate excelenta beneficiaza de izvoarele de apa minerala, in statiune fiind numeroase piscine cu apa termala; 
Riviera Holiday Club este o statiune de lux, amplasata intr-un parc format din copaci seculari pe marginea unei plaje extraordinare, la 17 km de Varna, in imediata apropiere a statiunii Nisipurile de Aur; Nessebar este unul dintre cele mai vechi orase din Europa amplasat intr-o mica peninsula, in apropiere de cea mai mare statiune din Bulgaria – Sunny Beach.<?php */?></p>
		  <?php /*?><div class="links clearfix">
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/albena/" class="link-blue" title="Cazare Albena">Cazare Albena</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/nisipurile-de-aur/" class="link-blue" title="Cazare Nisipurile de Aur" style="letter-spacing:-1px;">Cazare Nisipurile de Aur</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/sunny-beach/" class="link-blue" title="Cazare Sunny Beach">Cazare Sunny Beach</a></div>
          </div><?php */?>
          <div class="text-right">vezi <a href="<?php echo $sitepath; ?>sejur-bulgaria/litoral/" class="link-black" title="oferte Litoral Bulgaria"><strong>ofertele din Bulgaria</strong></a> &raquo;</div>
        </div>
        
        <div class="litoral-tari NEW-round8px article">
          <a href="<?php echo $sitepath; ?>sejur-grecia/" rel="nofollow"><img src="<?php echo $imgpath; ?>/index/grecia-logo.jpg" alt="Logo Grecia" class="img-logo" /></a>
          <h2>Litoral Grecia</h2>
          <p class="text-justify">Grecia, leaganul civilizatiei europene, este situata in sud-estul Europei, in Peninsula Balcanica. Se invecineaza la nord cu Bulgaria, cu Republica Macedonia si Albania, la est cu Turcia si Marea Egee, la vest cu Marea Ionica iar la sud cu Marea Mediterana. 
Neindoielnic, Grecia este una dintre cele mai stralucite civilizatii ale Antichitatii. Marturie stau neasemuitele sale simboluri. Templele se impun prin simplitate si perfectiune artistica.</p>
          <br class="clear" />
          <div><img src="<?php echo $sitepath; ?>oferte-litoral/images/img_grecia.jpg" alt="Litoral Grecia" class="NEW-round8px" /></div><br />
          <p class="text-justify">Grecia, ca destinatie turistica, se poate imparti in partea continentala, pentru care sunt recomandate ca destinatii  Atena, Salonic si partea insulara cu insulele Creta, Thassos, Zakynthos, Corfu, Mykonos, Santorini, Rodos, Kos, Naxos, Lefkada, Evia dar si Halkidiki, Paralia, Parga, Meteore ca destinatii de sejururi de neuitat. <?php /*?>Cele mai preferate locuri pentru sejur litoral sunt: Paralia, Insula Creta, peninsula Halkidiki (Kassandra, Sithonia), locatii de pe coasta Attica (Corfu, Kalamata, Kalymnos, Mykonos, Preveza, Rodos, Skiathos si Zakynthos), insula Thassos,  insula Santorini, Asprovalta, Olympic Beach, Nei Pori, Platamonas. Cele mai raspandite mancaruri traditionale, solicitate de turisti in timpul sejururlui pe litoral sunt: gyros – carne de porc sau pui (rareori de vita) prajita, cu sosuri si legume, invelita intr-o lipie prajita; souvlaki – frigarui din carne; tzatziki – un sos din iaurt, ulei de masline, usturoi, castraveti si marar sau menta; skordhalia – cartofi fierti amestecati cu usturoi, serviti de obicei cu peste prajit si sarat.
La desert puteti incerca produsele de patiserie (fourno), tiropita – placinta cu branza, spanakopita – placinta cu spanac, bougatsa – placinta cu crema, sau horiatiko psomi – o paine traditionala, crocanta, foarte gustoasa, baklava – o prajitura cu miere si nuci macinate sau galaktobouriko – o placinta asemanatoare cu mille feuille.. Toate acestea merg foarte bine cu o cafea greceasca. La fel de popular este si frappe-ul – cafea ness, apa, zahar, lapte si cuburi de gheata.<?php */?></p>
		  <?php /*?><div class="links clearfix">
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-grecia/corfu/" class="link-blue" title="Sejur Corfu">Sejur Corfu</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-grecia/creta/" class="link-blue" title="Sejur Creta">Sejur Creta</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-grecia/thassos/" class="link-blue" title="Sejur Thassos">Sejur Thassos</a></div>
          </div><?php */?>
          <div class="text-right">vezi <a href="<?php echo $sitepath; ?>sejur-grecia/" class="link-black" title="Litoral Grecia"><strong>ofertele din Grecia</strong></a> &raquo;</div>
        </div>
        
        <div class="litoral-tari NEW-round8px article">
          <a href="<?php echo $sitepath; ?>sejur-turcia/" rel="nofollow"><img src="<?php echo $imgpath; ?>/index/turcia-logo.jpg" alt="Logo Turcia" class="img-logo" /></a>
          <h2>Litoral Turcia</h2>
          <p class="text-justify">Incepand in Europa si intinzandu-se pana in Asia Mica, Turcia se afla la confluenta a doua civilizatii: europeana si orientala. Amplasarea geografica si cadrul natural al tarii sunt pitoresti. Inconjurata de trei mari (Mediterana, Egee si Marea Neagra),  destinatiile de vacanta sunt numeroase si variate in toate perioadele anului, datorita climei blande specifice invecinarii cu bazinul mediteraneean si ariditatii Asiei.

</p>
          <br class="clear" />
          <div><img src="<?php echo $sitepath; ?>oferte-litoral/images/img_turcia.jpg" alt="Litoral Turcia" class="NEW-round8px" /></div><br />
          <p class="text-justify">Coasta Marii Negre este in general umeda si neospitaliera datorita reliefului preponderent muntos, spre deosebire de statiunile infloritoare ale Mediteranei si Egeei, care se bucura de binefacerile climei tropicale. Partea europeana si asiatica a Turciei sunt separate de stramtoarea Bosfor, Marea Marmara si stramtoarea Dardanelle. <?php /*?>In general primavara si toamna sunt cele mai potrivite anotimpuri pentru a vizita Turcia. Datorita climei calde, si, in special vara, nu se recomanda expunerea directa la soare. Purtati palarie si ochelari de soare. In general trebuie sa consumati multe bauturi naturale si sa evitati alcoolul, mai ales in timpul zilei. Trebuie acordata o atentie sporita calitatii apei si alimentelor pe care le consumati. Sunt de preferat alimentele proaspat pregatite. Destinatiile turistice preferate pentru sejururi de litoral sunt cele oferite de coasta Marii Egee si Mediterane. Dar Turcia ofera mai mult decat albastrul marilor si caldura nisipurilor aurii. Calatorul aventurier va fi incantat de exotismul peisajului aspru al cascadelor de calcar de la Pamukkale, de peisajul unic al Capadochiei. Orasele sau porturile sunt puncte de atractie mai ales datorita vechimii lor milenare si vestigilor lasate de istoria bogata in evenimente a Turciei. Bizantismul urmat apoi de Islamism, au lasat fiecare splendori arhitectonice specifice, multe din ele dainuind chiar si pana azi. 
Coasta Marii Egee este un loc plin de istorie. Rezonante ale civilizatiei antice ca Troia, Hierapolis sau Efes alaturi de moderne statiuni de agrement de litoral ca Bodrum sau Marmaris sunt menite sa incante sufletul si trupul.<?php */?></p>
		  <?php /*?><div class="links clearfix">
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-turcia/antalya/" class="link-blue" title="Sejur Antalya">Sejur Antalya</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-turcia/bodrum/" class="link-blue" title="Sejur Bodrum">Sejur Bodrum</a></div>
            <div class="item NEW-round6px"><a href="<?php echo $sitepath; ?>sejur-turcia/kusadasi/" class="link-blue" title="Sejur Kusadasi">Sejur Kusadasi</a></div>
          </div><?php */?>
          <div class="text-right">vezi <a href="<?php echo $sitepath; ?>sejur-turcia/" class="link-black" title="oferte Litoral Turcia"><strong>ofertele de sejur din Turcia</strong></a> &raquo;</div>
        </div>
        
        <br class="clear" /><br />
        
      </div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/oferte_vizualizate.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>