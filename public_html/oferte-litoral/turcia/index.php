<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php");
include_once($_SERVER['DOCUMENT_ROOT']."/config/functii.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/config/functii_pt_afisare.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/includes/class/class_sejururi/class_sejur.php');
$tip = "program pentru seniori";
$linktip = "program-pentru-seniori";
$id_tip_sejur = get_id_tip_sejur($tip);
$tip_fii=get_id_tip_sejur_fii($id_tip_sejur);
$iduri="'".$id_tip_sejur."'"; if($tip_fii['id_tipuri']) $iduri=$iduri.','.$tip_fii['id_tipuri']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Oferte Litoral Turcia, sejururi Litoral Turcia</title>
<meta name="description" content="Litoral Turcia pe nisipurile fierbinti din frumoasele zone litorale Kusadasi, Bodrum, Antalya" />
<meta name="keywords" content="Litoral, Oferte Litoral, Sejururi Litoral, Cazare Litoral" />

<?php $link="http://www.ocaziituristice.ro".$_SERVER['REQUEST_URI']; ?>
<meta name="Publisher" content="ocaziituristice.ro" />
<meta name="ROBOTS" content="INDEX,FOLLOW" />
<meta name="language" content="ro" />
<meta name="revisit-after" content="1 days" />
<meta name="identifier-url" content="<?php echo $link; ?>" />
<meta name="Rating" content="General"  />
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?>
    </div>
    <div class="NEW-column-full">
    
      <div id="NEW-destinatie" class="clearfix">
      
        <h1 class="blue" style="float:left;">Litoral Turcia</h1>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/socials_top.php"); ?>
        
        <br class="clear" />
        <div class="Hline"></div>
        
        <div class="article pad20 bigger-11em">
          <?php /*?><a href="<?php echo $sitepath; ?>descopera-turcia/harta-turcia.html" title="Harta Turcia"><?php */?><img src="http://maps.googleapis.com/maps/api/staticmap?center=38.298559%2c30.662842&amp;zoom=5&amp;size=350x250&amp;maptype=roadmap&amp;sensor=false&amp;language=ro" alt="Litoral Turcia" width="350" height="250" class="NEW-round8px custom-static-map img-right" /><?php /*?></a><?php */?>
          <p align="justify">In general primavara si toamna sunt cele mai potrivite anotimpuri pentru a vizita Turcia. Datorita climei calde, si, in special vara, nu se recomanda expunerea directa la soare. Purtati palarie si ochelari de soare. In general trebuie sa consumati multe bauturi naturale si sa evitati alcoolul, mai ales in timpul zilei. Trebuie acordata o atentie sporita calitatii apei si alimentelor pe care le consumati. Sunt de preferat alimentele proaspat pregatite.</p>
          <p align="justify">Destinatiile turistice preferate pentru sejururi de litoral sunt cele oferite de coasta Marii Egee si Mediterane. Dar Turcia ofera mai mult decat albastrul marilor si caldura nisipurilor aurii. Calatorul aventurier va fi incantat de exotismul peisajului aspru al cascadelor de calcar de la Pamukkale, de peisajul unic al Capadochiei. Orasele sau porturile sunt puncte de atractie mai ales datorita vechimii lor milenare si vestigilor lasate de istoria bogata in evenimente a Turciei. Bizantismul urmat apoi de Islamism, au lasat fiecare splendori arhitectonice specifice, multe din ele dainuind chiar si pana azi. Coasta Marii Egee este un loc plin de istorie. Rezonante ale civilizatiei antice ca Troia, Hierapolis sau Efes alaturi de moderne statiuni de agrement de litoral ca Bodrum sau Marmaris sunt menite sa incante sufletul si trupul.</p>
        </div>
        
        <br class="clear" />
        
        <div class="litoral-tara">
          <a href="<?php echo $sitepath; ?>sejur-turcia/kusadasi/" class="zone" title="Kusadasi" style="background-image:url(images/kusadasi.jpg);">Kusadasi</a>
          <a href="<?php echo $sitepath; ?>sejur-turcia/bodrum/" class="zone" title="Bodrum" style="background-image:url(images/bodrum.jpg);">Bodrum</a>
          <a href="<?php echo $sitepath; ?>sejur-turcia/antalya/" class="zone" title="Antalya" style="background-image:url(images/antalya.jpg);">Antalya</a>
        </div>
        
        <br class="clear" /><br />
        
      </div>

    </div>
    <?php //include_once($_SERVER['DOCUMENT_ROOT']."/includes/newsletter_abonare.php"); ?>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>
</body>
</html>