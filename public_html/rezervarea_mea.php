<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/peste_tot.php"); ?>
<!DOCTYPE html>
<html lang="ro">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header_charset.php"); ?>
<title>Rezervarea mea - <?php echo $denumire_agentie; ?></title>
<meta name="description" content="Cum cumpar <?php echo $denumire_agentie; ?>" />
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/includes/addins_head.php'); ?>
<link href="/js/jquery_validate/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
</head>

<body onload="load_submenu()">
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/header/new_header.php"); ?>
<div id="middle">
  <div id="middleInner">
    <div class="breadcrumb">
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/navigator_new.php"); ?> 
    </div>
    <div class="NEW-column-full">
      <div id="NEW-destinatie" class="clearfix">
		
<?php
if((isset($_POST['adresa_email']) and isset($_POST['nr_rezervare'])) or isset($_GET['login']) or isset($_SESSION['login_rezervare'])) {
	if(isset($_GET['login'])) {
		$login_rez = explode('+++',base64_decode($_GET['login']));
		$email = $login_rez[0];
		$nr_rezervare = $login_rez[1];
		
		$_SESSION['login_rezervare'] = $_GET['login'];
	} else
	if(isset($_SESSION['login_rezervare'])) {
		$session_rez = explode('+++',base64_decode($_SESSION['login_rezervare']));
		$email = $session_rez[0];
		$nr_rezervare = $session_rez[1];
	} else
	if(isset($_POST['adresa_email']) and isset($_POST['nr_rezervare'])) {
		$email = $_POST['adresa_email'];
		$nr_rezervare = $_POST['nr_rezervare'];
		
		$_SESSION['login_rezervare'] = base64_encode($email.'+++'.$nr_rezervare);
	}
	
	$nr_rezervare = str_replace("OCZ", "", $nr_rezervare);
	$nr_rezervare = md5($nr_rezervare);
	$email = md5($email);
	
	$sel_rez = "SELECT 
	cerere_rezervare.*,
	useri_fizice.*
	FROM cerere_rezervare
	INNER JOIN useri_fizice ON cerere_rezervare.id_useri_fizice = useri_fizice.id_useri_fizice
	WHERE md5(cerere_rezervare.id_cerere) = '".$nr_rezervare."'
	AND md5(useri_fizice.email) = '".$email."'
	";
	$que_rez = mysql_query($sel_rez) or die(mysql_error());
	$row_rez = mysql_fetch_array($que_rez);
	
	if(mysql_num_rows($que_rez)==1) {
?>
		<h1 class="blue">Rezervari <span class="black"><?php echo $row_rez['prenume'].' '.$row_rez['nume']; ?></span></h1>
		
		<div class="Hline"></div>
		
		<div class="text-justify pad10 clearfix">
		  <div id="rezervare">
            <?php echo $row_rez['oferta_denumire']; ?>
          </div>
	<?php } else { ?>
    	  <h2 class="red">Email-ul sau numărul rezervării nu sunt corecte.</h2>
          <h2 class="green">Vă rugăm să le introduceți din nou.</h2>
          <meta http-equiv="refresh" content="3; url=/rezervarea-mea/">
          <?php unset($_SESSION['login_rezervare']); ?>
          <div class="italic bigger-12em pad10">Această pagina se va reîncărca automat in 3 secunde. Dacă nu se reîncarcă automat, <a href="/rezervarea-mea/" class="link-blue">click aici</a>.</div>
    <?php } ?>
<?php } else { ?>
		<h1 class="blue">Rezervarea mea</h1>
		
		<div class="Hline"></div>
		
		<div class="text-justify pad10 clearfix">
		
		  <div id="login">
            
            <div class="NEW-calculeaza w480">
            <form action="" method="post">
              <h2 class="blue underline pad10-0 mar0">Introduceți datele necesare mai jos</h2>
              <div class="turisti pad10" title="Introduceți adresa de email folosită la efectuarea rezervării">
                <span class="bold black bigger-15em inline-block w140">Email:</span>
                <input type="text" id="adresa_email" name="adresa_email" placeholder="Introduceți adresa de email" class="big" data-validation-engine="validate[required,custom[email]]">
              </div>
              <div class="turisti pad10" title="Introduceți numărul rezervării dumneavoastră primit pe email">
                <span class="bold black bigger-15em inline-block w140">Nr. rezervare:</span>
                <input type="text" id="nr_rezervare" name="nr_rezervare" placeholder="Introduceți numărul rezervării" class="big" data-validation-engine="validate[required]">
              </div>
              <div class="turisti pad10">
                <input type="submit" value="Afișează datele rezervării" class="button-red">
              </div>
            </form>
            </div>
            
          </div>
		
		</div>
<?php } ?>
		
      </div>
    </div>
  </div>
</div>
<div id="footer">
	<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/footer.php"); ?>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/includes/addins_bodybottom.php"); ?>

<script src="/js/jquery_validate/jquery.validationEngine-ro.js"></script>
<script src="/js/jquery_validate/jquery.validationEngine.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery("#login").validationEngine('attach', {promptPosition : "topRight"});
});
function checkHELLO(field, rules, i, options){
	if (field.val() != "HELLO") {
		return options.allrules.validate2fields.alertText;
	}
}
</script>

</body>
</html>