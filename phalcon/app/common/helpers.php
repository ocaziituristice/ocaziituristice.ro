<?php

if (!function_exists('d')) {
    function d($object, $kill = true)
    {
        echo '<pre style="text-align:left">';
        print_r($object);
        if ($kill) {
            die('END');
        }
        echo '</pre>';
    }
}

if (!function_exists('dd')) {
    function dd($object, $kill = true)
    {
        echo '<pre style="text-align:left">';
        var_dump($object);
        if ($kill) {
            die('END');
        }
        echo '</pre>';
    }
}

if (!function_exists('di')) {
    /**
     * @return \Phalcon\DiInterface
     */
    function di()
    {
        return \Phalcon\Di\FactoryDefault::getDefault();
    }
}

if (!function_exists('request')) {
    /**
     * @return mixed | \Phalcon\Http\Request
     */
    function request()
    {
        return di()->get('request');
    }
}

if (!function_exists('security')) {
    /**
     * @return mixed | Phalcon\Security
     */
    function security()
    {
        return di()->get('security');
    }
}

if (!function_exists('session')) {
    /**
     * @return mixed | Phalcon\Session\Adapter
     */
    function session()
    {
        return di()->get('session');
    }
}

if (!function_exists('legacyRoute')) {
    function legacyRoute($params) {
        return sprintf('%s://%s/legacy%s',di()->get('request')->getScheme(), di()->get('request')->getHttpHost(), str_ireplace('app.php', '', di()->get('url')->get($params)));
    }
}