<?php

namespace App\Components\Supplier\Adapter;

use App\Components\Supplier\BaseAdapter;
use GoGlobal\Exception;
use GoGlobal\Service;

class GoGlobal extends BaseAdapter
{
    private $goGlobal;

    public function __construct($config)
    {
        $this->goGlobal = new Service($config);
    }

    public function searchHotel($params = [])
    {
        if (isset($params['rooms'])) {
            $this->getHotelSearch()->addRoom($params['rooms']);
        }
    }

    /**
     * @return \GoGlobal\Request\HotelSearch
     */
    public function getHotelSearch()
    {
        return $this->goGlobal->hotelSearch();
    }

    /*
        try {
            $hotelSearch = $this->goGlobal->hotelSearch()->setDateFrom('2018-10-10')->setNights(5)->addRoom(1)->setHotelCode('122734');
            $response = $hotelSearch->getResponse();
            d($response->getData());
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }

    */
}