<?php

namespace App\Components\Supplier;

interface SupplierAdapterInterface
{
    public function getResponse();

    public function getBody();

    public function sendRequest();
}