<?php

namespace App\Components\Supplier;

use Phalcon\Config;
use Phalcon\Text;

class Factory
{
    public static function load($config)
    {
        if ($config instanceof Config) {
            $config = $config->toArray();
        }

        if (!isset($config['adapter'])) {
            throw new Exception('Please setup and adapter');
        }

        $adapter = $config['adapter'];

        if (class_exists($adapter)) {
            return new $adapter($config['options']);
        }

        if (false === stripos('\\', $adapter)) {
            if (class_exists(sprintf('App\Components\Supplier\Adapter\%s', Text::camelize($config['adapter'])))) {
                $adapter = sprintf('App\Components\Supplier\Adapter\%s', Text::camelize($config['adapter']));

                return new $adapter($config['options']);
            }
        }

        throw new Exception(sprintf('%s (%s) adapter was not found', $adapter, Text::camelize($adapter)));
    }
}