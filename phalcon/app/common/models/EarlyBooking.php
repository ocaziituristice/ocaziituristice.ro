<?php

namespace App\Models;

/**
 * EarlyBooking
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:01
 */
class EarlyBooking extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="id_oferta", type="integer", length=20, nullable=false)
     */
    protected $idOferta;

    /**
     *
     * @var string
     * @Column(column="tip", type="string", length=50, nullable=false)
     */
    protected $tip;

    /**
     *
     * @var string
     * @Column(column="end_date", type="string", nullable=false)
     */
    protected $endDate;

    /**
     *
     * @var integer
     * @Column(column="discount", type="integer", length=3, nullable=true)
     */
    protected $discount;

    /**
     *
     * @var string
     * @Column(column="early_inclus", type="string", nullable=true)
     */
    protected $earlyInclus;

    /**
     *
     * @var string
     * @Column(column="text_early", type="string", nullable=true)
     */
    protected $textEarly;

    /**
     * Method to set the value of field idOferta
     *
     * @param integer $idOferta
     * @return $this
     */
    public function setIdOferta($idOferta)
    {
        $this->idOferta = $idOferta;

        return $this;
    }

    /**
     * Method to set the value of field tip
     *
     * @param string $tip
     * @return $this
     */
    public function setTip($tip)
    {
        $this->tip = $tip;

        return $this;
    }

    /**
     * Method to set the value of field endDate
     *
     * @param string $endDate
     * @return $this
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param integer $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field earlyInclus
     *
     * @param string $earlyInclus
     * @return $this
     */
    public function setEarlyInclus($earlyInclus)
    {
        $this->earlyInclus = $earlyInclus;

        return $this;
    }

    /**
     * Method to set the value of field textEarly
     *
     * @param string $textEarly
     * @return $this
     */
    public function setTextEarly($textEarly)
    {
        $this->textEarly = $textEarly;

        return $this;
    }

    /**
     * Returns the value of field idOferta
     *
     * @return integer
     */
    public function getIdOferta()
    {
        return $this->idOferta;
    }

    /**
     * Returns the value of field tip
     *
     * @return string
     */
    public function getTip()
    {
        return $this->tip;
    }

    /**
     * Returns the value of field endDate
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Returns the value of field discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field earlyInclus
     *
     * @return string
     */
    public function getEarlyInclus()
    {
        return $this->earlyInclus;
    }

    /**
     * Returns the value of field textEarly
     *
     * @return string
     */
    public function getTextEarly()
    {
        return $this->textEarly;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("early_booking");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'early_booking';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EarlyBooking[]|EarlyBooking|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EarlyBooking|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_oferta' => 'idOferta',
            'tip' => 'tip',
            'end_date' => 'endDate',
            'discount' => 'discount',
            'early_inclus' => 'earlyInclus',
            'text_early' => 'textEarly'
        ];
    }

}
