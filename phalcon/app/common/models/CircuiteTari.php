<?php

namespace App\Models;

/**
 * CircuiteTari
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:00
 */
class CircuiteTari extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="id_circuit", type="integer", length=11, nullable=false)
     */
    protected $idCircuit;

    /**
     *
     * @var integer
     * @Column(column="id_tara", type="integer", length=11, nullable=true)
     */
    protected $idTara;

    /**
     * Method to set the value of field idCircuit
     *
     * @param integer $idCircuit
     * @return $this
     */
    public function setIdCircuit($idCircuit)
    {
        $this->idCircuit = $idCircuit;

        return $this;
    }

    /**
     * Method to set the value of field idTara
     *
     * @param integer $idTara
     * @return $this
     */
    public function setIdTara($idTara)
    {
        $this->idTara = $idTara;

        return $this;
    }

    /**
     * Returns the value of field idCircuit
     *
     * @return integer
     */
    public function getIdCircuit()
    {
        return $this->idCircuit;
    }

    /**
     * Returns the value of field idTara
     *
     * @return integer
     */
    public function getIdTara()
    {
        return $this->idTara;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("circuite_tari");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'circuite_tari';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CircuiteTari[]|CircuiteTari|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CircuiteTari|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_circuit' => 'idCircuit',
            'id_tara' => 'idTara'
        ];
    }

}
