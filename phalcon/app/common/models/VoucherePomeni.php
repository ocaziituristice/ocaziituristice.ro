<?php

namespace App\Models;

/**
 * VoucherePomeni
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:03
 */
class VoucherePomeni extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="id_voucher", type="string", length=255, nullable=false)
     */
    protected $idVoucher;

    /**
     *
     * @var string
     * @Column(column="nume_client", type="string", length=255, nullable=true)
     */
    protected $numeClient;

    /**
     *
     * @var string
     * @Column(column="email_client", type="string", length=255, nullable=true)
     */
    protected $emailClient;

    /**
     *
     * @var string
     * @Column(column="tel_client", type="string", length=255, nullable=true)
     */
    protected $telClient;

    /**
     *
     * @var string
     * @Column(column="alte_detalii_client", type="string", nullable=true)
     */
    protected $alteDetaliiClient;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field idVoucher
     *
     * @param string $idVoucher
     * @return $this
     */
    public function setIdVoucher($idVoucher)
    {
        $this->idVoucher = $idVoucher;

        return $this;
    }

    /**
     * Method to set the value of field numeClient
     *
     * @param string $numeClient
     * @return $this
     */
    public function setNumeClient($numeClient)
    {
        $this->numeClient = $numeClient;

        return $this;
    }

    /**
     * Method to set the value of field emailClient
     *
     * @param string $emailClient
     * @return $this
     */
    public function setEmailClient($emailClient)
    {
        $this->emailClient = $emailClient;

        return $this;
    }

    /**
     * Method to set the value of field telClient
     *
     * @param string $telClient
     * @return $this
     */
    public function setTelClient($telClient)
    {
        $this->telClient = $telClient;

        return $this;
    }

    /**
     * Method to set the value of field alteDetaliiClient
     *
     * @param string $alteDetaliiClient
     * @return $this
     */
    public function setAlteDetaliiClient($alteDetaliiClient)
    {
        $this->alteDetaliiClient = $alteDetaliiClient;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field idVoucher
     *
     * @return string
     */
    public function getIdVoucher()
    {
        return $this->idVoucher;
    }

    /**
     * Returns the value of field numeClient
     *
     * @return string
     */
    public function getNumeClient()
    {
        return $this->numeClient;
    }

    /**
     * Returns the value of field emailClient
     *
     * @return string
     */
    public function getEmailClient()
    {
        return $this->emailClient;
    }

    /**
     * Returns the value of field telClient
     *
     * @return string
     */
    public function getTelClient()
    {
        return $this->telClient;
    }

    /**
     * Returns the value of field alteDetaliiClient
     *
     * @return string
     */
    public function getAlteDetaliiClient()
    {
        return $this->alteDetaliiClient;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("vouchere_pomeni");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'vouchere_pomeni';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VoucherePomeni[]|VoucherePomeni|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VoucherePomeni|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'id_voucher' => 'idVoucher',
            'nume_client' => 'numeClient',
            'email_client' => 'emailClient',
            'tel_client' => 'telClient',
            'alte_detalii_client' => 'alteDetaliiClient'
        ];
    }

}
