<?php

namespace App\Models;

/**
 * ImportOferteSpeciale
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:01
 */
class ImportOferteSpeciale extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Identity
     * @Column(column="id_oferta_speciala", type="integer", length=20, nullable=false)
     */
    protected $idOfertaSpeciala;

    /**
     *
     * @var integer
     * @Column(column="id_hotel", type="integer", length=11, nullable=true)
     */
    protected $idHotel;

    /**
     *
     * @var integer
     * @Column(column="id_oferta_furnizor", type="integer", length=10, nullable=false)
     */
    protected $idOfertaFurnizor;

    /**
     *
     * @var string
     * @Column(column="Label", type="string", length=255, nullable=true)
     */
    protected $label;

    /**
     *
     * @var string
     * @Column(column="Description", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     *
     * @var string
     * @Column(column="ValidFrom", type="string", nullable=true)
     */
    protected $validFrom;

    /**
     *
     * @var string
     * @Column(column="ValidTo", type="string", nullable=true)
     */
    protected $validTo;

    /**
     *
     * @var string
     * @Column(column="TravelFrom", type="string", nullable=true)
     */
    protected $travelFrom;

    /**
     *
     * @var string
     * @Column(column="TravelTo", type="string", nullable=true)
     */
    protected $travelTo;

    /**
     *
     * @var integer
     * @Column(column="valoare_discount", type="integer", length=3, nullable=true)
     */
    protected $valoareDiscount;

    /**
     *
     * @var integer
     * @Column(column="tip_discount", type="integer", length=1, nullable=true)
     */
    protected $tipDiscount;

    /**
     *
     * @var integer
     * @Column(column="id_furnizor", type="integer", length=11, nullable=false)
     */
    protected $idFurnizor;

    /**
     * Method to set the value of field idOfertaSpeciala
     *
     * @param integer $idOfertaSpeciala
     * @return $this
     */
    public function setIdOfertaSpeciala($idOfertaSpeciala)
    {
        $this->idOfertaSpeciala = $idOfertaSpeciala;

        return $this;
    }

    /**
     * Method to set the value of field idHotel
     *
     * @param integer $idHotel
     * @return $this
     */
    public function setIdHotel($idHotel)
    {
        $this->idHotel = $idHotel;

        return $this;
    }

    /**
     * Method to set the value of field idOfertaFurnizor
     *
     * @param integer $idOfertaFurnizor
     * @return $this
     */
    public function setIdOfertaFurnizor($idOfertaFurnizor)
    {
        $this->idOfertaFurnizor = $idOfertaFurnizor;

        return $this;
    }

    /**
     * Method to set the value of field label
     *
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Method to set the value of field validFrom
     *
     * @param string $validFrom
     * @return $this
     */
    public function setValidFrom($validFrom)
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * Method to set the value of field validTo
     *
     * @param string $validTo
     * @return $this
     */
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;

        return $this;
    }

    /**
     * Method to set the value of field travelFrom
     *
     * @param string $travelFrom
     * @return $this
     */
    public function setTravelFrom($travelFrom)
    {
        $this->travelFrom = $travelFrom;

        return $this;
    }

    /**
     * Method to set the value of field travelTo
     *
     * @param string $travelTo
     * @return $this
     */
    public function setTravelTo($travelTo)
    {
        $this->travelTo = $travelTo;

        return $this;
    }

    /**
     * Method to set the value of field valoareDiscount
     *
     * @param integer $valoareDiscount
     * @return $this
     */
    public function setValoareDiscount($valoareDiscount)
    {
        $this->valoareDiscount = $valoareDiscount;

        return $this;
    }

    /**
     * Method to set the value of field tipDiscount
     *
     * @param integer $tipDiscount
     * @return $this
     */
    public function setTipDiscount($tipDiscount)
    {
        $this->tipDiscount = $tipDiscount;

        return $this;
    }

    /**
     * Method to set the value of field idFurnizor
     *
     * @param integer $idFurnizor
     * @return $this
     */
    public function setIdFurnizor($idFurnizor)
    {
        $this->idFurnizor = $idFurnizor;

        return $this;
    }

    /**
     * Returns the value of field idOfertaSpeciala
     *
     * @return integer
     */
    public function getIdOfertaSpeciala()
    {
        return $this->idOfertaSpeciala;
    }

    /**
     * Returns the value of field idHotel
     *
     * @return integer
     */
    public function getIdHotel()
    {
        return $this->idHotel;
    }

    /**
     * Returns the value of field idOfertaFurnizor
     *
     * @return integer
     */
    public function getIdOfertaFurnizor()
    {
        return $this->idOfertaFurnizor;
    }

    /**
     * Returns the value of field label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Returns the value of field validFrom
     *
     * @return string
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * Returns the value of field validTo
     *
     * @return string
     */
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * Returns the value of field travelFrom
     *
     * @return string
     */
    public function getTravelFrom()
    {
        return $this->travelFrom;
    }

    /**
     * Returns the value of field travelTo
     *
     * @return string
     */
    public function getTravelTo()
    {
        return $this->travelTo;
    }

    /**
     * Returns the value of field valoareDiscount
     *
     * @return integer
     */
    public function getValoareDiscount()
    {
        return $this->valoareDiscount;
    }

    /**
     * Returns the value of field tipDiscount
     *
     * @return integer
     */
    public function getTipDiscount()
    {
        return $this->tipDiscount;
    }

    /**
     * Returns the value of field idFurnizor
     *
     * @return integer
     */
    public function getIdFurnizor()
    {
        return $this->idFurnizor;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("import_oferte_speciale");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'import_oferte_speciale';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ImportOferteSpeciale[]|ImportOferteSpeciale|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ImportOferteSpeciale|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_oferta_speciala' => 'idOfertaSpeciala',
            'id_hotel' => 'idHotel',
            'id_oferta_furnizor' => 'idOfertaFurnizor',
            'Label' => 'label',
            'Description' => 'description',
            'ValidFrom' => 'validFrom',
            'ValidTo' => 'validTo',
            'TravelFrom' => 'travelFrom',
            'TravelTo' => 'travelTo',
            'valoare_discount' => 'valoareDiscount',
            'tip_discount' => 'tipDiscount',
            'id_furnizor' => 'idFurnizor'
        ];
    }

}
