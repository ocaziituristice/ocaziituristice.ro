<?php

namespace App\Models;

/**
 * RezervarePasageri
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:02
 */
class RezervarePasageri extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(column="id_rezervare_efectuata", type="integer", length=11, nullable=false)
     */
    protected $idRezervareEfectuata;

    /**
     *
     * @var integer
     * @Column(column="id_pasager", type="integer", length=11, nullable=true)
     */
    protected $idPasager;

    /**
     * Method to set the value of field idRezervareEfectuata
     *
     * @param integer $idRezervareEfectuata
     * @return $this
     */
    public function setIdRezervareEfectuata($idRezervareEfectuata)
    {
        $this->idRezervareEfectuata = $idRezervareEfectuata;

        return $this;
    }

    /**
     * Method to set the value of field idPasager
     *
     * @param integer $idPasager
     * @return $this
     */
    public function setIdPasager($idPasager)
    {
        $this->idPasager = $idPasager;

        return $this;
    }

    /**
     * Returns the value of field idRezervareEfectuata
     *
     * @return integer
     */
    public function getIdRezervareEfectuata()
    {
        return $this->idRezervareEfectuata;
    }

    /**
     * Returns the value of field idPasager
     *
     * @return integer
     */
    public function getIdPasager()
    {
        return $this->idPasager;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("rezervare_pasageri");
        $this->belongsTo('idPasager', 'App\Models\Pasageri', 'idPasager', ['alias' => 'Pasageri']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rezervare_pasageri';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RezervarePasageri[]|RezervarePasageri|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RezervarePasageri|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_rezervare_efectuata' => 'idRezervareEfectuata',
            'id_pasager' => 'idPasager'
        ];
    }

}
