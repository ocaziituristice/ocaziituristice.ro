<?php

namespace App\Models;

/**
 * Autocomplete
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:00
 */
class Autocomplete extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Column(column="tara", type="string", length=255, nullable=false)
     */
    protected $tara;

    /**
     *
     * @var string
     * @Column(column="zona", type="string", length=255, nullable=true)
     */
    protected $zona;

    /**
     *
     * @var string
     * @Column(column="localitate", type="string", length=255, nullable=true)
     */
    protected $localitate;

    /**
     * Method to set the value of field tara
     *
     * @param string $tara
     * @return $this
     */
    public function setTara($tara)
    {
        $this->tara = $tara;

        return $this;
    }

    /**
     * Method to set the value of field zona
     *
     * @param string $zona
     * @return $this
     */
    public function setZona($zona)
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * Method to set the value of field localitate
     *
     * @param string $localitate
     * @return $this
     */
    public function setLocalitate($localitate)
    {
        $this->localitate = $localitate;

        return $this;
    }

    /**
     * Returns the value of field tara
     *
     * @return string
     */
    public function getTara()
    {
        return $this->tara;
    }

    /**
     * Returns the value of field zona
     *
     * @return string
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Returns the value of field localitate
     *
     * @return string
     */
    public function getLocalitate()
    {
        return $this->localitate;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("autocomplete");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'autocomplete';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Autocomplete[]|Autocomplete|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Autocomplete|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'tara' => 'tara',
            'zona' => 'zona',
            'localitate' => 'localitate'
        ];
    }

}
