<?php

namespace App\Models;

/**
 * PlecariCircuit
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:02
 */
class PlecariCircuit extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="id_circuit", type="integer", length=20, nullable=false)
     */
    protected $idCircuit;

    /**
     *
     * @var string
     * @Column(column="data_plecare", type="string", nullable=false)
     */
    protected $dataPlecare;

    /**
     *
     * @var string
     * @Column(column="data_end", type="string", nullable=true)
     */
    protected $dataEnd;

    /**
     *
     * @var integer
     * @Column(column="tip_camera", type="integer", length=11, nullable=true)
     */
    protected $tipCamera;

    /**
     *
     * @var double
     * @Column(column="pret", type="double", nullable=true)
     */
    protected $pret;

    /**
     *
     * @var string
     * @Column(column="moneda", type="string", length=4, nullable=true)
     */
    protected $moneda;

    /**
     *
     * @var integer
     * @Column(column="pret_aditional", type="integer", length=11, nullable=true)
     */
    protected $pretAditional;

    /**
     *
     * @var string
     * @Column(column="moneda_aditional", type="string", length=3, nullable=true)
     */
    protected $monedaAditional;

    /**
     * Method to set the value of field idCircuit
     *
     * @param integer $idCircuit
     * @return $this
     */
    public function setIdCircuit($idCircuit)
    {
        $this->idCircuit = $idCircuit;

        return $this;
    }

    /**
     * Method to set the value of field dataPlecare
     *
     * @param string $dataPlecare
     * @return $this
     */
    public function setDataPlecare($dataPlecare)
    {
        $this->dataPlecare = $dataPlecare;

        return $this;
    }

    /**
     * Method to set the value of field dataEnd
     *
     * @param string $dataEnd
     * @return $this
     */
    public function setDataEnd($dataEnd)
    {
        $this->dataEnd = $dataEnd;

        return $this;
    }

    /**
     * Method to set the value of field tipCamera
     *
     * @param integer $tipCamera
     * @return $this
     */
    public function setTipCamera($tipCamera)
    {
        $this->tipCamera = $tipCamera;

        return $this;
    }

    /**
     * Method to set the value of field pret
     *
     * @param double $pret
     * @return $this
     */
    public function setPret($pret)
    {
        $this->pret = $pret;

        return $this;
    }

    /**
     * Method to set the value of field moneda
     *
     * @param string $moneda
     * @return $this
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Method to set the value of field pretAditional
     *
     * @param integer $pretAditional
     * @return $this
     */
    public function setPretAditional($pretAditional)
    {
        $this->pretAditional = $pretAditional;

        return $this;
    }

    /**
     * Method to set the value of field monedaAditional
     *
     * @param string $monedaAditional
     * @return $this
     */
    public function setMonedaAditional($monedaAditional)
    {
        $this->monedaAditional = $monedaAditional;

        return $this;
    }

    /**
     * Returns the value of field idCircuit
     *
     * @return integer
     */
    public function getIdCircuit()
    {
        return $this->idCircuit;
    }

    /**
     * Returns the value of field dataPlecare
     *
     * @return string
     */
    public function getDataPlecare()
    {
        return $this->dataPlecare;
    }

    /**
     * Returns the value of field dataEnd
     *
     * @return string
     */
    public function getDataEnd()
    {
        return $this->dataEnd;
    }

    /**
     * Returns the value of field tipCamera
     *
     * @return integer
     */
    public function getTipCamera()
    {
        return $this->tipCamera;
    }

    /**
     * Returns the value of field pret
     *
     * @return double
     */
    public function getPret()
    {
        return $this->pret;
    }

    /**
     * Returns the value of field moneda
     *
     * @return string
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * Returns the value of field pretAditional
     *
     * @return integer
     */
    public function getPretAditional()
    {
        return $this->pretAditional;
    }

    /**
     * Returns the value of field monedaAditional
     *
     * @return string
     */
    public function getMonedaAditional()
    {
        return $this->monedaAditional;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("plecari_circuit");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'plecari_circuit';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlecariCircuit[]|PlecariCircuit|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlecariCircuit|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_circuit' => 'idCircuit',
            'data_plecare' => 'dataPlecare',
            'data_end' => 'dataEnd',
            'tip_camera' => 'tipCamera',
            'pret' => 'pret',
            'moneda' => 'moneda',
            'pret_aditional' => 'pretAditional',
            'moneda_aditional' => 'monedaAditional'
        ];
    }

}
