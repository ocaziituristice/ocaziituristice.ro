<?php

namespace App\Models;

/**
 * RezervareFacturi
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:02
 */
class RezervareFacturi extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(column="id_factura_rezervare", type="integer", length=3, nullable=false)
     */
    protected $idFacturaRezervare;

    /**
     *
     * @var integer
     * @Column(column="id_rezervare", type="integer", length=11, nullable=true)
     */
    protected $idRezervare;

    /**
     *
     * @var integer
     * @Column(column="id_factura", type="integer", length=11, nullable=true)
     */
    protected $idFactura;

    /**
     *
     * @var string
     * @Column(column="tip_factura", type="string", length=12, nullable=true)
     */
    protected $tipFactura;

    /**
     * Method to set the value of field idFacturaRezervare
     *
     * @param integer $idFacturaRezervare
     * @return $this
     */
    public function setIdFacturaRezervare($idFacturaRezervare)
    {
        $this->idFacturaRezervare = $idFacturaRezervare;

        return $this;
    }

    /**
     * Method to set the value of field idRezervare
     *
     * @param integer $idRezervare
     * @return $this
     */
    public function setIdRezervare($idRezervare)
    {
        $this->idRezervare = $idRezervare;

        return $this;
    }

    /**
     * Method to set the value of field idFactura
     *
     * @param integer $idFactura
     * @return $this
     */
    public function setIdFactura($idFactura)
    {
        $this->idFactura = $idFactura;

        return $this;
    }

    /**
     * Method to set the value of field tipFactura
     *
     * @param string $tipFactura
     * @return $this
     */
    public function setTipFactura($tipFactura)
    {
        $this->tipFactura = $tipFactura;

        return $this;
    }

    /**
     * Returns the value of field idFacturaRezervare
     *
     * @return integer
     */
    public function getIdFacturaRezervare()
    {
        return $this->idFacturaRezervare;
    }

    /**
     * Returns the value of field idRezervare
     *
     * @return integer
     */
    public function getIdRezervare()
    {
        return $this->idRezervare;
    }

    /**
     * Returns the value of field idFactura
     *
     * @return integer
     */
    public function getIdFactura()
    {
        return $this->idFactura;
    }

    /**
     * Returns the value of field tipFactura
     *
     * @return string
     */
    public function getTipFactura()
    {
        return $this->tipFactura;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("rezervare_facturi");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rezervare_facturi';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RezervareFacturi[]|RezervareFacturi|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RezervareFacturi|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_factura_rezervare' => 'idFacturaRezervare',
            'id_rezervare' => 'idRezervare',
            'id_factura' => 'idFactura',
            'tip_factura' => 'tipFactura'
        ];
    }

}
