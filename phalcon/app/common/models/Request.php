<?php

namespace App\Models;

/**
 * Request
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:02
 */
class Request extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id_request", type="integer", length=11, nullable=false)
     */
    protected $idRequest;

    /**
     *
     * @var integer
     * @Column(column="id_tara", type="integer", length=11, nullable=true)
     */
    protected $idTara;

    /**
     *
     * @var integer
     * @Column(column="id_zona", type="integer", length=11, nullable=true)
     */
    protected $idZona;

    /**
     *
     * @var integer
     * @Column(column="id_localitate", type="integer", length=11, nullable=true)
     */
    protected $idLocalitate;

    /**
     *
     * @var string
     * @Column(column="data_plecare", type="string", nullable=true)
     */
    protected $dataPlecare;

    /**
     *
     * @var integer
     * @Column(column="nr_nopti", type="integer", length=11, nullable=true)
     */
    protected $nrNopti;

    /**
     *
     * @var integer
     * @Column(column="id_transport", type="integer", length=11, nullable=true)
     */
    protected $idTransport;

    /**
     *
     * @var integer
     * @Column(column="id_loc_plecare", type="integer", length=11, nullable=true)
     */
    protected $idLocPlecare;

    /**
     *
     * @var integer
     * @Column(column="nr_adulti", type="integer", length=11, nullable=true)
     */
    protected $nrAdulti;

    /**
     *
     * @var integer
     * @Column(column="nr_copii", type="integer", length=11, nullable=true)
     */
    protected $nrCopii;

    /**
     *
     * @var integer
     * @Column(column="varsta_copil_1", type="integer", length=11, nullable=true)
     */
    protected $varstaCopil1;

    /**
     *
     * @var integer
     * @Column(column="varsta_copil_2", type="integer", length=11, nullable=true)
     */
    protected $varstaCopil2;

    /**
     *
     * @var integer
     * @Column(column="varsta_copil_3", type="integer", length=11, nullable=true)
     */
    protected $varstaCopil3;

    /**
     *
     * @var string
     * @Column(column="data_request", type="string", nullable=true)
     */
    protected $dataRequest;

    /**
     * Method to set the value of field idRequest
     *
     * @param integer $idRequest
     * @return $this
     */
    public function setIdRequest($idRequest)
    {
        $this->idRequest = $idRequest;

        return $this;
    }

    /**
     * Method to set the value of field idTara
     *
     * @param integer $idTara
     * @return $this
     */
    public function setIdTara($idTara)
    {
        $this->idTara = $idTara;

        return $this;
    }

    /**
     * Method to set the value of field idZona
     *
     * @param integer $idZona
     * @return $this
     */
    public function setIdZona($idZona)
    {
        $this->idZona = $idZona;

        return $this;
    }

    /**
     * Method to set the value of field idLocalitate
     *
     * @param integer $idLocalitate
     * @return $this
     */
    public function setIdLocalitate($idLocalitate)
    {
        $this->idLocalitate = $idLocalitate;

        return $this;
    }

    /**
     * Method to set the value of field dataPlecare
     *
     * @param string $dataPlecare
     * @return $this
     */
    public function setDataPlecare($dataPlecare)
    {
        $this->dataPlecare = $dataPlecare;

        return $this;
    }

    /**
     * Method to set the value of field nrNopti
     *
     * @param integer $nrNopti
     * @return $this
     */
    public function setNrNopti($nrNopti)
    {
        $this->nrNopti = $nrNopti;

        return $this;
    }

    /**
     * Method to set the value of field idTransport
     *
     * @param integer $idTransport
     * @return $this
     */
    public function setIdTransport($idTransport)
    {
        $this->idTransport = $idTransport;

        return $this;
    }

    /**
     * Method to set the value of field idLocPlecare
     *
     * @param integer $idLocPlecare
     * @return $this
     */
    public function setIdLocPlecare($idLocPlecare)
    {
        $this->idLocPlecare = $idLocPlecare;

        return $this;
    }

    /**
     * Method to set the value of field nrAdulti
     *
     * @param integer $nrAdulti
     * @return $this
     */
    public function setNrAdulti($nrAdulti)
    {
        $this->nrAdulti = $nrAdulti;

        return $this;
    }

    /**
     * Method to set the value of field nrCopii
     *
     * @param integer $nrCopii
     * @return $this
     */
    public function setNrCopii($nrCopii)
    {
        $this->nrCopii = $nrCopii;

        return $this;
    }

    /**
     * Method to set the value of field varstaCopil1
     *
     * @param integer $varstaCopil1
     * @return $this
     */
    public function setVarstaCopil1($varstaCopil1)
    {
        $this->varstaCopil1 = $varstaCopil1;

        return $this;
    }

    /**
     * Method to set the value of field varstaCopil2
     *
     * @param integer $varstaCopil2
     * @return $this
     */
    public function setVarstaCopil2($varstaCopil2)
    {
        $this->varstaCopil2 = $varstaCopil2;

        return $this;
    }

    /**
     * Method to set the value of field varstaCopil3
     *
     * @param integer $varstaCopil3
     * @return $this
     */
    public function setVarstaCopil3($varstaCopil3)
    {
        $this->varstaCopil3 = $varstaCopil3;

        return $this;
    }

    /**
     * Method to set the value of field dataRequest
     *
     * @param string $dataRequest
     * @return $this
     */
    public function setDataRequest($dataRequest)
    {
        $this->dataRequest = $dataRequest;

        return $this;
    }

    /**
     * Returns the value of field idRequest
     *
     * @return integer
     */
    public function getIdRequest()
    {
        return $this->idRequest;
    }

    /**
     * Returns the value of field idTara
     *
     * @return integer
     */
    public function getIdTara()
    {
        return $this->idTara;
    }

    /**
     * Returns the value of field idZona
     *
     * @return integer
     */
    public function getIdZona()
    {
        return $this->idZona;
    }

    /**
     * Returns the value of field idLocalitate
     *
     * @return integer
     */
    public function getIdLocalitate()
    {
        return $this->idLocalitate;
    }

    /**
     * Returns the value of field dataPlecare
     *
     * @return string
     */
    public function getDataPlecare()
    {
        return $this->dataPlecare;
    }

    /**
     * Returns the value of field nrNopti
     *
     * @return integer
     */
    public function getNrNopti()
    {
        return $this->nrNopti;
    }

    /**
     * Returns the value of field idTransport
     *
     * @return integer
     */
    public function getIdTransport()
    {
        return $this->idTransport;
    }

    /**
     * Returns the value of field idLocPlecare
     *
     * @return integer
     */
    public function getIdLocPlecare()
    {
        return $this->idLocPlecare;
    }

    /**
     * Returns the value of field nrAdulti
     *
     * @return integer
     */
    public function getNrAdulti()
    {
        return $this->nrAdulti;
    }

    /**
     * Returns the value of field nrCopii
     *
     * @return integer
     */
    public function getNrCopii()
    {
        return $this->nrCopii;
    }

    /**
     * Returns the value of field varstaCopil1
     *
     * @return integer
     */
    public function getVarstaCopil1()
    {
        return $this->varstaCopil1;
    }

    /**
     * Returns the value of field varstaCopil2
     *
     * @return integer
     */
    public function getVarstaCopil2()
    {
        return $this->varstaCopil2;
    }

    /**
     * Returns the value of field varstaCopil3
     *
     * @return integer
     */
    public function getVarstaCopil3()
    {
        return $this->varstaCopil3;
    }

    /**
     * Returns the value of field dataRequest
     *
     * @return string
     */
    public function getDataRequest()
    {
        return $this->dataRequest;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("request");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'request';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Request[]|Request|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Request|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_request' => 'idRequest',
            'id_tara' => 'idTara',
            'id_zona' => 'idZona',
            'id_localitate' => 'idLocalitate',
            'data_plecare' => 'dataPlecare',
            'nr_nopti' => 'nrNopti',
            'id_transport' => 'idTransport',
            'id_loc_plecare' => 'idLocPlecare',
            'nr_adulti' => 'nrAdulti',
            'nr_copii' => 'nrCopii',
            'varsta_copil_1' => 'varstaCopil1',
            'varsta_copil_2' => 'varstaCopil2',
            'varsta_copil_3' => 'varstaCopil3',
            'data_request' => 'dataRequest'
        ];
    }

}
