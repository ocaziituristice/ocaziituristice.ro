<?php

namespace App\Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

/**
 * Rezervari
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:02
 */
class Rezervari extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id_rezervare", type="integer", length=20, nullable=false)
     */
    protected $idRezervare;

    /**
     *
     * @var integer
     * @Column(column="id_oferta", type="integer", length=20, nullable=true)
     */
    protected $idOferta;

    /**
     *
     * @var integer
     * @Column(column="id_circuit", type="integer", length=20, nullable=true)
     */
    protected $idCircuit;

    /**
     *
     * @var string
     * @Column(column="camera", type="string", length=255, nullable=true)
     */
    protected $camera;

    /**
     *
     * @var string
     * @Column(column="sex", type="string", nullable=true)
     */
    protected $sex;

    /**
     *
     * @var string
     * @Column(column="nume", type="string", length=255, nullable=true)
     */
    protected $nume;

    /**
     *
     * @var string
     * @Column(column="prenume", type="string", length=255, nullable=true)
     */
    protected $prenume;

    /**
     *
     * @var string
     * @Column(column="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     *
     * @var string
     * @Column(column="telefon", type="string", length=255, nullable=true)
     */
    protected $telefon;

    /**
     *
     * @var string
     * @Column(column="nr_adulti", type="string", length=10, nullable=true)
     */
    protected $nrAdulti;

    /**
     *
     * @var string
     * @Column(column="nr_copii", type="string", length=10, nullable=true)
     */
    protected $nrCopii;

    /**
     *
     * @var string
     * @Column(column="nr_nopti", type="string", length=10, nullable=true)
     */
    protected $nrNopti;

    /**
     *
     * @var string
     * @Column(column="asigurare", type="string", length=255, nullable=true)
     */
    protected $asigurare;

    /**
     *
     * @var string
     * @Column(column="perioada", type="string", length=255, nullable=true)
     */
    protected $perioada;

    /**
     *
     * @var string
     * @Column(column="observatii", type="string", length=255, nullable=true)
     */
    protected $observatii;

    /**
     *
     * @var string
     * @Column(column="procesata", type="string", nullable=true)
     */
    protected $procesata;

    /**
     *
     * @var string
     * @Column(column="data_adaugarii", type="string", nullable=true)
     */
    protected $dataAdaugarii;

    /**
     *
     * @var string
     * @Column(column="ip", type="string", length=255, nullable=true)
     */
    protected $ip;

    /**
     *
     * @var string
     * @Column(column="copii_varsta", type="string", nullable=true)
     */
    protected $copiiVarsta;

    /**
     *
     * @var string
     * @Column(column="data_nasterii", type="string", nullable=true)
     */
    protected $dataNasterii;

    /**
     *
     * @var string
     * @Column(column="data_nasterii_afisare", type="string", length=255, nullable=true)
     */
    protected $dataNasteriiAfisare;

    /**
     * Method to set the value of field idRezervare
     *
     * @param integer $idRezervare
     * @return $this
     */
    public function setIdRezervare($idRezervare)
    {
        $this->idRezervare = $idRezervare;

        return $this;
    }

    /**
     * Method to set the value of field idOferta
     *
     * @param integer $idOferta
     * @return $this
     */
    public function setIdOferta($idOferta)
    {
        $this->idOferta = $idOferta;

        return $this;
    }

    /**
     * Method to set the value of field idCircuit
     *
     * @param integer $idCircuit
     * @return $this
     */
    public function setIdCircuit($idCircuit)
    {
        $this->idCircuit = $idCircuit;

        return $this;
    }

    /**
     * Method to set the value of field camera
     *
     * @param string $camera
     * @return $this
     */
    public function setCamera($camera)
    {
        $this->camera = $camera;

        return $this;
    }

    /**
     * Method to set the value of field sex
     *
     * @param string $sex
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Method to set the value of field nume
     *
     * @param string $nume
     * @return $this
     */
    public function setNume($nume)
    {
        $this->nume = $nume;

        return $this;
    }

    /**
     * Method to set the value of field prenume
     *
     * @param string $prenume
     * @return $this
     */
    public function setPrenume($prenume)
    {
        $this->prenume = $prenume;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field telefon
     *
     * @param string $telefon
     * @return $this
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Method to set the value of field nrAdulti
     *
     * @param string $nrAdulti
     * @return $this
     */
    public function setNrAdulti($nrAdulti)
    {
        $this->nrAdulti = $nrAdulti;

        return $this;
    }

    /**
     * Method to set the value of field nrCopii
     *
     * @param string $nrCopii
     * @return $this
     */
    public function setNrCopii($nrCopii)
    {
        $this->nrCopii = $nrCopii;

        return $this;
    }

    /**
     * Method to set the value of field nrNopti
     *
     * @param string $nrNopti
     * @return $this
     */
    public function setNrNopti($nrNopti)
    {
        $this->nrNopti = $nrNopti;

        return $this;
    }

    /**
     * Method to set the value of field asigurare
     *
     * @param string $asigurare
     * @return $this
     */
    public function setAsigurare($asigurare)
    {
        $this->asigurare = $asigurare;

        return $this;
    }

    /**
     * Method to set the value of field perioada
     *
     * @param string $perioada
     * @return $this
     */
    public function setPerioada($perioada)
    {
        $this->perioada = $perioada;

        return $this;
    }

    /**
     * Method to set the value of field observatii
     *
     * @param string $observatii
     * @return $this
     */
    public function setObservatii($observatii)
    {
        $this->observatii = $observatii;

        return $this;
    }

    /**
     * Method to set the value of field procesata
     *
     * @param string $procesata
     * @return $this
     */
    public function setProcesata($procesata)
    {
        $this->procesata = $procesata;

        return $this;
    }

    /**
     * Method to set the value of field dataAdaugarii
     *
     * @param string $dataAdaugarii
     * @return $this
     */
    public function setDataAdaugarii($dataAdaugarii)
    {
        $this->dataAdaugarii = $dataAdaugarii;

        return $this;
    }

    /**
     * Method to set the value of field ip
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Method to set the value of field copiiVarsta
     *
     * @param string $copiiVarsta
     * @return $this
     */
    public function setCopiiVarsta($copiiVarsta)
    {
        $this->copiiVarsta = $copiiVarsta;

        return $this;
    }

    /**
     * Method to set the value of field dataNasterii
     *
     * @param string $dataNasterii
     * @return $this
     */
    public function setDataNasterii($dataNasterii)
    {
        $this->dataNasterii = $dataNasterii;

        return $this;
    }

    /**
     * Method to set the value of field dataNasteriiAfisare
     *
     * @param string $dataNasteriiAfisare
     * @return $this
     */
    public function setDataNasteriiAfisare($dataNasteriiAfisare)
    {
        $this->dataNasteriiAfisare = $dataNasteriiAfisare;

        return $this;
    }

    /**
     * Returns the value of field idRezervare
     *
     * @return integer
     */
    public function getIdRezervare()
    {
        return $this->idRezervare;
    }

    /**
     * Returns the value of field idOferta
     *
     * @return integer
     */
    public function getIdOferta()
    {
        return $this->idOferta;
    }

    /**
     * Returns the value of field idCircuit
     *
     * @return integer
     */
    public function getIdCircuit()
    {
        return $this->idCircuit;
    }

    /**
     * Returns the value of field camera
     *
     * @return string
     */
    public function getCamera()
    {
        return $this->camera;
    }

    /**
     * Returns the value of field sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Returns the value of field nume
     *
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * Returns the value of field prenume
     *
     * @return string
     */
    public function getPrenume()
    {
        return $this->prenume;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field telefon
     *
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Returns the value of field nrAdulti
     *
     * @return string
     */
    public function getNrAdulti()
    {
        return $this->nrAdulti;
    }

    /**
     * Returns the value of field nrCopii
     *
     * @return string
     */
    public function getNrCopii()
    {
        return $this->nrCopii;
    }

    /**
     * Returns the value of field nrNopti
     *
     * @return string
     */
    public function getNrNopti()
    {
        return $this->nrNopti;
    }

    /**
     * Returns the value of field asigurare
     *
     * @return string
     */
    public function getAsigurare()
    {
        return $this->asigurare;
    }

    /**
     * Returns the value of field perioada
     *
     * @return string
     */
    public function getPerioada()
    {
        return $this->perioada;
    }

    /**
     * Returns the value of field observatii
     *
     * @return string
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * Returns the value of field procesata
     *
     * @return string
     */
    public function getProcesata()
    {
        return $this->procesata;
    }

    /**
     * Returns the value of field dataAdaugarii
     *
     * @return string
     */
    public function getDataAdaugarii()
    {
        return $this->dataAdaugarii;
    }

    /**
     * Returns the value of field ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Returns the value of field copiiVarsta
     *
     * @return string
     */
    public function getCopiiVarsta()
    {
        return $this->copiiVarsta;
    }

    /**
     * Returns the value of field dataNasterii
     *
     * @return string
     */
    public function getDataNasterii()
    {
        return $this->dataNasterii;
    }

    /**
     * Returns the value of field dataNasteriiAfisare
     *
     * @return string
     */
    public function getDataNasteriiAfisare()
    {
        return $this->dataNasteriiAfisare;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("rezervari");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rezervari';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Rezervari[]|Rezervari|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Rezervari|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_rezervare' => 'idRezervare',
            'id_oferta' => 'idOferta',
            'id_circuit' => 'idCircuit',
            'camera' => 'camera',
            'sex' => 'sex',
            'nume' => 'nume',
            'prenume' => 'prenume',
            'email' => 'email',
            'telefon' => 'telefon',
            'nr_adulti' => 'nrAdulti',
            'nr_copii' => 'nrCopii',
            'nr_nopti' => 'nrNopti',
            'asigurare' => 'asigurare',
            'perioada' => 'perioada',
            'observatii' => 'observatii',
            'procesata' => 'procesata',
            'data_adaugarii' => 'dataAdaugarii',
            'ip' => 'ip',
            'copii_varsta' => 'copiiVarsta',
            'data_nasterii' => 'dataNasterii',
            'data_nasterii_afisare' => 'dataNasteriiAfisare'
        ];
    }

}
