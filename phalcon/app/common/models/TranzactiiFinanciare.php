<?php

namespace App\Models;

/**
 * TranzactiiFinanciare
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:02
 */
class TranzactiiFinanciare extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id_tranzactie", type="integer", length=5, nullable=false)
     */
    protected $idTranzactie;

    /**
     *
     * @var string
     * @Column(column="tip_tranzactie", type="string", nullable=true)
     */
    protected $tipTranzactie;

    /**
     *
     * @var string
     * @Column(column="mod_tranzactie", type="string", nullable=true)
     */
    protected $modTranzactie;

    /**
     *
     * @var string
     * @Column(column="data_tranzactie", type="string", nullable=true)
     */
    protected $dataTranzactie;

    /**
     *
     * @var string
     * @Column(column="suma_tranzactie", type="string", length=5, nullable=true)
     */
    protected $sumaTranzactie;

    /**
     *
     * @var string
     * @Column(column="valuta", type="string", nullable=true)
     */
    protected $valuta;

    /**
     * Method to set the value of field idTranzactie
     *
     * @param integer $idTranzactie
     * @return $this
     */
    public function setIdTranzactie($idTranzactie)
    {
        $this->idTranzactie = $idTranzactie;

        return $this;
    }

    /**
     * Method to set the value of field tipTranzactie
     *
     * @param string $tipTranzactie
     * @return $this
     */
    public function setTipTranzactie($tipTranzactie)
    {
        $this->tipTranzactie = $tipTranzactie;

        return $this;
    }

    /**
     * Method to set the value of field modTranzactie
     *
     * @param string $modTranzactie
     * @return $this
     */
    public function setModTranzactie($modTranzactie)
    {
        $this->modTranzactie = $modTranzactie;

        return $this;
    }

    /**
     * Method to set the value of field dataTranzactie
     *
     * @param string $dataTranzactie
     * @return $this
     */
    public function setDataTranzactie($dataTranzactie)
    {
        $this->dataTranzactie = $dataTranzactie;

        return $this;
    }

    /**
     * Method to set the value of field sumaTranzactie
     *
     * @param string $sumaTranzactie
     * @return $this
     */
    public function setSumaTranzactie($sumaTranzactie)
    {
        $this->sumaTranzactie = $sumaTranzactie;

        return $this;
    }

    /**
     * Method to set the value of field valuta
     *
     * @param string $valuta
     * @return $this
     */
    public function setValuta($valuta)
    {
        $this->valuta = $valuta;

        return $this;
    }

    /**
     * Returns the value of field idTranzactie
     *
     * @return integer
     */
    public function getIdTranzactie()
    {
        return $this->idTranzactie;
    }

    /**
     * Returns the value of field tipTranzactie
     *
     * @return string
     */
    public function getTipTranzactie()
    {
        return $this->tipTranzactie;
    }

    /**
     * Returns the value of field modTranzactie
     *
     * @return string
     */
    public function getModTranzactie()
    {
        return $this->modTranzactie;
    }

    /**
     * Returns the value of field dataTranzactie
     *
     * @return string
     */
    public function getDataTranzactie()
    {
        return $this->dataTranzactie;
    }

    /**
     * Returns the value of field sumaTranzactie
     *
     * @return string
     */
    public function getSumaTranzactie()
    {
        return $this->sumaTranzactie;
    }

    /**
     * Returns the value of field valuta
     *
     * @return string
     */
    public function getValuta()
    {
        return $this->valuta;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("tranzactii_financiare");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tranzactii_financiare';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TranzactiiFinanciare[]|TranzactiiFinanciare|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TranzactiiFinanciare|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_tranzactie' => 'idTranzactie',
            'tip_tranzactie' => 'tipTranzactie',
            'mod_tranzactie' => 'modTranzactie',
            'data_tranzactie' => 'dataTranzactie',
            'suma_tranzactie' => 'sumaTranzactie',
            'valuta' => 'valuta'
        ];
    }

}
