<?php

namespace App\Models;

/**
 * PretPivotAdaugat
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:02
 */
class PretPivotAdaugat extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id_pret", type="integer", length=20, nullable=false)
     */
    protected $idPret;

    /**
     *
     * @var integer
     * @Column(column="id_hotel", type="integer", length=20, nullable=false)
     */
    protected $idHotel;

    /**
     *
     * @var integer
     * @Column(column="id_oferta", type="integer", length=20, nullable=true)
     */
    protected $idOferta;

    /**
     *
     * @var integer
     * @Column(column="nr_formula", type="integer", length=20, nullable=true)
     */
    protected $nrFormula;

    /**
     *
     * @var string
     * @Column(column="data_start", type="string", nullable=false)
     */
    protected $dataStart;

    /**
     *
     * @var string
     * @Column(column="data_end", type="string", nullable=false)
     */
    protected $dataEnd;

    /**
     *
     * @var integer
     * @Column(column="tip_camera", type="integer", length=11, nullable=false)
     */
    protected $tipCamera;

    /**
     *
     * @var double
     * @Column(column="pret", type="double", length=11, nullable=false)
     */
    protected $pret;

    /**
     *
     * @var string
     * @Column(column="moneda", type="string", length=4, nullable=false)
     */
    protected $moneda;

    /**
     *
     * @var string
     * @Column(column="pret_pivot", type="string", nullable=true)
     */
    protected $pretPivot;

    /**
     *
     * @var integer
     * @Column(column="adulti", type="integer", length=3, nullable=true)
     */
    protected $adulti;

    /**
     *
     * @var integer
     * @Column(column="copii", type="integer", length=3, nullable=true)
     */
    protected $copii;

    /**
     *
     * @var string
     * @Column(column="varsta_min", type="string", length=3, nullable=true)
     */
    protected $varstaMin;

    /**
     *
     * @var string
     * @Column(column="varsta_max", type="string", length=3, nullable=true)
     */
    protected $varstaMax;

    /**
     *
     * @var double
     * @Column(column="procent", type="double", length=11, nullable=true)
     */
    protected $procent;

    /**
     *
     * @var string
     * @Column(column="formula", type="string", length=255, nullable=true)
     */
    protected $formula;

    /**
     *
     * @var string
     * @Column(column="copil1", type="string", length=255, nullable=true)
     */
    protected $copil1;

    /**
     *
     * @var string
     * @Column(column="copil2", type="string", length=255, nullable=true)
     */
    protected $copil2;

    /**
     *
     * @var string
     * @Column(column="copil3", type="string", length=255, nullable=true)
     */
    protected $copil3;

    /**
     *
     * @var string
     * @Column(column="teztour", type="string", nullable=true)
     */
    protected $teztour;

    /**
     *
     * @var integer
     * @Column(column="id_spo", type="integer", length=20, nullable=true)
     */
    protected $idSpo;

    /**
     *
     * @var integer
     * @Column(column="id_masa", type="integer", length=11, nullable=true)
     */
    protected $idMasa;

    /**
     * Method to set the value of field idPret
     *
     * @param integer $idPret
     * @return $this
     */
    public function setIdPret($idPret)
    {
        $this->idPret = $idPret;

        return $this;
    }

    /**
     * Method to set the value of field idHotel
     *
     * @param integer $idHotel
     * @return $this
     */
    public function setIdHotel($idHotel)
    {
        $this->idHotel = $idHotel;

        return $this;
    }

    /**
     * Method to set the value of field idOferta
     *
     * @param integer $idOferta
     * @return $this
     */
    public function setIdOferta($idOferta)
    {
        $this->idOferta = $idOferta;

        return $this;
    }

    /**
     * Method to set the value of field nrFormula
     *
     * @param integer $nrFormula
     * @return $this
     */
    public function setNrFormula($nrFormula)
    {
        $this->nrFormula = $nrFormula;

        return $this;
    }

    /**
     * Method to set the value of field dataStart
     *
     * @param string $dataStart
     * @return $this
     */
    public function setDataStart($dataStart)
    {
        $this->dataStart = $dataStart;

        return $this;
    }

    /**
     * Method to set the value of field dataEnd
     *
     * @param string $dataEnd
     * @return $this
     */
    public function setDataEnd($dataEnd)
    {
        $this->dataEnd = $dataEnd;

        return $this;
    }

    /**
     * Method to set the value of field tipCamera
     *
     * @param integer $tipCamera
     * @return $this
     */
    public function setTipCamera($tipCamera)
    {
        $this->tipCamera = $tipCamera;

        return $this;
    }

    /**
     * Method to set the value of field pret
     *
     * @param double $pret
     * @return $this
     */
    public function setPret($pret)
    {
        $this->pret = $pret;

        return $this;
    }

    /**
     * Method to set the value of field moneda
     *
     * @param string $moneda
     * @return $this
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Method to set the value of field pretPivot
     *
     * @param string $pretPivot
     * @return $this
     */
    public function setPretPivot($pretPivot)
    {
        $this->pretPivot = $pretPivot;

        return $this;
    }

    /**
     * Method to set the value of field adulti
     *
     * @param integer $adulti
     * @return $this
     */
    public function setAdulti($adulti)
    {
        $this->adulti = $adulti;

        return $this;
    }

    /**
     * Method to set the value of field copii
     *
     * @param integer $copii
     * @return $this
     */
    public function setCopii($copii)
    {
        $this->copii = $copii;

        return $this;
    }

    /**
     * Method to set the value of field varstaMin
     *
     * @param string $varstaMin
     * @return $this
     */
    public function setVarstaMin($varstaMin)
    {
        $this->varstaMin = $varstaMin;

        return $this;
    }

    /**
     * Method to set the value of field varstaMax
     *
     * @param string $varstaMax
     * @return $this
     */
    public function setVarstaMax($varstaMax)
    {
        $this->varstaMax = $varstaMax;

        return $this;
    }

    /**
     * Method to set the value of field procent
     *
     * @param double $procent
     * @return $this
     */
    public function setProcent($procent)
    {
        $this->procent = $procent;

        return $this;
    }

    /**
     * Method to set the value of field formula
     *
     * @param string $formula
     * @return $this
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Method to set the value of field copil1
     *
     * @param string $copil1
     * @return $this
     */
    public function setCopil1($copil1)
    {
        $this->copil1 = $copil1;

        return $this;
    }

    /**
     * Method to set the value of field copil2
     *
     * @param string $copil2
     * @return $this
     */
    public function setCopil2($copil2)
    {
        $this->copil2 = $copil2;

        return $this;
    }

    /**
     * Method to set the value of field copil3
     *
     * @param string $copil3
     * @return $this
     */
    public function setCopil3($copil3)
    {
        $this->copil3 = $copil3;

        return $this;
    }

    /**
     * Method to set the value of field teztour
     *
     * @param string $teztour
     * @return $this
     */
    public function setTeztour($teztour)
    {
        $this->teztour = $teztour;

        return $this;
    }

    /**
     * Method to set the value of field idSpo
     *
     * @param integer $idSpo
     * @return $this
     */
    public function setIdSpo($idSpo)
    {
        $this->idSpo = $idSpo;

        return $this;
    }

    /**
     * Method to set the value of field idMasa
     *
     * @param integer $idMasa
     * @return $this
     */
    public function setIdMasa($idMasa)
    {
        $this->idMasa = $idMasa;

        return $this;
    }

    /**
     * Returns the value of field idPret
     *
     * @return integer
     */
    public function getIdPret()
    {
        return $this->idPret;
    }

    /**
     * Returns the value of field idHotel
     *
     * @return integer
     */
    public function getIdHotel()
    {
        return $this->idHotel;
    }

    /**
     * Returns the value of field idOferta
     *
     * @return integer
     */
    public function getIdOferta()
    {
        return $this->idOferta;
    }

    /**
     * Returns the value of field nrFormula
     *
     * @return integer
     */
    public function getNrFormula()
    {
        return $this->nrFormula;
    }

    /**
     * Returns the value of field dataStart
     *
     * @return string
     */
    public function getDataStart()
    {
        return $this->dataStart;
    }

    /**
     * Returns the value of field dataEnd
     *
     * @return string
     */
    public function getDataEnd()
    {
        return $this->dataEnd;
    }

    /**
     * Returns the value of field tipCamera
     *
     * @return integer
     */
    public function getTipCamera()
    {
        return $this->tipCamera;
    }

    /**
     * Returns the value of field pret
     *
     * @return double
     */
    public function getPret()
    {
        return $this->pret;
    }

    /**
     * Returns the value of field moneda
     *
     * @return string
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * Returns the value of field pretPivot
     *
     * @return string
     */
    public function getPretPivot()
    {
        return $this->pretPivot;
    }

    /**
     * Returns the value of field adulti
     *
     * @return integer
     */
    public function getAdulti()
    {
        return $this->adulti;
    }

    /**
     * Returns the value of field copii
     *
     * @return integer
     */
    public function getCopii()
    {
        return $this->copii;
    }

    /**
     * Returns the value of field varstaMin
     *
     * @return string
     */
    public function getVarstaMin()
    {
        return $this->varstaMin;
    }

    /**
     * Returns the value of field varstaMax
     *
     * @return string
     */
    public function getVarstaMax()
    {
        return $this->varstaMax;
    }

    /**
     * Returns the value of field procent
     *
     * @return double
     */
    public function getProcent()
    {
        return $this->procent;
    }

    /**
     * Returns the value of field formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Returns the value of field copil1
     *
     * @return string
     */
    public function getCopil1()
    {
        return $this->copil1;
    }

    /**
     * Returns the value of field copil2
     *
     * @return string
     */
    public function getCopil2()
    {
        return $this->copil2;
    }

    /**
     * Returns the value of field copil3
     *
     * @return string
     */
    public function getCopil3()
    {
        return $this->copil3;
    }

    /**
     * Returns the value of field teztour
     *
     * @return string
     */
    public function getTeztour()
    {
        return $this->teztour;
    }

    /**
     * Returns the value of field idSpo
     *
     * @return integer
     */
    public function getIdSpo()
    {
        return $this->idSpo;
    }

    /**
     * Returns the value of field idMasa
     *
     * @return integer
     */
    public function getIdMasa()
    {
        return $this->idMasa;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("pret_pivot_adaugat");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pret_pivot_adaugat';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PretPivotAdaugat[]|PretPivotAdaugat|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PretPivotAdaugat|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_pret' => 'idPret',
            'id_hotel' => 'idHotel',
            'id_oferta' => 'idOferta',
            'nr_formula' => 'nrFormula',
            'data_start' => 'dataStart',
            'data_end' => 'dataEnd',
            'tip_camera' => 'tipCamera',
            'pret' => 'pret',
            'moneda' => 'moneda',
            'pret_pivot' => 'pretPivot',
            'adulti' => 'adulti',
            'copii' => 'copii',
            'varsta_min' => 'varstaMin',
            'varsta_max' => 'varstaMax',
            'procent' => 'procent',
            'formula' => 'formula',
            'copil1' => 'copil1',
            'copil2' => 'copil2',
            'copil3' => 'copil3',
            'teztour' => 'teztour',
            'id_spo' => 'idSpo',
            'id_masa' => 'idMasa'
        ];
    }

}
