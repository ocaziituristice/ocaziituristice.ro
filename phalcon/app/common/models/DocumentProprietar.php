<?php

namespace App\Models;

/**
 * DocumentProprietar
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:00
 */
class DocumentProprietar extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="id_document_proprietar", type="integer", length=5, nullable=true)
     */
    protected $idDocumentProprietar;

    /**
     *
     * @var integer
     * @Column(column="id_client_furnizor", type="integer", length=5, nullable=true)
     */
    protected $idClientFurnizor;

    /**
     * Method to set the value of field idDocumentProprietar
     *
     * @param integer $idDocumentProprietar
     * @return $this
     */
    public function setIdDocumentProprietar($idDocumentProprietar)
    {
        $this->idDocumentProprietar = $idDocumentProprietar;

        return $this;
    }

    /**
     * Method to set the value of field idClientFurnizor
     *
     * @param integer $idClientFurnizor
     * @return $this
     */
    public function setIdClientFurnizor($idClientFurnizor)
    {
        $this->idClientFurnizor = $idClientFurnizor;

        return $this;
    }

    /**
     * Returns the value of field idDocumentProprietar
     *
     * @return integer
     */
    public function getIdDocumentProprietar()
    {
        return $this->idDocumentProprietar;
    }

    /**
     * Returns the value of field idClientFurnizor
     *
     * @return integer
     */
    public function getIdClientFurnizor()
    {
        return $this->idClientFurnizor;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("document_proprietar");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_proprietar';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentProprietar[]|DocumentProprietar|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentProprietar|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_document_proprietar' => 'idDocumentProprietar',
            'id_client_furnizor' => 'idClientFurnizor'
        ];
    }

}
