<?php

namespace App\Models;

/**
 * LegVoucherPasageri
 * 
 * @package App\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2018-02-23, 15:59:01
 */
class LegVoucherPasageri extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="id_voucher", type="integer", length=11, nullable=false)
     */
    protected $idVoucher;

    /**
     *
     * @var integer
     * @Column(column="id_pasager", type="integer", length=11, nullable=false)
     */
    protected $idPasager;

    /**
     * Method to set the value of field idVoucher
     *
     * @param integer $idVoucher
     * @return $this
     */
    public function setIdVoucher($idVoucher)
    {
        $this->idVoucher = $idVoucher;

        return $this;
    }

    /**
     * Method to set the value of field idPasager
     *
     * @param integer $idPasager
     * @return $this
     */
    public function setIdPasager($idPasager)
    {
        $this->idPasager = $idPasager;

        return $this;
    }

    /**
     * Returns the value of field idVoucher
     *
     * @return integer
     */
    public function getIdVoucher()
    {
        return $this->idVoucher;
    }

    /**
     * Returns the value of field idPasager
     *
     * @return integer
     */
    public function getIdPasager()
    {
        return $this->idPasager;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ocaziituristice");
        $this->setSource("leg_voucher_pasageri");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'leg_voucher_pasageri';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return LegVoucherPasageri[]|LegVoucherPasageri|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return LegVoucherPasageri|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id_voucher' => 'idVoucher',
            'id_pasager' => 'idPasager'
        ];
    }

}
