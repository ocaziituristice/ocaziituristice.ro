<?php

namespace App\Tasks;

use App\Models\ImportHoteluri;
use App\Models\LocalitatiCorespondent;
use App\Models\Tari;
use App\Components\Supplier\Exception as SupplierException;
use App\Components\Supplier\Factory;
use GoGlobal\Exception;
use GoGlobal\Service;

class ImportTask extends TaskBase
{
    protected $idSupplier = 226;

    public function hotelAction()
    {
        $config = [
            'agency' => '1521429',
            'user' => 'DRMVYINXML',
            'password' => 'D3NACEQUDR',
        ];

        $goGlobal = new Service($config);

        try {
            $hotelSearch = $goGlobal->hotelSearch()->setDateFrom('2018-10-10')->setNights(5)->addRoom(1)->setHotelCode('122734');
            $response = $hotelSearch->getResponse();
            d($response->getData());
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }

    public function testAction()
    {
        try {
            $supplierAdapter = Factory::load($this->config->application->suppliers->goGlobal);

            $response = $supplierAdapter->getHotelSearch(['from' => '2018-10-10', 'nights' => 5, 'rooms' => 1, 'hotelCode' => '122734'])->setDateFrom('2018-10-10')->setNights(5)->addRoom(1)->setHotelCode('122734')->getResponse()->getData();

            d($response);

        } catch (SupplierException $exception) {
            $this->error($exception->getMessage());
        }
    }

    /**
     * CSV structure
     * Array
     * (
     * [0] => countryid
     * [1] => country
     * [2] => isoCode
     * [3] => cityid
     * [4] => city
     * [5] => hotelid
     * [6] => Name
     * [7] => Address
     * [8] => phone
     * [9] => fax
     * [10] => stars
     * [11] => starsID
     * [12] => Longitude
     * [13] => Latidude
     * [14] => IsApartment
     * )
     */
    public function csvAction()
    {
        $file = BASE_PATH . '/../adm/import_goglobal/Extended.csv';

        if (!is_readable($file)) {
            $this->error('File not found or unreadable', true);
        }

        $isoCodes = Tari::find(['conditions' => 'countryCode IS NOT NULL'])->toArray();
        $isoCodes = array_column($isoCodes, 'countryCode', 'idTara');

        if (($handle = fopen($file, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, "|")) !== false) {
                if (!in_array($data[2], $isoCodes)) {
                    continue;
                }

                $countryId = array_search($data[2], $isoCodes);

                $loc = LocalitatiCorespondent::findFirst([
                    'conditions' => 'idFurnizor = :supplier: AND codeFurnizor = :cityIdCsv: AND numeFurnizor = :cityCsv:',
                    'bind' => [
                        'supplier' => $this->idSupplier,
                        'cityIdCsv' => $data[3],
                        'cityCsv' => $data[4]
                    ]
                ]);

                if (!$loc instanceof LocalitatiCorespondent) {
                    $loc = new LocalitatiCorespondent();
                }

                $loc->setIdFurnizor($this->idSupplier)->setNumeFurnizor($data[4])->setCodeFurnizor($data[3]);

                if (!$loc->save()) {
                    foreach ($loc->getMessages() as $message) {
                        $this->error($message->getMessage());
                    }

                    exit;
                }

                $hotels = ImportHoteluri::findFirst([
                    'conditions' => 'numeHotel = :nume: AND steleHotel = :stele: AND city = :city: AND idFurnizor = :furnizor:',
                    'bind' => [
                        'nume' => $data[6],
                        'stele' => $data[10],
                        'city' => $data[3],
                        'furnizor' => $this->idSupplier
                    ]
                ]);

                if ($hotels instanceof ImportHoteluri) {
                    continue;
                }

                $hotels = new ImportHoteluri();
                $hotels->setIdHotel($data[5])
                       ->setNumeHotel($data[6])
                       ->setSteleHotel($data[10])
                       ->setCity($data[3])
                       ->setIdFurnizor($this->idSupplier)
                       ->setCity($loc->getId())
                       ->setCountry($countryId);
                if (!$hotels->save()) {
                    foreach ($hotels->getMessages() as $message) {
                        $this->error($message->getMessage());
                    }
                    exit();
                }

                $this->success(sprintf('Hotel %s added with success', $data[6]));
            }

            fclose($handle);
        }
    }
}