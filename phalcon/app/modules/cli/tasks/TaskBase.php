<?php

namespace App\Tasks;

use Phalcon\Cli\Task;
use Phalcon\Text;

class TaskBase extends Task
{
    private $foregroundColors = [];
    
    private $backgroundColors = [];

    protected $params;

    public function onConstruct()
    {
        // Set up shell colors
        $this->foregroundColors['black'] = '0;30';
        $this->foregroundColors['dark_gray'] = '1;30';
        $this->foregroundColors['blue'] = '0;34';
        $this->foregroundColors['light_blue'] = '1;34';
        $this->foregroundColors['green'] = '0;32';
        $this->foregroundColors['light_green'] = '1;32';
        $this->foregroundColors['cyan'] = '0;36';
        $this->foregroundColors['light_cyan'] = '1;36';
        $this->foregroundColors['red'] = '0;31';
        $this->foregroundColors['light_red'] = '1;31';
        $this->foregroundColors['purple'] = '0;35';
        $this->foregroundColors['light_purple'] = '1;35';
        $this->foregroundColors['brown'] = '0;33';
        $this->foregroundColors['yellow'] = '1;33';
        $this->foregroundColors['light_gray'] = '0;37';
        $this->foregroundColors['white'] = '1;37';

        $this->backgroundColors['black'] = '40';
        $this->backgroundColors['red'] = '41';
        $this->backgroundColors['green'] = '42';
        $this->backgroundColors['yellow'] = '43';
        $this->backgroundColors['blue'] = '44';
        $this->backgroundColors['magenta'] = '45';
        $this->backgroundColors['cyan'] = '46';
        $this->backgroundColors['light_gray'] = '47';
    }

    protected function hasParam($param)
    {
        return isset($this->params[$param]);
    }

    protected function getParam($param)
    {
        return $this->params[$param];
    }

    protected function setParam($param, $value)
    {
        $this->params[$param] = $value;

        return $this;
    }

    public function getColoredString($string, $foreground_color = null, $background_color = null)
    {
        $colored_string = "";

        // Check if given foreground color found
        if (isset($this->foregroundColors[$foreground_color])) {
            $colored_string .= "\033[" . $this->foregroundColors[$foreground_color] . "m";
        }
        // Check if given background color found
        if (isset($this->backgroundColors[$background_color])) {
            $colored_string .= "\033[" . $this->backgroundColors[$background_color] . "m";
        }

        // Add string and end coloring
        $colored_string .= $string . "\033[0m";

        return $colored_string;
    }

    public function getForegroundColors()
    {
        return array_keys($this->foregroundColors);
    }

    public function getBackgroundColors()
    {
        return array_keys($this->backgroundColors);
    }

    public function mainAction()
    {
        try {
            $task = new \ReflectionClass(get_called_class());
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }

        if ($task->getName() == 'MainTask') {
            echo 'Usage: php run <command>';
            echo PHP_EOL;
            exit();
        }

        echo 'Available actions to call:';
        echo PHP_EOL;

        foreach ($task->getMethods(\ReflectionMethod::IS_PUBLIC) as $action) {
            if ($action->class == $task->name && Text::endsWith($action->name, 'Action')) {
                $this->info('    ' . str_ireplace('Action', '', $action->name));
            }
        }
    }

    protected function prepareMessage($string)
    {
        if (is_array($string) || is_object($string)) {
            return json_encode($string);
        }

        return $string;
    }

    public function error($string, $kill = false)
    {
        echo $this->getColoredString(sprintf('%s', $this->prepareMessage($string)), 'red');
        echo PHP_EOL;

        if ($kill) {
            exit;
        }
    }

    public function success($string)
    {
        echo $this->getColoredString(sprintf('%s', $this->prepareMessage($string)), 'green');
        echo PHP_EOL;
    }

    public function info($string)
    {
        echo $this->getColoredString(sprintf('%s', $this->prepareMessage($string)), 'light_blue');
        echo PHP_EOL;
    }

    public function notification($string)
    {
        echo $this->getColoredString(sprintf('%s', $this->prepareMessage($string)), 'yellow');
        echo PHP_EOL;
    }

    public function message($string)
    {
        echo $this->prepareMessage($string);
        echo PHP_EOL;
    }
}