<?php
/**
 * Created by PhpStorm.
 * User: colbiocin
 * Date: 13/03/2018
 * Time: 20:58
 */

namespace App\Tasks;

use GoGlobal\Exception;
use GoGlobal\Service;


class CertificationTask extends TaskBase
{

    /** search multiple different room type */
    public function testoneAction()
    {
        $config = [
            'agency' => '1521429',
            'user' => 'DRMVYINXML',
            'password' => 'D3NACEQUDR',
        ];

        $goGlobal = new Service($config);

        try {
            $hotelSearch = $goGlobal->hotelSearch()->setCity(75)->setDateFrom('2018-07-31')->setNights(1)->addRoom(1)->addRoom(2)->addRoom(3)->setHotelCode('113825');
            $response = $hotelSearch->getResponse();
            d($response->getData());
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }

    }

    /** search multiple identical room type */
    public function testtwoAction()
    {
        $config = [
            'agency' => '1521429',
            'user' => 'DRMVYINXML',
            'password' => 'D3NACEQUDR',
        ];

        $goGlobal = new Service($config);

        try {
            $hotelSearch = $goGlobal->hotelSearch()->setCity(3715)->setDateFrom('2018-07-31')->setNights(3)->addRoom(2)->addRoom(2)->addRoom(2)->setHotelCode('88752');
            $response = $hotelSearch->getResponse();
            d($response->getData());
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }

    }

    /** search room with children */
    public function testthreeAction()
    {
        $config = [
            'agency' => '1521429',
            'user' => 'DRMVYINXML',
            'password' => 'D3NACEQUDR',
        ];

        $goGlobal = new Service($config);

        try {
            $hotelSearch = $goGlobal->hotelSearch()->setCity(75)->setDateFrom('2018-07-31')->setNights(3)->addRoom(1, array(7, 6))->setHotelCode('273');
            $response = $hotelSearch->getResponse();
            d($response->getData());
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }

    }

    /** search room with children */
    public function testsevenAction()
    {
        $config = [
            'agency' => '1521429',
            'user' => 'DRMVYINXML',
            'password' => 'D3NACEQUDR',
        ];

        $goGlobal = new Service($config);

        try {
            $hotelSearch = $goGlobal->hotelSearch()->setCity(1133)->setDateFrom('2018-07-31')->setNights(3)->addRoom(2, [2], 1)->setSort(1)->setHotelCode('17822');
            $response = $hotelSearch->getResponse()->getData();
            $this->error($response[17822]['rooms'][0]['hotel_search_code']);

            $room = [
                'adults' => [
                    ['firstname' => 'Ion', 'lastname' => 'Gheorghe', 'title' => 'MR.'],
                    ['firstname' => 'Mariana', 'lastname' => 'Gheorghe', 'title' => 'MRS.']
                ],
                'children' => [
                    ['firstname' => 'Mihaita', 'lastname' => 'Gheorghe', 'age' => 2]
                ],
                'infant' => 1
            ];

            $bookingInsert = $goGlobal->bookingInsert()
                ->addRoom($room)
                ->setNights(3)
                ->setAlternative(1)
                ->setLeader(1)
                ->setHotelSearchCode($response[17822]['rooms'][0]['hotel_search_code'])
                ->setDateFrom('2018-07-31');
        d($bookingInsert->toXml());
//            $response = $hotelSearch->getResponse();
            $insertResponse = $bookingInsert->getResponse();

//            $bookingCancel = $goGlobal->bookingCancel();


            d($insertResponse->getData());
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }

    }


}