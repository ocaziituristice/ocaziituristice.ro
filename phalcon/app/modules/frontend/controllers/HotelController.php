<?php

namespace App\Frontend\Controllers;

use App\Models\ImportHoteluri;
use App\Models\LocalitatiCorespondent;

class HotelController extends ControllerBase
{

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function searchByCityAction()
    {
//        ini_set('display_startup_errors',1);
//        ini_set('display_errors',1);
//        error_reporting(-1);

        $search = $this->request->getQuery('search');
        $idFurnizor = $this->request->getQuery('idFurnizor');
        $idL = $this->request->getQuery('idL');
        $limit = $this->request->getQuery('limit');
        $orderBy = $this->request->getQuery('orderBy');


        $builder = $this->modelsManager->createBuilder()->from(['ih' => ImportHoteluri::class])
            ->columns('ih.id, ih.idHotel, ih.numeHotel, ih.steleHotel, ih.idHotelCorespondent, lc.numeFurnizor as denLoc, lc.id as idLocalitate')
            ->innerJoin(LocalitatiCorespondent::class, 'lc.id = ih.city', 'lc');

        if (!empty($idFurnizor)) {
            $builder
                ->andWhere("ih.idFurnizor = :idFurnizor:", ["idFurnizor" => $idFurnizor]);
        }

        if (!empty($idL)) {
            $builder
                ->andWhere("lc.id = :idL:", ["idL" => $idL]);
        }

        if (!empty($search)) {
            $builder
                ->andWhere("lc.numeFurnizor LIKE :numeFurnizor:", ["numeFurnizor" => "" . $search . "%"]);
        }

        $builder->groupBy('lc.numeFurnizor');

        if (!empty($orderBy)) {
            $builder->orderBy($orderBy);
        } else {
            $builder->orderBy("lc.numeFurnizor");
        }

        if (!empty($limit)) {
            $builder
                ->limit($limit);
        } else {
            $builder
                ->limit(10);
        }

//        dd($builder->getQuery()->getSql());

        $builder = $builder
            ->getQuery()
            ->execute();

        $results = $builder->toArray();
//        $results['pagination']['more'] = true;
        return $this->response->setContentType('json')->setContent(json_encode($results));
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function searchByHotelAction()
    {
//        ini_set('display_startup_errors',1);
//        ini_set('display_errors',1);
//        error_reporting(-1);

        $search = $this->request->getQuery('search');
        $idFurnizor = $this->request->getQuery('idFurnizor');
        $idL = $this->request->getQuery('idL');
        $limit = $this->request->getQuery('limit');
        $orderBy = $this->request->getQuery('orderBy');


        $builder = $this->modelsManager->createBuilder()->from(['ih' => ImportHoteluri::class])
            ->columns('ih.id, ih.idHotel, ih.numeHotel, ih.steleHotel, ih.idHotelCorespondent, lc.numeFurnizor as denLoc, lc.id as idLocalitate')
            ->innerJoin(LocalitatiCorespondent::class, 'lc.id = ih.city', 'lc');

        if (!empty($idFurnizor)) {
            $builder
                ->andWhere("ih.idFurnizor = :idFurnizor:", ["idFurnizor" => $idFurnizor]);
        }

        if (!empty($idL)) {
            $builder
                ->andWhere("lc.id = :idL:", ["idL" => $idL]);
        }

        if (!empty($search)) {
            $builder
                ->andWhere("ih.numeHotel LIKE :numeHotel:", ["numeHotel" => "" . $search . "%"]);
        }

        if (!empty($orderBy)) {
            $builder->orderBy($orderBy);
        } else {
            $builder->orderBy("ih.numeHotel");
        }

        if (!empty($limit)) {
            $builder
                ->limit($limit);
        } else {
            $builder
                ->limit(10);
        }

        $builder = $builder
            ->getQuery()
            ->execute();

        $results = $builder->toArray();
//        $results['pagination']['more'] = true;
        return $this->response->setContentType('json')->setContent(json_encode($results));
    }
}
