<?php

namespace App\Frontend\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function show404Action()
    {
        $content = 'Pagina nu a fost gasita...';

        return $this->response->setContent($content)->setStatusCode(404)->send();
    }
}
