<?php

namespace App\Frontend;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Dispatcher;

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces([
            'App\Frontend\Controllers' => __DIR__ . '/controllers/',
            'App\Models' => APP_PATH . '/common/models'
        ]);

        $loader->register();
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        /**
         * Setting up the view component
         */
        $di->set('view', function () {
            $view = new View();
            $view->setDI($this);
            $view->setViewsDir(__DIR__ . '/views/');

            $view->registerEngines([
                '.volt' => 'voltShared',
                '.phtml' => PhpEngine::class
            ]);

            return $view;
        });

        $di->set('dispatcher', function () {

            //Create an EventsManager
            $eventsManager = new EventsManager();

            //Attach a listener
            $eventsManager->attach("dispatch:beforeException", function ($event, $dispatcher, $exception) {
                error_log(sprintf('Module: %s Controller: %s Action: %s', $dispatcher->getModuleName(), $dispatcher->getControllerName(), $dispatcher->getActionName()));
                //Handle 404
                switch ($exception->getCode()) {
                    //case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward([
                            'module' => 'frontend',
                            'namespace' => 'App\Frontend\Controllers',
                            'controller' => 'index',
                            'action' => 'show404'
                        ]);

                        return false;
                }
            });

            $dispatcher = new \Phalcon\Mvc\Dispatcher();

            //Bind the EventsManager to the dispatcher
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;

        }, true);
    }
}
