<?php

use Phalcon\Di\FactoryDefault;

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

$di = new FactoryDefault();

require_once APP_PATH . '/common/helpers.php';
require APP_PATH . '/config/services.php';
require APP_PATH . '/config/services_web.php';

$config = $di->getConfig();

include APP_PATH . '/config/loader.php';