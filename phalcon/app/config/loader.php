<?php

use Phalcon\Loader;

$loader = new Loader();

$loader->registerFiles([BASE_PATH . '/vendor/autoload.php']);

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'App\Models' => APP_PATH . '/common/models/',
    'App\Components\Supplier' => APP_PATH . '/common/components/supplier',
    'App\Components\Supplier\Adapter' => APP_PATH . '/common/components/supplier/adapter',
]);

/**
 * Register module classes
 */
$loader->registerClasses([
    'App\Frontend' => APP_PATH . '/modules/frontend/Module.php',
    'App\Cli' => APP_PATH . '/modules/cli/Module.php'
]);

$loader->register();