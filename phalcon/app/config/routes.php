<?php

use Phalcon\Mvc\Router;

$router = new Router();
$router->removeExtraSlashes(true);
$router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);

$router->mount(require_once __DIR__ . '/routes/frontend.php');

$apps = $this->get('config')->apps->toArray();
$hosts = array_column($apps, 'hostname', 'namespace');
$module = 'frontend';

if (in_array($this->get('request')->getServer('HTTP_HOST'), $hosts)) {
    $module = strtolower(array_search($this->get('request')->getServer('HTTP_HOST'), $hosts));
}

$app = $apps[$module];

$router->setDefaultModule($module);
$router->setDefaultNamespace(sprintf('%s\%s\Controllers', 'App', $app['namespace']));

return $router;

//return require __DIR__ . '/routes/frontend.php';