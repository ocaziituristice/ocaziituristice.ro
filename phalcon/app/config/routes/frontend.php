<?php

use Phalcon\Mvc\Router\Group;

$front = new Group([
    'module' => 'frontend',
    'namespace' => sprintf('%s\%s\Controllers', 'App', $this->get('config')->apps->frontend->namespace),
]);

$front->setHostname($this->get('config')->apps->frontend->hostname);

$front->add('/product/get-offers', 'Product::getOffers', ['POST'])->setName('get-offers');
$front->add('/hotel/search-by-city', 'Hotel::searchByCity', ['GET', 'POST'])->setName('hotel-search-by-city');
$front->add('/hotel/search-by-hotel', 'Hotel::searchByHotel', ['GET', 'POST'])->setName('hotel-search-by-hotel');

return $front;
